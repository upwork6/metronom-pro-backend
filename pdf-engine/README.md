# Docker Image of PDF Engine

### Details About the Image

The PDF Engine is based on 3 main frameworks/libraries:
  - Django==2.0.3
  - gunicorn==19.7.1
  - WeasyPrint==0.36

### Start the PDF Engine Docker

```
$ make pdf

```

It will provide:
* [localhost:8200](http://localhost:8200/) - `PDF Engine`



### Docs:

The Entire Engine is based on Django Framework and 


The flowing endpoints are available:

* [localhost:8200/](http://localhost:8200/) - `POST` requests taking a `data as Django View context` and returning the PDF

* [localhost:8200/test/pdf/](http://localhost:8200/test/pdf/) - `GET` PDFTester => Only for Test purpose with static data as `context`

* [localhost:8200/test/pdf/?lang=de-ch&doc_type=project&font=Proxima&filename=My%20Lovely%20PDF](http://localhost:8200/test/pdf/?lang=de-ch&doc_type=project&font=Proxima&filename=My%20Lovely%20PDF) - `GET` PDFTester => For Test purpose as previous but with all available `GET parameters`:
  - lang = [en, de, de-ch] #default=en
  - font = [Proxima, Lato, Roboto, Saira_Semi_Condensed] # default=Proxima
  - doc_type = [project,campaign] #default=project
  - filename = Sample Name Document #default="Sample Title" 


* [localhost:8200/test/html/](http://localhost:8200/test/html/) - `GET` HTMLTester => Only for Test purpose with static data as `context` returning the HTML rendered before being converted into PDF (helping to speed up development for the HTML with Gulp and BrowserSync)

* [localhost:8200/docs/](http://localhost:8200/docs/) - `GET` short documentation similar like this one
