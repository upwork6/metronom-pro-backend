from django.apps import AppConfig


class HtmltopdfConfig(AppConfig):
    name = 'html2pdf'
