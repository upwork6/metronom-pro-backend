from django.utils import timezone
from datetime import timedelta

company_metadata = {
    # META CONTENT
    "header": {
        "image": "html2pdf/img/pp-logo.svg",
        "text": ""
    },
    "footer": {
        "image": "",
        # text redered as html {{ footer.text|safe }}
        "text": "Partner & Partner AG ⋅ Integrierte Kommunikation ⋅ "
                "Rudolfstrasse 37 ⋅ CH-8400 Winterthur</br>Telefon +41 52 269 16 60 ⋅"
                " www.partner-partner.ch ⋅ kontakt@partner-partner.ch"
    },
    "currency": "CHF",
    "locale": "de",

    # OFFER FIELDS
    "agency_address_header": "Partner & Partner AG · Rudolfstrasse 37 · CH-8400 Winterthur",
    "customer": {
        "company": "axsana AG",
        # These can be titles prefixing a person's name,
        # e.g.: Mr, Mrs, Miss, Ms, Sir, Dr, Lady or Lord
        "contact_person_title": "Herr",
        "contact_person_name": "Samuel Eglin",
        "address": "Technoparkstrasse 1",
        "country_code": "CH",
        "postal_code": "8005",
        "city": "Winterthur",
    },
    "offer": {
        "offer_date": (timezone.now()).strftime("%Y-%m-%d %H:%M:%S"),
        "valid_until_date": (timezone.now() + timedelta(days=90)).strftime("%Y-%m-%d %H:%M:%S"),
        "manager": "Franziska Horber",
    },
}


terms = {
    # "terms": {
    #     "title": "General Terms of Business",
    #     "public_id": "O-15-1416-01",
    #     "terms_date": timezone.now() - timedelta(days=600),
    #     "paragraphs": [
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use 2",
    #             "text": "Schriftform Abweichungen von den nachfolgenden Bedingungen – wie auch des Auftrags und insbesondere der Kündigung desselben – bedürfen der Schriftform."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms",
    #             "text": "Schriftform Abweichungen von den nachfolgenden Bedingungen – wie auch des Auftrags und insbesondere der Kündigung desselben – bedürfen der Schriftform."
    #         },
    #         {
    #             "title": "Other Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Other stuff",
    #             "text": "Schriftform Abweichungen von den nachfolgenden Bedingungen – wie auch des Auftrags und insbesondere der Kündigung desselben – bedürfen der Schriftform."
    #         },
    #         {
    #             "title": "Other terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Schriftform Abweichungen von den nachfolgenden Bedingungen – wie auch des Auftrags und insbesondere der Kündigung desselben – bedürfen der Schriftform."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Schriftform Abweichungen von den nachfolgenden Bedingungen – wie auch des Auftrags und insbesondere der Kündigung desselben – bedürfen der Schriftform."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Schriftform Abweichungen von den nachfolgenden Bedingungen – wie auch des Auftrags und insbesondere der Kündigung desselben – bedürfen der Schriftform."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Schriftform Abweichungen von den nachfolgenden Bedingungen – wie auch des Auftrags und insbesondere der Kündigung desselben – bedürfen der Schriftform."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #         {
    #             "title": "Terms of Use",
    #             "text": "Vertragsbedingungen Die nachstehenden Bedingungen regeln die Beziehung zwischen Auftraggeber und der P&P AG. Sie sind integrierter Bestandteil eines Auftrags."
    #         },
    #         {
    #             "title": "Terms of Use 3",
    #             "text": "Leistungen der Firma P&P AG P&P AG erbringt folgende Leistungen im Bereich der visuellen Kommunikation: Auftragsvorbereitung und Auftragsplanung onzeption und Entwurf etailgestaltung und Ausführung Produktionsorganisation und -überwachung Für weitere Leistungen, insbesondere im Bereich des Texts, der Produkt- und Formgestaltung, arbeitet P&P AG nach den Richtlinien der einschlägigen Berufsverbände."
    #         },
    #     ]
    # }
}


service_optional = {
    "is_optional": True,
    "is_optional_text": "Optional",
    "is_external_cost": False,
    "is_external_cost_text": "External Cost",
}
service_external = {
    "is_optional": False,
    "is_optional_text": "Optional",
    "is_external_cost": True,
    "is_external_cost_text": "External Cost",
}
service_basic = {
    "is_optional": False,
    "is_optional_text": "Optional",
    "is_external_cost": False,
    "is_external_cost_text": "External Cost",
}


def get_project():
    return {
        **company_metadata,
        "public_id": "O-02280-003",
        "public_id_text": "Offer",
        "optional": False,
        "optional_text": "Option",
        "title": "Relaunch Webseite www.axsana.ch",
        "customer_refference": "Eigentümer: CS Funds AG, handelnd für CS REF Green Property, Uetlibergstrasse 231, 8045 Zürich Objekt: 07619, LOKwerk, Zürcherstrasse 51, 8406 Winterthur",
        "description": "<div class='f7'>Inhalt Basisversion</div><div class='f1'>u.a. bestehend aus den Elementen:<ul><li>Home (inkl. Teaser: über axsana, News, Aufbauprojekt, EPD Grundwissen, XAD Leistungsangebot, Kontakt)</li><li>Navigation (Gestaltung der Navigation basierend auf den Inhalten zu den Interessengruppen)</li><li>Unterseiten (basierend auf den Inhalten zu den Interessengruppen)</li><li>Google Maps Einbindung (Verwendung auf Kontaktseite und auf Wunsch im Footer)</li><li>Kontakt (separate Seite)</li><li>Footer (statischer Inhalt; Text inkl. Verlinkung und auf Wunsch Google Maps mit Standortanzeige)</li></ul>Optional: Newsletter, Kollaborationstool</div><br><div class='f1'>Weitere Inhaltsseiten im Rahmen der initialen Funktionen der Webseite können ohne zusätzlichen Aufwand vom Content Manager erstellt werden. Die Basisversion kann sowohl durch ergänzende Funktionen angereichert und ausgebaut werden. Entsprechende Aufwände werden bei Bedarf geschätzt.</div>",
        "total_optional": 4040,
        "total_optional_text": "Total Optionen",
        "total_external": 800,
        "total_external_text": "Total Externe Kosten",
        "total_tax": 1500,
        "is_tax_included": True,
        "total_tax_text": "MwSt. [Werte %]",
        "tax_rate": "8.0%",
        "total_offer": 17000,
        "total_offer_text": "Total CHF inkl. MwSt.",
        "discount_explanation": "Rabatte im Wert von CHF 330.00 sind im Offerttotal bereits abgezogen.",
        "total_discount": "330.00",
        "services_title": "Positionen",
        "services": [
            {
                "position": 1,
                **service_basic,
                "total_amount": 360,
                "discount": 0,
                "accepted": None,
                "title": "Grundkonzept",
                # description redered as html {{ description|safe }}
                "description": "Entwicklung Basiskonzept inkl. Optimierung für die responsive Umsetzung",
                                "steps": "Unverbindliches Mengengerüst: Vorbereitung/Briefing 2.0h à CHF 240.00, Konzeption Senior 3.0h à CHF 180.00, Präsentation Senior 3.0h à 150.00, Qaulitätskontrolle/Nachbearbeitung 2.0h à CHF 180.00, Organisation 8.0h à CHF 150.00"
            },
            {
                "position": 2,
                **service_basic,
                "total_amount": 2880,
                "discount": 0,
                "accepted": None,
                "title": "UX Design-Wireframe",
                "description": "UI / UX: Erstellung funktionaler Wireframes und Moodboard inkl. pragmatische Klärung des Markenauftritts von axsana AG",
                                "steps": "Unverbindliches Mengengerüst: Vorbereitung/Briefing 2.0h à CHF 240.00, Konzeption Senior 3.0h à CHF 180.00, Organisation 8.0h à CHF 150.00"

            },
            {
                "position": 3,
                **service_basic,
                "total_amount": 2880,
                "discount": 0,
                "accepted": {
                    "accepted_text": "Freigabe",
                    "accepted_date": (timezone.now() - timedelta(days=2)).strftime("%Y-%m-%d %H:%M:%S"),
                    "accepted_in_offer": "Verankerung Messeauftritt"
                },
                "title": "Screendesign",
                "description": "Exemplarische Entwürfe für die wichtigsten Seitentypen (Look&Feel Home und Inhaltsseiten, 2-3 Screens) inkl. 1 Korrekturrunde.",
                                "steps": "Unverbindliches Mengengerüst: Vorbereitung/Briefing 2.0h à CHF 240.00, Gestaltung Junior 3.0h à CHF 180.00, Meeting 8.0h à CHF 150.00 Organisation 8.0h à CHF 150.00"

            },
            {
                "position": 4,
                **service_basic,
                "total_amount": 210,
                "discount": 0,
                "accepted": {
                    "accepted_text": "Freigabe",
                    "accepted_date": (timezone.now() - timedelta(days=2)).strftime("%Y-%m-%d %H:%M:%S"),
                    "accepted_in_offer": "Heisser Draht"
                },
                "title": "Bildauswahl",
                "description": "Bildrecherche (Stimmungsbilder)",
                                "steps": "Unverbindliches Mengengerüst: Vorbereitung/Briefing 2.0h à CHF 240.00, Gestaltung Junior 3.0h à CHF 180.00, Meeting 3.0h à 150.00, Organisation 8.0h à CHF 150.00"
            },
            {
                "position": 5,
                **service_basic,
                "total_amount": 150,
                "discount": 0,
                "accepted": {
                    "accepted_date": (timezone.now() - timedelta(days=2)).strftime("%Y-%m-%d %H:%M:%S"),
                    "accepted_in_offer": "Verankerung Messeauftritt"
                },
                "title": "Bildbearbeitung Digital",
                "description": "Bildbearbeitung Web-optimiert",
                                "steps": "Unverbindliches Mengengerüst: Ausführung 2.0h à CHF 240.00, Qualitätskonrolle/Nachbearbeitung 3.0h à CHF 180.00, Organisation 3.0h à 150.00"

            },
            {
                "position": 6,
                **service_basic,
                "total_amount": 150,
                "discount": 0,
                "accepted": None,
                "title": "Enddatenaufbereitung",
                "description": "Aufbereitung der Assets und Dokumentation des Designs zur Übergabe an Programmierer",
                                "steps": "Unverbindliches Mengengerüst: Ausführung 2.0h à CHF 240.00, Qualitätskonrolle/Nachbearbeitung 3.0h à CHF 180.00, Organisation 3.0h à 150.00"

            },
            {
                "position": 7,
                **service_basic,
                "total_amount": 4800,
                "discount": 0,
                "accepted": None,
                "title": "Programmierung/Coding",
                "description": "Programmierung der gesamten Website mit gewünschten Anforderungen, Animationen, usw.",
                                "steps": "Unverbindliches Mengengerüst: Vorbereitung/Briefing 2.0h à CHF 240.00, Entwicklung 3.0h à CHF 180.00, Qualitätskontrolle/ Nachbearbeitung 3.0h à 150.00, Organisation 3.0h à 150.00"

            },
            {
                "position": 8,
                **service_basic,
                "total_amount": 600,
                "discount": 0,
                "accepted": None,
                "title": "Testing/Bugfixing",
                "description": "Testing für gängige Browsertypen und Endgeräte",
                                "steps": "Unverbindliches Mengengerüst: Vorbereitung/Briefing 2.0h à CHF 240.00, Entwicklung 3.0h à CHF 180.00, Qualitätskontrolle/ Nachbearbeitung 3.0h à 150.00, Organisation 3.0h à 150.00"
            },
            {
                "position": 9,
                **service_basic,
                "has_subtotal": 16730,
                "has_subtotal_text": "Zwischentotal CHF",
                "total_amount": 2400,
                "discount": 0,
                "accepted": None,
                "title": "Projektkoordination",
                "description": "Jobstruktur anlegen, Offerte erstellen, Abklärungen",
                                "steps": "Unverbindliches Mengengerüst: Administration 2.0h à CHF 240.00, Organisation 3.0h à 150.00, Offertwesen 3.0h à 150.00, Projekt schliessen 0.5h ä 100.00"
            },
            {
                "position": 10,
                **service_optional,
                "has_subtotal": 1800,
                "has_subtotal_text": "Zwischentotal CHF",
                "discount": 0,
                "accepted": None,
                "title": "Programmierung/Coding – CMS-internes Kollaborationstool",
                "description": "Verwaltung von Office-Dateien durch Content-Manager im CMS. Eingeloggte User können die Dateien im Frontend herunterladen. Alle Dokumente werden im CMS gespeichert und über die interne Mediathek genutzt. Unsere Empfehlung: wir setzen initial die CMS-interne Lösung ein und evaluieren mit der Weiterentwicklung des Projektes im Laufe vom 2018, wie die Kollaborationsplattform erweitert werden kann.",
                                "steps": "Unverbindliches Mengengerüst: Vorbereitung/Briefing 2.0h à CHF 240.00, Entwicklung 3.0h à CHF 180.00, Qualitätskontrolle/ Nachbearbeitung 3.0h à 150.00, Organisation 3.0h à 150.00"
            },
            {
                "position": 11,
                **service_external,
                "total_amount": 1440,
                "discount": 0,
                "accepted": None,
                "title": "Grundkonzept – Externes Kollaborationstool",
                "description": "Evaluierung eines externen Kollaborationstools inkl. Priorisierung mehrerer Services(sowohl Opensource, wie auch kostenpflichtig), Spezifizierug von Anforderungen zusammen mit dem Kunden, Einbindungsaufwand, Einschätzung zur Verwendung inkl. Alle Dokumente werden im CMS gespeichert und über die interne Mediathek genutzt. Unsere Empfehlung: wir setzen initial die CMS-interne Lösung ein und evaluieren mit der Weiterentwicklung des Office Account haben, OwnCloud erschwert zeitgleiches arbeiten.",
                                "steps": "Unverbindliches Mengengerüst: Vorbereitung/Briefing 2.0h à CHF 240.00, Entwicklung 3.0h à CHF 180.00, Qualitätskontrolle/ Nachbearbeitung 3.0h à 150.00, Organisation 3.0h à 150.00"
            },
            {
                "position": 12,
                **service_basic,
                "has_subtotal": 270,
                "has_subtotal_text": "Zwischentotal CHF",
                "total_amount": 600,
                "discount": 330,
                "accepted": None,
                "title": "Programmierung/Coding – Newsletter",
                "description": "Aufsetzen und Implementierung eines Newsletters. Verwendetes Tool: Mailchimp",
                                "steps": "Unverbindliches Mengengerüst: Vorbereitung/Briefing 2.0h à CHF 240.00, Entwicklung 3.0h à CHF 180.00, Testing/Bugfixing 3.0h à CHF 180.00, Qualitätskontrolle/Nachbearbeitung 3.0h à 150.00, Organisation 3.0h à 150.00"
            },
            {
                "position": 13,
                **service_optional,
                "total_amount": 800,
                "discount": 0,
                "title": "Bildlizenz",
                "description": "Bildbudget, approx.",
                "steps": "",
            },
            {
                "position": 14,
                **service_external,
                "total_amount": 800,
                "discount": 0,
                "title": "Hosting",
                "description": "Web- und Mailserver, DNS-Einträge, für 1 Jahr",
                "steps": "",
            },
            {
                "position": 15,
                **service_external,
                "total_amount": 1200,
                "discount": 0,
                "title": "Domain",
                "description": "Web- und Mailserver, DNS-Einträge, für 1 Jahr",
                "steps": "",
            },
            {
                "position": 16,
                **service_external,
                "total_amount": 1800,
                "discount": 0,
                "title": "Hosting/Domain",
                "description": "Web- und Mailserver, DNS-Einträge, für 1 Jahr",
                "steps": "",
            }
        ],
        **terms
    }


def get_campaign():
    return {
        **company_metadata,
        "public_id": "O-02280-002",
        "public_id_text": "Kampagnen-Offerte",
        "optional": False,
        "title": "Messeauftritt",
        "customer_refference": "",
        "description": "Weitere Inhaltsseiten im Rahmen der initialen Funktionen der Webseite können ohne zusätzlichen Aufwand vom Content Manager erstellt werden. Die Basisversion kann sowohl durch ergänzende Funktionen angereichert und ausgebaut werden. Entsprechende Aufwände werden bei Bedarf geschätzt.",
        "summary_text": "Zusammenfassung",
        "total_optional": 19500,
        "total_optional_text": "Total Optionen",
        "total_tax": 4800,
        "total_tax_text": "MwSt. [Werte %]",
        "total_offer": 64800,
        "total_offer_text": "Total CHF inkl. MwSt.",
        "projects_title": "Projekt-Offerte",
        "projects": create_offers(),
        **terms
    }


def create_offers():
    projects = [get_project() for x in range(5)]
    projects[1]['optional'] = True
    projects[3]['optional'] = True
    projects[0]['total_offer'] = 11000
    projects[1]['total_offer'] =  7500
    projects[2]['total_offer'] = 18500
    projects[3]['total_offer'] = 12000
    projects[4]['total_offer'] = 40500
    for service in projects[1]['services']:
        service['steps'] = ""
    return projects
