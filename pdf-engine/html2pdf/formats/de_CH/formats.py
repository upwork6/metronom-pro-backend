from html2pdf.formats.de.formats import *
# these are the separators for non-monetary numbers. For monetary numbers,
# the DECIMAL_SEPARATOR is a . (decimal point) and the THOUSAND_SEPARATOR is a
# ' (single quote).
# For details, please refer to http://www.bk.admin.ch/dokumentation/sprachen/04915/05016/index.html?lang=de
# (in German) and the documentation
DECIMAL_SEPARATOR = ','
# THOUSAND_SEPARATOR = '\xa0'  # non-breaking space
THOUSAND_SEPARATOR = '’'
NUMBER_GROUPING = 3
