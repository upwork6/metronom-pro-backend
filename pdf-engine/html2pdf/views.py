from django.views.generic import View, TemplateView
from django.shortcuts import render_to_response
from django.http import HttpResponse
from html2pdf.pdf import PDF
from html2pdf.tests.test_pdf import get_campaign, get_project
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.utils import translation
from datetime import datetime
from ast import literal_eval


@method_decorator(csrf_exempt, name='dispatch')
class PDFTester(View, PDF):

    def get(self, request):
        http_method_names = ['get']

        # set defaults for GET parameters
        pdf_options = get_default_options(request)
        # decide which template to use based on doc_type GET parameter
        context = get_project()
        html_template = 'html2pdf/project-offer.html'
        if pdf_options['doc_type'] == 'campaign':
            context = get_campaign()
            html_template = 'html2pdf/campaign-offer.html'
        # get the Style To overwrite the Default CSS
        style_overwrites = "body { background-color: transparent;}"

        # Make use of `to_pdf` method because of its inheritance from PDF class
        response = self.to_pdf(
            request=request,
            html_template=html_template,
            options=pdf_options,
            style_overwrites=style_overwrites,
            content_data=context)
        return response


# Only to speed development of the HTML with Gulp and BrowserSync
@method_decorator(csrf_exempt, name='dispatch')
class HTMLTester(View):
    http_method_names = ['get']

    def get(self, request):

        # set defaults for GET parameters
        html_option = get_default_options(request)
        # decide which template to use based on doc_type GET parameter
        context = get_project()

        html_template = 'html2pdf/project-offer.html'
        if html_option['doc_type'] == 'campaign':
            context = get_campaign()
            html_template = 'html2pdf/campaign-offer.html'

        # Manually Overwrites the Language and Formats to a certain Country
        translation.activate(html_option['lang'])
        response = render_to_response(html_template, context)
        return response


class Docs(TemplateView):
    template_name = 'docs.html'


def health(request):
    return HttpResponse('ok')


@method_decorator(csrf_exempt, name='dispatch')
class GeneratePDF(View, PDF):
    """
    WARNING
    If you don't use the Inheritance from the PDF class to generate
    the PDF Document and try to access just the method `to_pdf` directly
    it won't work even if you give it the same and exact parameters.
    The reason specified in Django documentation: The `request` looses its context
    because this method has to return a full and valid HTTPResponse.
    """
    http_method_names = ['post']

    def post(self, request):
        """
        This View is able to make use of 4 GET parameters:
            lang=[en, de, de-ch] # default=en
            doc_type=[campaign, project] # default=campaign
            font=[Proxima, Lato, Roboto, Saira_Semi_Condensed] # default=Proxima
            filename=Sample Name Document # default="Sample Title"
        """

        # set defaults for GET parameters
        option = get_default_options(request)
        # decide which template to use based on doc_type GET parameter
        html_template = 'html2pdf/project-offer.html'
        if option['doc_type'] == 'campaign':
            html_template = 'html2pdf/campaign-offer.html'

        context, data = {}, "{}"
        if request.POST:
            data = request.POST.get('context_data')

        if data:
            data_dictionary = literal_eval(data)
            for key, value in data_dictionary.items():
                context[key] = value

        # get the Style To overwrite the Default CSS
        style_to_overwrite = "body { background-color: transparent;}"

        # Make use of `to_pdf` method because of its inheritance from PDF class
        option['filename'] = f"{context.get('title', 'Sample Offer Title')} PDF"
        context['offer']['offer_date'] = self.convert_str_to_date_format(context['offer']['offer_date'])
        context['offer']['valid_until_date'] = self.convert_str_to_date_format(context['offer']['valid_until_date'])
        if context['terms'] and context['terms']['terms_date']:
            context['terms']['terms_date'] = self.convert_str_to_date_format(context['terms']['terms_date'])
        for service in context['services']:
            if service['accepted'] and service['accepted']['accepted_date']:
                service['accepted']['accepted_date'] = self.convert_str_to_date_format(service['accepted']['accepted_date'])
        response = self.to_pdf(
            request=request,
            html_template=html_template,
            options=option,
            style_overwrites=option,
            content_data=context)
        return response

    @staticmethod
    def convert_str_to_date_format(date_as_string=None):
        if date_as_string:
            return datetime.strptime(date_as_string, "%Y-%m-%d %H:%M:%S")
        return None


def get_default_options(request=None):

    options = {
        'doc_type': 'project',
        'lang': "en",
        'font': "Proxima",
        'filename': ""
    }
    if request.GET:
        options = {
            'doc_type': request.GET.get('doc_type', options['doc_type']),
            'lang': request.GET.get('lang', options['lang']),
            'font': request.GET.get('font', options['font']),
            'filename': request.GET.get('filename', options['filename'])
        }
    return options
