.PHONY: help backend frontend

FRONTEND_FOLDER=frontend
BACKEND_FOLDER=backend

FRONTEND_SERVICE=frontend
BACKEND_SERVICE=backend
POSTGRES_SERVICE=postgres
REDIS=redis


help: # This help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


## This is  bad stuff - we want to avoid make: *** No rule to make target `python'.  Stop because we war misusing the args of Makefile

bash:
	@echo ""

python:
	@echo ""

manage.py:
	@echo ""

shell_plus:
	@echo ""

migrate:
	@echo ""

makemigrations:
	@echo ""


##########################
### Database            ###
##########################

drop-db: ## Start the Docker container
	@echo "\033[92mDEPRECATED: Use 'reset-db' instead (it drops + creates).\033[0m"

create-db: ## Start the Docker container
	@echo "\033[92mDEPRECATED: Use 'reset-db' instead (it drops + creates).\033[0m"

install-extension: ## Install Postgresql \"hstore\" extension for HStoreField
	@echo "\033[92m( ͡° ͜ʖ ͡°) Installing Extension  "hstore"...\033[0m"
	docker exec -ti postgres  psql -U postgres -c "CREATE EXTENSION IF NOT EXISTS hstore;"
	@echo "\033[92m( ͡° ͜ʖ ͡°) Extension Installed...\033[0m"

reset-db-docker: ## Resets database, using Backend Docker container
	@echo "\033[92m(1/6) Resetting the database...\033[0m"
	@echo "\033[1;91mThis will clean a new DB for the DATABASE_URL=postgresql://metronom:secret@postgres/metronom \033[0m"
	./devops/bin/reset_db.sh
	@echo ""
	@echo "\033[92m(2/6) Running migrations...\033[0m"
	docker exec -ti $(BACKEND_SERVICE) python manage.py migrate
	@echo ""
	@echo "\033[92m(3/6) Creating base superuser...\033[0m"
	docker exec -ti $(BACKEND_SERVICE) python manage.py create_base_users
	@echo ""
	@echo "\033[92m(4/6) Adding example data...\033[0m"
	docker exec -ti $(BACKEND_SERVICE) python manage.py create_example_data
	@echo "\033[92m(5/6) Updating settings..\033[0m"
	docker exec -ti $(BACKEND_SERVICE) python manage.py update_settings
	@echo "\033[92m(5/6) Reseting cache...\033[0m"
	docker exec -ti $(REDIS) redis-cli FLUSHALL
	docker exec -ti $(REDIS) redis-cli FLUSHDB
	@echo ""
	@echo "\033[92mDone!\033[0m"
	@echo "\033[92mDo not forget to use \x1b[33;01mDATABASE_URL=postgresql://metronom:secret@postgres/metronom\033[92m in your \x1b[33;01mbackend/.env\033[0m"

reset-db: ## Resets the database, running the Backend locally
	@echo "\033[92m(1/6) Resetting the database...\033[0m"
	./devops/bin/reset_db.sh
	@echo ""
	@echo "\033[92m(2/6) Running migrations...\033[0m"
	python manage.py migrate
	@echo ""
	@echo "\033[92m(3/6) Creating base superuser... \033[0m"
	python manage.py create_base_users
	@echo "\033[92m(4/6) Adding example data...\033[0m"
	python manage.py create_example_data
	@echo ""
	@echo "\033[92m(5/6) Updating settings..\033[0m"
	python manage.py update_settings
	@echo "\033[92m(5/6) Reseting cache...\033[0m"
	docker exec -ti $(REDIS) redis-cli FLUSHALL
	docker exec -ti $(REDIS) redis-cli FLUSHDB
	@echo ""
	@echo "\033[92mDone!\033[0m"

prepare-data: ## Processes the data legacy file
	@echo "\033[92m(1/6) Reseting the database...\033[0m"
	./devops/bin/reset_db.sh
	@echo ""
	@echo "\033[92m(2/6) Importing example_data.sql...\033[0m"
	PGPASSWORD=secret psql -h localhost -d metronom -U metronom -p 5433 < example_data.sql
	@echo ""
	@echo "\033[92m(3/6) Running migrations...\033[0m"
	python manage.py migrate
	@echo ""
	@echo "\033[92m(4/6) Creating base superuser...\033[0m"
	python manage.py create_base_users
	@echo "\033[92m(5/6) Creating base data...\033[0m"
	python manage.py create_base_data
	@echo "\033[92m(6/6) Reseting cache...\033[0m"
	docker exec -ti $(REDIS) redis-cli FLUSHALL
	docker exec -ti $(REDIS) redis-cli FLUSHDB
	@echo ""
	@echo "\033[92mDone!\033[0m"

load-data: ## Inject example_data.sql ...
	@echo "\033[92m(1/6) Reseting the database...\033[0m"
	./devops/bin/reset_db.sh
	@echo ""
	@echo "\033[92m(2/6) Importing example_data.sql...\033[0m"
	PGPASSWORD=secret psql -h localhost -d metronom -U metronom -p 5433 < example_data.sql
	@echo ""
	@echo "\033[92m(3/6) Running migrations...\033[0m"
	python manage.py migrate
	@echo ""
	@echo "\033[92m(6/6) Reseting cache...\033[0m"
	docker exec -ti $(REDIS) redis-cli FLUSHALL
	docker exec -ti $(REDIS) redis-cli FLUSHDB
	@echo ""
	@echo "\033[92mDone!\033[0m"

create-dumpdata: ## Processes the data legacy file
	@echo "\033[92m(6/6) Creating new dumpdata file...\033[0m"
	python manage.py dumpdata -e cache_controller -e silk -e backoffice.AvailableSetting -e backoffice.Setting > data.json
	@echo ""
	@echo "\033[92mDone!\033[0m"

reset-legacy-db: ## Resets the legacy Metronom database
	@echo "\033[92m(1/5) Reseting the database...\033[0m"
	@echo "\033[1;91mThis will clean a new DB for the DATABASE_URL=postgresql://metronom:secret@postgres/metronom_legacy \033[0m"
	./devops/bin/reset_legacy_db.sh
	@echo ""
	@echo ""
	@echo "\033[92mDone!\033[0m"
	@echo "\033[92mYou can now use \x1b[33;01mDATABASE_URL=postgresql://metronom:secret@postgres/metronom_legacy\033[92m"

import-legacy-sql: ## Imports the legacy database
	@echo "\033[92m(1/4) Reseting the database...\033[0m"
	@echo "\033[1;91mThis will clean a new DB for the DATABASE_URL=postgresql://metronom:secret@postgres/metronom_legacy \033[0m"
	./devops/bin/reset_db.sh
	@echo ""
	@echo "\033[92m(2/4) Importing legacy.sql...\033[0m"
	PGPASSWORD=secret psql -h localhost -d metronom_legacy -U metronom -p 5433 < legacy.sql
	@echo ""
	@echo "\033[92m(3/4) Creating base superuser...\033[0m"
	python manage.py create_base_users
	@echo ""
	@echo "\033[92m(4/4) Reseting cache...\033[0m"
	docker exec -ti $(REDIS) redis-cli FLUSHALL
	docker exec -ti $(REDIS) redis-cli FLUSHDB
	@echo ""
	@echo "\033[92mDone!\033[0m"

reset-redis-cache: ## Reset Redis cache
	docker exec -ti $(REDIS) redis-cli FLUSHALL
	docker exec -ti $(REDIS) redis-cli FLUSHDB

##############
### DOCKER ###
##############

local: ## Command deprecated
	@echo "\x1b[33;01m\nCommand deprecated. Please use 'make docker'\n \033[0m"

docker-login: ## Login to Docker
	@echo "\033[92mLogin to Docker...\033[0m"
	./devops/bin/docker_login.sh

db: ## Start the Docker container
	docker-compose -f local.yml up --build -d redis postgres

docker: ## Start the Docker container
	@echo "\033[92mStarting an environment for local development...\033[0m"
	docker-compose -f local.yml up --build backend

pdf: ## Start the PDF Docker container
	@echo "\033[92mStarting PDF Docker for local development...\033[0m"
	docker-compose -f pdf-engine/local.yml up --build pdfengine

local-frontend: docker-login ## Start the Docker container
	@echo "\033[92mUpdates the Containers and starts Frontend...\033[0m"
	docker-compose -f local.yml pull
	docker-compose -f local.yml up --build frontend

latest-backend-image: docker-login ## Start the Docker container
	@echo "\033[92mUpdates the Containers and starts Backend from Docker...\033[0m"
	docker-compose -f local.yml pull docker-backend
	docker-compose -f local.yml up --build docker-backend

backend: ## Calls a command for backend Docker
	docker exec -ti $(BACKEND_SERVICE) $(filter-out $@,$(MAKECMDGOALS))

stop:
	docker stop $(APP_NAME)

docker-stop: ## Stop the Docker container
	docker kill $(docker ps -q)

docker-status: ## See the status of the Docker Container
	docker-compose ps

docker-restart:	docker-stop docker-start ## Restart the Docker container

docker-info: ## Get an Docker overview
	@echo "\033[92mCONTAINERS: \033[0m"
	docker ps
	@echo ""
	@echo "\033[92mIMAGES: \033[0m"
	docker images
	@echo ""
	@echo "\033[92mVOLUMES: \033[0m"
	docker volume ls

docker-clean: ## Clean Docker images, containers, volumes
	@echo "\033[92mCleaning unused files in Docker... \033[0m"
	./devops/bin/clean_docker.sh

docker-kill-all: ## Kill Docker containers
	@echo "\033[92mKilling Docker containers... \033[0m"
	./devops/bin/kill_all_docker_containers.sh

docker-delete-all: ## Delete all Docker files, use with caution
	@echo "\033[92mThis will delete all Docker containers, images & volumes on your computer! \033[0m"
	./devops/bin/delete_all_docker.sh

##############
### Admin    ###
##############

db-admin: ## Start the Docker container
	docker-compose -f local.yml up --build -d postgres-admin

redis-admin: ## Start the Docker container
	docker-compose -f local.yml up --build -d redis-admin

sphinx: ## Start the Docker container
	docker-compose -f local.yml up --build -d sphinx


##############
### PIP    ###
##############

pip-compile: ## Creates new pip requirement files, add  --generate-hashes
	@echo "\033[92mCreating new requirement-files from *.in-Files...\033[0m"
	pip-compile --output-file requirements/base.txt requirements/base.in
	pip-compile --output-file requirements/local.txt requirements/local.in
	pip-compile --output-file requirements/production.txt requirements/production.in
	@echo "\033[92mDone!\033[0m"

pip-update: ## Updates the pip requirements, add  --generate-hashes
	@echo "\033[92mUpdating all pip requirements...\033[0m"
	pip-compile -U --output-file requirements/base.txt requirements/base.in
	pip-compile -U --output-file requirements/local.txt requirements/local.in
	pip-compile -U --output-file requirements/production.txt requirements/production.in

pip-install: ## Install the local requirement files
	@echo "Please start local_setup.py for now."
	pip install -r requirements/local.txt


##############
### All    ###
##############

test: ## Run backend tests
	@echo "\033[92mCleaning up before Testing. Searching...\033[0m"
	find apps  -name \*.pyc -o -name \*.pyo -o -name __pycache__
	find apps -name \*.pyc -o -name \*.pyo -o -name __pycache__ -exec rm -r {} \+
	@echo "\033[92mAll Clean now...\033[0m"
	@echo "\033[92mStart Testing...\033[0m"
	docker exec -ti $(BACKEND_SERVICE) py.test $(filter-out $@,$(MAKECMDGOALS))

test-all: ## Run backend tests
	@echo "\033[92mCleaning up before Testing. Searching...\033[0m"
	find apps  -name \*.pyc -o -name \*.pyo -o -name __pycache__
	find apps -name \*.pyc -o -name \*.pyo -o -name __pycache__ -exec rm -r {} \+
	@echo "\033[92mAll Clean now...\033[0m"
	@echo "\033[92mStart Testing...\033[0m"
	docker exec -ti $(BACKEND_SERVICE) py.test -n auto $(filter-out $@,$(MAKECMDGOALS))

test-speed: ## Run backend tests
	@echo "\033[92mCleaning up before Testing. Searching...\033[0m"
	find apps  -name \*.pyc -o -name \*.pyo -o -name __pycache__
	find apps -name \*.pyc -o -name \*.pyo -o -name __pycache__ -exec rm -r {} \+
	@echo "\033[92mAll Clean now...\033[0m"
	@echo "\033[92mStart Testing...\033[0m"
	docker exec -ti $(BACKEND_SERVICE) py.test --durations=20 -n auto  $(filter-out $@,$(MAKECMDGOALS))

setup: setup-backend ## Run all setups

#deploy: ## Deploy to Heroku
#	@echo "\x1b[31;01mNot implemented yet!\033[0m"
#
#install: ## Local installation for developers
#	@echo "Please start local_setup.py for now."

debug: ## Prints debug informations
	@echo "\033[92mCollecting debug information...\033[0m"
	./devops/bin/debug.sh
	@echo "\033[92mWill copy content to clipboard (only on Mac)...\033[0m"
	pbcopy < debug.txt

tmate: ## Prints debug informations
	@echo "\033[92mSlack username to receive the link (e.g. jens)?\033[0m"
	./devops/bin/tmate.sh


copy: ## Copy the code
	@echo "\033[92mCopying the code to the given folder\033[0m"
	mkdir ../copy
	rsync -r --verbose --exclude '.git' --exclude '.venv' --exclude '.pyc' --exclude '__pycache__' --exclude '.cache'  -R . ../copy/


###################
### Internal    ###
###################

colors:
	@echo "\033[92mGreen!\033[0m"
	@echo "\x1b[33;01mYellow!\033[0m"
	@echo "\x1b[31;01mRed!\033[0m"

dirs:
	@echo $prog

#release: ## Release the given version
#ifndef version
#	$(error Please supply a version)
#endif
#	@echo Releasing version $(version)
#ifeq (,$(findstring $(version),$(shell git log --oneline -1)))
#	$(error Last commit does not match version)
#endif
#	git tag $(version)
#	git push
#	git push --tags
#	python setup.py sdist bdist_wheel upload
