# Metronom Pro - `Backend`

## External links

* [staging.metronom-dev.ch](http://staging.metronom-dev.ch) - Staging page
* [metronom-dev.ch](http://www.metronom-dev.ch) - Production page
* [Jenkins](http://jenkins.metronom-dev.ch) - CI testing
* [Github](https://github.com/jensneuhaus/metronom-pro) - Git Respository & Issues
* [Slack](https://einhornmanufaktur.slack.com) - Team communication & important notifications
* [Sentry](https://sentry.io/einhornmanufaktur/metronom-pro-backend) - Error monitoring


## Get started

### Start the Backend Docker

```
$ make docker

```

It will start the following services:
* [localhost:8009](http://localhost:8009/) - `Django backend` with the [Admin](http://localhost:8009/admin) and [Swagger API Docs](http://localhost:8009/docs/)
* Postgres (can be accessed via localhost:5432)
* Redis (can be accessed via localhost:6379)

### Pull the Frontend Docker

It will download the latest Frontend Docker image

```
$ make local-frontend

```

It will provide:
* [localhost:3005](http://localhost:3005/) - `React` Frontend

### Start developing in Backend

Start the database in the background
```
$ make db
```

Prepare the Python environment:

```
$ python3 -m venv .venv
$ source .venv/bin/activate
$ cp .env.example .env
$ pip install -r requirements/local.txt
```

### Important commands

* `make test` - It will run the backend tests (in the Docker container)
* `make backend "command""` - It will run a command in the Backend container
* `make reset-db` - Initializes or resets the Postgres Database ((it will create a user `metronom` and a add a database `metronom`)
* `make reset-redis-cache` - Flushes the Redis cache
* `make docker-delete-all` - Deletes all Docker images & containers on your computer
* `make db-admin` - Starts Postgres Admin (http://localhost:8007/)
* `make redis-admin` - Starts Postgres Admin (http://localhost:8019/)
* `make sphinx` - Starts Sphinx documentation (http://localhost:8007/)
* `make debug` - Collects debug information for help from other developers
* `make pdf` - Starts PDF Engine (http://localhost:8200/)

### Getting help

* Check the auto-generated [Redoc API Docs](http://localhost:8009/redoc/)
* Check the auto-generated [Swagger API Docs](http://localhost:8009/docs/)
* Use the auto-generated [Postman collection](http://localhost:8009/docs/?format=openapi) for Import in [Postman](https://www.getpostman.com)
* Slack: Ask something :)




## Details About Docker Image of PDF Engine

The PDF Engine is based on 3 main frameworks/libraries:
  - Django==2.0.3
  - gunicorn==19.7.1
  - WeasyPrint==0.36

### Start the PDF Engine Docker

```
$ make pdf

```

It will provide:
* [localhost:8200](http://localhost:8200/) - `PDF Engine`



### Docs:

The Entire Engine is based on Django Framework and 


The flowing endpoints are available:

* [localhost:8200/](http://localhost:8200/) - `POST` requests taking a `data as Django View context` and returning the PDF

* [localhost:8200/test/pdf/](http://localhost:8200/test/pdf/) - `GET` PDFTester => Only for Test purpose with static data as `context`

* [localhost:8200/test/pdf/?lang=de-ch&doc_type=project&font=Proxima&filename=My%20Lovely%20PDF](http://localhost:8200/test/pdf/?lang=de-ch&doc_type=project&font=Proxima&filename=My%20Lovely%20PDF) - `GET` PDFTester => For Test purpose as previous but with all available `GET parameters`:
  - lang = [en, de, de-ch] #default=en
  - font = [Proxima, Lato, Roboto, Saira_Semi_Condensed] # default=Proxima
  - doc_type = [project,campaign] #default=project
  - filename = Sample Name Document #default="Sample Title" 


* [localhost:8200/test/html/](http://localhost:8200/test/html/) - `GET` HTMLTester => Only for Test purpose with static data as `context` returning the HTML rendered before being converted into PDF (helping to speed up development for the HTML with Gulp and BrowserSync)

* [localhost:8200/docs/](http://localhost:8200/docs/) - `GET` short documentation similar like this one

