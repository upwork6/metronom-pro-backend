Commands
========

This page lists the custom management commands available to the user

Available Commands
------------------

Following commands are available in the application.

* clear_cache
* create_example_data
* create_lots_of_data
* import_bank_xslx

Command import_bank_xslx
------------------------

The import_bank_xslx commands expects two required arguments and one optional argument:

Arguments
---------
::

    country = DE | CH

Country is a required argument and you can provide either 'DE' or 'CH' as the value::

    source_file = /path/to/file.xlsx

Source file is a required argument which takes the path to excel file relative to the manage.py script.
I have added xlsx files i am importing to the backend/apps/data folder which is added to .gitignore.::

    Optional: --delete

'--delete' is an optional argument which defaults to False if not provided. When you provide '--delete' it will delete all
previous Bank records before importing the excel file

Command Examples
----------------
::

    python manage.py convert_csv_to_xslx DE ./data/DE.xlsx
    python manage.py convert_csv_to_xslx DE ./data/DE.xlsx --delete
    python manage.py convert_csv_to_xslx CB ./data/CH.xlsx
    python manage.py convert_csv_to_xslx CB ./data/CH.xlsx --delete

Download Links
--------------
* DE - https://www.bundesbank.de/Redaktion/DE/Standardartikel/Aufgaben/Unbarer_Zahlungsverkehr/bankleitzahlen_download.html
* CH - https://www.six-interbank-clearing.com/dam/downloads/bc-bank-master/bcbankenstamm_d.xls
