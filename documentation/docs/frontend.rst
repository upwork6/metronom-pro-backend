Frontend
====================

We are using React as a Frontend client.

Used packages (name(link)[license])
-----

**Visual:**
 - `antd <https://github.com/ant-design/ant-design>`_ [MIT]
 - `helmet <https://github.com/helmetjs/helmet>`_ [MIT] - headers handling
 - `normalize.css <https://github.com/necolas/normalize.css>`_ [MIT]
 - `react <https://github.com/facebook/react>`_ [MIT]
 - // TODO list all the react packages

**State management:**
 - `redux <https://github.com/reactjs/redux>`_ [MIT]
 - `redux-pack <https://github.com/lelandrichardson/redux-pack>`_ [MIT]
 - `redux-thunk <https://github.com/gaearon/redux-thunk>`_ [MIT]
 - `recompose <https://github.com/acdlite/recompose>`_ [MIT]
 - `history <https://github.com/browserstate/history.js/>`_ [New BSD]

**Translations:**
 - `i18n <https://github.com/i18next/i18next>`_ [MIT]

**APIs:**
 - `jsonwebtoken <https://github.com/auth0/node-jsonwebtoken>`_ [MIT]
 - `aws-sdk <https://github.com/aws/aws-sdk-js>`_ [Apache License 2.0]
 - `axios <https://github.com/axios/axios>`_ [MIT]
 - `socket.io <https://github.com/socketio/socket.io>`_ [MIT]

**Cookies:**
 - `universal-cookie / universal-cookie-express <https://github.com/reactivestack/cookies>`_ [MIT]

**Node Server:**
 - `body-parser <https://github.com/expressjs/body-parser>`_ [MIT]
 - `cors <https://github.com/expressjs/cors>`_ [MIT]
 - `express <https://github.com/expressjs/express>`_ [MIT]

**Utilities:**
 - `lodash <https://github.com/lodash/lodash>`_ [`license <https://github.com/lodash/lodash/blob/master/LICENSE>`_]
 - `modernizr <https://github.com/Modernizr/Modernizr>`_ [MIT]
 - `moment <https://github.com/moment/moment>`_ [MIT]

**Logging/ Debugging**
 - `colors <https://github.com/marak/colors.js/>`_ [MIT]
 - `nodemon <https://github.com/remy/nodemon>`_ [MIT]
 - `redux-logger <https://github.com/evgenyrodionov/redux-logger>`_ [MIT]

**Devs:**
 - TODO

Layout
-----


Testing concept
-----


State management
-----


Localisation
-----


API Handling
-----


socket.io
-----
