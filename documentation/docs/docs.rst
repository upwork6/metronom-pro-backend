================
1. Folder structure
================
Main project folder structure is taken diectly from `React Universally <https://github.com/ctrlplusb/react-universally>`_ boilerplate, that was used in the beginning of the project.

- `_test_config_` -- contains configuration files for `jest`
- `client` -- contains configuration related to browser rendering of react. ServiceWorker are included but not yet used (8.01.18)
   - `components` - contains code for hot reloading (development purpose)
   - `integrations` - contains code for integrations with Opbeat and Sentry
   - `polyfills` - contains code for polyfills (mainly HTML canvas)
- `config` -- contains code related to EnvVars used across the project as well as code for JWT token verification method.
- `docker` -- docker configuration for the frontend part of the project
- `internal` -- least changed folder. Most content comes from React Universally. Includes Webpack configuration factory, that is prepared to handle SCSS loading (instead of CSS modules originally used in the mentioned boilerplate)
- `locales` -- JSON files used to provide translations in the projects (EN, DE)
- `public` -- public files, with `favicons`, `manifest.json` and `updated.html` page used during the project's updates
- `server` -- contains code related to server side rendering
   - `middleware` -- Files related directly to SSR of React. Most of the code is commented and should be clear to read.
   - `routes` -- custom routes that handle AWS S3 (uploading and deleting profile images) and SocketIO
- `shared` -- all the code related to the app itself (its components, actions, reducers etc.)
   - `actions` -- all the actions (redux related)
   - `components` -- proper components that are being displayed on the screen. Most of them is written with the help of `Recompose`, yet not all of them are transformed to pure components.
      - The CRM related components contains
         `List` -- handles listing the records
         `Add` -- handles adding new record
         `Details` -- handles displaying the record's details
         `Form` -- contains code used mostly in the `Add` component
      - The components are loaded using `react-async-components` so every final folder contains
         `index.js` -- handles asynchronous loading
         `Route.js` -- handles the component itself (sometimes the naming is different, but it contains the `Route`)
   - `icons` -- all the icons used across the project
   - `middlewares` -- custom middlewares used in Redux (one of them is based on `redux-pack` -- improved by wide response config handling)
   - `reducers` -- all the reducers (redux related)
   - `services` -- contains code related to API handling (`axios` as main package)
   - `styles` -- SCSS files used for styling
   - `utils` -- custom methods used across the project, with most of them in `extensions.js` (as extensions to `lodash`)

===============
2. React rendering
===============

The frontend part of the Metronom-Pro was built to handle both server (SSR) and client side rendering, so it may be considered in the JS world as a universal application.

Higher Order Components (HOCs) that are used during the initial render includes:

- `ReactHotLoader` -- only for the development
- `AsyncComponentProvider` -- to provide asynchronous loading of the components
- `I18nextProvider` -- to handle the translations using `i18next` package
- `Provider` -- redux related
- `OpbeatRouter` -- this is the `Router` from `react-router` (v4) wrapped in Opbeat's own method
- `LocaleProvider` -- provides translations for `AntUI` components
- `CookiesProvider` -- provides access to cookies

When it comes to SSR (`/server/`) we use methods provided by `i18next` to wrap the whole React part. All the React related SSR code is placed in `/server/reactApplication` with `ServerHTML.js` file handling:

- Injections of code directly into HTML (like language config)
- Script loading (GoogleMaps, PolyfillIO)

Application uses Content Security Policy (CSP) configuration, that is configurable inside `/config/values.js` and is being used inside `/server/middleware/security.js`

=====================
3. Authentication
=====================

The JSONWebToken (JWT) is being verified on every page load (during server side rendering of the application) and if the user is not authenticated, he/she is redirected to `/login` page.

The `Bearer :JWT` (where :JWT is the actual token) is injected into `axios` APIs methods directly ofter login, what can be seen in `shared/App/actions/authentication.js`.

=====================
4. Development/ linting
=====================

The code was written using `airbnb` configuration, what can be seen `.eslintrc`. Small adjustments were made to the config itself to provide more enjoyable coding environment at the initial stages of the development (e.g. `Prop-Types` are disabled, yet they should be present in the final app).

`ES6 stage-3` babel presets are used in `.babelrc`

To shorten the name of the import Webpack aliases were used (with config places in `internal/webpack/resolve.js`. To handle the aliases in the code editor `babel-module-resover` was used.

As previously stated, almost every single component used inside the project was written using pure components concept with the use of `recompose` package. There is no visible separation of presentational and container components (#naming) yet the ideology was present during the development process.

======================
5. Profile image handling
======================

EnvVars (`AWSS3Region`, `AWSS3BucketName`, `AWSS3ProfileImagePath`) are used to configure the AWS S3 handling.

`getProfileImageInfo` method (that is an extension of `_` lodash) handles generation of the profile image path. Most of it is related to `AWSS3ProfileImagePath` that should be structure in the following way -- `your/path/to/aws/USER_HASH/FILE_NAME`. Both `USER_HASH` and `FILE_NAME` will be replaced be the automatically generated hash/ filename inside the mentined method.

Two images are uploaded to the server - `cropped` and `original`. After successful upload, a `PATCH` request is made to `/me/profile/` to update the image information (`x_coordinate`, `y_coordinate`, `zoom`). This data is needed in case user wants to edit his cropped photo (so the system knows how the image was previously cropped).

Cropping itself is done using `react-avatar-editor`. Whole code for ProfileImage handling can be seen in `shared/App/components/Profile/Details/Details/ProfileImage.js`.

In case of user without profile image, `profilePlaceholder.png` is used as a placeholder.

======================
6. Table and filtering
======================

`AntUI` tables were used as the base components for this purpose, yet it was not enough.

`TableWithFilters` component handles custom filtering inside the table that was designed to matched the one visible on Github.

**Props:**

- `tableName` -- name of the table (used to store saved searches in the local storage)
- `data` -- array of the records that should be displayed in the table
- `loading` -- loading status of the records
- `error` -- error status of the records
- `tableClassName` -- className of the table, so it can be styled)
- `recordsShownAtOnce` -- number of records that will be displayed at once (if the user wants more there is a `Load More` button
- `tableConfig` -- table configuration (described in details below)
- `disabledColumns` -- `key`s of the disabled columns (specifed in `tableConfig`; default to [])
- `filterbarDisabled` -- boolean that indicates if filterbar should be disabled (default to false)
- `rowSelection` -- method used in AntUI table
- `defaultKeyToFilter` - default record's attribute name, that should be used to filter the records without writing the specified filter structure (described below; default to `full_name`)

**tableConfig:**

The concept is to put it in a separate file, that will contain all the table configuration. It should return a method returning an object containing configuration of the followin:

- columns -- columns configuration as can be seen in the official AntUI documentation
- nameToSelectInput -- configuration object that handles displaying tags inside the filter bar `Select` component. It is a mapping between the filter name used inside the Select component, and the record's attribute name - example:
   - { full_name: 'name', city: 'city' } -- `name:John` will be used to search for record with attribute `full_name` that includes `john` (same with the city)
   - Special configuration is possible for `is:` filter structure, so if we want to filter using record's `category` attribute we should pass and object with the following keys:
      - mapping -- function that returns string value based on used list. Example can be seen in `/shared/App/components/Contacts/tableConfig.js`
      - prefix -- should be string that will be used to search (like `is` so we can search `is:company`
      - allowedValues -- an array of specially structured strings ('allowedValue:acceptedValue`)
      - basedOn -- configuration object that decide how to decide if a record is the wanted type (example in the same file as stated above)
         - name -- attribute name of the record
         - value -- array/string of accepted values
- availableSorters -- configuration file for the available sorters.

The best way to understand the process is to look into `Contacts/tableConfig.js` file next to `Contacts/Table.js`.
