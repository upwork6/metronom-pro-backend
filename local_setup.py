import os
import random
import subprocess
import sys


def create_env_file():
    """Create an .env file - if one is not existing already."""
    def generate_secret_key(count=50):
        return ''.join([random.SystemRandom().choice('abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)') for i in range(count)])

    env_file_name = ".env"
    env_path = os.path.join(os.getcwd(), env_file_name)

    try:
        with open(env_path, 'x') as env_file:
            print("The .env file was created: {}".format(env_path))

            env_file.write("DEBUG=True\n")
            env_file.write("STAGE=local\n")
            env_file.write("ALLOWED_HOSTS='*'\n")
            env_file.write("DATABASE_URL=postgres://postgres@postgres/metronom-backend-service\n")
            env_file.write("REDIS_URL=redis://redis:6379\n")
            env_file.write("BASE_SUPERUSERS=[['admin@metronom-dev.ch', 'test1234', 'admin', 'John', 'Doe']]\n")
            env_file.write("SECRET_KEY='{}'\n".format(generate_secret_key(50)))
            env_file.write("JWT_SECRET=notsafeforproduction\n")

    except FileExistsError:
        print(".env file already exists. Skipping .env creation")


def main(argv):

    try:
        create_env_file()
    except subprocess.CalledProcessError as e:
        print("Command: {}\nReturn code: {}\n{}\n{}".format(e.cmd,
                                                            e.returncode,
                                                            e.output,
                                                            e.stderr))

    # Everything worked!
    print("")
    print("Done!")
    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
