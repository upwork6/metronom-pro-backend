
from django.test import TestCase

from apps.articles.tests.factories import ArticleGroupFactory, TemplateArticleFactory


class TestTemplateArticle(TestCase):

    def test_position_change_depending_on_article_group_template_article(self):
        group1 = ArticleGroupFactory()
        group2 = ArticleGroupFactory()
        group1_act1 = TemplateArticleFactory(article_group=group1)
        group1_act2 = TemplateArticleFactory(article_group=group1)
        group1_act3 = TemplateArticleFactory(article_group=group1)
        group2_act1 = TemplateArticleFactory(article_group=group2)
        group2_act2 = TemplateArticleFactory(article_group=group2)
        assert group1_act1.position == 1
        assert group1_act2.position == 2
        assert group1_act3.position == 3
        assert group2_act1.position == 1
        assert group2_act2.position == 2
