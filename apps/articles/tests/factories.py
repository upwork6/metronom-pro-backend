import factory

from apps.accounting.tests.factories import AllocationFactory
from apps.articles.models import ArticleGroup, TemplateArticle


class ArticleGroupFactory(factory.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'ArticleGroup-{0}'.format(n))

    class Meta:
        model = ArticleGroup
        django_get_or_create = ('name',)


class TemplateArticleFactory(factory.DjangoModelFactory):
    article_group = factory.SubFactory(ArticleGroupFactory)
    allocation = factory.SubFactory(AllocationFactory)
    name = factory.Sequence(lambda n: 'name-%s' % n)
    description = factory.Faker('sentence')

    class Meta:
        model = TemplateArticle
