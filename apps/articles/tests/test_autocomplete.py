
import json

from django.core.cache import caches
from django.urls import reverse
from rest_framework import status

from apps.articles.tests.factories import (ArticleGroupFactory,
                                           TemplateArticleFactory)
from apps.metronom_commons.test import MetronomBaseAPITestCase

cache = caches['autocompletes']


class TestArticleGroupsAutocomplete(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.article_group1 = ArticleGroupFactory()
        cls.article_group2 = ArticleGroupFactory()
        cls.article1 = TemplateArticleFactory(article_group=cls.article_group1)
        cls.article2 = TemplateArticleFactory(article_group=cls.article_group2)
        cls.article3 = TemplateArticleFactory(article_group=cls.article_group2)
        cls.united_url = reverse('autocompletes:united')
        cache.clear()
        cls.response = cls.user_client.get(cls.united_url)
        cls.response_body = json.loads(cls.response.content)['agencies:article_groups']

    def test_response(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_article_group1(self):
        self.assertEqual(self.article_group1.name, self.response_body.get(str(self.article_group1.id)))

    def test_article_group2(self):
        self.assertEqual(self.article_group2.name, self.response_body.get(str(self.article_group2.id)))


class TestArticlesAutocomplete(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.article_group1 = ArticleGroupFactory()
        cls.article_group2 = ArticleGroupFactory()
        cls.article1 = TemplateArticleFactory(article_group=cls.article_group1)
        cls.article2 = TemplateArticleFactory(article_group=cls.article_group2)
        cls.article3 = TemplateArticleFactory(article_group=cls.article_group2)
        cls.united_url = reverse('autocompletes:united')
        cache.clear()
        cls.response = cls.user_client.get(cls.united_url)
        cls.response_body = json.loads(cls.response.content)['agencies:articles']

    def test_response(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def _compare_data(self, article_group, data):
        instances = article_group.templatearticle_set.order_by('id')
        dicts = [{str(x.id): x.name} for x in instances]
        for to_check in dicts:
            self.assertIn(to_check, data)

    def test_article_group1_articles(self):
        data = self.response_body.get(str(self.article_group1.id))
        self._compare_data(self.article_group1, data)

    def test_article_group2_articles(self):
        data = self.response_body.get(str(self.article_group2.id))
        self._compare_data(self.article_group2, data)
