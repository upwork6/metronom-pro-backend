import uuid

import pytest
from django.urls import reverse
from rest_framework import status

from apps.accounting.models import Allocation
from apps.accounting.tests.factories import AllocationFactory
from apps.articles.models import ArticleGroup, TemplateArticle
from apps.articles.tests.factories import (ArticleGroupFactory,
                                           TemplateArticleFactory)
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.tests.factories import ProjectArticleFactory, ProjectFactory


class BaseArticleGroup(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.article_group1 = ArticleGroupFactory()
        cls.article_group2 = ArticleGroupFactory()
        cls.list_url = reverse('api:articlegroup-list')
        cls.retrieve_url = reverse('api:articlegroup-detail', args=[cls.article_group1.id])


class TestArticleGroupList(BaseArticleGroup):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.response = cls.admin_client.get(cls.list_url)

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_response_contains_uuids(self):
        self.assertEqual(len(self.response.data), 2)
        uuids = [x['uuid'] for x in self.response.data]
        self.assertIn(str(self.article_group1.id), uuids)
        self.assertIn(str(self.article_group2.id), uuids)

    def test_response_contains_names(self):
        names = [x['name'] for x in self.response.data]
        self.assertIn(self.article_group1.name, names)
        self.assertIn(self.article_group2.name, names)

    def test_permissions(self):
        response = self.user_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)


class TestArticleGroupCreate(BaseArticleGroup):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.post_data = {
            'name': "Hosting"
        }
        cls.response = cls.admin_client.post(cls.list_url, cls.post_data, format="json")
        cls.new_article_group = ArticleGroup.objects.exclude(
            id__in=[cls.article_group1.id, cls.article_group2.id]
        ).last()

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_name(self):
        self.assertEqual(self.post_data['name'], self.new_article_group.name)

    def test_create_not_unique_name(self):
        self.response = self.admin_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(self.response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertErrorResponse(
            self.response,
            {'name': ['ArticleGroup with this name already exists.']}
        )

    def test_create_empty_name(self):
        self.response = self.admin_client.post(self.list_url, {'name': ''}, format="json")
        self.assertErrorResponse(
            self.response,
            {'name': ['This field may not be blank.']}
        )

    def test_permissions(self):
        post_data = {
            'name': "Not Hosting"
        }
        response = self.user_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        post_data = {
            'name': "Not Hosting2"
        }
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, 201)


class TestArticleGroupRetrieve(BaseArticleGroup):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.response = cls.admin_client.get(cls.retrieve_url)

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_response_contains_uuids(self):
        self.assertEqual(str(self.article_group1.id), self.response.data['uuid'])

    def test_response_contains_names(self):
        self.assertEqual(str(self.article_group1.name), self.response.data['name'])

    def test_permissions(self):
        response = self.user_client.get(self.retrieve_url)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.retrieve_url)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.retrieve_url)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.retrieve_url)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.retrieve_url)
        self.assertEqual(response.status_code, 200)


class TestArticleGroupUpdate(BaseArticleGroup):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        patch_data = {
            'name': "New hosting"
        }
        cls.response = cls.admin_client.patch(cls.retrieve_url, patch_data)
        cls.article_group1.refresh_from_db()

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_response_contains_uuids(self):
        self.assertEqual(str(self.article_group1.id), self.response.data['uuid'])

    def test_response_contains_names(self):
        self.assertEqual(str(self.article_group1.name), self.response.data['name'])

    def test_no_duplication_name(self):
        patch_data = {
            'name': self.article_group2.name
        }
        response = self.admin_client.patch(self.retrieve_url, patch_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertErrorResponse(
            response,
            {'name': ['ArticleGroup with this name already exists.']}
        )

    def test_no_empty_name(self):
        patch_data = {
            'name': ""
        }
        response = self.admin_client.patch(self.retrieve_url, patch_data)
        self.assertErrorResponse(
            response,
            {'__all__': ['You should send either name or uuid for the ArticleGroup']}
        )

    def test_permissions_article_group_update_not_used(self):
        patch_data = {
            'name': "Not Hosting"
        }
        response = self.user_client.patch(self.retrieve_url, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.retrieve_url, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.retrieve_url, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.retrieve_url, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        patch_data = {
            'name': "Not Hosting2"
        }
        response = self.admin_client.patch(self.retrieve_url, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_article_group_update_used(self):
        article1 = TemplateArticleFactory(article_group=self.article_group1)
        patch_data = {
            'name': "Not Hosting"
        }
        response = self.backoffice_client.patch(self.retrieve_url, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        patch_data = {
            'name': "Not Hosting2"
        }
        response = self.admin_client.patch(self.retrieve_url, patch_data, format="json")
        self.assertEqual(response.status_code, 200)


class TestArticleGroupDelete(BaseArticleGroup):

    def setUp(self):
        super().setUp()
        article_group = ArticleGroupFactory()
        self.endpoint = reverse('api:articlegroup-detail', args=[article_group.id])

    def test_response_status_code(self):
        before = ArticleGroup.objects.count()
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(before - 1, ArticleGroup.objects.count())

    def test_permissions_article_group_delete_not_used(self):
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)
        # we need to create new one
        article_group = ArticleGroupFactory()
        endpoint = reverse('api:articlegroup-detail', args=[article_group.id])
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_article_group_delete_used(self):
        article_group = ArticleGroupFactory()
        article1 = TemplateArticleFactory(article_group=article_group)
        endpoint = reverse('api:articlegroup-detail', args=[article_group.id])
        response = self.user_client.delete(endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used ArticleGroup"]
            }
        )


class BaseArticle(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.article1 = TemplateArticleFactory()
        cls.article2 = TemplateArticleFactory()
        cls.list_url = reverse('api:templatearticle-list')
        cls.retrieve_url = reverse('api:templatearticle-detail', args=[cls.article1.id])


class TestArticleList(BaseArticle):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.response = cls.admin_client.get(cls.list_url)

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_response_length(self):
        self.assertEqual(len(self.response.data), 2)

    def test_response_names(self):
        names = [x['name'] for x in self.response.data]
        self.assertIn(self.article1.name, names)
        self.assertIn(self.article2.name, names)

    def test_response_ids(self):
        uuids = [x['uuid'] for x in self.response.data]
        self.assertIn(str(self.article1.id), uuids)
        self.assertIn(str(self.article2.id), uuids)

    def test_response_description(self):
        descriptions = [x['description'] for x in self.response.data]
        self.assertIn(self.article1.description, descriptions)
        self.assertIn(self.article2.description, descriptions)

    def test_response_article_groups(self):
        groups = [
            {x['article_group']['uuid']: x['article_group']['name']} for x in self.response.data
        ]
        for article in [self.article1, self.article2]:
            data = {str(article.article_group.id): article.article_group.name}
            self.assertIn(data, groups)

    def test_response_allocations(self):
        allocations = [
            {x['allocation']['uuid']: x['allocation']['name']} for x in self.response.data
        ]
        for article in [self.article1, self.article2]:
            data = {str(article.allocation.id): article.allocation.name}
            self.assertIn(data, allocations)

    def test_permissions(self):
        response = self.user_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)


class TestArticleCreate(BaseArticle):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.allocation = AllocationFactory()
        cls.article_group = ArticleGroupFactory()
        cls.post_data = {
            'name': 'New article',
            'description': 'Big description',
            'article_group_uuid': str(cls.article_group.id),
            'allocation_uuid': str(cls.allocation.id)
        }
        cls.response = cls.admin_client.post(cls.list_url, cls.post_data, format="json")
        cls.new_article = TemplateArticle.objects.exclude(
            id__in=[cls.article1.id, cls.article2.id]
        ).last()

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_name(self):
        self.assertEqual(self.new_article.name, self.post_data['name'])

    def test_description(self):
        self.assertEqual(self.new_article.description, self.post_data['description'])

    def test_article_group(self):
        self.assertEqual(self.new_article.article_group.name, self.article_group.name)

    def test_allocation(self):
        self.assertEqual(self.new_article.allocation.name, self.allocation.name)

    def test_create_with_wrong_article_group_uuid(self):
        wrong_uuid = uuid.uuid4()
        post_data = {
            'name': 'New article2',
            'description': 'Big description2',
            'article_group_uuid': wrong_uuid,
            'allocation_uuid': str(self.allocation.id)
        }
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertErrorResponse(
            response,
            {'article_group_uuid': [f'Invalid pk "{wrong_uuid}" - object does not exist.']}
        )

    def test_create_with_wrong_allocation_uuid(self):
        wrong_uuid = uuid.uuid4()
        post_data = {
            'name': 'New article2',
            'description': 'Big description2',
            'article_group_uuid': str(self.article_group.id),
            'allocation_uuid': wrong_uuid
        }
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertErrorResponse(
            response,
            {'allocation_uuid': [f'Invalid pk "{wrong_uuid}" - object does not exist.']}
        )

    def test_permissions(self):
        response = self.user_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)
        response = self.admin_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)


class TestArticleRetrieve(BaseArticle):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.response = cls.admin_client.get(cls.retrieve_url)

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_name(self):
        self.assertEqual(self.response.data['name'], self.article1.name)

    def test_description(self):
        self.assertEqual(self.response.data['description'], self.article1.description)

    def test_allocation(self):
        self.assertEqual(self.response.data['allocation']['name'], self.article1.allocation.name)

    def test_article_group(self):
        self.assertEqual(self.response.data['article_group']['name'], self.article1.article_group.name)

    def test_permissions(self):
        response = self.user_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)


class TestArticleUpdate(BaseArticle):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.article3 = TemplateArticleFactory()
        cls.allocation = AllocationFactory()
        cls.article_group = ArticleGroupFactory()
        cls.patch_data = {
            'name': 'New article2',
            'description': 'Big description2',
            'article_group_uuid': str(cls.article_group.id),
            'allocation_uuid': str(cls.allocation.id)
        }
        cls.retrieve_url = reverse('api:templatearticle-detail', args=[cls.article3.id])
        cls.response = cls.admin_client.patch(cls.retrieve_url, cls.patch_data, format="json")
        cls.article3.refresh_from_db()

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_name(self):
        self.assertEqual(self.response.data['name'], self.article3.name)

    def test_description(self):
        self.assertEqual(self.response.data['description'], self.article3.description)

    def test_allocation(self):
        self.assertEqual(self.response.data['allocation']['name'], self.article3.allocation.name)

    def test_article_group(self):
        self.assertEqual(self.response.data['article_group']['name'], self.article3.article_group.name)

    def test_update_with_wrong_article_group_uuid(self):
        wrong_uuid = uuid.uuid4()
        patch_data = {
            'article_group_uuid': wrong_uuid,
        }
        response = self.admin_client.patch(self.retrieve_url, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {'article_group_uuid': [f'Invalid pk "{wrong_uuid}" - object does not exist.']}
        )

    def test_update_with_wrong_allocation_uuid(self):
        wrong_uuid = uuid.uuid4()
        patch_data = {
            'allocation_uuid': wrong_uuid,
        }
        response = self.admin_client.patch(self.retrieve_url, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {'allocation_uuid': [f'Invalid pk "{wrong_uuid}" - object does not exist.']}
        )

    def test_permissions_template_article_update_not_used(self):
        response = self.user_client.patch(self.retrieve_url, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.retrieve_url, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.retrieve_url, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.retrieve_url, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.retrieve_url, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_template_article_update_used(self):
        project = ProjectFactory(billing_currency="CHF")
        project_article = ProjectArticleFactory(used_template_article=self.article3, project=project)
        response = self.backoffice_client.patch(self.retrieve_url, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.retrieve_url, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)


class TestArticleDelete(BaseArticle):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.login_as_admin()

    def setUp(self):
        self.article3 = TemplateArticleFactory()
        self.endpoint = reverse('api:templatearticle-detail', args=[self.article3.id])

    def test_response(self):
        before = TemplateArticle.objects.count()
        self.response = self.admin_client.delete(self.endpoint)
        self.assertEqual(self.response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(before - 1, TemplateArticle.objects.count())

    def test_permissions_template_article_delete_if_not_used(self):
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)
        article = TemplateArticleFactory()
        endpoint = reverse('api:templatearticle-detail', args=[article.id])
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_template_article_delete_if_used(self):
        article = TemplateArticleFactory()
        project = ProjectFactory(billing_currency="CHF")
        ProjectArticleFactory(used_template_article=article, project=project)
        endpoint = reverse('api:templatearticle-detail', args=[article.id])
        response = self.user_client.delete(endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used TemplateArticle"]
            }
        )
