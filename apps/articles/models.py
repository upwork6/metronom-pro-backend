
from django.db import models
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

from apps.accounting.models import Allocation
from apps.metronom_commons.models import (OrderingMixin,
                                          OrderingMixinWithoutSignal,
                                          UserAccessBackofficeMetronomBaseModel)


class ArticleGroup(UserAccessBackofficeMetronomBaseModel, OrderingMixin):
    """ General category for the Articles

    """
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = _('Article Group')
        verbose_name_plural = _('Article Groups')


class TemplateArticle(UserAccessBackofficeMetronomBaseModel, OrderingMixinWithoutSignal):
    """ Articles are analog to activities (#81). Articles are products, activities are things to do.

    """
    article_group = models.ForeignKey(ArticleGroup, on_delete=models.PROTECT)
    name = models.CharField(max_length=200)
    description = models.TextField()
    allocation = models.ForeignKey(Allocation)

    def __str__(self):
        return f'{self.article_group.name}: {self.name}'

    @staticmethod
    def recalculate_position(sender, instance, created, *args, **kwargs):
        if created:
            position = 0
            qs = sender.objects.exclude(id=instance.id).filter(article_group=instance.article_group)
            # in some cases we allow to create objects with pre-defined positions
            if not instance.position or qs.filter(position=instance.position).exists():
                last_instance = qs.order_by('-position').first()
                if last_instance:
                    position = last_instance.position
                instance.position = position + 1
                instance.save(update_fields=['position'])

    class Meta:
        verbose_name = _('Template Article')
        verbose_name_plural = _('Template Articles')


post_save.connect(sender=TemplateArticle, receiver=TemplateArticle.recalculate_position,
                  dispatch_uid="template_article_recalculate_position_signal")
