
from dry_rest_permissions.generics import DRYPermissions
from rest_framework.viewsets import ModelViewSet

from apps.api.filters import MetronomPermissionsFilter
from apps.articles.models import ArticleGroup, TemplateArticle
from apps.articles.serializers import (ArticleGroupSerializer,
                                       TemplateArticleSerializer)
from apps.metronom_commons.mixins.views import DeleteForbiddenIfUsedMixin
from apps.projects.models import ProjectArticle


class ArticleGroupViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    """Article groups are pooling Articles"""
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    queryset = ArticleGroup.objects.order_by('position')
    serializer_class = ArticleGroupSerializer
    forbid_delete_if_used_in = {
        TemplateArticle: ['article_group']
    }


class TemplateArticleViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    """Article used in a Project"""
    queryset = TemplateArticle.objects.order_by('position')
    serializer_class = TemplateArticleSerializer
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    forbid_delete_if_used_in = {
        ProjectArticle: ['used_template_article']
    }
