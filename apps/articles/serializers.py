from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from apps.accounting.models import Allocation
from apps.articles.models import ArticleGroup, TemplateArticle
from apps.projects.models import ProjectArticle


class ArticleAllocationSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)
    name = serializers.CharField(required=False)

    class Meta:
        model = Allocation
        fields = ['uuid', 'name', 'category', 'public_id']


class ArticleGroupSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)
    name = serializers.CharField(required=False)

    class Meta:
        model = ArticleGroup
        fields = ['uuid', 'name']

    def validate_uuid(self, data):
        if not ArticleGroup.objects.filter(id=data).exists():
            raise serializers.ValidationError(_("There is no ArticleGroup with such an uuid"))
        return data

    def validate_name(self, data):
        if ArticleGroup.objects.filter(name=data).exists():
            raise serializers.ValidationError(_("ArticleGroup with this name already exists."))
        return data

    def validate(self, validated_data):
        validated_data = super().validate(validated_data)
        if not validated_data.get('id') and not validated_data.get('name'):
            raise serializers.ValidationError(_("You should send either name or uuid for the ArticleGroup"))
        return validated_data


class TemplateArticleSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)
    article_group_uuid = serializers.PrimaryKeyRelatedField(
        queryset=ArticleGroup.objects.all(),
        source="article_group",
        required=True,
        write_only=True
    )
    article_group = ArticleGroupSerializer(read_only=True)
    allocation_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Allocation.objects.all(),
        source="allocation",
        required=True,
        write_only=True
    )
    allocation = ArticleAllocationSerializer(read_only=True)

    class Meta:
        model = TemplateArticle
        fields = ["uuid", "article_group", "name", "description", "allocation",
                  "allocation_uuid", "article_group_uuid"]
