# -*- coding: utf-8 -*-
from django.contrib import admin

from apps.articles.models import ArticleGroup, TemplateArticle


@admin.register(ArticleGroup)
class ArticleGroupAdmin(admin.ModelAdmin):
    list_display = ('created_at', 'updated_at', 'id', 'name')
    list_filter = ('created_at', 'updated_at')
    search_fields = ('name',)


@admin.register(TemplateArticle)
class ArticleAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'article_group',
        'name',
        'description',
        'allocation',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'article_group',
        'allocation',
    )
    search_fields = ('name',)
