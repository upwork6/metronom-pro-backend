# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-17 17:38
from __future__ import unicode_literals

import uuid

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ProfileImage',
            fields=[
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True, verbose_name='ID')),
                ('original_url', models.URLField(blank=True, null=True)),
                ('cropped_url', models.URLField(blank=True, null=True)),
                ('zoom', models.FloatField(blank=True, null=True)),
                ('x_coordinate', models.FloatField(blank=True, null=True)),
                ('y_coordinate', models.FloatField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Profile Image',
                'verbose_name_plural': 'Profile Images',
            },
        ),
    ]
