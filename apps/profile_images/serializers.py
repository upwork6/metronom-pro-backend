from django.core.exceptions import ValidationError
from rest_framework import serializers

from apps.profile_images.models import ProfileImage


class ProfileImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfileImage
        fields = (
            'zoom',
            'x_coordinate',
            'y_coordinate',
        )
