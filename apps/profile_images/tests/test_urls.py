from django.urls import resolve, reverse

from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.profile_images.tests.factories import ProfileImageFactory


class TestProfileImageURLs(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.profile_image = ProfileImageFactory()

    def test_profile_detail_reverse(self):
        assert reverse('api_users:tenant_personnel_data-list', kwargs={'user_pk': 'me'}) == '/api/users/me/tenant_personnel_data/'

    def test_profile_base_resolve(self):
        assert resolve(f'/api/users/me/tenant_personnel_data/').view_name == 'api_users:tenant_personnel_data-list'
