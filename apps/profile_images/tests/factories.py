
import factory

from apps.profile_images.models import ProfileImage


class ProfileImageFactory(factory.DjangoModelFactory):
    zoom = factory.Iterator([0.1, 0.5, 1])
    x_coordinate = factory.Iterator([25, 50, 100])
    y_coordinate = factory.Iterator([20, 55, 100])

    class Meta:
        model = ProfileImage
