import pytest
from django.urls import reverse
from rest_framework import status

from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.profile_images.models import ProfileImage
from apps.profile_images.tests.factories import ProfileImageFactory
from apps.users.models import Profile, User
from apps.persons.models import Person


class ProfileImageBase(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.person_pk = cls.test_user.person_in_crm.pk
        cls.person_image = cls.test_user.person_in_crm.image

        cls.zoom = 0.95
        cls.x_coordinate = 125
        cls.y_coordinate = 75

        cls.new_zoom = None
        cls.new_x_coordinate = None
        cls.new_y_coordinate = 125

        cls.endpoint = reverse('api:person-detail', kwargs={'id': cls.person_pk})

        cls.patch_data = dict(
            image=dict(
                zoom=cls.zoom,
                x_coordinate=cls.x_coordinate,
                y_coordinate=cls.y_coordinate,
            )
        )


class TestCreateProfileImage(ProfileImageBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        assert Person.objects.get(pk=cls.person_pk).image is None

        cls.response = cls.user_client.patch(cls.endpoint, data=cls.patch_data, format='json')
        cls.data = cls.response.data.get('image', None)

    def tearDown(self):
        self.assertIsNotNone(Person.objects.get(pk=self.person_pk).image)

    def test_create_person_image_success(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.test_user.person_in_crm.refresh_from_db()
        self.assertIsNotNone(self.test_user.person_in_crm.image)

    def test_create_person_image_zoom(self):
        value = self.data.get('zoom', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.zoom)

    def test_create_person_image_x_coordinate(self):
        value = self.data.get('x_coordinate', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.x_coordinate)

    def test_create_person_image_y_coordinate(self):
        value = self.data.get('y_coordinate', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.y_coordinate)


class TestUpdateProfileImage(ProfileImageBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.test_user.person_in_crm.image = ProfileImageFactory()
        cls.test_user.person_in_crm.image.save()
        cls.test_user.person_in_crm.save()

        cls.person_pk = cls.test_user.person_in_crm.pk
        cls.person_image_pk = cls.test_user.person_in_crm.image.pk

    def setUp(self):
        self.profile_image = ProfileImage.objects.get(pk=self.person_image_pk)
        self.assertEquals(ProfileImage.objects.filter(pk=self.person_image_pk).count(), 1)
        self.assertEqual(User.objects.all().count(), 5)

        self.assertNotEqual(self.profile_image.zoom, self.new_zoom)
        self.assertNotEqual(self.profile_image.x_coordinate, self.new_x_coordinate)
        self.assertNotEqual(self.profile_image.y_coordinate, self.new_y_coordinate)

    def tearDown(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        person = Person.objects.get(pk=self.person_pk)
        self.assertIsNotNone(person.image)

        self.assertEquals(ProfileImage.objects.filter(pk=self.person_image_pk).count(), 1)

    def test_change_zoom(self):

        self.patch_data = dict(image=dict(zoom=self.new_zoom))
        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')

        self.profile_image.refresh_from_db()

        self.assertEqual(self.profile_image.zoom, self.new_zoom)
        self.assertEqual(self.response.data.get('image').get('zoom', None), self.new_zoom)

    def test_change_x_coordinate(self):

        self.patch_data = dict(image=dict(x_coordinate=self.new_x_coordinate))
        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')

        self.profile_image.refresh_from_db()

        self.assertEqual(self.profile_image.x_coordinate, self.new_x_coordinate)
        self.assertEqual(self.response.data.get('image').get('x_coordinate', None), self.new_x_coordinate)

    def test_change_y_coordinate(self):

        self.patch_data = dict(image=dict(y_coordinate=self.new_y_coordinate))
        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')

        self.profile_image.refresh_from_db()

        self.assertEqual(self.profile_image.y_coordinate, self.new_y_coordinate)
        self.assertEqual(self.response.data.get('image').get('y_coordinate', None), self.new_y_coordinate)


class TestDeleteProfileImage(ProfileImageBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def setUp(self):
        # data preparation
        self.test_user.person_in_crm.image = ProfileImageFactory()
        self.test_user.person_in_crm.save()
        self.person_image_pk = self.test_user.person_in_crm.image.pk
        self.person_pk = self.test_user.person_in_crm.pk

        # pre-checks, common to all:
        self.assertEquals(ProfileImage.objects.filter(pk=self.person_image_pk).count(), 1)
        self.assertIsNotNone(Person.objects.filter(pk=self.person_pk).all().count(), 1)
        self.assertIsNotNone(Person.objects.filter(pk=self.person_pk).all().first().image)

    # post-checks, common to all:
    def tearDown(self):
        self.assertIsNotNone(Person.objects.filter(pk=self.person_pk).all().count(), 1)
        self.assertIsNone(Person.objects.filter(pk=self.person_pk).all().first().image)

    def _send(self):
        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')

    def test_delete_image_none_field(self):
        # the image object have to be deleted
        self.patch_data = dict(
            image=None,
        )

        self._send()

        self.assertEquals(ProfileImage.objects.filter(pk=self.person_image_pk).count(), 0)
