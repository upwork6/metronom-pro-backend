from django.test import TestCase

from apps.profile_images.tests.factories import ProfileImageFactory


class TestProfileImage(TestCase):

    def setUp(self):
        self.profile_image = ProfileImageFactory()

    def test_profile_image(self):
        self.assertIsNotNone(self.profile_image)

    def test_zoom(self):
        self.assertTrue(self.profile_image.zoom)

    def test_x_coordinate(self):
        self.assertTrue(self.profile_image.x_coordinate)

    def test_y_coordinate(self):
        self.assertTrue(self.profile_image.y_coordinate)
