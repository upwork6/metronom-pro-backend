from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ProfileImagesConfig(AppConfig):
    name = 'apps.profile_images'
    verbose_name = _('Profile images')
