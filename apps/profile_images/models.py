from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.metronom_commons.models import UserAccessHRMetronomBaseModel


class ProfileImage(UserAccessHRMetronomBaseModel):
    class Meta:
        verbose_name = _('Profile Image')
        verbose_name_plural = _('Profile Images')

    zoom = models.FloatField(null=True, blank=True, )
    x_coordinate = models.FloatField(null=True, blank=True, )
    y_coordinate = models.FloatField(null=True, blank=True, )


# function used in the default attribute of the referencing object
def new_profile_image():
    return ProfileImage.objects.create().pk
