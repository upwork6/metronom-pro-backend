from django.core.management.base import BaseCommand

from apps.accounting.tests.factories import AllocationFactory
from apps.activities.tests.factories import (StepFactory,
                                             TemplateActivityNiceFactory)
from apps.articles.tests.factories import TemplateArticleFactory
from apps.backoffice.tests.factories import (TermsParagraphCollectionFactory, ThankYouNoteFactory, TaxNoteFactory,
                                             AgencyBankAccountFactory)
from apps.bankaccounts.tests.factories import BankFactory
from apps.companies.tests.factories import CompanyNiceFactory, SectorFactory
from apps.employments.tests.factories import EmploymentNiceFactory
from apps.persons.tests.factories import PersonNiceFactory, PromotionFactory
from apps.projects.tests.factories import (CampaignNiceFactory,
                                           ProjectNiceFactory)
from apps.urls.tests.factories import URLTypeNiceFactory
from apps.users.models import User
from apps.workinghours.tests.factories import ClosingHoursFactory, WorkingHoursFactory
from config.example_data import (ACTIVITIES, ALLOCATIONS, ARTICLES, BANKS,
                                 CAMPAIGNS, CLOSING_HOURS, COMPANIES, AGENCY_BANKACCOUNTS,
                                 EMPLOYMENTS, PERSONS, THANKYOU_NOTES, TAX_NOTES,
                                 PROJECTS, PROMOTIONS, SECTORS, STEPS, TERMS_PARAGRAPHS,
                                 URL_TYPES, WORKING_HOURS)

GENERATORS = [
    (f'Banks', BankFactory, BANKS),
    (f'URL Types', URLTypeNiceFactory, URL_TYPES),
    (f'Allocations', AllocationFactory, ALLOCATIONS),
    (f'Promotions', PromotionFactory, PROMOTIONS),
    (f'Tax Notes', TaxNoteFactory, TAX_NOTES),
    (f'Thank you Notes', ThankYouNoteFactory, THANKYOU_NOTES),
    (f'Term Paragraphs', TermsParagraphCollectionFactory, TERMS_PARAGRAPHS),
    (f'Agency Bank Accounts', AgencyBankAccountFactory, AGENCY_BANKACCOUNTS),
    (f'Sectors', SectorFactory, SECTORS),
    (f'Steps', StepFactory, STEPS),
    (f'Activities', TemplateActivityNiceFactory, ACTIVITIES),
    (f'Articles', TemplateArticleFactory, ARTICLES),
    (f'WorkingHours', WorkingHoursFactory, WORKING_HOURS),
    (f'ClosingHours', ClosingHoursFactory, CLOSING_HOURS)
]


class Command(BaseCommand):
    """
        Clear all caches
    """

    help = f'''Creates base data'''

    def handle(self, **options):

        user_count = User.objects.all().count()

        print(f"---------------------------------")

        print(f"Users in the system: {user_count}")

        print(f"---------------------------------")

        for title, factory, factory_data in GENERATORS:

            if isinstance(factory_data, int):
                print(f'Generating {factory_data} random {title} ...')
                factory.create_batch(factory_data)
                continue

            print(f'Generating {len(factory_data)} {title} with {factory.__name__}...')

            for row_data in factory_data:
                factory(**row_data)

        print(f'Done.')
