import traceback
from pathlib import Path

from django.core.management.base import BaseCommand
from django.db import transaction
from openpyxl import Workbook, load_workbook

from apps.bankaccounts.models import Bank


class Command(BaseCommand):
    """
        Convert input csv file to excel file
    """
    help = '''Converts input csv file to xslx file'''
    COUNTRY_CODE_TO_BANK_CODE_LENGTH = {
        'DE': 8,
        'BE': 3,
        'NL': 4,
        'LU': 3,
        'CH': 5,
        'AT': 5,
        'LI': 5,
    }
    CH_HEADERS = [
        'Gruppe', 'BCNr', 'Filial-ID', 'BCNr neu', 'SIC-Nr', 'Hauptsitz', 'BC-Art' 'gültig ab',
        'SIC', 'euroSIC', 'Sprache', 'Kurzbez.', 'Bank/Institut', 'Domizil', 'Postadresse',
        'PLZ', 'Ort', 'Telefon', 'Fax', 'Vorwahl', 'Landcode', 'Postkonto', 'SWIFT'
    ]

    DE_HEADERS = [
        'Bankleitzahl', 'Merkmal', 'Bezeichnung', 'PLZ', 'Ort', 'Kurzbezeichnung', 'PAN', 'BIC',
        'Prüfziffer-berechnungs-methode', 'Datensatz-nummer', 'Änderungs-kennzeichen', 'Bankleitzahl-löschung',
        'Nachfolge-Bankleitzahl'
    ]

    COUNTRY_CODE_TO_RELEVANT_BANK_COLUMNS = {
        'CH': {'bank_code': 1, 'bank_name': 12, 'bic': 22},
        'DE': {'bank_code': 0, 'bank_name': 2, 'bic': 7}
    }

    def add_arguments(self, parser):
        parser.add_argument('country', type=str)
        parser.add_argument('source_file', type=str)
        parser.add_argument(
            '--delete',
            action='store_const', const=True,
            default=False,
            help='Delete all banks before importing',
        )

    @transaction.atomic
    def handle(self, **options):
        self.country = options['country']
        source_file = Path(options['source_file'])
        self.delete = options['delete']
        self.read_file_to_entries(file=source_file)

    def read_file_to_entries(self, file):
        workbook = load_workbook(
            filename=file,
            read_only=True,
        )

        try:
            self._check_format(workbook)
            print("Format of file okay...")
            for worksheet in workbook.worksheets:
                print("Import Sheet {}...".format(worksheet.title))
                self._handle_worksheet(worksheet)

        except Exception:
            traceback.print_exc()
            raise

    def _check_format(self, workbook):
        for sheet in workbook.worksheets:
            data = list(sheet.iter_rows())

            try:
                first_row = data[0]
                data = data[1:]
            except IndexError:
                raise Exception('Sheet {0} data is malformed: {1}'.format(sheet.title, data))

            self.first_row = first_row

    def _handle_worksheet(self, worksheet):
        data = list(worksheet.iter_rows())[1:]

        if self.delete:
            print('Deleting old banks')
            Bank.objects.all().delete()

        for row in data:
            if not row[0].value:
                continue

            dict_bank = self._handle_row(row)

            if dict_bank.get('bic') == '':
                continue

            print(dict_bank)

            Bank.objects.get_or_create(
                bic=dict_bank['bic'],
                defaults={
                    'country_code': dict_bank['country_code'],
                    'bank_name': dict_bank['bank_name'],
                    'bank_code': dict_bank['bank_code']
                }
            )

    def _handle_row(self, row):
        bank_cols = self.COUNTRY_CODE_TO_RELEVANT_BANK_COLUMNS[self.country]
        try:
            bank_code = row[bank_cols.get('bank_code')].value if row[bank_cols.get(
                'bank_code')].value is not None else ''
            bank_code_length = self.COUNTRY_CODE_TO_BANK_CODE_LENGTH[self.country]
            bank_code_str = ''

            if bank_code:
                bank_code_str = str(bank_code)

            if self.country == 'CH':
                bank_code_new = row[3].value if row[3].value is not None else ''
                if bank_code_new:
                    bank_code_str = str(bank_code_new)

            bank_name = str.strip(row[bank_cols.get('bank_name')].value) if row[bank_cols.get(
                'bank_name')].value is not None else ''

            bic = str.strip(row[bank_cols.get('bic')].value) if row[bank_cols.get(
                'bic')].value is not None else ''

            bank_code_str = bank_code_str.rjust(bank_code_length, '0')

            return {
                'country_code': self.country,
                'bic': bic,
                'bank_name': bank_name,
                'bank_code': bank_code_str
            }

        except ValueError:
            print('value error')
            traceback.print_exc()
            raise
        except KeyError:
            print('Import stopped:')
            traceback.print_exc()
            raise

    def __init__(self):
        super(Command, self).__init__()
        self.country = ''
        self.delete = False
