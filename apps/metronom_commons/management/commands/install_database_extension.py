from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.postgres.operations import CreateExtension
from django.db import connection


class Command(BaseCommand):
    """Set up the hstore extension in PostgreSQL"""

    help = 'Set up the hstore extension in PostgreSQL'

    def handle(self, **options):
        with connection.cursor() as cursor:
            cursor.execute("CREATE EXTENSION IF NOT EXISTS hstore;")
            row = cursor.fetchone()
            cursor.close()
            self.stdout.write(self.style.SUCCESS('Successfully Installed "hstore":\n, "%s"' % row))
