from django.conf import settings
from django.core.cache import caches
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """
        Clear all caches
    """

    help = '''Clear all caches'''

    def handle(self, **options):
        for cache_alias in settings.CACHES.keys():
            caches[cache_alias].clear()
            print(f'Cache "{cache_alias}" has been cleared.')
