from django.core.management.base import BaseCommand
from django.db import transaction

from apps.companies.models import Company, Sector
from apps.companies.tests.factories import CompanyFactory, SectorFactory
from apps.employments.models import Employment
from apps.employments.tests.factories import EmploymentFactory
from apps.persons.models import Person
from apps.persons.tests.factories import PersonNiceFactory
from apps.projects.models import Campaign, Project
from apps.projects.tests.factories import CampaignFactory, ProjectFactory
from apps.users.models import User
from apps.users.tests.factories import UserFactory

DEFAULT_COUNT_CREATED = 10

FACTORIES = {
    'Person': PersonNiceFactory,
    'Company': CompanyFactory,
    'Sector': SectorFactory,
    'User': UserFactory,
    'Employment': EmploymentFactory,
    'Campaign': CampaignFactory,
    'Project': ProjectFactory
}


def create_model_instances(model_cls, count=DEFAULT_COUNT_CREATED):
    factory_cls = FACTORIES[model_cls.__name__]
    instances = []
    for i in range(count):
        instance = factory_cls()
        instances.append(instance)
    print(f'Count of created "{model_cls.__name__}": {count}')
    return instances


class Command(BaseCommand):
    help = 'Create test data'

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, nargs='?', default=DEFAULT_COUNT_CREATED,
                            help='Count of created instances for every model')

    def handle(self, count, **options):
        with transaction.atomic():
            create_model_instances(Sector, 5)
            create_model_instances(User, 10)
            create_model_instances(Person, count)
            create_model_instances(Company, count)
            create_model_instances(Employment, count)
            create_model_instances(Campaign, 20)
            create_model_instances(Project, count)
