import logging

logger = logging.getLogger('backend')

FORMAT = "\n\n%(message)s"
logging.basicConfig(format=FORMAT)

logger.setLevel(logging.DEBUG)
