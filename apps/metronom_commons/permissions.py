
from dry_rest_permissions.generics import DRYPermissions
from rest_framework.permissions import BasePermission, DjangoObjectPermissions, SAFE_METHODS


class IsHRPermission(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.groups.filter(name='HR').count() > 0


class IsAuthenticatedAndDeleteAllowedOnlyToAdmin(BasePermission):
    def has_permission(self, request, view):

        if request.method == 'DELETE':
            return request.user and request.user.is_authenticated and request.user.is_staff
        else:
            return request.user and request.user.is_authenticated


class GenericDRYPermissions(DRYPermissions):
    """ This is DRY permission update to make it possible to work with generics serializers like in the 
    batch update view.

    """

    def has_permission(self, request, view):
        """
        Overrides the standard function and figures out methods to call for global permissions.
        """
        if not self.global_permissions:
            return True

        serializer_class = view.get_serializer_class()

        model = serializer_class.get_model_from_request(request=request)
        if model is None:
            return False

        model_class = model

        action_method_name = None
        if hasattr(view, 'action'):
            action = self._get_action(view.action)
            action_method_name = "has_{action}_permission".format(action=action)
            # If the specific action permission exists then use it, otherwise use general.
            if hasattr(model_class, action_method_name):
                return getattr(model_class, action_method_name)(request)

        if request.method in SAFE_METHODS:
            assert hasattr(model_class, 'has_read_permission'), \
                self._get_error_message(model_class, 'has_read_permission', action_method_name)
            return model_class.has_read_permission(request)
        else:
            assert hasattr(model_class, 'has_write_permission'), \
                self._get_error_message(model_class, 'has_write_permission', action_method_name)
            return model_class.has_write_permission(request)

    def has_object_permission(self, request, view, obj):
        """
        Overrides the standard function and figures out methods to call for object permissions.
        """
        if not self.object_permissions:
            return True

        serializer_class = view.get_serializer_class()
        model_class = serializer_class.get_model_from_request(request=request)
        action_method_name = None
        if hasattr(view, 'action'):
            action = self._get_action(view.action)
            action_method_name = "has_object_{action}_permission".format(action=action)
            # If the specific action permission exists then use it, otherwise use general.
            if hasattr(obj, action_method_name):
                return getattr(obj, action_method_name)(request)

        if request.method in SAFE_METHODS:
            assert hasattr(obj, 'has_object_read_permission'), \
                self._get_error_message(model_class, 'has_object_read_permission', action_method_name)
            return obj.has_object_read_permission(request)
        else:
            assert hasattr(obj, 'has_object_write_permission'), \
                self._get_error_message(model_class, 'has_object_write_permission', action_method_name)
            return obj.has_object_write_permission(request)
