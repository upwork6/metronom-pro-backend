
import uuid

from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from dry_rest_permissions.generics import authenticated_users
from safedelete.models import SOFT_DELETE_CASCADE, SafeDeleteModel

from apps.metronom_commons.data import ACTIVITY_STATUS

# TODO: Checkout https://gist.github.com/kyle-eshares/fbecc71022b62515a484a4a33d13014d


class ROLE:
    """
        The roles for an User. Every user can only have one role!

        Limited roles:
            None - The default role, no access
            Customer - can accept his Offers and invoices, may see project progress (NOT IMPLEMENTED YET)
            Freelancer - can be added as a Worker to a Project and can add hours (NOT IMPLEMENTED YET)

        User roles:
            User - can READ most objects

        Content-based roles:
            HR - can change Roles, but not from Admin - can access all User data
            Accounting - can access accounting data, invoices, etc.; can delete invoices, can assign KAM to a Company
            Backoffice - can edit the Backoffice (Library, Bank accounts, etc.)

        Admins:
            Admin - can access everything, only the Admin can delete protected objects + see objects, which have been removed
            Owner - most powerful role, can create and remove Admin, access to Backoffice

     """

    # Limited roles
    NONE = 'none'
    CUSTOMER = 'customer'
    FREELANCER = 'freelancer'

    # Normal Users
    USER = 'user'

    # Content-related roles
    HR = 'hr'
    ACCOUNTING = 'accounting'
    BACKOFFICE = 'backoffice'

    # Admins
    ADMIN = 'admin'
    OWNER = 'owner'

    CHOICES = (
        (NONE, _('No role')),
        (CUSTOMER, _('Customer')),
        (FREELANCER, _('Freelancer')),

        (USER, _('User')),

        (HR, _('HR')),
        (ACCOUNTING, _('Accounting')),
        (BACKOFFICE, _('Backoffice')),

        (ADMIN, _('Admin')),
        (OWNER, _('Owner')),
    )

    LIMITED_ROLES = [NONE, CUSTOMER, FREELANCER]
    USER_ROLES = [USER, HR, ACCOUNTING, BACKOFFICE, ADMIN, OWNER]
    ADMIN_ROLES = [ADMIN, OWNER]


class SoftDeleteMixin(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    class Meta:
        abstract = True


class SoftDeleteUniqueMixin(SoftDeleteMixin):
    """ We need this mixin only for one reason - allow our code to easily distinguish whether it need
    to subscribe model to the pre soft delete signal to change unique fields or not.

    Every model inherited from this should provide
    _unique_fields_to_override_during_deletion list

    **BE AWARE**
    Signal support override for now only for CharField and TextField

    """
    _unique_fields_to_override_during_deletion = []

    class Meta:
        abstract = True

    @staticmethod
    def pre_softdelete_signal(sender, instance, *args, **kwargs):
        unique_fields = sender._unique_fields_to_override_during_deletion
        for field_name in unique_fields:
            old_value = getattr(instance, field_name, "")
            new_value = f"{old_value} (deleted)"
            i = 0
            while sender.deleted_objects.filter(Q(**{field_name: new_value})).exists():
                i += 1
                new_value = f"{old_value} (deleted) {i}"
            setattr(instance, field_name, new_value)
        instance.save(update_fields=unique_fields, keep_deleted=True)

    @staticmethod
    def post_undelete_signal(sender, instance, *args, **kwargs):
        unique_fields = sender._unique_fields_to_override_during_deletion
        for field_name in unique_fields:
            old_value = getattr(instance, field_name, "")
            original_value = old_value.split(" (deleted)")[0]
            new_value = original_value
            i = 1
            while sender.objects.filter(Q(**{field_name: new_value})).exists():
                i += 1
                new_value = f"{original_value} {i}"
            setattr(instance, field_name, new_value)
        instance.save(update_fields=unique_fields)


class MetronomBaseQueryset(models.QuerySet):
    """ This is the Base Queryset used for our BaseManager."""

    def delete(self, **kwargs):
        return super().delete(**kwargs)


class MetronomBaseManager(models.Manager):
    queryset_class = MetronomBaseQueryset
    use_for_related_fields = True

    def get_queryset(self):
        q = self.queryset_class(self.model)
        if hasattr(self, 'core_filters'):
            q = q.filter(
                **self.core_filters
            )

        return q

    def all_objects_including_void(self):
        return self.get_queryset()


class MetronomBaseModel(models.Model):
    """ Base model to take care of the following things:


    This list contains stuff, which belongs to a MetronomBaseSerializer to be created as well

    - [ ] Revision (django-reversion)
    - [ ] Archived + Soft delete
    - [ ] Permission
    - [ ] UUID
    - [ ] Logging (API_LOGGING=True)
    - [ ] Multi-Tenancy (TENANT_BASED=True)
    - [ ] created_by
    - [ ] Renaming to _date, _datetime, etc. depending on field type
    - [ ] Caching
    - [ ] Validation Errors
    - [ ] Pagination

    """

    id = models.UUIDField(_('UUID'), primary_key=True, unique=True, default=uuid.uuid4, editable=False, db_index=True)
    created_at = models.DateTimeField(_('Created'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Modified'), auto_now=True)

    objects = MetronomBaseManager()

    class Meta:
        abstract = True

    @staticmethod
    @authenticated_users
    def has_read_permission(request):
        return request.user.role == ROLE.ADMIN

    @staticmethod
    @authenticated_users
    def has_write_permission(request):
        return request.user.role == ROLE.ADMIN

    @staticmethod
    @authenticated_users
    def has_create_permission(request):
        return request.user.role == ROLE.ADMIN

    @staticmethod
    @authenticated_users
    def has_destroy_permission(request):
        return request.user.role == ROLE.ADMIN

    @authenticated_users
    def has_object_read_permission(self, request):
        return request.user.role == ROLE.ADMIN

    @authenticated_users
    def has_object_write_permission(self, request):
        return request.user.role == ROLE.ADMIN

    @authenticated_users
    def has_object_destroy_permission(self, request):
        return request.user.role == ROLE.ADMIN


class BackofficeBaseModel(MetronomBaseModel):
    """ User no access, Backoffice create/read/edit
    """

    class Meta:
        abstract = True

    @staticmethod
    @authenticated_users
    def has_read_permission(request):
        return request.user.role in [ROLE.BACKOFFICE, ROLE.ADMIN]

    @staticmethod
    @authenticated_users
    def has_write_permission(request):
        return request.user.role in [ROLE.BACKOFFICE, ROLE.ADMIN]

    @staticmethod
    @authenticated_users
    def has_create_permission(request):
        return request.user.role in [ROLE.BACKOFFICE, ROLE.ADMIN]

    @staticmethod
    @authenticated_users
    def has_destroy_permission(request):
        return request.user.role in [ROLE.BACKOFFICE, ROLE.ADMIN]

    @authenticated_users
    def has_object_read_permission(self, request):
        admin_check = super().has_object_read_permission(request)
        return admin_check or request.user.role == ROLE.BACKOFFICE

    @authenticated_users
    def has_object_write_permission(self, request):
        admin_check = super().has_object_write_permission(request)
        return admin_check or request.user.role == ROLE.BACKOFFICE

    @authenticated_users
    def has_object_destroy_permission(self, request):
        admin_check = super().has_object_destroy_permission(request)
        return admin_check or request.user.role == ROLE.BACKOFFICE


class UserAccessBackofficeMetronomBaseModel(BackofficeBaseModel):
    """ User has read, Backoffice CRUD
    """

    class Meta:
        abstract = True

    @staticmethod
    @authenticated_users
    def has_read_permission(request):
        return request.user.role in [ROLE.BACKOFFICE, ROLE.ADMIN, ROLE.USER]

    @authenticated_users
    def has_object_read_permission(self, request):
        admin_check = super().has_object_read_permission(request)
        return admin_check or request.user.role in [ROLE.BACKOFFICE, ROLE.USER]


class UserAccessMetronomBaseModel(MetronomBaseModel):
    """ User can CRU, most basic handling in the System
    """

    class Meta:
        abstract = True


class UserCRUAccountingDeletePermission(MetronomBaseModel):
    """ User can CRU, Accounting can CRUD for all
    """

    class Meta:
        abstract = True

    @staticmethod
    @authenticated_users
    def has_read_permission(request):
        return request.user.role in [ROLE.USER, ROLE.ADMIN, ROLE.ACCOUNTING]

    @staticmethod
    @authenticated_users
    def has_write_permission(request):
        return request.user.role in [ROLE.USER, ROLE.ADMIN, ROLE.ACCOUNTING]

    @staticmethod
    @authenticated_users
    def has_create_permission(request):
        return request.user.role in [ROLE.USER, ROLE.ADMIN, ROLE.ACCOUNTING]

    @staticmethod
    @authenticated_users
    def has_destroy_permission(request):
        return request.user.role in [ROLE.ADMIN, ROLE.ACCOUNTING]

    @authenticated_users
    def has_object_read_permission(self, request):
        admin_check = super().has_object_read_permission(request)
        return admin_check or request.user.role in [ROLE.ACCOUNTING, ROLE.USER]

    @authenticated_users
    def has_object_write_permission(self, request):
        admin_check = super().has_object_write_permission(request)
        return admin_check or request.user.role in [ROLE.ACCOUNTING, ROLE.USER]

    @authenticated_users
    def has_object_destroy_permission(self, request):
        admin_check = super().has_object_destroy_permission(request)
        return admin_check or request.user.role == ROLE.ACCOUNTING


class UserCanDeleteMetronomBaseModel(MetronomBaseModel):
    """ User can CRUD, used for user-generated Content like comments or file uploads
    """

    class Meta:
        abstract = True


class UserAccessHRMetronomBaseModel(MetronomBaseModel):
    """ User can CRU his own, HR is allowed everything
    """

    class Meta:
        abstract = True


class UserCRUBackofficeHRDeleteMetronomBaseModel(MetronomBaseModel):
    """ User can CRU his own, Back Office and HR is allowed everything
    """

    class Meta:
        abstract = True

    @staticmethod
    @authenticated_users
    def has_read_permission(request):
        return request.user.role in [ROLE.USER, ROLE.HR, ROLE.BACKOFFICE, ROLE.ADMIN]

    @staticmethod
    @authenticated_users
    def has_write_permission(request):
        return request.user.role in [ROLE.USER, ROLE.HR, ROLE.BACKOFFICE, ROLE.ADMIN]

    @staticmethod
    @authenticated_users
    def has_create_permission(request):
        return request.user.role in [ROLE.USER, ROLE.HR, ROLE.BACKOFFICE, ROLE.ADMIN]

    @staticmethod
    @authenticated_users
    def has_destroy_permission(request):
        return request.user.role in [ROLE.HR, ROLE.BACKOFFICE, ROLE.ADMIN]

    @authenticated_users
    def has_object_read_permission(self, request):
        admin_check = super().has_object_read_permission(request)
        return admin_check or request.user.role in [ROLE.USER, ROLE.HR, ROLE.BACKOFFICE, ROLE.ADMIN]

    @authenticated_users
    def has_object_write_permission(self, request):
        admin_check = super().has_object_write_permission(request)
        return admin_check or request.user.role in [ROLE.USER, ROLE.HR, ROLE.BACKOFFICE, ROLE.ADMIN]

    @authenticated_users
    def has_object_destroy_permission(self, request):
        admin_check = super().has_object_destroy_permission(request)
        return admin_check or request.user.role in [ROLE.HR, ROLE.BACKOFFICE, ROLE.ADMIN]


class UserReadOnlyHRMetronomBaseModel(MetronomBaseModel):
    """ User can R his own, HR is allowed everything
    """

    class Meta:
        abstract = True


class UserReadOnlyAccountingMetronomBaseModel(MetronomBaseModel):
    """ User can R, Accounting is allowed everything
    """

    class Meta:
        abstract = True


class OfferAndInvoiceMetronomBaseModel(MetronomBaseModel):
    """
    User => ReadOnly
    Accounting, KAM and PM/OM(ProjectManager/OfferManager) => CRUD
    REQUIRED:
        self.owner method
        self.key_account_manager method
    """

    @staticmethod
    @authenticated_users
    def has_create_permission(request):
        return request.user.role in [ROLE.ADMIN, ROLE.ACCOUNTING, ROLE.OWNER, ROLE.USER]

    @staticmethod
    @authenticated_users
    def has_read_permission(request):
        return request.user.role in ROLE.USER_ROLES

    @authenticated_users
    def has_object_read_permission(self, request):
        create_update_destroy_permission = self.get_create_update_destroy_permission(request)
        return create_update_destroy_permission or request.user.role == ROLE.USER

    @staticmethod
    @authenticated_users
    def has_write_permission(request):
        return request.user.role in [ROLE.ADMIN, ROLE.ACCOUNTING, ROLE.OWNER, ROLE.USER]

    @authenticated_users
    def has_object_write_permission(self, request):
        create_update_destroy_permission = self.get_create_update_destroy_permission(request)
        return create_update_destroy_permission

    @staticmethod
    @authenticated_users
    def has_destroy_permission(request):
        return request.user.role in [ROLE.ADMIN, ROLE.ACCOUNTING, ROLE.OWNER, ROLE.USER]

    @authenticated_users
    def has_object_destroy_permission(self, request):
        create_update_destroy_permission = self.get_create_update_destroy_permission(request)
        return create_update_destroy_permission

    @authenticated_users
    def get_create_update_destroy_permission(self, request):
        """ Apart from User and Accounting roles, here we have also KAM and PM permissions. They are not a roles.
        Admin - request.user.role == ROLE.ADMIN
        KAM - self.(project||campaign).company.key_account_manager
        PM - (OfferManager/ProjectManager) => self.offer_manager || self.(project||campaign).(project||campaign)_owner
        """
        is_admin,  is_accountant = request.user.role == ROLE.ADMIN, request.user.role == ROLE.ACCOUNTING
        is_pm, is_kam = self.owner == request.user, self.key_account_manager == request.user
        return is_admin or is_pm or is_kam or is_accountant

    class Meta:
        abstract = True


class AccountingMetronomBaseModel(MetronomBaseModel):
    """ only Accounting can CRUD; NOT IN USE FOR NOW
    """

    class Meta:
        abstract = True


class AdminMetronomBaseModel(MetronomBaseModel):
    """ only Admin can CRUD
    """

    class Meta:
        abstract = True


class OrderingMixin(models.Model):
    position = models.PositiveSmallIntegerField(default=1)

    class Meta:
        abstract = True


class OrderingMixinWithoutSignal(models.Model):
    """ This is the same mixin as the OrderingMixin, but in this case we  don't have auto post_save signal which 
    rearrange the position if there any duplicates. This is usefull for models like ProjectActivityStep, where
    position should be unique only inside the one project, not in the all system
    """
    position = models.PositiveSmallIntegerField(default=1)

    class Meta:
        abstract = True


class ActivityStepDynamicPermissions(models.Model):
    """It does Permissions like DRYPermissions only that id doesn't take `request` as args
    ONLY the USER from `**kwargs"""

    USER_ALLOWED_CHANGES = [ACTIVITY_STATUS.DONE, ACTIVITY_STATUS.PLAN, ACTIVITY_STATUS.WORK]
    SPECIAL_USERS_ALLOWED_CHANGES = [*USER_ALLOWED_CHANGES, ACTIVITY_STATUS.BILLABLE,
                                     ACTIVITY_STATUS.ON_HOLD, ACTIVITY_STATUS.CANCEL]

    def user_is_allowed_to_change_status(self, **kwargs):
        """
        :param kwargs: dict("user": logged_in_user, "status": new_status_for_activity_step_status)
        :type kwargs: dict
        :return: True | False
        :rtype: Boolean
        """
        update_permission, new_status, logged_in_user = False, kwargs.get('status'), kwargs.get('user')
        if self.status != ACTIVITY_STATUS.CANCEL:
            if logged_in_user.role == ROLE.USER and new_status in self.USER_ALLOWED_CHANGES:
                update_permission = True
            if logged_in_user.role in [*ROLE.ADMIN_ROLES, ROLE.ACCOUNTING] and \
                    new_status in self.SPECIAL_USERS_ALLOWED_CHANGES:
                update_permission = True
            if logged_in_user in [self.owner, self.key_account_manager] and \
                    new_status in self.SPECIAL_USERS_ALLOWED_CHANGES:
                update_permission = True
            if new_status == ACTIVITY_STATUS.CLOSED and logged_in_user.role in ROLE.ADMIN_ROLES:
                update_permission = True
        return update_permission

    class Meta:
        abstract = True


def post_save_position_change_signal(sender, instance, created, **kwargs):
    """ We need this signal to assign appropriate position for the OrderingMixin models.
    """
    if created:
        position = 0
        if hasattr(sender, "all_objects"):
            qs = sender.all_objects.exclude(id=instance.id)
        else:
            qs = sender.objects.exclude(id=instance.id)
        # in some cases we allow to create objects with pre-defined positions
        if not instance.position or qs.filter(position=instance.position).exists():
            last_instance = qs.order_by('-position').first()
            if last_instance:
                position = last_instance.position
            instance.position = position + 1
            instance.save(update_fields=['position'])
