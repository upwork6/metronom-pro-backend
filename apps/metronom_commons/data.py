from django.utils.translation import ugettext_lazy as _


class SALARY_EXTRA:
    NO_EXTRA_SALARY = 12
    THIRTEENTH_SALARY = 13
    FOURTEENTH_SALARY = 14

    CHOICES = (
        (NO_EXTRA_SALARY, _("No extra salary")),
        (THIRTEENTH_SALARY, _("13. Salaries")),
        (FOURTEENTH_SALARY, _("14. Salaries")),
    )


class CURRENCY:
    CHF = 'CHF'
    EUR = 'EUR'
    USD = 'USD'
    GBP = 'GBP'

    CHOICES = (
        (CHF, CHF),
        (EUR, EUR),
        (USD, USD),
        (GBP, GBP)
    )

    MONEY_CHOICES = [  # django-money requires a loist not, tuple
        (CHF, CHF),
        (EUR, EUR),
        (USD, USD),
        (GBP, GBP)
    ]


class INVOICE_TYPE:
    SINGLE_INVOICE = 'single_invoice'
    DOWN_PAYMENT_INVOICE = 'down_payment'
    CREDIT_INVOICE = 'credit'

    CHOICES = (
        (SINGLE_INVOICE, _('Single invoice')),
        (DOWN_PAYMENT_INVOICE, _('Down payment invoice')),
        (CREDIT_INVOICE, _('Credit invoice'))
    )


class ADDED_CHARGES_DEPENDENCY:
    AGENCY_SERVICES = 'agency'
    EXTERNAL_SERVICES = 'external'
    PASSING_SERVICES = 'passing'

    CHOICES = (
        (AGENCY_SERVICES, _('Agency')),
        (EXTERNAL_SERVICES, _('External services')),
        (PASSING_SERVICES, _('Passing services')),
    )


class BILLING_AGREEMENT:
    ACCORDING_TO_OFFER = 'according_to_offer'
    ACCORDING_TO_EFFORT = 'according_to_effort'

    CHOICES = (
        (ACCORDING_TO_OFFER, _('According to offer')),
        (ACCORDING_TO_EFFORT, _('According to effort')),
    )


class BILLING_DELIVERY:
    POST = 'post'
    EMAIL = 'email'

    CHOICES = (
        (POST, _('with Post')),
        (EMAIL, _('with Email')),
    )


class BILLING_INTERVAL:
    TASK_BASED = 'task_based'
    PROJECT_BASED = 'project_based'
    CAMPAIGN_BASED = 'campaign_based'

    CHOICES = (
        (TASK_BASED, _('Task based')),
        (PROJECT_BASED, _('Project based')),
        (CAMPAIGN_BASED, _('Campaign based')),
    )


class ABSENCE_REASONS(object):
    ACCIDENT = 'accident'
    CIVIL_DEFENCE = 'civil_defence'
    CIVIL_SERVICE = 'civil_service'
    COMPENSATION = 'compensation'
    EDUCATION = 'education'
    FAMILIAL = 'familial'
    MILITARY = 'military'
    MOTHERHOOD = 'motherhood'
    OVERTIME = 'overtime'
    RELOCATION = 'relocation'
    SICKNESS = 'sickness'
    VACATION = 'vacation'
    OTHER = 'other'

    CHOICES = (
        (ACCIDENT, _('Accident')),
        (CIVIL_DEFENCE, _('Civil defence')),
        (CIVIL_SERVICE, _('Civil service')),
        (COMPENSATION, _('Compensation')),
        (EDUCATION, _('Further education, school')),
        (FAMILIAL, _('Familial')),
        (MILITARY, _('Military')),
        (MOTHERHOOD, _('Motherhood')),
        (OVERTIME, _('Overtime correction')),
        (RELOCATION, _('Relocation')),
        (SICKNESS, _('Sickness')),
        (VACATION, _('Vacation')),
        (OTHER, _('Other'))
    )


class ABSENCE_TYPE(object):
    HALF_DAY, FULL_DAY = 0.5, 1

    CHOICES = (
        (HALF_DAY, _('Half day')),
        (FULL_DAY, _('Full day'))
    )


class WEEKDAYS:
    CHOICES = [
        (1, 'monday'),
        (2, 'tuesday'),
        (3, 'wednesday'),
        (4, 'thursday'),
        (5, 'friday'),
        (6, 'saturday'),
        (7, 'sunday')
    ]

    SHORT_CHOICES = [
        (1, _('Mo')),
        (2, _('Tu')),
        (3, _('We')),
        (4, _('Th')),
        (5, _('Fr')),
        (6, _('Sa')),
        (7, _('Su'))
    ]


class EtagCaching:
    ETAG_SETTINGS_CACHE_KEY = "settings_etag"


class INVOICE_STATUS(object):
    READY_FOR_INVOICING, INVOICED, PAID, NOT_INVOICED = 'READY', 'INVOICED', 'PAID', 'NOT_INVOICED'

    CHOICES = (
        (READY_FOR_INVOICING, _("Ready for invoicing")),
        (INVOICED, _("Invoiced")),
        (PAID, _("Paid")),
        (NOT_INVOICED, _("Not invoiced")),
    )


class ACTIVITY_STATUS(object):
    CANCEL, PLAN, WORK, DONE, ON_HOLD, = 'canceled', 'plan', 'work', 'done', 'on_hold'
    BILLABLE, CLOSED = 'billable', 'closed'

    CHOICES = (
        (CANCEL, _("Cancel")),
        (PLAN, _("Plan")),
        (WORK, _("Work")),
        (DONE, _("Done")),
        (ON_HOLD, _("On_hold")),
        (BILLABLE, _("Billable")),
        (CLOSED, _("Closed")),
    )


class PROJECT_ICONS(object):
    BILLABLE, DELAYED, FELL_ASLEEP, IS_CLOSED = 'billable', 'delayed', 'feel asleep', 'is_closed'


class NUMBER_FORMAT(object):
    SWISS, GERMAN, ENGLISH = 'swiss', 'german', 'english'

    CHOICES = (
        (SWISS, SWISS),
        (GERMAN, GERMAN),
        (ENGLISH, ENGLISH)
    )


class ASSIGNMENT_STATUS(object):
    ARCHIVED, ACTIVE = 0, 1

    CHOICES = (
        (ARCHIVED, _("Archived")),
        (ACTIVE, _("Active")),
    )


class WORKLOGS_STATUS(object):
    NONE, BAD, MHM, TIPTOP, YEAH = 0, 1, 2, 3, 4

    CHOICES = (
        (NONE, _("None")),
        (BAD, _("***")),
        (MHM, _("Mhm")),
        (TIPTOP, _("Tiptop")),
        (YEAH, _("Yeah")),
    )


class ACCOUNTING_TYPES(object):
    """ The Accounting types are set for a Project. They will classify the Project to be either a profit or invest one."""
    INVEST = 'invest'
    PROFIT = 'profit'
    INTERNAL = 'internal'

    CHOICES = (
        (INVEST, _('invest')),
        (PROFIT, _('profit')),
        (INTERNAL, _('internal')),
    )


class REOCURRING_TYPES(object):
    """ The reocurring types set for the Project model.
    """

    NO, MONTHLY, QUARTERLY, SEMIANNUAL, ANNUAL = "no", "monthly", "quarterly", "semiannual", "annual"

    CHOICES = (
        (NO, _("No")),
        (MONTHLY, _("Monthly")),
        (QUARTERLY, _("Quarterly")),
        (SEMIANNUAL, _("Semiannual")),
        (ANNUAL, _("Annual")),
    )


class APPROVAL_STATUS(object):
    REQUESTED, APPROVED, DECLINED = 'requested', 'approved', 'declined'

    CHOICES = (
        (REQUESTED, _('Requested')),
        (APPROVED, _('Approved')),
        (DECLINED, _('Declined'))
    )
