class MetronomException(Exception):
    """Basic Metronom Exception"""


class MetronomMissingRateException(MetronomException):
    """No Rate was defined for this Step"""
