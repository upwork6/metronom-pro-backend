import re

from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db.models.fields import CharField, IntegerField
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField


def clean_uid(value):
    """Clean input from anything other then digits."""
    only_digits = re.findall(r'[0-9]+', value)
    digits_string = ''.join(only_digits)
    return digits_string


def check_uid_length(value):
    """Check the length of the UID."""
    if len(value) < 9:
        raise ValidationError(_('This is not a UID, there are numbers missing.'))
    elif len(value) > 9:
        raise ValidationError(_('This is not a UID, there are too many numbers.'))
    else:
        return _('valid_length')


def check_uid_checksum(value):
    """Check if the number is a valid UID according to checksum.

    Calculation from
    https://www.bfs.admin.ch/bfs/en/home/registers/enterprise-register/enterprise-identification/uid-general/uid.html

    The UID is composed of nine figures, the last of which is a control figure. It is calculated as follows:

    1. position * 5
    2. position * 4
    3. position * 3
    4. position * 2
    5. position * 7
    6. position * 6
    7. position * 5
    8. position * 4

    All the products of the multiplications are added together.
    The sum is then divided by 11.
    The remainder (Modulo 11) is subtracted from 11 and the result of this subtraction gives the control figure.
    If the result of the subtraction is "10", the UID is not allocated as the UID would not be valid.
    If the result of the division by 11 leaves no remainder, the figure 0 is given as control figure.
    """
    separated_positions = list(value)
    numbers = [int(number_string) for number_string in separated_positions]
    try:
        control_figure = numbers.pop(8)
    except IndexError:
        raise ValidationError(_('The numbers in the UID are not correct.'))
    MULTIPLICATORS = [5, 4, 3, 2, 7, 6, 5, 4]
    products = [(number * MULTIPLICATOR) for (number, MULTIPLICATOR) in zip(numbers, MULTIPLICATORS)]
    sum_of_products = sum(products)
    DIVISOR = 11
    remainder = sum_of_products % DIVISOR

    if remainder == 0:
        result = 0
    else:
        SUBTRACT_FROM = 11
        result = SUBTRACT_FROM - remainder

    if result == control_figure:
        return _('valid_checksum')
    else:
        raise ValidationError(_('The numbers in the UID are not correct.'))


class UIDField(CharField):
    """A field for the Swiss UID."""
    description = _("UID")
    default_validators = [check_uid_checksum, check_uid_length]

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 9
        super().__init__(*args, **kwargs)

    def to_python(self, value):
        if not value:
            return value
        return clean_uid(value)

    def from_db_value(self, value, *args, **kwargs):
        """Represent value according to official suggestion."""
        if not value:
            return value
        return f'CHE-{value[:3]}.{value[3:6]}.{value[6:]}'

    def formfield(self, **kwargs):
        """Allow for a bigger formfield."""
        kwargs['max_length'] = 20
        return super().formfield(**kwargs)


class LandlinePhoneNumberField(PhoneNumberField):
    pass


class MobilePhoneNumberField(PhoneNumberField):
    pass


class ChoicesCharField(CharField):
    """Custom CharField with choices flow validation"""
    description = _("Choices CharField With Assisted Flow")

    def validate(self, value, model_instance, **kwargs):
        """Validate choice workflow by the Rules set. Ex: `CREATED_RULES = [SENT, CANCELED]`"""
        db_instance_char_field = model_instance.db_instance
        if db_instance_char_field:
            if not self.choices.validate(getattr(db_instance_char_field, self.name), value):
                raise ValidationError(
                    _("%(message)s can't change state from %(old_value)s to %(new_value)s for %(class_name)s"),
                    code='invalid_choice_flow',
                    params={
                        'message': self.choices.error_msg,
                        'old_value': getattr(db_instance_char_field, self.name),
                        'new_value': self.choices.get_value(value),
                        'class_name': db_instance_char_field.__class__.__name__
                    },
                )
        super(ChoicesCharField, self).validate(value, model_instance)


class ChoicesIntegerField(IntegerField):
    """Custom IntegerField with choices flow validation"""
    description = _("Choices IntegerField With Assisted Flow")

    def validate(self, value, model_instance):
        """Validate choice workflow by the Rules set. Ex: `CREATED_RULES = [SENT, CANCELED]`"""

        db_instance_integer_field = model_instance.db_instance
        if db_instance_integer_field:
            if not self.choices.validate(getattr(db_instance_integer_field, self.name), value):
                raise ValidationError(
                    _("%(message)s can't change state to %(value)s"),
                    code='invalid_choice_flow',
                    params={
                        'message': self.choices.error_msg,
                        'value': self.choices.get_value(value)
                    },
                )
        super(ChoicesIntegerField, self).validate(value, model_instance)
