
from datetime import date, datetime

from django.contrib.contenttypes.models import ContentType
from django.db.models import Model
from rest_framework import serializers

from apps.projects.serializers import (ProjectActivitySerializer,
                                       ProjectActivityStepSerializer,
                                       ProjectArticleSerializer)


class GenericBatchUpdateSerializer(serializers.Serializer):
    """ This is serializer for the batch update of the objects. It use other serializers depending on
    the type of the object. To add a new model for the batch update you should update
    registered_batch_updates with lower case model name representation and serializer to use with it.

    """

    values = serializers.DictField()
    object_type = serializers.CharField()
    objects = serializers.ListField()
    registered_batch_updates = {
        "projectactivity": ProjectActivitySerializer,
        "projectarticle":  ProjectArticleSerializer,
        "projectactivitystep": ProjectActivityStepSerializer,
    }

    @classmethod
    def get_model_from_request(cls, request):
        serializer = cls.registered_batch_updates.get(request.data.get("object_type"))
        if serializer is not None:
            return serializer.Meta.model
        return

    def validate_object_type(self, data):
        if data not in self.registered_batch_updates:
            raise serializers.ValidationError(_("You can't update this type of object in batch."))
        return data

    def get_queryset(self, model_name, uuids):
        ct = ContentType.objects.get(model=model_name.lower())
        model = ct.model_class()
        qs = model.objects.filter(id__in=uuids)
        return qs

    def validate(self, data):
        object_serializer = self.registered_batch_updates.get(data["object_type"])
        qs = self.get_queryset(data["object_type"], data["objects"])
        self.serializers = []
        for instance in qs:
            serializer = object_serializer(data=data["values"], instance=instance, partial=True)
            serializer.is_valid(raise_exception=True)
            self.serializers.append(serializer)
        return data

    def to_representation(self, validated_data):
        return_dict = {}
        list_attrs_to_get = [x for x in validated_data["values"]]
        for serializer in self.serializers:
            instance = serializer.instance
            return_dict[str(instance.id)] = {}
            for key in list_attrs_to_get:
                value = getattr(instance, key, None)
                if isinstance(value, date):
                    value = value.strftime("%Y-%m-%d")
                elif isinstance(value, datetime):
                    value = value.strftime("%Y-%m-%d %H:%M:%S")
                elif issubclass(value.__class__, Model):
                    value = {"uuid": str(value.id)}
                return_dict[str(instance.id)][key] = value
        return return_dict

    def update(self):
        for serializer in self.serializers:
            serializer.save()
