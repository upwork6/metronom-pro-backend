from corsheaders.conf import conf
from corsheaders.middleware import (ACCESS_CONTROL_ALLOW_HEADERS,
                                    ACCESS_CONTROL_ALLOW_ORIGIN,
                                    CorsMiddleware)


class MetronomCorsMiddleware(CorsMiddleware):
    """This middleware add ACCESS_CONTROL_ALLOW_HEADERS in response for every HTTP-method
    instead only OPTIONS (CorsMiddleware add only for OPTIONS method)."""

    def process_response(self, request, response):
        """
        Add the respective CORS headers
        """
        response = super(MetronomCorsMiddleware, self).process_response(request, response)
        if ACCESS_CONTROL_ALLOW_ORIGIN in response:
            response[ACCESS_CONTROL_ALLOW_HEADERS] = ', '.join(conf.CORS_ALLOW_HEADERS)
        return response
