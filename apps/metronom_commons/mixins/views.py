
from django.db.models.query import Q
from django.utils.translation import ugettext_lazy as _

from rest_framework.serializers import ValidationError
from queryset_sequence.pagination import SequenceCursorPagination
from rest_framework.response import Response
from queryset_sequence import QuerySetSequence


class SerializerDispatcherMixin(object):
    """
    Mixin used to configure custom serializer based on the type of the action field.
    For instance, by configuring the serializers_dispatcher like this:

    serializers_dispatcher = {
        "retrieve": MyRetrieveSerializer,
    }
    It is possible to instruct the ViewSet to use the MyRetrieveSerializer when the retrieve action is used.

    Note: As all the mixins, remember to list this, in the super class list, before the ModelViewSet or whichever
    other class implements the get_serializer_class method.
    """
    serializers_dispatcher = dict()

    def get_serializer_class(self):
        return self.serializers_dispatcher.get(self.action, super().get_serializer_class())


class DeleteForbiddenIfUsedMixin(object):
    """ This mixin exists to provide a generic solution for the situations when we need to allow delete
    of the object, but forbid to do this if this object used somewhere.
    To make this possible, we need to provide extra dict inside the ViewSet:
    forbid_delete_if_used_in.
    And optional query for extra filtration:
    get_forbid_delete_qs_modelname_lower
    Example:
    forbid_delete_if_used_in = {
        TemplateActivity: ['article_group']
    }

    def get_forbid_delete_qs_templateactivity(self):
        return TemplateActivity.objects.filter(status='active')

    """

    def perform_destroy(self, obj):
        if getattr(self, "forbid_delete_if_used_in", None) is not None:
            for model, keys in self.forbid_delete_if_used_in.items():
                lower_name = model.__name__.lower()
                model_attr_name = "get_forbid_delete_qs_%s" % lower_name
                qs = getattr(self, model_attr_name, None)
                if qs is not None:
                    qs = qs()
                else:
                    qs = model.objects
                for key in keys:
                    if qs.filter(**{key: obj}).exists():
                        raise ValidationError(_(f"You can't delete used {obj.__class__.__name__}"))
        return super().perform_destroy(obj)


class CombinedListMixin(object):
    """
        Mixin for combining lists, it is used for the /projects API (projects + campaigns) and in the CRM (Persons + Companies + Users)

        Example:
                class ProjectViewSet(CombinedListMixin, ..., ModelViewSet):


                    combined_lists = [
                    {
                        "instance_type": Project,
                        "queryset": Project.objects.all(),
                        "object_filter": ProjectFilter,
                        "serializer_class": ProjectListSerializer,
                        "order_by": "created_at",
                    }, {
                        "instance_type": Campaign,
                        "queryset": Campaign.objects.all(),
                        "object_filter": CampaignFilter,
                        "serializer_class": CampaignListSerializer,
                        "order_by": "created_at",
                        "specific_filters": {
                            'type': 'campaign'
                        }
                    }
                ]

    """

    def get_queryset(self):

        object_querysets = []
        order_bys = []

        for object_list in self.combined_lists:
            queryset = object_list["queryset"]
            serializer_class = object_list["serializer_class"]
            specific_filters = object_list["specific_filters"]

            if hasattr(serializer_class, 'setup_eager_loading'):
                queryset = serializer_class.setup_eager_loading(queryset)

            # With specific_filters one can define filters, which only work one of the given querysets
            for filter in specific_filters:
                if filter in self.request.query_params:
                    if self.request.query_params.get(filter, None) == specific_filters[filter] or specific_filters[filter] == "*":
                        return queryset

            order_bys.append(object_list["order_by"])
            object_querysets.append(queryset)

        return QuerySetSequence(*object_querysets).order_by(*order_bys)
