from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import uuid


class CloneMixin(models.Model):
    """
    This Mixin return an identical instance of the model that inherits it
    - `clone(**kwargs)` => is capable to take `**kwargs` to overwrite the fields before saving the model
    - kwargs => dict([
        ("field_1", <ProjectActivity: Activity Title>),
        ("field_2", "Changed value 2"),
        ("field_3", "Changed value 3")
    ]) OR {'field_1': <ProjectActivity: Activity Title>, 'field_2': 'Changed value 2', 'field_3': Changed value 3'}
    Limitations:
        Is not able to handle ManyToMany Fields, only DB Fields, ForeignKey, and OneToOneField
    """

    @property
    def db_instance(self):
        try:
            db_self = self.__class__.objects.get(id=self.id)
        except self.__class__.DoesNotExist:
            db_self = None
        return db_self

    def clone(self, **kwargs):
        field_list = list()
        for field in self._meta.fields:
            if field.name != 'id':
                if field.name in kwargs.keys():
                    field_list.append((field.name, kwargs.get(field.name, getattr(self, field.name))))
                else:
                    field_list.append((field.name, getattr(self, field.name)))
        clone = self.__class__.objects.create(**dict(field_list))
        if clone.pk != self.pk:
            return clone
        return None

    class Meta:
        abstract = True


class ReplaceMixin(CloneMixin):
    """ Class that inherits the CloneMixin => creates an identical instance
    - It is used for the special case of `replacing` an Offer, ProjectActivity|Article, Activity|ArticleOffered
    - Contains a `OneToOneField` `replaced_item` with a reverse lookup `replaced_by`
    to keep track of which is replacing which:
        - `model_instance.replaced_item` => the Old Item (that current one just replaced it)
        - `model_instance.replaced_by` => the New Item (that replaced the old one)
    """
    replaced_item = models.OneToOneField(
        'self',
        related_name='replaced_by',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    class Meta:
        abstract = True


class ProtectedFieldsMixin(models.Model):
    """Mixin to protect some of the ModelFields to be modified based on some other StatusField.
    I created 3 methods for the required 3 attributes so that I could change or custom separately the result.
    Required:
        - `self.db_instance` which is made us available through `CloneMixin`
        - self.PROTECTED_LIST => Fields we need to Protect: ==Note== Django Models have more "fields" inside
        `Model._meta.get_fields()` so it is easier to track fields that are protected than fields that are editable
        - self.PROTECTED_STATES => the list of Value/s when the Protection will take place
        - self.STATUS_FIELD_NAME => The name of the Field to check for it's value inside PROTECTED_STATES
            - can support also nested lookup like: `project.activity.activity_step.status`

    Checks for `is_editable()` method:
        - True => `save()` the instance
        - False => `raise ValidationError`
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.PROTECTED_LIST = self.get_protected_list()
        self.PROTECTED_STATES = self.get_protected_states()
        self.STATUS_FIELD_NAME = self.get_status_field_name()

    class Meta:
        abstract = True

    def get_protected_list(self):
        """ Get the list of field names to protect from inherited Classes OR return an Empty list
        :return: list(str)
        :rtype:
        """
        if hasattr(self.__class__, 'PROTECTED_LIST'):
            return self.__class__.PROTECTED_LIST
        return []

    def get_protected_states(self):
        """ Get the list of `field values` of the `STATUS_FIELD_NAME` when the protection will take place
        from inherited Classes OR return an Empty list
        :return: list(str)
        :rtype:
        """
        if hasattr(self.__class__, 'PROTECTED_STATES'):
            return self.__class__.PROTECTED_STATES
        return []

    def get_status_field_name(self):
        """ Get the `field name` to check if the protection has to be applied from inherited Classes
            OR return `pk` by default
        :return: str
        :rtype:
        """
        if hasattr(self.__class__, 'STATUS_FIELD_NAME'):
            return self.__class__.STATUS_FIELD_NAME
        return 'pk'

    def get_status_field_value(self):
        """
        Lookup for the Value of the Field that decides if the Instance is Protected or not.
        I opted to go only 3 levels deep for `getattr()` and to do it in one go (for more performance) each time
        instead of looping and returning each time the parent of the nex `getattr()` method.
        :return: STATUS_FIELD_NAME VALUE => getattr(instance, self.STATUS_FIELD_NAME)
        :rtype: str || int
        """
        status_field_value = None
        if self.STATUS_FIELD_NAME:
            lookup_attributes = self.STATUS_FIELD_NAME.split('.')
            if len(lookup_attributes) == 1:
                status_field_value = getattr(self.db_instance, self.STATUS_FIELD_NAME)
            elif len(lookup_attributes) == 2:
                status_field_value = getattr(getattr(self.db_instance, lookup_attributes[0]), lookup_attributes[1])
            elif len(lookup_attributes) == 3:
                status_field_value = getattr(
                    getattr(getattr(self.db_instance, lookup_attributes[0]), lookup_attributes[1]),
                    lookup_attributes[2]
                )
        return status_field_value

    @property
    def is_editable(self):
        """
        Check if the condition of Protection is True or False
        :return: Boolean
        :rtype:
        """

        model_fields = [field.name for field in self._meta.get_fields()]
        # Check if STATUS_FIELD value is part of the PROTECTED_STATES values
        if self.db_instance:
            if self.get_status_field_value() and self.get_status_field_value() in self.PROTECTED_STATES:
                # loop through the Protected Fields from PROTECTED_LIST instead of all the Model Fields
                # and compare their New Values with Old Values from db_instance
                for protected_field in self.PROTECTED_LIST:
                    if protected_field in model_fields and \
                            getattr(self.db_instance, protected_field, None) != getattr(self, protected_field, None):
                        return False
        return True

    def save(self, *args, **kwargs):
        if not self.is_editable:
            raise ValidationError(
                {self.STATUS_FIELD_NAME.split('.')[0]:
                     _(f'<{self.__class__.__name__}>: "{self.__class__.__str__(self)}" is not editable while'
                       f' "{self.STATUS_FIELD_NAME}" is "{self.get_status_field_value()}".')}
            )
        super().save(*args, **kwargs)


class UUIDModel(models.Model):
    """Use a UUID Field as a primary ID."""

    created_at = models.DateTimeField(_('Created'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Modified'), auto_now=True)
    id = models.UUIDField(_('ID'), primary_key=True, unique=True, default=uuid.uuid4, editable=False)

    class Meta:
        abstract = True
