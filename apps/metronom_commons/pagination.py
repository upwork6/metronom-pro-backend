from rest_framework.pagination import PageNumberPagination

from apps.backoffice.utils import get_metronom_setting


class MetronomPageNumberPagination(PageNumberPagination):
    page_size = get_metronom_setting('DEFAULT_PAGE_SIZE')
    page_size_query_param = 'page_size'
    max_page_size = get_metronom_setting('DEFAULT_MAX_PAGE_SIZE')
