
from allauth.account.models import EmailAddress
from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.utils import jwt_decode_handler

from apps.users.models import ROLE
from apps.users.tests.factories import UserFactory

User = get_user_model()


class APIRestAuthJWTClient(APIClient):

    def login(self, login_name='rest_login', **credentials):
        """
        Returns True if login is possible; False if the provided credentials
        are incorrect, or the user is inactive.
        """
        login_endpoint = reverse(login_name)
        login_response = self.post(login_endpoint, credentials, format='json')
        if login_response.status_code == status.HTTP_200_OK:
            self.credentials(
                HTTP_AUTHORIZATION="{0} {1}".format(api_settings.JWT_AUTH_HEADER_PREFIX, login_response.data['token']))
            return login_response
        else:
            return None


class MetronomBaseAPITestCase(APITestCase):
    """
        TestCase for API handling

        - Adds authenticated and login to test classes

    """

    maxDiff = None
    client_class = APIRestAuthJWTClient

    @classmethod
    def verify_user_email(cls, user):
        verification, created = EmailAddress.objects.get_or_create(user=user, email=user.email)
        verification.verified = True
        verification.set_as_primary()
        verification.save()

    @classmethod
    def assertErrorResponse(cls, response, message, expected_status_code=status.HTTP_400_BAD_REQUEST):
        try:
            assert response.status_code == expected_status_code
        except AssertionError as e:
            print(f"\033[93m {response.status_code} not equal to {expected_status_code} \033[0m")
            raise e
        try:
            assert response.data['errors'] == message
        except AssertionError as e:
            print(f"\033[93m {response.data['errors']} not equal to {message} \033[0m")
            raise e

    @classmethod
    def assertInErrorResponse(cls, response, messages, expected_status_code=status.HTTP_400_BAD_REQUEST):
        assert response.status_code == expected_status_code

        for key, value in messages.items():
            assert key in response.data['errors']
            assert response.data['errors'][key] == value

    @classmethod
    def assertQuerysetsEqual(cls, qs1, qs2):
        """ self.assertQuerysetEqual did not work out, using own logic for now """
        assert list(sorted(qs1)) == list(sorted(qs2))

    @classmethod
    def setUpTestData(cls):
        """
            Load initial data for the TestCase
        """

        # Some defaults
        cls.default_password = 'secret123'

        # Create test users
        cls.test_user = UserFactory(email='user@test.com', first_name="John",
                                    password=cls.default_password, role=ROLE.USER)
        cls.test_hr_user = UserFactory(email='hr_user@test.com', first_name="John",
                                       password=cls.default_password, role=ROLE.HR)
        cls.test_accounting_user = UserFactory(email='accounting_user@test.com', first_name="John", password=cls.default_password,
                                               role=ROLE.ACCOUNTING)
        cls.test_backoffice_user = UserFactory(email='backoffice_user@test.com', first_name="John", password=cls.default_password,
                                               role=ROLE.BACKOFFICE)

        cls.test_admin = get_user_model().objects.create_superuser(
            username="admin",
            first_name="John",
            email="admin@test.com",
            password=cls.default_password,
            role=ROLE.ADMIN
        )

        cls.user_client = APIRestAuthJWTClient()
        cls.admin_client = APIRestAuthJWTClient()
        cls.hr_client = APIRestAuthJWTClient()
        cls.backoffice_client = APIRestAuthJWTClient()
        cls.accounting_client = APIRestAuthJWTClient()

        # Verification of the Users

        cls.verify_user_email(cls.test_user)
        cls.verify_user_email(cls.test_hr_user)
        cls.verify_user_email(cls.test_accounting_user)
        cls.verify_user_email(cls.test_backoffice_user)

        cls.login_as_user(cls.hr_client, cls.test_hr_user)
        cls.login_as_user(cls.accounting_client, cls.test_accounting_user)
        cls.login_as_user(cls.backoffice_client, cls.test_backoffice_user)
        cls.login_as_user(cls.admin_client, cls.test_admin)
        cls.login_as_user(cls.user_client, cls.test_user)

    def setUp(self):
        """
            Running tests will modify class attributes. We need to reload attributes, which could have been modified in tests.
            As those are read operations, it still has a better performance.
        """
        self.test_user.refresh_from_db()
        # TODO: Is this still needed?

    @classmethod
    def login_as_user(cls, client=None, user=None):
        if client is None:
            client = cls.user_client

        if not user:
            user = cls.test_user

        credentials = {"email": user.email, "password": cls.default_password}
        cls.login_response = client.login(**credentials)

    @classmethod
    def login_as_hr_user(cls):
        cls.login_as_user(cls.hr_client, cls.test_hr_user)

    @classmethod
    def login_as_accounting_user(cls):
        cls.login_as_user(cls.accounting_client, cls.test_accounting_user)

    @classmethod
    def login_as_backoffice_user(cls):
        cls.login_as_user(cls.backoffice_client, cls.test_backoffice_user)

    @classmethod
    def login_as_admin(cls):
        cls.login_as_user(cls.admin_client, cls.test_admin)

    @classmethod
    def decode_jwt(cls):

        if not hasattr(cls, 'login_response'):
            raise AssertionError('You must call a login-method before accessing the decoded JWT')

        decoded_data = jwt_decode_handler(cls.login_response.data['token'])

        return decoded_data
