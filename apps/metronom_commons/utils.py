
from django import forms


def pop_create(base_data, key, model):
    """Create a new instance from data provided under key or return None; pop data from base_data."""
    data = base_data.pop(key, None)
    if data:
        if not isinstance(data, dict):
            data = {key: data}
        return model.objects.create(**data)
    else:
        return None


def update_or_create(instance, key, values):
    """Create a new instance or updates the existing one.

    - It uses an existing instance and overwrite the attribute
    - It creates an instance if not existing yet
    """

    existing_related_instance = getattr(instance, key)

    if existing_related_instance is None:
        fk_field = instance._meta.get_field(key)
        fk_model_cls = fk_field.related_model

        new_related_instance = pop_create(values, key, fk_model_cls)

        setattr(instance, key, new_related_instance)
    else:
        value = values.get(key)
        if isinstance(value, dict):
            for attr_name, value in value.items():
                setattr(existing_related_instance, attr_name, value)
        else:
            setattr(existing_related_instance, key, value)

        existing_related_instance.save()


def update_or_create_all_fk(instance, fk_keys: list, values: dict):
    """Create a new instance or updates the existing one for all field names (fk_keys).

    - It uses an existing instance and overwrite the attribute
    - It creates an instance if not existing yet
    """

    for key in fk_keys:
        value = values.get(key, None)
        if value:
            update_or_create(instance, key, values)


def cast_bool(boolean_string):
    """ Python bool() does not help with Booleans, we are casting Strings into boolean """

    if boolean_string:
        if boolean_string.lower() in ["true", "yes", "1"]:
            return True
        if boolean_string.lower() in ["false", "no", "0"]:
            return False
    return None


class CachingModelChoicesFormSet(forms.BaseInlineFormSet):
    """
    Used to avoid duplicate DB queries by caching choices and passing them all the forms.
    To be used in conjunction with `CachingModelChoicesForm`.
    https://stackoverflow.com/questions/40665770/django-inline-for-manytomany-generate-duplicate-queries/44932400#44932400
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        sample_form = self._construct_form(0)
        self.cached_choices = {}
        try:
            model_choice_fields = sample_form.model_choice_fields
        except AttributeError:
            pass
        else:
            for field_name in model_choice_fields:
                if field_name in sample_form.fields and not isinstance(
                        sample_form.fields[field_name].widget, forms.HiddenInput):
                    self.cached_choices[field_name] = [c for c in sample_form.fields[field_name].choices]

    def get_form_kwargs(self, index):
        kwargs = super().get_form_kwargs(index)
        kwargs['cached_choices'] = self.cached_choices
        return kwargs


class CachingModelChoicesForm(forms.ModelForm):
    """
    Gets cached choices from `CachingModelChoicesFormSet` and uses them in model choice fields in order to reduce
    number of DB queries when used in admin inlines.
    https://stackoverflow.com/questions/40665770/django-inline-for-manytomany-generate-duplicate-queries/44932400#44932400
    """

    @property
    def model_choice_fields(self):
        return [fn for fn, f in self.fields.items()
                if isinstance(f, (forms.ModelChoiceField, forms.ModelMultipleChoiceField,))]

    def __init__(self, *args, **kwargs):
        cached_choices = kwargs.pop('cached_choices', {})
        super().__init__(*args, **kwargs)
        for field_name, choices in cached_choices.items():
            if choices is not None and field_name in self.fields:
                self.fields[field_name].choices = choices


def all_statuses_are_the_same(status_list):
    """
    This method is necessary when we have a [list of values - statuses] like
    [ActivityOffered.offer_status + ArticleOffered.offer_status] and we need to check if all the values in that list
     are the same or not to know what to do after that(change also the status for the Offer itself)
    :param status_list: [list of values - statuses]
    :type status_list: list(str)
    :return: True | False
    :rtype: Boolean
    """
    previous_status = None
    for status in status_list:
        if status == previous_status or previous_status is None:
            previous_status = status
        else:
            return False
    return True