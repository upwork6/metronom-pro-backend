from django.utils.translation import ugettext_lazy as _
from apps.metronom_commons.choices_flow import ActivityArticleOfferedStatus
from decimal import Decimal
from apps.backoffice.utils import get_setting
from apps.backoffice.models import TermsCollection


def get_amount_from_money(money_instance):
    normalized_amount = round(Decimal(money_instance.amount), 2)
    return str(normalized_amount)


class ProjectOfferPDFUtils(object):

    def __init__(self, project_offer=None):
        self.project_offer = project_offer

    def get_project_offer_dictionary(self):
        """Generate the dictionary to be stroed in the DB (for later) AND/OR
        to be used inside CampaignOfferPDF  AND/OR
        to be used to compose the full context to render the PDF ProjectOffer document
        :return: dict
        :rtype:
        """
        project_offer_dictionary = {
            "title": self.project_offer.name,
            "public_id": self.project_offer.public_id,
            "public_id_text": _("Offer"),
            "optional": self.project_offer.is_optional,
            "optional_text": _("Option"),
            "customer_reference": self.project_offer.used_customer_reference,
            # text as html {{ footer.text|safe }}
            "description": self.project_offer.offer_description,
            "total_optional": get_amount_from_money(self.project_offer.total_optional_costs),
            "total_optional_text": _("Total Optional Cost"),
            "total_external": get_amount_from_money(self.project_offer.total_external_costs),
            "total_external_text": _("Total External Cost"),
            "total_tax": get_amount_from_money(self.project_offer.total_tax),
            "is_tax_included": self.project_offer.is_tax_included,
            "total_tax_text": _("MwSt. [Werte %]"),
            "tax_rate": f"{self.project_offer.used_tax_rate}%",
            "total_offer": get_amount_from_money(self.project_offer.get_total_offer_send_to_customer()),
            "total_offer_text": _(f"Total {self.project_offer.currency} inkl. MwSt."),
            "discount_explanation": f"The total Discount of {self.project_offer.currency} "
                                    f"{get_amount_from_money(self.project_offer.total_discounts)} "
                                    f"is deducted from the Total Offer.",
            "services_title": _("Positions"),
            "services": self.get_project_offer_positions_list()
        }
        return project_offer_dictionary

    def get_position_steps(self, position):
        """Creates additional text phrase inside ActivityOffered(Offer Position) Description"""
        if position.__class__.__name__ == 'ActivityOffered':
            if position.show_steps and position.project_activity.activity_steps.count():
                all_steps = position.project_activity.activity_steps.all()
                position_steps_text_first_phrase = "Non Binding Estimate: "
                currency = self.project_offer.currency
                position_steps_text_last_phrase = ", ".join([
                    f"{step.name} {step.planned_effort_in_hours}h at "
                    f"{currency} {get_amount_from_money(step.calculate_planned_budget_for_currency(currency))}"
                    for step in all_steps
                ])
                return position_steps_text_first_phrase + position_steps_text_last_phrase
        return None

    def get_accepted_text(self, position):
        """If a Position is Accepted will be showed like  => `Re-Design Website (Accepted - 245.04.2018)`"""
        if position.get_final_state() and \
                position.get_final_state_information().get('offer_status') == ActivityArticleOfferedStatus.ACCEPTED:
            accepted_date = position.get_final_state_information().get("date")
            status_text_in_front_of_positions_title = {
                "accepted_text": _("Accepted"),
                "accepted_date": accepted_date.strftime("%Y-%m-%d %H:%M:%S")
            }
            return status_text_in_front_of_positions_title
        return None

    def get_project_offer_positions_list(self):
        """Creates the Offer Positions => ActivityOffered + ArticleOffered as Dictionaries
        :returns list(dict)
        """
        services_list = [
            {
                "is_optional": position.is_optional,
                "position": position.position,
                "title": position.name,
                "is_optional_text": _("Option"),
                "is_external_cost": position.is_external_cost,
                "is_external_cost_text": _("External Cost"),
                "total_amount": get_amount_from_money(position.offered_sum_with_discount),
                "discount": get_amount_from_money(position.discount),
                "accepted": self.get_accepted_text(position),
                "description": position.description,
                "steps": self.get_position_steps(position)
            } for position in self.project_offer.all_items
        ]
        return sorted(services_list, key=lambda x: x['position'], reverse=True)

    def get_offer_metadata(self):
        """META CONTENT - Necessary as an Offer PDF Document but not inside a CampaignOffer PDF
        :returns dict
        """
        agency_address_header = _("PDF address headline must be set, please fix in Settings")
        footer_text = _("PDF footer must be set, please fix in Settings")
        agency_address_header = get_setting('PDF_ADDRESS_HEADLINE')
        footer_text = get_setting('PDF_FOOTER')

        offer_metadata = {
            "header": {
                "image": "html2pdf/img/pp-logo.svg",
                "text": "",
            },
            "footer": {
                "image": "",
                # text redered as html {{ footer.text|safe }}
                "text": str(footer_text),
            },
            "currency": self.project_offer.currency,
            "locale": "en",
            "agency_address_header": str(agency_address_header),
            "customer": {
                "contact_person_title": self.project_offer.contact_person.manner_of_address,
                "contact_person_name": self.project_offer.contact_person.name,
            },
            "offer": {
                "offer_date": self.project_offer.offer_date.strftime("%Y-%m-%d %H:%M:%S"),
                "valid_until_date": self.project_offer.valid_until_date.strftime("%Y-%m-%d %H:%M:%S"),
                "manager": self.project_offer.offer_manager.name,
            }
        }
        return offer_metadata

    def get_project_offer_complete_dictionary(self):
        """
        Compose a full Context necessary to render a DPF Offer Document
        :return: dict
        :rtype:
        """
        project_offer_complete_dictionary = {
            **self.get_offer_metadata(),
            **self.get_project_offer_dictionary()
        }
        return project_offer_complete_dictionary


class CampaignOfferPDFUtils(object):

    def __init__(self, campaign_offer=None):
        self.campaign_offer = campaign_offer

    def get_campaign_offer_dictionary(self):
        """Generate the dictionary to be stroed in the DB (for later) AND/OR
        to be used to Compose the full context to render the PDF CampaignOffer document
        :return: dict
        :rtype:
        """
        campaign_offer_total_tax = self.campaign_offer.get_total_tax()
        campaign_offer_dictionary = {
            "title": self.campaign_offer.title,
            "public_id": self.campaign_offer.public_id,
            "public_id_text": _("Campaign Offer"),
            "optional": False,
            "optional_text": _("Option"),
            "customer_reference": self.campaign_offer.used_customer_reference,
            # text as html {{ footer.text|safe }}
            "description": self.campaign_offer.description,
            "total_optional": get_amount_from_money(self.campaign_offer.total_optional_costs),
            "total_optional_text": _("Total Optional Cost"),
            "total_external": get_amount_from_money(self.campaign_offer.total_external_costs),
            "total_external_text": _("Total External Cost"),
            "total_tax": str(campaign_offer_total_tax),
            "is_tax_included": self.campaign_offer.is_tax_included,
            "total_tax_text": _("MwSt. [Werte %]"),
            "tax_rate": f"{self.campaign_offer.tax}%",
            "total_offer": get_amount_from_money(self.campaign_offer.get_total_campaign_offer_send_to_customer()),
            "total_offer_text": _(f"Total {self.campaign_offer.currency} inkl. MwSt."),
            "discount_explanation": f"The total Discount of {self.campaign_offer.currency} "
                                    f"{get_amount_from_money(self.campaign_offer.total_discounts)} "
                                    f"is deducted from the Total Offer.",
            "projects_title": "Project-Offer",
            "projects": self.get_project_offers(),
        }
        return campaign_offer_dictionary

    def get_project_offers(self):
        """
        Convert ProjectOffer model objects into Dictionaries
        :return: list(dict)
        :rtype:
        """
        if self.campaign_offer.project_offers.all():
            project_offers = [
                ProjectOfferPDFUtils(project_offer=project_offer).get_project_offer_dictionary()
                for project_offer in self.campaign_offer.project_offers.all()
            ]
            return project_offers
        return []

    def get_campaign_offer_metadata(self):
        """META CONTENT - Necessary to create a CampaignOffer PDF Document but not when for saving inside DB
        :returns dict
        """
        agency_address_header = str(get_setting('PDF_ADDRESS_HEADLINE'))
        footer_text = str(get_setting('PDF_FOOTER'))
        offer_metadata = {
            "header": {
                "image": "html2pdf/img/pp-logo.svg",
                "text": "",
            },
            "footer": {
                "image": "",
                "text": str(footer_text),
            },
            "currency": self.campaign_offer.currency,
            "locale": "en",
            "agency_address_header": str(agency_address_header),
            "customer": {
                "contact_person_title": self.campaign_offer.contact_person.manner_of_address,
                "contact_person_name": self.campaign_offer.contact_person.name,
            },
            "offer": {
                "offer_date": self.campaign_offer.offer_date.strftime("%Y-%m-%d %H:%M:%S"),
                "valid_until_date": self.campaign_offer.valid_until_date.strftime("%Y-%m-%d %H:%M:%S"),
                "manager": self.campaign_offer.offer_manager.name,
            }
        }
        return offer_metadata

    def get_campaign_offer_complete_dictionary(self):
        """
        Put together All the pieces necesary for a full Context to render CampaignOffer PDF
        MetaContent + live content + CampaignOffer Dictionary from DB OR created on the spot
        :return: dict
        :rtype:
        """
        campaign_offer_complete_dictionary = {
            **self.get_campaign_offer_metadata(),
            **self.get_campaign_offer_dictionary()
        }
        return campaign_offer_complete_dictionary


class TermsCollectionPDFUtils(object):
    """Creates the required structure necessary to render the Terms & Conditions inside Offer PDF Documents"""

    def __init__(self, terms_collection=None):
        self.terms = self.get_terms_collection_object(terms_collection)

    @staticmethod
    def get_terms_collection_object(terms_collection=None):
        if isinstance(terms_collection, TermsCollection):
            return terms_collection
        raise AttributeError({"terms_collection": _("Terms Collection is required.")})

    def get_terms_collection_dictionary(self):
        """
        Creates the a Python Dictionary from TermsCollectionObject necessary PDF rendering
        :return: TermsCollection dict
        :rtype: dict
        """
        paragraphs = self.get_terms_paragraphs()
        terms_collection_dictionary = {
                "title": _("General Terms of Business"),
                # TODO: Decide if `terms_date` will be: `created_date` OR updated_date`
                "terms_date": self.terms.created_at.strftime("%Y-%m-%d %H:%M:%S"),
                "paragraphs": paragraphs
            }
        return terms_collection_dictionary

    def get_terms_paragraphs(self):
        terms_paragraph_list = [
            {
                "title": paragraph.name,
                "text": paragraph.text,
                # TODO: get `position` field value for this specific relation
                "position": paragraph.terms_paragraphs.filter().last().position
            } for paragraph in self.terms.terms.all()
        ]
        return sorted(terms_paragraph_list, key=lambda x: x['position'], reverse=True)
