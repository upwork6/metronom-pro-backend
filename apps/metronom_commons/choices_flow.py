""" Choices Flow is designed to handle the Choices:
    `CHOICES = {(1: _("Option 1"), (2: _("Option 2"), (3: _("Option 3")}` and the Rules:
    `RULES` on how to handle `allow` or `reject(ValidationError)` the next `choice`

    How it works:
    The Choice Flow Validation is doing all these when the method `full_clean` method is called.
    We have 2 options:
    1. every time when we save the model before that we call `full_clean()` to check the `status`.
     - The downside of this is that you can forget about this extra check
     and save the models with the wrong status.
    2. overwrite the save method so every time when saving the model
     the method `full_clean()` will be called before.
    - This way you avoid forgetting the status check to be called but...
    in case we edit this through a Form...like in Django Admin
     the method `.full_clean()` will be called twice:
     (once inside ModelAdmin validation and second in our overwritten `save()` method)
"""
try:
    from django.conf import settings
except ImportError:
    settings = None
from django.utils.translation import ugettext_lazy as _

# try to get `CHOICES_FLOW_ERROR_MESSAGE` from `settings` first if it is setup
if hasattr(settings, 'CHOICES_FLOW_ERROR_MESSAGE'):
    CHOICES_FLOW_ERROR_MESSAGE = settings.CHOICES_FLOW_ERROR_MESSAGE
else:
    CHOICES_FLOW_ERROR_MESSAGE = _('Invalid choice:')


class MetaChoice(type):
    """- Convert attributes to Tuples (if it is all ready done)
       - Add an iterator in class attributes
    """

    def __init__(cls, *args, **kwargs):
        cls._rules = {}
        cls._data = []
        cls.__validate_msg = CHOICES_FLOW_ERROR_MESSAGE

        items = cls.__dict__.items()
        for name, value in list(items):
            cls._set_data(name, value)
            try:
                cls._hash = dict(cls._data)
            except TypeError:
                raise TypeError('Invalid Choice: %s' % cls._data)

    def _set_data(cls, name, value):
        # Update tuples to objects
        if not name.startswith('_') and not callable(value):
            if isinstance(value, tuple) and len(value) > 1:
                data = value
            else:
                data = (value, name)

            if isinstance(value, list) and '_RULES' in name.upper():
                cls._set_rules(name, value)
            else:
                cls._data.append(data)
                if isinstance(data[0], tuple):
                    setattr(cls, name, dict(data))
                else:
                    setattr(cls, name, data[0])

    def _set_rules(cls, name, value):
        # If attribute has _RULES add validations rules
        updated_name = name.rsplit('_', 1)[0]
        value_key = getattr(cls, updated_name)

        if isinstance(value_key, tuple):
            value_key = value_key[0]

        values = [isinstance(i, tuple) and i[0] or i for i in value]
        cls._rules.update({value_key: values})

    def __iter__(cls, *args, **kwds):
        for value, data in cls._data:
            yield (value, data)

    def __len__(cls, *args, **kwds):
        return len(cls._data)

    def __repr__(cls, *args, **kargs):
        return str(list(cls._data))

    @property
    def error_msg(cls):
        # get default error message
        return cls.__validate_msg

    @error_msg.setter
    def error_msg(cls, value):
        # set default error message for instance
        cls.__validate_msg = value

    def get_value(cls, key):
        return cls._hash.get(key)

    def validate(cls, status, new_status):
        # Validate workflow
        if not status:
            return new_status

        if repr(status) == repr(new_status):
            return new_status

        if not cls._rules.get(status) or new_status not in cls._rules.get(status) or new_status == status:
            return False
        return new_status


Choices = MetaChoice('Choices', (object, ), {})


# Only neutral place I could find to avoid circular imports between Projects and Offers
# Make sense to keep here all the `ModelChoices` Done handled by "Choices" class
class ActivityArticleOfferedStatus(Choices):

    CREATED = ('created', _("Created"))
    SENT = ('sent', _("Sent"))
    ACCEPTED = ('accepted', _("Accepted"))
    DECLINED = ('declined', _("Declined"))
    REPLACED = ('replaced', _("Replaced"))
    IMPROVABLE = ('improvable', _("Improvable"))
    IMPROVED = ('improved', _("Improved"))

    # set Flow rules for activity/ArticleOffered.offer_status
    # Ex: `SENT_RULES` means: from `SENT` status You can only go to: `[ACCEPTED, DECLINED]`
    CREATED_RULES = [SENT]
    SENT_RULES = [ACCEPTED, DECLINED, REPLACED]
    ACCEPTED_RULES = []
    DECLINED_RULES = []
    REPLACED_RULES = []
    IMPROVABLE_RULES = [IMPROVED, SENT]
    IMPROVED_RULES = [SENT]


# Only neutral place I could find to avoid circular imports between Projects and Offers
# Make sense to keep here all the `ModelChoices` Done handled by "Choices" class
class OfferStatus(Choices):
    """
    How it works:
    The Choice Flow Validation is doing all these when the method `full_clean` method is called.
    We have 2 options:
    1. every time when we save the model before that we call `full_clean()` to check the `status`.
     - The downside of this is that you can forget about this extra check
     and save the models with the wrong status.
    2. overwrite the save method so every time when saving the model
     the method `full_clean()` will be called before.
    - This way you avoid forgetting the status check to be called but...
    in case we edit this through a Form...like in Django Admin
     the method `.full_clean()` will be called twice:
     (once inside ModelAdmin validation and second in our overwritten `save()` method)
    """

    CREATED = ('created', _("Created"))
    SENT = ('sent', _("Sent"))
    ACCEPTED = ('accepted', _("Accepted"))
    PARTLY_ACCEPTED = ('partly_accepted', _("Partly Accepted"))
    DECLINED = ('declined', _("Declined"))
    REPLACED = ('replaced', _("Replaced"))

    # set Flow rules for Offer.status
    # Ex: `SENT_RULES` means: from `SENT` status You can only go to: `[ACCEPTED, DECLINED]`
    CREATED_RULES = [SENT]
    SENT_RULES = [ACCEPTED, DECLINED, REPLACED, PARTLY_ACCEPTED]
    ACCEPTED_RULES = []
    PARTLY_ACCEPTED_RULES = [REPLACED]
    # remove SENT after implementation
    DECLINED_RULES = []
    REPLACED_RULES = []
