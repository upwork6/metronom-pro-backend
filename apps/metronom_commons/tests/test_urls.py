import sys
from importlib import import_module, reload

from django.conf import settings
from django.core.urlresolvers import (NoReverseMatch, Resolver404,
                                      clear_url_caches)
from django.test import TestCase, override_settings
from django.urls import resolve, reverse


class ReloadURLConfMixin(TestCase):
    @classmethod
    def reload_url_conf(cls):
        clear_url_caches()
        assert settings.ROOT_URLCONF == 'config.urls'
        # assert settings.ROOT_URLCONF in sys.modules

        if settings.ROOT_URLCONF in sys.modules:
            reload(sys.modules[settings.ROOT_URLCONF])
        return import_module(settings.ROOT_URLCONF)


@override_settings(SHOW_DOCS_IN_PRODUCTION_MODE=True)
@override_settings(STAGE='local')
class TestReDocLocalURLs(ReloadURLConfMixin):
    def setUp(self):
        self.reload_url_conf()

    def test_redoc_reverse(self):
        assert reverse('schema-redoc') == f'/redoc/'

    def test_redoc_resolve(self):
        assert resolve(f'/redoc/').view_name == 'schema-redoc'


@override_settings(SHOW_DOCS_IN_PRODUCTION_MODE=False)
@override_settings(STAGE='production')
class TestReDocProductionURLs(ReloadURLConfMixin):
    def setUp(self):
        self.reload_url_conf()

    def test_redoc_reverse(self):
        with self.assertRaisesMessage(
                NoReverseMatch,
                "Reverse for 'schema-redoc' not found. 'schema-redoc' is not a valid view function or pattern name."):
            reverse('schema-redoc')

    def test_redoc_resolve(self):
        with self.assertRaises(Resolver404):
            resolve(f'/redoc/')
