from django.test import TestCase


class PyTestTest(TestCase):

    def setUp(self):
        pass

    def test_correct_environment_variables(self):
        """A site can only be set for one tenant."""
        from django.conf import settings

        # Tests should run with DEBUG=False
        self.assertEqual(settings.DEBUG, False)

        # A FEW THINGS are still different, when in CI testing - take care of those
        self.assertEqual(settings.RUNNING_TESTS, True)

        # Check the things, which are different in testing mode
        self.assertEqual(settings.DEFAULT_PROTOCOL, 'http')             # HTTP-only in testing
        self.assertEqual(settings.CELERY_TASK_ALWAYS_EAGER, True)       # Celery is not tested now
