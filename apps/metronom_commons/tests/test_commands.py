
import pytest
from ddt import data, ddt, unpack
from django.core.management import call_command
from django.test import testcases

from apps.accounting.models import Allocation, AllocationCategory
from apps.activities.models import ActivityGroup, Step, TemplateActivity
from apps.articles.models import ArticleGroup, TemplateArticle
from apps.bankaccounts.models import Bank, BankAccount
from apps.companies.models import Company, Office, Sector
from apps.employments.models import Employment
from apps.metronom_commons.management.commands.create_lots_of_data import \
    DEFAULT_COUNT_CREATED
from apps.persons.models import Person
from apps.projects.models import (Campaign, Project, ProjectActivity,
                                  ProjectActivityStep, ProjectArticle)
from apps.users.models import User
from config.example_data import ALLOCATIONS


class TestCommands(testcases.TestCase):
    """ This test checks next:
    1. after the calling `create_lots_of_data` command appears next data:
        1.1 DEFAULT_COUNT_CREATED Projects
        1.2 20 Campaigns

    """

    @pytest.mark.skip(
        reason="Uses PersonNiceFactory - need to fix either test, delete command or use different Factory")
    def test_create_lots_of_data(self):
        projects_before = Project.objects.count()
        campaigns_before = Campaign.objects.count()
        call_command("create_lots_of_data")
        self.assertEqual(Project.objects.count(), projects_before + DEFAULT_COUNT_CREATED)
        self.assertEqual(Campaign.objects.count(), campaigns_before + 20)

    @pytest.mark.skip(reason="Not sure why it fails - writing this down as a TODO")
    def test_create_example_data(self):
        self.assertEqual(ActivityGroup.objects.count(), 0)
        self.assertEqual(Allocation.objects.count(), 0)
        self.assertEqual(TemplateActivity.objects.count(), 0)
        self.assertEqual(ArticleGroup.objects.count(), 0)
        self.assertEqual(TemplateArticle.objects.count(), 0)
        self.assertEqual(BankAccount.objects.count(), 0)
        self.assertEqual(Campaign.objects.count(), 0)
        self.assertEqual(Company.objects.count(), 0)
        self.assertEqual(Employment.objects.count(), 0)
        self.assertEqual(Office.objects.count(), 0)
        self.assertEqual(Person.objects.count(), 0)
        self.assertEqual(Project.objects.count(), 0)
        self.assertEqual(ProjectArticle.objects.count(), 0)
        self.assertEqual(ProjectActivity.objects.count(), 0)
        self.assertEqual(ProjectActivityStep.objects.count(), 0)
        self.assertEqual(Step.objects.count(), 0)

        call_command('create_example_data')

        self.assertTrue(ActivityGroup.objects.count() > 1)
        self.assertTrue(Allocation.objects.count() > 1)
        self.assertTrue(TemplateActivity.objects.count() > 1)
        self.assertFalse(TemplateActivity.objects.filter(template_activity_steps__isnull=True).exists())
        self.assertTrue(ArticleGroup.objects.count() > 1)
        self.assertTrue(TemplateArticle.objects.count() > 1)
        self.assertTrue(BankAccount.objects.count() > 1)
        self.assertTrue(Campaign.objects.count() > 1)
        self.assertTrue(Company.objects.count() > 1)
        self.assertTrue(Employment.objects.count() > 1)
        self.assertTrue(Office.objects.count() > 1)
        self.assertTrue(Person.objects.count() > 1)
        self.assertTrue(Project.objects.count() > 1)
        self.assertFalse(Project.objects.filter(activities__isnull=True).exists())
        self.assertTrue(ProjectArticle.objects.count() > 1)
        self.assertTrue(ProjectActivity.objects.count() > 1)
        self.assertTrue(ProjectActivityStep.objects.count() > 1)
        self.assertTrue(Sector.objects.count() > 1)
        self.assertTrue(Step.objects.count() > 1)
        #self.assertTrue(User.objects.count() > 1)

        unique_categories = set([x['category__name'] for x in ALLOCATIONS])
        allocation_categories = AllocationCategory.objects.all().values_list('name', flat=True)
        for name in unique_categories:
            self.assertIn(name, allocation_categories)

        # we had weird +NoneNone phonenumbers after the command run, so we need to make sure
        # there is no longer such a problem
        landline_phones = [str(x.landline_phone) for x in Person.objects.all()]
        mobile_phones = [str(x.mobile_phone) for x in Person.objects.all()]
        self.assertNotIn("+NoneNone", landline_phones)
        self.assertNotIn("+NoneNone", mobile_phones)


@pytest.mark.skip("Is not working on CircleCI with AttributeError: 'PosixPath' object has no attribute 'seek' on call_command")
@ddt
class TestBankImportCommand(testcases.TestCase):
    """ This test imports bank data from excel files
    python manage.py import_bank_xlsx DE ./data/DE.xlsx
    python manage.py import_bank_xlsx DE ./data/DE.xlsx --delete
    python manage.py import_bank_xlsx CB ./data/CH.xlsx
    python manage.py import_bank_xlsx CB ./data/CH.xlsx --delete
    """

    @unpack
    @data(
        {'country': 'DE', 'file': './apps/metronom_commons/tests/test_data/DE.xlsx', 'delete': ''},
        {'country': 'DE', 'file': './apps/metronom_commons/tests/test_data/DE.xlsx', 'delete': '--delete'},
        {'country': 'CH', 'file': './apps/metronom_commons/tests/test_data/CH.xlsx', 'delete': ''},
        {'country': 'CH', 'file': './apps/metronom_commons/tests/test_data/CH.xlsx', 'delete': '--delete'}
    )
    def test_import_excel(self, country, file, delete):
        command = 'import_bank_xlsx'
        banks_before = Bank.objects.count()

        if delete:
            call_command(command, country, file, delete)
        else:
            call_command(command, country, file)

        assert Bank.objects.count() == banks_before + 10
