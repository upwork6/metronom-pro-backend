from django.core.exceptions import ValidationError
from django.test import TestCase, override_settings

from ..fields import check_uid_checksum, check_uid_length, clean_uid


@override_settings(LANGUAGE_CODE='en')
class CleanUIDTest(TestCase):

    def test_clean_uid(self):
        assert clean_uid('CHE-999.999.996') == '999999996'


@override_settings(LANGUAGE_CODE='en')
class ValidateUIDTest(TestCase):

    def test_check_uid_length(self):
        assert check_uid_length('999999996') == 'valid_length'

    def test_too_short(self):
        with self.assertRaisesMessage(ValidationError, 'This is not a UID, there are numbers missing.'):
            check_uid_length('12345678')

    def test_too_long(self):
        with self.assertRaisesMessage(ValidationError, 'This is not a UID, there are too many numbers.'):
            check_uid_length('1234567890')

    def test_check_uid_checksum(self):
        assert check_uid_checksum('109322551') == 'valid_checksum'

    def test_invalid_uid_error(self):
        with self.assertRaises(ValidationError):
            check_uid_checksum('109322550')

    def test_invalid_uid_message(self):
        with self.assertRaisesMessage(ValidationError, 'The numbers in the UID are not correct.'):
            check_uid_checksum('109322550')
