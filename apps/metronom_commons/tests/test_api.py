import sys
from importlib import import_module, reload

from django.conf import settings
from django.core.urlresolvers import clear_url_caches
from django.http import QueryDict
from django.test import override_settings
from rest_framework import status
from rest_framework.reverse import reverse

from apps.metronom_commons.test import MetronomBaseAPITestCase


@override_settings(SHOW_DOCS_IN_PRODUCTION_MODE=True)
@override_settings(STAGE='local')
class TestReDoc(MetronomBaseAPITestCase):
    @classmethod
    def reload_url_conf(cls):
        clear_url_caches()
        assert settings.ROOT_URLCONF == 'config.urls'

        if settings.ROOT_URLCONF in sys.modules:
            reload(sys.modules[settings.ROOT_URLCONF])

        return import_module(settings.ROOT_URLCONF)

    @classmethod
    def setUpTestData(cls):
        super(TestReDoc, cls).setUpTestData()
        cls.reload_url_conf()
        query_dictionary = QueryDict('', mutable=True)
        query_dictionary.update({
            'format': 'openapi'
        })
        url = reverse('schema-redoc')
        cls.endpoint = f'{url}?{query_dictionary.urlencode()}'

    def test_redoc_response(self):
        response = self.admin_client.get(self.endpoint)
        assert response.status_code == status.HTTP_200_OK
