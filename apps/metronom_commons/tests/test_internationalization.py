import json
from datetime import date, timedelta

import pytest
from django.urls import reverse
from rest_framework import status

from apps.companies.tests.factories import CompanyWithOfficeFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.persons.tests.factories import PersonFactory
from apps.projects.models import ACCOUNTING_TYPES
from apps.users.tests.factories import UserFactory


class TestGermanLocalization(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company_list_endpoint = reverse('api:company-list')
        cls.project_list_endpoint = reverse('api:project-list')
        cls.employment_endpoint = reverse('api:employment-list')

        # We use possibly reusable objects here
        cls.company = CompanyWithOfficeFactory()
        cls.billing_address = cls.company.offices.all()[0].address

        cls.person = PersonFactory()
        cls.project_owner = UserFactory()

        cls.fake_uuid = '921d9f85-5b5a-42d8-afcb-47eb5d48c0d4'
        cls.email = 'cto@company.com'
        cls.login_as_user()

    def test_company_post_localization(self):
        header = {"HTTP_ACCEPT_LANGUAGE": "de"}
        response = self.admin_client.post(self.company_list_endpoint, data={}, format='json', **header)
        self.assertErrorResponse(
            response,
            {
                "name": ['Dieses Feld ist erforderlich.'],
                'sector_uuid': ['Dieses Feld ist erforderlich.']
            }
        )

    @pytest.mark.skip(reason="TODO: is failing now")
    def test_project_post_localization(self):
        # I'm assuming that if this works for the projects part - it'll work for all project
        # because of DRF internationalization support
        # http://www.django-rest-framework.org/topics/3.1-announcement/#internationalization
        # Why projects app? Because I worked on it recently :)

        accounting_type = ACCOUNTING_TYPES.PROFIT
        finishing_date = date.today() + timedelta(weeks=4)
        start_date = finishing_date + timedelta(days=1)
        delivery_date = date.today() + timedelta(weeks=6)

        post_data = {
            'title': "International agency project",
            'contact_person_uuid': self.person.id,
            'accounting_type': accounting_type,
            'finishing_date': finishing_date,
            'start_date': start_date,
            'delivery_date': delivery_date,
            'project_owner_uuid': self.project_owner.id,
            'billing_address_uuid': self.billing_address.id,
        }

        # Default response with no header
        response = self.admin_client.post(self.project_list_endpoint, data=post_data, format='json')
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data['company_uuid'] == ['This field is required.']

        # Header english
        header = {"HTTP_ACCEPT_LANGUAGE": "en"}
        response = self.admin_client.post(self.project_list_endpoint, data=post_data, format='json', **header)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data['company_uuid'] == ['This field is required.']

        # Header german
        header = {"HTTP_ACCEPT_LANGUAGE": "de"}
        response = self.admin_client.post(self.project_list_endpoint, data=post_data, format='json', **header)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data['company_uuid'] == ['Dieses Feld ist erforderlich.']

        post_data['company_uuid'] = str(self.company.id)
        # Default response with no header
        response = self.admin_client.post(self.project_list_endpoint, data=post_data, format='json')
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data['error'] == ['The finishing date cannot be earlier than the start date.']

        # Header english
        header = {"HTTP_ACCEPT_LANGUAGE": "en"}
        response = self.admin_client.post(self.project_list_endpoint, data=post_data, format='json', **header)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data['error'] == ['The finishing date cannot be earlier than the start date.']

        # Header german
        header = {"HTTP_ACCEPT_LANGUAGE": "de"}
        response = self.admin_client.post(self.project_list_endpoint, data=post_data, format='json', **header)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data['error'] == ['Das Projektende kann nicht vor dem Projektstart liegen.']

    @pytest.mark.skip(reason="TODO: is failing now")
    def test_autocomplete_localization(self):
        url = reverse('autocompletes:united')
        response = self.admin_client.get(url)
        self.assertEqual(response.status_code, 200)
        response_body = json.loads(response.content)
        self.assertEqual(response_body['persons:languages']['de'], 'German')
        self.assertEqual(response_body['contacts:statuses']['active'], 'active')
        self.assertEqual(response_body['contacts:statuses']['restricted'], 'restricted')
        self.assertEqual(response_body['contacts:statuses']['archived'], 'archived')
        self.assertEqual(response_body['persons:genders']['male'], 'male')
        self.assertEqual(response_body['persons:genders']['female'], 'female')
        self.assertEqual(response_body['companies:added_charges_dependencies']['agency'], 'Agency')
        self.assertEqual(response_body['companies:added_charges_dependencies']['external'], 'External services')
        self.assertEqual(response_body['companies:added_charges_dependencies']['passing'], 'Passing services')
        self.assertEqual(response_body['companies:billing_deliveries']['post'], 'with Post')
        self.assertEqual(response_body['companies:billing_deliveries']['email'], 'with Email')
        self.assertEqual(response_body['companies:billing_agreements']['according_to_offer'], 'According to offer')
        self.assertEqual(response_body['companies:billing_agreements']['according_to_effort'], 'According to effort')
        self.assertEqual(response_body['companies:billing_intervals']['task_based'], 'Task based')
        self.assertEqual(response_body['companies:billing_intervals']['project_based'], 'Project based')
        self.assertEqual(response_body['companies:billing_intervals']['campaign_based'], 'Campaign based')
        self.assertEqual(response_body['companies:billing_intervals']['monthly'], 'Monthly')
        self.assertEqual(response_body['companies:billing_intervals']['quarterly'], 'Quarterly')
        self.assertEqual(response_body['companies:billing_intervals']['semi-annual'], 'Semi-annual')
        self.assertEqual(response_body['companies:billing_intervals']['annual'], 'Annual')
        self.assertEqual(response_body['companies:company_types']['unknown'], 'Unknown')
        self.assertEqual(response_body['companies:company_types']['service'], 'Service provider')
        self.assertEqual(response_body['companies:company_types']['customer'], 'Customer')
        self.assertEqual(response_body['companies:company_types']['freelancer'], 'Freelancer')
        url = reverse('autocompletes:united')
        headers = {'HTTP_ACCEPT_LANGUAGE': "de"}
        response = self.admin_client.get(url, **headers)
        response_body = json.loads(response.content)
        self.assertEqual(response_body['persons:languages']['de'], 'Deutsch')
        self.assertEqual(response_body['persons:languages']['it'], 'Italienisch')
        self.assertEqual(response_body['contacts:statuses']['active'], 'Aktiv')
        self.assertEqual(response_body['contacts:statuses']['restricted'], 'Geschützt')
        self.assertEqual(response_body['contacts:statuses']['archived'], 'Archiviert')
        self.assertEqual(response_body['persons:genders']['male'], 'männlich')
        self.assertEqual(response_body['persons:genders']['female'], 'weiblich')
        self.assertEqual(response_body['companies:added_charges_dependencies']['agency'], 'Agentur')
        self.assertEqual(response_body['companies:added_charges_dependencies']['external'], 'Fremdrechnungen')
        self.assertEqual(response_body['companies:added_charges_dependencies']['passing'], 'Durchgereichte Leistungen')
        self.assertEqual(response_body['companies:billing_deliveries']['post'], 'per Post')
        self.assertEqual(response_body['companies:billing_deliveries']['email'], 'per E-Mail')
        self.assertEqual(response_body['companies:billing_agreements']['according_to_offer'], 'Gemäß Offerte')
        self.assertEqual(response_body['companies:billing_agreements']['according_to_effort'], 'Gemäß Aufwand')
        self.assertEqual(response_body['companies:billing_intervals']['task_based'], 'Aufgabenweise')
        self.assertEqual(response_body['companies:billing_intervals']['project_based'], 'Projektweise')
        self.assertEqual(response_body['companies:billing_intervals']['campaign_based'], 'Kampagnenweise')
        self.assertEqual(response_body['companies:billing_intervals']['monthly'], 'Monatlich')
        self.assertEqual(response_body['companies:billing_intervals']['quarterly'], 'Quartalsweise')
        self.assertEqual(response_body['companies:billing_intervals']['semi-annual'], 'Halbjährlich')
        self.assertEqual(response_body['companies:billing_intervals']['annual'], 'Jährlich')
        self.assertEqual(response_body['companies:company_types']['unknown'], 'Unbekannt')
        self.assertEqual(response_body['companies:company_types']['service'], 'Dienstleister')
        self.assertEqual(response_body['companies:company_types']['customer'], 'Kunde')
        self.assertEqual(response_body['companies:company_types']['freelancer'], 'Freelancer')

    def test_employment_post_localization(self):

        post_data = {
            'person_uuid': str(self.person.id),
            'company_uuid': self.fake_uuid,
            'position_name': 'CTO',
            'email': self.email,
        }

        header = {"HTTP_ACCEPT_LANGUAGE": "de"}
        response = self.admin_client.post(self.employment_endpoint, data=post_data, format='json', **header)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert "Object existiert nicht" in str(response.data)
