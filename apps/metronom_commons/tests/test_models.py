
from django.test import TestCase

from apps.activities.models import Step
from apps.activities.tests.factories import StepFactory
from apps.projects.tests.factories import ProjectFactory, ProjectActivityFactory, ProjectArticleFactory
from apps.projects.models import Project, ProjectActivity, ProjectArticle


class TestSoftDeletion(TestCase):

    def test_simple_deletion(self):
        project = ProjectFactory()
        project.delete()
        self.assertEqual(Project.objects.count(), 0)
        self.assertEqual(Project.all_objects.count(), 1)

    def test_deletion_with_nested(self):
        project = ProjectFactory()
        ProjectArticleFactory(project=project)
        project.delete()
        self.assertEqual(Project.objects.count(), 0)
        self.assertEqual(Project.all_objects.count(), 1)
        self.assertEqual(ProjectArticle.objects.count(), 0)
        self.assertEqual(ProjectArticle.all_objects.count(), 1)

    def test_delete_with_uniqueness(self):
        step = StepFactory(name="Unique name")
        step.delete()
        self.assertEqual(Step.objects.count(), 0)
        self.assertEqual(Step.all_objects.count(), 1)
        # now let's check that we can create new step with the same name
        step = StepFactory(name="Unique name")

    def test_delete_with_uniqueness_with_duplication_name(self):
        step = StepFactory(name="Unique name")
        step.delete()
        self.assertEqual(Step.objects.count(), 0)
        self.assertEqual(Step.all_objects.count(), 1)
        # now let's check that we can create new step with the same name and delete it
        step = StepFactory(name="Unique name")
        step.delete()
        self.assertEqual(Step.objects.count(), 0)
        self.assertEqual(Step.all_objects.count(), 2)
        step = StepFactory(name="Unique name")

    def test_restoration_of_deleted(self):
        step1 = StepFactory(name="Unique name")
        step1.delete()
        self.assertEqual(Step.objects.count(), 0)
        self.assertEqual(Step.all_objects.count(), 1)
        step1.save()
        step1.refresh_from_db()
        self.assertEqual(step1.name, "Unique name")

    def test_restoration_of_deleted_with_unique_name_duplication(self):
        step1 = StepFactory(name="Unique name")
        step1.delete()
        self.assertEqual(Step.objects.count(), 0)
        self.assertEqual(Step.all_objects.count(), 1)
        # now let's check that we can create new step with the same name and delete it
        step2 = StepFactory(name="Unique name")
        step2.delete()
        self.assertEqual(Step.objects.count(), 0)
        self.assertEqual(Step.all_objects.count(), 2)
        step = StepFactory(name="Unique name")
        step1.save()
        step1.refresh_from_db()
        self.assertEqual(step1.name, "Unique name 2")
        step2.save()
        step2.refresh_from_db()
        self.assertEqual(step2.name, "Unique name 3")
