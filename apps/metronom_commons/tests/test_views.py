
from django.urls import reverse

from apps.activities.tests.factories import (ActivityGroupFactory,
                                             TemplateActivityFactory)
from apps.articles.tests.factories import TemplateArticleFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.tests.factories import (ProjectActivityFactory,
                                           ProjectActivityStepFactory,
                                           ProjectArticleFactory,
                                           ProjectFactory)


class TestGeneralOrderingView(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse("reordering")

    def setUp(self):
        self.group1 = ActivityGroupFactory(position=1)
        self.group2 = ActivityGroupFactory(position=2)
        self.group3 = ActivityGroupFactory(position=3)
        self.group4 = ActivityGroupFactory(position=4)
        self.group5 = ActivityGroupFactory(position=5)
        self.groups = [self.group1, self.group2, self.group3, self.group4, self.group5, ]

    def _refresh_from_db(self):
        for i in self.groups:
            i.refresh_from_db()

    def test_ordering_only_patch_supported(self):
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 405)
        response = self.admin_client.put(self.endpoint)
        self.assertEqual(response.status_code, 405)
        response = self.admin_client.post(self.endpoint)
        self.assertEqual(response.status_code, 405)
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 405)

    def test_ordering_correct_moving_forward(self):
        patch_data = {
            'type': 'activitygroup',
            'uuid': self.group4.id,
            'old_position': self.group4.position,
            'new_position': self.group2.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self._refresh_from_db()
        self.assertEqual(self.group4.position, patch_data['new_position'])
        self.assertEqual(self.group3.position, patch_data['old_position'])
        self.assertEqual(self.group2.position, patch_data['new_position'] + 1)

    def test_ordering_correct_moving_backward(self):
        patch_data = {
            'type': 'activitygroup',
            'uuid': self.group2.id,
            'old_position': self.group2.position,
            'new_position': self.group4.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self._refresh_from_db()
        self.assertEqual(self.group2.position, patch_data['new_position'])
        self.assertEqual(self.group3.position, patch_data['old_position'])
        self.assertEqual(self.group4.position, patch_data['new_position'] - 1)

    def test_ordering_wrong_type_send(self):
        patch_data = {
            'type': 'allocation',
            'uuid': self.group2.id,
            'old_position': self.group2.position,
            'new_position': self.group4.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {
                "__all__": ["There is no object with such uuid"]
            }
        )

    def test_ordering_wrong_not_existing_type_send(self):
        patch_data = {
            'type': 'le_allocation',
            'uuid': self.group2.id,
            'old_position': self.group2.position,
            'new_position': self.group4.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {
                "type": ["There is no such a model in our system"]
            }
        )

    def test_ordering_outdated_data(self):
        patch_data = {
            'type': 'activitygroup',
            'uuid': self.group2.id,
            'old_position': self.group2.position,
            'new_position': self.group4.position
        }
        self.group2.position = 55
        self.group2.save()
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {
                "__all__": ["Object old position is not correct!"]
            }
        )

    def test_ordering_wrong_new_position_in_data(self):
        patch_data = {
            'type': 'activitygroup',
            'uuid': self.group2.id,
            'old_position': self.group2.position,
            'new_position': 55
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {
                "__all__": ["No object to move into position"]
            }
        )

    def test_correct_ordering_for_other_mixin(self):
        """ We need to test that our OrderingWithoutSignalMixin as well works with this serialzier
        """
        ta1 = TemplateActivityFactory(activity_group=self.group1)
        ta2 = TemplateActivityFactory(activity_group=self.group1)
        ta3 = TemplateActivityFactory(activity_group=self.group1)
        ta4 = TemplateActivityFactory(activity_group=self.group1)
        ta5 = TemplateActivityFactory(activity_group=self.group1)
        patch_data = {
            'type': 'templateactivity',
            'uuid': ta4.id,
            'old_position': ta4.position,
            'new_position': ta2.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        ta4.refresh_from_db()
        ta3.refresh_from_db()
        ta2.refresh_from_db()
        self.assertEqual(ta4.position, patch_data['new_position'])
        self.assertEqual(ta3.position, patch_data['old_position'])
        self.assertEqual(ta2.position, patch_data['new_position'] + 1)


class TestBatchUpdateObjects(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project1 = ProjectFactory(billing_currency="CHF")
        cls.template_article1 = TemplateArticleFactory()
        cls.template_article2 = TemplateArticleFactory()
        cls.project_article1 = ProjectArticleFactory(project=cls.project1, used_template_article=cls.template_article1)
        cls.project_article2 = ProjectArticleFactory(project=cls.project1, used_template_article=cls.template_article1)
        cls.project_article3 = ProjectArticleFactory(project=cls.project1, used_template_article=cls.template_article1)
        cls.project_activity1 = ProjectActivityFactory(project=cls.project1, status="plan")
        cls.pa_step1 = ProjectActivityStepFactory(activity=cls.project_activity1)
        cls.pa_step2 = ProjectActivityStepFactory(activity=cls.project_activity1)
        cls.pa_step3 = ProjectActivityStepFactory(activity=cls.project_activity1)
        cls.project_activity2 = ProjectActivityFactory(project=cls.project1, status="plan")
        cls.project_activity3 = ProjectActivityFactory(project=cls.project1, status="plan")
        cls.endpoint = reverse("batch_update")

    def test_batch_update_of_the_project_activities(self):
        start_date_data = {
            "values": {
                "start_date": "2018-10-01",
                "status": "work"
            },
            "object_type": "projectactivity",
            "objects": [
                self.project_activity1.id,
                self.project_activity2.id
            ]
        }
        response = self.admin_client.patch(self.endpoint, start_date_data, format="json")
        data_to_check = {
            str(self.project_activity1.id): {
                "start_date": "2018-10-01",
                "status": "work"
            },
            str(self.project_activity2.id): {
                "start_date": "2018-10-01",
                "status": "work"
            }
        }
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, data_to_check)
        self.project_activity1.refresh_from_db()
        self.project_activity2.refresh_from_db()
        self.project_activity3.refresh_from_db()
        self.assertEqual(self.project_activity1.status, start_date_data["values"]["status"])
        self.assertEqual(self.project_activity2.status, start_date_data["values"]["status"])
        self.assertNotEqual(self.project_activity3.status, start_date_data["values"]["status"])
        self.assertEqual(
            self.project_activity1.start_date.strftime('%Y-%m-%d'),
            start_date_data["values"]["start_date"]
        )
        self.assertEqual(
            self.project_activity2.start_date.strftime('%Y-%m-%d'),
            start_date_data["values"]["start_date"]
        )
        self.assertNotEqual(
            self.project_activity3.start_date.strftime('%Y-%m-%d'),
            start_date_data["values"]["start_date"]
        )
        display_name_data = {
            "values": {
                "display_name": "My display name",
            },
            "object_type": "projectactivity",
            "objects": [
                self.project_activity1.id,
                self.project_activity3.id
            ]
        }
        response = self.admin_client.patch(self.endpoint, display_name_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.project_activity1.refresh_from_db()
        self.project_activity2.refresh_from_db()
        self.project_activity3.refresh_from_db()
        self.assertEqual(self.project_activity1.display_name, display_name_data["values"]["display_name"])
        self.assertEqual(self.project_activity3.display_name, display_name_data["values"]["display_name"])
        self.assertNotEqual(self.project_activity2.display_name, display_name_data["values"]["display_name"])

    def test_batch_update_of_the_project_articles(self):
        used_template_article_data = {
            "values": {
                "used_template_article": {
                    'uuid': str(self.template_article2.id)
                }
            },
            "object_type": "projectarticle",
            "objects": [
                self.project_article1.id,
                self.project_article2.id
            ]
        }
        response = self.admin_client.patch(self.endpoint, used_template_article_data, format="json")
        data_to_check = {
            str(self.project_article1.id): {
                "used_template_article": {
                    'uuid': str(self.template_article2.id)
                }
            },
            str(self.project_article2.id): {
                "used_template_article": {
                    'uuid': str(self.template_article2.id)
                }
            }
        }
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, data_to_check)
        self.project_article1.refresh_from_db()
        self.project_article2.refresh_from_db()
        self.project_article3.refresh_from_db()
        self.assertEqual({'uuid': str(self.project_article1.used_template_article.id)},
                         used_template_article_data["values"]["used_template_article"])
        self.assertEqual({'uuid': str(self.project_article2.used_template_article.id)},
                         used_template_article_data["values"]["used_template_article"])
        self.assertNotEqual({'uuid': str(self.project_article3.used_template_article.id)},
                            used_template_article_data["values"]["used_template_article"])

    def test_batch_update_of_the_project_activities_step(self):
        start_date_data = {
            "values": {
                "start_date": "2018-10-01",
            },
            "object_type": "projectactivitystep",
            "objects": [
                self.pa_step1.id,
                self.pa_step2.id
            ]
        }
        response = self.admin_client.patch(self.endpoint, start_date_data, format="json")
        data_to_check = {
            str(self.pa_step1.id): {
                "start_date": "2018-10-01",
            },
            str(self.pa_step2.id): {
                "start_date": "2018-10-01",
            }
        }
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, data_to_check)
        self.pa_step1.refresh_from_db()
        self.pa_step2.refresh_from_db()
        self.pa_step3.refresh_from_db()
        self.assertEqual(
            self.pa_step1.start_date.strftime('%Y-%m-%d'),
            start_date_data["values"]["start_date"]
        )
        self.assertEqual(
            self.pa_step2.start_date.strftime('%Y-%m-%d'),
            start_date_data["values"]["start_date"]
        )
        self.assertNotEqual(
            self.pa_step3.start_date.strftime('%Y-%m-%d'),
            start_date_data["values"]["start_date"]
        )

    def test_batch_update_validation(self):
        # after this change project_article1 should pe protected from editing
        self.project_article1.offer_status = "sent"
        self.project_article1.save(update_fields=["offer_status"])
        used_template_article_data = {
            "values": {
                "display_name": "New name"
            },
            "object_type": "projectarticle",
            "objects": [
                self.project_article1.id,
                self.project_article2.id
            ]
        }
        response = self.admin_client.patch(self.endpoint, used_template_article_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_batch_update_of_the_wrong_objects(self):
        display_name_data = {
            "values": {
                "display_name": "My display name",
            },
            "object_type": "activity",
            "objects": [
                self.project_activity1.id,
                self.project_activity3.id
            ]
        }
        response = self.admin_client.patch(self.endpoint, display_name_data, format="json")
        self.assertEqual(response.status_code, 403)

    def test_batch_update_permissions(self):
        used_template_article_data = {
            "values": {
                "used_template_article": {
                    'uuid': str(self.template_article2.id)
                }
            },
            "object_type": "projectarticle",
            "objects": [
                self.project_article1.id,
                self.project_article2.id
            ]
        }
        response = self.hr_client.patch(self.endpoint, used_template_article_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.endpoint, used_template_article_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.backoffice_client.patch(self.endpoint, used_template_article_data, format="json")
        self.assertEqual(response.status_code, 403)
