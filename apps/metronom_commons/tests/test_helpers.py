import uuid

from django.test import TestCase


def validate_uuid4_string(uuid_string):

    try:
        val = uuid.UUID(uuid_string, version=4)
    except ValueError:
        return False

    return uuid_string == str(val)


class UUIDValidationTest(TestCase):

    def setUp(self):
        self.created_uuid = uuid.uuid4()
        self.manual_uuid_string = "d34935c9-b72e-4aa8-8fd8-6f8a2e57266c"

    def test_validation_of_created_uuid(self):
        """A site can only be set for one tenant."""

        self.assertEqual(validate_uuid4_string(str(self.created_uuid)), True)

    def test_validation_of_uuid_string(self):
        """A site can only be set for one tenant."""

        self.assertEqual(validate_uuid4_string(self.manual_uuid_string), True)
