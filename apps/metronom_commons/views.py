
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.metronom_commons.permissions import GenericDRYPermissions
from apps.metronom_commons.serializers import GenericOrderingSerializer
from apps.metronom_commons.concrete_serializers import GenericBatchUpdateSerializer


class ReorderingView(APIView):
    http_method_names = ['patch']
    serializer_class = GenericOrderingSerializer

    def patch(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.update_position()
        return Response(serializer.data)


class BatchUpdateView(APIView):
    http_method_names = ['patch']
    serializer_class = GenericBatchUpdateSerializer
    permission_classes = (GenericDRYPermissions,)

    def get_serializer_class(self):
        """ We need this method to make our DRYPermissions workable
        """
        return self.serializer_class

    def patch(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.update()
        return Response(serializer.data)
