from django.db.models.constants import LOOKUP_SEP
from rest_framework.filters import SearchFilter
from uuid import UUID
from rest_framework.exceptions import ValidationError


def validate_uuid(value):
    """ Checks, if a given UUID is valid"""

    try:
        UUID(value, version=4)
    except ValueError as ve:
        raise ValidationError(detail=ve)


def validate_value(value, validation=None):
    """ Validates the Value against some of the existing validators """

    if value:
        if validation == "uuid":
            validate_uuid(value)

    return value


def get_value_list(values, validation=None):
    """ Gets the values from the request for the given attribute (&attribute=5,2), validates those and returns the list [5, 2]"""

    if values:
        value_list = values.split(u',')

        for value in value_list:
            validate_value(value, validation)

        return value_list

    return []


def get_value_or_value_list(values, validation=None):
    """ Gets the values from the request for the given attribute (&attribute=5,2), validates those and returns the list [5, 2]"""

    value_list = get_value_list(values, validation)

    if value_list:
        if len(value_list) == 1:
            return value_list[0]

        return value_list

    return None


class CombinedQuerysetSearchFilter(SearchFilter):
    """ Used for Search on Combined Querysets like the projects ist combined of projects+campaigns """

    def must_call_distinct(self, queryset, search_fields):
        """
        Return True if 'distinct()' should be used to query the given lookups.
        """

        # TODO: Is it working in call cases with False?
        return False

        for search_field in search_fields:
            opts = queryset.model._meta
            if search_field[0] in self.lookup_prefixes:
                search_field = search_field[1:]
            parts = search_field.split(LOOKUP_SEP)
            for part in parts:
                field = opts.get_field(part)
                if hasattr(field, 'get_path_info'):
                    # This field is a relation, update opts to follow the relation
                    path_info = field.get_path_info()
                    opts = path_info[-1].to_opts
                    if any(path.m2m for path in path_info):
                        # This field is a m2m relation so we know we need to call distinct
                        return True
        return False

