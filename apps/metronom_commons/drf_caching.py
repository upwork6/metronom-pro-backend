from django.conf import settings
from django.core.cache import caches
from django.utils import timezone
from django.utils.encoding import force_text
from rest_framework_extensions.key_constructor.bits import (HeadersKeyBit,
                                                            KeyBitBase)


def get_drf_view_cache():
    cache_alias = settings.REST_FRAMEWORK_EXTENSIONS['DEFAULT_USE_CACHE']
    cache = caches[cache_alias]
    return cache


cache = get_drf_view_cache()


class ModelUpdatedAtKeyBit(KeyBitBase):
    key_suffix = 'updated_at_timestamp'
    model_import_path = ''

    @classmethod
    def generate_cache_key(cls):
        return f'{cls.model_import_path}-{cls.key_suffix}'

    def get_data(self, **kwargs):
        key = self.generate_cache_key()
        value = cache.get(key, None)
        if not value:
            value = timezone.now()
            self.update_cache(value)
        return force_text(value)

    @classmethod
    def update_cache(cls, value=None):
        key = cls.generate_cache_key()
        cache.set(key, value=value or timezone.now())


class IfModifiedSinceHeadersKeyBit(HeadersKeyBit):
    def get_source_dict(self, params, view_instance, view_method, request, args, kwargs):
        if_modified_since = request.META.get('HTTP_IF_MODIFIED_SINCE', '')
        return if_modified_since
