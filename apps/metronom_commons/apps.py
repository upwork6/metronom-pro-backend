
from django.apps import AppConfig
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

from safedelete.signals import pre_softdelete, post_undelete


class MetronomCommonsConfig(AppConfig):
    name = 'apps.metronom_commons'
    verbose_name = _('Commons')

    def ready(self):
        """ We need to subscribe all subclasses of our OrderingMixin to the corresponding signal.
        Also we need to subscribe SoftDeleteUniqueMixin to the corresponding signal as well.
        """
        from .models import OrderingMixin, post_save_position_change_signal, SoftDeleteUniqueMixin
        all_subclasses = self._get_all_subclasses(OrderingMixin)
        for model_cls in all_subclasses:
            if not model_cls._meta.abstract:
                post_save.connect(
                    post_save_position_change_signal,
                    model_cls,
                    dispatch_uid=f"{model_cls.__name__}_position_change"
                )
        all_subclasses = self._get_all_subclasses(SoftDeleteUniqueMixin)
        for model_cls in all_subclasses:
            if not model_cls._meta.abstract:
                pre_softdelete.connect(
                    SoftDeleteUniqueMixin.pre_softdelete_signal,
                    model_cls,
                    dispatch_uid=f"{model_cls.__name__}_pre_softdelete"
                )
                post_undelete.connect(
                    SoftDeleteUniqueMixin.post_undelete_signal,
                    model_cls,
                    dispatch_uid=f"{model_cls.__name__}_post_undelete"
                )

    def _get_all_subclasses(self, cls):
        """ We need to get not only the direct subclasses but also all subsubclasses as well, so a bit of
        recursion here
        """
        return cls.__subclasses__() + [g for s in cls.__subclasses__()
                                       for g in self._get_all_subclasses(s)]
