
from collections import OrderedDict

from django.contrib.contenttypes.models import ContentType
from django.db.models import F
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.fields import Field

from .utils import update_or_create_all_fk


class ForeignKeyFieldsCreateOrUpdateMixin(object):
    """
    Create a new instance or updates the existing one for all field names (fk_keys)

    !!!
    !!! You must set attribute 'fk_keys' with list of field names which should be processing.
    !!! The mixin don't determine the foreign fields itself from model instance for flexible serialization.
    !!!
    """

    def _check_settings(self):
        if not hasattr(self.Meta, 'fk_keys'):
            raise AttributeError(_('You must set fk_keys in Meta class '
                                   'when working with the mixin ForeignKeyFieldsCreateOrUpdateMixin'))

    def create(self, validated_data, commit=True):

        self._check_settings()

        fk_keys = self.Meta.fk_keys
        instance_cls = self.Meta.model

        validated_data_without_fk_fields = {key: value
                                            for key, value in validated_data.items()
                                            if key not in fk_keys}
        instance = instance_cls(**validated_data_without_fk_fields)
        update_or_create_all_fk(instance, fk_keys=fk_keys, values=validated_data)

        if commit:
            instance.save()

        return instance

    def update(self, instance, validated_data, commit=True):
        """Use the respective IDs for updating Many2Many relationships and write nested data."""

        self._check_settings()

        fk_keys = self.Meta.fk_keys

        update_or_create_all_fk(instance, fk_keys=fk_keys, values=validated_data)

        for key, value in validated_data.items():
            if key not in fk_keys:
                setattr(instance, key, value)

        if commit:
            instance.save()

        return instance


class ForbidModifyIfUsedMixinSerializer(object):
    """ This mixin exists to provide a generic solution for the situations when we need to allow edit\delete
    of the object, but forbid to do this if this object used somewhere.
    To make this possible, we need to provide extra dict inside the Meta of the serializer:
    forbid_modify_if_used_in.
    Example:

    class Meta:
        forbid_modify_if_used_in = {
            TemplateActivity: ['article_group']
        }

    """

    def validate(self, validated_data):
        if (self.context['request'].method.lower() in ["patch", "put"] and
                getattr(self.Meta, "forbid_modify_if_used_in", None) is not None):
            for model, keys in self.Meta.forbid_modify_if_used_in.items():
                for key in keys:
                    if model.objects.filter(**{key: self.instance}).exists():
                        raise serializers.ValidationError(_(f"You can't modify used {self.Meta.model.__name__}"))
        return validated_data


class GenericOrderingSerializer(serializers.Serializer):
    old_position = serializers.IntegerField()
    new_position = serializers.IntegerField()
    type = serializers.CharField()
    uuid = serializers.UUIDField()

    def validate(self, validate_data):
        self.instance = self._get_instance(validate_data['type'], validate_data['uuid'])
        if self.instance.position == validate_data['new_position']:
            raise serializers.ValidationError(_("Object is already at this position"))
        if self.instance.position != validate_data['old_position']:
            raise serializers.ValidationError(_("Object old position is not correct!"))
        if not self.instance.__class__.objects.filter(position=validate_data['new_position']).exists():
            raise serializers.ValidationError(_("No object to move into position"))
        return validate_data

    def _get_instance(self, model_name, uuid):
        """ This method provide an instance from the given string with the model name and uuid
        """
        # we don't know to what app the mode
        ct = ContentType.objects.get(model=model_name.lower())
        model = ct.model_class()
        instance = model.objects.filter(id=uuid).first()
        if instance is None:
            raise serializers.ValidationError("There is no object with such uuid")
        return instance

    def validate_type(self, value):
        # we don't know to what app the model is related, so we use ContentType filtering instead
        # of get_model
        ct = ContentType.objects.filter(model=value.lower()).first()
        if ct is None:
            raise serializers.ValidationError("There is no such a model in our system")
        return value

    def update_position(self):
        # so basically we need to do next:
        # 1. If new_position is greater than old one, we need to move all related positions upwards
        # 2. If new_position is less than old_one, we need to move all related positions downwards
        data = self.validated_data
        if data['new_position'] > data['old_position']:
            self.instance.__class__.objects.exclude(id=self.instance.id).filter(
                position__lte=data['new_position'],
                position__gt=data['old_position']).update(position=F('position') - 1)
        else:
            self.instance.__class__.objects.exclude(id=self.instance.id).filter(
                position__gte=data['new_position'],
                position__lt=data['old_position']).update(position=F('position') + 1)
        self.instance.position = data['new_position']
        self.instance.save(update_fields=['position'])
        return self.instance

    def to_representation(self, *args, **kwargs):
        return {
            "position": self.instance.position,
            "uuid": self.instance.id,
        }


class SoftDeletionMetaclass(serializers.SerializerMetaclass):
    """ Because we don't want to rapidly copy&paste "is_deleted" field into the dozens of the serializiers
    and we still need to make sure it is always inside the Meta fields, we have to write our own metaclass, which 
    is basically inherits from the SerializerMetaclass, but always inserts is_deleted as the 
    SerializerMethodField and inserts it into the Meta fields.


    ** Be aware ** : if this metaclass used with some abstract serializer which is used with the model whom
    do not have deleted field, it can cause really weird errors.
    Check this to have a full info:
    https://github.com/jensneuhaus/metronom-pro-backend/issues/452
    https://stackoverflow.com/questions/50415002/unexpected-metaclass-overriding-for-the-drf-serializers

    """

    @classmethod
    def _get_declared_fields(cls, bases, attrs):
        fields = [(field_name, attrs.pop(field_name))
                  for field_name, obj in list(attrs.items())
                  if isinstance(obj, Field)]
        fields.append(("is_deleted", serializers.SerializerMethodField(read_only=True)))
        fields.sort(key=lambda x: x[1]._creation_counter)
        # If this class is subclassing another Serializer, add that Serializer's
        # fields.  Note that we loop over the bases in *reverse*. This is necessary
        # in order to maintain the correct order of fields.
        for base in reversed(bases):
            if hasattr(base, '_declared_fields'):
                fields = [
                    (field_name, obj) for field_name, obj
                    in base._declared_fields.items()
                    if field_name not in attrs
                ] + fields
        return OrderedDict(fields)

    def __new__(cls, name, bases, attrs):
        attrs['_declared_fields'] = cls._get_declared_fields(bases, attrs)
        meta = attrs.get('Meta')
        if meta:
            if isinstance(meta.fields, tuple):
                meta.fields += ("is_deleted", )
            else:
                meta.fields.append("is_deleted")
            attrs["Meta"] = meta
        return super(serializers.SerializerMetaclass, cls).__new__(cls, name, bases, attrs)


class SoftDeletionSerializer(metaclass=SoftDeletionMetaclass):
    """ To make things easier, this is class to inherit from for all serialziers, which should have
    is_archived behaviour

    ** Be aware ** : you should read note for the SoftDeletionMetaclass

    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Not Admin user shouldn't see is_archived field
        if (not self.context.get("request") or not hasattr(self.context.get("request"), 'user')
            or not self.context.get("request").user or
                not self.context.get("request").user.is_admin_role):
            self.fields.pop("is_deleted")

    def get_is_deleted(self, obj):
        return bool(obj.deleted)
