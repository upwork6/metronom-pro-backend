"""Update the context sent to all templates with every request."""

import os
import re
import sys
from datetime import datetime

import django
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site

User = get_user_model()


def admin_settings(request):
    """Collect settings for the admin."""

    python_version = "%s.%s.%s" % (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)

    logged_in_users = User.objects.exclude(signed_in=None)

    site_url = "-"

    try:
        site_url = get_current_site(request)
    except Site.DoesNotExist:
        pass

    ctx = {
        'STAGE': settings.STAGE,
        'MANAGEMENT_URLS': settings.MANAGEMENT_URLS,
        'FRONTEND_URL': settings.FRONTEND_URL,
        'PROJECT_NAME': settings.PROJECT_NAME,
        'django_version': django.get_version(),
        'python_version': python_version,
        'SITE_URL': site_url,
        'HOST_URL': request.get_host(),
        'logged_in_users': logged_in_users,
        'GIT_COMMIT_HASH': getattr(settings, 'METRONOM_INFO_SETTINGS', None).get('GIT_COMMIT_HASH')
    }

    SENTRY_DSN = os.environ.get('SENTRY_DSN', None)

    if SENTRY_DSN:

        try:
            regex_result = re.search('https://(.*):(.*)@(.*)', SENTRY_DSN)

            sentry_frontend_dsn = str.replace(SENTRY_DSN, ":" + regex_result.group(2), "")

            ctx.update({
                'SENTRY_FRONTEND_DSN': sentry_frontend_dsn,
            })

        except AttributeError:
            print("The SENTRY_DSN is in the wrong format, skipping: {}".format(SENTRY_DSN))

    if hasattr(request, "tenant") and request.tenant:
        ctx.update({
            'TENANT_NAME': request.tenant.name,
        })

    return ctx
