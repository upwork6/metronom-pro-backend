from collections import defaultdict
from functools import wraps

from django.conf import settings
from django.core.cache import caches
from django.db.models.signals import m2m_changed, post_delete, post_save

from . import tasks
from .signals import model_instances_changed


default_cache = caches['default']


class ModelSerializerCacheController(object):
    """A class that encapsulated the logic of storing and getting data of model serializator"""

    def __init__(self, serializer_cls, cache_alias='default', cache_timeout=None):
        self.serializer_cls = serializer_cls
        self.cache = caches[cache_alias]
        if not cache_timeout:
            cache_timeout = settings.CACHES[cache_alias]['TIMEOUT']
        self.cache_timeout = cache_timeout

    def _get_cache_key(self, model_instance_pk):
        return f'{self.serializer_cls.__name__}#{model_instance_pk}'

    def invalidate_cache(self, instance):
        key = self._get_cache_key(instance.pk)
        self.cache.delete(key)
        serializer = self.serializer_cls(instance)
        new_value = serializer.to_representation(instance)
        self.cache.set(key, new_value, self.cache_timeout)

    def get_from_cache(self, instance):
        cache_key = self._get_cache_key(instance.pk)
        cached = self.cache.get(cache_key)
        return cached

    def save_to_cache(self, instance_id, value):
        cache_key = self._get_cache_key(instance_id)
        self.cache.set(cache_key, value, self.cache_timeout)


class ModelSerializerCacheControllerRegister(object):
    """
    A class that adds caching (whole logic such as storing, getting, invalidation) to model serializer.
    Example:
        # some module serializers.py:
        ModelSerializerCacheControllerRegister.register_serializer(CompanyContactListSerializer, cache_timeout=3600)
    IMPORTANT:
        You must run signal `model_instances_changed` in all cases when serialized model instance is changed.
    """

    _cache_controllers_of_models = defaultdict(list)

    @classmethod
    def register_serializer(cls, serializer_cls, cache_alias: str='default', cache_timeout=None):
        """
        Adds caching for particular serializer class.
        :param serializer_cls: A class of serializer class.
        :param cache_alias: alias name of cache which will be used for storing cached data.
        :return: None
        """

        def _decorate_to_representation(cache_controller):
            def wrapper(f):
                @wraps(f)
                def wrapped(self, instance):
                    cached = cache_controller.get_from_cache(instance)
                    if cached:
                        return cached
                    else:
                        serialized = f(self, instance)
                        cache_controller.save_to_cache(instance.pk, serialized)
                        return serialized

                return wrapped
            return wrapper

        cache_controller = ModelSerializerCacheController(serializer_cls, cache_alias, cache_timeout)
        serializer_cls.to_representation = _decorate_to_representation(
            cache_controller
        )(serializer_cls.to_representation)

        model_cls = serializer_cls.Meta.model

        cls._register_invalidation_cache_map(cache_controller, model_cls)

        cls._register_invalidation_cache_signals(model_cls)

    @classmethod
    def _register_invalidation_cache_signals(cls, model_cls):
        post_save.connect(post_save_or_delete_or_m2m_changed_signal_receiver, sender=model_cls)
        post_delete.connect(post_save_or_delete_or_m2m_changed_signal_receiver, sender=model_cls)
        m2m_changed.connect(post_save_or_delete_or_m2m_changed_signal_receiver, sender=model_cls)
        model_instances_changed.connect(model_instances_changed_signal_receiver, sender=model_cls)

    @classmethod
    def _register_invalidation_cache_map(cls, cache_controller, model_cls):
        cls._cache_controllers_of_models[model_cls].append(cache_controller)

    @classmethod
    def invalidate_cache(cls, model_instances):
        for instance in model_instances:
            cache_controllers = cls._cache_controllers_of_models[instance.__class__]
            for cc in cache_controllers:
                cc.invalidate_cache(instance)


def model_instances_changed_signal_receiver(sender, instances: list, **kwargs):
    """Receiver for custom signal `model_instances_changed`.
    It run celery task which invalidates cache of model serializator.
    """
    if instances:
        app_label = instances[0]._meta.app_label
        model_name = instances[0]._meta.object_name
        task_kwargs = dict(
            app_label=app_label,
            model_name=model_name,
            instance_ids=[x.pk for x in instances],
        )
        tasks.invalidate_model_serializer_cache.apply_async(kwargs=task_kwargs)


def post_save_or_delete_or_m2m_changed_signal_receiver(sender, instance, **kwargs):
    """Receiver for signals:
        - post_save
        - post_delete
        - m2m_changed
     It run celery task which invalidates cache of model serializator.
     """
    app_label = instance._meta.app_label
    model_name = instance._meta.object_name
    task_kwargs = dict(
        app_label=app_label,
        model_name=model_name,
        instance_ids=[instance.pk],
    )
    tasks.invalidate_model_serializer_cache.apply_async(kwargs=task_kwargs)
