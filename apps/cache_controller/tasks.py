from config.celery import app


@app.task(bind=True)
def invalidate_model_serializer_cache(self, app_label: str, model_name: str, instance_ids: list):
    # This import (ContentType) here because celery initialize all tasks before loading all models.
    # If we use import in global level we get
    # an error "django.core.exceptions.AppRegistryNotReady: Apps aren't loaded yet."
    from django.contrib.contenttypes.models import ContentType
    from .serializer_cache_controllers import ModelSerializerCacheControllerRegister
    content_type = ContentType.objects.get_by_natural_key(app_label, model_name.lower())
    model_cls = content_type.model_class()
    instances = model_cls.objects.filter(pk__in=instance_ids)
    ModelSerializerCacheControllerRegister.invalidate_cache(instances)
    return True
