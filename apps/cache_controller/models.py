from django.db import models
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from .signals import model_instances_changed


class DummyArticle(models.Model):
    title = models.CharField(max_length=200, unique=True)
    author = models.ForeignKey('DummyAuthor')


class DummyAuthor(models.Model):
    name = models.CharField(max_length=100)


@receiver(post_save, sender=DummyAuthor)
@receiver(post_delete, sender=DummyAuthor)
def notify_about_dummy_article_changes(sender, instance, **kwargs):
    changed_author = instance
    articles_related_with_author = DummyArticle.objects.filter(author=changed_author)
    if articles_related_with_author:
        model_instances_changed.send(sender=DummyArticle, instances=articles_related_with_author)
