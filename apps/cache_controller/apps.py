from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CacheControllerConfig(AppConfig):
    name = 'apps.cache_controller'
    verbose_name = _('Cache controller')
