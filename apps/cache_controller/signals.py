from django.dispatch import Signal

model_instances_changed = Signal(providing_args=['instances'])
