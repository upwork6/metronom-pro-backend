from rest_framework import serializers

from ..models import DummyArticle, DummyAuthor


class DummyAuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = DummyAuthor
        fields = ['name']


class DummyArticleSerializer(serializers.ModelSerializer):
    author = DummyAuthorSerializer()

    class Meta:
        model = DummyArticle
        fields = [
            'title',
            'author',
        ]
