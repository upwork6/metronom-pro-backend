import time

from django.core.cache import caches
from django.test import TestCase

from ..models import DummyArticle, DummyAuthor
from ..serializer_cache_controllers import \
    ModelSerializerCacheControllerRegister
from .dummy_serializers import DummyArticleSerializer


default_cache = caches['default']


class TestSerializerCaching(TestCase):
    def setUp(self):
        default_cache.clear()
        self.author = DummyAuthor.objects.create(name='Author')
        self.article = DummyArticle.objects.create(
            title='Bla',
            author=self.author
        )
        ModelSerializerCacheControllerRegister.register_serializer(DummyArticleSerializer)

    def test_caching(self):
        serializer = DummyArticleSerializer(self.article)
        self.assertEqual(serializer.data['title'], 'Bla')

        # Changing model instance WITH running of SIGNALS
        self.article.title = 'Changed and ran of signals'
        self.article.save()
        self.article = DummyArticle.objects.get(title='Changed and ran of signals')
        serializer = DummyArticleSerializer(self.article)
        self.assertEqual(serializer.data['title'], 'Changed and ran of signals')

        # Changing RELATED model instance with running of signals
        self.article.author.name = 'Author changed and ran of signals'
        self.article.author.save()
        self.article = DummyArticle.objects.get(title='Changed and ran of signals')
        serializer = DummyArticleSerializer(self.article)
        self.assertEqual(serializer.data['author']['name'], 'Author changed and ran of signals')

        # Changing model instance WITHOUT running of SIGNALS
        DummyArticle.objects.filter(title='Changed and ran of signals')\
                            .update(title='Changed without running signals')
        article_changed_v2 = DummyArticle.objects.get(title='Changed without running signals')
        serializer = DummyArticleSerializer(article_changed_v2)
        self.assertEqual(serializer.data['title'], 'Changed and ran of signals')

    # def test_compare_time_with_and_without_caching(self):
    #     objects_count = 1000
    #     start_time = time.time()
    #
    #     for i in range(objects_count):
    #         DummyArticle.objects.create(
    #             title=f'Bla{i}',
    #             author=self.author
    #         )
    #
    #     default_cache.clear()
    #     serializer = DummyArticleSerializer(DummyArticle.objects.all(), many=True)
    #     serializer.data
    #
    #     end_time = time.time()
    #     duration = (end_time - start_time)
    #
    #     start_time_with_caching = time.time()
    #     serializer = DummyArticleSerializer(DummyArticle.objects.all(), many=True)
    #     serializer.data
    #     end_time_with_caching = time.time()
    #     duration_with_caching = (end_time_with_caching - start_time_with_caching)
    #
    #     print('***************')
    #     print(f'Test gets {objects_count} objects')
    #     print(f'(Getting from database) Runs in {duration} sec.')
    #     print(f'(Getting from cache) Runs in {duration_with_caching} sec.')
    #     speed_faster_times = round(duration / duration_with_caching, 1)
    #     print(f'Usage with cache {speed_faster_times} times faster.')
    #     print('***************')
    #
