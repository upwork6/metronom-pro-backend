
from ast import literal_eval
from datetime import date

from dateutil.relativedelta import relativedelta
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from apps.backoffice.models import (AgencyBankAccount, AvailableSetting,
                                    Setting, TaxNote, TermsCollection,
                                    TermsParagraph, TermsParagraphCollection,
                                    ThankYouNote)
from apps.bankaccounts.serializers import AbstractBankAccountSerializer
from apps.metronom_commons.data import CURRENCY
from apps.metronom_commons.serializers import SoftDeletionSerializer


class SimpleAvailableSettingSerialzier(SoftDeletionSerializer, serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)

    class Meta:
        model = AvailableSetting
        fields = ('name', 'uuid', 'value_type')
        extra_kwargs = {
            'name': {'read_only': True},
            'value_type': {'read_only': True},
        }


class SettingListSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    name = serializers.CharField(source="setting_to_override.name", read_only=True)
    eval_value = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Setting
        fields = [
            'value',
            'name',
            'eval_value'
        ]

    def get_eval_value(self, obj):
        try:
            data = literal_eval(obj.value)
        except (SyntaxError, ValueError):
            data = obj.value
        return data

    def object_to_dict(self, item):
        return {
            'value': item.value,
        }

    @staticmethod
    def setup_eager_loading(queryset):
        """
        Perform necessary eager loading of data.
        """
        queryset = queryset.select_related(
            'setting_to_override'
        )

        return queryset


class SettingPageListSerializer(SettingListSerializer):
    created_datetime = serializers.DateTimeField(source="created_at", read_only=True)
    value_uuid = serializers.UUIDField(source="id", read_only=True)
    name = serializers.CharField(source="setting_to_override.name", read_only=True)
    time_frame = serializers.CharField(source="setting_to_override.editable_in_time_frame", read_only=True)
    value_type = serializers.CharField(source="setting_to_override.value_type", read_only=True)
    past_values = serializers.SerializerMethodField(read_only=True)
    upcoming_values = serializers.SerializerMethodField(read_only=True)
    default_value = serializers.CharField(source="setting_to_override.default_value", read_only=True)
    eval_value = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Setting
        fields = SettingListSerializer.Meta.fields + [
            'start_date',
            'created_datetime',
            'value_uuid',
            'time_frame',
            'past_values',
            'upcoming_values',
            'default_value',
            'value_type',
        ]

    def object_to_dict(self, item):
        return {
            'value_uuid': str(item.id),
            'value': item.value,
            'created_datetime': item.created_at.strftime('%Y-%m-%d %H:%M'),
            'start_date': item.start_date.strftime('%Y-%m-%d')
        }

    def get_past_values(self, obj):
        return list(map(self.object_to_dict, obj.setting_to_override.get_past_settings()))

    def get_upcoming_values(self, obj):
        return list(map(self.object_to_dict, obj.setting_to_override.get_upcoming_settings()))


class SettingSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    setting_to_override = SimpleAvailableSettingSerialzier(read_only=True)
    name = serializers.CharField(write_only=True, required=True)
    eval_value = serializers.SerializerMethodField(read_only=True)
    value_type = serializers.CharField(source="setting_to_override.value_type", read_only=True)

    class Meta:
        model = Setting
        fields = (
            'setting_to_override',
            'value',
            'value_type',
            'eval_value',
            'start_date',
            'name',
        )
        extra_kwargs = {
            'start_date': {'required': True},
            'value': {'write_only': True, 'required': True},
        }

    def get_eval_value(self, obj):
        try:
            data = literal_eval(obj.value)
        except (SyntaxError, ValueError):
            data = obj.value
        return data

    def validate_name(self, data):
        if not AvailableSetting.objects.filter(name=data).exists():
            raise serializers.ValidationError(_("No available setting with such name exists"))
        return data

    def validate_start_date(self, data):
        if data < date.today():
            raise serializers.ValidationError(_("You can't create setting in the past"))
        return data

    def validate_value(self, data):
        try:
            literal_eval(data)
        except (SyntaxError, ValueError):
            try:
                literal_eval("'%s'" % data)
            except (SyntaxError, ValueError):
                raise serializers.ValidationError(
                    _("""
                        This data isn't valid python structure.
                        Consider next steps: if you send a string add additional ''. Like this: "'VALUE'".
                        if you send other structre, send them like this: "['a', 'b', 'c']" or "(1,2,3)"
                    """
                      )
                )
        return data

    def validate(self, data):
        setting_to_override_name = data.get('name')
        if Setting.objects.filter(setting_to_override__name=setting_to_override_name).exists():
            setting_to_override = AvailableSetting.objects.get(name=setting_to_override_name)
            start_date = data['start_date']
            from_date = Setting.objects.filter(
                setting_to_override=setting_to_override
            ).order_by('-start_date').first().start_date
            if setting_to_override.editable_in_time_frame == AvailableSetting.MONTHLY:
                next_date = (from_date + relativedelta(months=1)).replace(day=1)
                if start_date < next_date or start_date.day != 1:
                    next_date = next_date.strftime('%Y-%m-%d')
                    raise serializers.ValidationError(
                        _("Start date should be not earlier than {} "
                          "and it should be 1st day of the month".format(next_date)
                          )
                    )
            elif setting_to_override.editable_in_time_frame == AvailableSetting.YEARLY:
                next_date = (from_date + relativedelta(years=1)).replace(day=1, month=1)
                if start_date < next_date or start_date.day != 1:
                    next_date = next_date.strftime('%Y-%m-%d')
                    raise serializers.ValidationError(
                        _("Start date should be not earlier than {} "
                          "and it should be 1st day of the year".format(next_date)
                          )
                    )
        return data

    def create(self, data):
        setting_to_override_name = data.pop("name")
        setting = AvailableSetting.objects.get(name=setting_to_override_name)
        data['setting_to_override'] = setting
        instance = super().create(data)
        instance.created_by = self.context['request'].user
        instance.changed_by = self.context['request'].user
        instance.save(update_fields=["created_by", "changed_by"])
        return instance


class TaxNoteSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)
    default_in_currency = serializers.ChoiceField(allow_blank=True, choices=CURRENCY.CHOICES, required=False)

    class Meta:
        model = TaxNote
        fields = ('uuid', 'name', 'used_for_tax', 'default_in_currency')

    def validate_default_in_currency(self, data):
        if data and TaxNote.objects.filter(default_in_currency=data).exists():
            raise serializers.ValidationError(
                _("There is already one default Tax note using currency {}, "
                  "please remove that one first!".format(data))
            )
        return data


class ThankYouNoteSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)

    class Meta:
        model = ThankYouNote
        fields = ('uuid', 'name')


class AgencyBankAccountSerializer(AbstractBankAccountSerializer):
    class Meta(AbstractBankAccountSerializer.Meta):
        model = AgencyBankAccount


class TermsParagraphSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = TermsParagraph
        fields = ('id', 'name', 'text', 'is_archived')


class TermsParagraphOrderingSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(required=True)
    position = serializers.IntegerField(required=True)
    deleted = serializers.BooleanField(required=False, write_only=True)

    class Meta(TermsParagraphSerializer.Meta):
        model = TermsParagraph
        fields = TermsParagraphSerializer.Meta.fields + ('position', 'deleted')
        read_only_fields = ('name', 'text', 'is_archived')


class TermsCollectionSerializer(serializers.ModelSerializer):
    paragraphs = TermsParagraphOrderingSerializer(many=True, source='terms')

    class Meta(object):
        model = TermsCollection
        fields = ('id', 'name', 'paragraphs', 'is_archived')

    def create(self, validated_data):
        terms = validated_data.pop('terms', [])
        instance = TermsCollection.objects.create(**validated_data)

        for paragraph in terms:
            terms_paragraph = TermsParagraph.objects.get(pk=paragraph.get('id'))
            TermsParagraphCollection.objects.create(
                terms_paragraph=terms_paragraph,
                terms_collection=instance,
                position=paragraph.get('position')
            )

        return instance

    def update(self, instance, validated_data):
        terms = validated_data.pop('terms', [])

        instance.name = validated_data.get('name', instance.name)
        instance.is_archived = validated_data.get('is_archived', instance.is_archived)
        instance.save(update_fields=['name', 'is_archived'])
        for paragraph in terms:
            terms_paragraph = TermsParagraph.objects.get(pk=paragraph.get('id'))
            terms_paragraph_collection, __ = TermsParagraphCollection.objects.get_or_create(
                terms_paragraph=terms_paragraph,
                terms_collection=instance
            )
            if paragraph.get("deleted"):
                terms_paragraph_collection.delete()
            else:
                terms_paragraph_collection.position = paragraph.get('position')
                terms_paragraph_collection.save()

        return instance
