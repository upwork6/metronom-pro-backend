
from django.core.cache import caches
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from positions import PositionField

from apps.bankaccounts.models import AbstractBankAccountModel
from apps.metronom_commons.data import CURRENCY, EtagCaching
from apps.metronom_commons.models import (AdminMetronomBaseModel,
                                          OrderingMixin, SoftDeleteMixin,
                                          SoftDeleteUniqueMixin,
                                          UserAccessBackofficeMetronomBaseModel)
from apps.users.models import User

cache = caches['default']
ETAG_SETTINGS_CACHE_KEY = EtagCaching.ETAG_SETTINGS_CACHE_KEY


class SettingCategory(SoftDeleteMixin, AdminMetronomBaseModel):
    """ The category of the Setting, like accounting or PDF"""

    name = models.CharField(max_length=256, verbose_name=_('Category'))

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = _('Setting Category')
        verbose_name_plural = _('Setting Categories')


class AvailableSetting(SoftDeleteUniqueMixin, AdminMetronomBaseModel):
    IMMEDIATELY, MONTHLY, YEARLY = "immediately", "monthly", "yearly"

    CHANGE_CHOICES = (
        (IMMEDIATELY, _("Immediately")),
        (MONTHLY, _("Monthly")),
        (YEARLY, _("Yearly")),
    )
    _unique_fields_to_override_during_deletion = ["name"]

    name = models.CharField(max_length=256, verbose_name=_('Possible setting'), unique=True)
    default_value = models.CharField(max_length=256, verbose_name=_('Value'), blank=True, null=True)
    category = models.ForeignKey(SettingCategory, blank=True, null=True)
    editable_in_time_frame = models.CharField(choices=CHANGE_CHOICES, default=IMMEDIATELY, max_length=24)
    value_type = models.CharField(max_length=24, default="string")

    class Meta:
        verbose_name = _('Available Setting')
        verbose_name_plural = _('Available Settings')

    def __str__(self):
        return f"{self.name}"

    def get_current_setting(self):
        """ This method get the current setting for the availavle setting and returns it

        :returns: Setting instance or None
        """
        return self.setting_set.filter(start_date__lte=timezone.now()).order_by("-start_date", "-created_at").first()

    def get_past_settings(self):
        """ This method returns the queryset of the past expired settings

        :returns: queryset
        """
        qs = self.setting_set.filter(start_date__lte=timezone.now()).order_by("-start_date", "-created_at")
        # because current setting has start_date also in the past we need to exclude it
        current_setting = self.get_current_setting()
        if current_setting:
            qs = qs.exclude(id=current_setting.id)
        return qs

    def get_upcoming_settings(self):
        """ This method returns the queryset of the future settings

        :returns: queryset
        """
        qs = self.setting_set.filter(start_date__gt=timezone.now()).order_by("start_date", "created_at")
        return qs


class Setting(SoftDeleteMixin, AdminMetronomBaseModel):
    setting_to_override = models.ForeignKey(AvailableSetting)
    value = models.CharField(max_length=256, verbose_name=_('Value'))
    start_date = models.DateField(null=True)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="created_settings")
    changed_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="changed_settings")
    version = models.PositiveSmallIntegerField(default=1)

    class Meta:
        verbose_name = _('Setting')
        verbose_name_plural = _('Settings')

    def __str__(self):
        return f"{self.setting_to_override.name}: {self.value}"

    def save(self, *args, **kwargs):
        before_count = Setting.objects.filter(setting_to_override=self.setting_to_override).count()
        self.version = before_count + 1
        return super().save(*args, **kwargs)


class AgencyBankAccount(SoftDeleteMixin, UserAccessBackofficeMetronomBaseModel, AbstractBankAccountModel):
    is_default = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Agency bank account')
        verbose_name_plural = _('Agency bank accounts')


class TaxNote(SoftDeleteMixin, UserAccessBackofficeMetronomBaseModel, OrderingMixin):
    name = models.CharField(max_length=256, verbose_name=_('Name'))
    used_for_tax = models.BooleanField(default=False)
    default_in_currency = models.CharField(choices=CURRENCY.CHOICES, max_length=12, default="", blank=True)

    class Meta:
        verbose_name = _('Tax Note')
        verbose_name_plural = _('Tax Notes')

    def __str__(self):
        return f'{self.name}'

    def save(self, *args, **kwargs):
        if (self.default_in_currency and
                TaxNote.objects.filter(default_in_currency=self.default_in_currency).exclude(id=self.id).exists()):
            mes = "There is already one default Tax note using currency {}".format(self.default_in_currency)
            raise ValidationError(
                {
                    "__all__": _("{}, please remove that one first!".format(mes))
                }
            )
        return super().save(*args, **kwargs)


class ThankYouNote(SoftDeleteMixin, UserAccessBackofficeMetronomBaseModel):
    name = models.CharField(max_length=256, verbose_name=_('Name'))

    class Meta:
        verbose_name = _('Thank you Note')
        verbose_name_plural = _('Thank you Notes')

    def __str__(self):
        return f'{self.name}'


class TermsParagraph(SoftDeleteMixin, UserAccessBackofficeMetronomBaseModel, OrderingMixin):
    name = models.CharField(max_length=256, verbose_name=_('Name'))
    text = models.TextField(verbose_name=_('Text'))
    is_archived = models.BooleanField(verbose_name=_('Is archived'), default=False)

    class Meta(object):
        verbose_name = _('Terms Paragraph')
        verbose_name_plural = _('Terms Paragraphs')

    def __str__(self):
        return f'{self.name} ({self.is_archived})'


class TermsCollection(SoftDeleteMixin, UserAccessBackofficeMetronomBaseModel, OrderingMixin):
    name = models.CharField(max_length=256, verbose_name=_('Name'))
    terms = models.ManyToManyField(TermsParagraph, through='TermsParagraphCollection',
                                   related_name='terms_paragraphs')
    is_archived = models.BooleanField(verbose_name=_('Is archived'), default=False)

    class Meta(object):
        verbose_name = _('Terms Collection')
        verbose_name_plural = _('Terms Collections')

    def __str__(self):
        return f'{self.name} ({self.is_archived})'


class TermsParagraphCollection(UserAccessBackofficeMetronomBaseModel):
    terms_paragraph = models.ForeignKey(TermsParagraph, verbose_name=_("Paragraph"))
    terms_collection = models.ForeignKey(TermsCollection, verbose_name=_("Terms"))
    position = PositionField(collection='terms_collection')

    class Meta(object):
        unique_together = ('terms_paragraph', 'terms_collection')
        verbose_name = _('Terms Paragraph Collection')
        verbose_name_plural = _('Terms Paragraph Collections')


@receiver(post_delete, sender=Setting)
def reset_etag(sender, instance, *args, **kwargs):
    cache.set(ETAG_SETTINGS_CACHE_KEY, "")
    cache_key = "{}_{}".format(ETAG_SETTINGS_CACHE_KEY, instance.pk)
    cache.set(cache_key, "")
