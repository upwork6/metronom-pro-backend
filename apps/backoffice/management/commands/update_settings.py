
from datetime import date

from django.conf import settings
from django.core.management.base import BaseCommand

from apps.backoffice.models import AvailableSetting, Setting


class Command(BaseCommand):
    help = "This command parse METRONOM_SETTINGS and create AvailableSettings from it"

    def handle(self, *args, **kwargs):
        METRONOM_SETTINGS = settings.METRONOM_SETTINGS

        for key, value_json in METRONOM_SETTINGS.items():

            value = value_json.get('value')
            value_type = type(value).__name__
            instance, created = AvailableSetting.objects.get_or_create(
                name=key,
                defaults={'default_value': value, 'value_type': value_type}
            )
            if not created:
                instance.default_value = value
                instance.value_type = value_type
                instance.save(update_fields=['default_value', 'value_type'])
            editable_in_time_frame = value_json.get("editable_in_time_frame")
            if editable_in_time_frame is not None:
                instance.editable_in_time_frame = editable_in_time_frame
                instance.save(update_fields=['editable_in_time_frame'])

            if not Setting.objects.filter(setting_to_override=instance).count():
                Setting.objects.create(
                    setting_to_override=instance,
                    start_date=date.today(),
                    value=value
                )
                print(f"Setting for {key} created")
