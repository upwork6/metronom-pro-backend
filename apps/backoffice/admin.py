from django.contrib import admin

from apps.backoffice.models import (AvailableSetting, Setting, SettingCategory, TaxNote,
                                    TermsCollection, TermsParagraph,
                                    TermsParagraphCollection, AgencyBankAccount, ThankYouNote)


@admin.register(SettingCategory)
class SettingCategoryAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'name',
    )
    list_filter = ('created_at', 'updated_at')
    search_fields = ('name',)


@admin.register(AvailableSetting)
class AvailableSettingAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'name',
        'default_value',
        'category',
        'editable_in_time_frame'
    )
    list_filter = ('created_at', 'updated_at', 'category', 'editable_in_time_frame')
    search_fields = ('name',)


@admin.register(Setting)
class SettingAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'setting_to_override',
        'value',
        'start_date',
        'created_by',
        'changed_by',
        'version',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'setting_to_override',
        'start_date',
        'created_by',
        'changed_by',
    )


@admin.register(AgencyBankAccount)
class AgencyBankAccountAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'updated_at',
        'bank_name',
        'account_name',
        'owner_name',
        'bic',
        'iban',
    )
    list_filter = ('created_at', 'updated_at')


@admin.register(TaxNote)
class TaxNoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'updated_at', 'name')
    list_filter = ('created_at', 'updated_at')
    search_fields = ('name',)


@admin.register(ThankYouNote)
class ThankYouNoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'updated_at', 'name')
    list_filter = ('created_at', 'updated_at')
    search_fields = ('name',)


@admin.register(TermsParagraph)
class TermsParagraphAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'text',
        'is_archived',
        'created_at',
        'updated_at',
    )
    list_filter = ('created_at', 'updated_at', 'is_archived')
    search_fields = ('name',)


@admin.register(TermsCollection)
class TermsCollectionAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'is_archived',
        'created_at',
        'updated_at',
    )
    list_filter = ('created_at', 'updated_at', 'is_archived')
    raw_id_fields = ('terms',)
    search_fields = ('name',)


@admin.register(TermsParagraphCollection)
class TermsParagraphCollectionAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'terms_paragraph',
        'terms_collection',
        'position',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'terms_paragraph',
        'terms_collection',
    )
