# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-02 12:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backoffice', '0008_auto_20180501_1505'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='agencybankaccount',
            name='created_date',
        ),
        migrations.RemoveField(
            model_name='agencybankaccount',
            name='updated_date',
        ),
        migrations.RemoveField(
            model_name='availablesetting',
            name='created_date',
        ),
        migrations.RemoveField(
            model_name='availablesetting',
            name='updated_date',
        ),
        migrations.RemoveField(
            model_name='setting',
            name='created_date',
        ),
        migrations.RemoveField(
            model_name='setting',
            name='updated_date',
        ),
        migrations.RemoveField(
            model_name='settingcategory',
            name='created_date',
        ),
        migrations.RemoveField(
            model_name='settingcategory',
            name='updated_date',
        ),
        migrations.RemoveField(
            model_name='taxnote',
            name='created_date',
        ),
        migrations.RemoveField(
            model_name='taxnote',
            name='updated_date',
        ),
        migrations.RemoveField(
            model_name='termscollection',
            name='created_date',
        ),
        migrations.RemoveField(
            model_name='termscollection',
            name='updated_date',
        ),
        migrations.RemoveField(
            model_name='termsparagraph',
            name='created_date',
        ),
        migrations.RemoveField(
            model_name='termsparagraph',
            name='updated_date',
        ),
        migrations.RemoveField(
            model_name='termsparagraphcollection',
            name='created_date',
        ),
        migrations.RemoveField(
            model_name='termsparagraphcollection',
            name='updated_date',
        ),
        migrations.RemoveField(
            model_name='thankyounote',
            name='created_date',
        ),
        migrations.RemoveField(
            model_name='thankyounote',
            name='updated_date',
        ),
        migrations.AlterField(
            model_name='agencybankaccount',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='agencybankaccount',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified'),
        ),
        migrations.AlterField(
            model_name='availablesetting',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='availablesetting',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified'),
        ),
        migrations.AlterField(
            model_name='setting',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='setting',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified'),
        ),
        migrations.AlterField(
            model_name='settingcategory',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='settingcategory',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified'),
        ),
        migrations.AlterField(
            model_name='taxnote',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='taxnote',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified'),
        ),
        migrations.AlterField(
            model_name='termscollection',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='termscollection',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified'),
        ),
        migrations.AlterField(
            model_name='termsparagraph',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='termsparagraph',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified'),
        ),
        migrations.AlterField(
            model_name='termsparagraphcollection',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='termsparagraphcollection',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified'),
        ),
        migrations.AlterField(
            model_name='thankyounote',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='thankyounote',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified'),
        ),
    ]
