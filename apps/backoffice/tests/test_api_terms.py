import copy

from rest_framework import status
from rest_framework.reverse import reverse

from apps.backoffice.tests.factories import (TermsCollectionFactory, TermsParagraphCollectionFactory,
                                             TermsParagraphFactory)
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.offers.tests.factories import CampaignOfferFactory, OfferFactory
from apps.projects.tests.factories import ProjectFactory


class TestTermsParagraphList(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.paragraphs = TermsParagraphFactory.create_batch(size=2)
        cls.endpoint = reverse('api_backoffice:terms-paragraph-list')
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_data_length(self):
        assert len(self.response.data) == 2

    def test_response_data(self):
        names = [x['name'] for x in self.response.data]
        texts = [x['text'] for x in self.response.data]

        for paragraph in self.paragraphs:
            self.assertIn(paragraph.name, names)
            self.assertIn(paragraph.text, texts)

    def test_permission(self):
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestTermsParagraphPost(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.login_as_admin()

        cls.post_data = dict(
            name='paragraph 1',
            text='paragraph text 1',
            is_archived=False
        )

        cls.endpoint = reverse('api_backoffice:terms-paragraph-list')
        cls.response = cls.admin_client.post(cls.endpoint, cls.post_data, format='json')

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_paragraph_name(self):
        assert self.response.data['name'] == self.post_data.get('name')

    def test_paragraph_text(self):
        assert self.response.data['text'] == self.post_data.get('text')

    def test_paragraph_is_archived_false(self):
        assert self.response.data['is_archived'] == self.post_data.get('is_archived')

    def test_paragraph_is_archived_true(self):
        post_data = copy.copy(self.post_data)
        post_data['is_archived'] = True

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        assert response.data['is_archived'] == post_data.get('is_archived')

    def test_permission(self):
        response = self.user_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 201)
        response = self.admin_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 201)


class TestTermsParagraphDetail(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestTermsParagraphDetail, cls).setUpTestData()
        cls.login_as_admin()

        cls.paragraph = TermsParagraphFactory()
        cls.endpoint = reverse('api_backoffice:terms-paragraph-detail', kwargs={'pk': cls.paragraph.pk})
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_paragraph_name(self):
        assert self.response.data['name'] == self.paragraph.name

    def test_paragraph_text(self):
        assert self.response.data['text'] == self.paragraph.text

    def test_paragraph_is_archived_false(self):
        assert self.response.data['is_archived'] == self.paragraph.is_archived

    def test_paragraph_is_archived_true(self):
        archived_paragraph = TermsParagraphFactory(is_archived=True)
        endpoint = reverse('api_backoffice:terms-paragraph-detail', kwargs={'pk': archived_paragraph.pk})
        response = self.admin_client.get(endpoint)
        assert response.data['is_archived'] == archived_paragraph.is_archived
        assert response.data['is_archived']

    def test_permission(self):
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestTermsParagraphPatch(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestTermsParagraphPatch, cls).setUpTestData()
        cls.patch_data = dict(
            name='paragraph name updated',
        )

        cls.paragraph = TermsParagraphFactory()
        cls.endpoint = reverse('api_backoffice:terms-paragraph-detail', kwargs={'pk': cls.paragraph.pk})
        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format='json')

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_patch_name(self):
        assert self.response.data.get('name') == self.patch_data.get('name')

    def test_patch_text(self):
        patch_data = dict(text='paragraph text updated')
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert response.data.get('text') == patch_data.get('text')

    def test_patch_is_archived_true(self):
        patch_data = dict(is_archived=True)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert response.data.get('is_archived') == patch_data.get('is_archived')

    def test_patch_is_archived_false(self):
        patch_data = dict(is_archived=False)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert response.data.get('is_archived') == patch_data.get('is_archived')

    def test_permission_patch_terms_not_used(self):
        response = self.user_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_permission_patch_terms_paragraph_used(self):
        collection = TermsCollectionFactory()
        TermsParagraphCollectionFactory(
            terms_paragraph=self.paragraph,
            terms_collection=collection,
            position=1
        )
        response = self.backoffice_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 200)


class TestTermsParagraphDelete(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestTermsParagraphDelete, cls).setUpTestData()
        cls.paragraph = TermsParagraphFactory()
        cls.endpoint = reverse('api_backoffice:terms-paragraph-detail', kwargs={'pk': cls.paragraph.pk})

    def test_non_admin_cannot_delete(self):
        response = self.client.delete(self.endpoint)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_paragraph_delete(self):
        response = self.admin_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_permissions_delete_terms_paragraph_not_used(self):
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)
        paragraph = TermsParagraphFactory()
        endpoint = reverse('api_backoffice:terms-paragraph-detail', kwargs={
            'pk': paragraph.pk
        })
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_delete_terms_paragraph_used(self):
        paragraph = TermsParagraphFactory()
        collection = TermsCollectionFactory()
        TermsParagraphCollectionFactory(
            terms_paragraph=paragraph,
            terms_collection=collection,
            position=1
        )
        endpoint = reverse('api_backoffice:terms-paragraph-detail', kwargs={
            'pk': paragraph.pk
        })
        response = self.backoffice_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used TermsParagraph"]
            }
        )


class TestTermsCollectionList(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestTermsCollectionList, cls).setUpTestData()
        cls.login_as_admin()
        cls.paragraphs = TermsParagraphFactory.create_batch(size=3)
        cls.terms_collections = TermsCollectionFactory.create_batch(size=2)
        TermsParagraphCollectionFactory(
            terms_paragraph=cls.paragraphs[0], terms_collection=cls.terms_collections[0]
        )
        TermsParagraphCollectionFactory(
            terms_paragraph=cls.paragraphs[1], terms_collection=cls.terms_collections[0]
        )
        TermsParagraphCollectionFactory(
            terms_paragraph=cls.paragraphs[2], terms_collection=cls.terms_collections[0]
        )

        TermsParagraphCollectionFactory(
            terms_paragraph=cls.paragraphs[0], terms_collection=cls.terms_collections[1]
        )
        TermsParagraphCollectionFactory(
            terms_paragraph=cls.paragraphs[1], terms_collection=cls.terms_collections[1]
        )

        cls.endpoint = reverse('api_backoffice:term-list')
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_data_length(self):
        assert len(self.response.data) == 2

    def test_name(self):
        names = [x['name'] for x in self.response.data]

        for term in self.terms_collections:
            self.assertIn(term.name, names)

    def test_is_archived(self):
        archives = [x['is_archived'] for x in self.response.data]

        for term in self.terms_collections:
            self.assertIn(term.is_archived, archives)

    def test_paragraphs(self):
        paragraphs_list = [x['paragraphs'] for x in self.response.data]

        assert len(paragraphs_list) == 2
        terms_set_length = [2, 3]
        assert len(paragraphs_list[0]) in terms_set_length
        assert len(paragraphs_list[1]) in terms_set_length

        for paragraphs in paragraphs_list:
            assert isinstance(paragraphs, list)
            for paragraph in paragraphs:
                self.assertTrue('id' in paragraph)
                self.assertTrue('name' in paragraph)
                self.assertTrue('is_archived' in paragraph)
                self.assertTrue('text' in paragraph)
                self.assertTrue('position' in paragraph)

    def test_permission(self):
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestTermsCollectionPost(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestTermsCollectionPost, cls).setUpTestData()
        cls.login_as_admin()
        cls.paragraphs = TermsParagraphFactory.create_batch(size=3)

        cls.post_data = dict(
            name='term collection name',
            is_archived=False,
            paragraphs=[
                dict(id=cls.paragraphs[0].pk, position=0),
                dict(id=cls.paragraphs[1].pk, position=1),
                dict(id=cls.paragraphs[2].pk, position=2),
            ]
        )

        cls.endpoint = reverse('api_backoffice:term-list')
        cls.response = cls.admin_client.post(cls.endpoint, cls.post_data, format='json')

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_response_length(self):
        assert isinstance(self.response.data.get('paragraphs'), list)
        assert len(self.response.data['paragraphs']) == 3

    def test_response_name(self):
        assert self.response.data.get('name') == self.post_data.get('name')

    def test_response_is_archived_false(self):
        assert self.response.data.get('is_archived') == self.post_data.get('is_archived')

    def test_response_is_archived_true(self):
        post_data = copy.copy(self.post_data)
        post_data['is_archived'] = True

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        assert response.data['is_archived'] == post_data.get('is_archived')

    def test_response_terms(self):
        paragraph_ids = [x['id'] for x in self.response.data['paragraphs']]
        for paragraph in self.paragraphs:
            self.assertIn(str(paragraph.pk), paragraph_ids)

    def test_permission(self):
        response = self.user_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 201)
        response = self.admin_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 201)


class TestTermsCollectionDetail(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestTermsCollectionDetail, cls).setUpTestData()
        cls.login_as_admin()

        cls.paragraphs = TermsParagraphFactory.create_batch(size=3)
        cls.terms_collection = TermsCollectionFactory()
        TermsParagraphCollectionFactory(
            terms_paragraph=cls.paragraphs[0], terms_collection=cls.terms_collection
        )
        TermsParagraphCollectionFactory(
            terms_paragraph=cls.paragraphs[1], terms_collection=cls.terms_collection
        )
        TermsParagraphCollectionFactory(
            terms_paragraph=cls.paragraphs[2], terms_collection=cls.terms_collection
        )

        cls.endpoint = reverse('api_backoffice:term-detail', kwargs={'pk': cls.terms_collection.pk})
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_length(self):
        assert isinstance(self.response.data.get('paragraphs'), list)
        assert len(self.response.data.get('paragraphs')) == 3

    def test_response_name(self):
        assert self.response.data.get('name') == self.terms_collection.name

    def test_response_is_archived_false(self):
        assert self.response.data.get('is_archived') == self.terms_collection.is_archived

    def test_response_terms(self):
        paragraph_ids = [x['id'] for x in self.response.data.get('paragraphs')]
        for paragraph in self.paragraphs:
            self.assertIn(str(paragraph.pk), paragraph_ids)

        for paragraph in self.response.data.get('paragraphs'):
            self.assertTrue('id' in paragraph)
            self.assertTrue('name' in paragraph)
            self.assertTrue('is_archived' in paragraph)
            self.assertTrue('text' in paragraph)
            self.assertTrue('position' in paragraph)

    def test_permission(self):
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestTermsCollectionPatch(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestTermsCollectionPatch, cls).setUpTestData()
        cls.login_as_admin()
        cls.paragraphs = TermsParagraphFactory.create_batch(size=3)
        cls.patch_data = dict(
            name='term collection name updated',
        )

        cls.terms_collection = TermsCollectionFactory()
        cls.endpoint = reverse('api_backoffice:term-detail', kwargs={'pk': cls.terms_collection.pk})
        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format='json')

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_patch_name(self):
        assert self.response.data.get('name') == self.patch_data.get('name')

    def test_patch_is_archived_true(self):
        patch_data = dict(is_archived=True)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert response.data.get('is_archived') == patch_data.get('is_archived')

    def test_patch_is_archived_false(self):
        patch_data = dict(is_archived=False)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert response.data.get('is_archived') == patch_data.get('is_archived')

    def test_patch_terms_update_and_delete(self):
        patch_data = dict(
            paragraphs=[
                dict(id=self.paragraphs[0].pk, position=0),
                dict(id=self.paragraphs[1].pk, position=1),
                dict(id=self.paragraphs[2].pk, position=2),
            ])

        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        assert len(response.data.get('paragraphs')) == 3
        paragraph_ids = [x['id'] for x in response.data['paragraphs']]

        for paragraph in self.paragraphs:
            self.assertIn(str(paragraph.pk), paragraph_ids)

        # now let's check possibility to delete the paragraph from the terms
        patch_data = dict(
            paragraphs=[
                dict(id=self.paragraphs[0].pk, deleted=True),
            ]
        )
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        assert len(response.data.get('paragraphs')) == 2
        paragraph_ids = [x['id'] for x in response.data.get('paragraphs')]
        
        for i, paragraph in enumerate(self.paragraphs):
            if not i:
                self.assertNotIn(str(paragraph.pk), paragraph_ids)
                continue
            self.assertIn(str(paragraph.pk), paragraph_ids)

    def test_permission_terms_collection_patch_not_used(self):
        response = self.user_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_permission_terms_collection_patch_used(self):
        campaign_offer = CampaignOfferFactory(
            terms_collection=self.terms_collection
        )
        response = self.backoffice_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 200)


class TestTermsCollectionDelete(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestTermsCollectionDelete, cls).setUpTestData()
        cls.paragraphs = TermsParagraphFactory.create_batch(size=2)
        cls.terms_collection = TermsCollectionFactory()
        cls.endpoint = reverse('api_backoffice:term-detail', kwargs={'pk': cls.terms_collection.pk})
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_non_admin_cannot_delete(self):
        response = self.client.delete(self.endpoint)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_paragraph_delete(self):
        response = self.admin_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_permissions_delete_terms_collection_not_used(self):
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)
        terms_collection = TermsCollectionFactory()
        endpoint = reverse('api_backoffice:term-detail', kwargs={
            'pk': terms_collection.pk
        })
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_delete_terms_collection_used(self):
        terms_collection = TermsCollectionFactory()
        campaign_offer = CampaignOfferFactory(
            terms_collection=terms_collection
        )
        endpoint = reverse('api_backoffice:term-detail', kwargs={
            'pk': terms_collection.pk
        })
        response = self.backoffice_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        campaign_offer.delete()

        project = ProjectFactory(billing_currency="CHF")

        OfferFactory(
            terms_collection=terms_collection,
            project=project
        )
        response = self.backoffice_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used TermsCollection"]
            }
        )
