from django.test import TestCase
from datetime import datetime, timedelta
from apps.backoffice.models import TermsCollection, TermsParagraph, TermsParagraphCollection, AvailableSetting, Setting
from apps.backoffice.utils import get_setting, get_metronom_setting
from django.conf import settings
from apps.metronom_commons.test import MetronomBaseAPITestCase
from decimal import Decimal


class TestTermsParagraphPosition(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.paragraph1 = TermsParagraph.objects.create(name="paragraph 1", text="paragraph text 1")
        cls.paragraph2 = TermsParagraph.objects.create(name="paragraph 2", text="paragraph text 2")
        cls.paragraph3 = TermsParagraph.objects.create(name="paragraph 3", text="paragraph text 3")
        cls.terms_collection1 = TermsCollection.objects.create(name="collection 1")
        cls.terms_collection2 = TermsCollection.objects.create(name="collection 2")

        TermsParagraphCollection.objects.create(
            terms_collection=cls.terms_collection1, terms_paragraph=cls.paragraph1
        )

        TermsParagraphCollection.objects.create(
            terms_collection=cls.terms_collection1, terms_paragraph=cls.paragraph2
        )

        cls.terms_paragraph_collection1 = TermsParagraphCollection.objects.create(
            terms_collection=cls.terms_collection1, terms_paragraph=cls.paragraph3
        )

        TermsParagraphCollection.objects.create(
            terms_collection=cls.terms_collection2, terms_paragraph=cls.paragraph1
        )

        cls.terms_paragraph_collection2 = TermsParagraphCollection.objects.create(
            terms_collection=cls.terms_collection2, terms_paragraph=cls.paragraph2
        )

        cls.actual_order = list(TermsParagraphCollection.objects.filter(
            terms_collection=cls.terms_collection1
        ).values_list('terms_paragraph__name', 'position').order_by('position'))

        cls.expected_order = [(u'paragraph 1', 0), (u'paragraph 2', 1), (u'paragraph 3', 2)]

    def test_terms_paragraph_position(self):
        self.assertEqual(self.actual_order, self.expected_order)

    def test_paragraph3_position_change(self):
        self.terms_paragraph_collection1.position = 1
        self.terms_paragraph_collection1.save()

        actual_order = list(TermsParagraphCollection.objects.filter(
            terms_collection=self.terms_collection1
        ).values_list('terms_paragraph__name', 'position').order_by('position'))

        expected_order = [(u'paragraph 1', 0), (u'paragraph 3', 1), (u'paragraph 2', 2)]

        self.assertEqual(actual_order, expected_order)

    def test_paragraph2_position_change_affects_current_collection_only(self):
        self.terms_paragraph_collection2.position = 0
        self.terms_paragraph_collection2.save()

        actual_order = list(TermsParagraphCollection.objects.filter(
            terms_collection=self.terms_collection2
        ).values_list('terms_paragraph__name', 'position').order_by('position'))

        expected_order = [(u'paragraph 2', 0), (u'paragraph 1', 1)]

        self.assertEqual(actual_order, expected_order)
        self.assertEqual(self.actual_order, self.expected_order)

    def test_deletion_position_change(self):
        paragraph1 = TermsParagraph.objects.create(name="paragraph 1", text="paragraph text 1")
        paragraph2 = TermsParagraph.objects.create(name="paragraph 2", text="paragraph text 2")
        paragraph3 = TermsParagraph.objects.create(name="paragraph 3", text="paragraph text 3")
        before_position = paragraph3.position
        before_count = TermsParagraph.objects.count()
        paragraph3.delete()
        self.assertEqual(before_count - 1, TermsParagraph.objects.count())
        paragraph4 = TermsParagraph.objects.create(name="paragraph 3", text="paragraph text 3")
        self.assertEqual(paragraph4.position, before_position + 1)


class TestGetSettingsMethod(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        AvailableSetting.objects.get_or_create(
            name="CURRENCY_CHOICES",
            defaults=dict(default_value="[('CHF', 'CHF'), ('USD','USD'), ('GBP','GBP'), ('EUR', 'EUR')]")
        )
        AvailableSetting.objects.get_or_create(
            name="DEFAULT_AGENCY_CURRENCY",
            defaults=dict(default_value="'CHF'")
        )
        cls.login_as_admin()
        cls.previous_day = datetime.today() - timedelta(days=1)
        cls.pdf_footer = "Example Company ⋅ Doing examples ⋅ Example, Street 123 ⋅ CH-8400 Winterthur</br>Telefon"

    def test_get_default_agency_currency_type(self):
        """ Check TYPE of value not only the value itself
        """
        to_override = AvailableSetting.objects.get(name="DEFAULT_AGENCY_CURRENCY")
        currency = "'GBP'"
        Setting.objects.create(
            setting_to_override=to_override,
            value=currency,
            created_by=self.test_admin,
            start_date=self.previous_day
        )

        get_settings_value = get_setting("DEFAULT_AGENCY_CURRENCY")
        settings_value = get_metronom_setting('DEFAULT_AGENCY_CURRENCY')
        assert get_settings_value == 'GBP'
        assert type(get_settings_value) == type(settings_value)

    def test_get_default_tax_type_from_available_setting(self):
        AvailableSetting.objects.get_or_create(
            name="DEFAULT_TAX",
            defaults=dict(default_value="'8.2'")
        )
        get_settings_tax = get_setting("DEFAULT_TAX")
        settings_tax = get_metronom_setting('DEFAULT_TAX')
        format_value = type(settings_tax)
        assert type(get_settings_tax) == type(settings_tax)
        assert isinstance(get_settings_tax, format_value)

    def test_get_default_tax_type_from_setting(self):
        AvailableSetting.objects.get_or_create(
            name="DEFAULT_TAX",
            defaults=dict(default_value="'8.2'")
        )
        tax_to_override = AvailableSetting.objects.get(name="DEFAULT_TAX")
        tax_value = "'8.8'"
        Setting.objects.create(
            setting_to_override=tax_to_override,
            value=tax_value,
            created_by=self.test_admin,
            start_date=self.previous_day
        )
        get_settings_tax = get_setting("DEFAULT_TAX")
        settings_tax = get_metronom_setting('DEFAULT_TAX')
        format_value = type(settings_tax)
        assert type(get_settings_tax) == type(settings_tax)
        assert isinstance(get_settings_tax, format_value)

    # this test catches the PDF Error `SyntaxError: invalid syntax`
    def test_get_pdf_footer_type_from_available_setting(self):
        AvailableSetting.objects.get_or_create(
            name="PDF_FOOTER",
            defaults=dict(default_value=self.pdf_footer)
        )
        get_setting_pdf_footer = get_setting("PDF_FOOTER")
        settings_pdf_footer = get_metronom_setting('PDF_FOOTER')
        format_value = type(settings_pdf_footer)
        assert type(get_setting_pdf_footer) == type(settings_pdf_footer)
        assert isinstance(get_setting_pdf_footer, format_value)

    # this test catches the PDF Error `SyntaxError: invalid syntax`
    def test_get_pdf_footer_type_from_setting(self):
        AvailableSetting.objects.get_or_create(
            name="PDF_FOOTER",
            defaults=dict(default_value=self.pdf_footer)
        )
        tax_to_override = AvailableSetting.objects.get(name="PDF_FOOTER")
        added_pdf_footer = f"{self.pdf_footer} Added Value"
        Setting.objects.create(
            setting_to_override=tax_to_override,
            value=added_pdf_footer,
            created_by=self.test_admin,
            start_date=self.previous_day
        )
        get_settings_tax = get_setting("PDF_FOOTER")
        settings_tax = get_metronom_setting('PDF_FOOTER')
        format_value = type(settings_tax)
        assert type(get_settings_tax) == type(settings_tax)
        assert isinstance(get_settings_tax, format_value)
