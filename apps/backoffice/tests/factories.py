import factory

from apps.backoffice.models import (AgencyBankAccount, TaxNote, TermsCollection, TermsParagraph,
                                    TermsParagraphCollection, ThankYouNote)


class TaxNoteFactory(factory.DjangoModelFactory):
    name = factory.Faker('name')

    class Meta:
        model = TaxNote


class ThankYouNoteFactory(factory.DjangoModelFactory):
    name = factory.Faker('name')

    class Meta:
        model = ThankYouNote


class AgencyBankAccountFactory(factory.DjangoModelFactory):
    account_name = factory.Iterator(["Privatkonto", "Standard-Konto", "Neues Konto"])
    bank_name = factory.Iterator(["Privatkonto", "Standard-Konto", "Neues Konto"])
    bic = factory.Sequence(lambda n: 'AEKTCH2{:04d}'.format(n))
    iban = factory.Sequence(lambda n: 'CH4450010517540732493{0}'.format(n))

    class Meta:
        model = AgencyBankAccount


class TermsParagraphFactory(factory.DjangoModelFactory):
    name = factory.Faker('name')
    text = factory.Sequence(lambda n: 'Paragraph text {0}'.format(n))
    is_archived = False

    class Meta(object):
        model = TermsParagraph


class TermsCollectionFactory(factory.DjangoModelFactory):
    name = factory.Faker('name')

    class Meta(object):
        model = TermsCollection


class TermsParagraphCollectionFactory(factory.DjangoModelFactory):
    terms_paragraph = factory.SubFactory(TermsParagraphFactory)
    terms_collection = factory.SubFactory(TermsCollectionFactory)
    position = 0

    class Meta(object):
        model = TermsParagraphCollection


class TermsParagraphWithCollectionFactory(TermsParagraphFactory):
    terms_paragraph_collection = factory.RelatedFactory(TermsParagraphCollectionFactory, 'terms_paragraph')
