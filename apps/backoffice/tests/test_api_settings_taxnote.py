
import copy
from datetime import date, datetime, timedelta

import pytest
from dateutil.relativedelta import relativedelta
from django.core.cache import caches
from django.urls import reverse

from apps.backoffice.models import (AgencyBankAccount, AvailableSetting,
                                    Setting, SettingCategory, TaxNote, ThankYouNote)
from apps.backoffice.tests.factories import (AgencyBankAccountFactory,
                                             TaxNoteFactory, ThankYouNoteFactory)
from apps.backoffice.utils import get_setting
from apps.metronom_commons.data import EtagCaching
from apps.metronom_commons.test import MetronomBaseAPITestCase

cache = caches['default']
ETAG_SETTINGS_CACHE_KEY = EtagCaching.ETAG_SETTINGS_CACHE_KEY


class BaseForTheSettings(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        category = SettingCategory.objects.create(name="Accounting")
        AvailableSetting.objects.get_or_create(name="SOME_CHOICES", category=category)
        AvailableSetting.objects.get_or_create(name="OTHER_CHOICES", category=category)


class TestSettingsList(BaseForTheSettings):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.list_url = reverse('api:setting-list')
        cls.response = cls.admin_client.get(cls.list_url)
        cls.setting_first = AvailableSetting.objects.first()
        cls.setting_last = AvailableSetting.objects.last()
        cls.setting_first.editable_in_time_frame = "monthly"
        cls.setting_last.editable_in_time_frame = "yearly"
        cls.setting_first.save(update_fields=["editable_in_time_frame"])
        cls.setting_last.save(update_fields=["editable_in_time_frame"])
        cls.setting1 = Setting.objects.create(
            setting_to_override=cls.setting_last,
            value="['USD']",
            created_by=cls.test_admin,
            changed_by=cls.test_admin,
            start_date=date.today()
        )
        cls.setting2 = Setting.objects.create(
            setting_to_override=cls.setting_first,
            value="['USD']",
            created_by=cls.test_admin,
            changed_by=cls.test_admin,
            start_date=date.today()
        )
        cls.setting3 = Setting.objects.create(
            setting_to_override=cls.setting_first,
            value="['USD']",
            created_by=cls.test_admin,
            changed_by=cls.test_admin,
            start_date=date.today() - timedelta(days=10)
        )
        cls.setting4 = Setting.objects.create(
            setting_to_override=cls.setting_first,
            value="['USD']",
            created_by=cls.test_admin,
            changed_by=cls.test_admin,
            start_date=date.today() + timedelta(days=10)
        )

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_response_contains_correct_data(self):
        response = self.admin_client.get(self.list_url)
        self.assertEqual(len(response.data), 2)  # one for each AvailableSetting used
        values = [x['value'] for x in response.data]
        self.assertIn(self.setting1.value, values)
        self.assertIn(self.setting2.value, values)
        self.assertTrue('value_uuid' in response.data[0])
        self.assertTrue('created_datetime' in response.data[0])
        self.assertTrue('name' in response.data[0])
        self.assertTrue('time_frame' in response.data[0])
        self.assertTrue('start_date' in response.data[0])
        self.assertTrue('past_values' in response.data[0])
        self.assertTrue('upcoming_values' in response.data[0])
        uuids = [x['value_uuid'] for x in response.data]
        self.assertTrue(str(self.setting1.id) in uuids)
        self.assertTrue(str(self.setting2.id) in uuids)
        self.assertFalse(str(self.setting3.id) in uuids)
        self.assertFalse(str(self.setting3.id) in uuids)
        names = [x['name'] for x in response.data]
        self.assertTrue(self.setting_first.name in names)
        self.assertTrue(self.setting_last.name in names)
        time_frames = [x['time_frame'] for x in response.data]
        self.assertTrue(self.setting_first.editable_in_time_frame in time_frames)
        self.assertTrue(self.setting_last.editable_in_time_frame in time_frames)
        past_values = [x['past_values'] for x in response.data if x['past_values'] and x['past_values'] != [{}]]
        self.assertEqual(len(past_values), 1)
        past_value = past_values[0][0]
        self.assertEqual(past_value['value_uuid'], str(self.setting3.id))
        self.assertEqual(past_value['value'], self.setting3.value)
        self.assertEqual(past_value['created_datetime'], self.setting3.created_at.strftime('%Y-%m-%d %H:%M'))
        self.assertEqual(past_value['start_date'], self.setting3.start_date.strftime('%Y-%m-%d'))
        upcoming_values = [x['upcoming_values']
                           for x in response.data if x['upcoming_values'] and x['upcoming_values'] != [{}]]
        self.assertEqual(len(upcoming_values), 1)
        upcoming_value = upcoming_values[0][0]
        self.assertEqual(upcoming_value['value_uuid'], str(self.setting4.id))
        self.assertEqual(upcoming_value['value'], self.setting4.value)
        self.assertEqual(upcoming_value['created_datetime'], self.setting4.created_at.strftime('%Y-%m-%d %H:%M'))
        self.assertEqual(upcoming_value['start_date'], self.setting4.start_date.strftime('%Y-%m-%d'))

    def test_permissions(self):
        """ Check that only groups that have access to this endpoint are Admin
        """
        response = self.user_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.admin_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

    @pytest.mark.skip(reason="Caching problem not solved")
    def test_etag(self):
        setting5 = Setting.objects.create(
            setting_to_override=self.setting_first,
            value="['USD']",
            created_by=self.test_admin,
            changed_by=self.test_admin,
            start_date=date.today() - timedelta(days=10)
        )
        etag = cache.get(ETAG_SETTINGS_CACHE_KEY, '')
        self.response = self.admin_client.get(self.list_url, HTTP_IF_NONE_MATCH=etag)
        self.assertEqual(self.response.status_code, 304)
        self.setting1.value = "['USD']"
        self.setting1.save(update_fields=['value'])
        self.response = self.admin_client.get(self.list_url, HTTP_IF_NONE_MATCH=etag)
        self.assertEqual(self.response.status_code, 200)
        etag = cache.get(ETAG_SETTINGS_CACHE_KEY, '')
        self.response = self.admin_client.get(self.list_url, HTTP_IF_NONE_MATCH=etag)
        self.assertEqual(self.response.status_code, 304)
        setting5.delete()
        self.response = self.admin_client.get(self.list_url, HTTP_IF_NONE_MATCH=etag)
        self.assertEqual(self.response.status_code, 200)
        etag = cache.get(ETAG_SETTINGS_CACHE_KEY, '')
        self.response = self.admin_client.get(self.list_url, HTTP_IF_NONE_MATCH=etag)
        self.assertEqual(self.response.status_code, 304)


class TestSettingsCreate(BaseForTheSettings):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.list_url = reverse('api:setting-list')
        cls.setting_to_override = AvailableSetting.objects.last()
        cls.post_data = {
            'name': cls.setting_to_override.name,
            'value': "['a', 'b', 'c']",
            'start_date': datetime.today().strftime('%Y-%m-%d')
        }
        cls.login_as_admin()
        cls.response = cls.admin_client.post(cls.list_url, cls.post_data, format="json")

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, 201)

    def test_response_contains_correct_data(self):
        setting = Setting.objects.last()
        self.assertEqual(setting.setting_to_override, self.setting_to_override)
        self.assertEqual(setting.value, self.post_data['value'])
        self.assertEqual(setting.created_by, self.test_admin)
        self.assertEqual(setting.changed_by, self.test_admin)

    def test_error_on_unexisting_setting(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['name'] = 'Not existing name'
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_cant_change_created_by(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['created_by'] = self.test_user.id
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        setting = Setting.objects.last()
        self.assertEqual(setting.created_by, self.test_admin)
        self.assertEqual(setting.changed_by, self.test_admin)

    def test_valid_setting_string(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['value'] = "USD"
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        setting = Setting.objects.order_by('-created_at')[0]
        url = reverse("api:setting-detail", args=[setting.id])
        response = self.admin_client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(get_setting(self.setting_to_override.name), "USD")
        post_data['value'] = "[1,2,3]"
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        setting = Setting.objects.order_by('-created_at')[0]
        url = reverse("api:setting-detail", args=[setting.id])
        response = self.admin_client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(get_setting(self.setting_to_override.name), [1, 2, 3])
        self.assertEqual(response.data['eval_value'], [1, 2, 3])

    def test_creation_with_past_date(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['start_date'] = (datetime.today() - timedelta(days=1)).strftime('%Y-%m-%d')
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_permissions(self):
        """ Check that only groups that have access to this endpoint are Admin
        """
        response = self.user_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.admin_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)

    def test_can_be_changed_in_time_frame(self):
        setting = self.setting_to_override
        initial_start_date = datetime.today()
        post_data = copy.deepcopy(self.post_data)
        setting.editable_in_time_frame = AvailableSetting.MONTHLY
        setting.save(update_fields=["editable_in_time_frame"])
        next_day = (initial_start_date.replace(day=3) + relativedelta(months=1)).strftime('%Y-%m-%d')
        post_data['start_date'] = next_day
        next_month = (initial_start_date + relativedelta(months=1)).replace(day=1).strftime('%Y-%m-%d')
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertErrorResponse(
            response,
            {'__all__': [f'Start date should be not earlier than {next_month} and it should be 1st day of the month']}
        )

        start_date = (initial_start_date + relativedelta(months=1)).replace(day=2).strftime('%Y-%m-%d')
        post_data['start_date'] = start_date
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertErrorResponse(
            response,
            {'__all__': [f'Start date should be not earlier than {next_month} and it should be 1st day of the month']}
        )
        setting.editable_in_time_frame = AvailableSetting.YEARLY
        setting.save(update_fields=["editable_in_time_frame"])
        post_data['start_date'] = next_month
        next_year = (initial_start_date + relativedelta(years=1)).replace(day=1, month=1).strftime('%Y-%m-%d')
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertErrorResponse(
            response,
            {'__all__': [f'Start date should be not earlier than {next_year} and it should be 1st day of the year']}
        )
        start_date = (initial_start_date + relativedelta(years=1)).replace(day=2, month=1).strftime('%Y-%m-%d')
        post_data['start_date'] = start_date
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertErrorResponse(
            response,
            {'__all__': [f'Start date should be not earlier than {next_year} and it should be 1st day of the year']}
        )
        setting.editable_in_time_frame = AvailableSetting.IMMEDIATELY
        setting.save(update_fields=["editable_in_time_frame"])

    def test_admin_savings(self):
        """ Because of possibility to write data directly in the admin we need to check how the values are 
        evaluated
        """
        setting_to_override = AvailableSetting.objects.create(
            name="CURRENCY_CHOICES_NEW",
            default_value="[('CHF', 'CHF'), ('EUR', 'EUR'), ('USD', 'USD'), ('GBP', 'GBP')]",
            category=SettingCategory.objects.last(),
            value_type='list'
        )
        setting1 = Setting.objects.create(
            value=["CHF", "USD"],
            setting_to_override=setting_to_override,
        )
        url = reverse("api:setting-detail", args=[setting1.id])
        response = self.admin_client.get(url)
        self.assertEqual(response.data['eval_value'], ["CHF", "USD"])
        setting1.value = "['CHF', 'USD']"
        setting1.save()
        response = self.admin_client.get(url)
        self.assertEqual(response.data['eval_value'], ["CHF", "USD"])
        setting1.value = "[('CHF', 'CHF')]"
        setting1.save()
        response = self.admin_client.get(url)
        self.assertEqual(response.data['eval_value'], [('CHF', 'CHF')])


class TestSettingsRetrieve(BaseForTheSettings):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.setting_to_override = AvailableSetting.objects.last()
        cls.login_as_admin()
        cls.setting1 = Setting.objects.create(
            setting_to_override=cls.setting_to_override,
            value="['USD']",
            created_by=cls.test_admin,
            changed_by=cls.test_admin
        )
        cls.url = reverse("api:setting-detail", args=[cls.setting1.id])
        cls.response = cls.admin_client.get(cls.url)

    def test_detail_response(self):
        self.assertEqual(self.response.status_code, 200)
        data = self.response.data
        self.assertEqual(len(data.keys()), 5)
        self.assertTrue('setting_to_override' in data)
        self.assertTrue('value_type' in data['setting_to_override'])

    def test_permissions(self):
        """ Check that only groups that have access to this endpoint are Admin
        """
        response = self.user_client.get(self.url)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.get(self.url)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.url)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.url)
        self.assertEqual(response.status_code, 403)
        response = self.admin_client.get(self.url)
        self.assertEqual(response.status_code, 200)

    @pytest.mark.skip(reason="Caching problem not solved")
    def test_etag(self):
        cache_key = "{}_{}".format(ETAG_SETTINGS_CACHE_KEY, self.setting1.pk)
        etag = cache.get(cache_key, '')
        response = self.admin_client.get(self.url, HTTP_IF_NONE_MATCH=etag)
        self.assertEqual(response.status_code, 304)
        self.setting1.value = "['USD']"
        self.setting1.save(update_fields=['value'])
        response = self.admin_client.get(self.url, HTTP_IF_NONE_MATCH=etag)
        self.assertEqual(response.status_code, 200)
        etag = cache.get(cache_key, '')
        response = self.admin_client.get(self.url, HTTP_IF_NONE_MATCH=etag)
        self.assertEqual(response.status_code, 304)
        setting2 = Setting.objects.create(
            setting_to_override=self.setting_to_override,
            value="['USD']",
            created_by=self.test_admin,
            changed_by=self.test_admin
        )
        cache_key = "{}_{}".format(ETAG_SETTINGS_CACHE_KEY, setting2.pk)
        url = reverse("api:setting-detail", args=[setting2.id])
        etag = cache.get(cache_key, '')
        response = self.admin_client.get(url, HTTP_IF_NONE_MATCH=etag)
        self.assertEqual(response.status_code, 200)
        etag = cache.get(cache_key, '')
        response = self.admin_client.get(url, HTTP_IF_NONE_MATCH=etag)
        self.assertEqual(response.status_code, 304)
        setting2.delete()
        etag = cache.get(cache_key, '')
        self.assertEqual(etag, '')


class TestSettingsUpdate(BaseForTheSettings):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.setting_to_override = AvailableSetting.objects.last()
        cls.setting_to_change_on = AvailableSetting.objects.first()
        assert cls.setting_to_override != cls.setting_to_change_on
        cls.setting1 = Setting.objects.create(
            setting_to_override=AvailableSetting.objects.last(),
            value="['USD']"
        )
        cls.login_as_admin()
        cls.url = reverse("api:setting-detail", args=[cls.setting1.id])
        cls.setting1.created_by = cls.test_admin
        cls.setting1.changed_by = cls.test_admin
        cls.setting1.save(update_fields=['created_by', 'changed_by'])

    def test_patch_value(self):
        patch_data = {
            "value": "Some new value"
        }
        response = self.admin_client.patch(self.url, patch_data, format="json")
        self.assertEqual(response.status_code, 405)


class TestSettingsDelete(BaseForTheSettings):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.setting_to_override = AvailableSetting.objects.last()
        cls.login_as_admin()

    def setUp(self):
        self.setting1 = Setting.objects.create(
            setting_to_override=self.setting_to_override,
            value="['USD']",
            created_by=self.test_admin,
            changed_by=self.test_admin,
            start_date=date.today() + timedelta(days=1)
        )

        self.setting2 = Setting.objects.create(
            setting_to_override=self.setting_to_override,
            value="['USD']",
            created_by=self.test_admin,
            changed_by=self.test_admin,
            start_date=date.today()
        )

        self.url = reverse("api:setting-detail", args=[self.setting1.id])

    def test_delete_upcoming_setting(self):
        before = Setting.objects.count()
        response = self.admin_client.delete(self.url)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Setting.objects.count(), before - 1)

    def test_cant_delete_past_setting(self):
        self.setting1.start_date = date.today() - timedelta(days=1)
        self.setting1.save(update_fields=['start_date'])
        response = self.admin_client.delete(self.url)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['errors'], "You can delete only upcoming settings")

    def test_cant_delete_current_setting(self):
        url = reverse("api:setting-detail", args=[self.setting2.id])
        response = self.admin_client.delete(url)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['errors'], "You can delete only upcoming settings")

    def test_permissions(self):
        """ Check that only groups that have access to this endpoint are Admin
        """
        response = self.user_client.delete(self.url)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.url)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(self.url)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.url)
        self.assertEqual(response.status_code, 403)
        response = self.admin_client.delete(self.url)
        self.assertEqual(response.status_code, 204)


class TestTaxNotesApi(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.tax_notes1 = TaxNoteFactory()
        cls.list_endpoint = reverse('api:taxnote-list')
        cls.detail_endpoint = reverse('api:taxnote-detail', args=[cls.tax_notes1.id])
        cls.login_as_admin()

    def test_list_tax_notes(self):
        response = self.admin_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]['name'], self.tax_notes1.name)

    def test_create_tax_notes(self):
        post_data = {
            'name': "New name"
        }
        response = self.admin_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        self.assertTrue(TaxNote.objects.filter(name="New name").exists())

    def test_retrieve(self):
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], self.tax_notes1.name)

    def test_patch(self):
        patch_data = {
            'name': "New name"
        }
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], patch_data['name'])

    def test_delete(self):
        tax_notes2 = TaxNoteFactory()
        detail_endpoint = reverse('api:taxnote-detail', args=[tax_notes2.id])
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_read(self):
        """ Check that only groups that have access to this endpoint are Admin and Backoffice and User for read
        """
        response = self.user_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.user_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)

    def test_permissions_tax_note_create(self):
        """ Check that only groups that have access to this endpoint are Admin and Backoffice
        """
        post_data = {
            'name': "New name"
        }
        response = self.user_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        response = self.admin_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)

    def test_permissions_update(self):
        """ Check that only groups that have access to this endpoint are Admin and Backoffice
        """
        patch_data = {
            'name': "New name"
        }
        response = self.user_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_delete(self):
        to_delete = TaxNoteFactory()
        detail_endpoint = reverse('api:taxnote-detail', args=[to_delete.id])
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)
        to_delete = TaxNoteFactory()
        detail_endpoint = reverse('api:taxnote-detail', args=[to_delete.id])
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_taxnote_update_create_used_for_tax(self):
        post_data = {
            'name': "New name2",
            "used_for_tax": True
        }
        response = self.backoffice_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        self.assertTrue(response.data['used_for_tax'])
        patch_data = {
            'name': "New name",
            "used_for_tax": True
        }
        response = self.backoffice_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.data['used_for_tax'])

    def test_taxnote_default_in_currency_uniqueness(self):
        post_data = {
            'name': "GBP New name2",
            "used_for_tax": True,
            "default_in_currency": "GBP"
        }
        response = self.backoffice_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        response = self.backoffice_client.post(self.list_endpoint, post_data, format="json")
        self.assertErrorResponse(
            response,
            {
                "default_in_currency":
                ["There is already one default Tax note using currency GBP, please remove that one first!"]
            }
        )
        patch_data = {
            "default_in_currency": "GBP"
        }
        response = self.backoffice_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {
                "default_in_currency":
                ["There is already one default Tax note using currency GBP, please remove that one first!"]
            }
        )
        patch_data = {
            "default_in_currency": "USD"
        }
        response = self.backoffice_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)


class TestThankYouNotesApi(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.thank_you_note = ThankYouNoteFactory()
        cls.list_endpoint = reverse('api:thankyounote-list')
        cls.detail_endpoint = reverse('api:thankyounote-detail', args=[cls.thank_you_note.id])
        cls.login_as_admin()

    def test_list_thank_you_notes(self):
        response = self.admin_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]['name'], self.thank_you_note.name)

    def test_create_thank_you_notes(self):
        post_data = {
            'name': "New name"
        }
        response = self.admin_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        self.assertTrue(ThankYouNote.objects.filter(name="New name").exists())

    def test_retrieve_thank_you_note(self):
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], self.thank_you_note.name)

    def test_patch_thank_you_note(self):
        patch_data = {
            'name': "New name"
        }
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], patch_data['name'])

    def test_delete_thank_you_note(self):
        thank_you_note2 = ThankYouNoteFactory()
        detail_endpoint = reverse('api:thankyounote-detail', args=[thank_you_note2.id])
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_read_thank_you_note(self):
        """ Check that only groups that have access to this endpoint are Admin and Backoffice and User for read
        """
        response = self.user_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.user_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)

    def test_permissions_create_thank_you_note(self):
        """ Check that only groups that have access to this endpoint are Admin and Backoffice
        """
        post_data = {
            'name': "New name"
        }
        response = self.user_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        response = self.admin_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)

    def test_permissions_update_thank_you_note(self):
        """ Check that only groups that have access to this endpoint are Admin and Backoffice
        """
        patch_data = {
            'name': "New name"
        }
        response = self.user_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_delete_thank_you_note(self):
        to_delete = ThankYouNoteFactory()
        detail_endpoint = reverse('api:thankyounote-detail', args=[to_delete.id])
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)
        to_delete = ThankYouNoteFactory()
        detail_endpoint = reverse('api:thankyounote-detail', args=[to_delete.id])
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)


class TestAgencyBankAccount(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.agency_bank = AgencyBankAccountFactory()
        cls.list_endpoint = reverse('api:agencybankaccount-list')
        cls.detail_endpoint = reverse('api:agencybankaccount-detail', args=[cls.agency_bank.id])
        cls.login_as_admin()

    def test_list(self):
        response = self.admin_client.get(self.list_endpoint, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['id'], str(self.agency_bank.id))

    def test_create(self):
        before = AgencyBankAccount.objects.count()
        valid_data = {
            'bank_name': 'Bank 10',
            'account_name': 'PrivateNew',
            'owner_name': 'Owner 10',
            'bic': 'AAAAGE23000',
            'iban': 'DE89370400440532013000',
        }
        response = self.admin_client.post(self.list_endpoint, valid_data, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(AgencyBankAccount.objects.count(), before + 1)
        self.assertTrue(AgencyBankAccount.objects.filter(account_name=valid_data['account_name']).exists())

        before = AgencyBankAccount.objects.count()
        invalid_data = dict.copy(valid_data)
        invalid_data.update({
            'bic': '000',
        })
        response = self.admin_client.post(self.list_endpoint, invalid_data, format='json')
        self.assertEqual(AgencyBankAccount.objects.count(), before)
        self.assertErrorResponse(response, {'bic': ['This Bank Identifier Code (BIC) is invalid.']})

    def test_update(self):
        data = {
            'bank_name': self.agency_bank.bank_name,
            'account_name': 'Business',
            'owner_name': self.agency_bank.owner_name,
            'bic': self.agency_bank.bic,
            'iban': 'DE89370400440532013000',
        }
        response = self.admin_client.patch(self.detail_endpoint, data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_retrieve(self):
        response = self.admin_client.get(self.detail_endpoint, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], str(self.agency_bank.id))

    def test_deleting(self):
        agency_bank = AgencyBankAccountFactory()
        url = reverse('api:agencybankaccount-detail', args=[agency_bank.id])
        response = self.admin_client.delete(url)
        self.assertEqual(response.status_code, 204)
        self.assertFalse(AgencyBankAccount.objects.filter(id=agency_bank.id).exists())

    def test_permissions_get(self):
        """ Check that only groups that have access to this endpoint are Admin and Backoffice
        """
        response = self.user_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)

        response = self.user_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)

    def test_create_permission(self):
        valid_data = {
            'bank_name': 'Bank 11',
            'account_name': 'PrivatNew',
            'owner_name': 'Owner 11',
            'bic': 'AAAAGE23001',
            'iban': 'DE89370400440532013000',
        }
        response = self.user_client.post(self.list_endpoint, valid_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.list_endpoint, valid_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.list_endpoint, valid_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.list_endpoint, valid_data, format='json')
        self.assertEqual(response.status_code, 201)
        response = self.admin_client.post(self.list_endpoint, valid_data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_patch_permission(self):
        valid_data = {
            'bank_name': 'Bank 12'
        }
        response = self.user_client.patch(self.detail_endpoint, valid_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.detail_endpoint, valid_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.detail_endpoint, valid_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.detail_endpoint, valid_data, format='json')
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.detail_endpoint, valid_data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_delete_permission(self):
        agency_bank = AgencyBankAccountFactory()
        url = reverse('api:agencybankaccount-detail', args=[agency_bank.id])
        response = self.user_client.delete(url)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(url)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(url)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(url)
        self.assertEqual(response.status_code, 204)
        agency_bank = AgencyBankAccountFactory()
        url = reverse('api:agencybankaccount-detail', args=[agency_bank.id])
        response = self.admin_client.delete(url)
        self.assertEqual(response.status_code, 204)
