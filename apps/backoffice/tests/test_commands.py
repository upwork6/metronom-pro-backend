
from django.conf import settings
from django.core.management import call_command
from django.test import TestCase, override_settings

from apps.backoffice.models import AvailableSetting


@override_settings(
    METRONOM_SETTINGS={
        'WORKDAY_LENGTH_IN_HOURS': {'value': 8, "editable_in_time_frame": "monthly"},
        'DEFAULT_AGENCY_WORKING_HOURS': {'value': 35},
        'DEFAULT_AGENCY_LIST': {'value': [35, 2, 3, '44']},
        'DEFAULT_AGENCY_NAME': {'value': "Name"},
        'DEFAULT_AGENCY_BOOL': {'value': True},
        'DEFAULT_AGENCY_DICT': {'value': {'a': 'b'}},
    }
)
class TestCommands(TestCase):

    def test_update_settings_command(self):
        AvailableSetting.objects.all().delete()
        self.assertEqual(AvailableSetting.objects.count(), 0)
        call_command("update_settings")
        self.assertNotEqual(AvailableSetting.objects.count(), 0)
        self.assertEqual(AvailableSetting.objects.count(), len(settings.METRONOM_SETTINGS))
        setting = AvailableSetting.objects.last()
        setting.default_value = "FooBar"
        setting.save(update_fields=["default_value"])
        call_command("update_settings")
        setting.refresh_from_db()
        self.assertNotEqual(setting.default_value, "FooBar")
        self.assertTrue(AvailableSetting.objects.filter(editable_in_time_frame=AvailableSetting.IMMEDIATELY).exists())
        self.assertTrue(AvailableSetting.objects.filter(editable_in_time_frame=AvailableSetting.MONTHLY).exists())
        self.assertTrue(AvailableSetting.objects.filter(value_type="str").exists())
        self.assertTrue(AvailableSetting.objects.filter(value_type="bool").exists())
        self.assertTrue(AvailableSetting.objects.filter(value_type="int").exists())
        self.assertTrue(AvailableSetting.objects.filter(value_type="list").exists())
        self.assertTrue(AvailableSetting.objects.filter(value_type="dict").exists())
