from django.test import TestCase
from django.urls import resolve, reverse

from apps.backoffice.models import TermsCollection, TermsParagraph


class TestTermsParagraphURLs(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.paragraph = TermsParagraph.objects.create(name='paragraph 1')

    def test_terms_paragraph_list_reverse(self):
        assert reverse('api_backoffice:terms-paragraph-list') == f'/api/backoffice/terms-paragraphs/'

    def test_terms_paragraph_list_resolve(self):
        assert resolve(f'/api/backoffice/terms-paragraphs/').view_name == 'api_backoffice:terms-paragraph-list'

    def test_terms_paragraph_detail_reverse(self):
        assert reverse(
            'api_backoffice:terms-paragraph-detail', kwargs={'pk': self.paragraph.pk}
        ) == f'/api/backoffice/terms-paragraphs/{self.paragraph.pk}/'

    def test_terms_paragraph_detail_resolve(self):
        assert resolve(
            f'/api/backoffice/terms-paragraphs/{self.paragraph.pk}/'
        ).view_name == 'api_backoffice:terms-paragraph-detail'


class TestTermsCollectionURLs(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.terms_collection = TermsCollection.objects.create(name='terms collection 1')

    def test_terms_collection_list_reverse(self):
        assert reverse('api_backoffice:term-list') == f'/api/backoffice/terms/'

    def test_terms_collection_list_resolve(self):
        assert resolve(f'/api/backoffice/terms/').view_name == 'api_backoffice:term-list'

    def test_terms_collection_detail_reverse(self):
        assert reverse(
            'api_backoffice:term-detail', kwargs={'pk': self.terms_collection.pk}
        ) == f'/api/backoffice/terms/{self.terms_collection.pk}/'

    def test_terms_collection_detail_resolve(self):
        assert resolve(
            f'/api/backoffice/terms/{self.terms_collection.pk}/'
        ).view_name == 'api_backoffice:term-detail'
