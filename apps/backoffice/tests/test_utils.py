import datetime
from ast import literal_eval

from apps.backoffice.models import AvailableSetting, Setting
from apps.backoffice.utils import get_metronom_setting, get_setting
from apps.metronom_commons.test import MetronomBaseAPITestCase


class TestUtilsMethods(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        AvailableSetting.objects.get_or_create(
            name="CURRENCY_CHOICES",
            defaults=dict(default_value="[('CHF', 'CHF'), ('USD','USD'), ('GBP','GBP'), ('EUR', 'EUR')]")
        )
        AvailableSetting.objects.get_or_create(
            name="DEFAULT_AGENCY_CURRENCY",
            defaults=dict(default_value="'CHF'")
        )
        cls.login_as_admin()
        cls.previous_day = datetime.datetime.today() - datetime.timedelta(days=1)

    def test_get_setting_method(self):
        """ Check that method returns overwritten settings if it exists and default one
        from django settings if it not

        """
        to_override = AvailableSetting.objects.get(name="DEFAULT_AGENCY_CURRENCY")
        currency = "'GBP'"
        Setting.objects.create(
            setting_to_override=to_override,
            value=currency,
            created_by=self.test_admin,
            start_date=self.previous_day
        )
        value = get_setting("DEFAULT_AGENCY_CURRENCY")
        assert value == 'GBP'
        available_currencies = get_metronom_setting('CURRENCY_CHOICES')
        value = get_setting("CURRENCY_CHOICES")
        assert len(value) == len(available_currencies)
        for x in value:
            assert x in available_currencies

    def test_eval_value(self):
        to_override = AvailableSetting.objects.get(name="CURRENCY_CHOICES")
        string_value = "(('USD','USD'), ('GBP','GBP'))"
        Setting.objects.create(
            setting_to_override=to_override,
            value=string_value,
            created_by=self.test_admin,
            start_date=self.previous_day
        )
        get_setting_value = get_setting("CURRENCY_CHOICES")
        value_from_settings = get_metronom_setting('CURRENCY_CHOICES')
        proper_format = type(value_from_settings)
        prime_evaluation = literal_eval(string_value)
        final_result = proper_format(prime_evaluation)
        assert get_setting_value == final_result

    def test_get_default(self):
        value = get_setting("DEFAULT_AGENCY_CURRENCY")
        self.assertEqual(value, 'CHF')

    def test_get_default_workday(self):
        value = get_setting("WORKDAY_LENGTH_IN_HOURS")
        self.assertEqual(value, 8.0)

    def test_get_default_workday_no_user(self):
        value = get_setting("WORKDAY_LENGTH_IN_HOURS")
        self.assertEqual(value, 8.0)

    def test_get_default_currency_no_user(self):
        value = get_setting("DEFAULT_AGENCY_CURRENCY")
        self.assertEqual(value, get_metronom_setting('DEFAULT_AGENCY_CURRENCY'))

    def test_non_existent(self):
        value = get_setting("DEFAULT_AGENCY_CURRENCY_None")
        self.assertEqual(value, None)

    def test_start_date(self):
        to_override = AvailableSetting.objects.get(name="DEFAULT_AGENCY_CURRENCY")
        currency = "'GBP'"
        Setting.objects.create(
            setting_to_override=to_override,
            value=currency,
            created_by=self.test_admin,
            start_date=self.previous_day
        )
        currency2 = "'USD'"
        Setting.objects.create(
            setting_to_override=to_override,
            value=currency2,
            created_by=self.test_admin,
            start_date=datetime.datetime.today() + datetime.timedelta(days=2)
        )
        value = get_setting("DEFAULT_AGENCY_CURRENCY")
        self.assertEqual(value, literal_eval(currency))
        currency3 = "'EUR'"
        Setting.objects.create(
            setting_to_override=to_override,
            value=currency3,
            created_by=self.test_admin,
            start_date=datetime.datetime.today()
        )
        value = get_setting("DEFAULT_AGENCY_CURRENCY")
        self.assertEqual(value, literal_eval(currency3))

    def test_version(self):
        to_override = AvailableSetting.objects.get(name="DEFAULT_AGENCY_CURRENCY")
        currency = "'GBP'"
        setting = Setting.objects.create(
            setting_to_override=to_override,
            value=currency,
            created_by=self.test_admin,
            start_date=self.previous_day
        )
        currency2 = "'USD'"
        setting2 = Setting.objects.create(
            setting_to_override=to_override,
            value=currency2,
            created_by=self.test_admin,
            start_date=datetime.datetime.today() + datetime.timedelta(days=2)
        )
        self.assertEqual(setting.version, 1)
        self.assertEqual(setting2.version, 2)

    def test_same_start_date(self):
        to_override = AvailableSetting.objects.get(name="DEFAULT_AGENCY_CURRENCY")
        currency = "'GBP'"
        Setting.objects.create(
            setting_to_override=to_override,
            value=currency,
            created_by=self.test_admin,
            start_date=datetime.date.today()
        )
        currency2 = "'USD'"
        Setting.objects.create(
            setting_to_override=to_override,
            value=currency2,
            created_by=self.test_admin,
            start_date=datetime.date.today()
        )
        currency3 = "'EUR'"
        Setting.objects.create(
            setting_to_override=to_override,
            value=currency3,
            created_by=self.test_admin,
            start_date=datetime.date.today()
        )
        for i in range(0, 10):  # to make sure that different calls returns the same value
            value = get_setting("DEFAULT_AGENCY_CURRENCY")
            self.assertEqual(value, literal_eval(currency3))
