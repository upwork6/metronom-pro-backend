""" This file contains different utils for the backoffice application
"""
from ast import literal_eval

from django.conf import settings

from apps.backoffice.models import AvailableSetting


def get_metronom_setting(name):
    """
    This util method helps to get the default setting from metronom django settings
    :param name:
    :return:
    """
    return getattr(settings, 'METRONOM_SETTINGS', {}).get(name, {}).get('value', None)


def get_setting(name):
    """ This util method helps to get overwritten settings if those exists and returns the value
    from the django settings if it's not
    :param name: str
    """
    available_setting = AvailableSetting.objects.filter(name=name).first()
    default_settings_value_format = type(get_metronom_setting(name))
    valid_get_metronom_value = get_metronom_setting(name) != None
    # `default_settings_value_format` => str || dict || int || list || Decimal || (....)
    if available_setting:
        setting = available_setting.get_current_setting()
        if setting:
            try:
                value = literal_eval(setting.value)
            except SyntaxError:
                value = setting.value
            except ValueError:
                value = setting.value
            # ugly but the only method it worked to catch if is not None
            # Other methods throw this: TypeError: isinstance() arg 2 must be a type or tuple of types
            if valid_get_metronom_value:
                return default_settings_value_format(value)
            return value
        else:
            if available_setting.default_value:
                try:
                    value = literal_eval(available_setting.default_value)
                except SyntaxError:
                    value = available_setting.default_value
                except ValueError:
                    value = available_setting.default_value
                # ugly method to catch if is not None
                if valid_get_metronom_value:
                    return default_settings_value_format(value)
                return value
    return get_metronom_setting(name=name)
