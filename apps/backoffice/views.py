import hashlib
import json
from datetime import date

from django.core.cache import caches
from django.http import HttpResponseNotModified
from django.utils.cache import quote_etag
from django.utils.translation import ugettext_lazy as _
from dry_rest_permissions.generics import DRYPermissions
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.api.filters import MetronomPermissionsFilter
from apps.backoffice.models import AgencyBankAccount, Setting, TaxNote, TermsCollection, TermsParagraph, ThankYouNote
from apps.backoffice.serializers import (AgencyBankAccountSerializer, SettingListSerializer, SettingSerializer, SettingPageListSerializer,
                                         TaxNoteSerializer, TermsCollectionSerializer, TermsParagraphSerializer,
                                         ThankYouNoteSerializer)
from apps.api.views import MetronomBaseModelViewSet

from apps.metronom_commons.data import EtagCaching
from apps.metronom_commons.mixins.views import DeleteForbiddenIfUsedMixin, SerializerDispatcherMixin
from apps.offers.models import Offer, CampaignOffer

cache = caches['default']
ETAG_SETTINGS_CACHE_KEY = EtagCaching.ETAG_SETTINGS_CACHE_KEY


class SettingViewSet(SerializerDispatcherMixin, MetronomBaseModelViewSet):
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    queryset = Setting.objects.all()
    serializer_class = SettingSerializer
    http_method_names = ['get', 'post', 'head', 'options', 'trace', 'delete']
    serializers_dispatcher = {
        'list': SettingPageListSerializer
    }

    def list(self, request, *args, **kwargs):
        etag = cache.get(ETAG_SETTINGS_CACHE_KEY, '')
        if_none_match = request.META.get('HTTP_IF_NONE_MATCH', None)
        if if_none_match is not None and etag and if_none_match == etag:
            return HttpResponseNotModified()
        # we need to show only 1 current value per AvailableSetting, so first of well let's get distinct by it
        queryset = self.filter_queryset(self.get_queryset()).distinct("setting_to_override")
        current_ids = []
        for obj in queryset:
            available_setting = obj.setting_to_override
            # now we just get the current value id
            current_ids.append(available_setting.get_current_setting().id)
        # and now let's filter it and order by created date
        queryset = self.filter_queryset(self.get_queryset()).filter(id__in=current_ids).order_by('-created_at')

        summary = bool(request.GET.get('summary', False))

        if summary:
            serializer = SettingListSerializer(queryset, many=True)
        else:
            serializer = self.get_serializer(queryset, many=True)

        etag_key = quote_etag(hashlib.md5(json.dumps(serializer.data).encode("utf-8")).hexdigest())
        headers = {'ETag': etag}
        cache.set(ETAG_SETTINGS_CACHE_KEY, etag_key)
        return Response(serializer.data, headers=headers)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        cache_key = "{}_{}".format(ETAG_SETTINGS_CACHE_KEY, instance.pk)
        etag = cache.get(cache_key, '')
        if_none_match = request.META.get('HTTP_IF_NONE_MATCH', None)
        if if_none_match is not None and etag and if_none_match == etag:
            return HttpResponseNotModified()
        serializer = self.get_serializer(instance)
        etag_key = quote_etag(hashlib.md5(json.dumps(serializer.data).encode("utf-8")).hexdigest())
        headers = {'ETag': etag}
        cache.set(cache_key, etag_key)
        return Response(serializer.data, headers=headers)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        available_setting = instance.setting_to_override
        current_setting = available_setting.get_current_setting()
        if instance.id == current_setting.id or instance.start_date < date.today():
            return Response(status=400, data={"errors": _("You can delete only upcoming settings")})
        return super().destroy(request, *args, **kwargs)


class TaxNoteViewSet(ModelViewSet):
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    queryset = TaxNote.objects.order_by('-created_at')
    serializer_class = TaxNoteSerializer


class ThankYouNoteViewSet(ModelViewSet):
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    queryset = ThankYouNote.objects.order_by('-created_at')
    serializer_class = ThankYouNoteSerializer


class AgencyBankAccountViewSet(ModelViewSet):
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    queryset = AgencyBankAccount.objects.order_by('-created_at')
    serializer_class = AgencyBankAccountSerializer


class TermsParagraphViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    queryset = TermsParagraph.objects.order_by('position')
    serializer_class = TermsParagraphSerializer
    forbid_delete_if_used_in = {
        TermsCollection: ["terms"]
    }


class TermsCollectionViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    queryset = TermsCollection.objects.order_by('position')
    serializer_class = TermsCollectionSerializer
    forbid_delete_if_used_in = {
        Offer: ["terms_collection"],
        CampaignOffer: ["terms_collection"],
    }
