# -*- coding: utf-8 -*-
from django.contrib import admin

from apps.invoices.models import Invoice, ActivityInvoiced, ArticleInvoiced


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'project',
        'invoice_date',
        'payment_within_days',
        'used_project_description',
    )
    list_filter = ('created_at', 'updated_at', 'project', 'invoice_date')


@admin.register(ActivityInvoiced)
class ActivityInvoicedAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'invoice',
        'activity_offered',
        'discount',
        'used_description',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'invoice',
        'activity_offered',
    )


@admin.register(ArticleInvoiced)
class ArticleInvoicedAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'invoice',
        'article_offered',
        'discount',
        'used_description',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'invoice',
        'article_offered',
    )
