
from decimal import Decimal

from django.db import models
from django.utils.translation import ugettext_lazy as _
from djmoney.money import Money

from apps.backoffice.utils import get_setting
from apps.metronom_commons.models import (SoftDeleteMixin,
                                          UserReadOnlyAccountingMetronomBaseModel)
from apps.projects.models import (Project, ProjectActivity,
                                  ProjectActivityStep, ProjectArticle)


class Invoice(SoftDeleteMixin, UserReadOnlyAccountingMetronomBaseModel):
    project = models.ForeignKey(Project)
    invoice_date = models.DateField()
    payment_within_days = models.PositiveSmallIntegerField()
    used_project_description = models.TextField()

    def __str__(self):
        return f"{self.project.name}: {self.invoice.date}"

    class Meta:
        verbose_name = _('Invoice')
        verbose_name_plural = _('Invoices')


class ActivityInvoiced(UserReadOnlyAccountingMetronomBaseModel):
    invoice = models.ForeignKey(Invoice)
    activity_offered = models.ForeignKey(ProjectActivity)
    discount = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    used_description = models.TextField()

    @property
    def total_amount(self):
        """ Total amount of the invoiced acitivity in the default currency with discount

        :rtype: Decimal
        """

        # TODO: Use discount Money project currency and used_rate Money calculation

        discount = self.discount or Money(0, self.invoice.project.billing_currency)
        pa_steps = self.activity_offered.activity_steps.all()
        total_sum = 0
        for pa_step in pa_steps:
            total_sum += pa_step.used_rate * pa_step.planned_effort
        return total_sum - discount

    def total_effort(self):
        """ Total invoiced planned effort

        :rtype: Decimal
        """
        return self.activity_offered.activity_steps.aggregate(models.Sum('planned_effort'))['planned_effort__sum']

    class Meta:
        unique_together = ['invoice', 'activity_offered']
        verbose_name = _("Activity Invoiced")
        verbose_name_plural = _("Activities Invoiced")

    def __str__(self):
        return f"{self.invoice}: {self.activity_offered}"


class ArticleInvoiced(UserReadOnlyAccountingMetronomBaseModel):
    invoice = models.ForeignKey(Invoice)
    article_offered = models.ForeignKey(ProjectArticle)
    discount = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    used_description = models.TextField()

    class Meta:
        unique_together = ['invoice', 'article_offered']
        verbose_name = _("Article Invoiced")
        verbose_name_plural = _("Articles Invoiced")

    def __str__(self):
        return f"{self.invoice}: {self.article_offered}"
