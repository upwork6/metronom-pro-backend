import pytest

from datetime import date
from decimal import Decimal

from django.test import TestCase
from djmoney.money import Money

from apps.activities.tests.factories import RateFactory
from apps.backoffice.models import AvailableSetting, Setting
from apps.invoices.models import Invoice
from apps.invoices.tests.factories import ActivityInvoicedFactory, InvoiceFactory
from apps.projects.tests.factories import (ProjectFactory, ProjectActivityFactory,
                                           ProjectActivityStepFactory)
from apps.users.tests.factories import UserFactory


@pytest.mark.skip("Not required by now, logic not correct")
class ActivityInvoicedTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.invoice = InvoiceFactory(project=cls.project)
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.activity1 = ActivityInvoicedFactory(activity_offered=cls.project_activity, invoice=cls.invoice)
        cls.pa_step = ProjectActivityStepFactory(activity=cls.project_activity)
        cls.pa_step.planned_effort = Decimal("4.00")
        cls.pa_step.save()
        cls.user = UserFactory()

    def test_total_amount(self):
        self.assertEqual(self.activity1.total_amount, self.pa_step.used_rate * 4)

    def test_total_effort(self):
        self.assertEqual(self.activity1.total_effort(), Decimal("4.00"))

    # TODO: Fails but is not correct anyway, it should use the invoiced effort and the actual work of the User
    # def test_amount_with_discount(self):
    #     project_activity = ProjectActivityFactory(project=self.project)
    #     activity2 = ActivityInvoicedFactory(discount=Decimal("1.00"), activity_offered=project_activity)
    #     pa_step = ProjectActivityStepFactory(activity=project_activity, unit="hourly")
    #     pa_step.planned_effort = Decimal("4.00")
    #     pa_step.save()
    #     self.assertEqual(activity2.total_amount, pa_step.used_rate.amount * (4 - 1))

    def test_amount_with_several_rates(self):
        step = self.pa_step.step
        RateFactory(step=step, currency="USD")
        RateFactory(step=step, currency="GBP")
        self.assertEqual(self.activity1.total_amount, self.pa_step.used_rate * 4)

    def test_amount_with_several_steps(self):
        project_activity = ProjectActivityFactory(project=self.project)
        activity2 = ActivityInvoicedFactory(activity_offered=project_activity, invoice=self.invoice)
        total_amount = 0
        for i in range(1, 5):
            pa_step = ProjectActivityStepFactory(activity=project_activity, unit="hourly")
            pa_step.planned_effort = Decimal(str(i))
            pa_step.save()
            total_amount += pa_step.step.rates.filter(currency="CHF").last().hourly_rate.amount * i
        self.assertEqual(activity2.total_amount, total_amount)

    def test_amount_with_different_full_day_length(self):
        setting = AvailableSetting.objects.get_or_create(
            name="WORKDAY_LENGTH_IN_HOURS", defaults=dict(default_value="8"))[0]
        setting = Setting.objects.create(
            setting_to_override=setting,
            value="11.0",
            created_by=self.user,
            changed_by=self.user,
            start_date=date.today()
        )
        project_activity = ProjectActivityFactory(project=self.project)
        activity2 = ActivityInvoicedFactory(activity_offered=project_activity, invoice=self.invoice)
        pa_step = ProjectActivityStepFactory(activity=project_activity, unit="daily")
        pa_step.planned_effort = Decimal("4.00")
        pa_step.save()
        rate = pa_step.step.rates.filter(currency="CHF").last()
        rate.daily_rate = None
        rate.save()
        amount = rate.hourly_rate.amount
        self.assertEqual(activity2.total_amount, amount *
                         Decimal(setting.value) * pa_step.planned_effort)

    def test_activity_invoiced_daily_amount(self):
        project_activity = ProjectActivityFactory(project=self.project)
        activity2 = ActivityInvoicedFactory(activity_offered=project_activity, invoice=self.invoice)
        pa_step = ProjectActivityStepFactory(activity=project_activity, unit="daily")
        pa_step.planned_effort = Decimal("4.00")
        pa_step.save()
        rate = pa_step.step.rates.filter(currency="CHF").last()
        rate.daily_rate = Money(Decimal("500"), currency="CHF")
        rate.save()
        amount = rate.daily_rate.amount
        self.assertEqual(activity2.total_amount, amount * 4)


class TestInvoiceModelSoftDelete(TestCase):

    def test_deletion(self):
        project = ProjectFactory(billing_currency="CHF")
        invoice = InvoiceFactory(project=project)
        invoice2 = InvoiceFactory(project=project)
        self.assertEqual(Invoice.objects.count(), 2)
        invoice2.delete()
        self.assertEqual(Invoice.objects.count(), 1)
        self.assertEqual(Invoice.all_objects.count(), 2)
        invoice2.save()
        self.assertEqual(Invoice.objects.count(), 2)

    def test_cascade_deletion(self):
        project = ProjectFactory(billing_currency="CHF")
        invoice = InvoiceFactory(project=project)
        invoice2 = InvoiceFactory(project=project)
        self.assertEqual(Invoice.objects.count(), 2)
        project.delete()
        self.assertEqual(Invoice.objects.count(), 0)
        self.assertEqual(Invoice.all_objects.count(), 2)
