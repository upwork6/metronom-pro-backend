from datetime import date

import factory

from apps.invoices.models import ActivityInvoiced, Invoice, ArticleInvoiced
from apps.projects.tests.factories import (ProjectActivityFactory,
                                           ProjectArticleFactory,
                                           ProjectFactory)


class InvoiceFactory(factory.DjangoModelFactory):
    project = factory.SubFactory(ProjectFactory)
    invoice_date = date.today()
    payment_within_days = 5
    used_project_description = factory.Faker("sentence")

    class Meta:
        model = Invoice


class ActivityInvoicedFactory(factory.DjangoModelFactory):
    invoice = factory.SubFactory(InvoiceFactory)
    activity_offered = factory.SubFactory(ProjectActivityFactory)
    used_description = factory.Faker("sentence")

    class Meta:
        model = ActivityInvoiced


class ArticleInvoicedFactory(factory.DjangoModelFactory):
    invoice = factory.SubFactory(InvoiceFactory)
    article_offered = factory.SubFactory(ProjectArticleFactory)
    used_description = factory.Faker("sentence")

    class Meta:
        model = ArticleInvoiced
