from django.contrib import admin

from apps.search.models import UserSearch

# Register your models here.


class UserSearchAdmin(admin.ModelAdmin):
    """ Admin interface for user searched keys """
    list_display = ('keyword', 'user',)


admin.site.register(UserSearch, UserSearchAdmin)
