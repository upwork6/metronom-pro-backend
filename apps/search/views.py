import django
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.db import connection, models
from django.db.models import CharField, Q, TextField, Value
from django.db.models.functions import Concat
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.employments.models import Employment
from apps.persons.models import Person
from apps.companies.models import Company
from apps.projects.models import Project, Campaign
from apps.search.cache import DrfCache
from apps.search.drf_search import DrfSearch
from apps.search.models import UserSearch
from apps.search.serializers import UserSearchSerializer


class Search(APIView):
    """
    Class Search
    This class is used to get search data from registered models.

    """

    # Dummy Initiator for testing
    def __init__(self):
        self.search_model = []
        self.drf_cache = DrfCache()

    def get_database_backend(self):
        """
            Check if database is supported by DRF_SEARCH or not
        """
        supported_backend = ['postgresql', 'sql']
        return connection.vendor in supported_backend

    def get_queryset(self, query_array):
        # Create Q() Object
        q_object = Q()
        for query in query_array:
            q_object = q_object | query
        return q_object

    def sanitize_for_negation(self, keyword):
        """
            Sanitize and return query to be used for negation
        """
        negative_keywords = []
        positive_keyword = ""
        splitted_keywords = keyword.split(" ")

        for splitted_keyword in splitted_keywords:
            if splitted_keyword.startswith("-"):
                negative_keywords.append(splitted_keyword.strip()[1:])
            else:
                positive_keyword += " " + splitted_keyword

        result = [negative_keywords, positive_keyword]

        # TODO: It gives the negative keywords as list, and the positive keywords as string. why?
        return [negative_keywords, positive_keyword.strip()]

    def save_user_search(self, keyword, request_user):
        UserSearch(keyword=keyword, user=request_user if AnonymousUser !=
                                                         request_user.__class__ else None).save()

    def get(self, request):
        """
            Multi Model Searching

                - query: Search Keyword

            Description: Model needs to be registered before using search

                ds.register( [ { "model_name": MODEL-NAME,
                                 "searchable_fields": [MODEL-FIELDS],
                                "foreign_fields": [FOREIGN-MODEL] } ] )

        """

        # Parametrized Search Object passed in Get Params
        parametrized_search_model = None

        # Get DRF SEARCH SETTING
        drf_search_settings = getattr(settings, "DRF_SEARCH_SETTINGS", 0)

        # Main Search query KEY
        search_query = request.GET.get('query', None)

        if not search_query:
            return Response(
                {"error": "Search keyword not provided"},
                status=status.HTTP_400_BAD_REQUEST
            )

        # If Search keywords length is less than 3, exit
        if drf_search_settings:
            if len(search_query) < drf_search_settings["search_keyword_min_length"]:
                return Response(
                    {"error": "Search keyword less than 3"},
                    status=status.HTTP_400_BAD_REQUEST
                )

        if len(search_query) > 50:
            return Response(
                {
                    "error": "Search keyword is too long"
                    },
                status=status.HTTP_400_BAD_REQUEST
            )

        if len(request.GET.keys()) >= 2:
            content_type = request.GET.get('content_type', None)

            # query will be done directly to object appName.modelName
            try:
                search_model = django.apps.apps.get_model(
                    content_type.split(".")[0], content_type.split(".")[1])
                parametrized_search_model = search_model._meta.object_name
                print(parametrized_search_model)
            except Exception as e:
                return Response(
                    {"error": "Provided Model not searchable."},
                    status=status.HTTP_400_BAD_REQUEST
                )

        # Split to query & Negation query
        # returns array of positive and negative keywords
        santized_query = self.sanitize_for_negation(search_query)

        negation_query = []
        if len(santized_query[0]) >= 1:
            negation_query = santized_query[0]

        search_query = santized_query[1]

        if not search_query:
            return Response(
                {'objects': [], 'results': 0},
                status=status.HTTP_200_OK
            )

        # TODO: Why cache only here? It should be at the top
        # Check Cache and return if exist
        cached_data = self.drf_cache.get_cache(search_query)

        if cached_data:
            # Add extra message to reponse
            cached_data["cached"] = True

            return Response(
                cached_data, status=status.HTTP_200_OK
            )

        ds = DrfSearch()
        registered_models = ds.get_searchable_models()
        search_response = {"objects": []}

        # Iterate over registered Model
        for model_data in registered_models:

            # Check if there is any  Model in Get :  crm.Person=Ank
            if parametrized_search_model is not None:
                if model_data["model_name"]._meta.object_name == parametrized_search_model:
                    model = model_data["model_name"]
                else:
                    continue
            else:
                model = model_data["model_name"]

            # Only do search for CharField
            fields = [x for x in model._meta.fields if
                      isinstance(x, (TextField, CharField)) and x.name in
                      model_data["searchable_fields"]]

            # Check if field is supplied before doing query skip otherwise
            if len(fields) == 0:
                continue

            # Create Search Querues
            search_queries = [Q(**{x.name + "__icontains": search_query})
                              for x in fields]

            should_concat = model_data.get('concatenated_searchable_fields', None)
            if should_concat is not None:
                search_queries.append(Q(**{"concatenated_search_fields__icontains":
                                      search_query}))

            # Create Q() Object
            q_object = self.get_queryset(search_queries)

            # Append any Negation Query
            if negation_query:
                negation_queryset = []
                for negation_key in negation_query:
                    negation_queryset.append([~Q(**{x.name + "__icontains":
                                                        negation_key})
                                              for x in fields])

                for queries in negation_queryset:
                    for query in queries:
                        q_object = q_object & query

            # if concatenation is required
            if should_concat is not None:

                # Testing SearchVector
                # from django.contrib.postgres.search import SearchVector
                # concat_field = tuple(model_data["concatenated_searchable_fields"])
                # qset = model.objects.annotate(concatenated_search_fields=SearchVector("first_name", "last_name"))

                concat_field = ()
                for idx, b in enumerate(model_data["concatenated_searchable_fields"]):
                    if idx < len(model_data["concatenated_searchable_fields"]) - 1:
                        concat_field = concat_field + (b, Value(' '),)
                    else:
                        concat_field = concat_field + (b,)

                if len(concat_field) > 1:
                    concat_field = Concat(*concat_field)
                else:
                    concat_field = concat_field[0]

                qset = model.objects.annotate(concatenated_search_fields=concat_field)

                # Finally run the Queries
                results = qset.filter(q_object)
            else:
                # Finally run the Queries
                results = model.objects.filter(q_object)

            # Iterate Over Result to create proper Json
            for res in results:
                data = [getattr(res, x.name)
                        for x in res._meta.fields if x in fields]
                name = " ".join(data)
                json_response = {"name": name,
                                 "content_type": str(model._meta),
                                 "url": "" + str(res.get_absolute_url())
                                 }

                search_response["objects"].append(json_response)

            # If data is supposed to be fetched from any Foreign Model
            if model_data["foreign_fields"] and parametrized_search_model is None:
                for foreign_field in model_data["foreign_fields"]:

                    # Create Search Query for Foreign Fields
                    foreign_search_queries = []

                    searchable_meta_fields = {
                        "foreign_fields": foreign_field._meta.fields,
                        "parent_fields": fields}

                    # Loop through meta_fields ex ( person, company)
                    for meta_field in foreign_field._meta.fields:
                        if isinstance(meta_field, models.ForeignKey):

                            for res in results:
                                foreign_search_queries.append(
                                    Q(**{meta_field.name + "__id":
                                             res.id}))

                    # If any query Object is present
                    if len(foreign_search_queries) >= 1:
                        foreign_q_object = self.get_queryset(
                            foreign_search_queries)

                        # Finally run the Queries Of Foreign Model
                        print(foreign_field.objects.filter(
                            foreign_q_object).query)
                        results = foreign_field.objects.filter(
                            foreign_q_object)

                        foreign_json_res = self.generate_json_response(
                            results, searchable_meta_fields, model, registered_models)
                        for json_response in foreign_json_res:

                            # Check if the data contain any Negated keyword
                            if negation_query:
                                # Boolean to check if none of the negative keyword exist
                                negative_keyword_exist = False
                                for negation_key in negation_query:

                                    if negation_key in json_response["name"]:
                                        negative_keyword_exist = True
                                if not negative_keyword_exist:
                                    search_response["objects"].append(
                                        json_response)
                            else:
                                search_response["objects"].append(
                                    json_response)

        search_response["results"] = len(search_response["objects"])

        # Send For caching Result
        if search_response["results"] >= 1:
            self.drf_cache.set_cache(search_query, search_response)

        # Save to Database is usersearch is enabled and result is returned
        if drf_search_settings["save_user_search"] and search_response["results"] >= 1:
            self.save_user_search(search_query, request.user)

        # Return the repsonse
        return Response(
            search_response, status=status.HTTP_200_OK
        )

    def generate_json_response(self, result_object, searchable_meta_fields,
                               model, registered_models):
        json_response = []
        # Iterate Over Result to create proper Json
        for res in result_object:
            name = ""
            content_type = ""
            model_id = ""
            url = ""
            result_set = ""
            for idx, field in enumerate(registered_models):
                # If there is any Foreign Field,
                if len(field["foreign_fields"]) > 0:

                    if model._meta.model_name == field["model_name"]._meta.model_name:

                        result_set = getattr(res, model._meta.model_name).name
                        count = 0

                        for meta_field in field["meta_searchable_fields"]:
                            count += 1
                            name += (getattr(getattr(res,
                                                     field["meta_model_name"]._meta.model_name),
                                             str(meta_field)))

                            # Add space between fields if multiple
                            if count < len(field["meta_searchable_fields"]):
                                name += " "

                        content_type = field["meta_model_name"]._meta.app_label + \
                                       "." + field["meta_model_name"]._meta.model_name

                        if field['order_foreign_fields'] == False:
                            result_set, name = name, result_set
                        json_response.append(
                            {"name": name + " (" + result_set + ")",
                             "content_type": content_type,
                             "url": getattr(res, field["meta_model_name"]._meta.model_name).get_absolute_url()})

        return json_response


class SearchHistory(APIView):
    """
        Api class for Getting User Search History
    """

    def get(self, request):
        """
            Function get
            This function is used to get Search History of a User.

            :param request:
                A request that contains request params.

            :return:
                Return 5  Search History data if exists else return empty dict.
        """
        try:
            # If User is not logged in ( Temporarily )
            if not request.user:
                request.user = User.objects.all()[:1]
            search_history = UserSearch.objects.filter(user=request.user)[:5]
            serializer = UserSearchSerializer(
                instance=search_history, many=True)
            if serializer.data:
                print(serializer.data)
                return Response(
                    (serializer.data),
                    status=status.HTTP_200_OK
                )
            else:
                return Response({
                    "detail": 'No User Search history Found!!'},
                    status=status.HTTP_200_OK
                )
        except Exception as e:
            return Response({
                "error": 'Something went wrong!! Please try again!!',
                "detail": str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )


ds = DrfSearch()

ds.register(
    [
        {
            "model_name": Person,
            "searchable_fields": ["first_name", "last_name"],
            "foreign_fields": [Employment],
            "concatenated_searchable_fields": ["first_name", "last_name"],
            "order_foreign_fields": True,
            "meta_model_name": Company,
            "meta_searchable_fields": ["name"],
        }
    ]
)

ds.register(
    [
        {
            "model_name": Company,
            "searchable_fields": ["name"],
            "foreign_fields": [Employment],
            "order_foreign_fields": True,
            "meta_model_name": Person,
            "meta_searchable_fields": ["first_name", "last_name"],
        }
    ]
)

ds.register(
    [
        {
            "model_name": Project,
            "searchable_fields": ["title", "public_id"],
            "foreign_fields": [],
            "order_foreign_fields": False
        }
    ]
)

ds.register(
    [
        {
            "model_name": Campaign,
            "searchable_fields": ["title", "public_id"],
            "foreign_fields": [Campaign],
            "order_foreign_fields": False,
            "meta_model_name": Company,
            "meta_searchable_fields": ["name"],
        }
    ]
)


