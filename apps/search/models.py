import uuid

from django.contrib.auth.models import User
from django.db import models
from django.db.models import CharField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from apps.metronom_commons.models import UserAccessMetronomBaseModel
from apps.search.cache import DrfCache
from apps.search.drf_search import DrfSearch
from config import settings


class UserSearch(UserAccessMetronomBaseModel):
    """ Search keyword used by users while using DRF SEARH ENDPOINT """
    keyword = models.CharField(_('Keyword'), max_length=50, blank=True, default='')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)

    class Meta(object):
        verbose_name = _('User search')
        verbose_name_plural = _('User searches')


@receiver(post_save)
def invalidate_cache(sender, **kwargs):
    # Initialize DRF
    cache_instance = DrfCache()

    if cache_instance.is_cache_enabled():
        # initialize Drf search to get The Models
        df = DrfSearch()

        registered_models = df.get_searchable_models()

        # Iterate over Registered Model
        for model_data in registered_models:

            # If the Signal Model is same as the one registered through DRF
            if model_data["model_name"] == sender:

                # Only do search for CharField
                fields = [x for x in sender._meta.fields if
                          isinstance(x, CharField) and x.name in model_data["searchable_fields"]]

                for field in fields:
                    # Invalidate Cache
                    cache_instance.invalidate_cache_if_updated(getattr(kwargs["instance"], field.name))
