from rest_framework import serializers

from apps.search.models import UserSearch


class UserSearchSerializer(serializers.ModelSerializer):
    """Serializer for User Search."""

    class Meta:
        model = UserSearch
        fields = (
            'id',
            'keyword',
            'created_at'
        )
