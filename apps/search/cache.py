import ast

from django.conf import settings
from django.core.cache import cache


class DrfCache():
    """
        Custom Drf Cache class for all cahce related activity
    """

    def is_cache_enabled(self):
        """
            Boolean, check if cache is enabled or not
        """
        drf_cache = getattr(settings, "DRF_SEARCH_SETTINGS", None)
        return drf_cache["enable_cache"]

    def get_cache_timeout(self):
        """
            Get Default Cache time out from settings.py
        """
        drf_cache = getattr(settings, "DRF_SEARCH_SETTINGS", None)
        return drf_cache["cache_timeout"]

    def set_cache(self, key, value):
        """
            Set cache if enabled
        """
        if self.is_cache_enabled() and isinstance(value, dict):
            cache.set(key, str(value), timeout=self.get_cache_timeout())

    def get_cache(self, key):
        """
            Get cache if enabled
        """
        if self.is_cache_enabled() and cache.has_key(key):
            # Convert String saved in cache to dict
            return_data = ast.literal_eval(cache.get(key))
        else:
            return_data = False
        return return_data

    def invalidate_cache_if_updated(self, text):
        """
            Invalidate the cache if a matching cache key was found using Regex
        """
        cache.delete_pattern(text[:3] + "*")
