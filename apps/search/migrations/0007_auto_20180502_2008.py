# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-02 20:08
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0006_auto_20180502_1218'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='usersearch',
            options={'verbose_name': 'User search', 'verbose_name_plural': 'User searches'},
        ),
    ]
