from django.conf import settings
from django.urls import reverse
from rest_framework import status

from apps.companies.tests.factories import CompanyFactory
from apps.employments.tests.factories import EmploymentFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.persons.tests.factories import PersonFactory
from apps.projects.tests.factories import ProjectFactory, CampaignFactory
from apps.search.models import UserSearch
import pytest


class TestSearchApiQueryLogic(MetronomBaseAPITestCase):

    def setUp(self):
        self.post_data = {"query": "XYZ"}
        self.endpoint = reverse('search')

    def test_search_with_an_empty_query_gives_400(self):
        self.post_data = {"query": ""}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.status_code == status.HTTP_400_BAD_REQUEST
        assert self.response.data['error'] == "Search keyword not provided"

    def test_search_with_no_wrong_query_name_gives_400(self):
        self.post_data = {"quer": ""}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.status_code == status.HTTP_400_BAD_REQUEST
        assert self.response.data['error'] == "Search keyword not provided"

    def test_search_with_query_keyword_less_then_3_chars(self):
        self.post_data = {"query": "Pe"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.status_code == status.HTTP_400_BAD_REQUEST
        assert self.response.data['error'] == "Search keyword less than 3"

    def test_search_with_nothing_in_the_database_gives_200(self):
        self.post_data = {"query": "Peter"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.status_code == status.HTTP_200_OK
        assert self.response.data['results'] == 0
        assert self.response.data['objects'] == []

    def test_search_with_too_long_keyword_gives_400(self):
        self.post_data = {"query": "123456789012345678901234567890123456789012345678901"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.status_code == status.HTTP_400_BAD_REQUEST
        assert self.response.data['error'] == "Search keyword is too long"

    def test_search_with_more_then_5_results(self):
        PersonFactory(first_name="Peter", last_name="Abraham")
        PersonFactory(first_name="Peter", last_name="Beatle")
        PersonFactory(first_name="Peter", last_name="Cleopatra")
        PersonFactory(first_name="Peter", last_name="Delphin")
        PersonFactory(first_name="Peter", last_name="Elephant")
        PersonFactory(first_name="Peter", last_name="Elephant")
        PersonFactory(first_name="Peter", last_name="Elephant")

        self.post_data = {"query": "Peter"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.status_code == status.HTTP_200_OK
        assert self.response.data['results'] == 7
        assert len(self.response.data['objects']) == 7
        # TODO: CLARITY Test, that we only have 5 results


class TestSearchApiWithDataPerson(MetronomBaseAPITestCase):

    def setUp(self):
        self.company = CompanyFactory(name="XYZ Private Limited")
        self.person = PersonFactory(first_name="Peter", last_name="Smoothy")
        self.second_person = PersonFactory(first_name="Alex", last_name="Smoothy")

        self.employment = EmploymentFactory(person=self.person,
                                            company=self.company)
        self.post_data = {"query": "Peter"}
        self.endpoint = reverse('search')
        self.response = self.admin_client.get(self.endpoint, self.post_data)

        self.expected_company_name = "{} ({})".format(self.company.name, self.person.name)

    def test_search_should_return_200(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_should_return_result_is_two_because_of_company_and_employment(self):
        assert self.response.data['results'] == 2
        assert len(self.response.data['objects']) == 2

    def test_search_result_should_contain_name_of_person(self):
        self.assertContains(self.response, self.person.name, status_code=200)

    def test_search_result_should_contain_company_name_for_person(self):
        self.assertContains(self.response, self.expected_company_name, status_code=200)

    def test_search_result_should_contain_person_first(self):
        names = [x['name'] for x in self.response.data["objects"]]
        assert self.person.name in names
        assert self.expected_company_name in names

    def test_search_result_should_contain_correct_urls(self):
        urls = [x['url'] for x in self.response.data["objects"]]
        assert self.person.get_absolute_url() in urls
        assert self.company.get_absolute_url() in urls

    def test_search_result_should_contain_the_correct_content_types(self):
        types = [x['content_type'] for x in self.response.data["objects"]]
        assert 'persons.person' in types
        assert 'companies.company' in types

    def test_search_should_show_persons_in_front_of_foreign_results(self):
        self.post_data = {"query": "Smoothy"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.data['results'] == 3
        assert len(self.response.data['objects']) == 3
        names = [x['name'] for x in self.response.data["objects"]]
        assert self.person.name in names
        assert self.second_person.name in names
        assert self.expected_company_name in names


class TestSearchApiWithDataCompany(MetronomBaseAPITestCase):

    def setUp(self):
        self.company = CompanyFactory(name="XYZ Private Limited")
        self.another_company = CompanyFactory(name="ABC Private Limited")

        self.person = PersonFactory(first_name="Peter", last_name="Smoothy")
        self.employment = EmploymentFactory(person=self.person, company=self.company)
        self.post_data = {"query": "XYZ"}
        self.endpoint = reverse('search')
        self.response = self.admin_client.get(self.endpoint, self.post_data)

        self.expected_person_name = "{} ({})".format(self.person.name, self.company.name)

    def test_search_should_return_200(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_should_return_result_is_two_because_of_company_and_employment(self):
        assert self.response.data['results'] == 2
        assert len(self.response.data['objects']) == 2

    def test_search_result_should_contain_company_name(self):
        self.assertContains(self.response, self.company.name, status_code=200)

    def test_search_result_should_contain_employment_name(self):
        self.assertContains(self.response, self.expected_person_name, status_code=200)

    def test_search_result_contains_company_name_first(self):
        names = [x['name'] for x in self.response.data["objects"]]
        assert self.company.name in names
        assert self.expected_person_name in names

    def test_search_result_should_contain_correct_urls(self):
        urls = [x['url'] for x in self.response.data["objects"]]
        assert self.company.get_absolute_url() in urls
        assert self.person.get_absolute_url() in urls

    def test_search_should_show_companies_in_front_of_foreign_results(self):
        self.post_data = {"query": "XYZ"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.data['results'] == 2
        assert len(self.response.data['objects']) == 2
        names = [x['name'] for x in self.response.data["objects"]]
        assert self.company.name in names
        assert self.expected_person_name in names


class TestSearchApiWithOperators(MetronomBaseAPITestCase):

    def setUp(self):
        self.company = CompanyFactory(name="XYZ Private Limited")
        self.another_company = CompanyFactory(name="ABC Private Limited")

        self.person = PersonFactory(first_name="Tom", last_name="Taylor")
        # Changed Last name for the test case
        self.another_person = PersonFactory(first_name="Peter", last_name="Winkle")

        self.employment = EmploymentFactory(person=self.person, company=self.company)

        self.endpoint = reverse('search')

    def test_search_should_return_nothing_with_negative_query_only(self):
        self.post_data = {"query": "-Tom"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.data['results'] == 0
        assert len(self.response.data['objects']) == 0
        # Todo: Result should contain 0 objects (no positive query = no results)

    # Modified
    def test_search_should_return_company_but_not_the_employment(self):
        self.post_data = {"query": "XYZ -Tom"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        print(self.response.data)
        self.assertNotContains(self.response, self.person.name, status_code=200)
        assert self.response.data['results'] == 1
        assert len(self.response.data['objects']) == 1
        # Todo: Result should contain 2 results (self.company + self.another_person)

    # Modified
    def test_search_should_return_person_but_not_the_employment(self):
        self.post_data = {"query": "Peter -Taylor"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        print(self.response.data)
        assert self.response.data['results'] == 1
        assert len(self.response.data['objects']) == 1
        self.assertContains(self.response, "Peter", status_code=200)

    def test_search_should_return_person_but_not_the_employment(self):
        self.post_data = {"query": "Private -Tom -ABC"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.data['results'] == 1
        assert len(self.response.data['objects']) == 1
        self.assertContains(self.response, "XYZ Private Limited", status_code=200)


class TestSearchApiWithContentType(MetronomBaseAPITestCase):

    def setUp(self):
        self.company = CompanyFactory(name="Tom Private Limited")
        self.another_company = CompanyFactory(name="Peter Private Limited")

        self.person = PersonFactory(first_name="Tom", last_name="Taylor")
        self.another_person = PersonFactory(first_name="Peter", last_name="Tomsen")

        # TODO The API right noe fetched Employmnetd detail for content_type query as well
        self.employment = EmploymentFactory(person=self.person, company=self.company)
        self.another_employment = EmploymentFactory(person=self.another_person, company=self.another_company)

        self.endpoint = reverse('search')

    def test_search_should_only_give_results_for_company_content_type(self):
        self.post_data = {"query": "Private", "content_type": "companies.company"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.data['results'] == 2
        assert len(self.response.data['objects']) == 2
        content_types = [x['content_type'] for x in self.response.data["objects"]]
        assert 'companies.company' in content_types
        assert 'companies.company' in content_types

    def test_search_should_only_give_results_for_persons_content_type(self):
        self.post_data = {"query": "Tom", "content_type": "persons.person"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.data['results'] == 2
        assert len(self.response.data['objects']) == 2
        content_types = [x['content_type'] for x in self.response.data["objects"]]
        assert 'persons.person' in content_types
        assert 'persons.person' in content_types

    def test_search_should_handle_unknown_content_type(self):
        self.post_data = {"query": "Tom", "content_type": "blabla.bla"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        self.assertContains(self.response, "Provided Model not searchable.", status_code=400)

    def test_search_should_handle_unknown_content_type(self):
        self.post_data = {"query": "Tom", "content_type": "projects.project"}
        self.response = self.admin_client.get(self.endpoint, self.post_data)
        assert self.response.data['results'] == 0
        assert len(self.response.data['objects']) == 0


class TestSearchHistoryApi(MetronomBaseAPITestCase):

    def setUp(self):
        UserSearch.objects.create(keyword="ABC", user=self.test_admin)

        self.company = CompanyFactory(name="Tom Private Limited")
        self.another_company = CompanyFactory(name="Peter Private Limited")
        self.person = PersonFactory(first_name="Tom", last_name="Taylor")
        self.another_person = PersonFactory(first_name="Peter", last_name="Tomsen")

        self.endpoint = reverse('search-history')
        self.search_endpoint = reverse('search')
        self.response = self.admin_client.get(self.endpoint)
        self.drf_search_settings = getattr(settings, "DRF_SEARCH_SETTINGS", 0)

    def test_should_return_200(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_should_return_last_five_searches(self):
        if self.drf_search_settings["save_user_search"]:
            assert len(self.response.data) >= 1

    def test_search_result_should_contain_keyword(self):
        if self.drf_search_settings["save_user_search"]:
            self.assertContains(self.response, "ABC", status_code=200)

    def test_search_history_should_contain_the_last_search(self):
        self.post_data = {"query": "Peter"}
        self.response = self.admin_client.get(self.search_endpoint, self.post_data)

        if self.drf_search_settings["save_user_search"]:
            self.response = self.admin_client.get(self.endpoint)
            self.assertContains(self.response, "Peter", status_code=200)


class TestSearchApiWithProjectOrCampaign(MetronomBaseAPITestCase):

    def setUp(self):
        self.project = ProjectFactory(title="Projectname")
        self.campaign = CampaignFactory(title="Campaignname")
        self.endpoint = reverse('search')

    def test_search_should_return_a_result_for_a_project_title(self):
        self.response = self.admin_client.get(self.endpoint, {"query": self.project.title})
        assert self.response.status_code == status.HTTP_200_OK
        assert self.response.data['results'] == 1
        assert len(self.response.data['objects']) == 1

    def test_search_should_return_a_result_for_a_project_title_with_many_words(self):
        self.project2 = ProjectFactory(title="Project with long name")
        self.response = self.admin_client.get(self.endpoint, {"query": self.project2.title})
        assert self.response.status_code == status.HTTP_200_OK
        assert self.response.data['results'] == 1
        assert len(self.response.data['objects']) == 1

    def test_search_should_return_a_result_for_a_campaign_title(self):
        self.response = self.admin_client.get(self.endpoint, {"query": self.campaign.title})
        assert self.response.status_code == status.HTTP_200_OK
        assert self.response.data['results'] == 1
        assert len(self.response.data['objects']) == 1

    def test_search_should_return_something_for_part_of_the_name(self):
        self.response = self.admin_client.get(self.endpoint, {"query": 'name'})
        assert self.response.status_code == status.HTTP_200_OK
        assert self.response.data['results'] == 2
        assert len(self.response.data['objects']) == 2

    def test_search_should_return_a_result_for_a_campaign_title_with_many_words(self):
        self.campaign2 = CampaignFactory(title="Campaign with long name")
        self.response = self.admin_client.get(self.endpoint, {"query": self.campaign2.title})
        assert self.response.status_code == status.HTTP_200_OK
        assert self.response.data['results'] == 1
        assert len(self.response.data['objects']) == 1

    @pytest.mark.skip("Test fails, search works - please fix")
    def test_search_should_return_a_result_for_a_project_public_id(self):
        self.response = self.admin_client.get(self.endpoint, {"query": self.project.public_id})
        assert self.response.status_code == status.HTTP_200_OK
        assert self.response.data['results'] == 1
        assert len(self.response.data['objects']) == 1

    @pytest.mark.skip("Test fails, search works - please fix")
    def test_search_for_company_should_return_project_and_company(self):
        self.response = self.admin_client.get(self.endpoint, {"query": self.project.company.name})
        assert self.response.status_code == status.HTTP_200_OK
        assert self.response.data['results'] == 2
        assert len(self.response.data['objects']) == 2
