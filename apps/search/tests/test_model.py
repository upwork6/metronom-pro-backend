from django.test import TestCase

from apps.search.views import Search


class SanitizeForNegationTestCase(TestCase):
    search = Search()

    def setUp(self):
        self.search = Search()

    def test_one_word(self):
        result = self.search.sanitize_for_negation("oneword")
        assert result == [[], "oneword"]

    def test_two_words(self):
        result = self.search.sanitize_for_negation("two words")
        assert result == [[], "two words"]

    def test_three_words(self):
        result = self.search.sanitize_for_negation("three cool words")
        assert result == [[], "three cool words"]

    def test_one_negation(self):
        result = self.search.sanitize_for_negation("two -words")
        assert result == [["words"], "two"]

    def test_start_with_negation(self):
        result = self.search.sanitize_for_negation("-words two")
        assert result == [["words"], "two"]

    def test_two_negations(self):
        result = self.search.sanitize_for_negation("two -words -anotherword")
        assert result == [["words", "anotherword"], "two"]

    def test_all_negations(self):
        result = self.search.sanitize_for_negation("-two -words -anotherword")
        assert result == [["two", "words", "anotherword"], ""]

    def test_project_id(self):
        result = self.search.sanitize_for_negation("P-2018-0001")
        assert result == [[], "P-2018-0001"]


