import factory

from apps.search.models import UserSearch
from apps.users.tests.factories import UserFactory


class UserSearchFactory(factory.DjangoModelFactory):
	keyword = factory.Faker('name')
	user = factory.SubFactory(UserFactory)

	class Meta:
		model = UserSearch
