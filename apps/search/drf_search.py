def singleton(class_):
    """
        Utility Class
    """
    instances = {}

    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]

    return getinstance


@singleton
class DrfSearch:
    """
        Singleton class to accept the Model & its field for consumtion of DRF_SEARCH Engine
    """

    # Searchable Models
    searchable_model = []

    def register(self, model):
        """
            Register Django Model to make it searchable
        """
        for m in model:
            self.searchable_model.append(m)
        # self.set_signal( m["model_name"] )

    def get_searchable_models(self):
        return self.searchable_model
