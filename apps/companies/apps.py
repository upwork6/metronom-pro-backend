from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CompanyConfig(AppConfig):
    name = 'apps.companies'
    verbose_name = _('Companies')
