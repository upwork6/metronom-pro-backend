
from dry_rest_permissions.generics import DRYPermissions
from rest_framework import status
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.api.filters import MetronomPermissionsFilter
from apps.api.mixins import LoggerMixin, PublicIDLookupMixin
from apps.autocompletes.sources import ModelAutocompleteSource
from apps.autocompletes.views import BaseAutocompleteView
from apps.bankaccounts.serializers import (BankAccountSerializer,
                                           BankAccountThinSerializer)
from apps.companies.models import Company, Office, Sector
from apps.companies.serializers.base import (CompanyDefaultsSerializer,
                                             CompanyNestedUserSerializer,
                                             CompanySerializer,
                                             OfficeSerializer,
                                             SectorSerializer)
from apps.metronom_commons.mixins.views import (DeleteForbiddenIfUsedMixin,
                                                SerializerDispatcherMixin)
from apps.projects.serializers import CampaignDefaultsSerializer
from apps.employments.models import Employment


class CompanyOfficeViewSet(LoggerMixin, ModelViewSet):
    """Offices of a company."""
    serializer_class = OfficeSerializer
    # filter_backends = (MetronomPermissionsFilter,)

    def get_serializer_context(self):
        """Add company_public_id to the serializer context."""
        context = super().get_serializer_context()
        context['company_id'] = self.kwargs.get('company_id', None)
        return context

    def get_queryset(self):
        """Return only offices connected to the company defined with company_public_id."""
        company_id = self.kwargs.get('company_id', None)
        queryset = Office.objects.filter(company_id=company_id)
        return queryset


class CompanyViewSet(DeleteForbiddenIfUsedMixin, LoggerMixin, PublicIDLookupMixin, SerializerDispatcherMixin, ModelViewSet):
    """Companies in the CRM"""

    serializer_class = CompanySerializer
    queryset = Company.objects.all()
    lookup_field = 'id'
    http_method_names = ['get', 'post', 'patch', 'delete', 'head', 'options', 'trace']
    # filter_backends = (MetronomPermissionsFilter,)

    # https://github.com/jensneuhaus/metronom-pro-backend/issues/350
    # Company can't be deleted if there is still a Person working there => if a Company has Employment can't be deleted
    serializers_dispatcher = {
        "retrieve": CompanyNestedUserSerializer,
    }

    forbid_delete_if_used_in = {
        Employment: ["company"]
    }

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(CompanyNestedUserSerializer(instance).data)

    @detail_route(methods=['post'], url_path='bankaccounts')
    def add_bank_account(self, request, id=None):
        serializer = BankAccountSerializer(data=request.data)
        company = Company.objects.get(id=id)
        if serializer.is_valid(raise_exception=True):
            bank_account = serializer.save()
            company.bank_accounts.add(bank_account)
            company.save()
            bank_account_serializer = BankAccountThinSerializer(bank_account)
            headers = self.get_success_headers(serializer.data)
            return Response(bank_account_serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response(serializer.data)

    @detail_route(url_path='defaults')
    def get_company_and_campaign_defaults(self, *args, **kwargs):
        company = self.get_object()
        campaigns = company.campaigns
        company_defaults_serializer = CompanyDefaultsSerializer(company)
        campaign_defaults_serializer = CampaignDefaultsSerializer(campaigns, many=True)
        data = {**company_defaults_serializer.data, 'campaigns': campaign_defaults_serializer.data}
        return Response(data)


class CompanyAutocompleteView(BaseAutocompleteView):
    """A View for the autocomplete function of the companies."""

    sources = {
        'companies:companies': ModelAutocompleteSource(Company.objects.all(), id_prop='id', name_prop='name')
    }


class SectorViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    queryset = Sector.objects.all().order_by("name")
    serializer_class = SectorSerializer
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    forbid_delete_if_used_in = {
        Company: ["sector"]
    }
