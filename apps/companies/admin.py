from django.contrib import admin

from .models import Company, Office, Sector


@admin.register(Sector)
class SectorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', )
    search_fields = ('name',)


@admin.register(Office)
class OfficeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'address', 'is_headquarter')
    list_filter = ('is_headquarter',)
    search_fields = ('name',)


class OfficeInline(admin.TabularInline):
    model = Office
    extra = 0


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):

    inlines = [OfficeInline, ]

    list_display = (
        'id',
        'public_id',
        'status',
        'name',
        'company_type',
        'sector',
        'uid_number',
        'liable_for_taxation',
        'default_payment_within',
        'added_charges',
        'added_charges_dependency',
        'default_interpolation',
        'default_working_hour_marge',
        'billing_agreement',
        'billing_interval',
    )
    list_filter = ('status', 'sector')

    list_select_related = (
        'sector',
    )
