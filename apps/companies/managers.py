
from safedelete.queryset import SafeDeleteQueryset
from safedelete.managers import SafeDeleteManager


class CompanyQueryset(SafeDeleteQueryset):
    def updated_later_than(self, dt):
        return self.filter(updated_at__gte=dt)

    def __iter__(self):
        offices_by_company_id = self.get_offices_by_company_id()
        for obj in super(CompanyQueryset, self).__iter__():
            self._populate_headquarter_attr(obj, offices_by_company_id)
            yield obj

    def get(self, *args, **kwargs):
        obj = super(CompanyQueryset, self).get(*args, **kwargs)
        self._populate_headquarter_attr(obj)
        return obj

    def _populate_headquarter_attr(self, obj, offices_by_company_id=None):
        if offices_by_company_id is None:
            offices_by_company_id = self.get_offices_by_company_id()

        company_id = self._clear_uuid(obj.pk)

        appropriate_office = offices_by_company_id \
            .get(company_id, {}) \
            .get('is_headquarter', None)
        if appropriate_office is None:
            appropriate_office = offices_by_company_id \
                .get(company_id, {}) \
                .get('is_not_headquarter', None)

        obj.headquarter = appropriate_office

    def _clear_uuid(self, uuid):
        return str(uuid).replace('-', '')

    def get_offices_by_company_id(self):
        # Import here in order there is not an error of cross import.
        from .models import Office
        companies = self
        offices_qs = Office.objects\
            .filter(company__in=companies)\
            .select_related(
                'address',
                'email',
            )

        offices_by_company_id = {}
        for office in offices_qs:
            if office.company_id:
                company_id = self._clear_uuid(office.company_id)
                if company_id not in offices_by_company_id:
                    offices_by_company_id[company_id] = {
                        'is_headquarter': None,
                        'is_not_headquarter': None,
                    }

                if office.is_headquarter:
                    offices_by_company_id[company_id]['is_headquarter'] = office
                else:
                    offices_by_company_id[company_id]['is_not_headquarter'] = office

        return offices_by_company_id


class CompanyManager(SafeDeleteManager):
    def get_queryset(self):
        # Backwards compatibility, no need to move options to QuerySet.
        queryset = CompanyQueryset(self.model, using=self._db, hints=self._hints)
        queryset._safedelete_visibility = self._safedelete_visibility
        queryset._safedelete_visibility_field = self._safedelete_visibility_field
        return queryset
