
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.db.models.signals import (m2m_changed, post_delete, post_save,
                                      pre_delete, pre_save)
from django.dispatch import receiver
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

from apps.accounting.models import Allocation
from apps.addresses.models import Address
from apps.backoffice.utils import get_metronom_setting
from apps.cache_controller.signals import model_instances_changed
from apps.companies.managers import CompanyManager
from apps.contacts.models import Contact
from apps.emails.models import Email
from apps.metronom_commons.data import (ADDED_CHARGES_DEPENDENCY,
                                        BILLING_AGREEMENT, BILLING_DELIVERY,
                                        BILLING_INTERVAL, CURRENCY)
from apps.metronom_commons.fields import LandlinePhoneNumberField, UIDField
from apps.metronom_commons.models import (UserAccessBackofficeMetronomBaseModel,
                                          UserAccessMetronomBaseModel)
from apps.persons.models import Person
from config import settings


class Sector(UserAccessBackofficeMetronomBaseModel):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(verbose_name=_('Description'), blank=True)

    class Meta:
        verbose_name = _('Sector')
        verbose_name_plural = _('Sectors')

    def __str__(self):
        return self.name


class COMPANY_TYPE:
    UNKNOWN = 'unknown'
    SERVICE_PROVIDER = 'service'
    CUSTOMER = 'customer'
    FREELANCER = 'freelancer'

    CHOICES = (
        (UNKNOWN, _('Unknown')),
        (SERVICE_PROVIDER, _('Service provider')),
        (CUSTOMER, _('Customer')),
        (FREELANCER, _('Freelancer')),
    )


class Company(Contact):
    name = models.CharField(_('Name'), max_length=300)

    sector = models.ForeignKey(
        Sector,
        verbose_name=_('Sector'),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        help_text=_('The sector of the company'))

    uid_number = UIDField(_('UID number'), blank=True, null=True)

    liable_for_taxation = models.BooleanField(
        _('Is this company liable of tax?'),
        default=True,
        help_text=_('Is this company liable of paying tax?'),
    )

    default_payment_within = models.IntegerField(
        _('Individual Payment term'),
        blank=True,
        null=True,
        default=get_metronom_setting('DEFAULT_BILLING_PAYMENT_WITHIN'),
        help_text=_('Individual payment term in days - standard value is used if not set'),
    )

    added_charges = models.DecimalField(_('added charges'),
                                        max_digits=5,
                                        decimal_places=2,
                                        null=True,
                                        blank=True,
                                        help_text=_(
                                            'Are additional charges added to projects on top of the worked hours?')
                                        )

    added_charges_dependency = models.CharField(
        _('Added charge dependency'),
        max_length=12,
        choices=ADDED_CHARGES_DEPENDENCY.CHOICES,
        default=ADDED_CHARGES_DEPENDENCY.AGENCY_SERVICES,
        blank=True,
        help_text=_('What are the additional charges added on?')
    )

    default_interpolation = models.DecimalField(
        _('default interpolation'),
        max_digits=5,
        decimal_places=2,
        null=True,
        blank=True,
        help_text=_('Are there some additions on the working hours?')
    )
    default_working_hour_marge = models.DecimalField(
        _('default working hour marge'),
        max_digits=5,
        decimal_places=2,
        null=True,
        blank=True,
        help_text=_('Are we showing different working hours?')

    )

    billing_delivery = models.CharField(
        _('Billing delivery'),
        max_length=20,
        choices=BILLING_DELIVERY.CHOICES,
        blank=True,
        help_text=_('How is the invoice delivered to the company??')
    )

    billing_agreement = models.CharField(
        _('Billing agreement'),
        max_length=20,
        choices=BILLING_AGREEMENT.CHOICES,
        default=BILLING_AGREEMENT.ACCORDING_TO_OFFER,
        blank=True,
        help_text=_('Is the billing based on the offer or real amount of hours?')
    )

    billing_interval = models.CharField(
        _('Billing interval'),
        max_length=15,
        choices=BILLING_INTERVAL.CHOICES,
        blank=True,
        help_text=_('When are invoices sent?')
    )
    billing_currency = models.CharField(
        _('Master currency'),
        max_length=3,
        choices=CURRENCY.CHOICES,
        default=get_metronom_setting('DEFAULT_BILLING_CURRENCY'),
        help_text=_('This is the currency in which we work for the customer.'),
    )

    billing_address = models.ForeignKey(
        Address,
        verbose_name=_('billing address'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )

    key_account_manager = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='companies_as_key_account_manager',
        blank=True,
        null=True,
        help_text=_('Who is the key account manager of this company?')
    )

    bank_accounts = models.ManyToManyField(
        'bankaccounts.BankAccount',
        verbose_name=_('Bank accounts'),
        related_name='companies',
        blank=True,
        help_text=_('Bank accounts of the company')
    )

    allocations = models.ManyToManyField(Allocation,
                                         verbose_name=_('Allocations'),
                                         related_name='companies',
                                         blank=True,
                                         help_text=_('Accounting Allocations for the company')
                                         )

    objects = CompanyManager()

    class Meta:
        verbose_name = _('Company')
        verbose_name_plural = _('Companies')

    def __init__(self, *args, **kwargs):
        super(Company, self).__init__(*args, **kwargs)
        # The attribute `headquarter` contains Office or None
        # and are populated in CompanyManager (methods `__iter__` and `populate_headquarter`)

    def __str__(self):
        return self.name

    # TODO: is this still correct?
    def get_absolute_url(self):
        return f'/companies/{self.public_id}/'

    @cached_property
    def company_type(self):
        address = self.address1

        if self.projects.exists():
            return COMPANY_TYPE.CUSTOMER
        elif self.allocations.exists():
            return COMPANY_TYPE.SERVICE_PROVIDER

        return COMPANY_TYPE.UNKNOWN

    @cached_property
    def full_name(self):
        return self.name

    @cached_property
    def main_phone(self):
        # TODO: Get the phone of the office which is headquarter
        try:
            main_phone = self.headquarter.landline_phone
            return main_phone
        except AttributeError:
            return None

    @cached_property
    def main_email(self):
        # TODO: Get the email of the office which is headquarter
        try:
            return self.headquarter.email.email
        except AttributeError:
            return None

    @cached_property
    def address1(self):
        # TODO: Get the address of the office which is headquarter
        try:
            return self.headquarter.address.address1
        except AttributeError:
            return None

    @cached_property
    def postal_code(self):
        # TODO: Get the address of the office which is headquarter
        try:
            return self.headquarter.address.postal_code
        except AttributeError:
            return None

    @cached_property
    def city(self):
        # TODO: Get the address of the office which is headquarter
        try:
            return self.headquarter.address.city
        except AttributeError:
            return None

    @cached_property
    def country_code(self):
        # TODO: Get the address of the office which is headquarter
        try:
            return self.headquarter.address.country.code
        except AttributeError:
            return None

    @cached_property
    def employment_count(self):
        # TODO: Get the count of employments
        # TODO Move to queryset like a latest_employment attr
        from apps.employments.models import Employment
        count = Employment.objects.filter(company=self).count()
        return count

    def get_contacts_persons(self):
        """ Return all the persons who are related to this company

        :returns: queryset
        """
        return Person.objects.filter(pk__in=self.employments.values_list('person', flat=True))


class Office(UserAccessMetronomBaseModel):
    """
        The office of a company - it contains an address and additional data (email, phone, landline_phone)
        Only one office can be a headquarter.
    """

    HEADQUARTER_NAME = _('Headquarter')

    company = models.ForeignKey(Company, related_name="offices", on_delete=models.CASCADE)

    address = models.OneToOneField(Address, related_name='office', verbose_name=_('Address'), null=True, blank=True)
    email = models.OneToOneField(Email, related_name='office', verbose_name=_('Email'), null=True, blank=True)
    landline_phone = LandlinePhoneNumberField(blank=True)

    name = models.CharField(max_length=100, blank=True)

    is_headquarter = models.BooleanField(
        _('Is headquarter'),
        default=False,
        help_text=_('Is this Office the headquarter?'),
    )

    class Meta:
        verbose_name = _('Office')
        verbose_name_plural = _('Offices')

    def __str__(self):
        if self.is_headquarter:
            return f'{self.company} - {self.HEADQUARTER_NAME}'
        elif self.name:
            return f'{self.company} - {self.name}'
        else:
            return f'{self.company} - Office'

    def save(self, *args, **kwargs):
        """Set is_headquarter to True for first office; set to False on all other offices of company if True."""
        is_first_office_of_company = not self.company.offices.all().exists()
        if is_first_office_of_company:
            self.is_headquarter = True

        if self.is_headquarter:
            self.name = self.HEADQUARTER_NAME
        else:
            if self.name == self.HEADQUARTER_NAME:
                raise ValidationError(_('Only headquarter can be named %s') % self.HEADQUARTER_NAME)
            elif not self.name:
                raise ValidationError(_('Non headquarter offices must be named'))

        super().save(*args, **kwargs)


@receiver(post_save)
@receiver(post_delete)
@receiver(m2m_changed)
def actualize_company_updated_date(sender, instance, **kwargs):
    # TODO Dmitry Koldunov: Lets think about reliability of this solution. It is potential place of mistake.
    # In future can change dependencies between models but a developer will forget to change this code.
    # Maybe better invalidate cache when ANY model instance has been changed.
    now = timezone.now()

    companies = Company.objects.none()
    if isinstance(instance, Company):
        companies = Company.objects.filter(pk=instance.pk)
    elif isinstance(instance, Email):
        email = instance
        companies = Company.objects.filter(offices__email=email)
    elif isinstance(instance, Address):
        address = instance
        companies = Company.objects.filter(offices__address=address)
    elif isinstance(instance, Sector):
        sector = instance
        companies = Company.objects.filter(sector=sector)

    if companies.exists():
        companies.update(updated_at=now)
        model_instances_changed.send(sender=Company, instances=companies)


@receiver(pre_save)
@receiver(pre_delete)
def actualize_company_updated_date_by_employment(sender, instance, **kwargs):
    now = timezone.now()

    from apps.employments.models import Employment
    if isinstance(instance, Employment):
        employment = instance

        filters = Q(employments=employment)
        if employment.company:
            filters |= Q(pk=employment.company)

        companies = Company.objects.filter(filters)
        companies.update(updated_at=now)

        model_instances_changed.send(sender=Company, instances=companies)


@receiver(post_save, sender=Company)
@receiver(post_delete, sender=Company)
@receiver(m2m_changed, sender=Company)
def clear_company_autocomplete_view_output_cache(sender, instance=None, **kwargs):
    from apps.companies.views import CompanyAutocompleteView
    CompanyAutocompleteView.clear_cache()
