# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-03-20 13:19
from __future__ import unicode_literals

import apps.metronom_commons.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('accounting', '0001_initial'),
        ('addresses', '0001_initial'),
        ('contacts', '0001_initial'),
        ('users', '0001_initial'),
        ('phonenumbers', '0001_initial'),
        ('emails', '0001_initial'),
        ('bankaccounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('contact_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE,
                                                     parent_link=True, primary_key=True, serialize=False, to='contacts.Contact')),
                ('name', models.CharField(max_length=300, verbose_name='Name')),
                ('uid_number', apps.metronom_commons.fields.UIDField(
                    blank=True, max_length=9, null=True, verbose_name='UID number')),
                ('short_name', models.CharField(max_length=10, verbose_name='Organization unit code')),
                ('default_taxation', models.FloatField(default=8.0,
                                                       help_text='What is the default taxation?', null=True, verbose_name='Taxation')),
                ('default_payment_within', models.IntegerField(
                    blank=True, help_text='Individual payment term in days - standard value is used if not set', null=True, verbose_name='Individual Payment term')),
                ('added_charges', models.DecimalField(blank=True, decimal_places=2,
                                                      help_text='Are additional charges added to projects on top of the worked hours?', max_digits=5, null=True, verbose_name='added charges')),
                ('added_charges_dependency', models.CharField(blank=True, choices=[('agency', 'Agency'), ('external', 'External services'), (
                    'passing', 'Passing services')], default='agency', help_text='What are the additional charges added on?', max_length=12, verbose_name='Added charge dependency')),
                ('default_interpolation', models.DecimalField(blank=True, decimal_places=2,
                                                              help_text='Are there some additions on the working hours?', max_digits=5, null=True, verbose_name='default interpolation')),
                ('default_working_hour_marge', models.DecimalField(blank=True, decimal_places=2,
                                                                   help_text='Are we showing different working hours?', max_digits=5, null=True, verbose_name='default working hour marge')),
                ('billing_delivery', models.CharField(blank=True, choices=[('post', 'with Post'), (
                    'email', 'with Email')], help_text='How is the invoice delivered to the company??', max_length=20, verbose_name='Billing delivery')),
                ('billing_agreement', models.CharField(blank=True, choices=[('according_to_offer', 'According to offer'), ('according_to_effort', 'According to effort')],
                                                       default='according_to_offer', help_text='Is the billing based on the offer or real amount of hours?', max_length=20, verbose_name='Billing agreement')),
                ('billing_interval', models.CharField(blank=True, choices=[('task_based', 'Task based'), ('project_based', 'Project based'), ('campaign_based', 'Campaign based'), ('monthly', 'Monthly'), (
                    'quarterly', 'Quarterly'), ('semi-annual', 'Semi-annual'), ('annual', 'Annual')], help_text='When are invoices sent?', max_length=15, verbose_name='Billing interval')),
                ('billing_currency', models.CharField(choices=[('CHF', 'CHF'), ('EUR', 'EUR'), ('USD', 'USD'), (
                    'GBP', 'GBP')], default='CHF', help_text='This is the currency in which we work for the customer.', max_length=3, verbose_name='Master currency')),
                ('allocations', models.ManyToManyField(blank=True, help_text='Accounting Allocations for the company',
                                                       related_name='companies', to='accounting.Allocation', verbose_name='Allocations')),
                ('bank_accounts', models.ManyToManyField(blank=True, help_text='Bank accounts of the company',
                                                         related_name='companies', to='bankaccounts.BankAccount', verbose_name='Bank accounts')),
                ('billing_address', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL,
                                                      to='addresses.Address', verbose_name='billing address')),
            ],
            options={
                'verbose_name': 'Company',
                'verbose_name_plural': 'Companies',
            },
            bases=('contacts.contact',),
        ),
        migrations.CreateModel(
            name='Office',
            fields=[
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False,
                                        primary_key=True, serialize=False, unique=True, verbose_name='ID')),
                ('landline_phone', apps.metronom_commons.fields.LandlinePhoneNumberField(blank=True, max_length=128)),
                ('name', models.CharField(blank=True, max_length=100)),
                ('is_headquarter', models.BooleanField(default=False,
                                                       help_text='Is this Office the headquarter?', verbose_name='Is headquarter')),
                ('address', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                                 related_name='office', to='addresses.Address', verbose_name='Address')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                                              related_name='offices', to='companies.Company')),
                ('email', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                               related_name='office', to='emails.Email', verbose_name='Email')),
            ],
            options={
                'verbose_name': 'Office',
                'verbose_name_plural': 'Offices',
            },
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False,
                                        primary_key=True, serialize=False, unique=True, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
            ],
            options={
                'verbose_name': 'Sector',
                'verbose_name_plural': 'Sectors',
            },
        ),
        migrations.AddField(
            model_name='company',
            name='key_account_manager',
            field=models.ForeignKey(blank=True, help_text='Who is the key account manager of this company?', null=True,
                                    on_delete=django.db.models.deletion.CASCADE, related_name='companies_as_key_account_manager', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='company',
            name='sector',
            field=models.ForeignKey(blank=True, help_text='The sector of the company', null=True,
                                    on_delete=django.db.models.deletion.SET_NULL, to='companies.Sector', verbose_name='Sector'),
        ),
        migrations.RemoveField(
            model_name='company',
            name='short_name',
        ),
        migrations.RemoveField(
            model_name='company',
            name='default_taxation',
        ),
        migrations.AddField(
            model_name='company',
            name='liable_for_taxation',
            field=models.BooleanField(default=True, help_text='Is this company liable of paying tax?',
                                      verbose_name='Is this company liable of tax?'),
        ),
        migrations.AlterField(
            model_name='company',
            name='default_payment_within',
            field=models.IntegerField(blank=True, default=30, help_text='Individual payment term in days - standard value is used if not set',
                                      null=True, verbose_name='Individual Payment term'),
        ),
        migrations.AddField(
            model_name='office',
            name='is_void',
            field=models.BooleanField(default=False, verbose_name='Is in void?'),
        ),
        migrations.AddField(
            model_name='sector',
            name='is_void',
            field=models.BooleanField(default=False, verbose_name='Is in void?'),
        ),
        migrations.AlterField(
            model_name='office',
            name='created_date',
            field=model_utils.fields.AutoCreatedField(
                default=django.utils.timezone.now, editable=False, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='office',
            name='id',
            field=models.UUIDField(db_index=True, default=uuid.uuid4, editable=False,
                                   primary_key=True, serialize=False, unique=True, verbose_name='UUID'),
        ),
        migrations.AlterField(
            model_name='office',
            name='updated_date',
            field=model_utils.fields.AutoLastModifiedField(
                default=django.utils.timezone.now, editable=False, verbose_name='modified'),
        ),
        migrations.AlterField(
            model_name='sector',
            name='created_date',
            field=model_utils.fields.AutoCreatedField(
                default=django.utils.timezone.now, editable=False, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='sector',
            name='id',
            field=models.UUIDField(db_index=True, default=uuid.uuid4, editable=False,
                                   primary_key=True, serialize=False, unique=True, verbose_name='UUID'),
        ),
        migrations.AlterField(
            model_name='sector',
            name='updated_date',
            field=model_utils.fields.AutoLastModifiedField(
                default=django.utils.timezone.now, editable=False, verbose_name='modified'),
        ),
    ]
