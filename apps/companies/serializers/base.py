
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from apps.accounting.serializers import AllocationSerializer
from apps.addresses.models import Address
from apps.addresses.serializers import AddressSerializer
from apps.bankaccounts.serializers import BankAccountThinSerializer
from apps.cache_controller.serializer_cache_controllers import \
    ModelSerializerCacheControllerRegister
from apps.companies.models import Company, Office, Sector
from apps.emails.models import Email
from apps.metronom_commons.serializers import (ForeignKeyFieldsCreateOrUpdateMixin,
                                               SoftDeletionSerializer)
from apps.metronom_commons.utils import pop_create
from apps.projects.serializers import \
    AddressSerializer as CompactAddressSerializer
from apps.projects.serializers import ProjectPersonSerializer
from apps.users.serializers import UserUUIDSerializer

User = get_user_model()


class SectorSerializer(serializers.ModelSerializer):
    """Serialize data from the Sector."""
    uuid = serializers.UUIDField(source="id", required=False)

    class Meta:
        model = Sector
        fields = (
            'uuid',
            'name',
            'description'
        )


class OfficeSerializer(ForeignKeyFieldsCreateOrUpdateMixin, serializers.ModelSerializer):
    """Serialize data from the Sector."""

    email = serializers.EmailField(source='email.email', required=False)
    address = AddressSerializer(required=False)
    name = serializers.CharField(
        allow_blank=True,
        required=False,
    )

    class Meta:
        model = Office
        fields = (
            'address',
            'email',
            'id',
            'is_headquarter',
            'landline_phone',
            'name',
        )
        fk_keys = [
            'address',
            'email',
        ]

    def validate(self, attrs):
        office_already_exists = getattr(self.instance, 'pk', None)

        if not office_already_exists:
            if attrs.get('address') and attrs.get('is_headquarter') is None:
                raise serializers.ValidationError(
                    _('The field is_headquarter is required for the office with address.'))

        return attrs

    def validate_is_headquarter(self, new_value):
        instance = self.instance
        office_already_exists = getattr(instance, 'pk', None)
        old_value = getattr(instance, 'is_headquarter', None)
        is_headquarter_has_been_changed = new_value != old_value

        if office_already_exists and is_headquarter_has_been_changed:
            raise serializers.ValidationError(_('The field is_headquarter must remain unchanged.'))
        return new_value

    def create(self, validated_data):
        """Create a new Office for supplied Company and use nested data for Email and Address."""
        office = super(OfficeSerializer, self).create(validated_data, commit=False)

        company_id = self.context['company_id']
        company = Company.objects.get(id=company_id)
        office.company = company
        try:
            office.save()
        except ValidationError as e:
            raise serializers.ValidationError(e.message)

        return office


class CompanyBaseSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    """Serialize data from the Company.

        {
        "name":"Name of the company",
        "sector_uuid":"84f86cce-563e-41f0-9511-76409a38ed2a",
        "status":"active",
        "key_account_manager_uuid":"59982434-5df8-4741-baaa-550244c355b5",
        "offices":[
            {
                "is_headquarter":true,
                "address":
                {
                    "address1":"Hochstraße 72",
                    "postal_code":"47228",
                    "city":"Duisburg",
                    "country":"DE"
                }
            }
        ],
        "bankaccounts": [
        ]

    }
    """
    from apps.employments.serializers import EmploymentSerializerForCompany

    sector_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Sector.objects.all(),
        source="sector",
    )
    billing_address_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Address.objects.all(),
        source="billing_address",
        required=False,
    )

    # Nested data
    bank_accounts = BankAccountThinSerializer(many=True, read_only=True)
    employments = EmploymentSerializerForCompany(many=True, read_only=True)
    offices = OfficeSerializer(many=True, required=False, allow_null=True)
    allocations = AllocationSerializer(many=True, required=False, allow_null=True)
    billing_address = AddressSerializer(required=False, allow_null=True)

    class Meta:
        model = Company
        fields = (
            'added_charges',
            'added_charges_dependency',
            'bank_accounts',
            'billing_delivery',
            'billing_agreement',
            'billing_currency',
            'billing_interval',
            'company_type',
            'default_interpolation',
            'default_payment_within',
            'liable_for_taxation',
            'default_working_hour_marge',
            'employments',
            'id',
            'name',
            'offices',
            'public_id',
            'sector_uuid',
            'status',
            'uid_number',
            'allocations',
            'billing_address',
            'billing_address_uuid',
        )
        read_only_fields = ('id', 'public_id', 'company_type')

    def validate_offices(self, attrs):
        headquarters = [r.get('is_headquarter') for r in attrs if r.get('is_headquarter')]
        if len(headquarters) > 1:
            raise serializers.ValidationError(_('The field offices should contain only one headquarter'))

        return attrs

    def create(self, validated_data):
        """Use the respective IDs for writing ForeignKey and Many2Many relationships and write nested offices."""

        offices_data = validated_data.pop('offices', [])

        sector = validated_data.pop('sector', None)

        billing_address_data = validated_data.pop('billing_address', None)

        company = Company.objects.create(**validated_data)

        for office_data in offices_data:
            email = pop_create(office_data, 'email', Email)
            address = pop_create(office_data, 'address', Address)
            Office.objects.create(company=company, email=email, address=address, **office_data)

        if billing_address_data:
            billing_address = Address.objects.create(
                **billing_address_data
            )
            company.billing_address = billing_address

        company.sector = sector

        company.save()
        return company

    def update(self, instance, validated_data):
        """Use the respective IDs and UUIDs for updating ForeignKey and Many2Many relationships."""
        instance.sector = validated_data.pop('sector', instance.sector)

        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        return instance


class CompanyNestedUserSerializer(CompanyBaseSerializer):

    key_account_manager = UserUUIDSerializer()
    billing_address = CompactAddressSerializer()

    class Meta(CompanyBaseSerializer.Meta):
        fields = CompanyBaseSerializer.Meta.fields + (
            'key_account_manager',
        )


class CompanySerializer(CompanyBaseSerializer):
    key_account_manager_uuid = PrimaryKeyRelatedField(
        queryset=User.objects.all(),
        source="key_account_manager",
        required=False
    )

    def validate_key_account_manager_uuid(self, data):
        if not self.context['view'].action == 'create':
            user = self.context['request'].user
            valid_roles = set(['Admin', 'Accounting'])
            user_roles = list(user.groups.values_list('name', flat=True))
            if not set(user_roles).intersection(valid_roles):
                raise serializers.ValidationError(_("You don't have permissions to change this field"))
        return data

    class Meta(CompanyBaseSerializer.Meta):
        fields = CompanyBaseSerializer.Meta.fields + (
            'key_account_manager_uuid',
        )


class CompanyDefaultsSerializer(serializers.ModelSerializer):
    """Serialize defaults that are shown when creating campaigns and projects."""

    key_account_manager_uuid = PrimaryKeyRelatedField(
        queryset=User.objects.all(),
        source="key_account_manager",
    )
    offices = OfficeSerializer(many=True, required=False, allow_null=True)
    contacts_persons = ProjectPersonSerializer(many=True, source="get_contacts_persons", read_only=True)
    # TODO: Which one of the following is used in the Frontend?
    billing_address = AddressSerializer(required=False, allow_null=True, read_only=True)
    billing_address_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Address.objects.all(),
        source="billing_address",
        required=False,
    )

    class Meta:
        model = Company
        fields = (
            'liable_for_taxation',
            'default_payment_within',
            'added_charges',
            'added_charges_dependency',
            'default_interpolation',
            'default_working_hour_marge',
            'billing_address',
            'billing_address_uuid',
            'billing_agreement',
            'billing_currency',
            'billing_interval',
            'projects',
            'offices',
            'key_account_manager_uuid',
            'contacts_persons'
        )


class CompanyContactListSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    """Serialize data from the Company."""

    full_name = serializers.CharField(read_only=True)
    phone = PhoneNumberField(source="main_phone")
    email = serializers.EmailField(source='main_email')
    sector_uuid = serializers.PrimaryKeyRelatedField(read_only=True, source="sector")

    class Meta:
        model = Company
        fields = (
            'address1',
            'city',
            'company_type',
            'country_code',
            'email',
            'employment_count',
            'full_name',
            'id',
            'phone',
            'postal_code',
            'public_id',
            'sector_uuid',
            'status',
        )


ModelSerializerCacheControllerRegister.register_serializer(CompanyContactListSerializer)
