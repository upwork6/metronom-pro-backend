from rest_framework import serializers

from apps.companies.models import Company


class CompanyInEmploymentSerializer(serializers.ModelSerializer):
    """Serialize name and public_id from the Company."""

    #TODO: add uuid field inside nested company serializer: used inside EmploymentViewSet
    class Meta:
        model = Company
        fields = (
            'public_id',
            'name',
        )
