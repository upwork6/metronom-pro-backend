import copy

from django.core.exceptions import ValidationError
from django.test import TestCase

from apps.accounting.tests.factories import AllocationFactory
from apps.emails.tests.factories import EmailFactory
from apps.projects.tests.factories import ProjectFactory

from ..models import COMPANY_TYPE, Company, Office
from .factories import CompanyFactory


class TestOfficeIsHeadquarter(TestCase):

    def setUp(self):
        self.company = CompanyFactory()

    def test_one_office_defaults_to_headquarter(self):
        office = Office.objects.create(company=self.company)
        assert office.is_headquarter

    def test_second_office_defaults_not_to_headquarter(self):
        office1 = Office.objects.create(company=self.company)
        office2 = Office.objects.create(company=self.company, name='office')
        office1.refresh_from_db()
        assert office1.is_headquarter
        assert not office2.is_headquarter

    def test_office_name(self):
        headquarter_office = Office.objects.create(company=self.company, is_headquarter=True)
        headquarter_office.name = Office.HEADQUARTER_NAME

        office_wrong_name = Office.HEADQUARTER_NAME
        with self.assertRaises(ValidationError):
            headquarter_office = Office.objects.create(
                company=self.company,
                is_headquarter=False,
                name=office_wrong_name,
            )

        office_name = 'office'
        office = Office.objects.create(
            company=self.company,
            is_headquarter=False,
            name=office_name,
        )
        assert office.name == office_name


class TestOfficeIsHeadquarterForSeparateCompanies(TestCase):

    def setUp(self):
        self.company1 = CompanyFactory()
        self.company2 = CompanyFactory()

    def test_one_office_defaults_to_headquarter(self):
        office_company1 = Office.objects.create(company=self.company1)
        office_company2 = Office.objects.create(company=self.company2, name='office')
        office_company1.refresh_from_db()
        assert office_company1.is_headquarter
        assert office_company2.is_headquarter

    def test_second_office_defaults_not_to_headquarter(self):
        office1_company1 = Office.objects.create(company=self.company1)
        office2_company1 = Office.objects.create(company=self.company1, name='office')
        office1_company2 = Office.objects.create(company=self.company2)
        office2_company2 = Office.objects.create(company=self.company2, name='office')
        office1_company1.refresh_from_db()
        office2_company1.refresh_from_db()
        office1_company2.refresh_from_db()
        assert office1_company1.is_headquarter
        assert not office2_company1.is_headquarter
        assert office1_company2.is_headquarter
        assert not office2_company2.is_headquarter

    def test_low_performance_getting_office(self):
        count_companies = 4
        for i in range(count_companies):
            company = CompanyFactory()
            Office.objects.create(
                company=company,
                email=EmailFactory(),
            )
        with self.assertNumQueries(2):
            self.assertTrue([x.headquarter for x in Company.objects.all() if x.headquarter])

        with self.assertNumQueries(2):
            self.assertTrue([x.main_email for x in Company.objects.all() if x.headquarter])


class TestCompanyType(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company = CompanyFactory()
        Office.objects.create(
            company=cls.company,
            email=EmailFactory(),
        )

    def test_company_type_is_customer(self):
        company = copy.copy(self.company)
        ProjectFactory(company=company)
        assert company.company_type == COMPANY_TYPE.CUSTOMER

    def test_company_type_is_service_provider(self):
        company = copy.copy(self.company)
        company.allocations.add(AllocationFactory())
        assert company.company_type == COMPANY_TYPE.SERVICE_PROVIDER

    def test_company_type_is_unknown(self):
        assert self.company.company_type == COMPANY_TYPE.UNKNOWN

    def test_company_type_is_customer_with_projects_and_allocations_both(self):
        company = copy.copy(self.company)
        company.allocations.add(AllocationFactory())
        ProjectFactory(company=company)

        assert company.company_type == COMPANY_TYPE.CUSTOMER


class CompanySoftDeleteTest(TestCase):

    def test_soft_deletion(self):
        company1 = CompanyFactory()
        company2 = CompanyFactory()
        company3 = CompanyFactory()
        self.assertEqual(Company.objects.count(), 3)
        company3.delete()
        self.assertEqual(Company.objects.count(), 2)
        self.assertEqual(Company.all_objects.count(), 3)

    def test_soft_deletion_public_id(self):
        company1 = CompanyFactory()
        company2 = CompanyFactory()
        company3 = CompanyFactory()
        self.assertEqual(Company.objects.count(), 3)
        company3.delete()
        self.assertEqual(Company.objects.count(), 2)
        self.assertEqual(Company.all_objects.count(), 3)
        company4 = CompanyFactory()
        self.assertEqual(Company.objects.count(), 3)
        self.assertEqual(Company.all_objects.count(), 4)
        public_id = company3.public_id
        self.assertEqual(company4.public_id, public_id + 1)
        company3.save()
        self.assertEqual(Company.objects.count(), 4)
