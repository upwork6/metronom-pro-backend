from django.test import TestCase
from django.urls import resolve, reverse
from rest_framework import status

from apps.companies.tests.factories import CompanyFactory, OfficeFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase


class TestCompanyURLs(TestCase):
    def setUp(self):
        self.company = CompanyFactory()
        self.office = OfficeFactory(company=self.company)

    def test_company_base_reverse(self):
        assert reverse('api:company-list') == '/api/companies/'

    def test_company_base_resolve(self):
        assert resolve('/api/companies/').view_name == 'api:company-list'

    def test_company_detail_reverse(self):
        endpoint = reverse('api:company-detail', kwargs={'id': self.company.id})
        assert endpoint == f'/api/companies/{self.company.id}/'

    def test_company_detail_resolve(self):
        assert resolve(f'/api/companies/{self.company.public_id}/').view_name == 'api:company-detail'

    def test_company_add_bank_account_reverse(self):
        endpoint = reverse('api:company-bankaccounts', kwargs={'id': self.company.id})
        assert endpoint == f'/api/companies/{self.company.id}/bankaccounts/'

    def test_company_add_bank_account_resolve(self):
        assert resolve(f'/api/companies/{self.company.id}/bankaccounts/').view_name == 'api:company-bankaccounts'

    def test_offices_list_reverse(self):
        endpoint = reverse('api_company:company-offices-list', kwargs={'company_id': self.company.id})
        assert endpoint == f'/api/companies/{self.company.id}/offices/'

    def test_offices_list_resolve(self):
        view_name = resolve(f'/api/companies/{self.company.id}/offices/').view_name
        assert view_name == 'api_company:company-offices-list'

    def test_offices_detail_reverse(self):
        endpoint = reverse(
            'api_company:company-offices-detail',
            kwargs={'company_id': self.company.id, 'pk': self.office.id})
        assert endpoint == f'/api/companies/{self.company.id}/offices/{self.office.id}/'

    def test_offices_detail_resolve(self):
        view_name = resolve(f'/api/companies/{self.company.id}/offices/{self.office.id}/').view_name
        assert view_name == 'api_company:company-offices-detail'

    def test_defaults_for_campaigns_and_projects_reverse(self):
        endpoint = reverse('api:company-defaults', kwargs={'id': self.company.id})
        assert endpoint == f'/api/companies/{self.company.id}/defaults/'

    def test_defaults_for_campaigns_and_projects_resolve(self):
        view_name = resolve(f'/api/companies/{self.company.id}/defaults/').view_name
        assert view_name == 'api:company-defaults'


class TestAutocompleteURLs(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.companies_endpoint = reverse('autocompletes:companies')

        cls.methods = (
            cls.user_client.post,
            cls.user_client.put,
            cls.user_client.delete,
            cls.user_client.patch,
        )
        cls.endpoints = (cls.companies_endpoint,)

    def test_methods_not_allowed(self):
        for method in self.methods:
            for endpoint in self.endpoints:
                response = method(endpoint)
                self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_company_autocomplete_reverse(self):
        assert self.companies_endpoint == f'/autocompletes/companies/'

    def test_company_autocomplete_resolve(self):
        view_name = resolve(f'/autocompletes/companies/').view_name
        assert view_name == 'autocompletes:companies'
