
import copy
import json
import uuid

import pytest
from allauth.account.models import EmailAddress
from django.contrib.auth.models import Group
from django.core.cache import caches
from django.urls import reverse
from rest_framework import status

from apps.addresses.models import Address
from apps.addresses.serializers import AddressSerializer
from apps.addresses.tests.factories import AddressFactory
from apps.projects.tests.factories import CampaignFactory
from apps.companies.models import COMPANY_TYPE, Company, Sector
from apps.companies.tests.factories import (CompanyFactory, OfficeFactory,
                                            SectorFactory)
from apps.emails.models import Email
from apps.emails.tests.factories import EmailFactory
from apps.employments.tests.factories import EmploymentFactory
from apps.metronom_commons.data import BILLING_AGREEMENT, BILLING_INTERVAL
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.persons.tests.factories import PersonFactory
from apps.users.tests.factories import UserFactory


class TestCompanyList(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company1 = CompanyFactory()
        cls.company2 = CompanyFactory(name="Second")
        cls.endpoint = reverse('api:company-list')
        cls.response = cls.user_client.get(cls.endpoint)

    def test_get_should_return_ok_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_get_should_contain_the_list_of_companies(self):
        company_names = [self.company1.name, self.company2.name]
        self.assertTrue(self.response.data[0]['name'] in company_names)
        self.assertTrue(self.response.data[1]['name'] in company_names)

    def test_get_should_contain_two_companys(self):
        assert len(self.response.data) == 2

    def test_result_should_contain_ids(self):
        # we need str, because we'll get UUID objects in other case
        company_ids = [str(self.company1.id), str(self.company2.id)]
        self.assertTrue(self.response.data[0]['id'] in company_ids)
        self.assertTrue(self.response.data[1]['id'] in company_ids)

    def test_result_should_stringfy_decimal_fields(self):
        # Checking consistent handling of the numbers. Int as Int, Str as Str.
        for company in self.response.data:
            self.assertTrue(isinstance(company['default_payment_within'], int))
            self.assertTrue(isinstance(company['added_charges'], str))
            self.assertTrue(isinstance(company['default_interpolation'], str))
            self.assertTrue(isinstance(company['default_working_hour_marge'], str))

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        self.login_as_accounting_user()
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])


class TestCompanyDetails(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        key_account_manager = UserFactory()
        cls.company = CompanyFactory(key_account_manager=key_account_manager)
        cls.endpoint = reverse('api:company-detail', kwargs={'id': cls.company.id})
        cls.response = cls.user_client.get(cls.endpoint)

    def test_get_should_return_ok_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_get_should_contain_the_company_name(self):
        assert self.response.data['name'] == self.company.name

    def test_get_should_contain_nested_key_account_manager(self):
        self.assertIsNone(self.response.data.get('key_account_manager_uuid', None))
        self.assertIsNotNone(self.response.data.get('key_account_manager', None))
        self.assertIsNotNone(self.response.data.get('key_account_manager').get('has_image', None))

    def test_result_should_contain_the_correct_uuid(self):
        assert self.response.data['id'] == str(self.company.pk)

    def test_result_should_contain_a_public_id(self):
        assert self.response.data['public_id'] == self.company.public_id

    def test_result_should_contain_a_bank_accounts(self):
        self.assertTrue('bank_accounts' in self.response.data)
        account_public_names = [x.public_name for x in self.company.bank_accounts.all()]
        self.assertTrue(self.response.data['bank_accounts'][0]['public_name'] in account_public_names)

    def test_request_should_work_both_with_the_uuid_and_public_id(self):
        endpoint = reverse('api:company-detail', kwargs={'id': self.company.public_id})
        response = self.user_client.get(endpoint)
        assert response.status_code == status.HTTP_200_OK
        assert response.data['name'] == self.company.name
        assert response.data['public_id'] == self.company.public_id

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        self.login_as_accounting_user()
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])


class TestCreateCompany(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:company-list')

        cls.sector = SectorFactory()
        cls.other_sector = SectorFactory()

        cls.key_account_manager1 = UserFactory()
        cls.key_account_manager2 = UserFactory()

        cls.name = 'ABC Inc.'

        cls.post_data = {
            'name': cls.name,
            'sector_uuid': cls.sector.id,
        }

    def test_empty_post_should_give_an_error_on_required_fields(self):
        response = self.user_client.post(self.endpoint, data={}, format='json')

        self.assertInErrorResponse(response, {
            "name": ["This field is required."],
            "sector_uuid": ["This field is required."]
        })

    def test_minimal_data_post(self):
        response = self.user_client.post(self.endpoint, data=self.post_data, format='json')
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data['name'] == self.name
        assert response.data['sector_uuid'] == self.sector.id

    def test_sector_uuid_post(self):
        post_data = copy.copy(self.post_data)
        post_data['sector_uuid'] = self.other_sector.id
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data['sector_uuid'] == self.other_sector.id

    def test_post_sector_should_not_write_sector_response(self):
        """Posting data in sector should not write anything for sector.
        sector is read-only and sector_uuid should be used for posting.
        """
        post_data = copy.copy(self.post_data)
        post_data['sector'] = self.other_sector.id
        response = self.user_client.post(self.endpoint, data=post_data, format='json')

        assert not 'sector' in response.data
        assert response.data['sector_uuid'] == self.sector.id

    def test_key_account_manager_post_as_user(self):
        post_data = copy.copy(self.post_data)
        post_data['key_account_manager_uuid'] = self.key_account_manager2.id
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.skip("No permissions for now")
    def test_key_account_manager_post_permissions(self):
        new_user = UserFactory(password="123", username="Additional")
        verification = EmailAddress.objects.get_or_create(user=new_user,
                                                          email=new_user.email)[0]
        verification.verified = True
        verification.set_as_primary()
        verification.save()
        credentials = {"email": new_user.email, "password": "123"}
        self.login_response = self.client.login(**credentials)
        post_data = copy.copy(self.post_data)
        post_data['key_account_manager_uuid'] = self.key_account_manager2.id
        hr = Group.objects.get(name="HR")
        new_user.groups.set([hr])
        response = self.client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 403)

        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 400)

        response = self.accounting_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 201)

        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_office_only_phone_response(self):
        phone = '+41555111141'
        post_data = copy.copy(self.post_data)
        post_data['offices'] = [{'landline_phone': phone}]
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.data['offices'][0]['landline_phone'] == phone

    def test_two_offices_response(self):
        headquarter_phone = '+41555111141'
        phone2 = '+41555111147'
        post_data = copy.copy(self.post_data)
        post_data['offices'] = [
            {'landline_phone': headquarter_phone, 'is_headquarter': True},
            {'landline_phone': phone2, 'is_headquarter': False, 'name': 'office-1'}
        ]
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        office_phone_headquarter = {office['landline_phone']: office['is_headquarter']
                                    for office
                                    in response.data['offices']}
        assert office_phone_headquarter[headquarter_phone]
        assert not office_phone_headquarter[phone2]

    def test_two_headquarters_response(self):
        # The headquarter might be only one.
        phone1 = '+41555111141'
        phone2 = '+41555111147'
        post_data = copy.copy(self.post_data)
        post_data['offices'] = [
            {'landline_phone': phone1, 'is_headquarter': True},
            {'landline_phone': phone2, 'is_headquarter': True}
        ]
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == 400

    def test_address_for_non_headquarters_response(self):
        # If you send address you should send is_headquarter as well
        phone1 = '+41555111141'
        address = AddressFactory()
        address_serialized = AddressSerializer(address).data
        post_data = copy.copy(self.post_data)
        post_data['offices'] = [
            {
                'landline_phone': phone1,
                'is_headquarter': False,
                'name': 'office',
                'address': address_serialized,
            },
        ]
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == 201

        post_data['offices'] = [
            {
                'landline_phone': phone1,
                'address': address_serialized,
            },
        ]
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == 400

    def test_office_email_response(self):
        phone = '+41555111141'
        email = 'test@example.com'
        post_data = copy.copy(self.post_data)
        post_data['offices'] = [{
            'landline_phone': phone,
            'email': email,
        }]
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.data['offices'][0]['email'] == email

    def test_office_address_response(self):
        phone = '+41555111141'
        city = 'Zurich'
        country = 'CH'
        post_data = copy.copy(self.post_data)
        post_data['offices'] = [{
            'landline_phone': phone,
            'address': {'city': city, 'postal_code': '1234', 'country': country},
            'is_headquarter': False,
        }]
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.data['offices'][0]['address']['city'] == city

    def test_read_only_of_company_type(self):
        post_data = copy.copy(self.post_data)
        post_data['company_type'] = COMPANY_TYPE.SERVICE_PROVIDER
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == 201
        assert response.data['company_type'] == COMPANY_TYPE.UNKNOWN

    def test_uuid_number_response(self):
        wrong_number = '12345'
        post_data = copy.copy(self.post_data)
        post_data['uid_number'] = wrong_number
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == 400

        right_number = '543276547'
        post_data['uid_number'] = right_number
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == 201
        assert response.data['uid_number'] == right_number

    def test_create_with_billing_address(self):
        post_data = copy.deepcopy(self.post_data)
        address = AddressFactory()
        address_serialized = AddressSerializer(address).data
        post_data['billing_address'] = address_serialized
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 201)
        company = Company.objects.order_by('-created_at')[0]
        address = Address.objects.order_by('-created_at')[0]
        self.assertEqual(company.billing_address.id, address.id)
        self.assertEqual(response.data['billing_address']['id'], str(address.id))

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        self.login_as_accounting_user()
        response = self.client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 201)
        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 201)
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])


class TestUpdateCompany(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.sector = SectorFactory()
        cls.key_account_manager1 = UserFactory()
        cls.key_account_manager2 = UserFactory()

        cls.company = CompanyFactory(
            sector=cls.sector,
            key_account_manager=cls.key_account_manager1,
            billing_agreement=BILLING_AGREEMENT.ACCORDING_TO_OFFER,
            billing_interval=BILLING_INTERVAL.PROJECT_BASED,
        )

        cls.new_name = 'Shiny Bling Inc.'
        cls.patch_data = {'name': cls.new_name}
        cls.endpoint = reverse('api:company-detail', kwargs={'id': cls.company.id})

    def test_updated_response_should_contain_nested_key_account_manager(self):
        response = self.user_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertIsNone(response.data.get('key_account_manager_uuid', None))
        self.assertIsNotNone(response.data.get('key_account_manager', None))

    def test_change_name_response(self):
        response = self.user_client.patch(self.endpoint, self.patch_data, format='json')
        assert response.data['name'] == self.new_name

    def test_keep_sector_response(self):
        response = self.user_client.patch(self.endpoint, self.patch_data, format='json')
        assert response.data['sector_uuid'] == self.sector.id

    def test_change_sector_response(self):
        new_sector = SectorFactory()
        patch_data = {'sector_uuid': new_sector.id}
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        assert response.data['sector_uuid'] == new_sector.id

    def test_keep_key_account_manager_response(self):
        response = self.user_client.patch(self.endpoint, self.patch_data, format='json')
        assert response.data['key_account_manager']['uuid'] == str(self.key_account_manager1.id)
        assert response.data['key_account_manager']['last_name'] == self.key_account_manager1.last_name
        assert response.data['key_account_manager']['first_name'] == self.key_account_manager1.first_name

    def test_change_key_account_manager_response(self):
        patch_data = {'key_account_manager_uuid': self.key_account_manager2.id}
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 400)

    @pytest.mark.skip(reason="No permissions for now")
    def test_change_key_account_manager_permission(self):
        """ Check that only admin, accounting, finance (no this role yet) can change key_account_manager
        """
        new_user = UserFactory(password="123", username="Additional")
        verification = EmailAddress.objects.get_or_create(user=new_user,
                                                          email=new_user.email)[0]
        verification.verified = True
        verification.set_as_primary()
        verification.save()
        credentials = {"email": new_user.email, "password": "123"}
        self.login_response = self.client.login(**credentials)
        patch_data = {'key_account_manager_uuid': self.key_account_manager2.id}
        hr = Group.objects.get(name="HR")
        new_user.groups.set([hr])
        response = self.client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 403)

        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 400)

        response = self.accounting_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        admin = Group.objects.get(name="Admin")
        new_user.groups.set([admin])
        response = self.client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_change_billing_agreement(self):
        new_billing_agreement = BILLING_AGREEMENT.ACCORDING_TO_EFFORT
        patch_data = {'billing_agreement': new_billing_agreement}
        response = self.user_client.patch(self.endpoint, data=patch_data)
        self.company.refresh_from_db()
        assert response.data['billing_agreement'] == new_billing_agreement
        assert self.company.billing_agreement == new_billing_agreement

    def test_change_billing_interval(self):
        new_billing_interval = BILLING_INTERVAL.TASK_BASED
        patch_data = {'billing_interval': new_billing_interval}
        response = self.user_client.patch(self.endpoint, data=patch_data)
        self.company.refresh_from_db()
        assert response.data['billing_interval'] == new_billing_interval
        assert self.company.billing_interval == new_billing_interval

    def test_add_billing_address(self):
        address = AddressFactory()
        patch_data = {'billing_address_uuid': address.id}
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.company.refresh_from_db()
        self.assertEqual(self.company.billing_address, address)
        self.assertEqual(response.data['billing_address']['uuid'], str(address.id))

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.client.patch(self.endpoint, self.patch_data, format='json')
        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        self.login_as_accounting_user()
        response = self.client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])


class TestDeleteCompany(MetronomBaseAPITestCase):

    def setUp(self):
        super().setUp()
        self.company = CompanyFactory()
        self.endpoint = reverse('api:company-detail', kwargs={'id': self.company.public_id})

    @pytest.mark.skip(reason="TODO: Companies should only be deleted by admins, but Offices can be deleted by users")
    def test_delete_company_should_be_allowed_only_for_admins(self):
        response = self.client.delete(self.endpoint)
        assert response.status_code == status.HTTP_403_FORBIDDEN

        self.login_as_admin()
        response = self.client.delete(self.endpoint)
        assert response.status_code == status.HTTP_204_NO_CONTENT

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        response = self.client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        self.login_as_accounting_user()
        response = self.client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])

    def test_delete_company_with_employments_permissions(self):
        person = PersonFactory()
        EmploymentFactory(person=person, company=self.company)
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_company_no_employments_permissions(self):
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class TestAddBankAccountForCompany(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company = CompanyFactory()
        cls.endpoint = reverse('api:company-bankaccounts', kwargs={'id': str(cls.company.id)})
        cls.bank_account_data = dict(
            bank_name='Bank-11',
            account_name='Added through company viewset',
            owner_name='Owner-0',
            bic='AAAAGE23000',
            iban='DE89370400440532013000',
        )

    def test_add_bank_account(self):
        response = self.user_client.post(self.endpoint, self.bank_account_data, format='json')
        self.assertEqual(response.status_code, 201)
        gotten_public_name = response.data['public_name']
        self.assertTrue('Added through company viewset (xxxx3000)' in gotten_public_name)


class TestOffices(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company = CompanyFactory()
        cls.base_endpoint = reverse(
            'api_company:company-offices-list', kwargs={'company_id': cls.company.id})
        cls.phone = '+79053332222'
        cls.email = 'test@example.com'
        cls.city = 'Zurich'
        cls.post_data = {'landline_phone': cls.phone, 'is_headquarter': False, 'name': 'office'}

    @pytest.mark.skip(reason="TODO: Not working - is GET really not allowed?")
    def test_get_method_is_not_allowed(self):
        response = self.client.get(self.base_endpoint)
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_no_offices_response(self):
        response = self.user_client.get(self.base_endpoint)
        assert response.data == []

    def test_one_office_response(self):
        office = OfficeFactory(company=self.company)
        response = self.user_client.get(self.base_endpoint)
        assert response.data[0]['name'] == office.name

    def test_two_companies_with_one_office_each_response_length(self):
        company1 = CompanyFactory()
        company2 = CompanyFactory()
        OfficeFactory(company=company1, is_headquarter=True)
        OfficeFactory(company=company2, is_headquarter=False, name='Office')
        company1_endpoint = reverse('api_company:company-offices-list',
                                    kwargs={'company_id': company1.id})
        response = self.user_client.get(company1_endpoint)
        assert len(response.data) == 1

    def test_add_office_response(self):
        response = self.user_client.post(self.base_endpoint, self.post_data)
        assert response.data['landline_phone'] == self.phone

    def test_add_office_with_email_response(self):
        post_data = copy.copy(self.post_data)
        post_data['email'] = self.email
        post_data['is_headquarter'] = True
        response = self.user_client.post(self.base_endpoint, data=post_data, format='json')
        assert response.data['email'] == self.email

    def test_add_office_with_address_response(self):
        post_data = copy.copy(self.post_data)
        post_data['address'] = {'city': self.city, 'postal_code': '1234', 'country': 'CH'}
        response = self.user_client.post(self.base_endpoint, data=post_data, format='json')
        assert response.data['address']['city'] == self.city

    def test_add_few_offices_with_address_response(self):
        post_data = copy.copy(self.post_data)
        post_data['is_headquarter'] = True
        post_data['address'] = {
            'city': self.city,
            'country': 'CH',
            'postal_code': '1234',
            'raw': '-',
            'address1': '-',
            'notes': '-',
        }
        response = self.user_client.post(self.base_endpoint, data=post_data, format='json')
        assert response.data['address']['city'] == self.city

        post_data['is_headquarter'] = False
        city = 'London'
        post_data['address'] = {'city': city, 'postal_code': '2234', 'country': 'CH',}
        response = self.user_client.post(self.base_endpoint, data=post_data, format='json')
        assert response.data['address']['city'] == city

        post_data['is_headquarter'] = False
        city = 'Moscow'
        post_data['address'] = {'city': city, 'postal_code': '3234', 'country': 'CH',}
        response = self.user_client.post(self.base_endpoint, data=post_data, format='json')
        assert response.data['address']['city'] == city

    def test_add_office_without_name_response(self):
        office = OfficeFactory(company=self.company, is_headquarter=True)
        assert office.is_headquarter == True

        post_data = copy.copy(self.post_data)
        post_data['is_headquarter'] = False
        post_data['name'] = ''
        response = self.user_client.post(self.base_endpoint, data=post_data, format='json')
        assert response.status_code == 400

        post_data['is_headquarter'] = False
        post_data['name'] = None
        response = self.user_client.post(self.base_endpoint, data=post_data, format='json')
        assert response.status_code == 400

    def test_add_two_offices_list_response(self):
        phone1 = '+41555111141'
        phone2 = '+41555111147'
        post_data1 = {'landline_phone': phone1, 'is_headquarter': True,
                      'name': 'Headquarter'}  # FIXME if is_headquarter is not set it gets False
        post_data2 = {'landline_phone': phone2, 'is_headquarter': False, 'name': 'office'}
        self.user_client.post(self.base_endpoint, post_data1)
        self.user_client.post(self.base_endpoint, post_data2)
        response = self.user_client.get(self.base_endpoint)
        office_phone_headquarter = {office['landline_phone']: office['is_headquarter'] for office in response.data}
        assert office_phone_headquarter[phone1]
        assert not office_phone_headquarter[phone2]

    def test_patch_office_response(self):
        office = OfficeFactory(company=self.company)
        details_endpoint = reverse(
            'api_company:company-offices-detail', kwargs={'company_id': self.company.id, 'pk': office.id})
        patch_data = {'landline_phone': self.phone}
        response = self.user_client.patch(details_endpoint, patch_data)
        assert response.data['landline_phone'] == self.phone

    def test_patch_office_add_email_response(self):
        office = OfficeFactory(company=self.company)
        details_endpoint = reverse(
            'api_company:company-offices-detail', kwargs={'company_id': self.company.id, 'pk': office.id})
        patch_data = {'email': self.email}
        response = self.user_client.patch(details_endpoint, patch_data, format='json')
        assert response.data['email'] == self.email

    def test_patch_office_add_address_response(self):
        office = OfficeFactory(company=self.company)
        details_endpoint = reverse(
            'api_company:company-offices-detail', kwargs={'company_id': self.company.id, 'pk': office.id})
        patch_data = {'address': {'city': self.city, 'postal_code': '1234'}}
        response = self.user_client.patch(details_endpoint, patch_data, format='json')
        assert response.data['address']['city'] == self.city

    def test_patch_office_keep_email_response(self):
        email = EmailFactory()
        office = OfficeFactory(company=self.company, email=email)
        details_endpoint = reverse(
            'api_company:company-offices-detail', kwargs={'company_id': self.company.id, 'pk': office.id})
        patch_data = {}
        response = self.user_client.patch(details_endpoint, patch_data, format='json')
        assert response.data['email'] == email.email
        assert Email.objects.filter(email=email.email).count() == 1

    def test_patch_office_keep_address_response(self):
        address = AddressFactory()
        office = OfficeFactory(company=self.company, address=address)
        details_endpoint = reverse(
            'api_company:company-offices-detail', kwargs={'company_id': self.company.id, 'pk': office.id})
        patch_data = {}
        response = self.user_client.patch(details_endpoint, patch_data, format='json')
        assert response.data['address']['city'] == address.city

    def test_patch_office_change_email(self):
        old_email = EmailFactory()
        old_email_str = old_email.email
        office = OfficeFactory(company=self.company, email=old_email)
        details_endpoint = reverse(
            'api_company:company-offices-detail', kwargs={'company_id': self.company.id, 'pk': office.id})

        new_email = 'new-email@test.test'
        patch_data = {'email': new_email}
        response = self.user_client.patch(details_endpoint, patch_data, format='json')
        office.email.refresh_from_db()

        assert response.data['email'] == new_email
        assert office.email.email == new_email
        assert not Email.objects.filter(email=old_email_str).exists()

    def test_patch_office_change_address(self):
        old_address = AddressFactory()
        office = OfficeFactory(company=self.company, address=old_address)
        details_endpoint = reverse(
            'api_company:company-offices-detail', kwargs={'company_id': self.company.id, 'pk': office.id})
        patch_data = {'address': {'city': self.city, 'postal_code': '1234'}}
        response = self.user_client.patch(details_endpoint, patch_data, format='json')
        office.address.refresh_from_db()
        assert response.data['address']['city'] == self.city
        assert response.data['address']['id'] == str(old_address.id)
        assert office.address.city == self.city

    def test_remove_office_status(self):
        office = OfficeFactory(company=self.company, is_headquarter=True)
        detail_endpoint = reverse(
            'api_company:company-offices-detail', kwargs={'company_id': self.company.id, 'pk': office.id})
        response = self.admin_client.delete(detail_endpoint)
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_remove_office_list_response(self):
        office = OfficeFactory(company=self.company)
        details_endpoint = reverse(
            'api_company:company-offices-detail',
            kwargs={'company_id': self.company.id, 'pk': office.id})
        self.admin_client.delete(details_endpoint)
        response = self.user_client.get(self.base_endpoint)
        assert response.data == []

    def test_delete_office_should_be_allowed_for_users(self):
        """ was formerly test_delete_office_should_be_allowed_only_for_admins """
        office = OfficeFactory(company=self.company)
        details_endpoint = reverse(
            'api_company:company-offices-detail',
            kwargs={'company_id': self.company.id, 'pk': office.id})
        response = self.admin_client.delete(details_endpoint)
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_patch_office_change_is_headquarter(self):
        office = OfficeFactory(company=self.company)
        details_endpoint = reverse(
            'api_company:company-offices-detail', kwargs={'company_id': self.company.id, 'pk': office.id})
        patch_data = {'is_headquarter': False}
        response = self.user_client.patch(details_endpoint, patch_data, format='json')
        assert response.status_code == 400


class TestCompaniesAutocomplete(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls._create_companies()
        cls.endpoint = reverse('autocompletes:companies')
        cls.cache = caches['autocompletes']
        cls.cache.clear()

    @classmethod
    def _create_companies(cls, count_companies=3):
        cls.companies = []
        for i in range(count_companies):
            cls.companies.append(CompanyFactory())

    def test_autocomplete(self):
        self._check_autocomplete()

    def _check_autocomplete(self):
        response = self.user_client.get(self.endpoint)
        data = json.loads(response.content)
        gotten_companies = data['companies:companies']

        names_from_db = [c.name for c in Company.objects.all()]
        gotten_names = list(gotten_companies.values())
        assert gotten_names == names_from_db

    def test_caching_autocomplete(self):
        self.cache.clear()

        self._check_autocomplete()

        company = Company.objects.get(pk=self.companies[0].pk)
        company.name = 'Changed'
        company.save()
        self._check_autocomplete()


class TestCompanyDefaults(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.default_payment_within = 14
        cls.added_charges = '105.55'
        cls.default_interpolation = '107.77'
        cls.default_working_hour_marge = '125.25'
        cls.key_account_manager1 = UserFactory()
        cls.key_account_manager2 = UserFactory()
        cls.key_account_manager = UserFactory()
        cls.company = CompanyFactory(
            default_payment_within=cls.default_payment_within,
            added_charges=cls.added_charges,
            default_interpolation=cls.default_interpolation,
            default_working_hour_marge=cls.default_working_hour_marge,
            key_account_manager=cls.key_account_manager,
            billing_interval=BILLING_INTERVAL.PROJECT_BASED,
        )
        cls.endpoint = reverse('api:company-defaults', kwargs={'id': cls.company.id})
        cls.response = cls.user_client.get(cls.endpoint)

    def test_get_should_return_ok_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_includes_correct_default_payment_within(self):
        assert self.response.data['default_payment_within'] == self.default_payment_within

    def test_response_includes_correct_added_charges(self):
        assert self.response.data['added_charges'] == self.added_charges

    def test_response_includes_correct_added_charges_dependency(self):
        assert self.response.data['added_charges_dependency'] == self.company.added_charges_dependency

    def test_response_includes_correct_default_interpolation(self):
        assert self.response.data['default_interpolation'] == self.default_interpolation

    def test_response_includes_correct_default_working_hour_marge(self):
        assert self.response.data['default_working_hour_marge'] == self.default_working_hour_marge

    def test_response_includes_correct_billing_agreement(self):
        assert self.response.data['billing_agreement'] == self.company.billing_agreement

    def test_response_includes_correct_billing_interval(self):
        assert self.response.data['billing_interval'] == self.company.billing_interval

    def test_response_includes_correct_billing_currency(self):
        assert self.response.data['billing_currency'] == self.company.billing_currency

    def test_response_includes_correct_key_account_manager_uuid(self):
        assert self.response.data['key_account_manager_uuid'] == self.key_account_manager.id

    def test_response_should_contain_contacts_persons(self):
        self.assertTrue('contacts_persons' in self.response.data)
        person = PersonFactory()
        EmploymentFactory(
            person=person,
            company=self.company,
            email=EmailFactory(),
        )
        response = self.user_client.get(self.endpoint)
        uuids = [x['uuid'] for x in response.data['contacts_persons']]
        self.assertTrue(str(person.pk) in uuids)
        names = [x['name'] for x in response.data['contacts_persons']]
        self.assertTrue(person.name in names)
        public_ids = [x['public_id'] for x in response.data['contacts_persons']]
        self.assertTrue(person.public_id in public_ids)


class TestSectorApi(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.sector = SectorFactory()
        cls.list_endpoint = reverse('api:sector-list')
        cls.detail_endpoint = reverse('api:sector-detail', args=[cls.sector.id])

    def test_list_sector_categories(self):
        response = self.admin_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]['name'], self.sector.name)

    def test_create_sector_categories(self):
        before = Sector.objects.count()
        post_data = {
            'name': "New name"
        }
        response = self.admin_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(before + 1, Sector.objects.count())

    def test_retrieve(self):
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], self.sector.name)
        self.assertEqual(response.data['description'], self.sector.description)

    def test_patch(self):
        patch_data = {
            'name': "New name"
        }
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], patch_data['name'])

    def test_delete(self):
        sector_category2 = SectorFactory()
        detail_endpoint = reverse('api:sector-detail', args=[sector_category2.id])
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_sector_list(self):
        """ Check that only groups that have access to list endpoint are Admin, User, Backoffice
        """
        response = self.user_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)

    def test_permissions_sector_retrieve(self):
        """ Check that only groups that have access to detail endpoint are Admin, User, Backoffice
        """
        response = self.user_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)

    def test_permissions_sector_create(self):
        """ Check that only groups that have access to create endpoint are Admin and Backoffice
        """
        post_data = {
            'name': "New name"
        }
        response = self.user_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        post_data = {
            'name': "New name2"
        }
        response = self.admin_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)

    def test_permissions_sector_patch_not_used(self):
        patch_data = {
            'name': "New name"
        }
        response = self.user_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        patch_data = {
            'name': "New name2"
        }
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_sector_patch_used(self):
        CompanyFactory(sector=self.sector)
        patch_data = {
            'name': "New name"
        }
        response = self.backoffice_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        patch_data = {
            'name': "New name22"
        }
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_sector_delete_not_used(self):
        sector_category2 = SectorFactory()
        detail_endpoint = reverse('api:sector-detail', args=[sector_category2.id])
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)
        sector_category2 = SectorFactory()
        detail_endpoint = reverse('api:sector-detail', args=[sector_category2.id])
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_sector_delete_used(self):
        sector_category2 = SectorFactory()
        CompanyFactory(sector=sector_category2)
        detail_endpoint = reverse('api:sector-detail', args=[sector_category2.id])
        response = self.backoffice_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {"__all__": ["You can't delete used Sector"]}
        )


class TestCampaignDefaults(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.campaign1 = CampaignFactory(
            billing_agreement=BILLING_AGREEMENT.ACCORDING_TO_OFFER,
            billing_interval=BILLING_INTERVAL.PROJECT_BASED,
        )
        cls.campaign1.save()
        cls.campaign2 = CampaignFactory(company=cls.campaign1.company)
        cls.endpoint = reverse('api:company-defaults', kwargs={'id': cls.campaign1.company.id})
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_response_includes_two_campaigns(self):
        assert len(self.response.data['campaigns']) == 2

    def test_response_includes_correct_title(self):
        assert self.response.data['campaigns'][str(self.campaign1.id)]['title'] == self.campaign1.title

    def test_response_includes_correct_billing_agreement(self):
        assert self.response.data['campaigns'][str(self.campaign1.id)]['billing_agreement'] == \
            self.campaign1.billing_agreement

    def test_response_includes_correct_billing_interval(self):
        assert self.response.data['campaigns'][str(self.campaign1.id)]['billing_interval'] == \
            self.campaign1.billing_interval

    @pytest.mark.skip(reason="No permission not for now")
    def test_permissions_campaign_default(self):
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
