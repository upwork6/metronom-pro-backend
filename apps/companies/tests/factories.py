import random

import factory
from django.contrib.auth import get_user_model
from faker import Faker

from apps.accounting.models import Allocation
from apps.addresses.tests.factories import AddressFactory
from apps.bankaccounts.tests.factories import BankAccountFactory
from apps.companies.models import Company, Office, Sector
from apps.emails.tests.factories import EmailFactory
from apps.metronom_commons.data import (ADDED_CHARGES_DEPENDENCY,
                                        BILLING_AGREEMENT, BILLING_DELIVERY,
                                        BILLING_INTERVAL, CURRENCY)

User = get_user_model()

fake = Faker('de_DE')


def get_random_int_list(count_elements=10, count_digits=2, with_none=True):
    int_list = [int(10**count_digits * random.random()) for i in range(count_elements)]
    if with_none:
        int_list.insert(0, None)
    return int_list


class SectorFactory(factory.DjangoModelFactory):

    name = factory.Sequence(lambda n: 'sector-{0}'.format(n))
    description = factory.Sequence(lambda n: 'description-{0}'.format(n))

    class Meta:
        model = Sector


class OfficeFactory(factory.DjangoModelFactory):

    name = factory.Sequence(lambda n: 'office-{0}'.format(n))
    address = factory.SubFactory(AddressFactory)

    class Meta:
        model = Office


class OfficeNiceFactory(factory.DjangoModelFactory):

    name = factory.Sequence(lambda n: 'Fabrik {0}'.format(n))
    company = factory.Iterator(Company.objects.all())
    address = factory.SubFactory(AddressFactory)
    email = factory.SubFactory(EmailFactory)
    is_headquarter = factory.Iterator([True, False])
    # Can't use the factory.Faker('phone_number', locale="de_DE") because for some reasons (didn't check them though)
    # it has weird behaviour with the PhoneNumberField. To be more accurate it sometimes generate empty instances of
    # the PhoneNumberField and assign them to the Person model instance. And when this happen the string representation
    # of the PhoneNumberField becomes really weird - "+NoneNone"
    landline_phone = factory.Sequence(lambda n: "+494854073{}{}".format(n, random.choice(range(10, 99))))

    class Meta:
        model = Office


# TODO Change usage of CompanyFactory and OfficeFactory so that only this can be used
class CompanyWithOfficeFactory(factory.DjangoModelFactory):

    name = factory.Sequence(lambda n: 'company-{0}'.format(n))
    offices = factory.RelatedFactory(OfficeFactory, factory_related_name='company')

    class Meta:
        model = Company

    @factory.post_generation
    def bank_accounts(self, create, extracted, **kwargs):
        if create:
            for i in range(3):
                account = BankAccountFactory()
                self.bank_accounts.add(account)


class CompanyNiceFactory(factory.DjangoModelFactory):

    name = factory.Faker('company', locale="de_DE")
    # name = factory.Iterator(["Nestlé", "Novartis", "Migros", "Kühne + Nagel", "Coop", "Swisscom", "Liebherr",
    #                          "Emmi", "Tamoil", "Cartier", "Omega", "Lindt & Sprüngli", "Sulzer", "Rehau",
    #                          "AXA", "Volkswagen", "Apple", "Hewlett-Packard", "IBM Schweiz", "Daimler AG",
    #                          "Deutsche Telekom", "BMW AG", "E.ON SE", "Siemens AG", "Lidl Stiftung & Co KG",
    #                          "Edeka", "Rewe Group"])
    sector = factory.Iterator(Sector.objects.all())
    default_payment_within = factory.Iterator([7, 10, 14, 20, 21, 30, 60])
    added_charges = factory.Iterator(get_random_int_list())
    added_charges_dependency = factory.Iterator([x[0] for x in ADDED_CHARGES_DEPENDENCY.CHOICES])
    default_interpolation = factory.Iterator(get_random_int_list())
    default_working_hour_marge = factory.Iterator(get_random_int_list())
    billing_delivery = factory.Iterator([x[0] for x in BILLING_DELIVERY.CHOICES])
    billing_agreement = factory.Iterator([x[0] for x in BILLING_AGREEMENT.CHOICES])
    billing_interval = factory.Iterator([x[0] for x in BILLING_INTERVAL.CHOICES])
    billing_currency = factory.Iterator([x[0] for x in CURRENCY.CHOICES])
    key_account_manager = factory.Iterator(User.objects.all())
    uid_number = factory.Faker('ssn', locale="de_DE")

    class Meta:
        model = Company

    @factory.post_generation
    def bank_accounts(self, create, extracted, **kwargs):
        if create:
            bank_account1 = BankAccountFactory(account_name="Firmenkonto")
            bank_account2 = BankAccountFactory(account_name="Zweitkonto")
            self.bank_accounts.add(bank_account1)
            self.bank_accounts.add(bank_account2)

    @factory.post_generation
    def offices(self, create, extracted, **kwargs):
        if create:
            headquarter = OfficeNiceFactory(is_headquarter=True)
            other_office = OfficeNiceFactory(is_headquarter=False, name="Zweitsitz")
            self.offices.add(headquarter)
            self.offices.add(other_office)

    @factory.post_generation
    def allocations(self, create, extracted, **kwargs):
        if create:
            for i in range(0, random.randint(1, 3)):
                allocation = Allocation.objects.all().order_by('?').first()
                if allocation:
                    self.allocations.add(allocation)


class CompanyFactory(factory.DjangoModelFactory):

    name = factory.Sequence(lambda n: 'company-{0}'.format(n))

    default_payment_within = 3  # int
    added_charges = 7  # decimal
    default_interpolation = 8  # decimal
    default_working_hour_marge = 9  # decimal

    class Meta:
        model = Company

    @factory.post_generation
    def bank_accounts(self, create, extracted, **kwargs):
        if create:
            for i in range(3):
                account = BankAccountFactory()
                self.bank_accounts.add(account)
