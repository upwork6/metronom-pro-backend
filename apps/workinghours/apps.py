from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class WorkingHoursConfig(AppConfig):
    name = 'apps.workinghours'
    verbose_name = _('WorkingHours')
