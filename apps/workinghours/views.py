import calendar
from datetime import datetime, timedelta, date

from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from dry_rest_permissions.generics import DRYPermissions
from rest_framework import status
from rest_framework.generics import ListAPIView, get_object_or_404
from rest_framework.mixins import UpdateModelMixin, ListModelMixin
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
from rest_framework.viewsets import ModelViewSet, GenericViewSet, ReadOnlyModelViewSet

from apps.api.filters import MetronomPermissionsFilter
from apps.api.mixins import LoggerMixin
from apps.contracts.models import Contract
from apps.metronom_commons.data import ACCOUNTING_TYPES
from apps.metronom_commons.mixins.views import DeleteForbiddenIfUsedMixin
from apps.projects.models import WorkLog
from apps.users.mixins import MeURLMixin
from apps.workinghours.models import Absence, FlexWork, Holiday, WorkingHours, ClosingHours, WorkYear
from apps.workinghours.serializers import (AbsenceSerializer, FlexWorkSerializer, HolidaySerializer,
                                           WorkingHoursSerializer, ClosingHoursSerializer,
                                           WorkYearSerializer)
from apps.workinghours.utils import get_absence_hours, convert_hours_to_time_string


class UserAbsenceViewSet(LoggerMixin, MeURLMixin, ModelViewSet):
    lookup_url_kwarg = 'pk'
    permission_classes = (DRYPermissions,)
    serializer_class = AbsenceSerializer

    def perform_create(self, serializer):
        return serializer.save(user=self.get_user())

    def perform_update(self, serializer):
        return serializer.save(user=self.get_user())

    def get_queryset(self):
        return self.get_user().absences.all()

    def get_object(self):
        """
        According to DRF docs, override this method if there are more than multiple
        'kwargs' in the url conf. Since this is a nested view with both 'user_pk' and
        'pk' keys in url conf. So, we are overriding this method for correct permissions
        to apply
        """
        obj = get_object_or_404(self.get_queryset(), pk=self.kwargs.get('pk'))
        self.check_object_permissions(self.request, obj)
        return obj

    def list(self, request, *args, **kwargs):
        vacations_data = WorkYearSerializer(self.get_user().work_years.order_by('year'), many=True).data

        vacations = dict()
        for vacation in vacations_data:
            vacations[vacation['year']] = vacation

        data = dict(
            vacations=vacations,
            absences=self.get_serializer(self.get_queryset(), many=True).data
        )
        return Response(data, status=status.HTTP_200_OK)


class FlexWorkViewSet(LoggerMixin, ModelViewSet):
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    queryset = FlexWork.objects.all().order_by('given_name')
    serializer_class = FlexWorkSerializer
    http_method_names = ['get', 'post', 'patch', 'head', 'options', 'trace']


class UserWorkingHoursBaseViewSet(LoggerMixin, MeURLMixin, ListModelMixin, GenericViewSet):
    date_format = '%Y-%m-%d'
    # Cannot use date.min due to this big -> https://bugs.python.org/issue1777412
    MINDATE = date(1900, 1, 1)

    def get_all_user_contracts(self):
        return Contract.objects.filter(user=self.get_user())

    def get_contract_active_on_current_date(self, current_date):
        contracts = self.get_all_user_contracts()
        for contract in contracts:
            contract_end_date = contract.end_date if contract.end_date else date.max
            if contract.start_date <= current_date <= contract_end_date:
                return contract

        return Contract.objects.none()

    def get_queryset(self):
        contracts = self.get_all_user_contracts()
        if contracts:
            return WorkingHours.objects.filter(flex_work__contract__in=contracts)

        raise ValidationError("You do not have any valid contract for that period of time")

    def get_queryset_for_current_date(self, current_date):
        contract = self.get_contract_active_on_current_date(current_date=current_date)
        return self.get_queryset().filter(flex_work__contract=contract, weekday=current_date.weekday() + 1)

    def list(self, request, *args, **kwargs):
        try:
            start_date_qp = request.query_params.get('start_date', None)
            start_date = datetime.strptime(start_date_qp, self.date_format).date() if start_date_qp else None

            end_date_qp = request.query_params.get('end_date', None)
            end_date = datetime.strptime(end_date_qp, self.date_format).date() if end_date_qp else None
        except ValueError as e:
            raise ValidationError(e.args[0])

        if end_date and start_date:
            if end_date < start_date:
                raise ValidationError(_('The "end_date" cannot be earlier than the "start_date".'))
        elif start_date:
            end_date = date.max
        elif end_date:
            start_date = self.MINDATE
        else:
            start_date = date.max
            end_date = self.MINDATE

        data = self.get_response_for_date_range(start_date=start_date, end_date=end_date)

        return Response(data, status=status.HTTP_200_OK)

    def get_response_for_date_range(self, start_date, end_date):
        raise NotImplementedError(_('Abstract method not implemented.'))

    class Meta(object):
        abstract = True


class UserWorkingHoursViewSet(UserWorkingHoursBaseViewSet):
    serializer_class = WorkingHoursSerializer

    def get_response_for_date_range(self, start_date, end_date):
        working_hours_list = list()
        for i in range((end_date - start_date).days + 1):
            current_date = start_date + timedelta(days=i)
            queryset = self.get_queryset_for_current_date(current_date=current_date)

            working_hours = [dict(t) for t in set([
                tuple(d.items()) for d in
                self.get_serializer(queryset, many=True).data
            ])]
            closing_hours = ClosingHoursSerializer(
                ClosingHours.objects.filter(holiday__date=current_date), many=True
            ).data

            for closing_hour in closing_hours:
                if closing_hour in working_hours:
                    working_hours.remove(closing_hour)

            hours_dict = dict(
                date=current_date.strftime(self.date_format),
                hours=working_hours
            )

            if closing_hours:
                hours_dict['agency_closing_hours'] = closing_hours

            working_hours_list.append(hours_dict)

        flex_model = FlexWork.objects.filter(
            contract=self.get_contract_active_on_current_date(timezone.now().date())
        ).first()

        data = dict(
            flex_model=FlexWorkSerializer(instance=flex_model).data,
            working_hours=working_hours_list,
            absences=AbsenceSerializer(instance=self.get_user().absences.all(), many=True).data
        )
        return data


class HolidayViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    queryset = Holiday.objects.all().order_by('-created_at')
    serializer_class = HolidaySerializer
    forbid_delete_if_used_in = {
        ClosingHours: ['holiday']
    }


class TenantAbsenceView(LoggerMixin, ListAPIView):
    """
    Returns Absences for the current Tenant.
    Since we only have one tenant so return all absences for now
    """
    serializer_class = AbsenceSerializer
    queryset = Absence.objects.all()


class UserWorkingStatisticsViewSet(UserWorkingHoursBaseViewSet):
    serializer_class = WorkingHoursSerializer

    def get_response_for_date_range(self, start_date, end_date):
        working_statistics = list()
        user_work_logs = WorkLog.objects.filter(worker=self.get_user())
        for i in range((end_date - start_date).days + 1):
            current_date = start_date + timedelta(days=i)
            queryset = self.get_queryset_for_current_date(current_date=current_date)
            target_hours = sum([working_hours.hours for working_hours in queryset.all()])

            invest_logs = user_work_logs.filter(
                work_date=current_date,
                project_activity_step__activity__project__accounting_type=ACCOUNTING_TYPES.INVEST
            )
            profit_logs = user_work_logs.filter(
                work_date=current_date,
                project_activity_step__activity__project__accounting_type=ACCOUNTING_TYPES.PROFIT
            )
            intern_logs = user_work_logs.filter(
                work_date=current_date,
                project_activity_step__activity__project__accounting_type=ACCOUNTING_TYPES.INTERNAL
            )

            total_logs = user_work_logs.filter(work_date=current_date)

            invest_hours = float(sum([invest_logs.tracked_time for invest_logs in invest_logs.all()]))
            profit_hours = float(sum([profit_logs.tracked_time for profit_logs in profit_logs.all()]))
            intern_hours = float(sum([intern_logs.tracked_time for intern_logs in intern_logs.all()]))

            absence_hours = get_absence_hours(current_date=current_date)
            target_hours_with_absences = target_hours - absence_hours

            logged_hours = float(sum([total_logs.tracked_time for total_logs in total_logs.all()]))
            overtime_hours = logged_hours - target_hours_with_absences

            working_statistics.append(dict(
                date=current_date.strftime(self.date_format),
                invest_time=convert_hours_to_time_string(invest_hours),
                profit_time=convert_hours_to_time_string(profit_hours),
                intern_time=convert_hours_to_time_string(intern_hours),
                target_time=convert_hours_to_time_string(target_hours),
                target_time_with_absences=convert_hours_to_time_string(target_hours_with_absences),
                logged_working_time=convert_hours_to_time_string(logged_hours),
                overtime=convert_hours_to_time_string(overtime_hours)
            ))

        return working_statistics


class UserWorkYearViewSet(LoggerMixin, MeURLMixin, ReadOnlyModelViewSet, UpdateModelMixin):
    permission_classes = (DRYPermissions,)
    lookup_url_kwarg = 'pk'
    serializer_class = WorkYearSerializer

    def perform_update(self, serializer):
        return serializer.save(user=self.get_user())

    def get_queryset(self):
        return self.get_user().work_years


class UserStatisticsViewSet(UserWorkingHoursBaseViewSet):
    lookup_url_kwarg = 'pk'
    serializer_class = WorkingHoursSerializer

    def list(self, request, *args, **kwargs):
        work_year = self.get_user().work_years.order_by('year').last()
        if not work_year:
            work_year = WorkYear.objects.create(
                user=self.get_user(), year=timezone.now().year, vacation_days_moved_from_last_year=0
            )
        vacation = WorkYearSerializer(work_year).data

        data = dict(
            vacations={vacation['year']: vacation},
            working_times=dict(
                today=self.get_stats_for_today(),
                week=self.get_stats_for_week()
            )
        )
        return Response(data=data, status=status.HTTP_200_OK)

    def get_stats_for_today(self):
        today = timezone.now()
        return self.get_response_for_date_range(start_date=today.date(), end_date=today.date())

    def get_stats_for_week(self):
        start_date = timezone.now().date() + timedelta(days=-timezone.now().weekday())
        end_date = timezone.now().date() + timedelta(days=6 - timezone.now().weekday())
        return self.get_response_for_date_range(start_date=start_date, end_date=end_date)

    # TODO: The following method needs to be refactored it is somewhat repeating method a line 200 above
    def get_response_for_date_range(self, start_date, end_date):
        user_work_logs = WorkLog.objects.filter(worker=self.get_user())

        invest_logs = user_work_logs.filter(
            work_date__range=(start_date, end_date),
            project_activity_step__activity__project__accounting_type=ACCOUNTING_TYPES.INVEST
        )
        profit_logs = user_work_logs.filter(
            work_date__range=(start_date, end_date),
            project_activity_step__activity__project__accounting_type=ACCOUNTING_TYPES.PROFIT
        )
        intern_logs = user_work_logs.filter(
            work_date__range=(start_date, end_date),
            project_activity_step__activity__project__accounting_type=ACCOUNTING_TYPES.INTERNAL
        )

        total_logs = user_work_logs.filter(work_date__range=(start_date, end_date))

        invest_hours = float(sum([invest_logs.tracked_time for invest_logs in invest_logs.all()]))
        profit_hours = float(sum([profit_logs.tracked_time for profit_logs in profit_logs.all()]))
        intern_hours = float(sum([intern_logs.tracked_time for intern_logs in intern_logs.all()]))
        logged_hours = float(sum([total_logs.tracked_time for total_logs in total_logs.all()]))

        total_target_hours = total_target_hours_with_absences = total_overtime_hours = 0

        for i in range((end_date - start_date).days + 1):
            current_date = start_date + timedelta(days=i)
            queryset = self.get_queryset_for_current_date(current_date=current_date)
            target_hours = sum([working_hours.hours for working_hours in queryset.all()])

            absence_hours = get_absence_hours(current_date=current_date)
            target_hours_with_absences = target_hours - absence_hours

            total_target_hours += target_hours
            total_target_hours_with_absences += target_hours_with_absences

        total_overtime_hours += logged_hours - total_target_hours_with_absences

        return dict(
            invest_time=convert_hours_to_time_string(invest_hours),
            profit_time=convert_hours_to_time_string(profit_hours),
            intern_time=convert_hours_to_time_string(intern_hours),
            target_time=convert_hours_to_time_string(total_target_hours),
            target_time_with_absences=convert_hours_to_time_string(total_target_hours_with_absences),
            logged_working_time=convert_hours_to_time_string(logged_hours),
            overtime=convert_hours_to_time_string(total_overtime_hours)
        )
