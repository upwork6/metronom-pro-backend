from django.contrib import admin

from apps.workinghours.models import Absence, WorkingHours, ClosingHours, FlexWork, Holiday, WorkYear


@admin.register(Absence)
class AbsenceAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'reason',
        'start_date',
        'end_date',
        'absence_days',
        'is_all_day',
        'approval_status'
    )
    list_editable = (
        'approval_status',
    )
    list_filter = (
        'start_date',
        'end_date',
        'is_all_day',
    )


class WorkingHoursInline(admin.TabularInline):
    model = WorkingHours
    extra = 0


class ClosingHoursInline(admin.TabularInline):
    model = ClosingHours
    extra = 0


@admin.register(FlexWork)
class FlexWorkAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'given_name',
        'name',
        'description',
        'is_archived',
    )
    list_filter = ('given_name', 'is_archived')
    inlines = (WorkingHoursInline,)


@admin.register(Holiday)
class HolidayAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'date',
        'is_all_day',
    )
    list_filter = ('date', 'is_all_day')
    search_fields = ('name',)
    inlines = (ClosingHoursInline,)


@admin.register(ClosingHours)
class ClosingHoursAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'holiday',
        'from_hour',
        'to_hour',
    )
    list_filter = ('holiday',)


@admin.register(WorkYear)
class WorkYearAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'updated_at',
        'user',
        'year',
        'vacation_days_moved_from_last_year',
    )
    list_filter = (
        'user',
        'year',
        'created_at',
        'updated_at'
    )
