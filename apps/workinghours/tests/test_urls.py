from django.test import TestCase
from django.urls import resolve, reverse


class TenantAbsenceURLs(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def test_tenant_absence_list_reverse(self):
        assert reverse('api_agency:absences') == f'/api/agency/absences/'

    def test_tenant_absence_list_resolve(self):
        assert resolve(f'/api/agency/absences/').view_name == 'api_agency:absences'
