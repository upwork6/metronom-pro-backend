from apps.contracts.tests.factories import ContractFactory
from apps.workinghours.tests.factories import WorkingHoursFactory, FlexWorkFactory


class TestFlexWorkMixin(object):

    @staticmethod
    def get_full_day_full_week_flex_work():
        flex_work = FlexWorkFactory(given_name='Five full days work week')

        WorkingHoursFactory(weekday=1, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=1, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=2, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=2, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=4, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=4, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=5, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=5, from_hour='13:00', to_hour='17:00', flex_work=flex_work)

        return flex_work

    @staticmethod
    def get_half_day_full_week_flex_work():
        flex_work = FlexWorkFactory(given_name='Full half days work week')

        WorkingHoursFactory(weekday=1, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=2, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=4, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=5, from_hour='08:00', to_hour='12:00', flex_work=flex_work)

        return flex_work

    @staticmethod
    def get_full_day_mon_wed_fri_flex_work():
        flex_work = FlexWorkFactory(given_name='Three full days work week')

        WorkingHoursFactory(weekday=1, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=1, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=5, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=5, from_hour='13:00', to_hour='17:00', flex_work=flex_work)

        return flex_work

    @staticmethod
    def get_full_day_mon_tue_wed_thu_flex_work():
        flex_work = FlexWorkFactory(given_name='Four full days work week')

        WorkingHoursFactory(weekday=1, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=1, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=2, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=2, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=4, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=4, from_hour='13:00', to_hour='17:00', flex_work=flex_work)

        return flex_work

    @staticmethod
    def get_full_day_wed_thu_half_day_fri_flex_work():
        flex_work = FlexWorkFactory(given_name='Two full days, one half day work week')

        WorkingHoursFactory(weekday=3, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=4, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=4, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=5, from_hour='08:00', to_hour='12:00', flex_work=flex_work)

        return flex_work

    @staticmethod
    def get_full_day_mon_tue_wed_thu_half_day_fri_flex_work():
        flex_work = FlexWorkFactory(given_name='Four full days, one half day work week')

        WorkingHoursFactory(weekday=1, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=1, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=2, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=2, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=4, from_hour='08:00', to_hour='12:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=4, from_hour='13:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=5, from_hour='08:00', to_hour='12:00', flex_work=flex_work)

        return flex_work

    @staticmethod
    def get_mon_wed_10_13__14_17_flex_work():
        flex_work = FlexWorkFactory(given_name='Mo, We 10-13 + 14-17')

        WorkingHoursFactory(weekday=1, from_hour='10:00', to_hour='13:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=1, from_hour='14:00', to_hour='17:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='10:00', to_hour='13:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='14:00', to_hour='17:00', flex_work=flex_work)

        return flex_work

    @staticmethod
    def get_mon_10_14_wed_to_fri_10_14__15_18():
        flex_work = FlexWorkFactory(given_name='Mo 10-14, We, Fr 10-14 + 15-18')

        WorkingHoursFactory(weekday=1, from_hour='10:00', to_hour='14:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='10:00', to_hour='14:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=3, from_hour='15:00', to_hour='18:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=5, from_hour='10:00', to_hour='14:00', flex_work=flex_work)
        WorkingHoursFactory(weekday=5, from_hour='15:00', to_hour='18:00', flex_work=flex_work)

        return flex_work
