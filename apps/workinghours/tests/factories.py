from datetime import time, timedelta

import factory
from django.utils import timezone

from apps.metronom_commons.data import WEEKDAYS, ABSENCE_REASONS, ABSENCE_TYPE, APPROVAL_STATUS
from apps.users.tests.factories import UserFactory
from apps.workinghours.models import FlexWork, WorkingHours, Holiday, ClosingHours, Absence, WorkYear


class AbsenceFactory(factory.DjangoModelFactory):
    reason = factory.Iterator([k for (k, v) in ABSENCE_REASONS.CHOICES])
    start_date = timezone.now() + timedelta(days=-timezone.now().weekday(), weeks=-1)  # Monday the week before
    end_date = timezone.now() + timedelta(days=4 - timezone.now().weekday(), weeks=1)  # Friday the week after
    start_date_number = ABSENCE_TYPE.FULL_DAY
    end_date_number = ABSENCE_TYPE.FULL_DAY
    is_all_day = False
    note = factory.Sequence(lambda n: 'Note {}'.format(n))
    user = factory.SubFactory(UserFactory)
    approval_status = APPROVAL_STATUS.REQUESTED

    class Meta:
        model = Absence


class FlexWorkFactory(factory.DjangoModelFactory):
    given_name = factory.Iterator(['Full time', 'Half time - Mornings'])
    is_archived = False

    class Meta:
        model = FlexWork
        django_get_or_create = ('given_name', 'is_archived')


class WorkingHoursFactory(factory.DjangoModelFactory):
    flex_work = factory.SubFactory(FlexWorkFactory)
    weekday = factory.Iterator([x for (x, y) in WEEKDAYS.CHOICES])
    from_hour = time()
    to_hour = time()

    class Meta:
        model = WorkingHours


class HolidayFactory(factory.DjangoModelFactory):
    name = factory.Faker('name')
    date = factory.Sequence(lambda n: timezone.now().date() + timedelta(days=n))
    is_all_day = factory.Iterator([True, False])

    class Meta:
        model = Holiday
        django_get_or_create = ('name', 'date', 'is_all_day')


class ClosingHoursFactory(factory.DjangoModelFactory):
    holiday = factory.SubFactory(HolidayFactory)
    from_hour = time(0, 0)
    to_hour = time(0, 0)

    class Meta:
        model = ClosingHours


class WorkYearFactory(factory.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    year = factory.Iterator([2016, 2017, 2018, 2019, 2020])
    vacation_days_moved_from_last_year = factory.Iterator([2, 4, 6, 8, 10])

    class Meta:
        model = WorkYear
