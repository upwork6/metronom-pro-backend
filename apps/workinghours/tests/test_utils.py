from datetime import timedelta

from django.test import TestCase
from django.utils import timezone
from datetime import datetime
from apps.contracts.tests.factories import ContractFactory
from apps.metronom_commons.data import ABSENCE_TYPE, APPROVAL_STATUS
from apps.users.tests.factories import UserFactory
from apps.workinghours.models import WorkingHours
from apps.workinghours.tests.factories import HolidayFactory, ClosingHoursFactory, AbsenceFactory
from apps.workinghours.tests.mixins import TestFlexWorkMixin
from apps.workinghours.utils import get_absence_hours


class TestTargetTimeWithAbsences(TestCase, TestFlexWorkMixin):
    @classmethod
    def setUpTestData(cls):
        cls.date_format = '%Y-%m-%d'
        cls.wednesday_date = timezone.now().date() + timedelta(days=2 - timezone.now().weekday())
        cls.user = UserFactory()
        ContractFactory(
            start_date=cls.wednesday_date + timedelta(days=-1),
            end_date=cls.wednesday_date + timedelta(days=1),
            user=cls.user,
            flex_model=cls.get_full_day_full_week_flex_work()
        )
        cls.working_hours = WorkingHours.objects.filter(weekday=cls.wednesday_date.weekday())

        cls.target_hours = sum([working_hours.hours for working_hours in cls.working_hours.all()])

        assert cls.target_hours == 8

    def test_with_partial_holiday(self):
        holiday = HolidayFactory(date=self.wednesday_date, is_all_day=False)
        ClosingHoursFactory(from_hour='08:00', to_hour='12:00', holiday=holiday)

        absence_hours = get_absence_hours(current_date=self.wednesday_date)
        target_hours_with_absences = self.target_hours - absence_hours

        assert target_hours_with_absences == 4

    def test_with_full_holiday(self):
        HolidayFactory(date=self.wednesday_date, is_all_day=True)

        absence_hours = get_absence_hours(current_date=self.wednesday_date)
        target_hours_with_absences = self.target_hours - absence_hours

        assert target_hours_with_absences == 0

    def test_with_absence(self):
        AbsenceFactory(user=self.user, approval_status=APPROVAL_STATUS.APPROVED)

        absence_hours = get_absence_hours(current_date=self.wednesday_date)
        target_hours_with_absences = self.target_hours - absence_hours

        assert target_hours_with_absences == 0

    def test_with_absence_start_full_day(self):
        AbsenceFactory(
            user=self.user, approval_status=APPROVAL_STATUS.APPROVED, start_date=self.wednesday_date,
            start_date_number=ABSENCE_TYPE.FULL_DAY
        )

        absence_hours = get_absence_hours(current_date=self.wednesday_date)
        target_hours_with_absences = self.target_hours - absence_hours

        assert target_hours_with_absences == 0

    def test_with_absence_start_half_day(self):
        AbsenceFactory(
            user=self.user, approval_status=APPROVAL_STATUS.APPROVED, start_date=self.wednesday_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY
        )
        absence_hours = get_absence_hours(current_date=self.wednesday_date)
        target_hours_with_absences = self.target_hours - absence_hours

        assert target_hours_with_absences == 4

    def test_with_absence_end_full_day(self):
        AbsenceFactory(
            user=self.user, approval_status=APPROVAL_STATUS.APPROVED, end_date=self.wednesday_date,
            end_date_number=ABSENCE_TYPE.FULL_DAY
        )

        absence_hours = get_absence_hours(current_date=self.wednesday_date)
        target_hours_with_absences = self.target_hours - absence_hours

        assert target_hours_with_absences == 0

    def test_with_absence_end_half_day(self):
        AbsenceFactory(
            user=self.user, approval_status=APPROVAL_STATUS.APPROVED, end_date=self.wednesday_date,
            end_date_number=ABSENCE_TYPE.HALF_DAY
        )

        absence_hours = get_absence_hours(current_date=self.wednesday_date)
        target_hours_with_absences = self.target_hours - absence_hours

        assert target_hours_with_absences == 4

    def test_with_absence_end_half_day_and_holiday(self):
        holiday = HolidayFactory(date=self.wednesday_date, is_all_day=False)
        ClosingHoursFactory(from_hour='08:00', to_hour='10:00', holiday=holiday)

        AbsenceFactory(
            user=self.user, approval_status=APPROVAL_STATUS.APPROVED, end_date=self.wednesday_date,
            end_date_number=ABSENCE_TYPE.HALF_DAY
        )

        absence_hours = get_absence_hours(current_date=self.wednesday_date)
        target_hours_with_absences = self.target_hours - absence_hours

        assert target_hours_with_absences == 2

    def test_all_day_holiday_ignores_any_absence(self):
        HolidayFactory(date=self.wednesday_date, is_all_day=True)

        absence_hours = get_absence_hours(current_date=self.wednesday_date)
        target_hours_with_absences = self.target_hours - absence_hours

        assert target_hours_with_absences == 0

        AbsenceFactory(user=self.user, approval_status=APPROVAL_STATUS.APPROVED)

        absence_hours = get_absence_hours(current_date=self.wednesday_date)
        target_hours_with_absences = self.target_hours - absence_hours

        assert target_hours_with_absences == 0
