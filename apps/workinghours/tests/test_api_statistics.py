from datetime import timedelta

from django.urls import reverse
from django.utils import timezone
from rest_framework import status

from apps.contracts.tests.factories import ContractFactory
from apps.metronom_commons.data import ACCOUNTING_TYPES
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.tests.factories import ProjectFactory, ProjectActivityStepFactory, ProjectActivityFactory, \
    WorkLogFactory
from apps.workinghours.models import WorkYear
from apps.workinghours.tests.factories import WorkYearFactory
from apps.workinghours.tests.mixins import TestFlexWorkMixin
import pytest


class TestListStatistics(MetronomBaseAPITestCase, TestFlexWorkMixin):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        WorkYearFactory.create_batch(size=4, user=cls.test_user)
        cls.date_format = '%Y-%m-%d'
        cls.start_date = timezone.now().date() + timedelta(days=-timezone.now().weekday(), weeks=-6)
        cls.end_date = timezone.now().date() + timedelta(days=4 - timezone.now().weekday(), weeks=6)

        invest_project = ProjectFactory(accounting_type=ACCOUNTING_TYPES.INVEST)
        invest_activity_step = ProjectActivityStepFactory(activity=ProjectActivityFactory(project=invest_project))

        profit_project = ProjectFactory(accounting_type=ACCOUNTING_TYPES.PROFIT)
        profit_activity_step = ProjectActivityStepFactory(activity=ProjectActivityFactory(project=profit_project))

        intern_project = ProjectFactory(accounting_type=ACCOUNTING_TYPES.INTERNAL)
        intern_activity_step = ProjectActivityStepFactory(activity=ProjectActivityFactory(project=intern_project))

        cls.contract = ContractFactory(
            start_date=cls.start_date,
            end_date=None,
            user=cls.test_user,
            flex_model=cls.get_full_day_full_week_flex_work()
        )

        current_date = cls.start_date
        while current_date <= cls.end_date:
            if current_date.weekday() <= 4:
                WorkLogFactory.create_batch(
                    size=2, work_date=current_date, project_activity_step=invest_activity_step,
                    worker=cls.test_user, tracked_time=1
                )
                WorkLogFactory.create_batch(
                    size=2, work_date=current_date, project_activity_step=profit_activity_step,
                    worker=cls.test_user, tracked_time=1
                )
                WorkLogFactory.create_batch(
                    size=2, work_date=current_date, project_activity_step=intern_activity_step,
                    worker=cls.test_user, tracked_time=1.5
                )
            current_date += timedelta(days=1)

        cls.endpoint = reverse('api_users:user-statistics-list', kwargs={'user_pk': 'me'})
        cls.response = cls.user_client.get(cls.endpoint)
        cls.working_times = cls.response.data.get('working_times')

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_vacation_length(self):
        vacation_keys = self.response.data.get('vacations').keys()
        assert len(vacation_keys) == 1
        year = WorkYear.objects.order_by('year').values_list('year', flat=True).last()
        assert year in vacation_keys

    @pytest.mark.skip(reason="The Method itself has to be refactored because it Can't be tested only on Date.now()")
    def test_response_stats_today(self):
        today = self.working_times.get('today')
        self.assertIsNotNone(today)
        assert type(today) == dict

        self.assertDictEqual(today, dict(
            intern_time='3:00',
            invest_time='2:00',
            profit_time='2:00',
            target_time='8:00',
            target_time_with_absences='8:00',
            logged_working_time='7:00',
            overtime='-1:00'
        ))

    def test_response_stats_week(self):
        week = self.working_times.get('week')
        self.assertIsNotNone(week)
        assert type(week) == dict

        self.assertDictEqual(week, dict(
            intern_time='15:00',
            invest_time='10:00',
            profit_time='10:00',
            target_time='40:00',
            target_time_with_absences='40:00',
            logged_working_time='35:00',
            overtime='-5:00'
        ))


class TestListStatisticsWithoutWorkYear(MetronomBaseAPITestCase, TestFlexWorkMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.start_date = timezone.now().date() + timedelta(days=-timezone.now().weekday(), weeks=-6)
        cls.contract = ContractFactory(
            start_date=cls.start_date,
            end_date=None,
            user=cls.test_user,
            flex_model=cls.get_full_day_full_week_flex_work()
        )

    def test_response_status(self):
        endpoint = reverse('api_users:user-statistics-list', kwargs={'user_pk': 'me'})
        response = self.user_client.get(endpoint)

        assert response.status_code == status.HTTP_200_OK
