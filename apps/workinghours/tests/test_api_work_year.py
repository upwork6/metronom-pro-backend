from django.urls import reverse
from rest_framework import status

from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.workinghours.tests.factories import WorkYearFactory


class TestWorkYearList(MetronomBaseAPITestCase):
    """
    Test the following API
    /api/users/{user_pk}/work_years/
    """

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        WorkYearFactory.create_batch(size=4, user=cls.test_user)

    def test_response_for_backoffice(self):
        endpoint = reverse('api_users:user-work-year-list', kwargs={'user_pk': self.test_user.pk})
        response = self.backoffice_client.get(endpoint)

        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 4

    def test_response_for_user(self):
        endpoint = reverse('api_users:user-work-year-list', kwargs={'user_pk': 'me'})
        response = self.user_client.get(endpoint)

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestWorkYearPatch(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.work_year = WorkYearFactory(user=cls.test_user, vacation_days_moved_from_last_year=10)
        assert cls.work_year.vacation_days_moved_from_last_year == 10

        cls.endpoint = reverse('api_users:user-work-year-detail', kwargs={
            'user_pk': 'me',
            'pk': cls.work_year.pk
        })

        cls.patch_data = dict(vacation_days_moved_from_last_year=20)

    def test_user_work_year_patch(self):
        response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')
        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestWorkYearPatchHR(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.work_year = WorkYearFactory(
            user=cls.test_user, vacation_days_moved_from_last_year=10, year=2019
        )
        assert cls.work_year.vacation_days_moved_from_last_year == 10

        cls.endpoint = reverse('api_users:user-work-year-detail', kwargs={
            'user_pk': cls.test_user.pk,
            'pk': cls.work_year.pk
        })

        cls.patch_data = dict(vacation_days_moved_from_last_year=20)

    def test_user_work_year_patch_by_HR(self):
        response = self.hr_client.patch(self.endpoint, data=self.patch_data, format='json')

        assert response.status_code == status.HTTP_200_OK

        self.work_year.refresh_from_db()

        assert self.work_year.vacation_days_moved_from_last_year == 20

    def test_cannot_patch_readonly_field(self):
        patch_data = dict(year=2020)
        response = self.hr_client.patch(self.endpoint, data=patch_data, format='json')

        assert response.status_code == status.HTTP_200_OK

        self.work_year.refresh_from_db()

        assert self.work_year.vacation_days_moved_from_last_year == 10
