import copy
import datetime

from django.utils import timezone
from rest_framework import status
from rest_framework.reverse import reverse

from apps.contracts.tests.factories import ContractFactory
from apps.metronom_commons.data import ABSENCE_REASONS, ABSENCE_TYPE, APPROVAL_STATUS
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.workinghours.tests.factories import AbsenceFactory, WorkYearFactory
from apps.workinghours.tests.mixins import TestFlexWorkMixin


class TestListAbsenceBase(MetronomBaseAPITestCase, TestFlexWorkMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.date_format = '%Y-%m-%d'

        cls.flex_work = cls.get_full_day_full_week_flex_work()
        ContractFactory(user=cls.test_user, flex_model=cls.flex_work)

        start_date = timezone.now() + datetime.timedelta(days=-timezone.now().weekday(), weeks=-5)
        end_date = timezone.now() + datetime.timedelta(days=4 - timezone.now().weekday(), weeks=-4)
        cls.absence1 = AbsenceFactory(
            reason=ABSENCE_REASONS.ACCIDENT,
            start_date_number=ABSENCE_TYPE.FULL_DAY,
            end_date_number=ABSENCE_TYPE.FULL_DAY,
            start_date=start_date.date(),
            end_date=end_date.date(),
            user=cls.test_user
        )
        start_date = timezone.now() + datetime.timedelta(days=-timezone.now().weekday(), weeks=-3)
        end_date = timezone.now() + datetime.timedelta(days=4 - timezone.now().weekday(), weeks=-2)
        cls.absence2 = AbsenceFactory(
            reason=ABSENCE_REASONS.ACCIDENT,
            start_date_number=ABSENCE_TYPE.FULL_DAY,
            end_date_number=ABSENCE_TYPE.FULL_DAY,
            start_date=start_date.date(),
            end_date=end_date.date(),
            user=cls.test_user
        )

        cls.absences = [cls.absence1, cls.absence2]


class TestListTenantAbsence(TestListAbsenceBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api_agency:absences')

    def test_response_is_correct(self):
        response = self.user_client.get(self.endpoint)
        assert response.status_code == 200

    def test_response_data_length_is_correct(self):
        response = self.user_client.get(self.endpoint)
        assert len(response.data) == len(self.absences)

    def test_response_data_is_correct(self):
        response = self.user_client.get(self.endpoint)
        ids = [x['id'] for x in response.data]
        reasons = [x['reason'] for x in response.data]
        start_dates = [x['start_date'] for x in response.data]
        end_dates = [x['end_date'] for x in response.data]
        notes = [x['note'] for x in response.data]

        for absence in self.absences:
            self.assertIn(str(absence.id), ids)
            self.assertIn(absence.reason, reasons)
            self.assertIn(absence.start_date.strftime(self.date_format), start_dates)
            self.assertIn(absence.end_date.strftime(self.date_format), end_dates)
            self.assertIn(absence.note, notes)


class TestListAbsence(TestListAbsenceBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api_users:absences-list', kwargs={'user_pk': cls.test_user.id})
        cls.work_years = WorkYearFactory.create_batch(size=4, user=cls.test_user)

    def test_response_is_correct(self):
        response = self.user_client.get(self.endpoint)
        assert response.status_code == 200

    def test_response_data_length_is_correct(self):
        response = self.user_client.get(self.endpoint)
        assert len(response.data.get('absences')) == len(self.absences)
        assert len(response.data.get('vacations').keys()) == len(self.work_years)

    def test_response_data_is_correct(self):
        response = self.user_client.get(self.endpoint)
        absences = response.data.get('absences')
        ids = [x['id'] for x in absences]
        reasons = [x['reason'] for x in absences]
        start_dates = [x['start_date'] for x in absences]
        end_dates = [x['end_date'] for x in absences]
        notes = [x['note'] for x in absences]

        for absence in self.absences:
            self.assertIn(str(absence.id), ids)
            self.assertIn(absence.reason, reasons)
            self.assertIn(absence.start_date.strftime(self.date_format), start_dates)
            self.assertIn(absence.end_date.strftime(self.date_format), end_dates)
            self.assertIn(absence.note, notes)


class TestPostAbsences(TestListAbsenceBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.date_format = '%Y-%m-%d'
        cls.start_date = timezone.now() + datetime.timedelta(days=-timezone.now().weekday(), weeks=1)
        cls.end_date = timezone.now() + datetime.timedelta(days=4 - timezone.now().weekday(), weeks=1)
        cls.post_data = dict(
            reason=ABSENCE_REASONS.VACATION,
            is_all_day=False,
            start_date=cls.start_date.date(),
            end_date=cls.end_date.date(),
            note='no note',
            start_date_number=ABSENCE_TYPE.FULL_DAY,
            end_date_number=ABSENCE_TYPE.FULL_DAY
        )

        cls.endpoint = reverse('api_users:absences-list', kwargs={'user_pk': cls.test_user.id})

        cls.response = cls.user_client.post(cls.endpoint, data=cls.post_data, format='json')
        cls.data = cls.response.data

    def test_response_post_absenses_is_correct(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_add_new_absence_in_future(self):
        assert self.data.get('reason') == self.post_data['reason']
        assert self.data.get('reason') == ABSENCE_REASONS.VACATION

    def test_start_date_is_required(self):
        post_data = copy.copy(self.post_data)
        post_data.pop('start_date')
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        self.assertErrorResponse(
            response,
            {"start_date": ['This field is required.']}
        )

    def test_end_date_is_required(self):
        post_data = copy.copy(self.post_data)
        post_data.pop('end_date')
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        self.assertErrorResponse(
            response,
            {"end_date": ['This field is required.']}
        )

    def test_start_date_number_is_required(self):
        post_data = copy.copy(self.post_data)
        post_data.pop('start_date_number')
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        self.assertErrorResponse(
            response,
            {"start_date_number": ['This field is required.']}
        )

    def test_end_date_number_is_required(self):
        post_data = copy.copy(self.post_data)
        post_data.pop('end_date_number')
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        self.assertErrorResponse(
            response,
            {"end_date_number": ['This field is required.']}
        )

    def test_add_new_absence_in_past(self):
        start_date = self.start_date + datetime.timedelta(weeks=-1)
        end_date = self.end_date + datetime.timedelta(weeks=-1)
        post_data = copy.copy(self.post_data)
        post_data['start_date'] = start_date.strftime(self.date_format)
        post_data['end_date'] = end_date.strftime(self.date_format)

        response = self.user_client.post(self.endpoint, post_data, format='json')
        assert response.data.get('start_date') == post_data.get('start_date')
        assert response.data.get('end_date') == post_data.get('end_date')

    def test_add_new_absence_today(self):
        start_date = datetime.datetime.now()
        end_date = start_date
        post_data = copy.copy(self.post_data)
        post_data['start_date'] = start_date.strftime(self.date_format)
        post_data['end_date'] = end_date.strftime(self.date_format)
        post_data.pop('end_date_number')

        response = self.user_client.post(self.endpoint, post_data, format='json')
        if start_date.weekday() > 4:
            self.assertErrorResponse(
                response,
                {"__all__": ['Cannot start absence on a Non Working day.']}
            )
        else:
            assert response.data.get('start_date') == post_data.get('start_date')
            assert response.data.get('end_date') == post_data.get('end_date')

    def test_end_date_number_not_required(self):
        start_date = datetime.datetime.now()
        end_date = start_date
        post_data = copy.copy(self.post_data)
        post_data['start_date'] = start_date.strftime(self.date_format)
        post_data['end_date'] = end_date.strftime(self.date_format)

        response = self.user_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(
            response,
            {"__all__": ['The "end_date_number" is not required when start and end dates match.']}
        )

    def test_end_date_not_earlier_than_start_date(self):
        start_date = datetime.datetime.now()
        end_date = start_date - datetime.timedelta(days=2)
        post_data = copy.copy(self.post_data)
        post_data['start_date'] = start_date.strftime(self.date_format)
        post_data['end_date'] = end_date.strftime(self.date_format)

        response = self.user_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(
            response,
            {"__all__": ['The "end_date" cannot be earlier than the "start_date".']}
        )

    def test_case_insensitive_reason_is_allowed(self):
        start_date = self.start_date + datetime.timedelta(weeks=-1)
        end_date = self.end_date + datetime.timedelta(weeks=-1)
        post_data = copy.copy(self.post_data)
        post_data['start_date'] = start_date.strftime(self.date_format)
        post_data['end_date'] = end_date.strftime(self.date_format)
        post_data['reason'] = 'VACATION'
        response = self.user_client.post(self.endpoint, data=post_data, format='json')

        assert response.status_code == status.HTTP_201_CREATED
        assert response.data.get('reason') == post_data.get('reason').lower()

    def test_invalid_start_date_number(self):
        post_data = copy.copy(self.post_data)
        post_data['start_date_number'] = 2

        response = self.user_client.post(self.endpoint, data=post_data, format='json')

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data['errors']['start_date_number'][0] == '"2" is not a valid choice.'

    def test_invalid_end_date_number(self):
        post_data = copy.copy(self.post_data)
        post_data['end_date_number'] = 2

        response = self.user_client.post(self.endpoint, data=post_data, format='json')

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data['errors']['end_date_number'][0] == '"2" is not a valid choice.'

    def test_not_enough_vacation_days_left(self):
        WorkYearFactory(user=self.test_user, vacation_days_moved_from_last_year=2)
        start_date = self.start_date + datetime.timedelta(weeks=-1)
        end_date = self.end_date + datetime.timedelta(weeks=-1)
        post_data = copy.copy(self.post_data)
        post_data['start_date'] = start_date.strftime(self.date_format)
        post_data['end_date'] = end_date.strftime(self.date_format)
        response = self.user_client.post(self.endpoint, data=post_data, format='json')

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertErrorResponse(
            response,
            {"__all__": ["Not enough vacation days available. Requested: 5, Available: 2.0"]}
        )

    def test_enough_vacation_days_left(self):
        WorkYearFactory(user=self.test_user, vacation_days_moved_from_last_year=5)
        start_date = self.start_date + datetime.timedelta(weeks=-1)
        end_date = self.end_date + datetime.timedelta(weeks=-1)
        post_data = copy.copy(self.post_data)
        post_data['start_date'] = start_date.strftime(self.date_format)
        post_data['end_date'] = end_date.strftime(self.date_format)
        response = self.user_client.post(self.endpoint, data=post_data, format='json')

        assert response.status_code == status.HTTP_201_CREATED


class TestPatchAbsences(TestListAbsenceBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.date_format = '%Y-%m-%d'

        cls.start_date = timezone.now() + datetime.timedelta(days=-timezone.now().weekday(), weeks=-1)
        cls.end_date = timezone.now() + datetime.timedelta(days=4 - timezone.now().weekday(), weeks=1)
        cls.patch_data = dict(
            reason=ABSENCE_REASONS.ACCIDENT,
            start_date_number=ABSENCE_TYPE.FULL_DAY,
            end_date_number=ABSENCE_TYPE.FULL_DAY,
            start_date=cls.start_date.date(),
            end_date=cls.end_date.date(),
        )
        cls.endpoint = reverse('api_users:absences-detail', kwargs={
            'user_pk': cls.test_user.id,
            'pk': cls.absence1.pk
        })

    def test_response_status(self):
        response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')
        assert response.status_code == status.HTTP_200_OK

    def test_start_date_is_required(self):
        patch_data = dict(reason=ABSENCE_REASONS.EDUCATION, )
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertErrorResponse(
            response,
            {"__all__": ['The field "start_date" is required.']}
        )

    def test_end_date_is_required(self):
        patch_data = dict(reason=ABSENCE_REASONS.MILITARY, start_date=self.start_date.date())
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertErrorResponse(
            response,
            {"__all__": ['The field "end_date" is required.']}
        )

    def test_start_date_number_is_required(self):
        patch_data = dict(
            reason=ABSENCE_REASONS.MILITARY,
            start_date=self.start_date.date(),
            end_date=self.end_date.date()
        )
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertErrorResponse(
            response,
            {"__all__": ['The field "start_date_number" is required.']}
        )

    def test_end_date_number_is_required(self):
        patch_data = dict(
            reason=ABSENCE_REASONS.MILITARY,
            start_date=self.start_date.date(),
            end_date=self.end_date.date(),
            start_date_number=ABSENCE_TYPE.FULL_DAY
        )
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertErrorResponse(
            response,
            {"__all__": ['The field "end_date_number" is required.']}
        )

    def test_patch_future_absence(self):
        response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')

        assert response.data['start_date'] == self.start_date.strftime(self.date_format)
        assert response.data['end_date'] == self.end_date.strftime(self.date_format)
        assert response.status_code == status.HTTP_200_OK

    def test_patch_today_absence(self):
        patch_data = copy.copy(self.patch_data)
        start_date = timezone.now()
        patch_data['start_date'] = start_date.date()
        patch_data['end_date'] = start_date.date()
        patch_data['start_date_number'] = ABSENCE_TYPE.FULL_DAY
        patch_data.pop('end_date_number')

        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        if start_date.weekday() > 4:
            self.assertErrorResponse(
                response,
                {"__all__": ['Cannot start absence on a Non Working day.']}
            )
        else:
            assert response.data['start_date'] == patch_data.get('start_date').strftime(self.date_format)
            assert response.data['end_date'] == patch_data.get('end_date').strftime(self.date_format)
            assert response.status_code == status.HTTP_200_OK

    def test_end_date_number_not_required(self):
        patch_data = copy.copy(self.patch_data)
        start_date = timezone.now()
        patch_data['start_date'] = start_date.date()
        patch_data['end_date'] = start_date.date()
        patch_data['start_date_number'] = ABSENCE_TYPE.FULL_DAY

        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertErrorResponse(
            response,
            {"__all__": ['The "end_date_number" is not required when start and end dates match.']}
        )

    def test_patch_past_absence(self):
        patch_data = copy.copy(self.patch_data)
        start_date = timezone.now() - datetime.timedelta(days=3)

        while start_date.weekday() > 4:
            start_date -= datetime.timedelta(days=1)

        patch_data['start_date'] = start_date.date()
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')

        assert response.data['start_date'] == patch_data.get('start_date').strftime(self.date_format)
        assert response.data['end_date'] == self.end_date.strftime(self.date_format)
        assert response.status_code == status.HTTP_200_OK

    def test_end_date_not_earlier_than_start_date(self):
        end_date = self.start_date - datetime.timedelta(days=2)
        patch_data = copy.copy(self.patch_data)
        patch_data['start_date'] = self.start_date.strftime(self.date_format)
        patch_data['end_date'] = end_date.strftime(self.date_format)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        self.assertErrorResponse(
            response,
            {"__all__": ['The "end_date" cannot be earlier than the "start_date".']}
        )

    def test_case_insensitive_reason_is_allowed(self):
        patch_data = copy.copy(self.patch_data)
        patch_data['reason'] = 'VaCaTiOn'
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')

        assert response.status_code == status.HTTP_200_OK
        assert response.data.get('reason') == patch_data.get('reason').lower()

    def test_patch_for_already_approved_absence(self):
        patch_data = copy.copy(self.patch_data)
        patch_data['reason'] = ABSENCE_REASONS.COMPENSATION
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')

        assert response.status_code == status.HTTP_200_OK

        self.absence1.approval_status=APPROVAL_STATUS.APPROVED
        self.absence1.save()
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')

        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_not_enough_vacation_days_left(self):
        WorkYearFactory(user=self.test_user, vacation_days_moved_from_last_year=2)
        patch_data = copy.copy(self.patch_data)
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertErrorResponse(
            response,
            {"__all__": ["Not enough vacation days available. Requested: 15, Available: 2.0"]}
        )

    def test_enough_vacation_days_left(self):
        WorkYearFactory(user=self.test_user, vacation_days_moved_from_last_year=15)
        patch_data = copy.copy(self.patch_data)
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')

        assert response.status_code == status.HTTP_200_OK


class TestDeleteAbsences(TestListAbsenceBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api_users:absences-detail', kwargs={
            'user_pk': cls.test_user.id,
            'pk': cls.absence1.pk
        })

    def test_response_is_correct(self):
        response = self.user_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_response_is_permission_denied(self):
        self.absence1.approval_status=APPROVAL_STATUS.APPROVED
        self.absence1.save()
        response = self.user_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_403_FORBIDDEN
