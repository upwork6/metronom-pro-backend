from datetime import date, datetime, timedelta

from django.http import QueryDict
from django.urls import reverse
from django.utils import timezone
from rest_framework import status

from apps.contracts.tests.factories import ContractFactory
from apps.metronom_commons.data import ACCOUNTING_TYPES, ABSENCE_TYPE, APPROVAL_STATUS
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.tests.factories import (ProjectFactory, ProjectActivityFactory, ProjectActivityStepFactory,
                                           WorkLogFactory)
from apps.workinghours.tests.factories import HolidayFactory, ClosingHoursFactory, AbsenceFactory
from apps.workinghours.tests.mixins import TestFlexWorkMixin


class TestListWorkingStatistics(MetronomBaseAPITestCase, TestFlexWorkMixin):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.date_format = '%Y-%m-%d'
        cls.start_date = date(2018, 4, 15)
        cls.end_date = date(2018, 4, 30)

        invest_project = ProjectFactory(accounting_type=ACCOUNTING_TYPES.INVEST)
        invest_activity_step = ProjectActivityStepFactory(activity=ProjectActivityFactory(project=invest_project))

        profit_project = ProjectFactory(accounting_type=ACCOUNTING_TYPES.PROFIT)
        profit_activity_step = ProjectActivityStepFactory(activity=ProjectActivityFactory(project=profit_project))

        intern_project = ProjectFactory(accounting_type=ACCOUNTING_TYPES.INTERNAL)
        intern_activity_step = ProjectActivityStepFactory(activity=ProjectActivityFactory(project=intern_project))

        cls.flex_work1 = cls.get_full_day_full_week_flex_work()
        cls.flex_work2 = cls.get_half_day_full_week_flex_work()

        cls.contract1 = ContractFactory(
            start_date=date(2018, 4, 18),
            end_date=date(2018, 4, 23),
            user=cls.test_user,
            flex_model=cls.flex_work1
        )

        current_date = cls.contract1.start_date
        while current_date <= cls.contract1.end_date:
            if current_date.weekday() <= 4:
                WorkLogFactory.create_batch(
                    size=2, work_date=current_date, project_activity_step=invest_activity_step,
                    worker=cls.test_user, tracked_time=1
                )
                WorkLogFactory.create_batch(
                    size=2, work_date=current_date, project_activity_step=profit_activity_step,
                    worker=cls.test_user, tracked_time=1
                )
                WorkLogFactory.create_batch(
                    size=2, work_date=current_date, project_activity_step=intern_activity_step,
                    worker=cls.test_user, tracked_time=1.5
                )
            current_date += timedelta(days=1)

        cls.contract2 = ContractFactory(
            start_date=date(2018, 4, 25),
            end_date=None,
            user=cls.test_user,
            flex_model=cls.flex_work2
        )

        current_date = cls.contract2.start_date
        while current_date <= cls.end_date:
            if current_date.weekday() <= 4:
                WorkLogFactory(
                    work_date=current_date, project_activity_step=profit_activity_step,
                    worker=cls.test_user, tracked_time=1
                )
                WorkLogFactory(
                    work_date=current_date, project_activity_step=intern_activity_step,
                    worker=cls.test_user, tracked_time=1
                )
            current_date += timedelta(days=1)

        cls.endpoint_base = reverse('api_users:user-working-statistics-list', kwargs={'user_pk': 'me'})
        query_string = QueryDict(
            f'start_date={cls.start_date.strftime(cls.date_format)}&end_date={cls.end_date.strftime(cls.date_format)}'
        )
        cls.endpoint = f'{cls.endpoint_base}?{query_string.urlencode()}'
        cls.response = cls.user_client.get(cls.endpoint)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_data_count(self):
        count = (self.end_date - self.start_date).days + 1
        assert len(self.response.data) == count

    def test_response_data(self):
        working_statistics = self.response.data

        for stats in working_statistics:
            current_date = datetime.strptime(stats.get('date'), self.date_format).date()
            invest_time = stats.get('invest_time')
            profit_time = stats.get('profit_time')
            intern_time = stats.get('intern_time')
            target_time = stats.get('target_time')
            target_time_with_absences = stats.get('target_time_with_absences')
            logged_working_time = stats.get('logged_working_time')
            overtime = stats.get('overtime')

            if current_date.weekday() > 4:
                assert invest_time == '0:00'
                assert profit_time == '0:00'
                assert intern_time == '0:00'
                assert target_time == '0:00'
                assert target_time_with_absences == '0:00'
                assert logged_working_time == '0:00'
                assert overtime == '0:00'
            elif current_date < self.contract1.start_date:
                assert invest_time == '0:00'
                assert profit_time == '0:00'
                assert intern_time == '0:00'
                assert target_time == '0:00'
                assert target_time_with_absences == '0:00'
                assert logged_working_time == '0:00'
                assert overtime == '0:00'
            elif self.contract1.start_date < current_date < self.contract1.end_date:
                assert invest_time == '2:00'
                assert profit_time == '2:00'
                assert intern_time == '3:00'
                assert target_time == '8:00'
                assert target_time_with_absences == '8:00'
                assert logged_working_time == '7:00'
                assert overtime == '-1:00'
            elif self.contract1.end_date < current_date < self.contract2.start_date:
                assert invest_time == '0:00'
                assert profit_time == '0:00'
                assert intern_time == '0:00'
                assert target_time == '0:00'
                assert target_time_with_absences == '0:00'
                assert logged_working_time == '0:00'
                assert overtime == '0:00'
            elif current_date > self.contract2.start_date:
                assert invest_time == '0:00'
                assert profit_time == '1:00'
                assert intern_time == '1:00'
                assert target_time == '4:00'
                assert target_time_with_absences == '4:00'
                assert logged_working_time == '2:00'
                assert overtime == '-2:00'


class TestListWorkingStatisticsQueryParams(MetronomBaseAPITestCase, TestFlexWorkMixin):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.date_format = '%Y-%m-%d'
        cls.endpoint = reverse('api_users:user-working-statistics-list', kwargs={'user_pk': 'me'})

    def test_response_status_without_queryparams(self):
        response = self.user_client.get(self.endpoint)
        assert response.status_code == status.HTTP_200_OK

    def test_response_status_with_both_queryparams(self):
        today_date = timezone.now().date().strftime(self.date_format)
        query_string = QueryDict(f'start_date={today_date}&end_date={today_date}')
        endpoint = f'{self.endpoint}?{query_string.urlencode()}'

        response = self.user_client.get(endpoint)

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertErrorResponse(
            response,
            {"__all__": ['You do not have any valid contract for that period of time']}
        )

    def test_response_status_with_start_date_queryparam(self):
        today_date = timezone.now().date().strftime(self.date_format)
        query_string = QueryDict(f'start_date={today_date}')
        endpoint = f'{self.endpoint}?{query_string.urlencode()}'

        response = self.user_client.get(endpoint)

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertErrorResponse(
            response,
            {"__all__": ['You do not have any valid contract for that period of time']}
        )

    def test_response_status_with_end_date_queryparam(self):
        today_date = timezone.now().date().strftime(self.date_format)
        query_string = QueryDict(f'end_date={today_date}')
        endpoint = f'{self.endpoint}?{query_string.urlencode()}'

        response = self.user_client.get(endpoint)

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertErrorResponse(
            response,
            {"__all__": ['You do not have any valid contract for that period of time']}
        )


class TestWorkingStatistics(MetronomBaseAPITestCase, TestFlexWorkMixin):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.date_format = '%Y-%m-%d'
        cls.current_date = (timezone.now() + timedelta(days=1 - timezone.now().weekday())).date().strftime(
            cls.date_format)
        query_string = QueryDict(
            f'start_date={cls.current_date}&end_date={cls.current_date}'
        )
        endpoint_base = reverse('api_users:user-working-statistics-list', kwargs={'user_pk': 'me'})
        cls.endpoint = f'{endpoint_base}?{query_string.urlencode()}'

        ContractFactory(
            start_date=timezone.now() + timedelta(days=-timezone.now().weekday()),
            end_date=timezone.now() + timedelta(days=2 - timezone.now().weekday()),
            user=cls.test_user,
            flex_model=cls.get_full_day_full_week_flex_work()
        )

        invest_project = ProjectFactory(accounting_type=ACCOUNTING_TYPES.INVEST)
        cls.invest_activity_step = ProjectActivityStepFactory(activity=ProjectActivityFactory(project=invest_project))

        profit_project = ProjectFactory(accounting_type=ACCOUNTING_TYPES.PROFIT)
        cls.profit_activity_step = ProjectActivityStepFactory(activity=ProjectActivityFactory(project=profit_project))

        intern_project = ProjectFactory(accounting_type=ACCOUNTING_TYPES.INTERNAL)
        cls.intern_activity_step = ProjectActivityStepFactory(activity=ProjectActivityFactory(project=intern_project))

        WorkLogFactory.create_batch(
            size=2, work_date=cls.current_date, project_activity_step=cls.intern_activity_step,
            worker=cls.test_user, tracked_time=1.5
        )
        WorkLogFactory.create_batch(
            size=2, work_date=cls.current_date, project_activity_step=cls.invest_activity_step,
            worker=cls.test_user, tracked_time=1
        )
        WorkLogFactory.create_batch(
            size=2, work_date=cls.current_date, project_activity_step=cls.profit_activity_step,
            worker=cls.test_user, tracked_time=1
        )

    def test_response_status(self):
        response = self.user_client.get(self.endpoint)
        assert len(response.data) == 1

    def test_response_data_with_logs(self):
        response = self.user_client.get(self.endpoint)
        self.assertDictEqual(response.data[0], dict(
            date=self.current_date,
            intern_time='3:00',
            invest_time='2:00',
            profit_time='2:00',
            target_time='8:00',
            target_time_with_absences='8:00',
            logged_working_time='7:00',
            overtime='-1:00'
        ))

    def test_with_partial_holiday(self):
        holiday = HolidayFactory(date=self.current_date, is_all_day=False)
        ClosingHoursFactory(from_hour='08:00', to_hour='12:00', holiday=holiday)

        response = self.user_client.get(self.endpoint)

        self.assertDictEqual(response.data[0], dict(
            date=self.current_date,
            intern_time='3:00',
            invest_time='2:00',
            profit_time='2:00',
            target_time='8:00',
            target_time_with_absences='4:00',
            logged_working_time='7:00',
            overtime='3:00'
        ))

    def test_with_all_day_holiday(self):
        HolidayFactory(date=self.current_date, is_all_day=True)
        response = self.user_client.get(self.endpoint)

        self.assertDictEqual(response.data[0], dict(
            date=self.current_date,
            intern_time='3:00',
            invest_time='2:00',
            profit_time='2:00',
            target_time='8:00',
            target_time_with_absences='0:00',
            logged_working_time='7:00',
            overtime='7:00'
        ))

    def test_with_absence(self):
        AbsenceFactory(user=self.test_user, approval_status=APPROVAL_STATUS.APPROVED)
        response = self.user_client.get(self.endpoint)

        self.assertDictEqual(response.data[0], dict(
            date=self.current_date,
            intern_time='3:00',
            invest_time='2:00',
            profit_time='2:00',
            target_time='8:00',
            target_time_with_absences='0:00',
            logged_working_time='7:00',
            overtime='7:00'
        ))

    def test_with_absence_start_full_day(self):
        AbsenceFactory(
            user=self.test_user, approval_status=APPROVAL_STATUS.APPROVED, start_date=self.current_date,
            start_date_number=ABSENCE_TYPE.FULL_DAY
        )

        response = self.user_client.get(self.endpoint)

        self.assertDictEqual(response.data[0], dict(
            date=self.current_date,
            intern_time='3:00',
            invest_time='2:00',
            profit_time='2:00',
            target_time='8:00',
            target_time_with_absences='0:00',
            logged_working_time='7:00',
            overtime='7:00'
        ))

    def test_with_absence_start_half_day(self):
        AbsenceFactory(
            user=self.test_user, approval_status=APPROVAL_STATUS.APPROVED, start_date=self.current_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY
        )

        response = self.user_client.get(self.endpoint)

        self.assertDictEqual(response.data[0], dict(
            date=self.current_date,
            intern_time='3:00',
            invest_time='2:00',
            profit_time='2:00',
            target_time='8:00',
            target_time_with_absences='4:00',
            logged_working_time='7:00',
            overtime='3:00'
        ))

    def test_with_absence_end_full_day(self):
        AbsenceFactory(
            user=self.test_user, approval_status=APPROVAL_STATUS.APPROVED, end_date=self.current_date,
            end_date_number=ABSENCE_TYPE.FULL_DAY
        )

        response = self.user_client.get(self.endpoint)

        self.assertDictEqual(response.data[0], dict(
            date=self.current_date,
            intern_time='3:00',
            invest_time='2:00',
            profit_time='2:00',
            target_time='8:00',
            target_time_with_absences='0:00',
            logged_working_time='7:00',
            overtime='7:00'
        ))

    def test_with_absence_end_half_day(self):
        AbsenceFactory(
            user=self.test_user, approval_status=APPROVAL_STATUS.APPROVED, end_date=self.current_date,
            end_date_number=ABSENCE_TYPE.HALF_DAY
        )

        response = self.user_client.get(self.endpoint)

        self.assertDictEqual(response.data[0], dict(
            date=self.current_date,
            intern_time='3:00',
            invest_time='2:00',
            profit_time='2:00',
            target_time='8:00',
            target_time_with_absences='4:00',
            logged_working_time='7:00',
            overtime='3:00'
        ))

    def test_with_absence_end_half_day_and_holiday(self):
        holiday = HolidayFactory(date=self.current_date, is_all_day=False)
        ClosingHoursFactory(from_hour='08:00', to_hour='10:00', holiday=holiday)

        AbsenceFactory(
            user=self.test_user, approval_status=APPROVAL_STATUS.APPROVED, end_date=self.current_date,
            end_date_number=ABSENCE_TYPE.HALF_DAY
        )

        response = self.user_client.get(self.endpoint)

        self.assertDictEqual(response.data[0], dict(
            date=self.current_date,
            intern_time='3:00',
            invest_time='2:00',
            profit_time='2:00',
            target_time='8:00',
            target_time_with_absences='2:00',
            logged_working_time='7:00',
            overtime='5:00'
        ))

    def test_all_day_holiday_ignores_any_absence(self):
        HolidayFactory(date=self.current_date, is_all_day=True)

        response = self.user_client.get(self.endpoint)

        self.assertDictEqual(response.data[0], dict(
            date=self.current_date,
            intern_time='3:00',
            invest_time='2:00',
            profit_time='2:00',
            target_time='8:00',
            target_time_with_absences='0:00',
            logged_working_time='7:00',
            overtime='7:00'
        ))

        AbsenceFactory(user=self.test_user, approval_status=APPROVAL_STATUS.APPROVED)

        response = self.user_client.get(self.endpoint)

        self.assertDictEqual(response.data[0], dict(
            date=self.current_date,
            intern_time='3:00',
            invest_time='2:00',
            profit_time='2:00',
            target_time='8:00',
            target_time_with_absences='0:00',
            logged_working_time='7:00',
            overtime='7:00'
        ))
