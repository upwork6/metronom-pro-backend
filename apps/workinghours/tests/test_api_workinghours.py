from datetime import timedelta, datetime, date

from django.http import QueryDict
from django.utils import timezone
from rest_framework import status
from rest_framework.reverse import reverse

from apps.contracts.models import Contract
from apps.contracts.tests.factories import ContractFactory
from apps.metronom_commons.data import ABSENCE_TYPE
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.workinghours.tests.factories import (HolidayFactory,
                                               ClosingHoursFactory, AbsenceFactory)
from apps.workinghours.tests.mixins import TestFlexWorkMixin


class TestListWorkingHours(MetronomBaseAPITestCase, TestFlexWorkMixin):
    """
    Setup full-week flex work and a 3 days a week flex work for every week (Monday, Wednesday, Friday)
    Setup holiday on 3rd day of the current week
    """

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.date_format = '%Y-%m-%d'
        cls.start_date = timezone.now() + timedelta(days=-timezone.now().weekday())
        cls.end_date = timezone.now() + timedelta(days=4 - timezone.now().weekday())

        cls.flex_work = cls.get_full_day_mon_wed_fri_flex_work()
        ContractFactory(
            user=cls.test_user, flex_model=cls.flex_work,
            start_date=cls.start_date + timedelta(days=-1),
            end_date=cls.end_date + timedelta(days=1)
        )

        holiday_date = timezone.now() + timedelta(days=2 - timezone.now().weekday())
        holiday = HolidayFactory(name='test holiday', date=holiday_date, is_all_day=False)
        ClosingHoursFactory(from_hour='13:00', to_hour='17:00', holiday=holiday)

        cls.absence = AbsenceFactory(
            start_date=cls.start_date.date(),
            end_date=cls.end_date.date(),
            start_date_number=ABSENCE_TYPE.FULL_DAY,
            end_date_number=ABSENCE_TYPE.FULL_DAY,
            user=cls.test_user)

        cls.endpoint_base = reverse('api_users:user-working-hours-list', kwargs={
            'user_pk': 'me',
        })

        start_date = cls.start_date.strftime(cls.date_format)
        end_date = cls.end_date.strftime(cls.date_format)
        query_string = QueryDict(f'start_date={start_date}&end_date={end_date}')
        cls.endpoint = f'{cls.endpoint_base}?{query_string.urlencode()}'

    def test_response_status(self):
        response = self.user_client.get(self.endpoint)
        assert response.status_code == status.HTTP_200_OK

    def test_user_has_no_contracts(self):
        Contract.objects.all().delete()
        response = self.user_client.get(self.endpoint)
        self.assertErrorResponse(
            response,
            {"__all__": ["You do not have any valid contract for that period of time"]}
        )

    def test_start_date_is_valid(self):
        query_string = QueryDict(
            f'start_date=2000-24-80&end_date=2000-24-90'
        )
        endpoint = f'{self.endpoint_base}?{query_string.urlencode()}'

        response = self.user_client.get(endpoint)
        self.assertErrorResponse(
            response,
            {"__all__": ["time data '2000-24-80' does not match format '%Y-%m-%d'"]}
        )

    def test_end_date_is_valid(self):
        query_string = QueryDict(
            f'start_date={self.start_date.strftime(self.date_format)}&end_date=2000-24-90'
        )
        endpoint = f'{self.endpoint_base}?{query_string.urlencode()}'

        response = self.user_client.get(endpoint)
        self.assertErrorResponse(
            response,
            {"__all__": ["time data '2000-24-90' does not match format '%Y-%m-%d'"]}
        )

    def test_end_date_not_earlier_than_start_date(self):
        start_date = datetime.now()
        end_date = start_date - timedelta(days=2)

        query_string = QueryDict(
            f'start_date={start_date.strftime(self.date_format)}&end_date={end_date.strftime(self.date_format)}'
        )
        endpoint = f'{self.endpoint_base}?{query_string.urlencode()}'

        response = self.user_client.get(endpoint)
        self.assertErrorResponse(
            response,
            {"__all__": ['The "end_date" cannot be earlier than the "start_date".']}
        )

    def test_response_data_is_valid(self):
        response = self.user_client.get(self.endpoint)
        working_hours = response.data.get('working_hours')
        assert len(working_hours) == 5
        assert working_hours[0]['hours'] != []
        assert len(working_hours[0]['hours']) == 2

        assert working_hours[1]['hours'] == []
        assert working_hours[2]['hours'] != []
        assert len(working_hours[2]['hours']) == 1
        assert working_hours[2]['agency_closing_hours'] != []
        assert len(working_hours[2]['agency_closing_hours']) == 1

        assert working_hours[3]['hours'] == []
        assert working_hours[4]['hours'] != []
        assert len(working_hours[4]['hours']) == 2

    def test_response_contains_absences(self):
        response = self.user_client.get(self.endpoint)
        assert 'absences' in response.data.keys()
        absences = response.data.get('absences')
        assert len(absences) == 1

    def test_response_contains_flex_model(self):
        response = self.user_client.get(self.endpoint)
        assert 'flex_model' in response.data.keys()

    def test_response_absence_keys(self):
        response = self.user_client.get(self.endpoint)
        absences = response.data.get('absences')
        expected_keys = [
            'id',
            'reason',
            'start_date',
            'end_date',
            'start_date_number',
            'end_date_number',
            'note',
            'approval_status',
            'absence_days'
        ]
        for absence in absences:
            self.assertTrue(all([key in expected_keys for key in absence.keys()]))


class TestWorkingHoursWithContracts(MetronomBaseAPITestCase, TestFlexWorkMixin):
    """
    Test working hours in date time ranges with different contracts
    """

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.date_format = '%Y-%m-%d'
        cls.flex_work1 = cls.get_full_day_full_week_flex_work()
        cls.flex_work2 = cls.get_half_day_full_week_flex_work()

        cls.contract1 = ContractFactory(
            start_date=date(2018, 4, 18),
            end_date=date(2018, 5, 18),
            user=cls.test_user,
            flex_model=cls.flex_work1
        )
        cls.contract2 = ContractFactory(
            start_date=date(2018, 5, 25),
            end_date=None,
            user=cls.test_user,
            flex_model=cls.flex_work2
        )

        cls.start_date = date(2018, 4, 1)
        cls.end_date = date(2018, 5, 31)

        cls.endpoint_base = reverse('api_users:user-working-hours-list', kwargs={'user_pk': 'me'})
        query_string = QueryDict(
            f'start_date={cls.start_date.strftime(cls.date_format)}&end_date={cls.end_date.strftime(cls.date_format)}'
        )
        cls.endpoint = f'{cls.endpoint_base}?{query_string.urlencode()}'
        cls.response = cls.user_client.get(cls.endpoint)

    def test_response_status(self):
        response = self.user_client.get(self.endpoint)
        assert response.status_code == status.HTTP_200_OK

    def test_working_hours(self):
        working_hours = self.response.data.get('working_hours')
        for wh in working_hours:
            current_date = datetime.strptime(wh.get('date'), self.date_format).date()
            hours = wh.get('hours')

            if current_date.weekday() in [5, 6]:
                assert hours == []
            elif current_date < self.contract1.start_date:
                assert hours == []
            elif self.contract1.start_date < current_date < self.contract1.end_date:
                assert hours != []
                assert hours[0] in [{"from_hour": "0800", "to_hour": "1200"}, {"from_hour": "1300", "to_hour": "1700"}]
                assert hours[1] in [{"from_hour": "0800", "to_hour": "1200"}, {"from_hour": "1300", "to_hour": "1700"}]
            elif self.contract1.end_date < current_date < self.contract2.start_date:
                assert hours == []
            elif current_date > self.contract2.start_date:
                assert hours != []
                self.assertListEqual(hours, [{"from_hour": "0800", "to_hour": "1200"}])


class TestListWorkingHoursQueryParams(MetronomBaseAPITestCase, TestFlexWorkMixin):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.date_format = '%Y-%m-%d'
        cls.endpoint = reverse('api_users:user-working-hours-list', kwargs={'user_pk': 'me'})

    def test_response_status_without_queryparam(self):
        response = self.user_client.get(self.endpoint)
        assert response.status_code == status.HTTP_200_OK

    def test_response_status_with_both_queryparams(self):
        today_date = timezone.now().date().strftime(self.date_format)
        query_string = QueryDict(f'start_date={today_date}&end_date={today_date}')
        endpoint = f'{self.endpoint}?{query_string.urlencode()}'

        response = self.user_client.get(endpoint)

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertErrorResponse(
            response,
            {"__all__": ['You do not have any valid contract for that period of time']}
        )

    def test_response_status_with_start_date_queryparam(self):
        today_date = timezone.now().date().strftime(self.date_format)
        query_string = QueryDict(f'start_date={today_date}')
        endpoint = f'{self.endpoint}?{query_string.urlencode()}'

        response = self.user_client.get(endpoint)

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertErrorResponse(
            response,
            {"__all__": ['You do not have any valid contract for that period of time']}
        )

    def test_response_status_with_end_date_queryparam(self):
        today_date = timezone.now().date().strftime(self.date_format)
        query_string = QueryDict(f'end_date={today_date}')
        endpoint = f'{self.endpoint}?{query_string.urlencode()}'

        response = self.user_client.get(endpoint)

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertErrorResponse(
            response,
            {"__all__": ['You do not have any valid contract for that period of time']}
        )
