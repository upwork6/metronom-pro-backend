import copy
from datetime import datetime, timedelta

from django.urls import reverse
from django.utils import timezone
from rest_framework import status

from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.workinghours.tests.factories import ClosingHoursFactory, HolidayFactory


class TestHolidayList(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.holidays = HolidayFactory.create_batch(size=2)

        cls.endpoint = reverse('api:holiday-list')
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_list_response(self):
        self.assertEqual(self.response.status_code, 200)

    def test_response_data_length_is_correct(self):
        assert len(self.response.data) == 2

    def test_response_data_is_correct(self):
        names = [x['name'] for x in self.response.data]
        dates = [x['date'] for x in self.response.data]
        are_all_day = [x['is_all_day'] for x in self.response.data]
        closing_hours_set = [x['closing_hours'] for x in self.response.data]

        for holiday in self.holidays:
            self.assertIn(holiday.name, names)
            self.assertIn(holiday.date.strftime('%Y-%m-%d'), dates)
            self.assertIn(holiday.is_all_day, are_all_day)

        for closing_hours in closing_hours_set:
            assert isinstance(closing_hours, list)

    def test_closing_hours_are_empty(self):
        closing_hours_set = [x['closing_hours'] for x in self.response.data]
        for closing_hours in closing_hours_set:
            self.assertFalse(closing_hours)

    def test_closing_hours_present_in_response(self):
        ClosingHoursFactory.create_batch(size=2, holiday=self.holidays[0])
        ClosingHoursFactory(holiday=self.holidays[1])

        response = self.admin_client.get(self.endpoint)
        closing_hours_set = [x['closing_hours'] for x in response.data]

        assert len(closing_hours_set) == 2
        closing_hours_set_len_two = False
        closing_hours_set_len_one = False
        for i in closing_hours_set:
            if len(i) == 2:
                closing_hours_set_len_two = True
            if len(i) == 1:
                closing_hours_set_len_one = True
        self.assertTrue(all([closing_hours_set_len_one, closing_hours_set_len_two]))

    def test_permission_holiday_list(self):
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestHolidayPost(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.date_format = '%Y-%m-%d'

        cls.endpoint = reverse('api:holiday-list')


        cls.post_data = dict(
            date=datetime.now().date() + timedelta(days=1),
            name="holiday name",
            is_all_day=False,
            closing_hours=[
                dict(from_hour="0800", to_hour="1200"),
                dict(from_hour="1300", to_hour="1700")
            ]
        )

        cls.response = cls.admin_client.post(cls.endpoint, cls.post_data, format='json')

    def test_response_status_code(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_closing_hours_present_when_is_all_day_false(self):
        assert self.response.data['name'] == self.post_data['name']
        assert len(self.response.data['closing_hours']) == 2

    def test_cannot_add_closing_hours_when_is_all_day_true(self):
        post_data = copy.copy(self.post_data)
        post_data['is_all_day'] = True
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        self.assertErrorResponse(
            response,
            {
                "__all__": ['The "all day" holiday cannot have "closing hours"']
            }
        )

    def test_cannot_add_today_as_holiday(self):
        post_data = copy.copy(self.post_data)
        post_data['date'] = datetime.now().strftime(self.date_format)
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        self.assertErrorResponse(
            response,
            {
                "__all__": ['The field date must be in the future']
            }
        )

    def test_cannot_add_pastday_as_holiday(self):
        post_data = copy.copy(self.post_data)
        post_data['date'] = (datetime.now() - timedelta(days=1)).strftime(self.date_format)
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        self.assertErrorResponse(
            response,
            {
                "__all__": ['The field date must be in the future']
            }
        )

    def test_to_hour_is_required(self):
        post_data = copy.copy(self.post_data)
        post_data['closing_hours'] = [dict(from_hour='1000', )]

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(response, {'closing_hours': [{'to_hour': ['This field is required.']}]})

    def test_from_hour_is_required(self):
        post_data = copy.copy(self.post_data)
        post_data['closing_hours'] = [dict(to_hour='1000', )]

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(
            response,
            {'closing_hours': [{'from_hour': ['This field is required.']}]}
        )

    def test_closing_hours_are_required(self):
        post_data = copy.copy(self.post_data)
        post_data.pop('closing_hours')

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(
            response,
            {
                "__all__": ['The field "closing hours" is required when "is all day" is False.']
            }
        )

    def test_closing_hours_not_required(self):
        post_data = copy.copy(self.post_data)
        post_data.pop('closing_hours')
        post_data['is_all_day'] = True
        post_data['date'] = (timezone.now() + timedelta(days=2)).strftime(self.date_format)

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        assert response.status_code == status.HTTP_201_CREATED

    def test_holiday_already_exist(self):
        post_data = copy.copy(self.post_data)

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertErrorResponse(
            response,
            {
                "__all__": ['There is an existing holiday on the given date.']
            }
        )

    def test_permission_holiday_create(self):
        post_data = copy.copy(self.post_data)
        post_data['date'] = (timezone.now() + timedelta(days=3)).strftime(self.date_format)

        response = self.user_client.post(self.endpoint, post_data, format='json')
        self.assertEqual(response.status_code, 403)

        response = self.accounting_client.post(self.endpoint, post_data, format='json')
        self.assertEqual(response.status_code, 403)

        response = self.hr_client.post(self.endpoint, post_data, format='json')
        self.assertEqual(response.status_code, 403)

        response = self.backoffice_client.post(self.endpoint, post_data, format='json')
        self.assertEqual(response.status_code, 201)
        post_data['date'] = (timezone.now() + timedelta(days=4)).strftime(self.date_format)

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        self.assertEqual(response.status_code, 201)


class TestHolidayDetails(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.holiday = HolidayFactory(is_all_day=False)
        cls.closing_hours1 = ClosingHoursFactory(holiday=cls.holiday, from_hour='08:00', to_hour='12:00')
        cls.closing_hours2 = ClosingHoursFactory(holiday=cls.holiday, from_hour='13:00', to_hour='17:00')
        cls.details_endpoint = reverse('api:holiday-detail', kwargs={'pk': cls.holiday.pk})
        cls.response = cls.admin_client.get(cls.details_endpoint)

    def test_response_status_code(self):
        assert self.response.status_code == 200

    def test_closing_hours_length_is_correct(self):
        assert len(self.response.data['closing_hours']) == 2

    def test_permission_holiday_retrieve(self):
        response = self.user_client.get(self.details_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.details_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.details_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.details_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.details_endpoint)
        self.assertEqual(response.status_code, 200)


class TestHolidayPatch(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.holiday = HolidayFactory(is_all_day=False)
        cls.endpoint = reverse('api:holiday-detail', kwargs={
            'pk': cls.holiday.pk
        })
        cls.format = '%Y-%m-%d'
        cls.error_message = {'__all__': ['The field date must be in the future']}

        cls.patch_data = {
            "date": (datetime.now() + timedelta(days=3)).strftime(cls.format),
            "name": "holiday name",
            "is_all_day": False,
            "closing_hours": [
                {
                    "from_hour": "0800",
                    "to_hour": "1200"
                },
                {
                    "from_hour": "1300",
                    "to_hour": "1700"

                }
            ]
        }

        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format='json')
        cls.holiday.closing_hours.all().delete()

    def test_response_status_code(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_closing_hours_present_when_is_all_day_false(self):
        assert self.response.data['name'] == self.patch_data['name']
        assert len(self.response.data['closing_hours']) == 2

    def test_no_closing_hours_when_is_all_day_true(self):
        patch_data = dict(is_all_day=True, )
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert response.data['is_all_day'] == patch_data.get('is_all_day')
        assert response.data['closing_hours'] == []
        self.holiday.closing_hours.all().delete()

    def test_cannot_patch_today_as_holiday(self):
        patch_data = dict(date=datetime.now().strftime(self.format))
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')

        self.assertErrorResponse(response, self.error_message)

    def test_cannot_patch_pastday_as_holiday(self):
        patch_data = dict(date=(datetime.now() - timedelta(days=7)).strftime(self.format))
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        self.assertErrorResponse(response, self.error_message)

    def test_patch_future_date(self):
        patch_data = dict(date=(datetime.now() + timedelta(days=7)).strftime(self.format), )
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert response.data['date'] == patch_data.get('date')
        assert response.status_code == status.HTTP_200_OK
        self.holiday.closing_hours.all().delete()

    def test_patch_holiday_name(self):
        patch_data = dict(name='new holiday name', )
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert response.data['name'] == patch_data.get('name')
        assert response.status_code == status.HTTP_200_OK
        self.holiday.closing_hours.all().delete()

    def test_patch_closing_hours(self):
        patch_data = {
            "closing_hours": [
                {
                    "from_hour": "0900",
                    "to_hour": "1000"
                },
                {
                    "from_hour": "1200",
                    "to_hour": "1600"
                },
            ]
        }

        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert len(response.data['closing_hours']) == 2
        assert response.status_code == status.HTTP_200_OK
        self.holiday.closing_hours.all().delete()

    def test_cannot_patch_closing_hours_when_is_all_day_true(self):
        patch_data = {
            "is_all_day": True,
            "closing_hours": [
                {
                    "from_hour": "1000",
                    "to_hour": "1200"
                },

            ]
        }

        response = self.admin_client.patch(self.endpoint, patch_data, format='json')

        self.assertErrorResponse(response,
                                 {"__all__": ['The "all day" holiday cannot have "closing hours"']})

    def test_to_hour_is_required(self):
        patch_data = {
            "is_all_day": False,
            "closing_hours": [
                {
                    "from_hour": "1000",
                },

            ]
        }

        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        self.assertErrorResponse(
            response,
            {'closing_hours': [{'error': ['The field "to_hour" is required.']}]}
        )

    def test_from_hour_is_required(self):
        patch_data = dict(
            closing_hours=[
                dict(to_hour='1000', )
            ],
            is_all_day=False
        )
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        self.assertErrorResponse(
            response,
            {'closing_hours': [{'error': ['The field "from_hour" is required.']}]}
        )

    def test_permission_holiday_patch_not_used(self):
        response = self.user_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.holiday.closing_hours.all().delete()
        response = self.admin_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_permission_holiday_patch_used(self):
        hours = ClosingHoursFactory(holiday=self.holiday)
        response = self.backoffice_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't modify used Holiday"]
            }
        )
        hours.delete()


class TestHolidayDelete(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.holiday = HolidayFactory(is_all_day=False, date=timezone.now().date())
        cls.details_endpoint = reverse('api:holiday-detail', kwargs={
            'pk': cls.holiday.pk
        })

    def test_permissions_holiday_delete_not_used(self):
        response = self.user_client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.details_endpoint)
        self.assertEqual(response.status_code, 204)
        holiday = HolidayFactory(is_all_day=False)
        details_endpoint = reverse('api:holiday-detail', kwargs={
            'pk': holiday.pk
        })
        response = self.admin_client.delete(details_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_holiday_delete_used(self):
        holiday = HolidayFactory(is_all_day=False, date=timezone.now().date() + timedelta(days=1))
        details_endpoint = reverse('api:holiday-detail', kwargs={
            'pk': holiday.pk
        })
        ClosingHoursFactory(holiday=holiday)
        response = self.backoffice_client.delete(details_endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(details_endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used Holiday"]
            }
        )
