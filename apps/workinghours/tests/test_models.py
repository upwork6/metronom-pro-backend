from datetime import datetime, timedelta, date

from ddt import data, ddt, unpack
from django.core.exceptions import ValidationError
from django.test import TestCase, override_settings
from django.utils import timezone

from apps.backoffice.utils import get_metronom_setting
from apps.contracts.tests.factories import ContractFactory
from apps.metronom_commons.data import ABSENCE_REASONS, ABSENCE_TYPE, APPROVAL_STATUS
from apps.users.tests.factories import UserFactory
from apps.workinghours.models import Absence
from apps.workinghours.tests.factories import (AbsenceFactory, ClosingHoursFactory, FlexWorkFactory, HolidayFactory,
                                               WorkingHoursFactory, WorkYearFactory)
from apps.workinghours.tests.mixins import TestFlexWorkMixin
from apps.workinghours.utils import get_default_working_hours


class TestAbsenceBase(TestCase, TestFlexWorkMixin):
    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.flex_work = cls.get_full_day_mon_tue_wed_thu_flex_work()
        ContractFactory(user=cls.user, flex_model=cls.flex_work)

        HolidayFactory(name='Public Holiday', date=timezone.make_aware(datetime(2018, 3, 30)).date(), is_all_day=True)
        HolidayFactory(name='Public Holiday', date=timezone.make_aware(datetime(2018, 4, 2)).date(), is_all_day=True)


class TestAbsenceFullTime(TestAbsenceBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def test_absence_days(self):
        start_date = timezone.make_aware(datetime(2018, 3, 29)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 9)).date()

        absence = AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.FULL_DAY,
            end_date_number=ABSENCE_TYPE.FULL_DAY,
            user=self.user
        )

        assert absence.absence_days == 5.0

    def test_absence_same_day(self):
        start_date = timezone.make_aware(datetime(2018, 4, 3)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 3)).date()

        absence = AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.FULL_DAY,
            end_date_number=ABSENCE_TYPE.FULL_DAY,
            user=self.user
        )

        assert absence.absence_days == 1

    def test_cannot_absence_starts_on_holiday(self):
        start_date = timezone.make_aware(datetime(2018, 3, 30)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 9)).date()

        with self.assertRaisesMessage(ValidationError, 'Cannot start absence on a holiday.'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.FULL_DAY,
                end_date_number=ABSENCE_TYPE.FULL_DAY,
                user=self.user
            )

    def test_cannot_absence_end_on_holiday(self):
        start_date = timezone.make_aware(datetime(2018, 3, 29)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 2)).date()

        with self.assertRaisesMessage(ValidationError, 'Cannot end absence on a holiday.'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.FULL_DAY,
                end_date_number=ABSENCE_TYPE.FULL_DAY,
                user=self.user
            )

    def test_not_working_on_start_date_full_day(self):
        start_date = timezone.make_aware(datetime(2018, 1, 14)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()

        with self.assertRaisesMessage(ValidationError, 'Cannot start absence on a Non Working day.'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.FULL_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

    def test_not_working_on_start_date_half_day(self):
        start_date = timezone.make_aware(datetime(2018, 1, 14)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()
        with self.assertRaisesMessage(ValidationError, 'Cannot start absence on a Non Working day.'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

    def test_working_on_start_date_invalid_input(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()

        with self.assertRaisesMessage(ValidationError, '"0" is not a valid choice for "start_date_number".'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=0,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

    def test_working_on_start_date_full_day(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()
        absence = AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.FULL_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        assert absence.start_date_number == ABSENCE_TYPE.FULL_DAY

    def test_working_on_start_date_half_day(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()
        absence = AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        assert absence.start_date_number == ABSENCE_TYPE.HALF_DAY

    def test_not_working_on_end_date_half_day(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 28)).date()

        with self.assertRaisesMessage(ValidationError, 'Cannot end absence on a Non Working day.'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.FULL_DAY,
                user=self.user
            )

    def test_not_working_on_end_date_full_day(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 28)).date()
        with self.assertRaisesMessage(ValidationError, 'Cannot end absence on a Non Working day.'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.FULL_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

    def test_working_on_end_date_invalid_input(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()

        with self.assertRaisesMessage(ValidationError, '"0" is not a valid choice for "end_date_number".'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=0,
                user=self.user
            )

    def test_working_on_end_date_full_day(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()
        absence = AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.FULL_DAY,
            user=self.user
        )

        assert absence.end_date_number == ABSENCE_TYPE.FULL_DAY

    def test_working_on_end_date_half_day(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()
        absence = AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.FULL_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        assert absence.end_date_number == ABSENCE_TYPE.HALF_DAY

    def test_absence_start_date_overlap_existing_absence(self):
        start_date = timezone.make_aware(datetime(2018, 3, 29)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 9)).date()

        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        start_date = timezone.make_aware(datetime(2018, 4, 3)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 12)).date()

        with self.assertRaisesMessage(
                ValidationError, 'The "start_date" or "end_date" cannot overlap existing absence'
        ):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

    def test_absence_end_date_overlap_existing_absence(self):
        start_date = timezone.make_aware(datetime(2018, 3, 29)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 9)).date()

        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        start_date = timezone.make_aware(datetime(2018, 3, 28)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 4)).date()

        with self.assertRaisesMessage(
                ValidationError, 'The "start_date" or "end_date" cannot overlap existing absence'
        ):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

    def test_absence_fully_inside_existing_absence(self):
        start_date = timezone.make_aware(datetime(2018, 3, 29)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 9)).date()

        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        start_date = timezone.make_aware(datetime(2018, 4, 3)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 5)).date()

        with self.assertRaisesMessage(
                ValidationError, 'The "start_date" or "end_date" cannot overlap existing absence'
        ):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

    def test_absence_fully_outside_existing_absence(self):
        start_date = timezone.make_aware(datetime(2018, 4, 3)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 5)).date()

        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.FULL_DAY,
            user=self.user
        )

        start_date = timezone.make_aware(datetime(2018, 3, 29)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 9)).date()

        with self.assertRaisesMessage(
                ValidationError, 'The "start_date" or "end_date" cannot overlap existing absence'
        ):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.FULL_DAY,
                user=self.user
            )

    def test_two_half_absences_can_be_added_on_full_working_day(self):
        start_date = timezone.make_aware(datetime(2018, 4, 4)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 5)).date()

        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        start_date = timezone.make_aware(datetime(2018, 4, 3)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 4)).date()

        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

    def test_two_half_day_absences_can_be_added_on_full_working_day_reverse(self):
        start_date = timezone.make_aware(datetime(2018, 4, 3)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 4)).date()

        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        start_date = timezone.make_aware(datetime(2018, 4, 4)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 5)).date()

        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

    def test_half_day_absence_cannot_be_added_on_absence_ending_full_working_day(self):
        start_date = timezone.make_aware(datetime(2018, 4, 3)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 4)).date()

        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.FULL_DAY,
            user=self.user
        )

        start_date = timezone.make_aware(datetime(2018, 4, 4)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 5)).date()

        with self.assertRaisesMessage(
                ValidationError, 'The "start_date" or "end_date" cannot overlap existing absence'
        ):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

    def test_half_day_absence_cannot_be_added_on_absence_starting_full_working_day(self):
        start_date = timezone.make_aware(datetime(2018, 4, 4)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 5)).date()

        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.FULL_DAY,
            end_date_number=ABSENCE_TYPE.FULL_DAY,
            user=self.user
        )

        start_date = timezone.make_aware(datetime(2018, 4, 3)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 4)).date()

        with self.assertRaisesMessage(
                ValidationError, 'The "start_date" or "end_date" cannot overlap existing absence'
        ):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

    def test_cannot_add_two_identical_absences(self):
        start_date = timezone.make_aware(datetime(2018, 4, 3)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 4)).date()

        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        start_date = timezone.make_aware(datetime(2018, 4, 3)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 4)).date()

        with self.assertRaisesMessage(
                ValidationError, 'The "start_date" or "end_date" cannot overlap existing absence'
        ):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )


class TestAbsenceHalfTime(TestCase, TestFlexWorkMixin):
    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.flex_work = cls.get_half_day_full_week_flex_work()
        ContractFactory(user=cls.user, flex_model=cls.flex_work)

        HolidayFactory(name='Public Holiday', date=datetime(2018, 3, 30).date(), is_all_day=True)
        HolidayFactory(name='Public Holiday', date=datetime(2018, 4, 2).date(), is_all_day=True)

    def test_absence_days(self):
        start_date = timezone.make_aware(datetime(2018, 3, 29)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 9)).date()

        absence = AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        assert absence.absence_days == 3.0

    def test_absence_same_day(self):
        start_date = timezone.make_aware(datetime(2018, 4, 3)).date()
        end_date = timezone.make_aware(datetime(2018, 4, 3)).date()

        absence = AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        assert absence.absence_days == 0.5

    def test_not_working_on_start_date_invalid_input(self):
        start_date = timezone.make_aware(datetime(2018, 1, 14)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()

        with self.assertRaisesMessage(ValidationError, 'Cannot start absence on a Non Working day.'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.FULL_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

        with self.assertRaisesMessage(ValidationError, 'Cannot start absence on a Non Working day.'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

    def test_working_on_start_date_invalid_input(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()

        with self.assertRaisesMessage(ValidationError, '"0" is not a valid choice for "start_date_number".'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=0,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

        with self.assertRaisesMessage(ValidationError, '"Full day" is not a valid choice for a Half Working day.'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.FULL_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

    def test_working_on_start_date_valid_input(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()

        absence = AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        assert absence.start_date_number == ABSENCE_TYPE.HALF_DAY

    def test_not_working_on_end_date_invalid_input(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 28)).date()

        with self.assertRaisesMessage(ValidationError, 'Cannot end absence on a Non Working day.'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.FULL_DAY,
                user=self.user
            )

        with self.assertRaisesMessage(ValidationError, 'Cannot end absence on a Non Working day.'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.HALF_DAY,
                user=self.user
            )

    def test_working_on_end_date_invalid_input(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()

        with self.assertRaisesMessage(ValidationError, '"0" is not a valid choice for "end_date_number".'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=0,
                user=self.user
            )

        with self.assertRaisesMessage(ValidationError, '"Full day" is not a valid choice for a Half Working day.'):
            AbsenceFactory(
                reason=ABSENCE_REASONS.VACATION,
                start_date=start_date,
                end_date=end_date,
                start_date_number=ABSENCE_TYPE.HALF_DAY,
                end_date_number=ABSENCE_TYPE.FULL_DAY,
                user=self.user
            )

    def test_working_on_end_date_valid_input(self):
        start_date = timezone.make_aware(datetime(2018, 1, 15)).date()
        end_date = timezone.make_aware(datetime(2018, 1, 29)).date()

        absence = AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        assert absence.start_date_number == ABSENCE_TYPE.HALF_DAY


class TestAbsenceDays(TestCase, TestFlexWorkMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = UserFactory()

    def test_absence_days_1(self):
        flex_work = self.get_full_day_wed_thu_half_day_fri_flex_work()
        ContractFactory(user=self.user, flex_model=flex_work)

        start_date = timezone.make_aware(datetime(2018, 2, 14)).date()
        end_date = timezone.make_aware(datetime(2018, 2, 16)).date()

        absence = AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.FULL_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        assert absence.absence_days == 2.5

    def test_absence_days_2(self):
        flex_work = self.get_full_day_mon_tue_wed_thu_half_day_fri_flex_work()
        ContractFactory(user=self.user, flex_model=flex_work)

        start_date = timezone.make_aware(datetime(2018, 5, 4)).date()
        end_date = timezone.make_aware(datetime(2018, 5, 18)).date()

        absence = AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION,
            start_date=start_date,
            end_date=end_date,
            start_date_number=ABSENCE_TYPE.HALF_DAY,
            end_date_number=ABSENCE_TYPE.HALF_DAY,
            user=self.user
        )

        assert absence.absence_days == 9.5


class TestYearlyAbsencesBase(TestAbsenceBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.year = 2018
        HolidayFactory(
            name='Christmas Eve', date=timezone.make_aware(datetime(cls.year, 12, 24)).date(), is_all_day=True
        )
        HolidayFactory(
            name='Christmas Day', date=timezone.make_aware(datetime(cls.year, 12, 25)).date(), is_all_day=True
        )
        HolidayFactory(
            name='Day After Christmas', date=timezone.make_aware(datetime(cls.year, 12, 26)).date(), is_all_day=True
        )
        HolidayFactory(
            name='New Year\'s Eve', date=timezone.make_aware(datetime(cls.year, 12, 31)).date(), is_all_day=True
        )


class TestYearlyAbsences(TestYearlyAbsencesBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.year = 2018
        start_date = timezone.make_aware(datetime(cls.year, 1, 15)).date()
        end_date = timezone.make_aware(datetime(cls.year, 1, 15)).date()

        AbsenceFactory(
            reason=ABSENCE_REASONS.EDUCATION, start_date=start_date, end_date=end_date, user=cls.user,
            start_date_number=ABSENCE_TYPE.HALF_DAY, end_date_number=ABSENCE_TYPE.HALF_DAY
        )

        start_date = timezone.make_aware(datetime(cls.year, 12, 20)).date()
        end_date = timezone.make_aware(datetime(cls.year + 1, 1, 8)).date()
        AbsenceFactory(reason=ABSENCE_REASONS.MILITARY, start_date=start_date, end_date=end_date, user=cls.user)

        start_date = timezone.make_aware(datetime(cls.year, 1, 18)).date()
        end_date = timezone.make_aware(datetime(cls.year, 1, 22)).date()
        AbsenceFactory(reason=ABSENCE_REASONS.ACCIDENT, start_date=start_date, end_date=end_date, user=cls.user)

        start_date = timezone.make_aware(datetime(cls.year - 1, 12, 19)).date()
        end_date = timezone.make_aware(datetime(cls.year, 1, 2)).date()
        AbsenceFactory(reason=ABSENCE_REASONS.MOTHERHOOD, start_date=start_date, end_date=end_date, user=cls.user)

    def test_non_vacation_absences(self):
        total_absences = Absence.objects.get_non_vacation_absences(user=self.user, year=self.year)
        assert total_absences == 6.5

    def test_vacation_days_used(self):
        vacation_days_used = Absence.objects.get_vacation_days_used(user=self.user, year=self.year)
        assert vacation_days_used == 0

    def test_vacation_days_requested(self):
        vacation_days_requested = Absence.objects.get_vacation_days_requested(user=self.user, year=self.year)
        assert vacation_days_requested == 0

    def test_no_absences_for_user(self):
        user_absences = Absence.objects.get_non_vacation_absences(user=UserFactory(), year=self.year)
        assert user_absences == 0


class TestYearlyVacations(TestYearlyAbsencesBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.year = 2018

        start_date = timezone.make_aware(datetime(cls.year, 1, 15)).date()
        end_date = timezone.make_aware(datetime(cls.year, 1, 15)).date()
        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION, start_date=start_date, end_date=end_date, user=cls.user,
            start_date_number=ABSENCE_TYPE.HALF_DAY, end_date_number=ABSENCE_TYPE.HALF_DAY
        )

        start_date = timezone.make_aware(datetime(cls.year, 12, 20)).date()
        end_date = timezone.make_aware(datetime(cls.year + 1, 1, 8)).date()
        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION, start_date=start_date, end_date=end_date, user=cls.user,
            approval_status=APPROVAL_STATUS.APPROVED
        )

        start_date = timezone.make_aware(datetime(cls.year, 1, 18)).date()
        end_date = timezone.make_aware(datetime(cls.year, 1, 22)).date()
        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION, start_date=start_date, end_date=end_date, user=cls.user
        )

        start_date = timezone.make_aware(datetime(cls.year - 1, 12, 19)).date()
        end_date = timezone.make_aware(datetime(cls.year, 1, 2)).date()
        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION, start_date=start_date, end_date=end_date, user=cls.user,
            approval_status=APPROVAL_STATUS.APPROVED
        )

    def test_vacation_days_used(self):
        vacation_days_used = Absence.objects.get_vacation_days_used(user=self.user, year=self.year)
        assert vacation_days_used == 4

    def test_vacation_days_requested(self):
        vacation_days_requested = Absence.objects.get_vacation_days_requested(user=self.user, year=self.year)
        assert vacation_days_requested == 2.5

    def test_user_absences(self):
        user_absences = Absence.objects.get_user_absences(user=self.user, year=self.year)
        assert user_absences == 6.5

    def test_no_absences_for_user(self):
        user_absences = Absence.objects.get_user_absences(user=UserFactory(), year=self.year)
        assert user_absences == 0


class TestAllDayAbsence(TestAbsenceBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.upcoming_monday = timezone.now() + timedelta(days=-timezone.now().weekday(), weeks=1)

    def test_is_all_day_absence_valid(self):
        absence = AbsenceFactory(
            start_date=self.upcoming_monday.date(), end_date=self.upcoming_monday.date(),
            reason=ABSENCE_REASONS.VACATION, is_all_day=True, note='all day event', user=self.user
        )

        assert absence.is_all_day
        assert absence.start_date == absence.end_date

    def test_is_all_day_absence_invalid(self):
        upcoming_wednesday = self.upcoming_monday + timedelta(days=2)
        with self.assertRaisesMessage(ValidationError,
                                      'The "start date" and "end date" must match for "is_all_day" event'):
            AbsenceFactory(
                start_date=self.upcoming_monday.date(), end_date=upcoming_wednesday.date(),
                reason=ABSENCE_REASONS.VACATION, is_all_day=True, note='all day event', user=self.user
            )


@ddt
class TestFlexWorkProperties(TestCase, TestFlexWorkMixin):
    def setUp(self):
        self.flex_work = FlexWorkFactory(given_name='Full work week', is_archived=False)

    def calculate_percentage(self):
        return '{:.1%}'.format(self.flex_work.hours / get_default_working_hours())

    def test_name_is_correct(self):
        assert self.flex_work.name == 'Full work week'

    def test_name_is_correct_when_archived(self):
        self.flex_work.is_archived = True
        assert self.flex_work.name == 'Full work week (archived)'

    def test_name_is_correct_with_percentage(self):
        self.working_hours1 = WorkingHoursFactory(
            flex_work=self.flex_work, weekday=1, from_hour='04:00', to_hour='06:30'
        )
        self.working_hours2 = WorkingHoursFactory(
            flex_work=self.flex_work, weekday=3, from_hour='02:00', to_hour='05:00'
        )
        percentage = self.calculate_percentage()
        assert self.flex_work.name[:5] == percentage
        assert self.flex_work.percentage == percentage
        assert self.flex_work.name[:5] == self.flex_work.percentage

    def test_percentage_is_correct(self):
        assert self.flex_work.percentage == self.calculate_percentage()
        WorkingHoursFactory(
            flex_work=self.flex_work, weekday=1, from_hour='04:00', to_hour='06:30'
        )

        assert self.flex_work.percentage == self.calculate_percentage()

        WorkingHoursFactory(
            flex_work=self.flex_work, weekday=3, from_hour='02:00', to_hour='06:30'
        )

        assert self.flex_work.percentage == self.calculate_percentage()

    def test_percentage_is_correct_with_more_hours_added(self):
        WorkingHoursFactory(
            weekday=1, from_hour='08:00', to_hour='12:00', flex_work=self.flex_work
        )
        WorkingHoursFactory(
            weekday=2, from_hour='08:00', to_hour='12:00', flex_work=self.flex_work
        )
        WorkingHoursFactory(
            weekday=3, from_hour='08:00', to_hour='12:00', flex_work=self.flex_work
        )
        WorkingHoursFactory(
            weekday=4, from_hour='08:00', to_hour='12:00', flex_work=self.flex_work
        )
        WorkingHoursFactory(
            weekday=1, from_hour='13:00', to_hour='17:00', flex_work=self.flex_work
        )
        WorkingHoursFactory(
            weekday=2, from_hour='13:00', to_hour='17:00', flex_work=self.flex_work
        )
        WorkingHoursFactory(
            weekday=3, from_hour='13:00', to_hour='17:00', flex_work=self.flex_work
        )
        WorkingHoursFactory(
            weekday=4, from_hour='13:00', to_hour='17:00', flex_work=self.flex_work
        )
        WorkingHoursFactory(
            weekday=5, from_hour='08:00', to_hour='11:00', flex_work=self.flex_work
        )
        assert self.flex_work.percentage == self.calculate_percentage()

    @override_settings(
        METRONOM_SETTINGS={
            'DEFAULT_AGENCY_WORKING_HOURS': {'value': 0}
        }
    )
    def test_percentage_is_correct_when_setting_unavailable(self):
        default_agency_working_hours = get_metronom_setting('DEFAULT_AGENCY_WORKING_HOURS')
        assert default_agency_working_hours == 0
        WorkingHoursFactory(
            flex_work=self.flex_work, weekday=1, from_hour='04:00', to_hour='06:30'
        )

        assert self.flex_work.percentage == self.calculate_percentage()

        default_agency_working_hours = get_metronom_setting('DEFAULT_AGENCY_WORKING_HOURS')
        assert default_agency_working_hours == 0

    def test_description_1(self):
        test_description = 'Mo, Tu, We, Th, Fr 08-12 + 13-17'
        flex_work = self.get_full_day_full_week_flex_work()

        assert flex_work.description == test_description

    def test_description_2(self):
        test_description = 'Mo, We 10-13 + 14-17'
        flex_work = self.get_mon_wed_10_13__14_17_flex_work()

        assert flex_work.description == test_description

    def test_description_3(self):
        test_description = 'Mo 10-14, We, Fr 10-14 + 15-18'
        flex_work = self.get_mon_10_14_wed_to_fri_10_14__15_18()

        assert flex_work.description == test_description

    def test_hours_1(self):
        assert self.flex_work.hours == 0.0

    @unpack
    @data(
        {'size': 1, 'from_hour': '08:00', 'to_hour': '12:30', 'expected_result': 4.5},
        {'size': 3, 'from_hour': '13:00', 'to_hour': '17:00', 'expected_result': 12.0},
        {'size': 5, 'from_hour': '10:00', 'to_hour': '13:00', 'expected_result': 15.0},
    )
    def test_hours_calculations_are_correct(self, size, from_hour, to_hour, expected_result):
        WorkingHoursFactory.create_batch(
            size=size, from_hour=from_hour, to_hour=to_hour, flex_work=self.flex_work
        )

        assert self.flex_work.hours == expected_result

    def test_to_hour_less_than_from_hour(self):
        self.assertRaises(ValidationError, lambda: WorkingHoursFactory(
            from_hour='12:00', to_hour='10:00', flex_work=self.flex_work
        ))

    @unpack
    @data(
        {'size': 3, 'from_hour': '', 'to_hour': ''},
    )
    def test_missing_working_hours(self, size, from_hour, to_hour):
        self.assertRaises(ValidationError, lambda: WorkingHoursFactory.create_batch(
            size=size, from_hour=from_hour, to_hour=to_hour, flex_work=self.flex_work
        ))

    def test_overlapping_hours(self):
        WorkingHoursFactory(
            weekday=1, from_hour='08:00', to_hour='12:30', flex_work=self.flex_work
        )

        self.assertRaises(ValidationError, lambda: WorkingHoursFactory(
            weekday=1, from_hour='10:00', to_hour='13:00', flex_work=self.flex_work
        ))
        self.assertRaises(ValidationError, lambda: WorkingHoursFactory(
            weekday=1, from_hour='11:00', to_hour='17:00', flex_work=self.flex_work
        ))


class TestHolidays(TestCase):
    def setUp(self):
        self.holiday = HolidayFactory(name='test holiday', date=timezone.now().date(), is_all_day=False)

    def test_to_hour_less_than_from_hour(self):
        with self.assertRaisesMessage(
                ValidationError, 'The "to hour" cannot be earlier than the "from hour".'
        ):
            ClosingHoursFactory(
                from_hour='12:00', to_hour='10:00', holiday=self.holiday
            )

    def test_is_all_day_false(self):
        closing_hours = ClosingHoursFactory(
            from_hour='08:00', to_hour='12:00', holiday=self.holiday
        )

        self.assertIsNotNone(closing_hours.from_hour)
        self.assertIsNotNone(closing_hours.to_hour)
        self.assertFalse(closing_hours.holiday.is_all_day)

    def test_is_all_day_true(self):
        self.holiday.is_all_day = True

        with self.assertRaisesMessage(
                ValidationError, 'The "all day" holiday cannot have "closing hours"'
        ):
            ClosingHoursFactory(
                from_hour='08:00', to_hour='12:00', holiday=self.holiday
            )

    def test_hours_property(self):
        ClosingHoursFactory(
            from_hour='08:00', to_hour='12:00', holiday=self.holiday
        )

        assert self.holiday.hours == 4

    def test_hours_property_for_all_day_holiday(self):
        self.holiday.is_all_day = True
        ClosingHoursFactory(holiday=self.holiday)
        work_day_length_in_hours = get_metronom_setting('WORKDAY_LENGTH_IN_HOURS')

        assert self.holiday.hours == work_day_length_in_hours

    def test_holiday_already_exists(self):
        with self.assertRaisesMessage(
                ValidationError, 'There is an existing holiday on the given date.'
        ):
            HolidayFactory(date=timezone.now().date())


class TestWorkYearBase(TestCase, TestFlexWorkMixin):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = UserFactory()
        cls.flex_work = cls.get_full_day_full_week_flex_work()


@ddt
class TestWorkYearVacationDaysOverall(TestWorkYearBase):
    year = timezone.now().year

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    @unpack
    @data(
        {
            'contracts': [
                {'start_date': date(year, 1, 1), 'end_date': date(year, 6, 30), 'vacation_days': 20},
                {'start_date': date(year, 7, 1), 'end_date': date(year, 12, 31), 'vacation_days': 16}
            ], 'vacation_days_moved_from_last_year': 2, 'expected_result': 20
        },
        {
            'contracts': [
                {'start_date': date(year - 1, 10, 1), 'end_date': date(year, 3, 31), 'vacation_days': 20},
                {'start_date': date(year, 10, 1), 'end_date': None, 'vacation_days': 16}
            ], 'vacation_days_moved_from_last_year': 4, 'expected_result': 13
        },
        {
            'contracts': [
                {'start_date': date(year, 4, 1), 'end_date': None, 'vacation_days': 20}
            ], 'vacation_days_moved_from_last_year': 6, 'expected_result': 21
        },
        {
            'contracts': [
                {'start_date': date(year - 1, 7, 1), 'end_date': None, 'vacation_days': 16}
            ], 'vacation_days_moved_from_last_year': 8, 'expected_result': 24
        },
        {
            'contracts': [], 'vacation_days_moved_from_last_year': 8, 'expected_result': 8
        }
    )
    def test_vacation_days_overall(self, contracts, vacation_days_moved_from_last_year, expected_result):
        for contract in contracts:
            ContractFactory(
                user=self.user,
                start_date=contract.get('start_date'),
                end_date=contract.get('end_date'),
                flex_model=self.flex_work,
                vacation_days=contract.get('vacation_days')
            )

        work_year = WorkYearFactory(
            vacation_days_moved_from_last_year=vacation_days_moved_from_last_year, user=self.user, year=self.year
        )
        assert work_year.vacation_days_overall == expected_result


@ddt
class TestWorkYearVacationProperties(TestWorkYearBase):
    year = timezone.now().year

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        ContractFactory(
            user=cls.user,
            start_date=date(cls.year - 1, 7, 1),
            end_date=None,
            flex_model=cls.flex_work,
            vacation_days=20
        )

        HolidayFactory(
            name='Christmas Eve', date=timezone.make_aware(datetime(cls.year, 12, 24)).date(), is_all_day=True
        )
        HolidayFactory(
            name='Christmas Day', date=timezone.make_aware(datetime(cls.year, 12, 25)).date(), is_all_day=True
        )
        HolidayFactory(
            name='Day After Christmas', date=timezone.make_aware(datetime(cls.year, 12, 26)).date(), is_all_day=True
        )
        HolidayFactory(
            name='New Year\'s Eve', date=timezone.make_aware(datetime(cls.year, 12, 31)).date(), is_all_day=True
        )

        start_date = timezone.make_aware(datetime(cls.year, 1, 15)).date()
        end_date = timezone.make_aware(datetime(cls.year, 1, 15)).date()
        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION, start_date=start_date, end_date=end_date, user=cls.user,
            start_date_number=ABSENCE_TYPE.HALF_DAY, end_date_number=ABSENCE_TYPE.HALF_DAY
        )

        start_date = timezone.make_aware(datetime(cls.year, 12, 20)).date()
        end_date = timezone.make_aware(datetime(cls.year + 1, 1, 8)).date()
        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION, start_date=start_date, end_date=end_date, user=cls.user,
            approval_status=APPROVAL_STATUS.APPROVED
        )

        start_date = timezone.make_aware(datetime(cls.year, 1, 18)).date()
        end_date = timezone.make_aware(datetime(cls.year, 1, 22)).date()
        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION, start_date=start_date, end_date=end_date, user=cls.user
        )

        start_date = timezone.make_aware(datetime(cls.year - 1, 12, 19)).date()
        end_date = timezone.make_aware(datetime(cls.year, 1, 2)).date()
        AbsenceFactory(
            reason=ABSENCE_REASONS.VACATION, start_date=start_date, end_date=end_date, user=cls.user,
            approval_status=APPROVAL_STATUS.APPROVED
        )

        cls.work_year = WorkYearFactory(
            vacation_days_moved_from_last_year=2, user=cls.user, year=cls.year
        )

    def test_vacation_days_overall(self):
        assert self.work_year.vacation_days_overall == 22

    def test_vacation_days_used(self):
        assert self.work_year.vacation_days_used == 6

    def test_vacation_days_requested(self):
        assert self.work_year.vacation_days_requested == 3.5

    def test_vacation_days_available(self):
        assert self.work_year.vacation_days_available == 12.5

    # TODO: test for half day holiday
