import copy

from django.urls import reverse
from rest_framework import status

from apps.contracts.tests.factories import ContractFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.workinghours.tests.factories import FlexWorkFactory, WorkingHoursFactory
from apps.workinghours.utils import get_default_working_hours


class TestFlexWorkList(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.flex_works = FlexWorkFactory.create_batch(2)
        cls.list_endpoint = reverse('api:flexwork-list')
        cls.response = cls.admin_client.get(cls.list_endpoint)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_data_length_is_correct(self):
        assert len(self.response.data) == 2

    def test_response_data_is_correct_flexwork(self):
        names = [x['name'] for x in self.response.data]
        percentages = [x['percentage'] for x in self.response.data]
        descriptions = [x['description'] for x in self.response.data]
        archives = [x['is_archived'] for x in self.response.data]
        working_hours_set = [x['working_hours'] for x in self.response.data]
        weekday_names = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
        self.assertTrue('created_at' in self.response.data[0])
        for flex_work in self.flex_works:
            self.assertIn(flex_work.name, names)
            self.assertIn(flex_work.percentage, percentages)
            self.assertIn(flex_work.description, descriptions)
            self.assertIn(flex_work.is_archived, archives)

        for working_hours in working_hours_set:
            weekdays = working_hours.keys()
            assert all(weekday in weekday_names for weekday in weekdays)
            assert isinstance(working_hours, dict)

    def test_working_hours_are_empty(self):
        working_hours_set = [x['working_hours'] for x in self.response.data]

        for working_hours in working_hours_set:
            self.assertFalse(any(working_hours.keys()))

    def test_working_hours_present_in_response(self):
        # create two working hours records for monday & one for wednesday
        WorkingHoursFactory(flex_work=self.flex_works[0], weekday=1, from_hour='08:00', to_hour='12:00')
        WorkingHoursFactory(flex_work=self.flex_works[0], weekday=1, from_hour='13:00', to_hour='17:00')
        WorkingHoursFactory(flex_work=self.flex_works[1], weekday=3, from_hour='08:00', to_hour='12:00')

        response = self.admin_client.get(self.list_endpoint)
        working_hours_set = [x['working_hours'] for x in response.data]

        assert len(working_hours_set) == 2
        day_with_monday = [x for x in working_hours_set if 'monday' in x]
        day_with_wednesday = [x for x in working_hours_set if 'wednesday' in x]
        assert len(day_with_monday[0]['monday']) == 2
        assert len(day_with_wednesday[0]['wednesday']) == 1

    def test_permission_flexwork_list(self):
        response = self.user_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)


class TestFlexWorkCreate(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.flex_works = FlexWorkFactory.create_batch(2)
        cls.endpoint = reverse('api:flexwork-list')
        cls.post_data = dict(
            given_name='Test given name',
            working_hours=dict(
                monday=[
                    dict(from_hour='0800', to_hour='1200'),
                    dict(from_hour='1300', to_hour='1700'),
                ],
                friday=[
                    dict(from_hour='0800', to_hour='1200')
                ]
            ),
            is_archived=False
        )
        cls.response = cls.admin_client.post(cls.endpoint, cls.post_data, format='json')

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_response_given_name(self):
        assert self.response.data['given_name'] == self.post_data['given_name']

    def test_response_working_hours_length(self):
        assert len(self.response.data['working_hours']) == 2

    def test_working_hours_wrong_type(self):
        post_data = dict(
            given_name='XYZ',
            working_hours=[dict(from_hour='1245', to_hour='1545', weekday=1)]
        )

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(
            response,
            {'__all__': ['Invalid data. Expected a dictionary, but got list.']}
        )

    def test_working_hours_wrong_times(self):
        post_data = dict(
            given_name='XYZ',
            working_hours=dict(
                friday=[dict(from_hour='54000800', to_hour='abcd')]
            )
        )
        response = self.admin_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(
            response,
            {'working_hours': [{
                'from_hour': ['Time has wrong format. Use one of these formats instead: hhmm.'],
                'to_hour': ['Time has wrong format. Use one of these formats instead: hhmm.']
            }]}
        )

    def test_working_hours_wrong_weekday(self):
        post_data = dict(
            given_name='XYZ',
            working_hours=dict(
                mon=[]
            )
        )
        response = self.admin_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(
            response,
            {'__all__': ['Invalid data. Expected a valid weekday.']}
        )

    def test_working_hours_empty_list(self):
        post_data = dict(
            given_name='XYZ',
            working_hours=[]
        )
        response = self.admin_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(
            response,
            {"working_hours": ["This field is required."]}
        )

    def test_working_hours_required(self):
        post_data = dict(
            given_name='XYZ'
        )
        response = self.admin_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(
            response,
            {"working_hours": ["This field is required."]}
        )

    def test_post(self):
        response = self.admin_client.post(self.endpoint, self.post_data, format='json')
        assert response.status_code == 201
        assert response.data['given_name'] == self.post_data['given_name']
        assert len(response.data['working_hours']) == 2

    def test_working_hours_empty(self):
        post_data = dict(
            given_name='XYZ',
            working_hours=''
        )

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(
            response,
            {"working_hours": ["This field is required."]}
        )

    def test_permission_flexwork_create(self):
        post_data = copy.copy(self.post_data)
        response = self.user_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)


class TestFlexWorkDetails(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.flex_work = FlexWorkFactory(given_name='Flexwork name')
        cls.working_hours1 = WorkingHoursFactory(
            flex_work=cls.flex_work, weekday=1, from_hour='04:00', to_hour='06:30'
        )
        cls.working_hours2 = WorkingHoursFactory(
            flex_work=cls.flex_work, weekday=3, from_hour='02:00', to_hour='05:00'
        )

        cls.details_endpoint = reverse('api:flexwork-detail', kwargs={'pk': cls.flex_work.id})
        cls.response = cls.admin_client.get(cls.details_endpoint)

    def calculate_percentage(self):
        return '{:.1%}'.format(self.flex_work.hours / get_default_working_hours())

    def test_response_status_code(self):
        assert self.response.status_code == 200

    def test_working_hours_length_is_correct(self):
        assert len(self.response.data['working_hours']) == 2

    def test_description_is_correct(self):
        assert self.response.data['description'] == self.flex_work.description
        assert self.response.data['description'] == 'Mo 04-06.30, We 02-05'

    def test_percentage_is_correct(self):
        assert self.response.data['percentage'] == self.flex_work.percentage
        assert self.response.data['percentage'] == self.calculate_percentage()

    def test_total_hours_are_correct(self):
        self.flex_work.refresh_from_db()
        assert self.response.data['hours'] == self.flex_work.hours
        assert self.response.data['hours'] == 5.5

    def test_nice_name_is_correct(self):
        assert self.response.data['name'] == self.flex_work.name
        assert self.response.data['name'] == f'{self.calculate_percentage()} Flexwork name'

    def test_nice_name_when_archived_is_correct(self):
        flex_work = FlexWorkFactory(given_name='Flexwork name', is_archived=True)
        details_endpoint = reverse('api:flexwork-detail', kwargs={'pk': flex_work.id})
        response = self.admin_client.get(details_endpoint)
        assert response.data['name'] == flex_work.name
        assert response.data['name'] == 'Flexwork name (archived)'

    def test_permission_flexwork_retrieve(self):
        response = self.user_client.get(self.details_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.details_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.details_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.details_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.details_endpoint)
        self.assertEqual(response.status_code, 200)


class TestFlexWorkPatch(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.flex_work = FlexWorkFactory(given_name='Flexwork name')
        cls.details_endpoint = reverse('api:flexwork-detail', kwargs={
            'pk': cls.flex_work.id
        })

    def test_patch_is_ok(self):
        patch_data = {
            'given_name': 'Patch given name',
            'is_archived': False
        }
        response = self.admin_client.patch(self.details_endpoint, patch_data)
        assert response.status_code == 200

    def test_patch_given_name(self):
        patch_data = {
            'given_name': 'Patch given name',
        }
        response = self.admin_client.patch(self.details_endpoint, patch_data)

        assert response.data['given_name'] == patch_data['given_name']
        assert response.data['given_name'] != self.flex_work.given_name
        self.flex_work.refresh_from_db()
        assert response.data['given_name'] == self.flex_work.given_name

    def test_patch_is_archived(self):
        patch_data = {
            'is_archived': True,
        }
        response = self.admin_client.patch(self.details_endpoint, patch_data)

        assert response.data['is_archived'] == patch_data['is_archived']
        self.flex_work.refresh_from_db()
        assert response.data['is_archived'] == self.flex_work.is_archived

    def test_cant_patch_working_hours(self):
        patch_data = dict(
            given_name='Test given name',
            working_hours=dict(
                monday=[
                    dict(from_hour='0800', to_hour='1200'),
                    dict(from_hour='1300', to_hour='1700'),
                ],
                friday=[
                    dict(from_hour='0800', to_hour='1200')
                ]
            ),
            is_archived=True
        )
        response = self.admin_client.patch(self.details_endpoint, patch_data, format='json')
        self.assertErrorResponse(
            response,
            {
                "__all__": ['The update of field "working_hours" is not allowed.']
            }
        )

    def test_permission_flexwork_patch_not_used(self):
        patch_data = {
            'given_name': 'Patch given name',
        }
        response = self.user_client.patch(self.details_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.details_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.details_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.details_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.details_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permission_flexwork_patch_used(self):
        patch_data = {
            'given_name': 'Patch given name',
        }
        ContractFactory(flex_model=self.flex_work, user=self.test_admin)
        response = self.backoffice_client.patch(self.details_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.patch(self.details_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't modify used FlexWork"]
            }
        )
