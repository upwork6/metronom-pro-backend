from datetime import timedelta

from apps.backoffice.utils import get_metronom_setting
from apps.metronom_commons.data import ABSENCE_TYPE, APPROVAL_STATUS


def get_default_working_hours():
    try:
        default_value = get_metronom_setting('DEFAULT_AGENCY_WORKING_HOURS')
        return default_value if default_value > 0 else 35
    except ValueError:
        return 35


def get_formatted_working_hours(item):
    time_format = '%H.%M'
    from_time = str(item.from_hour.strftime(time_format))[:2] if str(
        item.from_hour.strftime(time_format)
    )[-2:] == '00' else item.from_hour.strftime(time_format)
    to_time = str(item.to_hour.strftime(time_format))[:2] if str(
        item.to_hour.strftime(time_format)
    )[-2:] == '00' else item.to_hour.strftime(time_format)

    return f'{from_time}-{to_time}'


def get_absence_days(absence, start_date, end_date):
    from apps.workinghours.models import Holiday

    absence_day_count = 0
    for i in range((end_date - start_date).days + 1):
        current_date = start_date + timedelta(days=i)

        if Holiday.objects.filter(date=current_date).exists():
            continue

        absence_type = get_absence_type_for_current_date(absence, current_date)
        if absence_type:
            absence_day_count += absence_type

    return absence_day_count


def get_absence_hours(current_date):
    from apps.workinghours.models import Absence, Holiday
    hours = 0
    holiday = Holiday.objects.filter(date=current_date).first()

    if holiday:
        if holiday.is_all_day:
            return holiday.hours
        else:
            hours = holiday.hours

    work_day_length_in_hours = int(get_metronom_setting('WORKDAY_LENGTH_IN_HOURS'))
    absence = Absence.objects.filter(
        start_date__lte=current_date, end_date__gte=current_date, approval_status=APPROVAL_STATUS.APPROVED
    ).first()

    if absence:
        absence_type = get_absence_type_for_current_date(absence, current_date)
        hours += absence_type * work_day_length_in_hours

    return hours


def get_absence_type_for_current_date(absence, current_date):
    working_hours = absence.get_working_hours_for_date(current_date)
    if len(working_hours) == 1:
        return ABSENCE_TYPE.HALF_DAY
    if len(working_hours) >= 2:
        if current_date == absence.start_date and float(absence.start_date_number) == ABSENCE_TYPE.HALF_DAY:
            return ABSENCE_TYPE.HALF_DAY
        elif current_date == absence.end_date and float(absence.end_date_number) == ABSENCE_TYPE.HALF_DAY:
            return ABSENCE_TYPE.HALF_DAY
        else:
            return ABSENCE_TYPE.FULL_DAY


def convert_hours_to_time_string(time_hours):
    hours = int(time_hours)
    minutes = (time_hours * 60) % 60

    time_string = "%d:%02d" % (hours, minutes)
    return time_string
