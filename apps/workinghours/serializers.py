
from datetime import datetime

from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.settings import api_settings

from apps.contracts.models import Contract
from apps.metronom_commons.data import ABSENCE_TYPE, WEEKDAYS
from apps.metronom_commons.serializers import ForbidModifyIfUsedMixinSerializer, SoftDeletionSerializer
from apps.workinghours.models import (Absence, ClosingHours, FlexWork, Holiday,
                                      WorkingHours, WorkYear)


class AbsenceSerializer(serializers.ModelSerializer):
    start_date = serializers.DateField(format='%Y-%m-%d')
    end_date = serializers.DateField(format='%Y-%m-%d')
    end_date_number = serializers.ChoiceField(choices=ABSENCE_TYPE.CHOICES, required=False)

    class Meta:
        model = Absence
        fields = (
            'id',
            'reason',
            'start_date',
            'end_date',
            'start_date_number',
            'end_date_number',
            'note',
            'approval_status',
            'absence_days'
        )

        extra_kwargs = {
            'approval_status': {'read_only': True},
            'absence_days': {'read_only': True}
        }

    def validate(self, data):
        start_date = data.get('start_date', None)
        end_date = data.get('end_date', None)
        start_date_number = data.get('start_date_number', None)
        end_date_number = data.get('end_date_number', None)
        choices = [k for k, v in ABSENCE_TYPE.CHOICES]

        if start_date and end_date:
            if end_date < start_date:
                raise serializers.ValidationError(_('The "end_date" cannot be earlier than the "start_date".'))

            if start_date == end_date and end_date_number:
                raise serializers.ValidationError(_(
                    'The "end_date_number" is not required when start and end dates match.'
                ))
            else:
                data['end_date_number'] = start_date_number

        if self.context['view'].action == 'create':
            if start_date != end_date and not end_date_number:
                raise serializers.ValidationError(
                    {'end_date_number': [_('This field is required.')]}
                )

        if self.context['view'].action == 'partial_update':
            if not start_date:
                raise serializers.ValidationError(_('The field "start_date" is required.'))
            if not end_date:
                raise serializers.ValidationError(_('The field "end_date" is required.'))
            if start_date_number not in choices:
                raise serializers.ValidationError(_('The field "start_date_number" is required.'))
            if end_date_number not in choices:
                if start_date == end_date:
                    data['end_date_number'] = start_date_number
                else:
                    raise serializers.ValidationError(_('The field "end_date_number" is required.'))

        return data

    def to_internal_value(self, data):
        """
        Allow case-insensitive 'reason' to be posted/patched
        """
        reason = data.get('reason', None)
        if reason:
            data['reason'] = reason.lower()

        return super().to_internal_value(data=data)


class WorkingHoursSerializer(serializers.ModelSerializer):
    from_hour = serializers.TimeField(format='%H%M', input_formats=['%H%M'])
    to_hour = serializers.TimeField(format='%H%M', input_formats=['%H%M'])
    weekday = serializers.IntegerField(write_only=True)

    class Meta:
        model = WorkingHours
        fields = (
            'from_hour',
            'to_hour',
            'weekday'
        )

        extra_kwargs = {
            'flex_work': {'write_only': True}
        }


class FlexWorkBaseSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    class Meta:
        model = FlexWork
        fields = (
            'id',
            'given_name',
            'description',
            'created_at',
            'percentage',
        )


class FlexWorkContractSerializer(FlexWorkBaseSerializer):
    pass


class FlexWorkSerializer(ForbidModifyIfUsedMixinSerializer, FlexWorkBaseSerializer):
    working_hours = WorkingHoursSerializer(many=True, write_only=True)

    class Meta(FlexWorkBaseSerializer.Meta):
        fields = FlexWorkBaseSerializer.Meta.fields + (
            'name',
            'hours',
            'is_archived',
            'working_hours'
        )
        forbid_modify_if_used_in = {
            Contract: ['flex_model']
        }

    def to_representation(self, instance):
        response_dict = super(FlexWorkSerializer, self).to_representation(instance)
        working_hours = dict()
        for key, value in WEEKDAYS.CHOICES:
            queryset = instance.working_hours.filter(weekday=key)
            if not queryset.exists():
                continue

            working_hours[str(value)] = WorkingHoursSerializer(
                queryset, many=True
            ).data

        response_dict['working_hours'] = working_hours
        return response_dict

    def to_internal_value(self, validated_data):
        working_hours_set = []
        working_hours = validated_data.pop('working_hours', None) if 'working_hours' in validated_data else {}

        if isinstance(working_hours, dict):
            for day, slots in working_hours.items():
                if not [k for k, v in WEEKDAYS.CHOICES if v == day]:
                    self.raise_validation_error(day, message='Invalid data. Expected a valid weekday.')
                weekday = next(k for k, v in WEEKDAYS.CHOICES if v == day)
                for slot in slots:
                    slot['weekday'] = weekday
                    working_hours_set.append(slot)

        elif working_hours:
            self.raise_validation_error(working_hours)

        if working_hours_set:
            validated_data['working_hours'] = working_hours_set
        return super(FlexWorkSerializer, self).to_internal_value(validated_data)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if self.context['view'].action == 'partial_update':
            if attrs.get('working_hours'):
                raise serializers.ValidationError(_('The update of field "working_hours" is not allowed.'))

        return attrs

    def raise_validation_error(self, attrs, *args, **kwargs):
        if 'message' not in kwargs:
            message = self.error_messages['invalid'].format(datatype=type(attrs).__name__)
        else:
            message = kwargs.get('message')
        raise serializers.ValidationError({
            api_settings.NON_FIELD_ERRORS_KEY: [message]
        }, code='invalid')

    def create(self, validated_data):
        working_hours_set = validated_data.pop('working_hours', None) if 'working_hours' in validated_data else {}
        flex_work = FlexWork.objects.create(**validated_data)

        for working_hours in working_hours_set:
            working_hours['flex_work'] = flex_work

        serializer = WorkingHoursSerializer(data=working_hours_set, many=True)
        if serializer.is_valid(raise_exception=True):
            serializer.create(working_hours_set)

        return flex_work

    def update(self, instance, validated_data):
        instance.given_name = validated_data.get('given_name', instance.given_name)
        instance.is_archived = validated_data.get('is_archived', instance.is_archived)
        instance.save(update_fields=['given_name', 'is_archived'])
        return instance


class ClosingHoursSerializer(serializers.ModelSerializer):
    from_hour = serializers.TimeField(format='%H%M', input_formats=['%H%M'], required=True)
    to_hour = serializers.TimeField(format='%H%M', input_formats=['%H%M'], required=True)

    class Meta:
        model = ClosingHours
        fields = (
            'from_hour',
            'to_hour'
        )

        extra_kwargs = {
            'holiday': {'write_only': True}
        }

    def validate(self, attrs):
        if not attrs.get('from_hour', None):
            raise serializers.ValidationError(_('The field "from_hour" is required.'))

        if not attrs.get('to_hour', None):
            raise serializers.ValidationError(_('The field "to_hour" is required.'))

        return attrs


class HolidaySerializer(ForbidModifyIfUsedMixinSerializer, serializers.ModelSerializer):
    closing_hours = ClosingHoursSerializer(many=True, required=False)

    class Meta:
        model = Holiday
        fields = (
            'id',
            'name',
            'date',
            'is_all_day',
            'closing_hours'
        )
        forbid_modify_if_used_in = {
            ClosingHours: ['holiday']
        }

    def validate(self, data):
        data = super().validate(data)
        today = datetime.now().date()
        if data.get('date') and data.get('date') <= today:
            raise serializers.ValidationError(_('The field date must be in the future'))

        if 'is_all_day' in data.keys():
            if data.get('is_all_day') and 'closing_hours' in data.keys():
                raise serializers.ValidationError(_('The "all day" holiday cannot have "closing hours"'))

            if not data.get('is_all_day') and 'closing_hours' not in data.keys():
                raise serializers.ValidationError(
                    _('The field "closing hours" is required when "is all day" is False.'))

        return data

    def create(self, validated_data):
        closing_hours_data = validated_data.pop('closing_hours') if 'closing_hours' in validated_data else []
        holiday = Holiday.objects.create(**validated_data)

        if not validated_data.pop('is_all_day'):
            for closing_hours in closing_hours_data:
                closing_hours['holiday'] = holiday
                serializer = ClosingHoursSerializer(data=closing_hours)
                if serializer.is_valid():
                    serializer.create(closing_hours)

        return holiday

    def update(self, instance, validated_data):
        instance.is_all_day = validated_data.get('is_all_day', instance.is_all_day)
        if instance.is_all_day:
            instance.closing_hours.all().delete()

        if not instance.is_all_day and 'closing_hours' in validated_data:
            instance.closing_hours.all().delete()
            for closing_hours in validated_data.pop('closing_hours'):
                closing_hours['holiday'] = instance
                serializer = ClosingHoursSerializer(data=closing_hours)
                if serializer.is_valid():
                    serializer.create(closing_hours)

        instance.name = validated_data.get('name', instance.name)
        instance.date = validated_data.get('date', instance.date)
        instance.save(update_fields=['name', 'date', 'is_all_day'])
        return instance


class WorkYearSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = WorkYear
        fields = (
            'id',
            'year',
            'vacation_days_moved_from_last_year',
            'vacation_days_overall',
            'vacation_days_used',
            'vacation_days_requested',
            'vacation_days_available'
        )
        read_only_fields = ('year',)
