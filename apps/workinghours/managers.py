from datetime import datetime

from django.db import models
from django.db.models import Q
from django.utils import timezone

from apps.workinghours.utils import get_absence_days
from apps.metronom_commons.data import ABSENCE_REASONS, APPROVAL_STATUS


class AbsenceManager(models.Manager):

    @staticmethod
    def get_absence_days_for_year(queryset, year):
        year_start_date = timezone.make_aware(datetime(year, 1, 1)).date()
        year_end_date = timezone.make_aware(datetime(year, 12, 31)).date()
        absences = queryset.filter(
            Q(end_date__range=(year_start_date, year_end_date)) |
            Q(start_date__range=(year_start_date, year_end_date))
        )
        total_absences = 0.0

        for absence in absences:
            if absence.start_date == absence.end_date:
                total_absences += float(absence.start_date_number)
            elif absence.start_date < absence.end_date:
                start_date = absence.start_date if absence.start_date >= year_start_date else year_start_date
                end_date = absence.end_date if absence.end_date <= year_end_date else year_end_date
                total_absences += get_absence_days(absence, start_date, end_date)

        return total_absences

    def get_non_vacation_absences(self, user, year):
        reasons = [k for k, v in ABSENCE_REASONS.CHOICES if k != ABSENCE_REASONS.VACATION]
        queryset = self.filter(user=user, reason__in=reasons)
        return self.get_absence_days_for_year(queryset=queryset, year=year)

    def get_vacation_days_used(self, user, year):
        queryset = self.filter(user=user, approval_status=APPROVAL_STATUS.APPROVED, reason=ABSENCE_REASONS.VACATION)
        return self.get_absence_days_for_year(queryset=queryset, year=year)

    def get_vacation_days_requested(self, user, year):
        queryset = self.filter(user=user, approval_status=APPROVAL_STATUS.REQUESTED, reason=ABSENCE_REASONS.VACATION)
        return self.get_absence_days_for_year(queryset=queryset, year=year)

    def get_user_absences(self, user, year):
        queryset = self.filter(user=user)
        return self.get_absence_days_for_year(queryset=queryset, year=year)
