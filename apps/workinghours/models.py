from collections import defaultdict
from datetime import date, datetime, time

from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from dry_rest_permissions.generics import authenticated_users

from apps.backoffice.utils import get_metronom_setting
from apps.metronom_commons.data import (ABSENCE_REASONS, ABSENCE_TYPE,
                                        APPROVAL_STATUS, WEEKDAYS)
from apps.metronom_commons.models import (ROLE, BackofficeBaseModel,
                                          SoftDeleteMixin,
                                          UserAccessBackofficeMetronomBaseModel,
                                          UserAccessHRMetronomBaseModel,
                                          UserCRUBackofficeHRDeleteMetronomBaseModel)
from apps.users.models import User
from apps.workinghours.managers import AbsenceManager
from apps.workinghours.utils import (get_absence_days,
                                     get_default_working_hours,
                                     get_formatted_working_hours)


class Absence(UserCRUBackofficeHRDeleteMetronomBaseModel):
    reason = models.CharField(choices=ABSENCE_REASONS.CHOICES, max_length=24, verbose_name=_('Reason'))
    start_date = models.DateField(verbose_name=_('Start date'))
    end_date = models.DateField(verbose_name=_('End date'))
    start_date_number = models.CharField(choices=ABSENCE_TYPE.CHOICES, max_length=10,
                                         verbose_name=_('Start date number'))
    end_date_number = models.CharField(choices=ABSENCE_TYPE.CHOICES, max_length=10, verbose_name=_('End date number'))
    is_all_day = models.BooleanField(verbose_name=_('All day'), default=False)
    note = models.TextField(verbose_name=_('Note'))
    user = models.ForeignKey(User, related_name='absences')
    approval_status = models.CharField(choices=APPROVAL_STATUS.CHOICES, default=APPROVAL_STATUS.REQUESTED,
                                       max_length=10, verbose_name=_('Approval status'))

    objects = AbsenceManager()

    @property
    def absence_days(self):
        if self.start_date != self.end_date:
            return get_absence_days(self, self.start_date, self.end_date)

        return float(self.start_date_number)

    @staticmethod
    def get_choice_name(date_number):
        if isinstance(date_number, str):
            date_number = float(date_number) if '.' in date_number else int(date_number)

        choice = next(v for k, v in ABSENCE_TYPE.CHOICES if k == date_number)
        return choice if choice else date_number

    def get_working_hours_for_date(self, date):
        from apps.contracts.models import Contract
        contracts = Contract.objects.filter(user=self.user)
        return WorkingHours.objects.filter(
            flex_work__contract__in=contracts, weekday=date.weekday() + 1
        )

    @staticmethod
    def has_choice(choice):
        choices = [k for k, v in ABSENCE_TYPE.CHOICES]

        if isinstance(choice, str):
            choice = float(choice) if '.' in choice else int(choice)

        return choice in choices

    @staticmethod
    @authenticated_users
    def has_destroy_permission(request):
        return request.user.role in [ROLE.USER, ROLE.HR, ROLE.BACKOFFICE, ROLE.ADMIN]

    @authenticated_users
    def has_object_write_permission(self, request):
        if request.user.role in [ROLE.USER, ROLE.HR, ROLE.BACKOFFICE, ROLE.ADMIN]:
            return self.approval_status != APPROVAL_STATUS.APPROVED
        return False

    @authenticated_users
    def has_object_destroy_permission(self, request):
        if request.user.role in [ROLE.USER, ROLE.HR, ROLE.BACKOFFICE, ROLE.ADMIN]:
            return self.approval_status != APPROVAL_STATUS.APPROVED
        return False

    def clean(self):
        # it's a bit dirty way, but we need to disable all this checks to make possible legacy data processing
        if getattr(self, "_skip_model_check", False):
            return

        if not self.has_choice(self.start_date_number):
            raise ValidationError(_(f'"{self.start_date_number}" is not a valid choice for "start_date_number".'))
        if not self.has_choice(self.end_date_number):
            raise ValidationError(_(f'"{self.end_date_number}" is not a valid choice for "end_date_number".'))

        if Holiday.objects.filter(date=self.start_date, is_all_day=True).exists():
            raise ValidationError(_('Cannot start absence on a holiday.'))

        if Holiday.objects.filter(date=self.end_date, is_all_day=True).exists():
            raise ValidationError(_('Cannot end absence on a holiday.'))

        start_choice_name = self.get_choice_name(self.start_date_number)
        end_choice_name = self.get_choice_name(self.end_date_number)

        start_date_working_hours = self.get_working_hours_for_date(self.start_date).count()
        end_date_working_hours = self.get_working_hours_for_date(self.end_date).count()

        if start_date_working_hours == 0:
            raise ValidationError(_(f'Cannot start absence on a Non Working day.'))
        if start_date_working_hours == 1 and self.start_date_number != ABSENCE_TYPE.HALF_DAY:
            raise ValidationError(_(f'"{start_choice_name}" is not a valid choice for a Half Working day.'))
        if start_date_working_hours >= 2 and not self.has_choice(self.start_date_number):
            raise ValidationError(_(f'"{start_choice_name}" is not a valid choice for a Full Working day.'))

        if end_date_working_hours == 0:
            raise ValidationError(_(f'Cannot end absence on a Non Working day.'))
        if end_date_working_hours == 1 and self.end_date_number != ABSENCE_TYPE.HALF_DAY:
            raise ValidationError(_(f'"{end_choice_name}" is not a valid choice for a Half Working day.'))
        if end_date_working_hours >= 2 and not self.has_choice(self.end_date_number):
            raise ValidationError(_(f'"{end_choice_name}" is not a valid choice for a Full Working day.'))

        if self.is_all_day and self.start_date != self.end_date:
            raise ValidationError(_('The "start date" and "end date" must match for "is_all_day" event'))
        if self.end_date and self.start_date and self.end_date < self.start_date:
            raise ValidationError(_('The "end date" cannot be earlier than the "start date"'))

        queryset = Absence.objects.filter(user=self.user).filter(
            Q(start_date__range=(self.start_date, self.end_date)) |
            Q(end_date__range=(self.start_date, self.end_date)) |
            Q(start_date__lte=self.start_date, end_date__gte=self.end_date)
        ).exclude(pk=self.pk)

        if queryset.exists():
            start_date_qs = queryset.filter(start_date=self.end_date, start_date_number=self.end_date_number)
            end_date_qs = queryset.filter(end_date=self.start_date, end_date_number=self.start_date_number)

            if self.end_date_number == ABSENCE_TYPE.HALF_DAY and end_date_working_hours >= 2 and start_date_qs.exists() and start_date_qs.count() <= 2:
                return
            elif self.start_date_number == ABSENCE_TYPE.HALF_DAY and start_date_working_hours >= 2 and end_date_qs.exists() and end_date_qs.count() <= 2:
                return
            else:
                raise ValidationError(_(
                    'The "start_date" or "end_date" cannot overlap existing absence'
                ))

        work_year = WorkYear.objects.order_by('year').last()
        absence_days = get_absence_days(self, self.start_date, self.end_date)
        if work_year and work_year.vacation_days_available < absence_days:
            available_days = work_year.vacation_days_available
            raise ValidationError(_(
                f'Not enough vacation days available. Requested: {absence_days}, Available: {available_days}'
            ))

    def save(self, *args, **kwargs):
        self.full_clean(exclude=['start_date_number', 'end_date_number'])
        return super().save(*args, **kwargs)

    class Meta(object):
        verbose_name = _('Absence')
        verbose_name_plural = _('Absences')

    def __str__(self):
        return f'{self.note} {self.reason} - {self.is_all_day}'


class FlexWork(SoftDeleteMixin, UserAccessBackofficeMetronomBaseModel):
    given_name = models.CharField(max_length=256, verbose_name=_('Given name'))
    is_archived = models.BooleanField(verbose_name=_('Archived'), default=False)

    @property
    def name(self):
        archived_placeholder = '(archived)' if self.is_archived else ''
        percentage = self.percentage if self.percentage != '0.0%' else ''
        return f'{percentage} {self.given_name} {archived_placeholder}'.strip()

    @property
    def description(self):
        desc = ''

        weekdays = defaultdict(list)
        working_hours = self.working_hours.order_by('created_at')

        for item in working_hours:
            day = next(v for k, v in WEEKDAYS.SHORT_CHOICES if k == item.weekday)
            hours = get_formatted_working_hours(item)

            if hours not in weekdays[day]:
                weekdays[day].append(hours)

        for k, v in weekdays.items():
            weekdays[k] = ' + '.join(v)

        new_dict = defaultdict(list)
        for k, v in weekdays.items():
            new_dict[v].append(k)

        for k, v in new_dict.items():
            val = ', '.join(str(value) for value in v)
            desc += f'{val} {k}, '

        return desc.rstrip(', ')

    @property
    def percentage(self):
        return '{:.1%}'.format(self.hours / get_default_working_hours())

    @property
    def hours(self):
        """
         Convert seconds to hours and return hours
        """
        return sum([working_hours.hours for working_hours in self.working_hours.all()])

    class Meta(object):
        verbose_name = _('Flex Work')
        verbose_name_plural = _('Flex Works')

    def __str__(self):
        return f'{self.name} {self.description}'


class WorkingHours(BackofficeBaseModel):
    flex_work = models.ForeignKey(FlexWork, verbose_name=_('FlexWork'), related_name='working_hours')
    weekday = models.IntegerField(choices=WEEKDAYS.CHOICES, verbose_name=_('Weekday'))
    from_hour = models.TimeField(verbose_name=_('From hour'))
    to_hour = models.TimeField(verbose_name=_('To hour'))

    @property
    def hours(self):
        from_date = datetime.combine(date(1, 1, 1), self.from_hour)
        to_date = datetime.combine(date(1, 1, 1), self.to_hour)

        return (to_date - from_date).total_seconds() / 60 / 60

    def clean(self):
        if self.to_hour < self.from_hour:
            raise ValidationError(_('The "to hour" cannot be earlier than the "from hour".'))

        if WorkingHours.objects.filter(
                flex_work=self.flex_work, weekday=self.weekday, to_hour__range=(self.from_hour, self.to_hour)
        ).exists() or WorkingHours.objects.filter(
            flex_work=self.flex_work, weekday=self.weekday, from_hour__range=(self.from_hour, self.to_hour)
        ).exists():
            raise ValidationError(_(
                'The "to hour" or "from hour" cannot overlap existing working hours for same weekday'
            ))

    def save(self, *args, **kwargs):
        self.full_clean()
        return super(WorkingHours, self).save(*args, **kwargs)

    class Meta(object):
        verbose_name = _('Working Hours')
        verbose_name_plural = _('Working Hours')

    def __str__(self):
        return f'{self.flex_work} {self.weekday} ({self.from_hour} - {self.to_hour})'


class Holiday(UserAccessBackofficeMetronomBaseModel):
    name = models.CharField(max_length=256, verbose_name=_('Name'))
    date = models.DateField(verbose_name=_('Date'))
    is_all_day = models.BooleanField(verbose_name=_('All day'))

    @property
    def hours(self):
        if self.is_all_day:
            return int(get_metronom_setting('WORKDAY_LENGTH_IN_HOURS'))

        return sum([closing_hours.hours for closing_hours in self.closing_hours.all()])

    def clean(self):
        if Holiday.objects.filter(date=self.date).exclude(pk=self.pk).exists():
            raise ValidationError(_('There is an existing holiday on the given date.'))

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = _('Holiday')
        verbose_name_plural = _('Holidays')

    def __str__(self):
        return f'{self.name} {self.date} - {self.is_all_day}'


class ClosingHours(BackofficeBaseModel):
    DEFAULT_TIME = time(0, 0)
    holiday = models.ForeignKey(Holiday, verbose_name=_('Holiday'), related_name='closing_hours')
    from_hour = models.TimeField(verbose_name=_('From hour'), default=DEFAULT_TIME, null=True)
    to_hour = models.TimeField(verbose_name=_('To hour'), default=DEFAULT_TIME, null=True)

    @property
    def hours(self):
        from_date = datetime.combine(date(1, 1, 1), self.from_hour)
        to_date = datetime.combine(date(1, 1, 1), self.to_hour)

        return (to_date - from_date).total_seconds() / 60 / 60

    def clean(self):
        if self.to_hour and self.from_hour and self.to_hour < self.from_hour:
            raise ValidationError(_('The "to hour" cannot be earlier than the "from hour".'))

        if self.holiday.is_all_day:
            if self.to_hour != self.DEFAULT_TIME or self.from_hour != self.DEFAULT_TIME:
                raise ValidationError(_('The "all day" holiday cannot have "closing hours"'))

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = _('Closing Hours')
        verbose_name_plural = _('Closing Hours')

    def __str__(self):
        return f'{self.holiday} ({self.from_hour} - {self.to_hour})'


class WorkYear(BackofficeBaseModel):
    user = models.ForeignKey(User, verbose_name=_('User'), related_name='work_years')
    year = models.PositiveSmallIntegerField(verbose_name=_('Year'))
    vacation_days_moved_from_last_year = models.PositiveSmallIntegerField(
        verbose_name=_('Vacation days moved from last year')
    )

    @property
    def vacation_days_overall(self):
        vacation_days_of_contracts = sum([x.get_overall_vacation_days(self.year) for x in self.user.contracts.all()])

        return self.vacation_days_moved_from_last_year + int(round(vacation_days_of_contracts))

    @property
    def vacation_days_used(self):
        return Absence.objects.get_vacation_days_used(user=self.user, year=self.year)

    @property
    def vacation_days_requested(self):
        return Absence.objects.get_vacation_days_requested(user=self.user, year=self.year)

    @property
    def vacation_days_available(self):
        return self.vacation_days_overall - self.vacation_days_used - self.vacation_days_requested

    @staticmethod
    @authenticated_users
    def has_write_permission(request):
        return request.user.role in [ROLE.BACKOFFICE, ROLE.ADMIN, ROLE.HR]

    @authenticated_users
    def has_object_write_permission(self, request):
        admin_check = super().has_object_write_permission(request)
        return admin_check or request.user.role in [ROLE.BACKOFFICE, ROLE.HR]

    class Meta(object):
        verbose_name = _('Work Year')
        verbose_name_plural = _('Work Years')

    def __str__(self):
        return f'{self.user.email} ({self.year} - {self.vacation_days_overall})'
