from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class PhonenumbersConfig(AppConfig):
    name = 'apps.phonenumbers'
    verbose_name = _('Phone numbers')