from rest_framework.reverse import reverse

from apps.metronom_commons.test import MetronomBaseAPITestCase


class TestPhoneNumber(MetronomBaseAPITestCase):

    def setUp(self):
        self.incorrect_phone_number = '+9233312345678'
        self.correct_phone_number = '+923331234567'

        self.endpoint = reverse('api_helpers:check_phone_number')
        self.post_data = {
            'phone_number': self.correct_phone_number
        }

    def test_check_phone_number_correct(self):
        response = self.user_client.post(self.endpoint, self.post_data)
        self.assertEqual(response.data['is_valid'], True)

    def test_check_phone_number_incorrect(self):
        self.post_data['phone_number'] = self.incorrect_phone_number
        response = self.user_client.post(self.endpoint, self.post_data)
        self.assertEqual(response.data['error_message'], 'Enter a valid phone number.')

    def test_check_phone_number_required(self):
        response = self.user_client.post(self.endpoint)
        self.assertEqual(response.data['error_message'], 'This field is required.')

    def test_check_phone_number_not_blank(self):
        self.post_data['phone_number'] = ''
        response = self.user_client.post(self.endpoint, self.post_data)
        self.assertEqual(response.data['error_message'], 'This field may not be blank.')
