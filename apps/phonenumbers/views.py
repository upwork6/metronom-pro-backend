from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from apps.phonenumbers.serializers import CheckPhoneNumberSerializer


class CheckPhoneNumber(CreateAPIView):
    """
    Phone number validation check
    Received the Phone number:  {'phone_number': '+923331234567'}
    """
    serializer_class = CheckPhoneNumberSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            error_message = {
                'error_message': serializer.errors['phone_number'][0]
            }

            return Response(error_message, status=status.HTTP_400_BAD_REQUEST)

        phone_number = serializer.data['phone_number']

        json_response = {
            "is_valid": True,
            "phone_number": phone_number
        }

        return Response(json_response, status=status.HTTP_200_OK)
