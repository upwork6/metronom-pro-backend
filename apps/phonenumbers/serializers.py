from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers


class CheckPhoneNumberSerializer(serializers.Serializer):
    """Serialize data from the phone number."""

    phone_number = PhoneNumberField()
