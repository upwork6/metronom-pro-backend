import random
from datetime import date, datetime, timedelta
from decimal import Decimal

from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import models
from django.db.models import Sum
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _
from djmoney.models.fields import MoneyField
from djmoney.money import Money
from dry_rest_permissions.generics import authenticated_users

from apps.accounting.models import Allocation
from apps.activities.models import (ActivityGroup, ActivityStepGeneralModel,
                                    Step, TemplateActivity,
                                    TemplateActivityStep)
from apps.addresses.models import Address
from apps.articles.models import ArticleGroup, TemplateArticle
from apps.backoffice.utils import get_setting
from apps.companies.models import Company
from apps.metronom_commons.choices_flow import (ActivityArticleOfferedStatus,
                                                OfferStatus)
from apps.metronom_commons.data import (ACCOUNTING_TYPES, ACTIVITY_STATUS,
                                        ASSIGNMENT_STATUS, BILLING_AGREEMENT,
                                        BILLING_INTERVAL, CURRENCY,
                                        INVOICE_STATUS, PROJECT_ICONS,
                                        REOCURRING_TYPES, WORKLOGS_STATUS)
from apps.metronom_commons.fields import ChoicesCharField
from apps.metronom_commons.mixins import (CloneMixin, ProtectedFieldsMixin,
                                          ReplaceMixin, UUIDModel)
from apps.metronom_commons.models import (ROLE, ActivityStepDynamicPermissions,
                                          MetronomBaseModel, SoftDeleteMixin,
                                          UserAccessHRMetronomBaseModel,
                                          UserAccessMetronomBaseModel,
                                          UserCRUAccountingDeletePermission)
from apps.metronom_commons.utils import all_statuses_are_the_same
from apps.persons.models import Person
from apps.projects.utils import project_validator
from apps.users.models import User
from config import settings
from djmoney_rates.utils import convert_money


class CampaignOrProject(SoftDeleteMixin, UserCRUAccountingDeletePermission):
    """ Abstract method for Campaign / project basics, used in both. """
    old_id = models.CharField(max_length=16, blank=True, default="")
    title = models.CharField(max_length=150, verbose_name=_('title'))
    company = models.ForeignKey(Company, verbose_name=_('company'), related_name='%(class)ss', on_delete=models.CASCADE)
    contact_person = models.ForeignKey(
        Person,
        verbose_name=_('contact person'),
        null=True,
        on_delete=models.SET_NULL,
    )
    description = models.TextField(_('description'), blank=True)
    customer_reference = models.TextField(_('customer reference'), blank=True, help_text=_("max. 2 rows"))
    start_date = models.DateField(_('start date'), default=date.today)
    finishing_date = models.DateField(_('finishing date'))
    delivery_date = models.DateField(_('delivery date'))

    billing_currency = models.CharField(
        _('Master currency'),
        max_length=3,
        choices=CURRENCY.CHOICES,
        default=CURRENCY.CHF,
        help_text=_('This is the currency of the Project or Campaign'),
    )
    billing_address = models.ForeignKey(
        Address,
        verbose_name=_('billing address'),
        null=True,
        on_delete=models.SET_NULL,
    )
    billing_agreement = models.CharField(
        _('Billing agreement'),
        max_length=20,
        choices=BILLING_AGREEMENT.CHOICES,
        blank=True,
    )
    billing_interval = models.CharField(
        _('Billing interval'),
        max_length=15,
        choices=BILLING_INTERVAL.CHOICES,
        blank=True,
    )
    number = models.PositiveIntegerField(_('number'), editable=False)
    year_number = models.PositiveIntegerField(_('year number'), editable=False)
    public_id = models.CharField(_('Public ID'), max_length=12, unique=True, editable=False)

    class Meta:
        abstract = True
        unique_together = (('number', 'year_number'),)

    def __str__(self):
        return f'{self.title}'

    def calculate_public_id(self, letter):
        """ Calculate and assign the number and year_number """

        if not self.year_number:
            self.year_number = int(f'{date.today().year}')

        if not self.number:
            max_numbers = self.__class__.all_objects.aggregate(models.Max('number'))

            if max_numbers['number__max'] is None:
                self.number = 1
            else:
                self.number = max_numbers['number__max'] + 1

        self.public_id = f'{letter}-{str(self.year_number)[:4]}-{self.number:04d}'

    def save(self, *args, **kwargs):

        # Validate the dates and billing_address
        valid, errors = project_validator(instance=self, model_check=True)
        if not valid:
            raise ValidationError(errors)

        super().save(*args, **kwargs)

    @property
    def progress(self):
        return float(random.randint(1, 99)) / 100

    @staticmethod
    @authenticated_users
    def has_destroy_permission(request):
        return request.user.role in [ROLE.USER, ROLE.ADMIN, ROLE.ACCOUNTING]

    @authenticated_users
    def has_object_read_permission(self, request):
        """ Apart from User and Accounting roles, here we have also KAM and PM permissions. They are not a roles.
        KAM - company.key_account_manager
        PM - just the owner of the instance
        """
        if super().has_object_read_permission(request):
            return True
        model_name = self.__class__.__name__.lower()
        owner_attr = '{}_owner'.format(model_name)
        owner = getattr(self, owner_attr)
        kam_pm_check = False
        if owner == request.user:
            kam_pm_check = True
        elif self.company.key_account_manager is not None and self.company.key_account_manager == request.user:
            kam_pm_check = True
        return kam_pm_check

    @authenticated_users
    def has_object_write_permission(self, request):
        """ Apart from User and Accounting roles, here we have also KAM and PM permissions. They are not a roles.
        KAM - company.key_account_manager
        PM - just the owner of the instance
        """
        if super().has_object_write_permission(request):
            return True
        model_name = self.__class__.__name__.lower()
        owner_attr = '{}_owner'.format(model_name)
        owner = getattr(self, owner_attr)
        kam_pm_check = False
        if owner == request.user:
            kam_pm_check = True
        elif self.company.key_account_manager is not None and self.company.key_account_manager == request.user:
            kam_pm_check = True
        return kam_pm_check

    @authenticated_users
    def has_object_destroy_permission(self, request):
        """ Apart from User and Accounting roles, here we have also KAM and PM permissions. They are not a roles.
        KAM - company.key_account_manager
        PM - just the owner of the instance
        """
        if super().has_object_destroy_permission(request):
            return True
        model_name = self.__class__.__name__.lower()
        owner_attr = '{}_owner'.format(model_name)
        owner = getattr(self, owner_attr)
        kam_pm_check = False
        if owner == request.user:
            kam_pm_check = True
        elif self.company.key_account_manager is not None and self.company.key_account_manager == request.user:
            kam_pm_check = True
        return kam_pm_check


class Campaign(CampaignOrProject):
    """ Campaigns are concentrating Projects together. """

    campaign_owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('campaign_owner'),
        related_name='projects_campaign_owner',
        null=True,
        on_delete=models.SET_NULL,
    )

    class Meta:
        verbose_name = _('campaign')
        verbose_name_plural = _('campaigns')

    def save(self, *args, **kwargs):
        if not self.public_id:
            self.calculate_public_id("K")

        super().save(*args, **kwargs)

    @property
    def total_planned_budget(self):
        result = sum(x.total_planned_budget for x in self.projects.all())
        return result or Money(0, self.billing_currency)

    @property
    def total_real_cost(self):
        result = sum(x.total_real_cost for x in self.projects.all())
        return result or Money(0, self.billing_currency)

    def get_absolute_url(self):
        return f'/campaigns/{self.id}/'


class Project(CampaignOrProject, CloneMixin):
    """
        Projects are created for customers, activities and articles are added to them.
        Offers and invoices will be created from the Project
    """

    campaign = models.ForeignKey(
        Campaign,
        verbose_name=_('campaign'),
        related_name='projects',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )
    accounting_type = models.CharField(max_length=8, verbose_name=_(
        'accounting type'), choices=ACCOUNTING_TYPES.CHOICES)

    project_owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('project owner'),
        related_name='projects_project_owner',
        null=True,
        on_delete=models.SET_NULL,
    )
    reoccurring = models.CharField(max_length=10, choices=REOCURRING_TYPES.CHOICES, default=REOCURRING_TYPES.NO)

    class Meta:
        verbose_name = _('project')
        verbose_name_plural = _('projects')

    def save(self, *args, **kwargs):
        if not self.public_id:
            self.calculate_public_id("P")

        super().save(*args, **kwargs)

    @property
    def icons(self):
        icons = []

        activities = self.activities.all()
        work_logs = WorkLog.objects.filter(
            project_activity_step__activity__project=self
        )
        if activities.exists():
            if self.billing_interval == BILLING_INTERVAL.TASK_BASED and any(
                    ACTIVITY_STATUS.DONE == activity.status for activity in activities):
                icons.append(PROJECT_ICONS.BILLABLE)

            elif self.billing_interval != BILLING_INTERVAL.TASK_BASED and all(
                    ACTIVITY_STATUS.DONE == activity.status for activity in activities):
                icons.append(PROJECT_ICONS.BILLABLE)

        if self.delivery_date < timezone.now().date():
            icons.append(PROJECT_ICONS.DELAYED)

        if work_logs.exists():
            work_log = work_logs.latest('work_date')

            if work_log.work_date < (timezone.now() - timedelta(days=30)).date():
                icons.append(PROJECT_ICONS.FELL_ASLEEP)
        if activities and all_statuses_are_the_same(activities.values_list('status', flat=True)) and \
                activities[0].status == ACTIVITY_STATUS.CLOSED:
            icons.append(PROJECT_ICONS.IS_CLOSED)

        return icons

    @property
    def total_planned_budget_of_activities(self):
        result = sum(x.total_planned_budget for x in self.activities.all())
        return result or Money(0, self.billing_currency)

    @property
    def total_planned_budget_of_articles(self):
        result = sum(x.total_planned_budget for x in self.articles.all())
        return result or Money(0, self.billing_currency)

    @property
    def total_planned_budget(self):
        result = sum([self.total_planned_budget_of_activities, self.total_planned_budget_of_articles])
        return result or Money(0, self.billing_currency)

    @property
    def total_planned_budget_of_finished_activities(self):
        result = sum(x.total_planned_budget for x in self.activities.filter(status=ACTIVITY_STATUS.DONE))
        return result or Money(0, self.billing_currency)

    @property
    def total_planned_budget_of_finished_articles(self):
        result = sum(x.total_planned_budget for x in self.articles.all())
        return result or Money(0, self.billing_currency)

    @property
    def total_planned_budget_of_finished_items(self):
        result = sum([self.total_planned_budget_of_finished_activities, self.total_planned_budget_of_finished_articles])
        return result or Money(0, self.billing_currency)

    @property
    def total_real_cost_of_activities(self):
        try:
            result = sum(x.total_real_cost for x in self.activities.all())
        except TypeError:
            return "NaN"
        return result or Money(0, self.billing_currency)

    @property
    def total_real_cost_of_articles(self):
        result = sum(x.total_real_cost for x in self.articles.all())
        return result or Money(0, self.billing_currency)

    @property
    def total_real_cost(self):
        try:
            result = sum([self.total_real_cost_of_activities, self.total_real_cost_of_articles])
        except TypeError:
            return "NaN"
        return result or Money(0, self.billing_currency)

    @property
    def total_real_cost_of_finished_activities(self):
        result = sum(x.total_real_cost for x in self.activities.filter(status=ACTIVITY_STATUS.DONE))
        return result or Money(0, self.billing_currency)

    @property
    def total_real_cost_of_finished_articles(self):
        result = sum(x.total_real_cost for x in self.articles.all())
        return result or Money(0, self.billing_currency)

    @property
    def total_real_cost_of_finished_items(self):
        result = sum([self.total_real_cost_of_finished_activities, self.total_real_cost_of_finished_articles])
        return result or Money(0, self.billing_currency)

    @property
    def total_number_of_activities(self):
        return self.activities.count()

    @property
    def total_number_of_finished_activities(self):
        return self.activities.filter(status=ACTIVITY_STATUS.DONE).count()

    def get_absolute_url(self):
        return f'/projects/{self.id}/'

    @property
    def offers(self):
        """this property is used also inside ProjectOfferSerializer l:50"""
        return self.offer_set.all()

    def get_cloned_project(self, logged_in_user=None):
        new_number = f'{(int(self.__class__.objects.aggregate(models.Max("number"))["number__max"]) + 1):04d}'
        new_public_id = f'P-{int(date.today().year)}-{new_number}'
        new_project_owner = self.project_owner
        if logged_in_user and isinstance(logged_in_user, User):
            new_project_owner = logged_in_user
        # We Apply `RELATIVE Dates` according to the Old Project
        # TODO: Add "(Kopie)" or "(Copy)" depending on the language
        modified_kwargs = dict([
            ("number", new_number),
            ('title', _(f'{self.title} (Copy)')),
            ('project_owner', new_project_owner),
            ('public_id', new_public_id),
            ('start_date', date.today()),
            ('finishing_date', date.today() + (self.finishing_date - self.start_date)),
            ('delivery_date', date.today() + (self.delivery_date - self.start_date)),
            ('created_at', datetime.today()),
        ])
        cloned_project = self.clone(**modified_kwargs)
        excluded_items = [ActivityArticleOfferedStatus.REPLACED, ActivityArticleOfferedStatus.DECLINED]
        [activity.duplicate_under_new_project(cloned_project) for
         activity in self.activities.exclude(offer_status__in=excluded_items)]
        [article.duplicate_under_new_project(cloned_project) for
         article in self.articles.exclude(offer_status__in=excluded_items)]
        return cloned_project


class ActivityOrArticle(SoftDeleteMixin, ProtectedFieldsMixin, UserCRUAccountingDeletePermission, ReplaceMixin):
    """Abstract Model for Project Activity and Project Article
    If the Offer is SENT any changes are not allowed =>
    When this happens also the `offer_status` for ProjectActivity/Article be changed so it make sense to protect
    """

    PROTECTED_STATES = [ActivityArticleOfferedStatus.SENT, ActivityArticleOfferedStatus.ACCEPTED,
                        ActivityArticleOfferedStatus.DECLINED]
    PROTECTED_LIST = ['display_name', 'description', 'project']
    STATUS_FIELD_NAME = 'offer_status'

    version = models.PositiveSmallIntegerField(_('position'), default=1)
    display_name = models.CharField(max_length=128, blank=True)
    offer_status = ChoicesCharField(
        max_length=10,
        choices=ActivityArticleOfferedStatus,
        default=ActivityArticleOfferedStatus.CREATED
    )
    invoice_status = models.CharField(
        choices=INVOICE_STATUS.CHOICES,
        null=True,
        blank=True,
        max_length=12
    )
    description = models.TextField()
    position = models.PositiveSmallIntegerField(default=1)

    class Meta:
        abstract = True

    # Will return name property "name" from each article and activity
    def __str__(self):
        return f'{self.name}'

    def get_final_state(self):
        if self.get_final_state_information():
            return True
        return False

    def get_final_state_information(self):
        if self.get_accepted_or_declined_item_offered():
            accepted_or_declined_item_offered = self.get_accepted_or_declined_item_offered()
            final_state_information = {
                "offer_status": accepted_or_declined_item_offered.offer.offer_status,
                "offer_type": "project_offer",
                "date": accepted_or_declined_item_offered.offer.offer_date,
                "offer": {
                    "public_id": accepted_or_declined_item_offered.offer.public_id
                }
            }
            return final_state_information
        return None

    # Common Property since Both Activity/Article will have a "project"
    @property
    def currency(self):
        """ We use the Currency of the Project and use it as a property here to make things easier
            Each Activity/Article will have a 'project' field but with different "related_name"
        """
        return self.project.billing_currency

    def change_status_to_sent(self):
        valid_to_send = [ActivityArticleOfferedStatus.CREATED,
                         ActivityArticleOfferedStatus.IMPROVED,
                         ActivityArticleOfferedStatus.IMPROVABLE]
        if self.offer_status in valid_to_send:
            self.offer_status = ActivityArticleOfferedStatus.SENT
            self.save()

    def change_status_to_declined(self):
        if self.offer_status == ActivityArticleOfferedStatus.SENT:
            self.offer_status = ActivityArticleOfferedStatus.DECLINED
            self.save()

    # TODO: When an invoice is created, the `status` changes to `closed`
    def change_invoice_status_to_invoiced(self):
        # the status of Activity and all ActivitySteps ?
        pass

    def save(self, *args, **kwargs):
        # To validate Choice Flow in one single step: when Calling `save()`
        self.full_clean()
        super().save(*args, **kwargs)

    @staticmethod
    @authenticated_users
    def has_destroy_permission(request):
        return request.user.role in [ROLE.USER, ROLE.ADMIN, ROLE.ACCOUNTING]

    @authenticated_users
    def has_object_read_permission(self, request):
        """ Apart from User and Accounting roles, here we have also KAM and PM permissions. They are not a roles.
        KAM - company.key_account_manager
        PM - just the owner of the instance
        """
        if super().has_object_read_permission(request):
            return True
        model_name = self.__class__.__name__.lower()
        owner = self.project.project_owner
        kam_pm_check = False
        company = self.project.company
        if owner == request.user:
            kam_pm_check = True
        elif company.key_account_manager is not None and company.key_account_manager == request.user:
            kam_pm_check = True
        return kam_pm_check

    @authenticated_users
    def has_object_write_permission(self, request):
        """ Apart from User and Accounting roles, here we have also KAM and PM permissions. They are not a roles.
        KAM - company.key_account_manager
        PM - just the owner of the instance
        """
        if super().has_object_write_permission(request):
            return True
        model_name = self.__class__.__name__.lower()
        owner = self.project.project_owner
        kam_pm_check = False
        company = self.project.company
        if owner == request.user:
            kam_pm_check = True
        elif company.key_account_manager is not None and company.key_account_manager == request.user:
            kam_pm_check = True
        return kam_pm_check

    @authenticated_users
    def has_object_destroy_permission(self, request):
        """ Apart from User and Accounting roles, here we have also KAM and PM permissions. They are not a roles.
        KAM - company.key_account_manager
        PM - just the owner of the instance
        """
        if super().has_object_destroy_permission(request):
            return True
        model_name = self.__class__.__name__.lower()
        owner = self.project.project_owner
        kam_pm_check = False
        company = self.project.company
        if owner == request.user:
            kam_pm_check = True
        elif company.key_account_manager is not None and company.key_account_manager == request.user:
            kam_pm_check = True
        return kam_pm_check


class ProjectActivity(ActivityOrArticle):
    """
        Project activities are activities added to a Project. Those are items, Users in the Agency work on.
        They can be used as Offer Activities in an Offer and will be invoiced as Invoiced Activities in an Invoice.

        How it works:
            - planned_effort: How many hours OR days are planned for this step?
            - planned_cost_in_billing_currency: What is the planned cost for this step depending on the given rates?
            - real_effort_in_hours: How many hours people had to work on this, calculated by added Working logs
            - real_cost_in_billing_currency: How much did this step cost for real?
    """
    PROTECTED_LIST = [*ActivityOrArticle.PROTECTED_LIST]

    project = models.ForeignKey(Project, related_name='activities')
    start_date = models.DateField()
    end_date = models.DateField()
    used_template_activity = models.ForeignKey(TemplateActivity, null=True, blank=True)
    activity_group = models.ForeignKey(ActivityGroup)
    status = models.CharField(
        default=ACTIVITY_STATUS.PLAN,
        choices=ACTIVITY_STATUS.CHOICES,
        max_length=10,
        blank=True
    )
    used_project_activity = models.ForeignKey('self', null=True, blank=True)

    class Meta:
        verbose_name = _("Project Activity")
        verbose_name_plural = _("Project Activities")

    @property
    def name(self):
        """ If there is a display_name set, we use this one;
        if not we go for the default Name of the Template Activity
        """
        if self.display_name:
            return self.display_name
        if self.used_template_activity:
            return self.used_template_activity.name
        return ""

    @cached_property
    def owner(self):
        return self.project.project_owner or None

    @cached_property
    def key_account_manager(self):
        return self.project.company.key_account_manager or None

    def start_activity(self):
        self.status = ACTIVITY_STATUS.WORK
        self.save()
        self.activity_steps.all().update(status=ACTIVITY_STATUS.WORK)

    def stop_activity(self):
        self.status = ACTIVITY_STATUS.CANCEL
        self.save()
        self.activity_steps.filter().update(status=ACTIVITY_STATUS.CANCEL)

    # TODO: Implement Sending notifications to workers
    def send_notification_to_workers(self):
        if self.status == ACTIVITY_STATUS.PLAN:
            pass

    # TODO: Implement Sending notifications to PL
    def send_notification_to_PL(self):
        if self.status == ACTIVITY_STATUS.DONE:
            pass

    # TODO: Implement Sending notifications to Accountant
    def send_notification_to_accountant(self):
        if self.status == ACTIVITY_STATUS.BILLABLE:
            pass

    def get_sent_in_project_offers(self):
        """Return:
        None or a list of Project offers with UUID+Name,
        where the Project activity/article was sent in
        """
        offer_states = [OfferStatus.SENT, OfferStatus.ACCEPTED]
        return [activity_offered.offer for activity_offered in
                self.activityoffered_set.filter(offer__offer_status__in=offer_states)]

    @property
    def sent_in_project_offers(self):
        """Return:
        None or a list of Project offers with UUID+Name,
        where the Project activity/article was sent in
        """
        if self.get_sent_in_project_offers():
            return self.get_sent_in_project_offers()
        return None

    def get_accepted_or_declined_item_offered(self):
        result = self.activityoffered_set.filter(
            offer__offer_status__in=[OfferStatus.ACCEPTED, OfferStatus.DECLINED]
        ).order_by('-updated_at').last()
        return result

    def change_status_to_accepted(self):
        if self.offer_status == ActivityArticleOfferedStatus.SENT:
            self.offer_status = ActivityArticleOfferedStatus.ACCEPTED
            self.save()
        self.activityoffered_set.filter(
            offer_status=ActivityArticleOfferedStatus.SENT
        ).update(offer_status=ActivityArticleOfferedStatus.ACCEPTED)

    def duplicate_under_new_project(self, cloned_project):
        if cloned_project and isinstance(cloned_project, Project):
            modified_kwargs = dict([
                ("version", 1),
                ('offer_status', ActivityArticleOfferedStatus.CREATED),
                ('project', cloned_project),
                ('start_date', date.today()),
                ('end_date', date.today() + (self.end_date - self.start_date)),
                ('created_at', datetime.today())
            ])
            activity_clone = self.clone(**modified_kwargs)
            [activity_step.duplicate_under_new_activity(activity_clone) for
             activity_step in self.activity_steps.all()]

    def change_status_to_replaced(self):
        if self.offer_status == ActivityArticleOfferedStatus.SENT:
            modified_kwargs = dict([
                ("version", self.version + 1),
                ('offer_status', ActivityArticleOfferedStatus.IMPROVABLE),
                ('replaced_item', self)
            ])
            clone = self.clone(**modified_kwargs)
            self.status = ACTIVITY_STATUS.CANCEL
            self.offer_status = ActivityArticleOfferedStatus.REPLACED
            self.save()
            self.move_activity_step_to_other_activity()

    def can_be_replaced_or_declined(self, offer_item=None):
        """You are not allowed to REPLACE an Activity in Offer_A if
        it is still SENT in an Offer_B (and not yet `accepted` or `declined`).
        If the Offer Activity of Offer_A is still in another Offer_B (via its Project Activity)

        When an Activity was declined in Offer_A and there is
        another Offer_B already, the Project Activity will keep the status SENT
        It will only move to `declined` if it was declined in all Offers (!!!).
        As soon as it is `declined`, it can not be used for an Offer C.
        :return: Boolean value
        """
        if offer_item:
            activity_offer_status = self.activityoffered_set.filter(
                offer_status=ActivityArticleOfferedStatus.SENT
            ).exclude(pk=offer_item.pk)
            if not activity_offer_status:
                return True
        # TODO: Raise an Error OR return avoid silently to change the instance
        raise ValidationError({
            "offer_status": _(
                f"'offer_status' for ActivityProject can't be changed to while other dependent ActivityOffers are 'SENT' for aproval.")
        })

    def change_status_to_plan(self):
        self.status = ACTIVITY_STATUS.PLAN
        self.save()

    def move_activity_step_to_other_activity(self):
        """Move ActivityAssignment and WorkLog  to the new created ProjectActivity"""
        for activity_step in self.activity_steps.all():
            activity_step.replace_and_move_assignments_and_worklogs()
        # ProjectActivityStep.objects.filter(activity=self).update(activity=activity)

    def already_accepted(self):
        for activity_offered in self.activityoffered_set.all():
            if activity_offered.offer.offer_status == OfferStatus.ACCEPTED:
                return activity_offered.offer
        return None

    @property
    def total_planned_budget(self):
        result = sum(x.planned_budget for x in self.activity_steps.all())
        return result or Money(0, self.currency)

    @property
    def total_planned_effort_in_hours(self):
        """ Total offered planned effort
         rtype: Decimal
        """
        return sum(x.planned_effort_in_hours for x in self.activity_steps.all()) or 0

    @property
    def total_real_effort_in_hours(self):
        return sum(x.real_effort_in_hours for x in self.activity_steps.all())

    @property
    def total_real_cost(self):
        try:
            result = sum(x.real_cost for x in self.activity_steps.all())
        except TypeError:
            return "NaN"
        return result or Money(0, self.currency)

    @property
    def total_invoiced_effort(self):
        # TODO: Must be days and hours..
        return sum(x.invoiced_effort for x in self.activity_steps.all())

    @property
    def total_invoiced_cost(self):
        try:
            result = sum(x.invoiced_cost for x in self.activity_steps.all())
        except TypeError:
            return "NaN"
        return result or Money(0, self.currency)

    @property
    def use_count(self):
        return ProjectActivity.objects.filter(used_project_activity=self).count()

    def change_status_to_improved(self, from_activity_offered=False):
        self.offer_status = ActivityArticleOfferedStatus.IMPROVED
        self.save()
        if not from_activity_offered:
            [activity.change_status_to_improved(True) for
             activity in self.activityoffered_set.filter(
                offer_status=ActivityArticleOfferedStatus.IMPROVABLE
            )]


class ProjectActivityStep(SoftDeleteMixin, ActivityStepDynamicPermissions,
                          ProtectedFieldsMixin, ActivityStepGeneralModel, ReplaceMixin):
    """ Project Activities are put together by single Steps. Each step has an effort in hours and
        a used Rate combined with this type of used Step.
        ProjectActivityStep is using some templates on which are initially build
        and they are kept as `used_*`for statistical purpose:
            - used_template_activity_step => TemplateActivityStep
            - used_step => STEP
    """

    PROTECTED_STATES = [ActivityArticleOfferedStatus.SENT, ActivityArticleOfferedStatus.ACCEPTED,
                        ActivityArticleOfferedStatus.DECLINED]
    PROTECTED_LIST = ['used_name', 'used_rate', 'unit', 'planned_effort']
    STATUS_FIELD_NAME = 'activity.offer_status'

    used_step = models.ForeignKey(
        Step,
        verbose_name=_('Used Step'),
        help_text=_("The Activity Step originaly used to create current ProjectActivityStep.")
    )
    activity = models.ForeignKey(ProjectActivity, related_name="activity_steps")
    used_template_activity_step = models.ForeignKey(TemplateActivityStep, blank=True,
                                                    null=True, on_delete=models.SET_NULL)

    used_rate = MoneyField(verbose_name=_('Used rate'), max_digits=10, decimal_places=2)

    planned_effort = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    invoiced_effort = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    start_date = models.DateField()
    end_date = models.DateField()
    status = models.CharField(default=ACTIVITY_STATUS.PLAN, choices=ACTIVITY_STATUS.CHOICES, max_length=10)
    used_name = models.CharField(_('Used name'), max_length=128, null=True, blank=True)

    class Meta:
        verbose_name = _("Project Activity Step")
        verbose_name_plural = _("Project Activity Steps")

    def __str__(self):
        return self.name

    def duplicate_under_new_activity(self, cloned_activity):
        if cloned_activity and isinstance(cloned_activity, ProjectActivity):
            modified_kwargs = dict([
                ('status', ACTIVITY_STATUS.PLAN),
                ('activity', cloned_activity),
                ('planned_effort', 0),
                ('invoiced_effort', 0),
                ('start_date', date.today()),
                ('end_date', date.today() + (self.end_date - self.start_date)),
                ('created_at', datetime.today())
            ])
            clone = self.clone(**modified_kwargs)

    @cached_property
    def owner(self):
        return self.activity.owner

    @cached_property
    def key_account_manager(self):
        return self.activity.key_account_manager

    def replace_and_move_assignments_and_worklogs(self):
        self.status = ACTIVITY_STATUS.CANCEL
        self.save()
        if hasattr(self.activity, 'replaced_by'):
            activity = self.activity.replaced_by
        else:
            activity = self.activity
        modified_kwargs = dict([
            ("activity", activity),
            ('status', ACTIVITY_STATUS.PLAN),
            ('replaced_item', self)
        ])
        clone = self.clone(**modified_kwargs)
        self.assignments.update(project_activity_step=clone)
        self.hour_logs.update(project_activity_step=clone)

    @property
    def name(self):
        if self.used_name:
            return self.used_name
        return self.used_step.name if self.used_step else self.activity.name

    @property
    def dependency_position(self):
        return None if not self.dependency else self.dependency.position

    @property
    def planned_effort_in_hours(self):
        """ The planned effort in hours (days calculated into hours by the Workday length) """
        # TODO #386 - Tests missing for this new methods
        if not self.planned_effort:
            return 0

        if self.unit == ProjectActivityStep.HOURS:
            return round(Decimal(self.planned_effort), 2)
        if self.unit == ProjectActivityStep.DAYS:
            default_workday_length = Decimal(get_setting("WORKDAY_LENGTH_IN_HOURS"))
            return round(default_workday_length * Decimal(self.planned_effort), 2)

    def calculate_planned_budget_for_currency(self, currency):
        """
        What is the planned cost for this step depending on the given rates?

        Calculates the planned budget for the step if any rate for this step exists and if step has planned_effort

        :returns: Money object
        """

        if not self.planned_effort:
            return Money(0, currency)

        return self.used_rate * self.planned_effort

    @property
    def planned_budget(self):
        """
        What is the planned budget for this step depending on the given rates?

        Calculates the planned budget for the step if any rate for this step exists and if step has planned_effort

        :returns: Money object
        """

        return self.calculate_planned_budget_for_currency(self.activity.currency)

    @property
    def real_effort_in_hours(self):
        """ Calculated real effort taken by the Step - How many hours people had to work on this, calculated by added Working logs?

        :returns: Decimal or 0
        """
        total_effort = self.hour_logs.all().aggregate(Sum('tracked_time'))['tracked_time__sum']
        return total_effort or 0

    @property
    def real_working_days(self):
        """ Calculated the number of working days
        :returns: Decimal or 0
        """
        total_working_days = self.hour_logs.values_list('work_date', flat=True).count()
        return total_working_days or 0

    def calculate_real_cost_for_currency(self, currency):
        """ Calculate the real cost if rate in such currency exists

        :returns: Decimal or 0
        """
        if not self.real_effort_in_hours:
            return Money(0, currency)

        if self.unit == ProjectActivityStep.HOURS:
            return self.real_effort_in_hours * self.used_rate
        if self.unit == ProjectActivityStep.DAYS:
            return self.real_working_days * self.used_rate

    @property
    def real_cost(self):
        """
        What is the real cost for this step depending on the given rates?

        Calculates the real cost for the step if any rate for this step exists and if step has planned_effort

        :returns: Money object
        """
        return self.calculate_real_cost_for_currency(self.activity.currency)

    @property
    def invoiced_cost(self):
        """
        """
        # TODO: Add tests
        return self.real_cost

    def get_rate(self, currency):
        """ Get the rate for this Step with this Currency, is only used at Save """

        try:
            rate = self.used_step.rates.get(currency=currency)
        except ObjectDoesNotExist:
            raise ValidationError(f"No rates defined for {currency}, please define those or use a different Currency!")

        if self.unit == ProjectActivityStep.HOURS:
            if rate.hourly_rate is None:
                raise ValidationError(
                    f"No hourly rate in {currency} defined for the step {self.used_step.name}, please define one!")
            return rate.hourly_rate
        if self.unit == ProjectActivityStep.DAYS:
            if rate.daily_rate is None:
                raise ValidationError(
                    f"No daily rate in {currency} defined for the step {self.used_step.name}, please define one!")
            return rate.daily_rate
        else:
            return Money(0, currency)

    def save(self, *args, **kwargs):

        # Save the used_rate
        if self.id:
            step_instance = ProjectActivityStep.objects.filter(id=self.id).last()
            if step_instance and self.used_step_id != step_instance.used_step_id:
                self.used_rate = self.get_rate(self.activity.currency)
        if not self.used_rate:
            self.used_rate = self.get_rate(self.activity.currency)
        # Make sure when creating a New ActivityStep we give the name from `used_step.name`
        if not self.used_name:
            self.used_name = self.used_step.name
        # Check if all the Steps inside Activity are the same and change Activity as well
        activity = self.activity
        # The Other Steps having the same status as this Step
        other_steps = activity.activity_steps.filter(status=self.status).exclude(pk=self.pk).count()
        # The All Steps exceptr the Current*(self) one
        all_steps = activity.activity_steps.all().exclude(pk=self.pk).count()
        if other_steps == all_steps:
            self.activity.status = self.status
            self.activity.save()
        super(ProjectActivityStep, self).save(*args, **kwargs)


class ActivityAssignment(UserAccessMetronomBaseModel):
    """ An Activity step can be assigned to a one or many Users of the Agency to work on it"""

    worker = models.ForeignKey(User, related_name="assignments")
    project_activity_step = models.ForeignKey(ProjectActivityStep, related_name="assignments")
    seen_in_dashboard = models.BooleanField(default=False)
    notification_sent = models.BooleanField(default=False)
    accepted = models.NullBooleanField(default=None)
    status = models.PositiveSmallIntegerField(
        default=ASSIGNMENT_STATUS.ACTIVE,
        choices=ASSIGNMENT_STATUS.CHOICES
    )

    class Meta:
        verbose_name = _("Activity assignments")
        verbose_name_plural = _("Activity assignments")
        unique_together = [('worker', 'project_activity_step')]

    def __str__(self):
        return "{} : {}".format(self.project_activity_step.name, self.worker.name)


class WorkLog(SoftDeleteMixin, UserCRUAccountingDeletePermission):
    """ Worked hours added to a step to track the real effort"""

    project_activity_step = models.ForeignKey(ProjectActivityStep, verbose_name=_(
        "Project activity step"), related_name="hour_logs")
    worker = models.ForeignKey("users.User", verbose_name=_("Worker"))
    tracked_time = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_("Tracked time"))
    work_date = models.DateField(verbose_name=_("Time of the work"))
    note = models.CharField(max_length=256, verbose_name=_("Note"))
    satisfaction = models.PositiveSmallIntegerField(choices=WORKLOGS_STATUS.CHOICES, verbose_name=_("Satisfaction"))

    @staticmethod
    @authenticated_users
    def has_create_permission(request):
        return True

    @staticmethod
    @authenticated_users
    def has_write_permission(request):
        return request.user.role in [ROLE.USER, ROLE.ACCOUNTING, ROLE.ADMIN]

    @authenticated_users
    def has_object_write_permission(self, request):
        if self.project_activity_step.status in ACTIVITY_STATUS.CLOSED:
            return False
        if request.user.role == ROLE.USER:
            # let's check for the PM and KAM first
            if (self.project_activity_step.activity.project.project_owner == request.user or
                    self.project_activity_step.activity.project.company.key_account_manager == request.user):
                if self.project_activity_step.status not in [ACTIVITY_STATUS.PLAN,
                                                             ACTIVITY_STATUS.WORK, ACTIVITY_STATUS.DONE]:
                    return False
                return True
            if self.project_activity_step.status not in [ACTIVITY_STATUS.PLAN, ACTIVITY_STATUS.WORK]:
                return False

            return self.worker == request.user
        if self.project_activity_step.status not in [ACTIVITY_STATUS.PLAN, ACTIVITY_STATUS.BILLABLE,
                                                     ACTIVITY_STATUS.WORK, ACTIVITY_STATUS.DONE]:
            return False
        return True

    @staticmethod
    @authenticated_users
    def has_destroy_permission(request):
        return request.user.role in [ROLE.USER]

    @authenticated_users
    def has_object_destroy_permission(self, request):
        if self.project_activity_step.status not in [ACTIVITY_STATUS.PLAN, ACTIVITY_STATUS.WORK]:
            return False
        return self.worker == request.user

    class Meta:
        verbose_name = _("Work Log")
        verbose_name_plural = _("Work Logs")

    def __str__(self):
        return "{} : {}".format(self.worker.name, self.tracked_time)


class ProjectArticle(ActivityOrArticle):
    """
        Project articles are Articles added to a Project. Those are products where now actual work is involved
        They can be used as Offer Articles in an Offer and will be invoiced as Invoiced Articles in an Invoice.
    """

    PROTECTED_LIST = [*ActivityOrArticle.PROTECTED_LIST, 'is_external_cost', 'budget_in_project_currency', 'allocation']

    project = models.ForeignKey(Project, related_name='articles')
    budget_in_project_currency = MoneyField(max_digits=10, decimal_places=2, default_currency="CHF")
    used_template_article = models.ForeignKey(TemplateArticle, null=True, blank=True)
    article_group = models.ForeignKey(ArticleGroup)
    is_external_cost = models.BooleanField(default=False)
    allocation = models.ForeignKey(Allocation, null=True)

    class Meta:
        verbose_name = _("Project Article")
        verbose_name_plural = _("Project Articles")

    @property
    def name(self):
        if self.display_name:
            return self.display_name
        return self.used_template_article.name

    @property
    def sent_in_project_offers(self):
        """Return:
        None or a list of Project offers with UUID+Name,
        where the Project activity/article was sent in
        """
        if self.get_sent_in_project_offers():
            return self.get_sent_in_project_offers()
        return None

    def get_sent_in_project_offers(self):
        """Return:
        None or a list of Project offers with UUID+Name,
        where the Project activity/article was sent in
        """
        offer_states = [OfferStatus.SENT, OfferStatus.ACCEPTED]
        return [article_offered.offer for article_offered in
                self.articleoffered_set.filter(offer__offer_status__in=offer_states)]

    def get_accepted_or_declined_item_offered(self):
        # Filter for the first ACCEPTED One
        result = self.articleoffered_set.filter(
            offer__offer_status__in=[OfferStatus.ACCEPTED, OfferStatus.DECLINED]
        ).order_by('-updated_at').last()
        return result

    def change_status_to_accepted(self):
        if self.offer_status == ActivityArticleOfferedStatus.SENT:
            self.offer_status = ActivityArticleOfferedStatus.ACCEPTED
            self.save()
            self.articleoffered_set.filter(
                offer_status=ActivityArticleOfferedStatus.SENT
            ).update(offer_status=ActivityArticleOfferedStatus.ACCEPTED)

    def change_status_to_replaced(self):
        if self.offer_status == ActivityArticleOfferedStatus.SENT:
            modified_kwargs = dict([
                ("version", self.version + 1),
                ('offer_status', ActivityArticleOfferedStatus.IMPROVABLE),
                ('replaced_item', self)
            ])
            clone = self.clone(**modified_kwargs)
            self.move_expenses_to_other_article(clone)
            self.offer_status = ActivityArticleOfferedStatus.REPLACED
            self.save()

    def can_be_replaced_or_declined(self, offer_item=None):
        """You are not allowed to REPLACE an Article in Offer_A if
        it is still SENT in an Offer_B (and not yet `accepted` or `declined`).
        If the Offer Article of Offer_A is still in another Offer_B (via its Project Article)

        When an Article was declined in Offer_A and there is
        another Offer_B already, the Project Article will keep the status SENT
        It will only move to `declined` if it was declined in all Offers (!!!).
        As soon as it is `declined`, it can not be used for an Offer C.
        :return: Boolean value
        """
        if offer_item:
            article_offer_status = self.articleoffered_set.filter(
                offer_status=ActivityArticleOfferedStatus.SENT
            ).exclude(pk=offer_item.pk)
            if not article_offer_status:
                return True
        # TODO: Decide wether to raise Error OR fail silently to change the article
        raise ValidationError({
            "offer_status": _(
                f"'offer_status' for ArticleProject can't be changed to while other dependent ArticleOffers are 'SENT' for aproval.")
        })

    # TODO: Implement `move_expenses_to_other_article()`
    def move_expenses_to_other_article(self, article=None):
        # does nothing for now. => TBD
        pass

    def already_accepted(self):
        for article_offered in self.articleoffered_set.all():
            if article_offered.offer.offer_status == OfferStatus.ACCEPTED:
                return article_offered.offer
        return None

    def change_status_to_improved(self, from_article_offered=False):
        self.offer_status = ActivityArticleOfferedStatus.IMPROVED
        self.save()
        if not from_article_offered:
            [article.change_status_to_improved(True) for
             article in self.articleoffered_set.filter(
                offer_status=ActivityArticleOfferedStatus.IMPROVABLE
            )]

    @property
    def total_planned_budget(self):
        return self.budget_in_project_currency

    @property
    def total_real_cost(self):
        result = sum(x.cost_in_default_currency for x in self.article_expenses.all())
        return result or Money(0, self.currency)

    def duplicate_under_new_project(self, cloned_project):
        if cloned_project and isinstance(cloned_project, Project):
            modified_kwargs = dict([
                ("version", 1),
                ('offer_status', ActivityArticleOfferedStatus.CREATED),
                ('project', cloned_project),
                ('created_at', datetime.today())
            ])
            clone = self.clone(**modified_kwargs)


class ProjectArticleExpense(UserAccessMetronomBaseModel):
    name = models.CharField(verbose_name=_('Name'), max_length=128, blank=True)
    description = models.TextField(verbose_name=_('Description'))
    article = models.ForeignKey(to=ProjectArticle, verbose_name=_('Project article'), related_name='article_expenses')
    company = models.ForeignKey(to=Company, verbose_name=_('Company'), related_name='company_article_expenses',
                                null=True, blank=True)
    is_company_selected = models.BooleanField(verbose_name=_('Is company selected'), default=False)
    cost_in_receipt_currency = MoneyField(verbose_name=_('Cost in receipt currency'), max_digits=10, decimal_places=2)
    receipt_currency = models.CharField(verbose_name=_('Receipt currency'), max_length=3, choices=CURRENCY.CHOICES,
                                        default=CURRENCY.CHF)
    expense_date = models.DateField(verbose_name=_('Expense date'), null=True, blank=True)

    class Meta:
        verbose_name = _('Project Article Expense')
        verbose_name_plural = _('Project Article Expenses')

    def __str__(self):
        return f'{self.name}'

    @property
    def cost_in_default_currency(self):
        result = convert_money(self.cost_in_receipt_currency.amount, self.receipt_currency, self.article.currency)
        return result or Money(0, self.article.currency)


@receiver(post_save, sender=ProjectActivity)
@receiver(post_save, sender=ProjectArticle)
def position_change_signal(sender, instance, created, **kwargs):
    """ We need this signal to assign appropriate position for the ProjectActivity and Project Article objects.
    """
    if created:
        # because of uuid instead of ids we shouldn't care about the possible collision between ids
        last_position_activity = instance.project.activities.all(
            force_visibility=True).exclude(id=instance.id).order_by('-position').first()
        last_position_activity = last_position_activity.position if last_position_activity else 0
        last_position_article = instance.project.articles.all(
            force_visibility=True).exclude(id=instance.id).order_by('-position').first()
        last_position_article = last_position_article.position if last_position_article else 0
        position = last_position_activity if last_position_article < last_position_activity else last_position_article
        instance.position = position + 1
        instance.save(update_fields=['position'])


@receiver(post_save, sender=ProjectArticle)
def currency_set_for_the_article_signal(sender, instance, created, **kwargs):
    """ We need this signal to assign appropriate currency for the ProjectArticle budget_in_project_currency field
    """
    if created:
        instance.budget_in_project_currency = Money(
            instance.budget_in_project_currency.amount, instance.project.billing_currency
        )
        instance.save()
