""" Base serializers to inherit from for the projects and related things
"""
from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.db.models import F
from django.utils.translation import ugettext_lazy as _
from djmoney.contrib.django_rest_framework.fields import MoneyField
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from apps.activities.models import (ActivityGroup, Rate, Step,
                                    TemplateActivityStep)
from apps.api.exceptions import ObjectValidationError
from apps.metronom_commons.data import ACTIVITY_STATUS
from apps.metronom_commons.serializers import SoftDeletionSerializer
from apps.projects.models import (ActivityAssignment, Project, ProjectActivity,
                                  ProjectActivityStep)

User = get_user_model()


class BaseOrderingSerializer(serializers.ModelSerializer):
    """ This is base serializer to handle ordering of the ProjectActivity and ProjectArticle inside the Project
    """
    uuid = serializers.UUIDField(source="id", required=True)
    new_position = serializers.IntegerField(required=True, write_only=True)
    old_position = serializers.IntegerField(required=True, write_only=True)

    class Meta:
        fields = ['uuid', 'new_position', 'old_position']

    def _get_object_by_position(self, project, position):
        activity = project.activities.filter(position=position).first()
        article = project.articles.filter(position=position).first()
        return activity or article

    def validate(self, data):
        project = self.Meta.model.objects.get(id=data['id']).project
        old_position_object = self._get_object_by_position(project, data['old_position'])
        if old_position_object is None:
            raise serializers.ValidationError(_("Object with this old_position doesn't exists"))
        if old_position_object.id != data['id']:
            raise serializers.ValidationError(_("Looks like position already changed for this object"))
        new_position_object = self._get_object_by_position(project, data['new_position'])
        if new_position_object is None:
            raise serializers.ValidationError(_("No object to move into position"))
        return data

    def update(self, instance, data):
        # so basically we need to do next:
        # 1. If new_position is greater than old one, we need to move all related positions upwards
        # 2. If new_position is less than old_one, we need to move all related positions downwards
        instance.position = data['new_position']
        project = instance.project
        if data['new_position'] > data['old_position']:
            # we should move only affected positions
            project.activities.exclude(id=instance.id).filter(
                position__lte=data['new_position'],
                position__gt=data['old_position']).update(position=F('position') - 1)
            project.articles.exclude(id=instance.id).filter(
                position__lte=data['new_position'],
                position__gt=data['old_position']).update(position=F('position') - 1)
        else:
            project.activities.exclude(id=instance.id).filter(
                position__gte=data['new_position'],
                position__lt=data['old_position']).update(position=F('position') + 1)
            project.articles.exclude(id=instance.id).filter(
                position__gte=data['new_position'],
                position__lt=data['old_position']).update(position=F('position') + 1)
        instance.save(update_fields=['position'])
        return instance


class BaseSimpleSerializer(serializers.ModelSerializer):
    """ This is abstract class for our simple serializers.
    """

    uuid = serializers.UUIDField(source="id", required=True)

    def validate_uuid(self, data):
        if not self.Meta.model.objects.filter(id=data).exists():
            raise ObjectValidationError(
                _("There is no {} with such an uuid".format(self.Meta.model.__name__)), identifier=data)
        return data


class SimpleActivityGroupSerializer(BaseSimpleSerializer):
    """ Simple serializer to use with ProjectActivitySerializer
    """

    class Meta:
        model = ActivityGroup
        fields = ['uuid', 'name']
        extra_kwargs = {
            'name': {'read_only': True}
        }


class SimpleWorkerSerializer(serializers.ModelSerializer):
    """ This is serializer for project app """
    uuid = serializers.UUIDField(required=True, source="id")

    def validate_uuid(self, data):
        if not User.objects.filter(id=data).exists():
            raise serializers.ValidationError(
                _("There is no User with such an uuid"))
        return data

    class Meta:
        model = User
        fields = ('uuid',)


class SimpleUserSerializer(serializers.ModelSerializer):
    """ This is serializer for project app """
    name = serializers.SerializerMethodField()
    uuid = serializers.UUIDField(source="id")

    class Meta:
        model = User
        fields = ('uuid', 'name', 'public_id')

    def get_name(self, obj):
        return f'{obj.first_name} {obj.last_name}'


class ActivityAssignmentSerializer(BaseSimpleSerializer):
    uuid = serializers.UUIDField(source="id", required=False)
    worker = SimpleWorkerSerializer(required=False)
    deleted = serializers.BooleanField(write_only=True, required=False)

    class Meta:
        model = ActivityAssignment
        fields = ['uuid', 'worker', 'accepted', 'status', 'deleted']
        extra_kwargs = {
            'accepted': {'read_only': True},
        }

    def validate(self, data):
        if data.get('id'):
            if set(['id', 'status']) != set(data.keys()) and set(['id', 'deleted']) != set(data.keys()):
                raise serializers.ValidationError(_("You can't edit assignment, only change status"))
        return data

    def create(self, data):
        data['worker'] = User.objects.get(id=data['worker']['id'])
        return super().create(data)

    def update(self, instance, data):
        if data.get('deleted'):
            instance.delete()
            return
        return super().update(instance, data)


class RateSerializer(serializers.ModelSerializer):
    """ Nested serializer for the StepDetailSerializer
    """
    remove = serializers.NullBooleanField(required=False, write_only=True)
    id = serializers.UUIDField(required=False)

    class Meta:
        model = Rate
        fields = [
            'id',
            'currency',
            'daily_rate',
            'hourly_rate',
            'remove'
        ]
        extra_kwargs = {'currency': {'required': True}}


class SimpleTemplateActivityStepSerializer(BaseSimpleSerializer):
    class Meta:
        model = TemplateActivityStep
        fields = ['uuid']


class SimpleStepSerializer(BaseSimpleSerializer):
    # TODO: Added name + rates for testing, we might remove it later
    name = serializers.CharField(read_only=True, validators=[UniqueValidator(queryset=Step.objects.all())])
    rates = RateSerializer(many=True, read_only=True)

    class Meta:
        model = Step
        fields = ['uuid', 'name', 'rates']

    def validate_uuid(self, data):
        if not Step.objects.filter(id=data).exists():
            raise ObjectValidationError(_("There is no Step with such an uuid"), identifier=data)
        return data


class SimpleProjectActivityStepSerializer(BaseSimpleSerializer):
    class Meta:
        model = ProjectActivityStep
        fields = ['uuid']


class ProjectActivityStepSerializer(BaseSimpleSerializer):
    uuid = serializers.UUIDField(source="id", required=False)
    assignments = ActivityAssignmentSerializer(many=True, required=False)
    used_step = SimpleStepSerializer()
    used_template_activity_step = SimpleTemplateActivityStepSerializer(required=False)
    planned_budget = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    planned_budget_currency = serializers.CharField(source='planned_budget.currency', read_only=True)
    planned_effort_in_hours = serializers.SerializerMethodField(read_only=True)
    invoiced_effort = serializers.SerializerMethodField(read_only=True)
    dependency_position = serializers.IntegerField(required=False, allow_null=True)
    real_cost = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    real_cost_currency = serializers.CharField(source='real_cost.currency', read_only=True)
    invoiced_cost = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    invoiced_cost_currency = serializers.CharField(source='invoiced_cost.currency', read_only=True)
    name = serializers.CharField(read_only=True)
    deleted = serializers.BooleanField(write_only=True, required=False)
    position = serializers.IntegerField(required=True)

    class Meta:
        model = ProjectActivityStep
        fields = ['uuid',
                  'used_template_activity_step',
                  'name',
                  'planned_effort',
                  'status',
                  'position',
                  'invoiced_effort',
                  'start_date',
                  'end_date',
                  'assignments',
                  'used_step',
                  'used_name',
                  'unit',
                  'dependency_position',
                  'planned_budget',
                  'planned_budget_currency',
                  'real_effort_in_hours',
                  'planned_effort_in_hours',
                  'real_cost',
                  'real_cost_currency',
                  'invoiced_effort',
                  'invoiced_cost',
                  'invoiced_cost_currency',
                  'deleted'
                  ]

    def get_invoiced_effort(self, obj):
        return str(obj.invoiced_effort)

    def get_planned_effort_in_hours(self, obj):
        return str(obj.planned_effort_in_hours)

    def validate(self, data):
        if data.get("deleted") and data.get('id'):
            instance = ProjectActivityStep.objects.get(id=data['id'])
            if instance.hour_logs.exists():
                raise serializers.ValidationError(_("You can't delete step with logged hours"))
        if data.get("end_date") and data.get("start_date") and data.get("end_date") < data.get("start_date"):
            raise serializers.ValidationError(_("End date should be greater than start date"))
        if data.get('used_step'):
            data['used_step'] = Step.objects.get(id=data['used_step']['id'])
            project = Project.objects.get(id=self.context['project'])
            rates = data['used_step'].rates.filter(currency=project.billing_currency)
            if not rates.exists():
                raise ObjectValidationError(
                    _("There is no rate for such project currency for this step"), identifier=data['used_step'])
            if data.get('unit'):
                if (data['unit'] == "daily" and not rates.filter(daily_rate__isnull=False).exists()) or (
                        data['unit'] == "hourly" and not rates.filter(hourly_rate__isnull=False).exists()):
                    raise serializers.ValidationError(_("There is no rate for such a unit for this step"))
        if data.get('unit') and not data.get('used_step'):
            raise serializers.ValidationError(_("You need to send step as well to change unit"))
        if data.get('assignments'):
            assignments = data.get('assignments')
            # get data to check uniqueness
            ids = [x.get('worker', {}).get('id') for x in assignments]
            ids = list(filter(None, ids))  # remove all None form the list
            # if we delete/create in the same time, we should process this situation as well
            exclude_assignment_ids = [x.get('id') for x in assignments if x.get("deleted")]
            if set([x for x in ids if ids.count(x) > 1]):
                raise serializers.ValidationError(_("You can't assign the same worker to the activity step twice"))
            if data.get('id'):
                instance = ProjectActivityStep.objects.get(id=data['id'])
                user_qs = User.objects.filter(id__in=ids)
                qs = ActivityAssignment.objects.filter(
                    worker__in=user_qs,
                    project_activity_step=instance
                ).exclude(id__in=exclude_assignment_ids)
                if qs.exists():
                    raise serializers.ValidationError(_("You can't assign the same worker to the activity step twice"))
        return data

    def create(self, data):
        data['activity'] = ProjectActivity.objects.get(pk=self.context['activity'])
        if data.get('used_template_activity_step'):
            data['used_template_activity_step'] = TemplateActivityStep.objects.get(
                id=data['used_template_activity_step']['id'])
        assignments_data = data.pop("assignments", [])
        dependency_position = data.pop("dependency_position", -1)
        if dependency_position != -1 and dependency_position:
            data["dependency"] = ProjectActivityStep.objects.get(
                position=dependency_position, activity=data['activity'])
        instance = super().create(data)
        for assignment_data in assignments_data:
            assignment_data['project_activity_step'] = instance
            serializer = ActivityAssignmentSerializer(data=assignment_data)
            serializer.create(assignment_data)
        return instance

    def update(self, instance, data, **kwargs):
        """
        :param instance: <ProjectActivityStep: model instance>
        :type instance: object
        :param data: Serialized <ProjectActivityStep: model instance>
        :type data: dict
        :param kwargs: dict("user": request.user)
        :type kwargs: dict
        :return: object
        :rtype: object
        """
        assignments_data = data.pop("assignments", [])
        for assignment_data in assignments_data:
            assignment_data['project_activity_step'] = instance
            serializer = ActivityAssignmentSerializer(data=assignment_data)
            if not assignment_data.get("id"):
                serializer.create(assignment_data)
            else:
                assignment = ActivityAssignment.objects.get(id=assignment_data.get("id"))
                serializer.update(assignment, assignment_data)
        if data.get('deleted'):
            instance.delete()
            return
        else:
            dependency_position = data.pop("dependency_position", -1)
            if dependency_position != -1 and dependency_position:
                data["dependency"] = ProjectActivityStep.objects.get(
                    position=dependency_position, activity=instance.activity)
            elif dependency_position is None:
                data["dependency"] = None
            # Do the Permissions according to `User` and `instance.status`
            kwargs['status'] = data.get("status", None)
            if data.get("status", None) == ACTIVITY_STATUS.CANCEL and \
                    instance.status != ACTIVITY_STATUS.CANCEL and instance.hour_logs.count():
                raise ObjectValidationError({
                    "status": f"You cannot change status to CANCEL for ActivityStep with worked hours unbilled"
                })
            if data.get("status", None) and not instance.user_is_allowed_to_change_status(**kwargs):
                raise PermissionDenied
            instance = super().update(instance, data)
            return instance


class BaseProjectActivitySerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    """
    This is base serializer for the ProjectActivity model
    """
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)
    activity_group = SimpleActivityGroupSerializer()
    activity_steps = ProjectActivityStepSerializer(many=True, required=False)
    name = serializers.SerializerMethodField(read_only=True)
    offer_status = serializers.SerializerMethodField(read_only=True)

    total_planned_effort_in_hours = serializers.SerializerMethodField(read_only=True)
    total_real_effort_in_hours = serializers.SerializerMethodField(read_only=True)
    total_invoiced_effort = serializers.SerializerMethodField(read_only=True)

    total_planned_budget = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_planned_budget_currency = serializers.CharField(source='total_planned_budget.currency', read_only=True)
    total_real_cost = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_real_cost_currency = serializers.CharField(source='total_real_cost.currency', read_only=True)
    total_invoiced_cost = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_invoiced_cost_currency = serializers.CharField(source='total_invoiced_cost.currency', read_only=True)

    class Meta:
        model = ProjectActivity
        fields = [
            "activity_group",
            "activity_steps",
            "description",
            "display_name",
            "name",
            "offer_status",
            "end_date",
            "start_date",
            "total_planned_budget_currency",
            "total_planned_budget",
            "total_planned_effort_in_hours",
            "total_real_effort_in_hours",
            "total_real_cost",
            "total_real_cost_currency",
            "total_invoiced_cost",
            "total_invoiced_cost_currency",
            "total_invoiced_effort",
            "uuid",
            "use_count"
        ]

        read_only_fields = ('use_count',)

    def validate(self, data):
        activity_steps = data.get('activity_steps', [])
        all_created_positions = [x.get('position') for x in activity_steps]
        for step_data in activity_steps:
            dep_position = step_data.get('dependency_position', -1)
            if dep_position != -1 and dep_position:
                if self.instance:
                    if (dep_position not in all_created_positions and
                            not ProjectActivityStep.objects.filter(activity=self.instance,
                                                                   position=dep_position).exists()):
                        raise serializers.ValidationError(
                            _("There is no activity step with such position to depends on"))
                else:
                    if dep_position not in all_created_positions:
                        raise serializers.ValidationError(
                            _("There is no activity step with such position to depends on"))
                if dep_position == step_data.get('position'):
                    raise serializers.ValidationError(
                        _("Activity step can't depends on itself"))
        if set([x for x in all_created_positions if all_created_positions.count(x) > 1]):
            raise serializers.ValidationError(_("Position should be unique"))
        if self.instance:
            all_steps_data = self.instance.activity_steps.values("id", "position")
            for step_data in activity_steps:
                position = step_data.get('position')
                uuid = step_data.get('id')
                if position:
                    duplicate_check = [x for x in all_steps_data if x.get('position', -1) == position]
                    if duplicate_check:
                        duplicate_uuid = duplicate_check[0]['id']
                        if duplicate_uuid != uuid:
                            activity_object = [x for x in activity_steps if x.get('id', -1) == duplicate_uuid]
                            if (not activity_object or
                                    (activity_object and (activity_object[0].get('position') is None
                                                          or activity_object[0].get('position') == position))):
                                raise serializers.ValidationError(
                                    _("You need to send all Steps, where a position was changed by another Step")
                                )

        return data

    def get_name(self, obj):
        return str(obj.name)

    def get_offer_status(self, obj):
        return str(obj.offer_status)

    def get_total_planned_effort_in_hours(self, obj):
        return str(obj.total_planned_effort_in_hours)

    def get_total_real_effort_in_hours(self, obj):
        return str(obj.total_real_effort_in_hours)

    def get_total_invoiced_effort(self, obj):
        return str(obj.total_invoiced_effort)
