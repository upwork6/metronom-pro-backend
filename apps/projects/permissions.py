""" Contains projects-specific permissions
"""

from rest_framework.permissions import DjangoObjectPermissions

from apps.projects.models import Project


class ProjectActivityArticlePermission(DjangoObjectPermissions):
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': ['%(app_label)s.view_%(model_name)s'],
        'HEAD': ['%(app_label)s.view_%(model_name)s'],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }

    def has_permission(self, request, view):
        # Workaround to ensure DjangoModelPermissions are not applied
        # to the root view when using DefaultRouter.
        if getattr(view, '_ignore_model_permissions', False):
            return True

        if not request.user or (
           not request.user.is_authenticated() and self.authenticated_users_only):
            return False

        queryset = self._queryset(view)
        perms = self.get_required_permissions(request.method, queryset.model)

        if request.method in ["POST", "PATCH"]:
            # we need to ensure that this request.user have permissions OR is PM or KAM
            project_pk = view.kwargs.get("project_pk")
            project_qs = Project.objects.filter(id=project_pk)
            if project_qs.exists():
                project = project_qs.last()
                if request.user.has_perms(perms) or request.user.has_perm('projects.change_project', project):
                    return True

        return request.user.has_perms(perms)
