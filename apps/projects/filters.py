import operator
from functools import reduce
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from rest_framework import filters
from rest_framework.exceptions import ValidationError

from apps.metronom_commons.data import WORKLOGS_STATUS
from apps.projects.models import Project
from apps.metronom_commons.filters import get_value_list, validate_value, get_value_or_value_list
from apps.metronom_commons.utils import cast_bool


class WorkLogFilterBackend(filters.BaseFilterBackend):
    """
        This filter set will return a list of all work logs determined by
        the query string portion of the url.
        If no query string is found, all work logs are returned
    """

    def filter_work_date_queryset(self, request, queryset):
        start_work_date = request.query_params.get('start_work_date', None)
        end_work_date = request.query_params.get('end_work_date', None)

        if start_work_date and end_work_date:
            if end_work_date < start_work_date:
                raise ValidationError(_('The "end work date" cannot be earlier than the "start work date"'))
            else:
                queryset = queryset.filter(work_date__range=(start_work_date, end_work_date))

        elif start_work_date:
            queryset = queryset.filter(work_date__gte=start_work_date)
        elif end_work_date:
            queryset = queryset.filter(work_date__lte=end_work_date)

        return queryset

    def filter_log_date_queryset(self, request, queryset):
        start_log_datetime = request.query_params.get('start_log_datetime', None)
        end_log_datetime = request.query_params.get('end_log_datetime', None)

        if start_log_datetime and end_log_datetime:
            if end_log_datetime < start_log_datetime:
                raise ValidationError(_('The "end log datetime" cannot be earlier than the "start log datetime"'))
            else:
                queryset = queryset.filter(created_at__range=(start_log_datetime, end_log_datetime))
        elif start_log_datetime:
            queryset = queryset.filter(created_at__gte=start_log_datetime)
        elif end_log_datetime:
            queryset = queryset.filter(created_at__lte=end_log_datetime)

        return queryset

    def filter_queryset(self, request, queryset, view):

        # TODO: Add validators to "uuid" for all uuids

        user_uuid = validate_value(request.query_params.get('user_uuid', None), "uuid")
        project_activity_step_uuid = request.query_params.get('project_activity_step_uuid', None)
        project_activity_uuid = request.query_params.get('project_activity_uuid', None)
        project_uuid = request.query_params.get('project_uuid', None)
        campaign_uuid = request.query_params.get('campaign_uuid', None)
        company_uuid = request.query_params.get('company_uuid', None)
        satisfaction = request.query_params.get('satisfaction', None)

        workers = get_value_list(request.query_params.get('workers', None))
        projects = get_value_list(request.query_params.get('projects', None))
        project_activities = get_value_list(request.query_params.get('project_activities', None))
        project_activity_steps = get_value_list(request.query_params.get('project_activity_steps', None))

        try:
            if satisfaction and int(satisfaction) in [k for k, v in WORKLOGS_STATUS.CHOICES]:
                queryset = queryset.filter(satisfaction=satisfaction)
        except ValueError as ve:
            raise ValidationError(detail=ve)

        if user_uuid:
            queryset = queryset.filter(worker=user_uuid)

        if project_activity_step_uuid:
            queryset = queryset.filter(project_activity_step=project_activity_step_uuid)

        if project_activity_uuid:
            queryset = queryset.filter(project_activity_step__activity=project_activity_uuid)

        if project_uuid:
            queryset = queryset.filter(project_activity_step__activity__project=project_uuid)

        if projects:

            query = reduce(operator.or_, (
                Q(project_activity_step__activity__project__title__icontains=item) |
                Q(project_activity_step__activity__project__pk__startswith=item) |
                Q(project_activity_step__activity__project__public_id__iexact=item) for item in projects))

            queryset = queryset.filter(query)

        if project_activities:

            query = reduce(operator.or_, (
                Q(project_activity_step__activity__display_name__icontains=item) |
                Q(project_activity_step__activity__used_template_activity__name__istartswith=item) |
                Q(project_activity_step__activity__pk__startswith=item) for item in project_activities))

            queryset = queryset.filter(query)

        if project_activity_steps:

            query = reduce(operator.or_, (
                Q(project_activity_step__used_name__icontains=item) |
                Q(project_activity_step__used_step__name__istartswith=item) |
                Q(project_activity_step__pk__startswith=item) for item in project_activity_steps))

            queryset = queryset.filter(query)

        if workers:

            query = reduce(operator.or_, (
                Q(worker__username__icontains=item) |
                Q(worker__first_name__icontains=item) |
                Q(worker__last_name__icontains=item) |
                Q(worker__pk__startswith=item) |
                Q(worker__person_in_crm__public_id__iexact=item) for item in workers))

            queryset = queryset.filter(query)

        queryset = self.filter_work_date_queryset(request=request, queryset=queryset)
        queryset = self.filter_log_date_queryset(request=request, queryset=queryset)

        # value_list = value.split(u',')

        # Project-based filtering for the Worklog

        projects_queryset = Project.objects.all()
        use_projects_filter = False

        if campaign_uuid:
            projects = projects_queryset.filter(campaign=campaign_uuid)
            use_projects_filter = True

        if company_uuid:
            projects = projects_queryset.filter(company=company_uuid)
            use_projects_filter = True

        if use_projects_filter:
            queryset = queryset.filter(project_activity_step__activity__project__in=projects)

        return queryset


class ProjectAndCampaignCommonFilterBackend(filters.BaseFilterBackend):
    """
        This filter set will filter Campaigns (used in /api/campaigns/)
    """

    def filter_queryset(self, request, queryset, view):

        title = request.query_params.get('title', None)
        start_date_lte = request.query_params.get('start_date_lte', None)
        start_date_gte = request.query_params.get('start_date_gte', None)
        finishing_date_gte = request.query_params.get('finishing_date_gte', None)
        finishing_date_lte = request.query_params.get('finishing_date_lte', None)
        real_budget_lte = request.query_params.get('real_budget_lte', None)
        real_budget_gte = request.query_params.get('real_budget_gte', None)
        billing_currency = request.query_params.get('billing_currency', None)
        company_uuid = request.query_params.get('company_uuid', None)
        contact_person_uuid = request.query_params.get('contact_person_uuid', None)
        assigned_to_user_uuid = request.query_params.get('assigned_to_user_uuid', None)

        contact_persons = get_value_list(request.query_params.get('contact_persons', None))
        companies = get_value_list(request.query_params.get('companies', None))

        # check_uuids([company_uuid, contact_person_uuid, assigned_to_user_uuid])

        if title:
            queryset = queryset.filter(title__icontains=title)

        if billing_currency:
            queryset = queryset.filter(billing_currency__iexact=billing_currency)

        if company_uuid:
            queryset = queryset.filter(company__id=company_uuid)

        if contact_person_uuid:
            queryset = queryset.filter(contact_person__id=contact_person_uuid)

        if contact_persons:

            query = reduce(operator.or_, (
                Q(contact_person__first_name__icontains=item) |
                Q(contact_person__last_name__icontains=item) |
                Q(contact_person__landline_phone__istartswith=item) |
                Q(contact_person__mobile_phone__istartswith=item) |
                Q(contact_person__email__email__icontains=item) |
                Q(contact_person__public_id__iexact=item) |
                Q(contact_person__id__startswith=item) for item in contact_persons))

            queryset = queryset.filter(query)

        if companies:

            query = reduce(operator.or_, (
                Q(company__name__icontains=item) |
                Q(company__public_id__iexact=item) |
                Q(company__id__startswith=item) for item in companies))

            queryset = queryset.filter(query)

        if assigned_to_user_uuid:
            queryset = queryset.filter(activities__activity_steps__assignments__worker_id=assigned_to_user_uuid).distinct()

        if start_date_gte:
            queryset = queryset.filter(start_date__gte=start_date_gte)

        if start_date_lte:
            queryset = queryset.filter(start_date__lte=start_date_lte)

        # TODO: Change this when total_ logic is changed
        if real_budget_lte:
            pass

        if real_budget_gte:
            pass

        if finishing_date_gte:
            queryset = queryset.filter(finishing_date__gte=finishing_date_gte)

        if finishing_date_lte:
            queryset = queryset.filter(finishing_date__lte=finishing_date_lte)

        return queryset


class ProjectFilterBackend(filters.BaseFilterBackend):
    """
        This filter set will filter Projects (used in /api/projects/)
    """

    def filter_queryset(self, request, queryset, view):

        project_owner_uuid = request.query_params.get('project_owner_uuid', None)
        accounting_type = request.query_params.get('accounting_type', None)
        belongs_to_campaign = request.query_params.get('belongs_to_campaign', False)

        project_owners = get_value_list(request.query_params.get('project_owners', None))

        # check_uuids([project_owner_uuid])

        if project_owner_uuid:
            queryset = queryset.filter(project_owner__id=project_owner_uuid)

        if project_owners:

            query = reduce(operator.or_, (
                Q(project_owner__username__icontains=item) |
                Q(project_owner__first_name__icontains=item) |
                Q(project_owner__last_name__icontains=item) |
                Q(project_owner__pk__startswith=item) |
                Q(project_owner__person_in_crm__public_id__iexact=item) for item in project_owners))

            queryset = queryset.filter(query)

        if accounting_type:
            queryset = queryset.filter(accounting_type=accounting_type)

        if belongs_to_campaign:
            belongs_to_campaign = cast_bool(belongs_to_campaign)

            if belongs_to_campaign:
                queryset = queryset.exclude(campaign=None)
            else:
                queryset = queryset.filter(campaign=None)

        return queryset


class CampaignFilterBackend(filters.BaseFilterBackend):
    """
        This filter set will filter Campaigns (used in /api/campaigns/)
    """

    def filter_queryset(self, request, queryset, view):

        campaign_owner_uuid = request.query_params.get('campaign_owner_uuid', None)
        campaign_owners = get_value_list(request.query_params.get('campaign_owners', None))

        # check_uuids([campaign_owner_uuid])

        if campaign_owner_uuid:
            queryset = queryset.filter(campaign_owner__id=campaign_owner_uuid)

        if campaign_owners:

            query = reduce(operator.or_, (
                Q(campaign_owner__username__icontains=item) |
                Q(campaign_owner__first_name__icontains=item) |
                Q(campaign_owner__last_name__icontains=item) |
                Q(campaign_owner__pk__startswith=item) |
                Q(campaign_owner__person_in_crm__public_id__iexact=item) for item in campaign_owners))

            queryset = queryset.filter(query)

        return queryset
