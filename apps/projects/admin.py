
from django.contrib import admin

from apps.projects.models import (ActivityAssignment, Campaign, Project,
                                  ProjectActivity, ProjectActivityStep,
                                  ProjectArticle, WorkLog, ProjectArticleExpense)
from apps.metronom_commons.utils import CachingModelChoicesForm, CachingModelChoicesFormSet


class ProjectInline(admin.StackedInline):
    model = Project
    extra = 0


@admin.register(Campaign)
class CampaignAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'pk',
        'title',
        'company',
        'contact_person',
        'description',
        'customer_reference',
        'start_date',
        'finishing_date',
        'delivery_date',
        'billing_currency',
        'billing_address',
        'billing_agreement',
        'billing_interval',
        'number',
        'year_number',
        'campaign_owner',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'start_date',
        'finishing_date',
        'delivery_date',
        'campaign_owner',
        'billing_currency'
    )
    inlines = (
        ProjectInline,
    )


class ProjectActivityInline(admin.StackedInline):
    model = ProjectActivity
    extra = 0


class ProjectArticleInline(admin.StackedInline):
    model = ProjectArticle
    extra = 0


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'pk',
        'title',
        'company',
        'contact_person',
        'description',
        'customer_reference',
        'start_date',
        'finishing_date',
        'delivery_date',
        'billing_currency',
        'billing_address',
        'billing_agreement',
        'billing_interval',
        'number',
        'year_number',
        'campaign',
        'accounting_type',
        'project_owner',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'start_date',
        'finishing_date',
        'delivery_date',
        'billing_currency',
        'project_owner'
    )
    inlines = (
        ProjectActivityInline,
        ProjectArticleInline
    )


class ProjectActivityStepInlineForm(CachingModelChoicesForm):
    class Meta:
        model = ProjectActivityStep
        fields = [
            "position",
            "used_rate",
            "planned_effort",
            "invoiced_effort",
            "start_date",
            "end_date",
            "status",
            "dependency",
            "unit",
            "used_step",
            "used_template_activity_step"
        ]


class ProjectActivityStepInline(admin.TabularInline):
    model = ProjectActivityStep
    extra = 0
    form = ProjectActivityStepInlineForm
    formset = CachingModelChoicesFormSet


@admin.register(ProjectActivity)
class ProjectActivityAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'project',
        'display_name',
        'start_date',
        'end_date',
        'description',
        'used_template_activity',
        'activity_group',
        'status'
    )
    list_filter = (
        'created_at',
        'updated_at',
        'start_date',
        'end_date',
        'used_template_activity',
        'activity_group',
        'status'
    )
    inlines = (
        ProjectActivityStepInline,
    )


class WorklogInline(admin.TabularInline):
    model = WorkLog
    extra = 0


class AssignmentInline(admin.TabularInline):
    model = ActivityAssignment
    extra = 0


@admin.register(ProjectActivityStep)
class ProjectActivityStepAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'position',
        'used_step',
        'dependency',
        'unit',
        'activity',
        'name',
        'used_template_activity_step',
        'planned_effort',
        'invoiced_effort',
        'start_date',
        'end_date',
        'status',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'start_date',
        'end_date',
        'status',
    )
    search_fields = ('pk',)
    inlines = (
        AssignmentInline,
        WorklogInline,
    )


@admin.register(ActivityAssignment)
class ActivityAssignmentAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'worker',
        'project_activity_step',
        'seen_in_dashboard',
        'notification_sent',
        'accepted',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'worker',
        'project_activity_step',
        'seen_in_dashboard',
        'notification_sent',
    )


@admin.register(WorkLog)
class WorkLogAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'project_activity_step',
        'worker',
        'tracked_time',
        'work_date',
        'note',
        'satisfaction',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'worker',
        'work_date',
    )


@admin.register(ProjectArticle)
class ProjectArticleAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'project',
        'display_name',
        'budget_in_project_currency',
        'invoice_status',
        'description',
        'used_template_article',
        'article_group',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'project',
        'used_template_article',
        'article_group',
    )


@admin.register(ProjectArticleExpense)
class ProjectArticleExpenseAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'name',
        'description',
        'article',
        'company',
        'is_company_selected',
        'cost_in_receipt_currency',
        'receipt_currency',
        'cost_in_default_currency',
        'expense_date',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'article',
        'company',
        'is_company_selected',
        'expense_date',
    )
    search_fields = ('name',)
