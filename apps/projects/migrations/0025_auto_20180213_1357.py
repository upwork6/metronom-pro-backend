# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-02-13 13:57
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0024_remove_projectactivitystep_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='worklog',
            old_name='related_activity',
            new_name='project_activity_step',
        ),
    ]
