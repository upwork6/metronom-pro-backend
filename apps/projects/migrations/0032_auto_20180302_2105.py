# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-03-02 21:05
from __future__ import unicode_literals

import django.utils.timezone
import model_utils.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0031_projectactivity_used_project_activity'),
    ]

    operations = [
        migrations.AddField(
            model_name='campaign',
            name='is_void',
            field=models.BooleanField(default=False, verbose_name='Is in void?'),
        ),
        migrations.AddField(
            model_name='project',
            name='is_void',
            field=models.BooleanField(default=False, verbose_name='Is in void?'),
        ),
        migrations.AlterField(
            model_name='campaign',
            name='created_date',
            field=model_utils.fields.AutoCreatedField(
                default=django.utils.timezone.now, editable=False, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='campaign',
            name='updated_date',
            field=model_utils.fields.AutoLastModifiedField(
                default=django.utils.timezone.now, editable=False, verbose_name='modified'),
        ),
        migrations.AlterField(
            model_name='project',
            name='created_date',
            field=model_utils.fields.AutoCreatedField(
                default=django.utils.timezone.now, editable=False, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='project',
            name='updated_date',
            field=model_utils.fields.AutoLastModifiedField(
                default=django.utils.timezone.now, editable=False, verbose_name='modified'),
        ),
    ]
