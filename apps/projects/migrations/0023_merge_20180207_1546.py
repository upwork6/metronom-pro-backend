# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-02-07 15:46
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0021_auto_20180203_1434'),
        ('projects', '0022_display_name_not_required'),
    ]

    operations = [
    ]
