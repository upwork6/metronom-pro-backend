# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-02-15 13:56
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('projects', '0025_auto_20180213_1357'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='activityassignment',
            unique_together=set([('worker', 'project_activity_step')]),
        ),
    ]
