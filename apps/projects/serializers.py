
from datetime import date

from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from djmoney.contrib.django_rest_framework.fields import MoneyField
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from apps.accounting.models import Allocation
from apps.activities.models import ActivityGroup, TemplateActivity
from apps.addresses.models import Address
from apps.articles.models import ArticleGroup, TemplateArticle
from apps.companies.models import Company, Office
from apps.metronom_commons.choices_flow import ActivityArticleOfferedStatus
from apps.metronom_commons.data import ACTIVITY_STATUS
from apps.metronom_commons.serializers import SoftDeletionSerializer
from apps.offers.serializers import (CampaignOfferSerializer,
                                     OfferListSerializer,
                                     ProjectOfferSerializer)
from apps.persons.models import Person
from apps.projects.base_serializers import (BaseOrderingSerializer,
                                            BaseProjectActivitySerializer,
                                            BaseSimpleSerializer,
                                            ProjectActivityStepSerializer,
                                            SimpleUserSerializer)
from apps.projects.models import (Campaign, Project, ProjectActivity,
                                  ProjectActivityStep, ProjectArticle,
                                  ProjectArticleExpense, WorkLog)
from apps.projects.utils import project_validator
from apps.users.serializers import UserUUIDSerializer

User = get_user_model()


class SimpleTemplateActivitySerializer(BaseSimpleSerializer):
    """
    Simple serializer to use with ProjectActivitySerializer
    """

    class Meta:
        model = TemplateActivity
        fields = ['uuid', 'name']
        extra_kwargs = {
            'name': {'read_only': True}
        }


class SimpleProjectActivitySerializer(BaseSimpleSerializer):
    """
    Simple serializer to use with ProjectActivitySerializer
    """

    class Meta:
        model = ProjectActivity
        fields = ['uuid', 'name']
        read_only_fields = ['uuid', 'name']


class SimpleProjectActivityStepSerializer(BaseSimpleSerializer):
    """
    Simple serializer to use with ProjectActivitySerializer
    """

    class Meta:
        model = ProjectActivityStep
        fields = [
            'uuid',
            'name',
            'position',
        ]
        read_only_fields = [
            'uuid',
            'name',
            'position',
        ]


class ProjectActivitySerializer(BaseProjectActivitySerializer):
    used_template_activity = SimpleTemplateActivitySerializer(required=False)
    used_project_activity = SimpleProjectActivitySerializer(required=False, write_only=True)
    sent_in_project_offers = OfferListSerializer(many=True, read_only=True)
    final_state = serializers.BooleanField(source='get_final_state', read_only=True, required=False)
    final_state_information = serializers.DictField(
        source='get_final_state_information', read_only=True, required=False
    )

    class Meta(BaseProjectActivitySerializer.Meta):
        fields = BaseProjectActivitySerializer.Meta.fields + [
            "version",
            "sent_in_project_offers",
            "final_state",
            "final_state_information",
            "position",
            "used_template_activity",
            "used_project_activity",
            "status",
            # "invoice_status"
            # TODO #388: Put invoice_status back to the Serializer
        ]

        extra_kwargs = {
            # 'invoice_status': {'allow_null': True},
            'position': {'read_only': True},
            'version': {'read_only': True}
        }

    def create(self, data):
        # this fields are not nullable in the DB, but still can be omitted inside the request, so we need this small
        # check
        activity_steps_data = data.pop("activity_steps", [])
        # we have uuid of the project inside the url, so just make frontend not duplicate obvious thing to the backend
        # with extra post param
        data['project'] = Project.objects.get(id=self.context['project'])
        if data.get('used_template_activity'):
            data['used_template_activity'] = TemplateActivity.objects.get(id=data['used_template_activity']['id'])

        if 'used_project_activity' in data and 'id' in data.get('used_project_activity'):
            data['used_project_activity'] = ProjectActivity.objects.get(id=data.get('used_project_activity').get('id'))

        data['activity_group'] = ActivityGroup.objects.get(id=data['activity_group']['id'])
        instance = super().create(data)
        for data in activity_steps_data:
            serializer = ProjectActivityStepSerializer(data=data, context={'activity': instance.id})
            serializer.create(data)

        return instance

    def update(self, instance, data, **kwargs):
        """
        :param instance: ProjectActivity model instance
        :type instance: object
        :param data: Serialized ProjectActivity Object
        :type data: OrderedDict
        :param kwargs: {"user": request.user}
        :type kwargs: dict
        :return: serialized.object
        :rtype: object
        """
        activity_steps_data = data.pop("activity_steps", [])
        for activity_step_data in activity_steps_data:
            serializer = ProjectActivityStepSerializer(data=activity_step_data, context={'activity': instance.id})
            if not activity_step_data.get("id"):
                serializer.create(activity_step_data)
            else:
                activity_step = ProjectActivityStep.objects.get(id=activity_step_data.get("id"))
                serializer.update(activity_step, activity_step_data, **kwargs)

        if data.get('used_template_activity'):
            data['used_template_activity'] = TemplateActivity.objects.get(id=data['used_template_activity']['id'])

        if 'used_project_activity' in data and 'id' in data.get('used_project_activity'):
            data['used_project_activity'] = ProjectActivity.objects.get(id=data.get('used_project_activity').get('id'))

        if data.get('activity_group'):
            data['activity_group'] = ActivityGroup.objects.get(id=data['activity_group']['id'])
        if instance.offer_status == ActivityArticleOfferedStatus.IMPROVABLE:
            instance.change_status_to_improved()

        return super().update(instance, data)


class AddressSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        try:
            if obj.office:
                return obj.office.name
        except Office.DoesNotExist:
            pass
        return ''

    class Meta:
        model = Address
        fields = ['uuid', 'name']


class ProjectCompanySerializer(serializers.ModelSerializer):
    """ This is serializer for project app for company part
    """
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)

    class Meta:
        model = Company
        fields = ['uuid', 'public_id', 'name']


class SimpleCampaignSerializer(serializers.ModelSerializer):
    """ This is serializer for project app """
    uuid = serializers.UUIDField(source="id")

    class Meta:
        model = Campaign
        fields = ('uuid', 'title',)


class ProjectPersonSerializer(serializers.ModelSerializer):
    """ This is serializer for project app for contact person part
    """
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)
    email = serializers.SerializerMethodField()
    phone = PhoneNumberField(source="main_phone", read_only=True)

    def get_email(self, obj):
        return obj.email.email if obj.email else ""

    class Meta:
        model = Person
        fields = ['uuid', 'name', 'email', 'phone', 'public_id']


class CampaignOrProjectListSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    type = serializers.SerializerMethodField()

    def get_type(self, obj):
        if isinstance(obj, Project):
            return Project.__name__.lower()
        elif isinstance(obj, Campaign):
            return Campaign.__name__.lower()

        return obj.__class__.__name__.lower()

    class Meta(object):
        fields = ('type',)


class CampaignListSerializer(CampaignOrProjectListSerializer):
    company = ProjectCompanySerializer()
    contact_person = ProjectPersonSerializer()
    campaign_owner = SimpleUserSerializer()
    total_planned_budget = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_real_cost = MoneyField(max_digits=10, decimal_places=2, read_only=True)

    class Meta(CampaignOrProjectListSerializer.Meta):
        model = Campaign
        fields = CampaignOrProjectListSerializer.Meta.fields + (
            'id',
            'public_id',
            'campaign_owner',
            'title',
            'company',
            'start_date',
            'finishing_date',
            'contact_person',
            'billing_currency',
            'total_planned_budget',
            'total_real_cost'
        )

        read_only_fields = (
            'public_id',
        )

    @staticmethod
    def setup_eager_loading(queryset):
        """
        Perform necessary eager loading of data.
        """
        queryset = queryset.select_related(
            'company',
            'company__key_account_manager',
            'company__contact_ptr',
            'company__billing_address',
            'company__sector',
            'contact_person',
            'contact_person__email',
            'contact_person__contact_ptr',
            'billing_address',
            'campaign_owner',
        )

        queryset = queryset.prefetch_related(
            'projects',
            'projects__activities',
            'projects__articles',
        )

        return queryset


class ProjectCampaignDetailSerializer(serializers.ModelSerializer):
    """ This is serializer for project detail view for campaign part
    """
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)

    class Meta:
        model = Campaign
        fields = ['uuid', 'public_id', 'title']


class ProjectListSerializer(CampaignOrProjectListSerializer):
    company = ProjectCompanySerializer(read_only=True)
    contact_person = ProjectPersonSerializer(read_only=True)
    project_owner = SimpleUserSerializer(read_only=True)
    campaign_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Campaign.objects.all(),
        source="campaign",
        required=False,
        allow_null=True
    )

    total_planned_budget = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_real_cost = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    activities = serializers.SerializerMethodField()

    class Meta(CampaignOrProjectListSerializer.Meta):
        model = Project
        fields = CampaignOrProjectListSerializer.Meta.fields + (
            'id',
            'public_id',
            'title',
            'company',
            'project_owner',
            'start_date',
            'finishing_date',
            'accounting_type',
            'contact_person',
            'billing_currency',
            'total_planned_budget',
            'total_real_cost',
            'campaign_uuid',
            'activities'
        )

        read_only_fields = (
            'public_id',
        )

    @staticmethod
    def setup_eager_loading(queryset):
        """ Perform necessary eager loading of data. """
        # select_related for "to-one" relationships
        queryset = queryset.select_related(
            'company',
            'company__key_account_manager',
            'company__contact_ptr',
            'company__billing_address',
            'company__sector',
            'contact_person',
            'billing_address',
            'project_owner',
            'project_owner__person_in_crm',
            'campaign',
        )

        queryset = queryset.prefetch_related(
            'articles',
        )

        return queryset

    def get_activities(self, obj):
        request = self.context['request']

        # TODO: Remove the following check after Fixing request context in TestProjectDetailSerializer class
        if not hasattr(request, 'query_params'):
            return None

        include_activities = request.query_params.get('include_activities')
        if include_activities is not None:
            if isinstance(obj, Project):
                serializer = ProjectActivitySerializer(obj.activities, many=True)
                return serializer.data
        return None


class CampaignProjectListSerializer(ProjectListSerializer):
    accounting_type = serializers.SerializerMethodField()
    campaign = serializers.SerializerMethodField()
    campaign_uuid = serializers.SerializerMethodField()
    campaign_owner = serializers.SerializerMethodField()

    def get_accounting_type(self, obj):
        if isinstance(obj, Project):
            return obj.accounting_type
        elif isinstance(obj, Campaign):
            return None

    def get_campaign(self, obj):
        if isinstance(obj, Project) and obj.campaign:
            serializer = SimpleCampaignSerializer(obj.campaign)
            return serializer.data
        elif isinstance(obj, Campaign):
            return None

    def get_campaign_uuid(self, obj):
        if isinstance(obj, Project) and obj.campaign:
            serializer = SimpleCampaignSerializer(obj.campaign)
            return serializer.data["uuid"]
        elif isinstance(obj, Campaign):
            return None

    def get_campaign_owner(self, obj):
        if isinstance(obj, Project):
            return None
        elif isinstance(obj, Campaign):
            serializer = SimpleUserSerializer(obj.campaign_owner)
            return serializer.data

    class Meta(ProjectListSerializer.Meta):
        model = Project
        fields = ProjectListSerializer.Meta.fields + (
            'campaign',
            'campaign_uuid',
            'campaign_owner'
        )


class ProjectDetailSerializer(ProjectListSerializer):
    total_planned_budget_of_activities = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_planned_budget_of_articles = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_planned_budget_of_finished_activities = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_planned_budget_of_finished_articles = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_planned_budget_of_finished_items = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_real_cost_of_activities = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_real_cost_of_articles = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_real_cost_of_finished_activities = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_real_cost_of_finished_articles = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_real_cost_of_finished_items = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    project_offers = ProjectOfferSerializer(source="offers", many=True, read_only=True)

    billing_address = AddressSerializer(read_only=True)
    billing_address_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Address.objects.all(),
        source="billing_address",
        write_only=True
    )

    company_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Company.objects.all(),
        source="company",
        write_only=True
    )
    contact_person_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Person.objects.all(),
        source="contact_person",
        write_only=True
    )
    campaign_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Campaign.objects.all(),
        source="campaign",
        required=False,
        write_only=True,
        allow_null=True
    )
    project_owner_uuid = PrimaryKeyRelatedField(
        queryset=User.objects.all(),
        source="project_owner",
        write_only=True
    )

    campaign = ProjectCampaignDetailSerializer(read_only=True)

    class Meta(ProjectListSerializer.Meta):
        model = Project
        fields = ProjectListSerializer.Meta.fields + (
            'project_offers',
            'total_planned_budget_of_activities',
            'total_planned_budget_of_articles',
            'total_planned_budget_of_finished_activities',
            'total_planned_budget_of_finished_articles',
            'total_planned_budget_of_finished_items',
            'total_real_cost_of_activities',
            'total_real_cost_of_articles',
            'total_real_cost_of_finished_activities',
            'total_real_cost_of_finished_articles',
            'total_real_cost_of_finished_items',
            'total_number_of_activities',
            'total_number_of_finished_activities',
            'company_uuid',
            'contact_person_uuid',
            'campaign_uuid',
            'description',
            'customer_reference',
            'delivery_date',
            'project_owner_uuid',
            'billing_address_uuid',
            'billing_agreement',
            'billing_interval',
            'campaign',
            'billing_address',
            'icons'
        )

        read_only_fields = ProjectListSerializer.Meta.read_only_fields + (
            'total_number_of_activities',
            'total_number_of_finished_activities',
            'icons'
        )

    def validate(self, data):
        valid, errors = project_validator(instance=self.instance, data=data)
        if not valid:
            raise serializers.ValidationError(errors)
        return data


class CampaignProjectSerializer(ProjectListSerializer):
    project_offers = ProjectOfferSerializer(source="offers", many=True, read_only=True)

    class Meta(ProjectListSerializer.Meta):
        model = Project
        fields = ProjectListSerializer.Meta.fields + (
            'project_offers',
        )


class CampaignSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    company_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Company.objects.all(),
        source="company",
        write_only=True
    )
    contact_person_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Person.objects.all(),
        source="contact_person",
        write_only=True
    )
    campaign_owner_uuid = PrimaryKeyRelatedField(
        queryset=User.objects.all(),
        source="campaign_owner",
        write_only=True
    )
    billing_address_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Address.objects.all(),
        source="billing_address",
        required=False,
        write_only=True
    )
    billing_address = AddressSerializer(allow_null=True, required=False, read_only=True)
    projects_uuids = serializers.ListField(
        child=serializers.PrimaryKeyRelatedField(
            queryset=Project.objects.all(),
        ),
        required=False,
        write_only=True
    )
    projects = CampaignProjectSerializer(allow_null=True, required=False, many=True, read_only=True)
    campaign_owner = SimpleUserSerializer(read_only=True)
    company = ProjectCompanySerializer(read_only=True)
    contact_person = ProjectPersonSerializer(read_only=True)
    campaign_offers = CampaignOfferSerializer(source="offers", many=True, read_only=True)

    class Meta:
        model = Campaign
        fields = (
            'id',
            'public_id',
            'title',
            'campaign_owner_uuid',
            'company_uuid',
            'contact_person_uuid',
            'description',
            'customer_reference',
            'start_date',
            'finishing_date',
            'delivery_date',
            'billing_address',
            'billing_address_uuid',
            'billing_agreement',
            'billing_interval',
            'projects',
            'projects_uuids',
            'campaign_owner',
            'company',
            'contact_person',
            'campaign_offers'
        )
        read_only_fields = (
            'public_id',
            'campaign_offers',
            'projects',
        )

    def create(self, data):
        campaign = Campaign.objects.create(
            **{
                'title': data.get('title'),
                'campaign_owner': data.get('campaign_owner'),
                'company': data.get('company'),
                'contact_person': data.get('contact_person'),
                'description': data.get('description', ''),
                'customer_reference': data.get('customer_reference', ''),
                'start_date': data.get('start_date', date.today()),
                'finishing_date': data.get('finishing_date'),
                'delivery_date': data.get('delivery_date'),
                'billing_address': data.get('billing_address'),
                'billing_agreement': data.get('billing_agreement', ''),
                'billing_interval': data.get('billing_interval', ''),
            }
        )
        for project in data.get('projects_uuids', []):
            project.campaign = campaign
            project.save(update_fields=['campaign'])
        return campaign

    def update(self, instance, data):
        keys = [
            'title',
            'campaign_owner',
            'company',
            'contact_person',
            'description',
            'customer_reference',
            'start_date',
            'finishing_date',
            'delivery_date',
            'billing_agreement',
            'billing_interval',
        ]
        for key in keys:
            setattr(instance, key, data.get(key, getattr(instance, key)))
        if 'billing_address' in data.keys() and data.get('billing_address'):
            instance.billing_address = data.get('billing_address')
        if data.get('projects_uuids') is not None:
            if not data.get('projects_uuids', []):
                instance.projects.update(campaign=None)
            else:
                for project in data.get('projects_uuids', []):
                    project.campaign = instance
                    project.save(update_fields=['campaign'])
        instance.save()
        return instance

    @staticmethod
    def setup_eager_loading(queryset):
        """ Perform necessary eager loading of data. """
        # select_related for "to-one" relationships
        queryset = queryset.select_related(
            'company',
            'company__contact_ptr',
            'contact_person',
            'contact_person__email',
            'contact_person__contact_ptr',
            'billing_address',
            'campaign_owner',
        )

        queryset = queryset.prefetch_related(
            'projects',
            'projects__activities',
            'projects__articles',
            'projects__offer_set',
            'offers'
        )

        return queryset


class IdDataSerializer(serializers.ListSerializer):

    def to_representation(self, data):
        instance_list = super().to_representation(data)
        id_data = {item.pop('id'): item for item in instance_list}
        return id_data

    @property
    def data(self):
        return super(serializers.ListSerializer, self).data


class CampaignDefaultsSerializer(serializers.ModelSerializer):
    contact_person_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Person.objects.all(),
        source="contact_person",
    )
    billing_address_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Address.objects.all(),
        source="billing_address",
    )

    class Meta:
        model = Campaign
        list_serializer_class = IdDataSerializer
        fields = (
            'id',
            'title',
            'contact_person_uuid',
            'start_date',
            'finishing_date',
            'delivery_date',
            'billing_address_uuid',
            'billing_agreement',
            'billing_interval',
        )


class SimpleProjectSerializer(BaseSimpleSerializer):
    """
    Simple serializer to use with ProjectActivitySerializer
    """

    class Meta:
        model = Project
        fields = ['uuid', 'public_id', 'title']
        read_only_fields = ['uuid', 'public_id', 'title']


class SimpleTemplateArticleSerializer(BaseSimpleSerializer):
    """ Simple serializer to use with ProjectArticleSerializer
    """

    class Meta:
        model = TemplateArticle
        fields = ['uuid', 'name']
        extra_kwargs = {
            'name': {'read_only': True}
        }


class SimpleArticleGroupSerializer(BaseSimpleSerializer):
    """ Simple serializer to use with ProjectArticleSerializer
    """

    class Meta:
        model = ArticleGroup
        fields = ['uuid', 'name']
        extra_kwargs = {
            'name': {'read_only': True}
        }


class SimpleArticleAllocationSerializer(BaseSimpleSerializer):
    """ Simple serializer to use with ProjectArticleSerializer
    """

    class Meta:
        model = Allocation
        fields = ['uuid']


class ProjectArticleSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)
    used_template_article = SimpleTemplateArticleSerializer()
    article_group = SimpleArticleGroupSerializer()
    allocation = SimpleArticleAllocationSerializer(required=False)
    name = serializers.CharField(read_only=True)
    total_planned_budget = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_planned_budget_currency = serializers.CharField(source='total_planned_budget.currency', read_only=True)
    total_real_cost = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_real_cost_currency = serializers.CharField(source='total_real_cost.currency', read_only=True)
    sent_in_project_offers = OfferListSerializer(many=True, read_only=True)
    final_state = serializers.BooleanField(source='get_final_state', read_only=True, required=False)
    final_state_information = serializers.DictField(
        source='get_final_state_information', read_only=True, required=False)
    offer_status = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ProjectArticle
        fields = [
            "uuid",
            "version",
            "display_name",
            "description",
            "article_group",
            "sent_in_project_offers",
            "final_state",
            "final_state_information",
            "budget_in_project_currency",
            "used_template_article",
            "invoice_status",
            "is_external_cost",
            "allocation",
            "name",
            "position",
            "total_real_cost",
            "total_real_cost_currency",
            "total_planned_budget",
            "total_planned_budget_currency",
            "offer_status",
        ]
        extra_kwargs = {
            'invoice_status': {'allow_null': True},
            'position': {'read_only': True},
            'version': {'read_only': True}
        }

    def create(self, data):
        # we have uuid of the project inside the url, so just make frontend not duplicate obvious thing to the backend
        # with extra post param
        data['project'] = Project.objects.get(id=self.context['project'])
        data['used_template_article'] = TemplateArticle.objects.get(id=data['used_template_article']['id'])
        data['article_group'] = ArticleGroup.objects.get(id=data['article_group']['id'])
        if data.get('allocation'):
            data['allocation'] = Allocation.objects.get(id=data['allocation']['id'])
        return super().create(data)

    def update(self, instance, data):
        if data.get('used_template_article'):
            data['used_template_article'] = TemplateArticle.objects.get(id=data['used_template_article']['id'])
        if data.get('article_group'):
            data['article_group'] = ArticleGroup.objects.get(id=data['article_group']['id'])
        if data.get('allocation'):
            data['allocation'] = Allocation.objects.get(id=data['allocation']['id'])
        if instance.offer_status == ActivityArticleOfferedStatus.IMPROVABLE:
            instance.change_status_to_improved()
        return super().update(instance, data)

    def get_offer_status(self, obj):
        return str(obj.offer_status)


class ProjectArticleExpenseSerializer(serializers.ModelSerializer):
    company_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Company.objects.all(),
        source='company',
        write_only=True,
        required=False
    )
    company = ProjectCompanySerializer(read_only=True)
    article_uuid = serializers.PrimaryKeyRelatedField(
        queryset=ProjectArticle.objects.all(),
        source='article'
    )
    is_company_selected = serializers.BooleanField()
    expense_date = serializers.DateField(required=False)
    cost_in_receipt_currency = MoneyField(max_digits=10, decimal_places=2)
    cost_in_default_currency = MoneyField(max_digits=10, decimal_places=2, read_only=True)

    def validate(self, attrs):
        if attrs.get('is_company_selected') and not attrs.get('company', None):
            raise serializers.ValidationError(_('The field "company_uuid" is required, when is_company_selected=True'))

        if not attrs.get('is_company_selected') and attrs.get('company', None):
            raise serializers.ValidationError(
                _('The field "company_uuid" should not be sent, when is_company_selected=False'))

        return attrs

    class Meta(object):
        model = ProjectArticleExpense
        fields = (
            'id',
            'name',
            'article_uuid',
            'company_uuid',
            'company',
            'is_company_selected',
            'cost_in_receipt_currency',
            'receipt_currency',
            'cost_in_default_currency',
            'expense_date'
        )


class ProjectActivityOrderingSerializer(BaseOrderingSerializer):
    class Meta(BaseOrderingSerializer.Meta):
        model = ProjectActivity

    def to_representation(self, instance):
        project = instance.project
        activities = project.activities.order_by("position").values('id', 'position')
        return dict(map(lambda x: (str(x['id']), x['position']), activities))


class ProjectArticleOrderingSerializer(BaseOrderingSerializer):
    class Meta(BaseOrderingSerializer.Meta):
        model = ProjectArticle

    def to_representation(self, instance):
        project = instance.project
        articles = project.articles.order_by("position").values('id', 'position')
        return dict(map(lambda x: (str(x['id']), x['position']), articles))


class UserWorkLogSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    class Meta:
        model = WorkLog

        fields = (
            'id',
            'project_activity_step',
            'tracked_time',
            'work_date',
            'note',
            'satisfaction'
        )

    def validate(self, attrs):
        worker = self.context['worker']
        if not worker:
            raise serializers.ValidationError(_('The field "worker" is required.'))

        if (self.context['view'].action == "create" and
                attrs.get("project_activity_step").status not in [ACTIVITY_STATUS.PLAN, ACTIVITY_STATUS.WORK]):
            raise serializers.ValidationError(
                _("You can create new worklog only for steps with plan or work status"))

        if self.instance and self.context['request'].user != self.instance.worker:
            if attrs.get("tracked_time") is not None or attrs.get("work_date") is not None:
                raise serializers.ValidationError(
                    _("You can't change tracked_time or work_date"))

        return attrs

    def create(self, validated_data):
        project_activity_step = validated_data.pop('project_activity_step', None)
        worker = validated_data.pop('worker', None)
        tracked_time = validated_data.pop('tracked_time', None)
        work_date = validated_data.pop('work_date', None)
        note = validated_data.pop('note', None)
        satisfaction = validated_data.pop('satisfaction', None)

        instance = WorkLog.objects.create(project_activity_step=project_activity_step, worker=worker,
                                          tracked_time=tracked_time,
                                          work_date=work_date, note=note, satisfaction=satisfaction)

        return instance

    def update(self, instance, validated_data):
        instance.project_activity_step = validated_data.get('project_activity_step', instance.project_activity_step)
        instance.worker = validated_data.get('worker', instance.worker)
        instance.tracked_time = validated_data.get('tracked_time', instance.tracked_time)
        instance.work_date = validated_data.get('work_date', instance.work_date)
        instance.note = validated_data.get('note', instance.note)
        instance.satisfaction = validated_data.get('satisfaction', instance.satisfaction)

        instance.save(update_fields=['project_activity_step', 'worker',
                                     'tracked_time', 'work_date', 'note', 'satisfaction'])
        return instance


class FullWorkLogSerializer(UserWorkLogSerializer):
    """ Used in the /worklogs/ API """

    worker = UserUUIDSerializer(read_only=True)
    project_activity_step = SimpleProjectActivityStepSerializer(read_only=True)
    project = SimpleProjectSerializer(source='project_activity_step.activity.project', read_only=True)
    project_activity = SimpleProjectActivitySerializer(source='project_activity_step.activity', read_only=True)

    class Meta:
        model = WorkLog

        fields = UserWorkLogSerializer.Meta.fields + (
            'worker',
            'created_at',
            'project',
            'project_activity',
            'project_activity_step'
        )


class UserProjectActivitySerializer(ProjectActivitySerializer):
    project = ProjectListSerializer()

    class Meta(BaseProjectActivitySerializer.Meta):
        fields = ProjectActivitySerializer.Meta.fields + [
            "project",
        ]
