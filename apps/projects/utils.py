
from django.utils.translation import ugettext_lazy as _

from apps.metronom_commons.data import ACCOUNTING_TYPES


def project_validator(instance=None, data=None, model_check=False):
    """ This is validator to check if the data inside the project is correct. 

    :param instance: Project or None
        instance to check
    :param data: dict or None
        data to check
    :param model_check: boolean or None
        If this parameter is true, than we run validations only for instance parameter

    :rtype: tuple
        (boolean, dict)
    :returns: Returns the  tuple. 
        First element of tuple is boolean indicating if validation is passed. 
        Second element of tuple represents dict with the errors during validation

    """
    error_texts = {
        'finishing_date': _('The finishing date cannot be earlier than the start date.'),
        'delivery_date': _('The delivery date cannot be earlier than the start date.'),
        'billing_address': _('The billing address has to be connected to an office of the company.'),
        'campaign': _("Internal project can't be added to the campaign."),
        'billing_currency': _("You can't change project billing currency to another one because there are "
                              "project activity steps or project articles related to this currency already.")
    }
    errors = {}

    def _date_checks(finishing_date, start_date, delivery_date):
        nonlocal errors
        if finishing_date and start_date and finishing_date < start_date:
            errors['finishing_date'] = error_texts['finishing_date']
        if delivery_date and start_date and delivery_date < start_date:
            errors['delivery_date'] = error_texts['delivery_date']

    if instance:
        _date_checks(instance.finishing_date, instance.start_date, instance.delivery_date)

        if instance.billing_address and instance.billing_address not in [office.address for office in
                                                                         instance.company.offices.all()]:
            errors['billing_address'] = error_texts['billing_address']
        if hasattr(instance, "accounting_type") and hasattr(instance, "campaign"):
            if instance.accounting_type == ACCOUNTING_TYPES.INTERNAL and instance.campaign:
                errors['campaign'] = error_texts['campaign']
        if hasattr(instance, "billing_currency") and hasattr(instance, "campaign"):
            from apps.projects.models import ProjectActivityStep
            if ProjectActivityStep.objects.filter(activity__project=instance)\
                                          .exclude(used_rate_currency=instance.billing_currency).exists():
                errors['billing_currency'] = error_texts['billing_currency']
            elif instance.articles.exclude(budget_in_project_currency_currency=instance.billing_currency).exists():
                errors['billing_currency'] = error_texts['billing_currency']

    if data and not model_check:
        billing_address = data.get('billing_address')
        finishing_date = data.get('finishing_date')
        start_date = data.get('start_date')
        delivery_date = data.get('delivery_date')
        company = data.get('company')
        if company is None and instance:
            company = instance.company
        if company:
            addresses = [office.address.id for office in company.offices.all()]
            if billing_address and billing_address and billing_address.id not in addresses:
                errors['billing_address'] = error_texts['billing_address']
        if instance:
            _date_checks(
                finishing_date or instance.finishing_date,
                start_date or instance.start_date,
                delivery_date or instance.delivery_date)
        else:
            _date_checks(finishing_date, start_date, delivery_date)
    return (not errors, errors)
