
from dry_rest_permissions.generics import DRYPermissions

from rest_framework.generics import UpdateAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import (GenericViewSet, ModelViewSet,
                                     ReadOnlyModelViewSet)
from rest_framework.filters import SearchFilter

from apps.metronom_commons.utils import cast_bool
from apps.api.filters import MetronomPermissionsFilter
from apps.api.mixins import LoggerMixin
from apps.api.views import MetronomBaseModelViewSet
from apps.invoices.models import ActivityInvoiced, ArticleInvoiced, Invoice
from apps.metronom_commons.choices_flow import OfferStatus
from apps.metronom_commons.mixins.views import DeleteForbiddenIfUsedMixin, CombinedListMixin
from apps.metronom_commons.pagination import MetronomPageNumberPagination
from apps.metronom_commons.filters import CombinedQuerysetSearchFilter
from rest_framework.permissions import IsAuthenticated
from apps.offers.models import (ActivityOffered, ArticleOffered, CampaignOffer,
                                Offer)
from apps.projects.filters import WorkLogFilterBackend, ProjectFilterBackend, CampaignFilterBackend, ProjectAndCampaignCommonFilterBackend
from apps.projects.models import (Campaign, Project, ProjectActivity,
                                  ProjectArticle, ProjectArticleExpense,
                                  WorkLog)
from apps.projects.serializers import (CampaignListSerializer,
                                       CampaignSerializer,
                                       CampaignProjectListSerializer,
                                       FullWorkLogSerializer,
                                       ProjectActivityOrderingSerializer,
                                       ProjectActivitySerializer,
                                       UserProjectActivitySerializer,
                                       ProjectArticleExpenseSerializer,
                                       ProjectArticleOrderingSerializer,
                                       ProjectArticleSerializer,
                                       ProjectDetailSerializer,
                                       ProjectListSerializer,
                                       UserWorkLogSerializer)
from apps.users.views import MeURLMixin
from rest_framework.response import Response
from rest_framework.views import APIView


class CampaignViewSet(DeleteForbiddenIfUsedMixin, MetronomBaseModelViewSet):
    """Campaigns (pools Projects)"""

    pagination_class = MetronomPageNumberPagination
    serializer_class = CampaignSerializer
    queryset = Campaign.objects.all().order_by("id")
    http_method_names = ['get', 'post', 'patch', 'delete', 'head', 'options', 'trace']
    permission_classes = (DRYPermissions,)
    filter_backends = (
        MetronomPermissionsFilter,
        CampaignFilterBackend,
        ProjectAndCampaignCommonFilterBackend,
        SearchFilter,
    )

    forbid_delete_if_used_in = {
        Project: ["campaign"],
        CampaignOffer: ["campaign"]
    }

    search_fields = ('description', 'public_id', 'title', 'company__name')

    def get_forbid_delete_qs_campaignoffer(self):
        """ This is method of the DeleteForbiddenIfUsedMixin to provide additional filtration for the Offer qs
        we'll use during the deletion check
        """
        return CampaignOffer.objects.exclude(campaign_status=OfferStatus.CREATED)


class ProjectViewSet(DeleteForbiddenIfUsedMixin, MetronomBaseModelViewSet):
    """ Project list """

    pagination_class = MetronomPageNumberPagination
    serializer_class = ProjectDetailSerializer
    queryset = Project.objects.all().order_by("id")
    http_method_names = ['get', 'post', 'patch', 'delete', 'head', 'options', 'trace']
    permission_classes = (DRYPermissions,)

    filter_backends = (
        MetronomPermissionsFilter,
        ProjectFilterBackend,
        ProjectAndCampaignCommonFilterBackend,
        SearchFilter,
    )

    search_fields = ('description', 'public_id', 'title', 'company__name')

    forbid_delete_if_used_in = {
        WorkLog: ["project_activity_step__activity__project"],
        Invoice: ["project"],
        Offer: ["project"]
    }

    def get_queryset(self):
        if self.request.user.is_admin_role:
            return Project.all_objects.all().order_by("id")
        return self.queryset

    def get_serializer_class(self):

        if self.action == 'list':
            return ProjectListSerializer

        return self.serializer_class

    def get_forbid_delete_qs_offer(self):
        """ This is method of the DeleteForbiddenIfUsedMixin to provide additional filtration for the Offer qs
        we'll use during the deletion check
        """
        return Offer.objects.exclude(offer_status=OfferStatus.CREATED)


class ProjectCampaignViewSet(CombinedListMixin, LoggerMixin, ModelViewSet):
    """This is used for the special projects_and_campaigns endpoint """

    pagination_class = MetronomPageNumberPagination
    serializer_class = CampaignProjectListSerializer
    http_method_names = ['get', 'head', 'options', 'trace']

    permission_classes = (DRYPermissions,)
    filter_backends = (
        CombinedQuerysetSearchFilter,
        ProjectFilterBackend,
        CampaignFilterBackend,
        ProjectAndCampaignCommonFilterBackend,
        MetronomPermissionsFilter
    )

    search_fields = ('description', 'public_id', 'title', 'company__name')

    combined_lists = [
        {
            "instance_type": Project,
            "queryset": Project.objects.all(),
            "serializer_class": ProjectListSerializer,
            "order_by": "-created_at",
            "specific_filters": {
                'type': 'project',
                'accounting_type': '*',
                'belongs_to_campaign': '*',
                'project_owner_uuid': '*',
                'project_owner': '*',
                'assigned_to_user_uuid': '*',
            }
        }, {
            "instance_type": Campaign,
            "queryset": Campaign.objects.all(),
            "serializer_class": CampaignListSerializer,
            "order_by": "-created_at",
            "specific_filters": {
                'type': 'campaign',
                'campaign_owner_uuid': '*',
                'campaign_owner': '*',
            }
        }
    ]


class ProjectActivityViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    http_method_names = ['get', 'post', 'patch', 'delete', 'head', 'options', 'trace']
    serializer_class = ProjectActivitySerializer
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    forbid_delete_if_used_in = {
        ActivityOffered: ["project_activity"],
        ActivityInvoiced: ["activity_offered"],
        WorkLog: ["project_activity_step__activity"],
    }

    def get_forbid_delete_qs_activityoffered(self):
        """ This is method of the DeleteForbiddenIfUsedMixin to provide additional filtration for the ActivityOffered qs
        we'll use during the deletion check
        """
        return ActivityOffered.objects.exclude(offer_status=OfferStatus.CREATED)

    def get_queryset(self):
        project_pk = self.kwargs.get('project_pk')
        project = Project.objects.filter(pk=project_pk).first()
        activity_pk = self.kwargs.get('pk')
        if not project:
            return ProjectActivity.objects.none()
        if activity_pk:
            return project.activities.filter(id=activity_pk).order_by('-created_at')
        return project.activities.all().order_by('-created_at')

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['project'] = self.kwargs.get('project_pk')
        return context

    def partial_update(self, request, *args, **kwargs):
        """We need to Overwrite the Partial_Update for ProjectActivity because:
            - we need to apply Dynamic Permissions for ActivityStep which are handled only with this ViewSet
            - we can't reach to the ProjectActivityStep with DRYPermissions since this applies
            to the ViewSet.queryset model only: ProjectActivity.
            - We need the same User.role + object.permissions so we send to the Serializer.update the request.USER
        Dynamic Permissions explained:
            - User.role == USER => can change the steps to either PLAN, WORK and DONE
            - user.Role == Accountant + PM/KAM() => can also change to BILLABLE , ON_HOLD or CANCELED
        """
        instance = self.get_queryset().first()
        serializer = self.get_serializer(instance=instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        # We create a special **kwargs to send the user back to the serializer to filter any updates
        # to the ActivityStep depending on the User and Status
        user_kwargs = {"user": request.user}
        serializer.update(instance, serializer.validated_data, **user_kwargs)
        return Response(serializer.data)


class ProjectArticleViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    serializer_class = ProjectArticleSerializer
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    forbid_delete_if_used_in = {
        ProjectArticleExpense: ["article"],
        ArticleOffered: ["project_article"],
        ArticleInvoiced: ["article_offered"]
    }

    def get_forbid_delete_qs_articleoffered(self):
        """ This is method of the DeleteForbiddenIfUsedMixin to provide additional filtration for the ArticleOffered qs
        we'll use during the deletion check
        """
        return ArticleOffered.objects.exclude(offer_status=OfferStatus.CREATED)

    def get_queryset(self):
        project_pk = self.kwargs.get('project_pk')
        article_pk = self.kwargs.get('pk')
        project = Project.objects.filter(pk=project_pk).first()
        if not project:
            return ProjectArticle.objects.none()
        if article_pk:
            return project.articles.filter(id=article_pk).order_by('-created_at')
        return project.articles.all().order_by('-created_at')

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['project'] = self.kwargs.get('project_pk')
        return context


class ProjectArticleExpenseViewSet(ModelViewSet):
    serializer_class = ProjectArticleExpenseSerializer

    # filter_backends = (MetronomPermissionsFilter,)
    # permission_classes = (ProjectActivityArticlePermission,)

    def get_queryset(self):
        project_pk = self.kwargs.get('project_pk')
        article_pk = self.kwargs.get('article_pk')
        return ProjectArticleExpense.objects.filter(article=article_pk, article__project=project_pk)


class ProjectOrderingView(UpdateAPIView):
    # permission_classes = (ProjectActivityArticlePermission,)
    http_method_names = ['patch']

    def get_serializer_class(self):
        data_type = self.request.data.get('type')
        if data_type == 'article':
            return ProjectArticleOrderingSerializer
        return ProjectActivityOrderingSerializer

    def get_queryset(self):
        data_type = self.request.data.get('type')
        if data_type == 'article':
            model = ProjectArticle
            m2m_name = 'articles'
        else:
            model = ProjectActivity
            m2m_name = 'activities'
        project_pk = self.kwargs.get('project_pk')
        project = Project.objects.filter(pk=project_pk).first()
        if not project:
            return model.objects.none()
        return getattr(project, m2m_name).all().order_by('-created_at')

    def get_object(self):
        queryset = self.get_queryset()
        uuid = self.request.data.get('uuid')
        return queryset.filter(id=uuid).first()


class UserWorkLogViewSet(MeURLMixin, LoggerMixin, ModelViewSet):
    serializer_class = UserWorkLogSerializer
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    http_method_names = ['get', 'patch', 'post', 'head', 'options', 'trace', 'delete']

    def get_queryset(self):
        return WorkLog.objects.filter(worker=self.get_user())

    def perform_create(self, serializer):
        return serializer.save(worker=self.get_user())

    def perform_update(self, serializer):
        return serializer.save(worker=self.get_user())

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['worker'] = self.get_user()

        return context


class WorkLogViewSet(LoggerMixin, ListModelMixin, GenericViewSet):
    """
    This view set will return a list of all worklogs determined by
    the query string portion of the url.
    If no query string is found, all worklogs are returned
    """
    # permission_classes = (IsAuthenticated,)
    queryset = WorkLog.objects.all().order_by("id")
    serializer_class = FullWorkLogSerializer
    filter_backends = (WorkLogFilterBackend,)


class CampaignProjectViewSet(ReadOnlyModelViewSet):
    serializer_class = ProjectDetailSerializer
    # filter_backends = (MetronomPermissionsFilter,)
    http_method_names = ['get', 'head', 'options', 'trace']

    def get_queryset(self):
        return self.get_serializer().setup_eager_loading(
            Project.objects.filter(campaign=self.kwargs.get('campaign_pk')))


class UserProjectActivityViewSet(MeURLMixin, LoggerMixin, ModelViewSet):
    """This is used for the special /api/me/activities/ endpoint """

    pagination_class = MetronomPageNumberPagination
    serializer_class = UserProjectActivitySerializer
    http_method_names = ['get', 'head', 'options', 'trace']
    filter_backends = (MetronomPermissionsFilter,)
    permission_classes = (DRYPermissions,)

    def get_queryset(self):
        return ProjectActivity.objects.filter(activity_steps__assignments__worker=self.get_user())


class DuplicateProjectViewSet(APIView, LoggerMixin):
    http_method_names = ['post', 'head', 'options', 'trace']
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        project_pk = self.kwargs.get('project_pk')
        project = Project.objects.filter(pk=project_pk)
        if not project:
            raise Project.DoesNotExist
        return project.first()

    def post(self, request, *args, **kwargs):
        """Clone {project_pk} Project and return a dictionary with both Old and New Project IDs.

        ```
        POST empty data and receive:
        {
            "old_project_uuid": old_project.id,
            "new_project_uuid": new_project.id
        }```
        """
        logged_in_user = request.user
        project = self.get_queryset()
        new_project = project.get_cloned_project(logged_in_user)
        return Response({
            "old_project_uuid": str(project.id),
            "new_project_uuid": str(new_project.id)
        })
