import copy
import uuid
from datetime import date, timedelta

import pytest
from allauth.account.models import EmailAddress
from django.contrib.auth.models import Group
from django.http import QueryDict
from django.urls import reverse
from django.utils import timezone
from rest_framework import status

from apps.activities.tests.factories import (ActivityGroupFactory,
                                             TemplateActivityFactory)
from apps.companies.tests.factories import (CompanyFactory,
                                            CompanyWithOfficeFactory,
                                            OfficeFactory)
from apps.metronom_commons.choices_flow import OfferStatus
from apps.metronom_commons.data import BILLING_AGREEMENT, BILLING_INTERVAL
from apps.metronom_commons.models import ROLE
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.offers.tests.factories import CampaignOfferFactory, OfferFactory
from apps.persons.tests.factories import PersonFactory
from apps.projects.models import (ACCOUNTING_TYPES, Campaign, Project,
                                  ProjectActivity)
from apps.projects.tests.factories import (CampaignFactory,
                                           ProjectActivityFactory,
                                           ProjectFactory)
from apps.users.tests.factories import UserFactory


class TestCampaignsAndProjectsList(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.campaign1 = CampaignFactory(billing_currency="CHF")
        cls.campaign2 = CampaignFactory(billing_currency="CHF")
        cls.project1 = ProjectFactory(billing_currency='CHF')
        cls.project2 = ProjectFactory(campaign=cls.campaign2, billing_currency='CHF')
        cls.project_endpoint = reverse('api:project-list')
        cls.campaign_endpoint = reverse('api:campaign-list')
        cls.project_response = cls.admin_client.get(cls.project_endpoint)
        cls.campaign_response = cls.admin_client.get(cls.campaign_endpoint)

        cls.response_projects = cls.project_response.data.get('results', None)
        cls.response_campaigns = cls.campaign_response.data.get('results', None)

        # cls.response_projects = [x for x in cls.results if x.get('type') == 'project']
        # cls.response_campaigns = [x for x in cls.results if x.get('type') == 'campaign']

        cls.project_offer1 = OfferFactory(project=cls.project1)
        cls.project_offer2 = OfferFactory(project=cls.project1)
        cls.campaign_offer1 = CampaignOfferFactory(campaign=cls.campaign1)
        cls.campaign_offer1.project_offers.add(cls.project_offer1, cls.project_offer2)
        cls.campaign_offer2 = CampaignOfferFactory(campaign=cls.campaign1)

    def test_get_should_return_ok_status(self):
        assert self.project_response.status_code == status.HTTP_200_OK
        assert self.campaign_response.status_code == status.HTTP_200_OK

    def test_get_should_contain_the_campaigns_and_projects(self):
        campaigns_titles = [campaign['title'] for campaign in self.response_campaigns]
        projects_titles = [project['title'] for project in self.response_projects]
        assert self.campaign1.title in campaigns_titles
        assert self.campaign2.title in campaigns_titles
        assert self.project1.title in projects_titles
        assert self.project2.title in projects_titles

    def test_get_should_contain_campaign_offers_list(self):
        for campaign in self.campaign_response.data['results']:
            if campaign['id'] == self.campaign1.id:
                assert self.campaign1.offers.all().count() == len(self.campaign_response.data['campaign_offers'])
                assert self.campaign1.offers.first().name == self.campaign_response.data['campaign_offers'][0]['name']
                assert self.campaign_offer2.name == self.campaign_response.data['campaign_offers'][0]['name']

    def test_get_should_contain_project_offers_inside_campaign_offer(self):
        for campaign in self.campaign_response.data['results']:
            if campaign['id'] == self.campaign1.id:
                for campaign_offer in campaign['campaign_offers']:
                    if campaign_offer['uuid'] == self.campaign_offer1.id:
                        assert self.campaign_offer1.project_offers.all(
                        ).count() == len(campaign_offer['project_offers'])

    def test_response_should_contain_currency_for_project(self):
        self.assertEqual(self.response_projects[0]['billing_currency'], 'CHF')
        self.assertEqual(self.response_projects[1]['billing_currency'], 'CHF')

    def test_get_should_contain_two_campaigns(self):
        assert len(self.response_campaigns) == 2

    def test_get_should_contain_two_projects(self):
        assert len(self.response_projects) == 2

    def test_response_should_contain_the_correct_public_id_for_project(self):
        assert self.project1.public_id in (elem['public_id'] for elem in self.response_projects)
        assert self.project2.public_id in (elem['public_id'] for elem in self.response_projects)

    def test_response_should_contain_the_correct_title_for_campaign(self):
        assert self.campaign1.title in (elem['title'] for elem in self.response_campaigns)
        assert self.campaign2.title in (elem['title'] for elem in self.response_campaigns)

    def test_response_should_contain_the_correct_dates_for_campaign(self):
        assert str(self.campaign1.start_date) in (elem['start_date'] for elem in self.response_campaigns)
        assert str(self.campaign2.start_date) in (elem['start_date'] for elem in self.response_campaigns)
        assert str(self.campaign1.finishing_date) in (elem['finishing_date'] for elem in self.response_campaigns)
        assert str(self.campaign2.finishing_date) in (elem['finishing_date'] for elem in self.response_campaigns)

    def test_response_should_contain_the_correct_title_for_project(self):
        assert self.project1.title in (elem['title'] for elem in self.response_projects)
        assert self.project2.title in (elem['title'] for elem in self.response_projects)

    def test_response_should_contain_the_correct_dates_for_project(self):
        assert str(self.project1.start_date) in (elem['start_date'] for elem in self.response_projects)
        assert str(self.project2.start_date) in (elem['start_date'] for elem in self.response_projects)
        assert str(self.project1.finishing_date) in (elem['finishing_date'] for elem in self.response_projects)
        assert str(self.project2.finishing_date) in (elem['finishing_date'] for elem in self.response_projects)

    def test_response_should_contain_the_correct_company_for_campaign(self):
        self.response_campaigns_companies = [elem['company'] for elem in self.response_campaigns]
        assert str(self.campaign1.company.id) in (elem['uuid'] for elem in self.response_campaigns_companies)
        assert str(self.campaign2.company.id) in (elem['uuid'] for elem in self.response_campaigns_companies)

        assert self.campaign1.company.public_id in (elem['public_id'] for elem in self.response_campaigns_companies)
        assert self.campaign2.company.public_id in (elem['public_id'] for elem in self.response_campaigns_companies)

        assert self.campaign1.company.name in (elem['name'] for elem in self.response_campaigns_companies)
        assert self.campaign2.company.name in (elem['name'] for elem in self.response_campaigns_companies)

    def test_response_should_contain_the_correct_company_for_project(self):
        self.response_projects_companies = [elem['company'] for elem in self.response_projects]
        assert str(self.project1.company.id) in (elem['uuid'] for elem in self.response_projects_companies)
        assert str(self.project2.company.id) in (elem['uuid'] for elem in self.response_projects_companies)

        assert self.project1.company.public_id in (elem['public_id'] for elem in self.response_projects_companies)
        assert self.project2.company.public_id in (elem['public_id'] for elem in self.response_projects_companies)

        assert self.project1.company.name in (elem['name'] for elem in self.response_projects_companies)
        assert self.project2.company.name in (elem['name'] for elem in self.response_projects_companies)

    def test_response_should_contain_the_correct_project_owner_for_project(self):
        self.response_projects_project_owners = [elem['project_owner'] for elem in self.response_projects]
        assert str(self.project1.project_owner.id) in (elem['uuid'] for elem in self.response_projects_project_owners)
        assert str(self.project2.project_owner.id) in (elem['uuid'] for elem in self.response_projects_project_owners)

    def test_response_should_contain_the_correct_campaigns_owner_for_campaigns(self):
        self.response_campaigns_campaign_owners = [elem['campaign_owner'] for elem in self.response_campaigns]
        assert str(self.campaign1.campaign_owner.id) in (elem['uuid'] for elem in
                                                         self.response_campaigns_campaign_owners)
        assert str(self.campaign2.campaign_owner.id) in (elem['uuid'] for elem in
                                                         self.response_campaigns_campaign_owners)

    def test_response_should_contain_the_correct_accounting_type_for_project(self):
        self.response_projects_accounting_type = [elem['accounting_type'] for elem in self.response_projects]
        assert self.project1.accounting_type in self.response_projects_accounting_type
        assert self.project2.accounting_type in self.response_projects_accounting_type

    def test_response_should_contain_the_correct_contact_person_for_project(self):
        ids = [str(self.project1.contact_person.id), str(self.project2.contact_person.id)]
        names = [self.project1.contact_person.name, self.project2.contact_person.name]
        emails = [self.project1.contact_person.email.email, self.project2.contact_person.email.email]
        phones = [self.project1.contact_person.main_phone, self.project2.contact_person.main_phone]
        assert self.response_projects[0]['contact_person']['uuid'] in ids
        assert self.response_projects[0]['contact_person']['name'] in names
        assert self.response_projects[0]['contact_person']['email'] in emails
        assert self.response_projects[0]['contact_person']['phone'] in phones
        assert self.response_projects[1]['contact_person']['uuid'] in ids
        assert self.response_projects[1]['contact_person']['name'] in names
        assert self.response_projects[1]['contact_person']['email'] in emails
        assert self.response_projects[1]['contact_person']['phone'] in phones

    def test_response_should_contain_the_correct_contact_person_for_campaign(self):
        self.response_campaigns_contact_persons = [elem['contact_person'] for elem in self.response_campaigns]
        assert str(self.campaign1.contact_person.id) in (elem['uuid'] for elem in
                                                         self.response_campaigns_contact_persons)
        assert str(self.campaign2.contact_person.id) in (elem['uuid'] for elem in
                                                         self.response_campaigns_contact_persons)

        assert self.campaign1.contact_person.name in (elem['name'] for elem in self.response_campaigns_contact_persons)
        assert self.campaign2.contact_person.name in (elem['name'] for elem in self.response_campaigns_contact_persons)

        assert self.campaign1.contact_person.email.email in (elem['email'] for elem in
                                                             self.response_campaigns_contact_persons)
        assert self.campaign2.contact_person.email.email in (elem['email'] for elem in
                                                             self.response_campaigns_contact_persons)

        assert self.campaign1.contact_person.main_phone in (elem['phone'] for elem in
                                                            self.response_campaigns_contact_persons)
        assert self.campaign2.contact_person.main_phone in (elem['phone'] for elem in
                                                            self.response_campaigns_contact_persons)

    def test_response_should_contain_campaign_uuid_for_project(self):
        ids = [x['campaign_uuid'] for x in self.response_projects]
        self.assertTrue(self.project2.campaign.id in ids)

    def test_should_send_an_empty_result_when_there_are_no_projects(self):
        Project.objects.all().delete()
        response = self.user_client.get(self.project_endpoint)
        results = response.data.get('results', None)
        projects = [x for x in results if x.get('type') == 'project']
        assert projects == []

    def test_should_send_an_empty_result_when_there_are_no_campaigns(self):
        Campaign.objects.all().delete()
        response = self.admin_client.get(self.project_endpoint)
        results = response.data.get('results', None)
        campaigns = [x for x in results if x.get('type') == 'campaigns']
        assert campaigns == []

    def test_pagination_keys_are_present(self):
        expected_keys = ['count', 'next', 'previous', 'results']
        actual_keys = self.project_response.data.keys()

        assert all([key in expected_keys for key in actual_keys])

    def test_pagination_invalid_page(self):
        Campaign.objects.all().delete()
        Project.objects.all().delete()

        response = self.user_client.get(self.project_endpoint)
        assert response.data.get('results') == []

        query_string = QueryDict(f'page=2')
        endpoint = f'{self.project_endpoint}?{query_string.urlencode()}'
        response = self.user_client.get(endpoint)

        assert response.data.get('errors') == {"Not found": "No objects found."}
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.skip(reason="We have issues with last modified in #321, so it's disabled for now")
    def test_last_modified(self):
        date_2016 = timezone.make_aware(timezone.datetime(2016, 1, 1, 0, 0, 0, 0))
        date_2017 = timezone.make_aware(timezone.datetime(2017, 1, 1, 0, 0, 0, 0))
        Campaign.objects.all().update(updated_at=date_2017)
        Project.objects.all().update(updated_at=date_2017)
        response = self.admin_client.get(self.project_endpoint, HTTP_IF_MODIFIED_SINCE=date_2016)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['campaigns']), Campaign.objects.count())
        self.assertEqual(len(response.data['projects']), Project.objects.count())
        Campaign.objects.all().update(updated_at=date_2016)
        Project.objects.all().update(updated_at=date_2016)
        response = self.admin_client.get(self.project_endpoint, HTTP_IF_MODIFIED_SINCE=date_2017)
        self.assertEqual(response.status_code, 304)

    def test_permissions_campaigns_list(self):
        response = self.hr_client.get(self.project_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.project_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.get(self.project_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.project_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.user_client.get(self.project_endpoint)
        self.assertEqual(response.status_code, 200)


class TestCampaignCreate(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.title = "Big Campaign"
        cls.company = CompanyWithOfficeFactory()
        cls.contact_person = PersonFactory()
        cls.finishing_date = date.today() + timedelta(weeks=10)
        cls.delivery_date = date.today() + timedelta(weeks=12)
        cls.billing_address = cls.company.offices.all()[0].address
        cls.campaign_owner = UserFactory()
        cls.post_data = {
            'title': cls.title,
            'company_uuid': cls.company.id,
            'contact_person_uuid': cls.contact_person.id,
            'finishing_date': cls.finishing_date,
            'delivery_date': cls.delivery_date,
            'campaign_owner_uuid': cls.campaign_owner.id,
            'billing_address_uuid': cls.billing_address.id,
        }
        cls.endpoint = reverse('api:campaign-list')
        cls.response = cls.admin_client.post(cls.endpoint, data=cls.post_data, format='json')
        cls.campaign = Campaign.objects.all().first()

    def test_response_should_return_created_status(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_no_redundant_fields(self):
        self.assertNotIn('projects_uuids', self.response.data.keys())
        self.assertNotIn('billing_address_uuid', self.response.data.keys())

    def test_response_should_contain_the_correct_title(self):
        assert self.response.data['title'] == self.title

    def test_database_should_contain_the_correct_title(self):
        assert self.campaign.title == self.title

    def test_response_should_contain_the_correct_company(self):
        assert self.response.data['company']['uuid'] == str(self.company.id)

    def test_response_should_contain_the_campaign_owner(self):
        assert self.response.data['campaign_owner']['uuid'] == str(self.campaign_owner.id)

    def test_db_should_contain_the_correct_company(self):
        assert self.campaign.company == self.company

    def test_response_should_contain_the_correct_contact_person(self):
        assert self.response.data['contact_person']['uuid'] == str(self.contact_person.id)

    def test_db_should_contain_the_correct_contact_person(self):
        assert self.campaign.contact_person == self.contact_person

    def test_response_should_contain_the_correct_finishing_date(self):
        assert self.response.data['finishing_date'] == str(self.finishing_date)

    def test_db_should_contain_the_correct_finishing_date(self):
        assert self.campaign.finishing_date == self.finishing_date

    def test_response_should_contain_the_correct_delivery_date(self):
        assert self.response.data['delivery_date'] == str(self.delivery_date)

    def test_db_should_contain_the_correct_delivery_date(self):
        assert self.campaign.delivery_date == self.delivery_date

    def test_response_should_contain_the_correct_billing_address(self):
        assert self.response.data['billing_address']['uuid'] == str(self.billing_address.id)

    def test_db_should_contain_the_correct_billing_address(self):
        assert self.campaign.billing_address == self.billing_address

    def test_response_should_contain_optional_description(self):
        post_data = copy.copy(self.post_data)
        description = "This campaign is will make us great."
        post_data['description'] = description
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        assert response.data['description'] == description

    def test_response_should_contain_optional_customer_reference(self):
        customer_reference = "This is the customer reference for that campaign."
        post_data = copy.copy(self.post_data)
        post_data['customer_reference'] = customer_reference
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        assert response.data['customer_reference'] == customer_reference

    def test_create_with_projects_list(self):
        post_data = copy.deepcopy(self.post_data)
        project1 = ProjectFactory(billing_currency="CHF").id
        project2 = ProjectFactory(billing_currency="CHF").id
        project3 = ProjectFactory(billing_currency="CHF").id
        post_data['projects_uuids'] = [
            str(project1), str(project2), str(project3)
        ]
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 201)
        campaign = Campaign.objects.latest('created_at')
        ids = campaign.projects.all().values_list('id', flat=True)
        self.assertIn(project1, ids)
        self.assertIn(project2, ids)
        self.assertIn(project3, ids)

    def test_permissions_campaign_create(self):
        response = self.hr_client.post(self.endpoint, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.endpoint, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.endpoint, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)
        response = self.admin_client.post(self.endpoint, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)
        response = self.admin_client.post(self.endpoint, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)


class TestCampaignDetail(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.campaign = CampaignFactory(billing_currency="CHF")
        cls.project = ProjectFactory(campaign=cls.campaign)
        office = cls.campaign.billing_address.office
        office.name = "Test Office name"
        office.save(update_fields=['name'])
        cls.endpoint = reverse('api:campaign-detail', kwargs={'pk': cls.campaign.id})
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_get_should_return_ok_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_should_contain_id(self):
        self.assertEqual(self.response.data['id'], str(self.campaign.id))

    def test_response_should_contain_the_correct_public_id(self):
        assert self.response.data['public_id'] == str(self.campaign.public_id)

    def test_response_should_contain_the_correct_title(self):
        assert self.response.data['title'] == self.campaign.title

    def test_response_should_contain_the_correct_company(self):
        assert self.response.data['company']['uuid'] == str(self.campaign.company.id)

    def test_response_should_contain_the_correct_contact_person(self):
        assert self.response.data['contact_person']['uuid'] == str(self.campaign.contact_person.id)

    def test_response_should_contain_the_correct_delivery_date(self):
        assert self.response.data['delivery_date'] == str(self.campaign.delivery_date)

    def test_response_should_contain_the_correct_campaign_owner(self):
        assert self.response.data['campaign_owner']['uuid'] == str(self.campaign.campaign_owner.id)

    def test_response_should_contain_billing_address(self):
        to_check = {
            'uuid': str(self.campaign.billing_address.id),
            'name': self.campaign.billing_address.office.name
        }
        self.assertEqual(self.response.data['billing_address'], to_check)

    def test_response_should_contain_projects(self):
        self.assertEqual(self.response.data['projects'][0]['id'], str(self.project.id))

    def test_permissions_campaign_retrieve(self):
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestCampaignUpdate(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.campaign = CampaignFactory(billing_currency="CHF")
        cls.endpoint = reverse('api:campaign-detail', kwargs={'pk': cls.campaign.id})
        cls.login_as_admin()

    def test_change_title(self):
        new_title = "The REAL campaign name"
        patch_data = {'title': new_title}
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.campaign.refresh_from_db()
        assert response.data['title'] == new_title
        assert self.campaign.title == new_title
        self.assertFalse(response.data.get('campaign_owner_uuid'))
        self.assertFalse(response.data.get('contact_person_uuid'))
        self.assertFalse(response.data.get('company_uuid'))
        self.assertEqual(response.data['campaign_owner']['uuid'], str(self.campaign.campaign_owner.id))
        self.assertEqual(response.data['contact_person']['uuid'], str(self.campaign.contact_person.id))
        self.assertEqual(response.data['company']['uuid'], str(self.campaign.company.id))

    def test_change_company(self):
        new_company = CompanyWithOfficeFactory()
        new_billing_address = new_company.offices.all()[0].address
        patch_data = {'company_uuid': new_company.id, 'billing_address_uuid': new_billing_address.id}
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.campaign.refresh_from_db()
        assert response.data['company']['uuid'] == str(new_company.id)
        assert response.data['billing_address']['uuid'] == str(new_billing_address.id)
        assert self.campaign.company == new_company
        assert self.campaign.billing_address == new_billing_address

    def test_change_contact_person(self):
        new_contact_person = PersonFactory()
        patch_data = {'contact_person_uuid': new_contact_person.id}
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.campaign.refresh_from_db()
        assert response.data['contact_person']['uuid'] == str(new_contact_person.id)
        assert self.campaign.contact_person == new_contact_person

    def test_change_description(self):
        new_description = "This describes what this campaign is all about!"
        patch_data = {'description': new_description}
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.campaign.refresh_from_db()
        assert response.data['description'] == new_description
        assert self.campaign.description == new_description

    def test_change_customer_reference(self):
        new_customer_reference = "Referenced by 654321"
        patch_data = {'customer_reference': new_customer_reference}
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.campaign.refresh_from_db()
        assert response.data['customer_reference'] == new_customer_reference
        assert self.campaign.customer_reference == new_customer_reference

    def test_change_start_date(self):
        new_start_date = date.today() - timedelta(days=3)
        patch_data = {'start_date': new_start_date}
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.campaign.refresh_from_db()
        assert response.data['start_date'] == str(new_start_date)
        assert self.campaign.start_date == new_start_date

    def test_change_finishing_date(self):
        new_finishing_date = date.today() + timedelta(weeks=20)
        patch_data = {'finishing_date': new_finishing_date}
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.campaign.refresh_from_db()
        assert response.data['finishing_date'] == str(new_finishing_date)
        assert self.campaign.finishing_date == new_finishing_date

    def test_change_delivery_date(self):
        new_delivery_date = date.today() + timedelta(weeks=25)
        patch_data = {'delivery_date': new_delivery_date}
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.campaign.refresh_from_db()
        assert response.data['delivery_date'] == str(new_delivery_date)
        assert self.campaign.delivery_date == new_delivery_date

    def test_change_billing_address(self):
        new_office = OfficeFactory(company=self.campaign.company)
        new_billing_address = new_office.address
        patch_data = {'billing_address_uuid': new_billing_address.id}
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.campaign.refresh_from_db()
        assert response.data['billing_address']['uuid'] == str(new_billing_address.id)
        assert self.campaign.billing_address == new_billing_address

    def test_change_billing_agreement(self):
        campaign = CampaignFactory(billing_agreement=BILLING_AGREEMENT.ACCORDING_TO_OFFER)
        endpoint = reverse('api:campaign-detail', kwargs={'pk': campaign.id})
        new_billing_agreement = BILLING_AGREEMENT.ACCORDING_TO_EFFORT
        patch_data = {'billing_agreement': new_billing_agreement}
        response = self.admin_client.patch(endpoint, data=patch_data, format='json')
        campaign.refresh_from_db()
        assert response.data['billing_agreement'] == new_billing_agreement
        assert campaign.billing_agreement == new_billing_agreement

    def test_change_billing_interval(self):
        campaign = CampaignFactory(billing_interval=BILLING_INTERVAL.PROJECT_BASED)
        endpoint = reverse('api:campaign-detail', kwargs={'pk': campaign.id})
        new_billing_interval = BILLING_INTERVAL.TASK_BASED
        patch_data = {'billing_interval': new_billing_interval}
        response = self.admin_client.patch(endpoint, data=patch_data, format='json')
        campaign.refresh_from_db()
        assert response.data['billing_interval'] == new_billing_interval
        assert campaign.billing_interval == new_billing_interval

    def test_change_projects_list(self):
        project1 = ProjectFactory(billing_currency="CHF").id
        project2 = ProjectFactory(billing_currency="CHF").id
        project3 = ProjectFactory(billing_currency="CHF").id
        patch_data = {
            'projects_uuids': [str(project1), str(project2), str(project3)]
        }
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        campaign = Campaign.objects.latest('created_at')
        ids = campaign.projects.all().values_list('id', flat=True)
        self.assertIn(project1, ids)
        self.assertIn(project2, ids)
        self.assertIn(project3, ids)
        patch_data = {
            'projects_uuids': []
        }
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.get('projects'), [])

    def test_permissions_campaign_update(self):
        new_description = "This describes what this campaign is all about!"
        patch_data = {'description': new_description}
        response = self.hr_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)


class TestCampaignDelete(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def setUp(self):
        self.campaign = CampaignFactory(billing_currency="CHF")
        self.endpoint = reverse('api:campaign-detail', kwargs={'pk': self.campaign.id})

    def test_delete_campaign_should_allowed_only_for_admins(self):
        response = self.user_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_403_FORBIDDEN

        response = self.admin_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_permissions_campaign_delete(self):
        response = self.hr_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)
        campaign = CampaignFactory(billing_currency="CHF")
        endpoint = reverse('api:campaign-detail', kwargs={'pk': campaign.id})
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_project_manager_campaign_delete(self):
        """ Let's check that users from `User` group can delete this object only if they have
        project_manager relation
        """
        new_user = UserFactory(password="123", username="Additional", role=ROLE.USER)
        verification = EmailAddress.objects.get_or_create(user=new_user,
                                                          email=new_user.email)[0]
        verification.verified = True
        verification.set_as_primary()
        verification.save()
        credentials = {"email": new_user.email, "password": "123"}
        self.login_response = self.client.login(**credentials)
        self.assertTrue(self.login_response)
        response = self.client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        campaign = CampaignFactory(campaign_owner=new_user)
        endpoint = reverse('api:campaign-detail', kwargs={'pk': campaign.id})
        response = self.client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_key_account_manager_permission_campaign_delete(self):
        new_user = UserFactory(password="123", username="Additional", role=ROLE.USER)
        verification = EmailAddress.objects.get_or_create(user=new_user,
                                                          email=new_user.email)[0]
        verification.verified = True
        verification.set_as_primary()
        verification.save()

        company = CompanyWithOfficeFactory(key_account_manager=new_user)
        campaign = CampaignFactory(campaign_owner=self.test_user, company=company)

        credentials = {"email": new_user.email, "password": "123"}
        self.login_response = self.client.login(**credentials)

        # chekc that new user can't update already created one
        response = self.client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)

        endpoint = reverse('api:campaign-detail', kwargs={'pk': campaign.id})
        response = self.client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_delete_campaign_if_project_exists(self):
        ProjectFactory(campaign=self.campaign)
        response = self.admin_client.delete(self.endpoint)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used Campaign"]
            }
        )

    def test_delete_campaign_if_campaign_offer_sent_exists(self):
        CampaignOfferFactory(campaign=self.campaign, campaign_status=OfferStatus.SENT)
        response = self.admin_client.delete(self.endpoint)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used Campaign"]
            }
        )

    def test_delete_campaign_if_campaign_offer_created_exists(self):
        CampaignOfferFactory(campaign=self.campaign, campaign_status=OfferStatus.CREATED)
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)


class TestCampaignProjectListIcons(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestCampaignProjectListIcons, cls).setUpTestData()
        campaign = CampaignFactory(billing_currency="CHF")
        ProjectFactory.create_batch(size=4, campaign=campaign)
        cls.endpoint = reverse('api_campaigns:project-list', kwargs={
            'campaign_pk': campaign.pk
        })

        cls.response = cls.admin_client.get(cls.endpoint)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_icons(self):
        assert all('icons' in item for item in self.response.data)


class TestCampaignProjectDetailIcons(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestCampaignProjectDetailIcons, cls).setUpTestData()
        campaign = CampaignFactory(billing_currency="CHF")
        project = ProjectFactory(campaign=campaign)
        cls.endpoint = reverse('api_campaigns:project-detail', kwargs={
            'campaign_pk': campaign.pk, 'pk': project.pk
        })

        cls.response = cls.admin_client.get(cls.endpoint)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_icons(self):
        assert 'icons' in self.response.data
