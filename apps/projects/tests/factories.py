import random
from datetime import date, timedelta
from decimal import Decimal

import factory
from django.contrib.auth import get_user_model
from djmoney.money import Money

from apps.accounting.tests.factories import AllocationFactory
from apps.activities.models import ActivityGroup, Step, TemplateActivity
from apps.activities.tests.factories import (ActivityGroupFactory, StepFactory,
                                             TemplateActivityFactory,
                                             TemplateActivityStepFactory,
                                             TemplateActivityStepNiceFactory)
from apps.articles.models import ArticleGroup, TemplateArticle
from apps.articles.tests.factories import (ArticleGroupFactory,
                                           TemplateArticleFactory)
from apps.companies.models import Company
from apps.companies.tests.factories import CompanyWithOfficeFactory
from apps.metronom_commons.data import (BILLING_AGREEMENT, BILLING_INTERVAL,
                                        CURRENCY)
from apps.persons.models import Person
from apps.persons.tests.factories import PersonFactory, PersonNiceFactory
from apps.projects.models import (ACCOUNTING_TYPES, ActivityAssignment,
                                  Campaign, Project, ProjectActivity,
                                  ProjectActivityStep, ProjectArticle, WorkLog, ProjectArticleExpense)
from apps.users.tests.factories import UserFactory

User = get_user_model()


class CampaignAndProjectCommonFields(factory.DjangoModelFactory):
    company = factory.SubFactory(CompanyWithOfficeFactory)
    contact_person = factory.SubFactory(PersonFactory)
    finishing_date = factory.Sequence(lambda n: date.today() + timedelta(weeks=1 + n))
    delivery_date = factory.Sequence(lambda n: date.today() + timedelta(weeks=2 + n))
    billing_address = factory.LazyAttribute(lambda obj: obj.company.offices.all()[0].address)
    billing_currency = factory.LazyAttribute(lambda a: random.choice(['CHF', 'EUR', 'USD']))


class CampaignFactory(CampaignAndProjectCommonFields):
    """
        Creates a Campaign.

        self.campaign = CampaignFactory(billing_currency="CHF")

        You can use it with a Project:
        self.project = ProjectFactory(campaign=self.campaign)
    """

    title = factory.Sequence(lambda n: f'campaign-{n}')
    campaign_owner = factory.SubFactory(UserFactory)
    customer_reference = factory.Faker("sentence")

    class Meta:
        model = Campaign


class ProjectFactory(CampaignAndProjectCommonFields):
    """
        Creates a Project.

        self.project = ProjectFactory(billing_currency="CHF")
    """

    title = factory.Sequence(lambda n: f'project-{n}')
    accounting_type = factory.Iterator([value for value, name in ACCOUNTING_TYPES.CHOICES][:2])
    project_owner = factory.SubFactory(UserFactory)

    class Meta:
        model = Project


class CampaignAndProjectNiceFactoryCommonFields(factory.DjangoModelFactory):
    company = factory.Iterator(Company.objects.all())

    start_date = date.today()
    finishing_date = factory.Sequence(lambda n: date.today() + timedelta(weeks=1 + n))
    delivery_date = factory.Sequence(lambda n: date.today() + timedelta(weeks=2 + n))

    billing_agreement = factory.Iterator([x[0] for x in BILLING_AGREEMENT.CHOICES])
    billing_interval = factory.Iterator([x[0] for x in BILLING_INTERVAL.CHOICES])
    billing_address = factory.LazyAttribute(lambda obj: obj.company.offices.all()[0].address)

    description = factory.Faker('sentence')
    customer_reference = factory.Faker('sentence')

    @factory.post_generation
    def contact_person(self, create, extracted, **kwargs):
        if create:
            try:
                contact_person = Person.objects.filter(employments__company=self.company).order_by('?').first()
            except Person.DoesNotExist:
                contact_person = PersonNiceFactory()
            self.contact_person = contact_person
            self.save()


class CampaignNiceFactory(CampaignAndProjectNiceFactoryCommonFields):
    title = factory.Iterator(["Flughafen-Neueröffnung", "1. Halbjahr 2018", "2.Halbjahr 2018", "Frühjahr 2018",
                              "Neupositionierung", "Rebranding", "Branding 2.0", "Redesign Werbedrucksachen",
                              "Neue Marketing-Kampagne", "Relaunch Webseite", "Branding Kampagne"])
    campaign_owner = factory.Iterator(User.objects.all())

    class Meta:
        model = Campaign

    @factory.post_generation
    def projects(self, create, extracted, **kwargs):
        if create:
            for i in range(0, random.randint(0, 6)):
                project = ProjectNiceFactory(company=self.company)
                project.campaign = self
                project.save()


class ProjectActivityFactory(factory.DjangoModelFactory):
    """
        Creates an Project Activity.

        Must be called explicit with a Project, e.g.

        self.project = ProjectFactory(billing_currency="CHF")
        self.project_activity = ProjectActivityFactory(project=self.project)
    """

    display_name = factory.Faker("sentence")
    start_date = date.today()
    end_date = date.today() + timedelta(weeks=2)
    description = factory.Faker("sentence")
    used_template_activity = factory.SubFactory(TemplateActivityFactory)
    activity_group = factory.SubFactory(ActivityGroupFactory)

    class Meta:
        model = ProjectActivity


class ProjectActivityStepFactory(factory.DjangoModelFactory):
    """
        Creates a Project Activity Step.

        Must be called explicit with a Project activity, e.g.

        self.project = ProjectFactory(billing_currency="CHF")
        self.project_activity = ProjectActivityFactory(project=self.project)
        self.project_activity_step = ProjectActivityStepFactory(activity=self.project_activity)
    """

    position = factory.Sequence(lambda n: 0 + n)
    used_step = factory.SubFactory(StepFactory)
    unit = factory.Iterator(["hourly", "daily"])
    activity = factory.SubFactory(ProjectActivityFactory)
    used_template_activity_step = factory.SubFactory(TemplateActivityStepFactory)
    planned_effort = factory.Sequence(lambda n: 1.0 + n)
    start_date = date.today()
    end_date = date.today() + timedelta(weeks=2)

    class Meta:
        model = ProjectActivityStep


class ActivityAssignmentFactory(factory.DjangoModelFactory):
    worker = factory.SubFactory(UserFactory)
    project_activity_step = factory.SubFactory(ProjectActivityStepFactory)
    seen_in_dashboard = True
    notification_sent = True
    accepted = True

    class Meta:
        model = ActivityAssignment


class WorkLogFactory(factory.DjangoModelFactory):
    project_activity_step = factory.SubFactory(ProjectActivityStepFactory)
    worker = factory.SubFactory(UserFactory)
    tracked_time = factory.Sequence(lambda n: 1 + n)
    work_date = factory.Sequence(lambda n: date.today() + timedelta(days=n))
    note = factory.Faker("sentence")
    satisfaction = factory.Iterator([1, 2, 3, 4])

    class Meta:
        model = WorkLog


class ProjectArticleFactory(factory.DjangoModelFactory):
    """
        Creates a Project Article.

        Must be called explicit with a Project, e.g.

        self.project = ProjectFactory(billing_currency="CHF")
        self.project_activity = ProjectActivityFactory(project=self.project)
    """

    display_name = factory.Faker("sentence")
    description = factory.Faker("sentence")
    budget_in_project_currency = Money("10.00", CURRENCY.CHF)
    used_template_article = factory.SubFactory(TemplateArticleFactory)
    article_group = factory.SubFactory(ArticleGroupFactory)
    allocation = factory.SubFactory(AllocationFactory)

    class Meta:
        model = ProjectArticle


class ProjectActivityNiceFactory(factory.DjangoModelFactory):
    project = factory.Iterator(Project.objects.all())
    display_name = factory.Iterator(TemplateActivity.objects.order_by("-id").values_list("name", flat=True))
    start_date = date.today()
    end_date = date.today() + timedelta(weeks=2)
    description = factory.Faker("sentence")
    used_template_activity = factory.Iterator(TemplateActivity.objects.order_by("-id").all())
    activity_group = factory.Iterator(
        ActivityGroup.objects.filter(
            id__in=TemplateActivity.objects.order_by("-id").values_list("activity_group", flat=True)
        )
    )

    class Meta:
        model = ProjectActivity
        django_get_or_create = ('project',)


class ProjectActivityStepNiceFactory(factory.DjangoModelFactory):
    position = factory.Sequence(lambda n: 0 + n)
    used_step = factory.Iterator(Step.objects.all().order_by("id"))
    unit = factory.Iterator(["hourly", "daily"])
    activity = factory.Iterator(ProjectActivity.objects.all())
    used_template_activity_step = factory.SubFactory(TemplateActivityStepNiceFactory)
    planned_effort = factory.Sequence(lambda n: 1.0 + n)
    start_date = date.today()
    end_date = date.today() + timedelta(weeks=2)

    class Meta:
        model = ProjectActivityStep
        django_get_or_create = ('activity',)


class ProjectArticleNiceFactory(factory.DjangoModelFactory):
    project = factory.Iterator(Project.objects.all())
    display_name = factory.Iterator(TemplateArticle.objects.order_by("-id").values_list("name", flat=True))
    description = factory.Faker("sentence")
    budget_in_project_currency = Decimal("10.00")
    used_template_article = factory.Iterator(TemplateArticle.objects.order_by("-id").all())
    article_group = factory.Iterator(
        ArticleGroup.objects.filter(
            id__in=TemplateArticle.objects.order_by("-id").values_list("article_group", flat=True)
        )
    )
    allocation = factory.SubFactory(AllocationFactory)

    class Meta:
        model = ProjectArticle
        django_get_or_create = ('project',)


class ProjectArticleExpenseFactory(factory.DjangoModelFactory):
    name = factory.Faker('name')
    description = factory.Faker('sentence')
    article = factory.SubFactory(ProjectArticleFactory)
    company = factory.SubFactory(CompanyWithOfficeFactory)
    is_company_selected = False
    cost_in_receipt_currency = Decimal('10.00')
    receipt_currency = factory.Iterator([CURRENCY.CHF, CURRENCY.EUR, CURRENCY.USD, CURRENCY.GBP])
    expense_date = date.today()

    class Meta:
        model = ProjectArticleExpense


class ProjectNiceFactory(CampaignAndProjectNiceFactoryCommonFields):
    title = factory.Iterator(["Webseite / Shop", "Facebook Post", "Senior Beratung", "Context - Ausgabe",
                              "Kleinaufträge", "TV-Spot Weihnachten", "TV-Spot", "Werbefilm", "Radio-Spot",
                              "Neue Drucksachen", "PDF-Prospekt", "Landingpage Vertrieb", "Neues Portal",
                              "Weihnachts-Broschüre", "Neue Webseite", "Shooting für Webseite",
                              "Marketingkampagne Los-Gehts", "Newsletter-Erstellung", "Broschüre Messe",
                              "Messeauftritt Kampagne", "Aktion Umbau", "Jubiläums-Webseite"])
    accounting_type = factory.Iterator([x[0] for x in ACCOUNTING_TYPES.CHOICES][:2])
    project_owner = factory.Iterator(User.objects.all())

    class Meta:
        model = Project

    @factory.post_generation
    def contact_person(self, create, extracted, **kwargs):
        if create:
            for i in range(0, random.randint(1, 7)):
                project_activity = ProjectActivityNiceFactory(project=self)
                for i in range(0, random.randint(1, 3)):
                    ProjectActivityStepNiceFactory(activity=project_activity)
            for i in range(0, random.randint(0, 3)):
                ProjectArticleNiceFactory(project=self)
