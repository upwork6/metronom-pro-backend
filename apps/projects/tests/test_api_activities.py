
import copy
import uuid
from datetime import date, timedelta
from django.core.exceptions import ValidationError
import pytest
from allauth.account.models import EmailAddress
from django.contrib.auth.models import Group
from django.urls import reverse
from rest_framework import status

from apps.activities.tests.factories import (ActivityGroupFactory,
                                             TemplateActivityFactory)
from apps.companies.tests.factories import CompanyWithOfficeFactory
from apps.invoices.tests.factories import ActivityInvoicedFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.offers.models import OfferStatus
from apps.offers.tests.factories import ActivityOfferedFactory, OfferFactory, ArticleOfferedFactory
from apps.projects.models import ProjectActivity
from apps.projects.tests.factories import (ProjectActivityFactory,
                                           ProjectActivityStepFactory,
                                           ProjectArticleFactory,
                                           ProjectFactory, WorkLogFactory)
from apps.users.tests.factories import UserFactory
from apps.offers.tests.test_models import TestOfferPropertiesBase


class TestProjectActivityList(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super(TestProjectActivityList, cls).setUpTestData()
        cls.project1 = ProjectFactory(billing_currency="CHF")
        cls.project_activity1 = ProjectActivityFactory(project=cls.project1)
        cls.project_activity2 = ProjectActivityFactory(project=cls.project1)
        cls.list_url = reverse('api_projects:activity-list', kwargs={'project_pk': cls.project1.id})
        cls.response = cls.user_client.get(cls.list_url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_uuid_in_response(self):
        data = [x['uuid'] for x in self.response.data]
        self.assertIn(str(self.project_activity1.id), data)
        self.assertIn(str(self.project_activity2.id), data)

    def test_display_name_in_response(self):
        data = [x['display_name'] for x in self.response.data]
        self.assertIn(self.project_activity1.display_name, data)
        self.assertIn(self.project_activity2.display_name, data)

    def test_total_planned_budget(self):
        self.assertTrue('total_planned_budget' in self.response.data[0].keys())
        self.assertEqual(self.response.data[0]['total_planned_budget'], '0.00')
        pa_step = ProjectActivityStepFactory(activity=self.project_activity1, unit="hourly", planned_effort=4)
        response = self.user_client.get(self.list_url)
        budgets = [x['total_planned_budget'] for x in response.data]
        self.assertIn(str(pa_step.planned_budget.amount), budgets)
        pa_step.delete()

    def test_total_planned_effort_in_hours(self):
        self.assertTrue('total_planned_effort_in_hours' in self.response.data[0].keys())
        self.assertEqual(self.response.data[0]['total_planned_effort_in_hours'], '0')
        pa_step = ProjectActivityStepFactory(activity=self.project_activity1, unit="hourly", planned_effort=4)
        response = self.user_client.get(self.list_url)
        budgets = [x['total_planned_effort_in_hours'] for x in response.data]
        self.assertIn('4.00', budgets)
        pa_step.delete()

    def test_total_real_effort_in_hours(self):
        self.assertTrue('total_real_effort_in_hours' in self.response.data[0].keys())
        self.assertEqual(self.response.data[0]['total_real_effort_in_hours'], '0')

    def test_wrong_uuid(self):
        list_url = reverse('api_projects:activity-list', kwargs={'project_pk': uuid.uuid4()})
        response = self.user_client.get(list_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, [])

    def test_status_exists(self):
        self.assertTrue('status' in self.response.data[0])

    def test_permissions_project_activity_list(self):
        response = self.hr_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        response = self.backoffice_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.admin_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        response = self.user_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)


class TestProjectActivityCreate(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super(TestProjectActivityCreate, cls).setUpTestData()
        cls.activity_group = ActivityGroupFactory()
        cls.template_activity = TemplateActivityFactory(activity_group=cls.activity_group)
        cls.project1 = ProjectFactory(billing_currency="CHF")
        cls.used_project_activity = ProjectActivityFactory(project=cls.project1)
        cls.start_date = date.today() + timedelta(weeks=10)
        cls.end_date = date.today() + timedelta(weeks=12)
        cls.post_data = {
            "start_date": cls.start_date,
            "end_date": cls.end_date,
            "description": "Displayed description different from something",
            "used_template_activity": {'uuid': cls.template_activity.id},
            "used_project_activity": {'uuid': cls.used_project_activity.id},
            "activity_group": {'uuid': cls.activity_group.id},
        }
        cls.list_url = reverse('api_projects:activity-list', kwargs={'project_pk': cls.project1.id})
        cls.response = cls.admin_client.post(cls.list_url, cls.post_data, format="json")

    def test_project_activity_create_without_template_activity(self):
        post_data = {
            "start_date": self.start_date,
            "end_date": self.end_date,
            "description": "Displayed descriptions different from something",
            "used_project_activity": {'uuid': self.used_project_activity.id},
            "activity_group": {'uuid': self.activity_group.id},
        }
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, 201)

    def test_status_code(self):
        self.assertEqual(self.response.status_code, 201)

    def test_if_data_correct(self):
        post_data = copy.copy(self.post_data)
        instance = ProjectActivity.objects.filter(used_project_activity=self.used_project_activity).last()
        for key in post_data.keys():
            value = getattr(instance, key)
            if key in ["used_template_activity", "activity_group", "used_project_activity"]:
                value = value.id
                assert post_data[key]['uuid'] == value
            else:
                assert post_data[key] == value
        self.assertFalse(instance.invoice_status)

    def test_required_fields(self):
        post_data = copy.copy(self.post_data)
        post_data.pop('used_project_activity')
        post_data.pop('used_template_activity')

        for key in post_data.keys():
            new_post_data = copy.copy(post_data)
            del new_post_data[key]
            response = self.admin_client.post(self.list_url, new_post_data, format="json")
            self.assertEqual(response.status_code, 400, key)

    def test_permissions_project_activity_create(self):
        response = self.hr_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)
        response = self.backoffice_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.user_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)
        response = self.admin_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)

    def test_non_required_fields(self):
        post_data = copy.copy(self.post_data)
        post_data.pop('used_project_activity')

        response = self.user_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_nested_with_wrong_uuid(self):
        new_post_data = copy.copy(self.post_data)
        new_post_data["used_template_activity"] = {
            'uuid': uuid.uuid4()
        }
        response = self.user_client.post(self.list_url, new_post_data, format="json")
        self.assertEqual(response.status_code, 400)
        new_post_data = copy.copy(self.post_data)
        new_post_data["activity_group"] = {
            'uuid': uuid.uuid4()
        }
        response = self.user_client.post(self.list_url, new_post_data, format="json")
        self.assertEqual(response.status_code, 400)

    @pytest.mark.skip(reason="No permissions for now")
    def test_project_manager(self):
        project = ProjectFactory(project_owner=self.test_user)
        list_url = reverse('api_projects:activity-list', kwargs={'project_pk': project.id})
        response = self.user_client.post(list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)

        # now let's check that if pm changed - user lose theirs access
        endpoint = reverse('api:project-detail', kwargs={'pk': project.id})
        patch_data = {
            'project_owner_uuid': self.test_admin.id
        }
        response = self.user_client.patch(endpoint, data=patch_data)
        self.assertEqual(response.status_code, 200)
        project.refresh_from_db()
        self.assertEqual(project.project_owner, self.test_admin)

        response = self.user_client.post(list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)

    @pytest.mark.skip(reason="No permissions for now")
    def test_key_account_manager_permission(self):
        post_data = copy.copy(self.post_data)
        new_user = UserFactory(password="123", username="Additional")
        verification = EmailAddress.objects.get_or_create(user=new_user,
                                                          email=new_user.email)[0]
        verification.verified = True
        verification.set_as_primary()
        verification.save()

        company = CompanyWithOfficeFactory(key_account_manager=new_user)
        project = ProjectFactory(project_owner=self.test_user, company=company)

        credentials = {"email": new_user.email, "password": "123"}
        self.login_response = self.user_client.login(**credentials)

        list_url = reverse('api_projects:activity-list', kwargs={'project_pk': project.id})
        response = self.user_client.post(list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)

        # now let's check that if kam changed - user lose theirs access
        endpoint = reverse('api:company-detail', kwargs={'id': company.id})
        patch_data = {
            'key_account_manager_uuid': self.test_user.id
        }
        response = self.user_client.patch(endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        company.refresh_from_db()
        self.assertEqual(company.key_account_manager.id, self.test_user.id)
        # and now it's impossible to create
        response = self.user_client.post(list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)


class TestProjectActivityUpdate(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super(TestProjectActivityUpdate, cls).setUpTestData()
        cls.project1 = ProjectFactory(billing_currency="CHF")
        cls.used_project_activity = ProjectActivityFactory(project=cls.project1)
        cls.project_activity = ProjectActivityFactory(project=cls.project1,
                                                      used_project_activity=cls.used_project_activity)
        cls.patch_data = {
            "display_name": "Displayed name",
        }
        assert cls.project_activity.display_name != cls.patch_data["display_name"]
        cls.endpoint = reverse('api_projects:activity-detail',
                               kwargs={'project_pk': cls.project1.id, 'pk': cls.project_activity.id})
        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format="json")

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_changed_data(self):
        self.project_activity.refresh_from_db()
        self.assertEqual(self.project_activity.display_name, self.patch_data["display_name"])

    def test_change_used_template_activity(self):
        template_activity = TemplateActivityFactory()
        patch_data = {
            'used_template_activity': {
                'uuid': template_activity.id
            }
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.project_activity.refresh_from_db()
        self.assertEqual(self.project_activity.used_template_activity.id, patch_data['used_template_activity']['uuid'])

    def test_change_used_project_activity(self):
        old_used_project_activity_id = self.project_activity.used_project_activity.id
        used_project_activity = ProjectActivityFactory(project=self.project1)
        patch_data = dict(used_project_activity=dict(uuid=used_project_activity.id))

        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        self.project_activity.refresh_from_db()
        assert self.project_activity.used_project_activity.id == patch_data.get('used_project_activity').get('uuid')
        assert self.project_activity.used_project_activity.id != old_used_project_activity_id

    def test_change_activity_group(self):
        activity_group = ActivityGroupFactory()
        patch_data = {
            'activity_group': {
                'uuid': activity_group.id
            }
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.project_activity.refresh_from_db()
        self.assertEqual(self.project_activity.activity_group.id, patch_data['activity_group']['uuid'])

    def test_permissions_project_activity_update(self):
        response = self.hr_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.backoffice_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.user_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)


class ProjectActivityOrderingTest(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project1 = ProjectFactory(billing_currency="CHF")
        cls.endpoint = reverse('project-ordering', kwargs={'project_pk': cls.project1.id})

    def setUp(self):
        super().setUp()
        self.project_activity1 = ProjectActivityFactory(project=self.project1)
        self.project_activity2 = ProjectActivityFactory(project=self.project1)
        self.project_activity3 = ProjectActivityFactory(project=self.project1)
        self.project_activity4 = ProjectActivityFactory(project=self.project1)
        self.project_activity5 = ProjectActivityFactory(project=self.project1)

    def test_moving_backward(self):
        patch_data = {
            'type': 'activity',
            'uuid': self.project_activity1.id,
            'old_position': self.project_activity1.position,
            'new_position': self.project_activity3.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertIn(str(self.project_activity1.id), response.data)
        self.assertIn(str(self.project_activity2.id), response.data)
        self.assertIn(str(self.project_activity3.id), response.data)
        self.assertIn(str(self.project_activity4.id), response.data)
        self.assertIn(str(self.project_activity5.id), response.data)
        self.assertEqual(response.data[str(self.project_activity1.id)], patch_data['new_position'])
        self.project_activity1.refresh_from_db()
        self.project_activity2.refresh_from_db()
        self.project_activity3.refresh_from_db()
        self.project_activity4.refresh_from_db()
        self.project_activity5.refresh_from_db()
        self.assertEqual(self.project_activity1.position, 3)
        self.assertEqual(self.project_activity2.position, 1)
        self.assertEqual(self.project_activity3.position, 2)
        self.assertEqual(self.project_activity4.position, 4)
        self.assertEqual(self.project_activity5.position, 5)

    def test_wrong_new_position_position_in_data(self):
        patch_data = {
            'type': 'activity',
            'uuid': self.project_activity1.id,
            'old_position': self.project_activity1.position,
            'new_position': 143
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_wrong_old_position_in_data(self):
        patch_data = {
            'type': 'activity',
            'uuid': self.project_activity1.id,
            'old_position': 142,
            'new_position': self.project_activity3.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_mismatched_uuid_and_position(self):
        patch_data = {
            'type': 'activity',
            'uuid': self.project_activity1.id,
            'old_position': self.project_activity2.position,
            'new_position': self.project_activity3.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_data_with_article(self):
        article = ProjectArticleFactory(project=self.project1)
        self.assertEqual(article.position, 6)
        patch_data = {
            'type': 'activity',
            'uuid': self.project_activity1.id,
            'old_position': self.project_activity1.position,
            'new_position': article.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.project_activity1.refresh_from_db()
        article.refresh_from_db()
        self.assertEqual(self.project_activity1.position, patch_data['new_position'])
        self.assertEqual(article.position, 5)

    def test_moving_forward(self):
        article = ProjectArticleFactory(project=self.project1)
        self.assertEqual(article.position, 6)
        patch_data = {
            'type': 'activity',
            'uuid': self.project_activity4.id,
            'old_position': self.project_activity4.position,
            'new_position': self.project_activity2.position,
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.project_activity1.refresh_from_db()
        self.project_activity2.refresh_from_db()
        self.project_activity3.refresh_from_db()
        self.project_activity4.refresh_from_db()
        self.project_activity5.refresh_from_db()
        article.refresh_from_db()
        self.assertEqual(self.project_activity1.position, 1)
        self.assertEqual(self.project_activity4.position, 2)
        self.assertEqual(self.project_activity2.position, 3)
        self.assertEqual(self.project_activity3.position, 4)
        self.assertEqual(self.project_activity5.position, 5)
        self.assertEqual(article.position, 6)


class TestProjectActivityDelete(MetronomBaseAPITestCase):

    def setUp(self):
        super().setUp()
        self.project1 = ProjectFactory(billing_currency="CHF")
        self.offer = OfferFactory(project=self.project1)
        self.used_project_activity = ProjectActivityFactory(project=self.project1)
        self.project_activity = ProjectActivityFactory(project=self.project1, used_project_activity=self.used_project_activity)
        self.endpoint = reverse('api_projects:activity-detail',
                                kwargs={'project_pk': self.project1.id, 'pk': self.project_activity.id})

    def test_permissions_project_activity_delete(self):
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)
        project_activity = ProjectActivityFactory(project=self.project1,
                                                  used_project_activity=self.used_project_activity)
        endpoint = reverse('api_projects:activity-detail',
                           kwargs={'project_pk': self.project1.id, 'pk': project_activity.id})
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_project_manager_delete_project_activity(self):
        project = ProjectFactory(project_owner=self.test_admin, title="Checking")
        project_activity = ProjectActivityFactory(project=project)
        detail_endpoint = reverse('api_projects:activity-detail',
                                  kwargs={'project_pk': project.id, 'pk': project_activity.id})
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)

        project = ProjectFactory(project_owner=self.test_user, title="Checking")
        project_activity = ProjectActivityFactory(project=project)
        detail_endpoint = reverse('api_projects:activity-detail',
                                  kwargs={'project_pk': project.id, 'pk': project_activity.id})
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_key_account_manager_delete_project_activity(self):
        company = CompanyWithOfficeFactory(key_account_manager=self.test_admin)
        project = ProjectFactory(project_owner=self.test_admin, company=company)
        project_activity = ProjectActivityFactory(project=project)
        detail_endpoint = reverse('api_projects:activity-detail',
                                  kwargs={'project_pk': project.id, 'pk': project_activity.id})
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)

        company = CompanyWithOfficeFactory(key_account_manager=self.test_user)
        project = ProjectFactory(project_owner=self.test_admin, company=company)
        project_activity = ProjectActivityFactory(project=project)
        detail_endpoint = reverse('api_projects:activity-detail',
                                  kwargs={'project_pk': project.id, 'pk': project_activity.id})
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_delete_project_activity_offer_sent_exists(self):
        ActivityOfferedFactory(project_activity=self.project_activity, offer_status=OfferStatus.SENT, offer=self.offer)
        response = self.admin_client.delete(self.endpoint)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used ProjectActivity"]
            }
        )

    def test_permissions_offer_created_exists(self):
        ActivityOfferedFactory(project_activity=self.project_activity, offer_status=OfferStatus.CREATED, offer=self.offer)
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_invoice_exists(self):
        ActivityInvoicedFactory(activity_offered=self.project_activity)
        response = self.admin_client.delete(self.endpoint)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used ProjectActivity"]
            }
        )

    def test_permission_worklog_exists(self):
        WorkLogFactory(project_activity_step=ProjectActivityStepFactory(activity=self.project_activity))
        response = self.admin_client.delete(self.endpoint)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used ProjectActivity"]
            }
        )


class TestProjectActivityOrArticleStatus(TestOfferPropertiesBase, MetronomBaseAPITestCase):

    def setUp(self):
        super().setUp()
        #   Available instances:
        #   self.project
        #   self.offer
        #   self.project_activity1
        #   self.project_activity2
        #   self.project_article1
        #   self.project_article2
        #   self.project_activity_step
        self.activity_offered1 = ActivityOfferedFactory(project_activity=self.project_activity1, offer=self.offer)
        self.article_offered1 = ArticleOfferedFactory(project_article=self.project_article1, offer=self.offer)
        self.activity_endpoint = reverse('api_projects:activity-detail',
                                kwargs={'project_pk': self.project.id, 'pk': self.project_activity1.id})
        self.article_endpoint = reverse('api_projects:article-detail',
                                         kwargs={'project_pk': self.project.id, 'pk': self.project_article1.id})

    def test_activity_not_editable_when_offer_not_created_anymore(self):

        offer_created_patch_response = self.admin_client.patch(
            self.activity_endpoint,
            {"display_name": "new patch activity"}
        )

        assert offer_created_patch_response.status_code == status.HTTP_200_OK
        assert offer_created_patch_response.data['display_name'] == 'new patch activity'

        self.offer.change_status_to_sent()
        self.project_activity1.refresh_from_db()

        self.assertEquals(self.project_activity1.offer_status, OfferStatus.SENT)

        offer_sent_patch_response = self.admin_client.patch(
            self.activity_endpoint,
            {"display_name": "new patch activity Version 2"}
        )
        self.assertEquals(self.project_activity1.offer_status, OfferStatus.SENT)
        self.assertEquals(offer_sent_patch_response.status_code, status.HTTP_400_BAD_REQUEST)

        self.offer.change_status_to_accepted()
        self.project_activity1.refresh_from_db()

        offer_accepted_patch_response = self.admin_client.patch(
            self.activity_endpoint,
            {"display_name": "Second try to patch activity"}
        )
        self.assertEquals(self.project_activity1.offer_status, OfferStatus.ACCEPTED)
        self.assertEquals(offer_accepted_patch_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_article_not_editable_when_offer_not_created_anymore(self):
        offer_created_patch_response_second_try = self.admin_client.patch(
            self.article_endpoint,
            {"display_name": "new patch Article"}
        )

        assert offer_created_patch_response_second_try.status_code == status.HTTP_200_OK
        assert offer_created_patch_response_second_try.data['display_name'] == 'new patch Article'

        self.offer.change_status_to_sent()
        self.project_article1.refresh_from_db()

        self.assertEquals(self.project_article1.offer_status, OfferStatus.SENT)

        offer_sent_patch_article_response = self.admin_client.patch(
            self.article_endpoint,
            {"display_name": "new patch article Version 2"}
        )
        self.assertEquals(self.project_article1.offer_status, OfferStatus.SENT)
        self.assertEquals(offer_sent_patch_article_response.status_code, status.HTTP_400_BAD_REQUEST)

        self.offer.change_status_to_accepted()
        self.project_article1.refresh_from_db()

        offer_accepted_patch_article_response = self.admin_client.patch(
            self.article_endpoint,
            {"display_name": "Second try to patch Article"}
        )
        self.assertEquals(self.project_article1.offer_status, OfferStatus.ACCEPTED)
        self.assertEquals(offer_accepted_patch_article_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_patch_project_activity_step_when_offer_created(self):
        # unit = ["hourly", "daily"]
        # planned_effort = Decimal("100.00")
        # start_date = date.today()
        # end_date = date.today() + timedelta(weeks=2)
        activity_step_patch_data = {
            "activity_steps": [{
                'uuid': self.project_activity_step.id,
                'unit': 'hourly',
                'used_name': 'First Name',
                'end_date': date.today() + timedelta(weeks=4),
                'used_step': {'uuid': self.step.id}
            }]
        }
        activity_step_patch_response = self.admin_client.patch(
            self.activity_endpoint,
            activity_step_patch_data,
            format='json'
        )
        self.assertEqual(activity_step_patch_response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            activity_step_patch_response.data["activity_steps"][0]["end_date"],
            (date.today() + timedelta(weeks=4)).strftime("%Y-%m-%d")
        )

        self.offer.change_status_to_sent()
        self.project_article1.refresh_from_db()

        self.assertEquals(self.project_article1.offer_status, OfferStatus.SENT)

        offer_sent_activity_step_patch_data = {
            "activity_steps": [{
                'uuid': self.project_activity_step.id,
                # end_date field is not protected
                'end_date': date.today() + timedelta(weeks=2),
                'used_step': {'uuid': self.step.id}
            }]
        }
        offer_sent_activity_step_patch_response = self.admin_client.patch(
            self.activity_endpoint,
            offer_sent_activity_step_patch_data,
            format='json'
        )
        self.assertEqual(offer_sent_activity_step_patch_response.status_code, status.HTTP_200_OK)

        offer_sent_protected_unit_field_patch_data = {
            "activity_steps": [{
                'uuid': self.project_activity_step.id,
                # unit field is protected
                'unit': 'daily',
                'used_step': {'uuid': self.step.id}
            }]
        }
        offer_sent_protected_unit_field_patch_response = self.admin_client.patch(
            self.activity_endpoint,
            offer_sent_protected_unit_field_patch_data,
            format='json'
        )
        self.assertEqual(offer_sent_protected_unit_field_patch_response.status_code, status.HTTP_400_BAD_REQUEST)

        offer_sent_protected_used_name_patch_data = {
            "activity_steps": [{
                'uuid': self.project_activity_step.id,
                # used_name field is protected
                'used_name': 'Testing Name',
                'used_step': {'uuid': self.step.id}
            }]
        }
        offer_sent_protected_used_name_patch_response = self.admin_client.patch(
            self.activity_endpoint,
            offer_sent_protected_used_name_patch_data,
            format='json'
        )
        self.assertEqual(offer_sent_protected_used_name_patch_response.status_code, status.HTTP_400_BAD_REQUEST)
