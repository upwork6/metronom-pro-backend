import copy
from datetime import timedelta
from decimal import Decimal

from django.contrib.auth.models import Group
from django.core.exceptions import ImproperlyConfigured
from django.core.management import call_command
from django.test import override_settings
from django.utils import timezone
from djmoney_rates.settings import money_rates_settings
from djmoney_rates.backends import OpenExchangeBackend
from rest_framework import status
from rest_framework.reverse import reverse

from apps.companies.tests.factories import CompanyWithOfficeFactory
from apps.metronom_commons.data import CURRENCY
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.tests.factories import ProjectFactory, ProjectArticleFactory, ProjectArticleExpenseFactory
from apps.projects.tests.mixins import TestRateBackend


class TestProjectArticleExpenseBase(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company = CompanyWithOfficeFactory()
        cls.project = ProjectFactory(company=cls.company)
        cls.article = ProjectArticleFactory(project=cls.project)

        money_rates_settings.DEFAULT_BACKEND = TestRateBackend
        call_command('update_rates')


class TestProjectArticleExpenseList(TestProjectArticleExpenseBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        ProjectArticleExpenseFactory.create_batch(size=3, article=cls.article)

        cls.endpoint = reverse('api_articles:expense-list', kwargs={
            'project_pk': cls.project.pk,
            'article_pk': cls.article.pk
        })

        cls.response = cls.admin_client.get(cls.endpoint)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_length(self):
        assert isinstance(self.response.data, list)
        assert len(self.response.data) == 3

    def test_response_keys(self):
        expected_keys = ['id', 'name', 'article_uuid', 'company_uuid', 'company', 'is_company_selected',
                         'cost_in_receipt_currency', 'receipt_currency', 'cost_in_default_currency', 'expense_date']

        for x in self.response.data:
            self.assertTrue(all([key in expected_keys for key in x.keys()]))

    def test_permissions(self):
        self.login_as_user()
        response = self.admin_client.get(self.endpoint)
        assert response.status_code == status.HTTP_200_OK

        self.login_as_accounting_user()
        response = self.admin_client.get(self.endpoint)
        assert response.status_code == status.HTTP_200_OK

        self.login_as_hr_user()
        response = self.admin_client.get(self.endpoint)
        assert response.status_code == status.HTTP_200_OK

        self.login_as_backoffice_user()
        response = self.admin_client.get(self.endpoint)
        assert response.status_code == status.HTTP_200_OK

        self.login_as_admin()
        response = self.admin_client.get(self.endpoint)
        assert response.status_code == status.HTTP_200_OK


class TestProjectArticleExpensePost(TestProjectArticleExpenseBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.format = '%Y-%m-%d'

        cls.post_data = dict(
            name='Expense name',
            company_uuid=cls.company.pk,
            article_uuid=cls.article.pk,
            is_company_selected=True,
            cost_in_receipt_currency='5.00',
            receipt_currency=CURRENCY.EUR,
            expense_date=timezone.now().date(),
        )

        cls.endpoint = reverse('api_articles:expense-list', kwargs={
            'project_pk': cls.project.pk,
            'article_pk': cls.article.pk
        })

        cls.response = cls.admin_client.post(cls.endpoint, cls.post_data, format='json')
        cls.not_required_fields = ['expense_date', 'name', 'receipt_currency']

    def test_article_expense_post_response_status(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_article_expense_post_response_name(self):
        assert self.response.data.get('name') == self.post_data.get('name')

    def test_article_expense_post_response_company_name(self):
        company = self.response.data.get('company')
        assert company['name'] == self.company.name

    def test_article_expense_post_response_company_uuid(self):
        company = self.response.data.get('company')
        assert company.get('uuid') == str(self.company.pk)

    def test_article_expense_post_response_article_uuid(self):
        assert self.response.data.get('article_uuid') == self.article.pk

    def test_article_expense_post_response_is_company_selected(self):
        assert self.response.data.get('is_company_selected') == self.post_data.get('is_company_selected')

    def test_article_expense_post_response_cost_in_receipt_currency(self):
        assert self.response.data.get('cost_in_receipt_currency') == self.post_data.get('cost_in_receipt_currency')

    def test_article_expense_post_response_receipt_currency(self):
        assert self.response.data.get('receipt_currency') == self.post_data.get('receipt_currency')

    def test_article_expense_post_response_expense_date(self):
        assert self.response.data.get('expense_date') == self.post_data.get('expense_date').strftime(self.format)

    def test_article_expense_post_permissions(self):
        self.login_as_user()
        response = self.admin_client.post(self.endpoint, self.post_data, format="json")
        assert response.status_code == status.HTTP_201_CREATED

        self.login_as_accounting_user()
        response = self.admin_client.post(self.endpoint, self.post_data, format="json")
        assert response.status_code == status.HTTP_201_CREATED

        self.login_as_hr_user()
        response = self.admin_client.post(self.endpoint, self.post_data, format="json")
        assert response.status_code == status.HTTP_201_CREATED

        self.login_as_backoffice_user()
        response = self.admin_client.post(self.endpoint, self.post_data, format="json")
        assert response.status_code == status.HTTP_201_CREATED

        self.login_as_admin()
        response = self.admin_client.post(self.endpoint, self.post_data, format="json")
        assert response.status_code == status.HTTP_201_CREATED

    def test_required_fields(self):
        post_data = copy.copy(self.post_data)
        [post_data.pop(key) for key in self.not_required_fields]

        for key in post_data.keys():
            new_post_data = copy.copy(post_data)
            new_post_data.pop(key)
            response = self.admin_client.post(self.endpoint, new_post_data, format="json")
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, key)

    def test_not_required_fields(self):
        post_data = copy.copy(self.post_data)
        [post_data.pop(key) for key in self.not_required_fields]

        response = self.user_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_is_company_selected(self):
        post_data = copy.copy(self.post_data)
        post_data.pop('company_uuid')

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(response, {'__all__': ['The field "company_uuid" is required, when is_company_selected=True']})

    def test_is_company_not_selected_1(self):
        post_data = copy.copy(self.post_data)
        post_data['is_company_selected'] = False

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        self.assertErrorResponse(response, {'__all__': ['The field "company_uuid" should not be sent, when is_company_selected=False']})

    def test_is_company_not_selected_2(self):
        post_data = copy.copy(self.post_data)
        post_data['is_company_selected'] = False
        post_data.pop('company_uuid')

        response = self.admin_client.post(self.endpoint, post_data, format='json')
        assert response.status_code == status.HTTP_201_CREATED


class TestProjectArticleExpensePatch(TestProjectArticleExpenseBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.expense = ProjectArticleExpenseFactory(article=cls.article)
        cls.format = '%Y-%m-%d'

        cls.patch_data = dict(
            name='expense name updated',
        )

        cls.endpoint = reverse('api_articles:expense-detail', kwargs={
            'project_pk': cls.project.pk,
            'article_pk': cls.article.pk,
            'pk': cls.expense.pk
        })

        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format='json')

    def test_article_expense_patch_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_article_expense_patch_response_name(self):
        assert self.response.data.get('name') == self.patch_data.get('name')
        assert self.response.data.get('name') != self.expense.name

    def test_article_expense_patch_response_company_uuid(self):
        company = CompanyWithOfficeFactory()
        patch_data = dict(company_uuid=company.pk)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        self.assertErrorResponse(response, {'__all__': ['The field "company_uuid" should not be sent, when is_company_selected=False']})

    def test_article_expense_patch_response_article_uuid(self):
        article = ProjectArticleFactory(project=self.project)
        patch_data = dict(article_uuid=article.pk)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        article_uuid = response.data.get('article_uuid')
        assert article_uuid == article.pk
        assert article_uuid != self.article.pk

    def test_article_expense_patch_response_is_company_selected_1(self):
        assert not self.expense.is_company_selected
        patch_data = dict(is_company_selected=True)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        self.assertErrorResponse(response, {'__all__': ['The field "company_uuid" is required, when is_company_selected=True']})

    def test_article_expense_patch_response_is_company_selected_2(self):
        company = CompanyWithOfficeFactory()
        assert not self.expense.is_company_selected
        patch_data = dict(is_company_selected=True, company_uuid=company.pk)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert response.data.get('is_company_selected')

    def test_article_expense_patch_response_cost_in_receipt_currency(self):
        patch_data = dict(cost_in_receipt_currency=Decimal('20.00'))
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        cost_in_receipt_currency = response.data.get('cost_in_receipt_currency')
        assert cost_in_receipt_currency == str(patch_data.get('cost_in_receipt_currency'))
        assert cost_in_receipt_currency != str(self.expense.cost_in_receipt_currency)

    def test_article_expense_patch_response_receipt_currency(self):
        patch_data = dict(receipt_currency=CURRENCY.USD)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        receipt_currency = response.data.get('receipt_currency')
        assert receipt_currency == patch_data.get('receipt_currency')
        assert receipt_currency != self.expense.receipt_currency

    def test_article_expense_patch_response_expense_date(self):
        patch_data = dict(expense_date=(timezone.now() + timedelta(days=1)).date())
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        expense_date = response.data.get('expense_date')
        assert expense_date == patch_data.get('expense_date').strftime(self.format)
        assert expense_date != self.expense.expense_date.strftime(self.format)

    def test_article_expense_patch_response_cost_is_read_only(self):
        patch_data = dict(cost_in_default_currency=Decimal('20.00'))
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')

        cost_in_default_currency = response.data.get('cost_in_default_currency')
        assert cost_in_default_currency != str(patch_data.get('cost_in_default_currency'))
        assert cost_in_default_currency == str(self.expense.cost_in_default_currency.amount)


class TestProjectArticleExpenseDelete(TestProjectArticleExpenseBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.expense = ProjectArticleExpenseFactory(article=cls.article)
        cls.format = '%Y-%m-%d'
        cls.endpoint = reverse('api_articles:expense-detail', kwargs={
            'project_pk': cls.project.pk,
            'article_pk': cls.article.pk,
            'pk': cls.expense.pk
        })

    def test_non_admin_cannot_delete(self):
        response = self.user_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_response_allocation_delete(self):
        response = self.admin_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_204_NO_CONTENT


class TestProjectArticleExpenseDjangoMoneyRatesSettings(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company = CompanyWithOfficeFactory()
        cls.project = ProjectFactory(company=cls.company)
        cls.article = ProjectArticleFactory(project=cls.project)
        ProjectArticleExpenseFactory.create_batch(size=3, article=cls.article)

        cls.endpoint = reverse('api_articles:expense-list', kwargs={
            'project_pk': cls.project.pk,
            'article_pk': cls.article.pk
        })

    def test_response_openexchange_url(self):
        money_rates_settings.DEFAULT_BACKEND = OpenExchangeBackend
        money_rates_settings.OPENEXCHANGE_APP_ID = 'some-app-id'
        money_rates_settings.OPENEXCHANGE_URL = None

        response = self.admin_client.get(self.endpoint)

        self.assertErrorResponse(response, {'__all__': 'OPENEXCHANGE_URL setting should not be empty when using OpenExchangeBackend'},
                                 status.HTTP_422_UNPROCESSABLE_ENTITY)

    def test_response_openexchange_app_id(self):
        money_rates_settings.DEFAULT_BACKEND = OpenExchangeBackend
        money_rates_settings.OPENEXCHANGE_URL = 'http://openexchangerates.org/api/latest.json'
        money_rates_settings.OPENEXCHANGE_APP_ID = None

        response = self.admin_client.get(self.endpoint)
        print(response.status_code)
        print(response.data)

        self.assertErrorResponse(response, {'__all__': 'OPENEXCHANGE_APP_ID setting should not be empty when using OpenExchangeBackend'},
                                 status.HTTP_422_UNPROCESSABLE_ENTITY)
