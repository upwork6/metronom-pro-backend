from django.test import TestCase
from rest_framework_extensions.test import APIRequestFactory

from apps.projects.models import ProjectActivityStep
from apps.projects.serializers import (CampaignListSerializer,
                                       ProjectActivitySerializer,
                                       ProjectDetailSerializer)
from apps.projects.tests.factories import (ProjectActivityFactory,
                                           ProjectActivityStepFactory,
                                           ProjectFactory)
from apps.projects.tests.mixins import (TestCampaignSetupMixin,
                                        TestProjectSetupMixin)


class TestProjectDetailSerializer(TestProjectSetupMixin):
    @classmethod
    def setUpTestData(cls):
        super(TestProjectDetailSerializer, cls).setUpTestData()
        factory = APIRequestFactory()
        request = factory.get('/api/projects/?include_activities=True')
        cls.serializer = ProjectDetailSerializer(instance=cls.project, context={'request': request})
        cls.keys = cls.serializer.data.keys()

    def test_total_planned_budget_of_activities(self):
        self.assertIn('total_planned_budget_of_activities', self.keys)

    def test_total_planned_budget_of_articles(self):
        self.assertIn('total_planned_budget_of_articles', self.keys)

    def test_total_planned_budget(self):
        self.assertIn('total_planned_budget', self.keys)

    def test_total_planned_budget_of_finished_activities(self):
        self.assertIn('total_planned_budget_of_finished_activities', self.keys)

    def test_total_planned_budget_of_finished_articles(self):
        self.assertIn('total_planned_budget_of_finished_articles', self.keys)

    def test_total_planned_budget_of_finished_items(self):
        self.assertIn('total_planned_budget_of_finished_items', self.keys)

    def test_total_real_cost_of_activities(self):
        self.assertIn('total_real_cost_of_activities', self.keys)

    def test_total_real_cost_of_articles(self):
        self.assertIn('total_real_cost_of_articles', self.keys)

    def test_total_real_cost(self):
        self.assertIn('total_real_cost', self.keys)

    def test_total_real_cost_of_finished_activities(self):
        self.assertIn('total_real_cost_of_finished_activities', self.keys)

    def test_total_real_cost_of_finished_articles(self):
        self.assertIn('total_real_cost_of_finished_articles', self.keys)

    def test_total_real_cost_of_finished_items(self):
        self.assertIn('total_real_cost_of_finished_items', self.keys)

    def test_total_number_of_activities(self):
        self.assertIn('total_number_of_activities', self.keys)

    def test_total_number_of_finished_activities(self):
        self.assertIn('total_number_of_finished_activities', self.keys)

    def test_billing_currency(self):
        self.assertIn('billing_currency', self.keys)

    def test_has_method_setup_eager_loading(self):
        self.assertTrue(hasattr(self.serializer, "setup_eager_loading"))


class TestCampaignListSerializer(TestCampaignSetupMixin):
    @classmethod
    def setUpTestData(cls):
        super(TestCampaignListSerializer, cls).setUpTestData()

        cls.serializer = CampaignListSerializer(instance=cls.campaign)
        cls.keys = cls.serializer.data.keys()

    def test_total_planned_budget(self):
        self.assertIn('total_planned_budget', self.keys)

    def test_total_real_cost(self):
        self.assertIn('total_real_cost', self.keys)

    def test_billing_currency(self):
        self.assertIn('billing_currency', self.keys)

    def test_has_method_setup_eager_loading(self):
        self.assertTrue(hasattr(self.serializer, "setup_eager_loading"))


class TestProjectActivitySerializer(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestProjectActivitySerializer, cls).setUpTestData()
        activity = ProjectActivityFactory(project=ProjectFactory(billing_currency="CHF"))
        ProjectActivityStepFactory(activity=activity, planned_effort=2, unit=ProjectActivityStep.HOURS)
        cls.serializer = ProjectActivitySerializer(instance=activity)
        cls.keys = cls.serializer.data.keys()
        cls.data = cls.serializer.data

    def test_used_template_activity_step_not_in_project_activity(self):
        self.assertNotIn('used_template_activity_step', self.keys)

    def test_used_template_activity_step_in_project_activity_step(self):
        activity_steps = self.data.get('activity_steps')
        self.assertTrue(
            all(['used_template_activity_step' in activity_step.keys() for activity_step in activity_steps])
        )
