from datetime import timedelta

from ddt import ddt, unpack, data
from django.http import QueryDict
from django.urls import reverse
from django.utils import timezone
from queryset_sequence import QuerySetSequence
from rest_framework import status

from apps.companies.tests.factories import CompanyWithOfficeFactory
from apps.metronom_commons.data import CURRENCY, ACCOUNTING_TYPES
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.persons.tests.factories import PersonFactory
from apps.projects.models import Project, Campaign
from apps.projects.tests.factories import ProjectFactory, CampaignFactory, ProjectActivityStepFactory, \
    ProjectActivityFactory, ActivityAssignmentFactory
from apps.users.tests.factories import UserFactory


@ddt
class TestListProjectAndCampaign(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.date_format = '%Y-%m-%d'
        cls.page_size = 10
        cls.start_date = timezone.now().date()
        cls.end_date = timezone.now().date() + timedelta(days=11)
        cls.endpoint = reverse('api:campaign-projects-list')

        cls.person = PersonFactory()
        cls.campaign_owner = UserFactory()
        cls.project_owner = UserFactory()

        cls.company1 = CompanyWithOfficeFactory()
        cls.company2 = CompanyWithOfficeFactory()

        campaigns = CampaignFactory.create_batch(
            size=3, title='first campaign', company=cls.company1, billing_currency=CURRENCY.CHF,
            contact_person=cls.person, campaign_owner=cls.campaign_owner
        )

        current_date = cls.start_date
        while current_date <= cls.end_date:
            ProjectFactory(
                title='first project', billing_currency=CURRENCY.EUR, contact_person=cls.person,
                start_date=current_date, finishing_date=current_date + timedelta(days=12),
                accounting_type=ACCOUNTING_TYPES.INVEST,
            )
            CampaignFactory(
                title='second campaign', company=cls.company2, billing_currency=CURRENCY.USD,
                start_date=current_date, finishing_date=current_date + timedelta(days=12)
            )

            current_date += timedelta(days=1)

        ProjectFactory.create_batch(
            size=3, title='second project', billing_currency=CURRENCY.CHF, project_owner=cls.project_owner,
            accounting_type=ACCOUNTING_TYPES.PROFIT, campaign=campaigns[0]
        )

        activity = ProjectActivityFactory(project=ProjectFactory(accounting_type=ACCOUNTING_TYPES.INTERNAL))
        project_activity_step1 = ProjectActivityStepFactory(activity=activity)
        project_activity_step2 = ProjectActivityStepFactory(activity=activity)
        ActivityAssignmentFactory(worker=cls.test_user, project_activity_step=project_activity_step1)
        ActivityAssignmentFactory(worker=cls.test_user, project_activity_step=project_activity_step2)

        cls.campaign_projects = QuerySetSequence(Project.objects.all(), Campaign.objects.all())
        cls.response = cls.user_client.get(cls.endpoint)

    def get_response(self, query_string):
        endpoint = f'{self.endpoint}?{query_string.urlencode()}'
        return self.user_client.get(endpoint)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK
        assert len(self.response.data.get('results', None)) == self.page_size

    def test_total_result_count(self):
        assert int(self.response.data.get('count')) == self.campaign_projects.count()

    @unpack
    @data(
        dict(title='first project', types=['project']),
        dict(title='second project', types=['project']),
        dict(title='project', types=['project']),
        dict(title='first campaign', types=['campaign']),
        dict(title='second campaign', types=['campaign']),
        dict(title='campaign', types=['campaign']),
        dict(title='first', types=['project', 'campaign']),
        dict(title='second', types=['project', 'campaign']),
    )
    def test_queryparam_title(self, title, types):
        response = self.get_response(QueryDict(f'title={title}'))
        results = response.data.get('results')
        count = response.data.get('count')

        assert count == self.campaign_projects.filter(title__icontains=title).count()
        assert all(title in result.get('title') for result in results)
        assert all(result.get('type') in types for result in results)

    @unpack
    @data(
        dict(billing_currency=CURRENCY.CHF),
        dict(billing_currency=CURRENCY.EUR),
        dict(billing_currency=CURRENCY.USD)
    )
    def test_queryparam_billing_currency(self, billing_currency):
        response = self.get_response(QueryDict(f'billing_currency={billing_currency}'))
        results = response.data.get('results')
        count = response.data.get('count')

        assert count == self.campaign_projects.filter(billing_currency__iexact=billing_currency).count()
        assert all(result.get('billing_currency') == billing_currency for result in results)

    def test_queryparam_company_uuid(self):
        for company_uuid in [self.company1.pk, self.company2.pk]:
            response = self.get_response(QueryDict(f'company_uuid={company_uuid}'))
            results = response.data.get('results')
            count = response.data.get('count')

            assert count == self.campaign_projects.filter(company__id=company_uuid).count()
            assert all(result.get('company').get('uuid') == str(company_uuid) for result in results)

    def test_queryparam_contact_person_uuid(self):
        response = self.get_response(QueryDict(f'contact_person_uuid={self.person.pk}'))
        results = response.data.get('results')
        count = response.data.get('count')

        assert count == self.campaign_projects.filter(contact_person__id=self.person.pk).count()
        assert all(result.get('contact_person').get('uuid') == str(self.person.pk) for result in results)

    def test_queryparam_assigned_to_user_uuid(self):
        response = self.get_response(QueryDict(f'assigned_to_user_uuid={self.test_user.pk}'))
        count = response.data.get('count')

        assert count == Project.objects.filter(
            activities__activity_steps__assignments__worker_id=self.test_user.pk
        ).distinct().count()

    def test_queryparam_start_date_gte(self):
        start_date = self.start_date + timedelta(days=5)
        response = self.get_response(QueryDict(f'start_date_gte={start_date}'))
        results = response.data.get('results')
        count = response.data.get('count')

        assert count > 0
        assert count == self.campaign_projects.filter(start_date__gte=start_date).count()
        assert all(result.get('start_date') >= start_date.strftime(self.date_format) for result in results)

    def test_queryparam_start_date_lte(self):
        start_date = self.start_date + timedelta(days=5)
        response = self.get_response(QueryDict(f'start_date_lte={start_date}'))
        results = response.data.get('results')
        count = response.data.get('count')

        assert count > 0
        assert count == self.campaign_projects.filter(start_date__lte=start_date).count()
        assert all(result.get('start_date') <= start_date.strftime(self.date_format) for result in results)

    def test_queryparam_finishing_date_gte(self):
        finishing_date = self.start_date + timedelta(days=5)
        response = self.get_response(QueryDict(f'finishing_date_gte={finishing_date}'))
        results = response.data.get('results')
        count = response.data.get('count')

        assert count > 1
        assert count == self.campaign_projects.filter(finishing_date__gte=finishing_date).count()
        assert all(result.get('finishing_date') >= finishing_date.strftime(self.date_format) for result in results)

    def test_queryparam_finishing_date_lte(self):
        finishing_date = self.end_date + timedelta(days=5)
        response = self.get_response(QueryDict(f'finishing_date_lte={finishing_date}'))
        results = response.data.get('results')
        count = response.data.get('count')

        assert count > 1
        assert count == self.campaign_projects.filter(finishing_date__lte=finishing_date).count()
        assert all(result.get('finishing_date') <= finishing_date.strftime(self.date_format) for result in results)

    def test_queryparam_project_owner_uuid(self):
        response = self.get_response(QueryDict(f'project_owner_uuid={self.project_owner.pk}'))
        results = response.data.get('results')
        count = response.data.get('count')

        assert count > 1
        assert count == Project.objects.filter(project_owner=self.project_owner).count()
        assert all(result.get('project_owner').get('uuid') <= str(self.project_owner.pk) for result in results)

    def test_queryparam_campaign_owner_uuid(self):
        response = self.get_response(QueryDict(f'campaign_owner_uuid={self.campaign_owner.pk}'))
        results = response.data.get('results')
        count = response.data.get('count')

        assert count > 1
        assert count == Campaign.objects.filter(campaign_owner=self.campaign_owner).count()
        assert all(result.get('campaign_owner').get('uuid') <= str(self.campaign_owner.pk) for result in results)

    @unpack
    @data(
        dict(accounting_type=ACCOUNTING_TYPES.INVEST, expected_result=12),
        dict(accounting_type=ACCOUNTING_TYPES.PROFIT, expected_result=3),
        dict(accounting_type=ACCOUNTING_TYPES.INTERNAL, expected_result=1)
    )
    def test_queryparam_accounting_type(self, accounting_type, expected_result):
        response = self.get_response(QueryDict(f'accounting_type={accounting_type}'))
        results = response.data.get('results')
        count = response.data.get('count')

        assert count == expected_result
        assert count == Project.objects.filter(accounting_type=accounting_type).count()
        assert all(result.get('accounting_type') <= accounting_type for result in results)

    @unpack
    @data(
        dict(belongs_to_campaign=True, expected_result=3),
        dict(belongs_to_campaign='true', expected_result=3),
        dict(belongs_to_campaign='Yes', expected_result=3),
        dict(belongs_to_campaign='yes', expected_result=3),
        dict(belongs_to_campaign='1', expected_result=3),
        dict(belongs_to_campaign=1, expected_result=3),
    )
    def test_queryparam_belongs_to_campaign_true(self, belongs_to_campaign, expected_result):
        response = self.get_response(QueryDict(f'belongs_to_campaign={belongs_to_campaign}'))
        count = response.data.get('count')

        assert count == expected_result
        assert count == Project.objects.exclude(campaign=None).count()

    @unpack
    @data(
        dict(belongs_to_campaign=False, expected_result=13),
        dict(belongs_to_campaign='false', expected_result=13),
        dict(belongs_to_campaign='No', expected_result=13),
        dict(belongs_to_campaign='no', expected_result=13),
        dict(belongs_to_campaign='0', expected_result=13),
        dict(belongs_to_campaign=0, expected_result=13),
    )
    def test_queryparam_belongs_to_campaign_false(self, belongs_to_campaign, expected_result):
        response = self.get_response(QueryDict(f'belongs_to_campaign={belongs_to_campaign}'))
        count = response.data.get('count')

        assert count == expected_result
        assert count == Project.objects.filter(campaign=None).count()
