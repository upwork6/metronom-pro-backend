import copy
import uuid

from django.urls import reverse
from djmoney.money import Money

from apps.accounting.tests.factories import AllocationFactory
from apps.articles.tests.factories import (ArticleGroupFactory,
                                           TemplateArticleFactory)
from apps.companies.tests.factories import CompanyWithOfficeFactory
from apps.invoices.tests.factories import ArticleInvoicedFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.offers.models import OfferStatus
from apps.offers.tests.factories import ArticleOfferedFactory, OfferFactory
from apps.projects.models import ProjectArticle
from apps.projects.tests.factories import (ProjectActivityFactory,
                                           ProjectArticleFactory,
                                           ProjectFactory, ProjectArticleExpenseFactory)


class TestProjectArticleList(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project1 = ProjectFactory(billing_currency="CHF")
        cls.allocation1 = AllocationFactory()
        cls.allocation2 = AllocationFactory()
        cls.project_article1 = ProjectArticleFactory(project=cls.project1, allocation=cls.allocation1)
        cls.project_article2 = ProjectArticleFactory(project=cls.project1, allocation=cls.allocation2)
        cls.list_url = reverse("api_projects:article-list", kwargs={'project_pk': cls.project1.id})
        cls.response = cls.admin_client.get(cls.list_url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_uuid_in_response(self):
        data = [x['uuid'] for x in self.response.data]
        self.assertIn(str(self.project_article1.id), data)
        self.assertIn(str(self.project_article2.id), data)

    def test_display_name_in_response(self):
        data = [x['display_name'] for x in self.response.data]
        self.assertIn(self.project_article1.display_name, data)
        self.assertIn(self.project_article2.display_name, data)

    def test_wrong_uuid(self):
        list_url = reverse('api_projects:activity-list', kwargs={'project_pk': uuid.uuid4()})
        response = self.admin_client.get(list_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, [])

    def test_is_external_cost(self):
        self.assertTrue(all([x.get('is_external_cost') is not None for x in self.response.data]))

    def test_allocation(self):
        data = [x.get('allocation') for x in self.response.data]
        self.assertNotIn(str(self.allocation1.id), data)
        data2 = [x.get('uuid') for x in data if x is not None]
        self.assertIn(str(self.allocation1.id), data2)

    def test_permissions_project_article_list(self):
        response = self.hr_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        response = self.backoffice_client.get(self.list_url)
        self.assertEqual(response.status_code, 403)
        response = self.admin_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)
        response = self.user_client.get(self.list_url)
        self.assertEqual(response.status_code, 200)


class TestProjectArticleCreate(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.article_group = ArticleGroupFactory()
        cls.template_article = TemplateArticleFactory(article_group=cls.article_group)
        cls.project1 = ProjectFactory(billing_currency="CHF")
        cls.allocation = AllocationFactory()
        cls.post_data = {
            "description": "Displayed description different from something",
            "used_template_article": {'uuid': cls.template_article.id},
            "article_group": {'uuid': cls.article_group.id},
            "budget_in_project_currency": "5.00",
            "allocation": {"uuid": cls.allocation.id}
        }
        cls.list_url = reverse("api_projects:article-list", kwargs={'project_pk': cls.project1.id})
        cls.response = cls.admin_client.post(cls.list_url, cls.post_data, format="json")

    def test_project_article_create_status_code(self):
        self.assertEqual(self.response.status_code, 201)

    def test_if_data_correct(self):
        instance = ProjectArticle.objects.filter(description=self.post_data['description']).last()
        for key in self.post_data.keys():
            value = getattr(instance, key)
            if key in ["used_template_article", "article_group", "allocation"]:
                value = value.id
                self.assertEqual(self.post_data[key]['uuid'], value)
            elif key == "budget_in_project_currency":
                self.assertEqual(Money(self.post_data[key], 'CHF'), Money(value.amount, 'CHF'))
            else:
                self.assertEqual(self.post_data[key], value)

        self.assertFalse(instance.invoice_status)

    def test_required_fields(self):
        for key in self.post_data.keys():
            new_post_data = copy.copy(self.post_data)
            del new_post_data["budget_in_project_currency"]
            response = self.admin_client.post(self.list_url, new_post_data, format="json")
            self.assertEqual(response.status_code, 201)

    def test_is_external_cost_and_display_name_and_allocation(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['display_name'] = "New display name"
        post_data['description'] = "New display name"
        post_data['is_external_cost'] = True
        post_data['allocation'] = {'uuid': self.allocation.id}
        response = self.admin_client.post(self.list_url, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        instance = ProjectArticle.objects.filter(description=post_data['description']).last()
        self.assertEqual(instance.is_external_cost, post_data['is_external_cost'])
        self.assertEqual(instance.display_name, post_data['display_name'])
        self.assertEqual(instance.allocation.id, post_data['allocation']['uuid'])

    def test_permissions_project_article_create(self):
        response = self.hr_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)
        response = self.backoffice_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.admin_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)
        response = self.user_client.post(self.list_url, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)

    def test_nested_with_wrong_uuid(self):
        new_post_data = copy.copy(self.post_data)
        new_post_data["used_template_article"] = {
            'uuid': uuid.uuid4()
        }
        response = self.admin_client.post(self.list_url, new_post_data, format="json")
        self.assertEqual(response.status_code, 400)
        new_post_data = copy.copy(self.post_data)
        new_post_data["article_group"] = {
            'uuid': uuid.uuid4()
        }
        response = self.admin_client.post(self.list_url, new_post_data, format="json")
        self.assertEqual(response.status_code, 400)


class TestProjectArticleUpdate(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project1 = ProjectFactory(billing_currency="CHF")
        cls.project_article = ProjectArticleFactory(project=cls.project1)

        cls.patch_data = {
            "display_name": "Displayed name",
        }
        assert cls.project_article.display_name != cls.patch_data["display_name"]
        cls.endpoint = reverse("api_projects:article-detail",
                               kwargs={'project_pk': cls.project1.id, 'pk': cls.project_article.id})
        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format="json")

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_changed_data(self):
        self.project_article.refresh_from_db()
        self.assertEqual(self.project_article.display_name, self.patch_data["display_name"])

    def test_change_is_external_cost(self):
        patch_data = {
            'is_external_cost': True
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.project_article.refresh_from_db()
        self.assertEqual(self.project_article.is_external_cost, patch_data['is_external_cost'])

    def test_change_allocation(self):
        template_article = TemplateArticleFactory()
        allocation = template_article.allocation
        patch_data = {
            'allocation': {
                'uuid': allocation.id
            }
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.project_article.refresh_from_db()
        self.assertEqual(self.project_article.allocation.id, patch_data['allocation']['uuid'])

    def test_project_change_used_template_article(self):
        template_article = TemplateArticleFactory()
        patch_data = {
            'used_template_article': {
                'uuid': template_article.id
            }
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.project_article.refresh_from_db()
        self.assertEqual(self.project_article.used_template_article.id, patch_data['used_template_article']['uuid'])

    def test_change_article_group(self):
        article_group = ArticleGroupFactory()
        patch_data = {
            'article_group': {
                'uuid': article_group.id
            }
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.project_article.refresh_from_db()
        self.assertEqual(self.project_article.article_group.id, patch_data['article_group']['uuid'])

    def test_permissions_project_article_update(self):
        response = self.hr_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.backoffice_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.user_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)


class ProjectArticleOrderingTest(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project1 = ProjectFactory(billing_currency="CHF")
        cls.endpoint = reverse("project-ordering", kwargs={'project_pk': cls.project1.id})

    def setUp(self):
        super().setUp()
        self.project_article1 = ProjectArticleFactory(project=self.project1)
        self.project_article2 = ProjectArticleFactory(project=self.project1)
        self.project_article3 = ProjectArticleFactory(project=self.project1)
        self.project_article4 = ProjectArticleFactory(project=self.project1)
        self.project_article5 = ProjectArticleFactory(project=self.project1)

    def test_simple_case(self):
        patch_data = {
            'type': 'article',
            'uuid': self.project_article2.id,
            'old_position': self.project_article2.position,
            'new_position': self.project_article4.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertIn(str(self.project_article1.id), response.data)
        self.assertIn(str(self.project_article2.id), response.data)
        self.assertIn(str(self.project_article3.id), response.data)
        self.assertIn(str(self.project_article4.id), response.data)
        self.assertIn(str(self.project_article5.id), response.data)
        self.assertEqual(response.data[str(self.project_article2.id)], patch_data['new_position'])
        self.project_article1.refresh_from_db()
        self.project_article2.refresh_from_db()
        self.project_article3.refresh_from_db()
        self.project_article4.refresh_from_db()
        self.project_article4.refresh_from_db()
        self.assertEqual(self.project_article1.position, 1)
        self.assertEqual(self.project_article3.position, 2)
        self.assertEqual(self.project_article4.position, 3)
        self.assertEqual(self.project_article2.position, 4)
        self.assertEqual(self.project_article5.position, 5)

    def test_wrong_new_position_position_in_data(self):
        patch_data = {
            'type': 'article',
            'uuid': self.project_article1.id,
            'old_position': self.project_article1.position,
            'new_position': 143
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_wrong_old_position_in_data(self):
        patch_data = {
            'type': 'article',
            'uuid': self.project_article1.id,
            'old_position': 142,
            'new_position': self.project_article3.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_mismatched_uuid_and_position(self):
        patch_data = {
            'type': 'article',
            'uuid': self.project_article1.id,
            'old_position': self.project_article2.position,
            'new_position': self.project_article3.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_data_with_activity(self):
        activity = ProjectActivityFactory(project=self.project1)
        self.assertEqual(activity.position, 6)
        patch_data = {
            'type': 'article',
            'uuid': self.project_article1.id,
            'old_position': self.project_article1.position,
            'new_position': activity.position
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.project_article1.refresh_from_db()
        activity.refresh_from_db()
        self.assertEqual(self.project_article1.position, patch_data['new_position'])
        self.assertEqual(activity.position, 5)

    def test_corner_case_project_article_sorting(self):
        project2 = ProjectFactory(billing_currency="CHF")
        endpoint = reverse("project-ordering", kwargs={'project_pk': project2.id})
        project_article1 = ProjectArticleFactory(project=project2)
        project_article2 = ProjectArticleFactory(project=project2)
        activity1 = ProjectActivityFactory(project=project2)
        activity2 = ProjectActivityFactory(project=project2)
        for i, obj in enumerate([project_article1, project_article2, activity1, activity2], start=1):
            self.assertEqual(obj.position, i)
        activity3 = ProjectActivityFactory(project=project2)
        patch_data = {
            'type': 'activity',
            'uuid': activity3.id,
            'old_position': activity3.position,
            'new_position': activity2.position
        }
        response = self.admin_client.patch(endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        activity3.refresh_from_db()
        activity2.refresh_from_db()
        patch_data = {
            'type': 'activity',
            'uuid': activity3.id,
            'old_position': activity3.position,
            'new_position': project_article1.position
        }
        response = self.admin_client.patch(endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        activity3.refresh_from_db()
        activity2.refresh_from_db()
        project_article1.refresh_from_db()
        project_article2.refresh_from_db()
        patch_data = {
            'type': 'article',
            'uuid': project_article2.id,
            'old_position': project_article2.position,
            'new_position': activity2.position
        }
        response = self.admin_client.patch(endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_moving_forward(self):
        activity = ProjectActivityFactory(project=self.project1)
        self.assertEqual(activity.position, 6)
        patch_data = {
            'type': 'article',
            'uuid': self.project_article4.id,
            'old_position': self.project_article4.position,
            'new_position': self.project_article2.position,
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.project_article1.refresh_from_db()
        self.project_article2.refresh_from_db()
        self.project_article3.refresh_from_db()
        self.project_article4.refresh_from_db()
        self.project_article5.refresh_from_db()
        activity.refresh_from_db()
        self.assertEqual(self.project_article1.position, 1)
        self.assertEqual(self.project_article4.position, 2)
        self.assertEqual(self.project_article2.position, 3)
        self.assertEqual(self.project_article3.position, 4)
        self.assertEqual(self.project_article5.position, 5)
        self.assertEqual(activity.position, 6)
        # TODO: we need some permisisons check here


class TestProjectArticleDelete(MetronomBaseAPITestCase):

    def setUp(self):
        super().setUp()
        self.project1 = ProjectFactory(billing_currency="CHF")
        self.offer = OfferFactory(project=self.project1)
        self.project_article = ProjectArticleFactory(project=self.project1)
        self.endpoint = reverse("api_projects:article-detail",
                                kwargs={'project_pk': self.project1.id, 'pk': self.project_article.id})

    def test_response_project_article_delete(self):
        before = ProjectArticle.objects.count()
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(before - 1, ProjectArticle.objects.count())

    def test_permission_project_article_delete(self):
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)

    def test_project_manager_project_article_delete(self):
        project = ProjectFactory(project_owner=self.test_user, title="Checking")
        project_article = ProjectArticleFactory(project=project)
        detail_endpoint = reverse("api_projects:article-detail",
                                  kwargs={'project_pk': project.id, 'pk': project_article.id})
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_key_account_manager_project_article_delete(self):
        company = CompanyWithOfficeFactory(key_account_manager=self.test_user)
        project = ProjectFactory(project_owner=self.test_accounting_user, company=company)
        project_article = ProjectArticleFactory(project=project)
        detail_endpoint = reverse("api_projects:article-detail",
                                  kwargs={'project_pk': project.id, 'pk': project_article.id})
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_project_article_expense_exists_delete(self):
        ProjectArticleExpenseFactory(article=self.project_article)
        response = self.admin_client.delete(self.endpoint)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used ProjectArticle"]
            }
        )

    def test_project_article_article_offer_sent_exists_delete(self):

        ArticleOfferedFactory(project_article=self.project_article, offer_status=OfferStatus.SENT, offer=self.offer)
        response = self.admin_client.delete(self.endpoint)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used ProjectArticle"]
            }
        )

    def test_project_article_article_offer_created_exists_delete(self):
        ArticleOfferedFactory(project_article=self.project_article, offer_status=OfferStatus.CREATED, offer=self.offer)
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)

    def test_project_article_invoice_exists_delete(self):
        ArticleInvoicedFactory(article_offered=self.project_article)
        response = self.admin_client.delete(self.endpoint)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used ProjectArticle"]
            }
        )
