
import copy
import datetime
from datetime import date, timedelta

import pytest
from allauth.account.models import EmailAddress
from django.contrib.auth.models import Group
from django.urls import reverse
from djmoney.money import Money
from rest_framework import status

from apps.activities.tests.factories import TemplateActivityStepFactory
from apps.articles.tests.factories import (ArticleGroupFactory,
                                           TemplateArticleFactory)
from apps.companies.tests.factories import (CompanyFactory,
                                            CompanyWithOfficeFactory,
                                            OfficeFactory)
from apps.invoices.tests.factories import InvoiceFactory
from apps.metronom_commons.choices_flow import OfferStatus
from apps.metronom_commons.data import BILLING_AGREEMENT, BILLING_INTERVAL
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.offers.tests.factories import OfferFactory
from apps.persons.tests.factories import PersonFactory
from apps.projects.models import ACCOUNTING_TYPES, Project
from apps.projects.tests.factories import (ActivityAssignmentFactory,
                                           CampaignFactory,
                                           ProjectActivityFactory,
                                           ProjectActivityStepFactory,
                                           ProjectArticleFactory,
                                           ProjectFactory, WorkLogFactory)
from apps.users.tests.factories import UserFactory


class TestProjectCreate(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.title = "Fantastic new Project"
        cls.company = CompanyWithOfficeFactory()
        cls.contact_person = PersonFactory()
        cls.accounting_type = ACCOUNTING_TYPES.PROFIT
        cls.finishing_date = date.today() + timedelta(weeks=4)
        cls.delivery_date = date.today() + timedelta(weeks=6)
        cls.project_owner = UserFactory()
        cls.billing_address = cls.company.offices.all()[0].address
        cls.post_data = {
            'title': cls.title,
            'company_uuid': cls.company.id,
            'contact_person_uuid': cls.contact_person.id,
            'accounting_type': cls.accounting_type,
            'finishing_date': cls.finishing_date,
            'delivery_date': cls.delivery_date,
            'project_owner_uuid': cls.project_owner.id,
            'billing_address_uuid': cls.billing_address.id,
        }
        cls.endpoint = reverse('api:project-list')
        cls.response = cls.admin_client.post(cls.endpoint, data=cls.post_data, format='json')

        cls.project = Project.objects.all().first()

    def test_response_should_return_created_status(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_response_should_contain_the_correct_title(self):
        assert self.response.data['title'] == self.title

    def test_database_should_contain_the_correct_title(self):
        assert self.project.title == self.title

    def test_response_should_contain_the_correct_company(self):
        assert self.response.data['company']['uuid'] == str(self.company.id)

    def test_db_should_contain_the_correct_company(self):
        assert self.project.company == self.company

    def test_response_should_contain_the_correct_contact_person(self):
        assert self.response.data['contact_person']['uuid'] == str(self.contact_person.id)

    def test_db_should_contain_the_correct_contact_person(self):
        assert self.project.contact_person == self.contact_person

    def test_response_should_contain_the_correct_accounting_type(self):
        assert self.response.data['accounting_type'] == self.accounting_type

    def test_db_should_contain_the_correct_accounting_type(self):
        assert self.project.accounting_type == self.accounting_type

    def test_response_should_contain_the_correct_finishing_date(self):
        assert self.response.data['finishing_date'] == str(self.finishing_date)

    def test_db_should_contain_the_correct_finishing_date(self):
        assert self.project.finishing_date == self.finishing_date

    def test_response_should_contain_the_correct_delivery_date(self):
        assert self.response.data['delivery_date'] == str(self.delivery_date)

    def test_db_should_contain_the_correct_delivery_date(self):
        assert self.project.delivery_date == self.delivery_date

    def test_response_should_contain_the_correct_project_owner(self):
        assert self.response.data['project_owner']['uuid'] == str(self.project_owner.id)
        self.assertEqual(self.response.data['project_owner']['name'],
                         "{} {}".format(self.project_owner.first_name, self.project_owner.last_name))

    def test_db_should_contain_the_correct_project_owner(self):
        assert self.project.project_owner == self.project_owner

    def test_response_should_contain_the_correct_billing_address(self):
        assert self.response.data['billing_address']['uuid'] == str(self.billing_address.id)

    def test_db_should_contain_the_correct_billing_address(self):
        assert self.project.billing_address == self.billing_address

    def test_response_should_contain_optional_description(self):
        description = "This project is about solving everybody's problems."
        post_data = copy.copy(self.post_data)
        post_data['description'] = description
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        assert response.data['description'] == description

    def test_response_should_contain_optional_campaign(self):
        campaign = CampaignFactory(billing_currency="CHF")
        post_data = copy.copy(self.post_data)
        post_data['campaign_uuid'] = campaign.id
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        assert response.data['campaign']['uuid'] == str(campaign.id)

    def test_response_should_contain_optional_customer_reference(self):
        customer_reference = "This is the customer reference."
        post_data = copy.copy(self.post_data)
        post_data['customer_reference'] = customer_reference
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        assert response.data['customer_reference'] == customer_reference

    def test_response_should_contain_the_icons(self):
        assert 'icons' in self.response.data

    def test_permissions_project_create(self):
        response = self.hr_client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        self.login_as_accounting_user()
        response = self.accounting_client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 201)
        response = self.admin_client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 201)
        response = self.user_client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 201)


class TestProjectDetail(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.endpoint = reverse('api:project-detail', kwargs={'pk': cls.project.id})
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_get_should_return_ok_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_should_contain_the_correct_public_id(self):
        assert self.response.data['public_id'] == self.project.public_id

    def test_response_should_contain_the_correct_title(self):
        assert self.response.data['title'] == self.project.title

    def test_response_should_contain_the_correct_company(self):
        assert self.response.data['company']['uuid'] == str(self.project.company.id)
        assert self.response.data['company']['public_id'] == self.project.company.public_id
        assert self.response.data['company']['name'] == self.project.company.name

    def test_response_should_contain_the_correct_contact_person(self):
        assert self.response.data['contact_person']['uuid'] == str(self.project.contact_person.id)
        assert self.response.data['contact_person']['public_id'] == self.project.contact_person.public_id
        assert self.response.data['contact_person']['name'] == self.project.contact_person.name
        assert self.response.data['contact_person']['email'] == self.project.contact_person.email.email
        assert self.response.data['contact_person']['phone'] == self.project.contact_person.main_phone

    def test_response_should_contain_the_correct_project_owner(self):
        assert self.response.data['project_owner']['uuid'] == str(self.project.project_owner.id)

    def test_response_should_contain_the_correct_accounting_type(self):
        assert self.response.data['accounting_type'] == self.project.accounting_type

    def test_response_should_contain_the_correct_delivery_date(self):
        assert self.response.data['delivery_date'] == str(self.project.delivery_date)

    def test_response_should_contain_billing_address(self):
        self.assertEqual(self.response.data['billing_address']['uuid'], str(self.project.billing_address.id))

    def test_response_should_contain_currency(self):
        self.assertEqual(self.response.data['billing_currency'], 'CHF')

    def test_response_for_project_with_campaign(self):
        campaign = CampaignFactory(billing_currency="CHF")
        project2 = ProjectFactory(campaign=campaign)
        endpoint = reverse('api:project-detail', kwargs={'pk': project2.id})
        response = self.admin_client.get(endpoint)
        self.assertEqual(response.status_code, 200)
        assert response.data['campaign']['uuid'] == str(project2.campaign.id)
        assert response.data['campaign']['public_id'] == project2.campaign.public_id
        assert response.data['campaign']['title'] == project2.campaign.title

    def test_response_should_contain_the_icons(self):
        assert 'icons' in self.response.data

    def test_permissions_project_retrieve(self):
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestProjectUpdate(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project = ProjectFactory(
            start_date=date.today(),
            finishing_date=date.today() + timedelta(days=7),
            delivery_date=date.today() + timedelta(days=7)
        )
        cls.endpoint = reverse('api:project-detail', kwargs={'pk': cls.project.id})

    def test_change_title(self):
        new_title = "Fancy new project name"
        patch_data = {'title': new_title}
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.project.refresh_from_db()
        assert response.data['title'] == new_title
        assert self.project.title == new_title
        self.assertFalse(response.data.get('campaign_owner_uuid'))
        self.assertFalse(response.data.get('contact_person_uuid'))
        self.assertFalse(response.data.get('company_uuid'))
        self.assertEqual(response.data['project_owner']['uuid'], str(self.project.project_owner.id))
        self.assertEqual(response.data['contact_person']['uuid'], str(self.project.contact_person.id))
        self.assertEqual(response.data['company']['uuid'], str(self.project.company.id))

    def test_change_company(self):
        new_company = CompanyWithOfficeFactory()
        new_billing_address = new_company.offices.all()[0].address
        patch_data = {'company_uuid': new_company.id, 'billing_address_uuid': new_billing_address.id}
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.project.refresh_from_db()
        assert response.data['company']['uuid'] == str(new_company.id)
        assert response.data['billing_address']['uuid'] == str(new_billing_address.id)
        assert self.project.company == new_company
        assert self.project.billing_address == new_billing_address

    def test_change_contact_person(self):
        new_contact_person = PersonFactory()
        patch_data = {'contact_person_uuid': new_contact_person.id}
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.project.refresh_from_db()
        assert response.data['contact_person']['uuid'] == str(new_contact_person.id)
        assert self.project.contact_person == new_contact_person

    def test_change_campaign(self):
        new_campaign = CampaignFactory(billing_currency="CHF")
        patch_data = {'campaign_uuid': new_campaign.id}
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.project.refresh_from_db()
        assert response.data['campaign']['uuid'] == str(new_campaign.id)
        assert self.project.campaign == new_campaign

    def test_change_accounting_type(self):
        project = ProjectFactory(accounting_type=ACCOUNTING_TYPES.PROFIT)
        endpoint = reverse('api:project-detail', kwargs={'pk': project.id})
        new_accounting_type = ACCOUNTING_TYPES.INVEST
        patch_data = {'accounting_type': new_accounting_type}
        response = self.admin_client.patch(endpoint, data=patch_data)
        project.refresh_from_db()
        assert response.data['accounting_type'] == new_accounting_type
        assert project.accounting_type == new_accounting_type

    def test_change_description(self):
        new_description = "This describes what this project is all about!"
        patch_data = {'description': new_description}
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.project.refresh_from_db()
        assert response.data['description'] == new_description
        assert self.project.description == new_description

    def test_change_customer_reference(self):
        new_customer_reference = "Referenced by 123456"
        patch_data = {'customer_reference': new_customer_reference}
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.project.refresh_from_db()
        assert response.data['customer_reference'] == new_customer_reference
        assert self.project.customer_reference == new_customer_reference

    def test_change_start_date(self):
        new_start_date = date.today() - timedelta(days=3)
        patch_data = {'start_date': new_start_date}
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.project.refresh_from_db()
        assert response.data['start_date'] == str(new_start_date)
        assert self.project.start_date == new_start_date

    def test_change_finishing_date(self):
        new_finishing_date = date.today() + timedelta(weeks=2)
        patch_data = {'finishing_date': new_finishing_date}
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.project.refresh_from_db()
        assert response.data['finishing_date'] == str(new_finishing_date)
        assert self.project.finishing_date == new_finishing_date

    def test_change_delivery_date(self):
        new_delivery_date = date.today() + timedelta(weeks=2)
        patch_data = {'delivery_date': new_delivery_date}
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.project.refresh_from_db()
        assert response.data['delivery_date'] == str(new_delivery_date)
        assert self.project.delivery_date == new_delivery_date

    def test_change_project_owner(self):
        new_project_owner = UserFactory()
        patch_data = {'project_owner_uuid': new_project_owner.id}
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.project.refresh_from_db()
        assert response.data['project_owner']['uuid'] == str(new_project_owner.id)
        assert self.project.project_owner == new_project_owner

    def test_change_billing_address(self):
        new_office = OfficeFactory(company=self.project.company)
        new_billing_address = new_office.address
        patch_data = {'billing_address_uuid': new_billing_address.id}
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.project.refresh_from_db()
        assert response.data['billing_address']['uuid'] == str(new_billing_address.id)
        assert self.project.billing_address == new_billing_address

    def test_change_to_wrong_billing_address(self):
        new_office = OfficeFactory(company=CompanyFactory())
        new_billing_address = new_office.address
        patch_data = {'billing_address_uuid': new_billing_address.id, 'company_uuid': self.project.company.id}
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertErrorResponse(response, {'billing_address': [
                                 'The billing address has to be connected to an office of the company.']})

        patch_data = {'billing_address_uuid': new_billing_address.id}
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertErrorResponse(response, {'billing_address': [
                                 'The billing address has to be connected to an office of the company.']})

    def test_change_billing_agreement(self):
        project = ProjectFactory(billing_agreement=BILLING_AGREEMENT.ACCORDING_TO_EFFORT)
        endpoint = reverse('api:project-detail', kwargs={'pk': project.id})
        new_billing_agreement = BILLING_AGREEMENT.ACCORDING_TO_OFFER
        patch_data = {'billing_agreement': new_billing_agreement}
        response = self.admin_client.patch(endpoint, data=patch_data)
        project.refresh_from_db()
        assert response.data['billing_agreement'] == new_billing_agreement
        assert project.billing_agreement == new_billing_agreement

    def test_change_billing_interval(self):
        project = ProjectFactory(billing_interval=BILLING_INTERVAL.PROJECT_BASED)
        endpoint = reverse('api:project-detail', kwargs={'pk': project.id})
        new_billing_interval = BILLING_INTERVAL.TASK_BASED
        patch_data = {'billing_interval': new_billing_interval}
        response = self.admin_client.patch(endpoint, data=patch_data)
        project.refresh_from_db()
        assert response.data['billing_interval'] == new_billing_interval
        assert project.billing_interval == new_billing_interval

    def test_permissions_update_project(self):
        new_title = "Fancy new project name"
        patch_data = {'title': new_title}
        response = self.hr_client.patch(self.endpoint, data=patch_data)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.endpoint, data=patch_data)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.endpoint, data=patch_data)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.assertEqual(response.status_code, 200)
        response = self.user_client.patch(self.endpoint, data=patch_data)
        self.assertEqual(response.status_code, 200)

    def test_change_delivery_date_with_error(self):
        new_delivery_date = self.project.start_date - timedelta(weeks=200)
        patch_data = {'delivery_date': new_delivery_date}
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.assertErrorResponse(
            response, {"delivery_date": ["The delivery date cannot be earlier than the start date."]})

    def test_change_start_date_with_error(self):
        project = ProjectFactory(
            start_date=date.today(),
            finishing_date=date.today() + timedelta(days=7),
            delivery_date=date.today() + timedelta(days=7)
        )
        endpoint = reverse('api:project-detail', kwargs={'pk': project.id})
        new_start_date = project.start_date + timedelta(weeks=222)
        patch_data = {'start_date': new_start_date}
        response = self.admin_client.patch(endpoint, data=patch_data)

        self.assertInErrorResponse(response, {
            'delivery_date': ['The delivery date cannot be earlier than the start date.'],
            'finishing_date': ['The finishing date cannot be earlier than the start date.']
        })

    def test_change_finishing_date_with_error(self):
        new_finishing_date = self.project.start_date - timedelta(weeks=333)
        patch_data = {'finishing_date': new_finishing_date}
        response = self.admin_client.patch(self.endpoint, data=patch_data)
        self.assertErrorResponse(
            response, {'finishing_date': ['The finishing date cannot be earlier than the start date.']})


class TestProjectDelete(MetronomBaseAPITestCase):

    def setUp(self):
        self.project = ProjectFactory(billing_currency="CHF")
        self.endpoint = reverse('api:project-detail', kwargs={'pk': self.project.id})

    def test_delete_project_should_allowed_for_admins(self):
        response = self.user_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_403_FORBIDDEN
        response = self.admin_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_permissions_project_delete(self):
        response = self.hr_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)

    def test_project_manager_for_project_delete(self):
        """ Let's check that users from `User` group can delete this object only if they have
        project_manager relation
        """
        new_user = UserFactory(password="123", username="Additional", role='user')
        verification = EmailAddress.objects.get_or_create(user=new_user,
                                                          email=new_user.email)[0]
        verification.verified = True
        verification.set_as_primary()
        verification.save()
        credentials = {"email": new_user.email, "password": "123"}
        self.login_response = self.client.login(**credentials)
        self.assertTrue(self.login_response)
        response = self.client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        project = ProjectFactory(project_owner=new_user)
        endpoint = reverse('api:project-detail', kwargs={'pk': project.id})
        response = self.client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_key_account_manager_permission_delete_project(self):
        new_user = UserFactory(password="123", username="Additional", role='user')
        verification = EmailAddress.objects.get_or_create(user=new_user,
                                                          email=new_user.email)[0]
        verification.verified = True
        verification.set_as_primary()
        verification.save()

        company = CompanyWithOfficeFactory(key_account_manager=new_user)
        project = ProjectFactory(project_owner=self.test_user, company=company)

        credentials = {"email": new_user.email, "password": "123"}
        self.login_response = self.client.login(**credentials)

        # chekc that new user can't update already created one
        response = self.client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)

        endpoint = reverse('api:project-detail', kwargs={'pk': project.id})
        response = self.client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_delete_project_if_worklog_exists(self):
        project = ProjectFactory(billing_currency="CHF")
        project_activity = ProjectActivityFactory(project=project)
        project_activity_step = ProjectActivityStepFactory(activity=project_activity)
        assignment = ActivityAssignmentFactory(
            worker=self.test_admin, project_activity_step=project_activity_step)
        worklogs = WorkLogFactory(
            project_activity_step=project_activity_step, worker=self.test_admin
        )
        endpoint = reverse('api:project-detail', kwargs={'pk': project.id})
        response = self.admin_client.delete(endpoint)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used Project"]
            }
        )

    def test_delete_project_if_invoice_exists(self):
        project = ProjectFactory(billing_currency="CHF")
        invoice = InvoiceFactory(project=project)
        endpoint = reverse('api:project-detail', kwargs={'pk': project.id})
        response = self.admin_client.delete(endpoint)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used Project"]
            }
        )

    def test_delete_project_if_offer_sent_exists(self):
        project = ProjectFactory(billing_currency="CHF")
        offer = OfferFactory(project=project, offer_status=OfferStatus.SENT)
        endpoint = reverse('api:project-detail', kwargs={'pk': project.id})
        response = self.admin_client.delete(endpoint)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used Project"]
            }
        )

    def test_delete_project_if_offer_created_exists(self):
        project = ProjectFactory(billing_currency="CHF")
        offer = OfferFactory(project=project, offer_status=OfferStatus.CREATED)
        endpoint = reverse('api:project-detail', kwargs={'pk': project.id})
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 204)


class ProjectTotalBudgetTests(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.article_group = ArticleGroupFactory()
        cls.template_article = TemplateArticleFactory(article_group=cls.article_group)
        cls.allocation = cls.template_article.allocation
        cls.template_activity_step = TemplateActivityStepFactory()
        cls.step = cls.template_activity_step.step
        cls.project = ProjectFactory(
            start_date=date.today(),
            finishing_date=date.today() + timedelta(days=7),
            delivery_date=date.today() + timedelta(days=7),
            billing_currency="EUR"
        )
        cls.project_activity1 = ProjectActivityFactory(project=cls.project)
        cls.article_list_url = reverse("api_projects:article-list", kwargs={'project_pk': cls.project.id})
        cls.activity_list_url = reverse('api_projects:activity-list', kwargs={'project_pk': cls.project.id})
        cls.project_detail_url = reverse('api:project-detail', kwargs={'pk': cls.project.id})
        cls.project_activity_step_endpoint = reverse('api_projects:activity-detail',
                                                     kwargs={'project_pk': cls.project.id, 'pk': cls.project_activity1.id})

    def test_project_article_with_different_currency_creation(self):
        post_data = {
            "description": "Displayed description different from something",
            "used_template_article": {'uuid': self.template_article.id},
            "article_group": {'uuid': self.article_group.id},
            "budget_in_project_currency": "5.00",
            "allocation": {"uuid": self.allocation.id}
        }
        response = self.admin_client.post(self.article_list_url, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(self.project.total_planned_budget,
                         Money(post_data['budget_in_project_currency'], self.project.billing_currency))

    def test_project_activity_with_different_currency_creation(self):
        patch_data = {
            "activity_steps": [{
                'used_template_activity_step': {'uuid': self.template_activity_step.pk},
                'planned_effort': 5.0,
                'used_step': {'uuid': self.step.pk},
                'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
                'unit': 'hourly',
                'position': 1,
            }]
        }
        response = self.admin_client.patch(self.project_activity_step_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        patch_data = {
            "billing_currency": "USD"
        }
        response = self.admin_client.patch(self.project_detail_url, patch_data, format="json")
        # you can't edit those, because there is activity steps with prefileed used rate already
        self.assertEqual(response.status_code, 400)

    def test_project_article_change_billing_currency(self):
        post_data = {
            "description": "Displayed description different from something",
            "used_template_article": {'uuid': self.template_article.id},
            "article_group": {'uuid': self.article_group.id},
            "budget_in_project_currency": "5.00",
            "allocation": {"uuid": self.allocation.id}
        }
        response = self.admin_client.post(self.article_list_url, post_data, format="json")
        self.assertEqual(response.status_code, 201)

        patch_data = {
            "billing_currency": "USD"
        }
        response = self.admin_client.patch(self.project_detail_url, patch_data, format="json")
        self.assertEqual(response.status_code, 400)


class TestProjectApiSoftDeletion(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project1 = ProjectFactory(project_owner=cls.test_user)
        cls.project2 = ProjectFactory(project_owner=cls.test_user)
        cls.endpoint = reverse('api:project-list')
        cls.detail_endpoint = reverse('api:project-detail', args=[cls.project1.id])
        cls.project1.delete()

    def test_user_response(self):
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        data = response.data["results"]
        self.assertEqual(len(data), 1)
        self.assertFalse("is_deleted" in data[0].keys())

    def test_admin_response(self):
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        data = response.data["results"]
        self.assertEqual(len(data), 2)
        self.assertTrue(any([x["is_deleted"] for x in data]))

    def test_user_detail_response(self):
        response = self.user_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 404)

    def test_admin_detail_response(self):
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertTrue("is_deleted" in response.data.keys())
