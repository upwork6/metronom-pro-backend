
from datetime import date, timedelta
from decimal import Decimal
from unittest.mock import patch

import pytest
from django.core.exceptions import ValidationError
from django.core.management import call_command
from django.test import TestCase, override_settings
from django.utils import timezone
from djmoney.money import Money

from apps.activities.tests.factories import (ActivityGroupFactory, RateFactory,
                                             StepFactory,
                                             TemplateActivityFactory)
from apps.addresses.tests.factories import AddressFactory
from apps.companies.tests.factories import CompanyWithOfficeFactory
from apps.metronom_commons.choices_flow import (ActivityArticleOfferedStatus,
                                                OfferStatus)
from apps.metronom_commons.data import (ACTIVITY_STATUS, BILLING_INTERVAL,
                                        CURRENCY, PROJECT_ICONS)
from apps.offers.tests.factories import (ActivityOfferedFactory,
                                         ArticleOfferedFactory, OfferFactory)
from apps.projects.models import (ActivityAssignment, Campaign, Project,
                                  ProjectActivityStep, ProjectActivity, ProjectArticle,
                                  WorkLog)
from apps.projects.tests.factories import (ActivityAssignmentFactory,
                                           CampaignFactory,
                                           ProjectActivityFactory,
                                           ProjectActivityStepFactory,
                                           ProjectArticleExpenseFactory,
                                           ProjectArticleFactory,
                                           ProjectFactory, WorkLogFactory)
from apps.projects.tests.mixins import (TestCampaignSetupMixin,
                                        TestProjectSetupMixin, TestRateBackend)
from apps.users.tests.factories import UserFactory
from djmoney_rates.settings import money_rates_settings
from djmoney_rates.utils import convert_money


class TestCampaignCreate(TestCase):

    def setUp(self):
        self.campaign = CampaignFactory(billing_currency="CHF")

    def test_get_instance(self):
        project_from_db = Campaign.objects.get(pk=self.campaign.pk)
        assert project_from_db == self.campaign

    def test_start_date_default_sets_today(self):
        assert self.campaign.start_date == date.today()

    def test_first_number(self):
        assert self.campaign.number == 1

    def test_second_number(self):
        campaign2 = CampaignFactory(billing_currency="CHF")
        assert campaign2.number == 2

    def test_first_year_number(self):
        assert self.campaign.year_number == int(f'{date.today().year}')

    def test_second_year_number(self):
        campaign2 = CampaignFactory(billing_currency="CHF")
        assert campaign2.year_number == int(f'{date.today().year}')

    def test_advancing_to_new_year(self):
        next_year_date = date.today() + timedelta(weeks=53)
        with patch('apps.projects.models.date') as mock_date:
            mock_date.today.return_value = next_year_date
            next_year_campaign = CampaignFactory(billing_currency="CHF")
        assert next_year_campaign.year_number == int(f'{next_year_date.year}')


class TestCampaignStringRepresentations(TestCase):

    def setUp(self):
        self.company_name = "TOP GmbH"
        self.campaign_title = "Fancy Campaign"
        self.company = CompanyWithOfficeFactory(name=self.company_name)
        self.campaign = CampaignFactory(title=self.campaign_title, company=self.company)

    def test_str(self):
        assert str(self.campaign) == f"{self.campaign_title}"

    def test_public_id(self):
        assert self.campaign.public_id == f'K-{date.today().year}-0001'


@override_settings(LANGUAGE_CODE='en')
class TestCampaignValidation(TestCase):

    def setUp(self):
        self.yesterday = date.today() - timedelta(days=1)

    def test_finishing_date_is_after_start_date(self):
        with self.assertRaisesMessage(ValidationError, 'The finishing date cannot be earlier than the start date.'):
            CampaignFactory(finishing_date=self.yesterday)

    def test_delivery_date_is_after_start_date(self):
        with self.assertRaisesMessage(ValidationError, 'The delivery date cannot be earlier than the start date.'):
            CampaignFactory(delivery_date=self.yesterday)

    def test_billing_address_is_associated_with_company(self):
        unconnected_address = AddressFactory()
        with self.assertRaisesMessage(
                ValidationError, 'The billing address has to be connected to an office of the company.'):
            CampaignFactory(billing_address=unconnected_address)


class TestProjectCreate(TestCase):

    def setUp(self):
        self.project = ProjectFactory(billing_currency="CHF")

    def test_get_instance(self):
        project_from_db = Project.objects.get(pk=self.project.pk)
        assert project_from_db == self.project

    def test_start_date_default_sets_today(self):
        assert self.project.start_date == date.today()

    def test_first_number(self):
        assert self.project.number == 1

    def test_second_number(self):
        project2 = ProjectFactory(billing_currency="CHF")
        assert project2.number == 2

    def test_first_year_number(self):
        assert self.project.year_number == int(f'{date.today().year}')

    def test_second_year_number(self):
        project2 = ProjectFactory(billing_currency="CHF")
        assert project2.year_number == int(f'{date.today().year}')

    def test_advancing_to_new_year(self):
        next_year_date = date.today() + timedelta(weeks=53)
        with patch('apps.projects.models.date') as mock_date:
            mock_date.today.return_value = next_year_date
            next_year_project = ProjectFactory(billing_currency="CHF")
        assert next_year_project.year_number == int(f'{next_year_date.year}')


class TestProjectStringRepresentations(TestCase):

    def setUp(self):
        self.company_name = "Tip GmbH"
        self.project_title = "Big Project"
        self.company = CompanyWithOfficeFactory(name=self.company_name)
        self.project = ProjectFactory(title=self.project_title, company=self.company)

    def test_str(self):
        assert str(self.project) == f"{self.project_title}"

    def test_public_id(self):
        assert self.project.public_id == f'P-{date.today().year}-0001'


@override_settings(LANGUAGE_CODE='en')
class TestProjectValidation(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.yesterday = date.today() - timedelta(days=1)

    def test_finishing_date_is_after_start_date(self):
        with self.assertRaisesMessage(ValidationError, 'The finishing date cannot be earlier than the start date.'):
            ProjectFactory(finishing_date=self.yesterday)

    def test_delivery_date_is_after_start_date(self):
        with self.assertRaisesMessage(ValidationError, 'The delivery date cannot be earlier than the start date.'):
            ProjectFactory(delivery_date=self.yesterday)

    def test_billing_address_is_associated_with_company(self):
        unconnected_address = AddressFactory()
        with self.assertRaisesMessage(
                ValidationError, 'The billing address has to be connected to an office of the company.'):
            ProjectFactory(billing_address=unconnected_address)

    def test_internal_project_validator(self):
        campaign = CampaignFactory()
        with self.assertRaisesMessage(ValidationError, "Internal project can't be added to the campaign."):
            ProjectFactory(accounting_type='internal', campaign=campaign)


class TestProjectActivityStep(TestCase):

    @classmethod
    def setUpTestData(cls):
        worker = UserFactory()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.activity = ProjectActivityFactory(project=cls.project)
        cls.step1 = ProjectActivityStepFactory(activity=cls.activity, planned_effort=2, unit="hourly")
        cls.step2 = ProjectActivityStepFactory(activity=cls.activity, planned_effort=2, unit="daily")
        cls.step3 = ProjectActivityStepFactory(activity=cls.activity)
        for x in [cls.step1, cls.step2, cls.step3] * 2:
            WorkLogFactory(worker=worker, project_activity_step=x, tracked_time=1)
        WorkLogFactory(worker=worker, project_activity_step=cls.step3, tracked_time=1)

    def test_real_effort_in_hours(self):
        self.assertEqual(self.step1.real_effort_in_hours, Decimal('2.00'))
        self.assertEqual(self.step2.real_effort_in_hours, Decimal('2.00'))
        self.assertEqual(self.step3.real_effort_in_hours, Decimal('3.00'))

    def test_planned_budget(self):
        rate = RateFactory(step=self.step1.used_step, currency='CHF')
        rate2 = RateFactory(step=self.step2.used_step, currency='CHF')
        self.assertEqual(self.step1.planned_budget.amount, rate.hourly_rate.amount * 2)
        self.assertEqual(self.step2.planned_budget.amount, rate2.daily_rate.amount * 2)

    def test_get_rate(self):
        rate = RateFactory(step=self.step1.used_step, currency='CHF', hourly_rate=0, daily_rate=0)
        rate2 = RateFactory(step=self.step2.used_step, currency='CHF', hourly_rate=0, daily_rate=0)
        self.step1.used_step.rates.update(hourly_rate=0, daily_rate=0)
        self.step2.used_step.rates.update(hourly_rate=0, daily_rate=0)
        self.assertEqual(self.step1.get_rate(currency="CHF").amount, 0)
        self.assertEqual(self.step2.get_rate(currency="CHF").amount, 0)

    def test_activity_step_name(self):
        step_template = StepFactory(name="Step Template")
        activity_step = ProjectActivityStepFactory(
            activity=self.activity, planned_effort=2, unit="hourly", used_step=step_template
        )
        self.assertEqual(activity_step.name, step_template.name)

        step_template.name = "Some Other Name"
        step_template.save()
        step_template.refresh_from_db()

        self.assertEqual(step_template.name, "Some Other Name")
        self.assertNotEqual(step_template.name, activity_step.name)


class TestProjectActivity(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.step1 = ProjectActivityStepFactory(activity=cls.project_activity,
                                               planned_effort=4, unit=ProjectActivityStep.HOURS)
        cls.user = UserFactory()

    def test_total_planned_budget(self):
        self.assertEqual(self.project_activity.total_planned_budget, self.step1.used_rate * 4)

    def test_display_name_empty(self):
        project_activity = ProjectActivityFactory(project=self.project, display_name='')
        self.assertIsNotNone(project_activity.display_name)
        self.assertEqual(project_activity.name, project_activity.used_template_activity.name)
        self.assertNotEqual(project_activity.display_name, project_activity.used_template_activity.name)

    def test_test_display_name_not_empty(self):
        project_activity = ProjectActivityFactory(project=self.project, display_name='ASdf1')
        self.assertIsNotNone(project_activity.display_name)
        self.assertNotEqual(project_activity.name, project_activity.used_template_activity.name)
        self.assertNotEqual(project_activity.display_name, project_activity.used_template_activity.name)
        self.assertEqual(project_activity.name, project_activity.display_name)

    def test_total_planned_effort_in_hours(self):
        self.assertEqual(self.project_activity.total_planned_effort_in_hours, Decimal("4.00"))

    def test_amount_with_several_rates(self):
        step = ProjectActivityStep.objects.filter(
            activity=self.project_activity).last().used_step
        RateFactory(step=step, currency="USD")
        RateFactory(step=step, currency="GBP")
        amount = ProjectActivityStep.objects.filter(
            activity=self.project_activity).last().used_step.rates.filter(currency="CHF").last().hourly_rate.amount
        self.assertEqual(self.project_activity.total_planned_budget.amount, amount * 4)

    def test_amount_with_several_steps(self):
        project_activity = ProjectActivityFactory(project=self.project)
        total_amount = 0
        for i in range(1, 5):
            pa_step = ProjectActivityStepFactory(activity=project_activity, unit="hourly")
            pa_step.planned_effort = Decimal(str(i))
            pa_step.save()
            total_amount += pa_step.used_step.rates.filter(currency="CHF").last().hourly_rate * i
        self.assertEqual(project_activity.total_planned_budget.amount, total_amount.amount)

    def test_daily_amount(self):
        project_activity = ProjectActivityFactory(project=self.project)
        pa_step = ProjectActivityStepFactory(activity=project_activity, unit="daily")
        pa_step.planned_effort = Decimal("4.00")
        pa_step.save()

        self.assertEqual(project_activity.total_planned_budget, pa_step.used_rate * 4)

    def test_activity_already_accepted(self):
        offer = OfferFactory(project=self.project)
        ActivityOfferedFactory(
            offer=offer,
            project_activity=self.project_activity
        )
        offer.offer_status = OfferStatus.SENT
        offer.save()
        offer.offer_status = OfferStatus.ACCEPTED
        offer.save()
        the_offer = self.project_activity.already_accepted()

        assert self.project_activity.already_accepted().offer_status == OfferStatus.ACCEPTED, \
            'The Offer has the status `accepted`'
        assert the_offer == offer, \
            'if projectActivity belongs to an OfferedActivity ACCEPTED Should return that Offer'

    def test_position(self):
        self.assertEqual(self.project_activity.position, 1)
        project_activity = ProjectActivityFactory(project=Project.objects.last())
        self.assertEqual(project_activity.position, 2)
        project_article = ProjectArticleFactory(project=Project.objects.last())
        self.assertEqual(project_article.position, 3)
        project_activity = ProjectActivityFactory(project=Project.objects.last())
        self.assertEqual(project_activity.position, 4)

    def test_total_real_effort_in_hours(self):
        self.assertEqual(self.project_activity.total_real_effort_in_hours, 0)
        WorkLogFactory(worker=self.user, project_activity_step=self.step1, tracked_time=3)
        self.assertEqual(str(self.project_activity.total_real_effort_in_hours), '3.00')

    def test_total_real_cost(self):
        step2 = ProjectActivityStepFactory(activity=self.project_activity, planned_effort=2,
                                           unit=ProjectActivityStep.DAYS)
        WorkLogFactory.create_batch(size=3, worker=self.user, project_activity_step=self.step1, tracked_time=1)
        WorkLogFactory.create_batch(size=3, worker=self.user, project_activity_step=step2, tracked_time=1)
        amount_hourly = (self.step1.real_effort_in_hours * self.step1.get_rate(self.project_activity.currency)).amount
        amount_daily = (step2.real_working_days * step2.get_rate(self.project_activity.currency)).amount
        assert self.project_activity.total_real_cost.amount == sum([amount_hourly, amount_daily])


class TestProjectArticle(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.discount = Money(Decimal('1.00'), 'CHF')
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_article1 = ProjectArticleFactory(project=cls.project)
        cls.user = UserFactory()
        cls.project_article = ProjectArticleFactory(project=cls.project)

        money_rates_settings.DEFAULT_BACKEND = TestRateBackend
        call_command('update_rates')

    def test_article_budget_in_project_currency(self):
        self.budget = Money(Decimal('100.00'), 'CHF')
        self.project_article1 = ProjectArticleFactory(
            budget_in_project_currency=self.budget,
            project=self.project
        )
        assert self.budget == self.project_article1.budget_in_project_currency

    def test_display_name_empty(self):
        project_article = ProjectArticleFactory(display_name='', project=self.project)
        self.assertIsNotNone(project_article.display_name)
        self.assertEqual(project_article.name, project_article.used_template_article.name)
        self.assertNotEqual(project_article.display_name, project_article.used_template_article.name)

    def test_test_display_name_not_empty(self):
        project_article = ProjectArticleFactory(display_name='ASdf1', project=self.project)
        self.assertIsNotNone(project_article.display_name)
        self.assertNotEqual(project_article.name, project_article.used_template_article.name)
        self.assertNotEqual(project_article.display_name, project_article.used_template_article.name)
        self.assertEqual(project_article.name, project_article.display_name)

    def test_position(self):
        self.assertEqual(self.project_article1.position, 1)
        self.assertEqual(self.project_article.position, 2)
        project_article = ProjectArticleFactory(project=Project.objects.last())
        self.assertEqual(project_article.position, 3)
        project_activity = ProjectActivityFactory(project=Project.objects.last())
        self.assertEqual(project_activity.position, 4)
        project_article = ProjectArticleFactory(project=Project.objects.last())
        self.assertEqual(project_article.position, 5)

    def test_total_real_cost(self):
        ProjectArticleExpenseFactory.create_batch(
            size=4, article=self.project_article1, cost_in_receipt_currency='25.00', receipt_currency=CURRENCY.EUR)

        ProjectArticleExpenseFactory.create_batch(
            size=4, article=self.project_article1, cost_in_receipt_currency='15.00', receipt_currency=CURRENCY.GBP)

        total_real_cost = sum(
            [expense.cost_in_default_currency for expense in self.project_article1.article_expenses.all()]
        )
        assert self.project_article1.total_real_cost == total_real_cost
        assert self.project_article1.total_real_cost.amount == total_real_cost.amount
        assert str(self.project_article1.total_real_cost.currency) == self.project_article1.currency


class TestActivityAssignment(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.worker = UserFactory()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_activity_step = ProjectActivityStepFactory(activity=cls.project_activity)

        cls.assignment = ActivityAssignmentFactory(
            worker=cls.worker, project_activity_step=cls.project_activity_step)

    def test_assignment_to_user(self):
        assert ActivityAssignment.objects.filter(
            worker=self.worker,
            project_activity_step=self.project_activity_step,
            project_activity_step__activity=self.project_activity,
            project_activity_step__activity__project=self.project
        ).exists()


class TestWorkLog(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.worker = UserFactory()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_activity_step = ProjectActivityStepFactory(activity=cls.project_activity)

    def test_worklog_when_activity_assigned_to_user(self):
        assignment = ActivityAssignmentFactory(worker=self.worker, project_activity_step=self.project_activity_step)
        worklog = WorkLogFactory(worker=self.worker, project_activity_step=self.project_activity_step)

        assert worklog.project_activity_step == assignment.project_activity_step
        assert worklog.worker == assignment.worker


class TestProjectIcons(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestProjectIcons, cls).setUpTestData()
        cls.worker = UserFactory()
        cls.project = ProjectFactory(
            billing_interval=BILLING_INTERVAL.PROJECT_BASED,
            start_date=(timezone.now() - timedelta(days=30)).date(),
            delivery_date=(timezone.now() - timedelta(days=1)).date(),
            billing_currency="CHF"
        )
        project_activity = ProjectActivityFactory(project=cls.project, status=ACTIVITY_STATUS.DONE)
        cls.project_activity_step = ProjectActivityStepFactory(activity=project_activity, status=ACTIVITY_STATUS.DONE)

    def test_project_is_billable_when_task_based_and_any_activity_done(self):
        project = ProjectFactory(billing_interval=BILLING_INTERVAL.TASK_BASED, billing_currency="CHF")
        ProjectActivityFactory(project=project, status=ACTIVITY_STATUS.DONE)
        ProjectActivityFactory(project=project)

        assert project.icons == [PROJECT_ICONS.BILLABLE]

    def test_project_is_billable_when_not_task_based_and_all_activities_done(self):
        project = ProjectFactory(billing_interval=BILLING_INTERVAL.PROJECT_BASED)
        ProjectActivityFactory.create_batch(size=4, project=project, status=ACTIVITY_STATUS.DONE)
        assert project.icons == [PROJECT_ICONS.BILLABLE]

    def test_project_delayed(self):
        project = ProjectFactory(
            start_date=(timezone.now() - timedelta(days=30)).date(),
            delivery_date=(timezone.now() - timedelta(days=1)).date()
        )
        assert project.activities.count() == 0
        assert project.icons == [PROJECT_ICONS.DELAYED]

    def test_project_fell_asleep(self):
        project = ProjectFactory(billing_currency="CHF")
        project_activity = ProjectActivityFactory(project=project)
        project_activity_step = ProjectActivityStepFactory(activity=project_activity)
        WorkLogFactory(
            worker=self.worker, project_activity_step=project_activity_step,
            work_date=(timezone.now() - timedelta(days=31)).date()
        )
        assert project.icons == [PROJECT_ICONS.FELL_ASLEEP]

    def test_project_delayed_and_fell_asleep(self):
        ProjectActivityFactory(project=self.project, status=ACTIVITY_STATUS.PLAN)
        WorkLogFactory(
            worker=self.worker, project_activity_step=self.project_activity_step,
            work_date=(timezone.now() - timedelta(days=31)).date()
        )
        assert self.project.icons == [PROJECT_ICONS.DELAYED, PROJECT_ICONS.FELL_ASLEEP]

    def test_project_billable_and_delayed(self):
        ProjectActivityFactory.create_batch(size=4, project=self.project, status=ACTIVITY_STATUS.DONE)
        assert self.project.icons == [PROJECT_ICONS.BILLABLE, PROJECT_ICONS.DELAYED]

    def test_project_billable_delayed_and_fell_asleep(self):
        ProjectActivityFactory.create_batch(size=4, project=self.project, status=ACTIVITY_STATUS.DONE)
        WorkLogFactory(
            worker=self.worker, project_activity_step=self.project_activity_step,
            work_date=(timezone.now() - timedelta(days=31)).date()
        )
        assert self.project.icons == [PROJECT_ICONS.BILLABLE, PROJECT_ICONS.DELAYED, PROJECT_ICONS.FELL_ASLEEP]

    def test_project_not_billable_delayed_or_fell_asleep(self):
        project = ProjectFactory(billing_currency="CHF")
        assert project.icons == []


class TestProjectActivityStepWorkingDays(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestProjectActivityStepWorkingDays, cls).setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.worker = UserFactory()
        cls.activity = ProjectActivityFactory(project=cls.project)
        cls.step = ProjectActivityStepFactory(activity=cls.activity, planned_effort=2, unit=ProjectActivityStep.HOURS)

    def test_no_real_working_days(self):
        assert self.step.real_working_days == 0

    def test_real_working_days_full_week(self):
        WorkLogFactory.create_batch(size=5, worker=self.worker, project_activity_step=self.step, tracked_time=1)
        assert self.step.real_working_days == 5

    def test_real_working_days(self):
        monday = timezone.now() + timedelta(days=-timezone.now().weekday(), weeks=1)
        wednesday = monday + timedelta(days=2)
        friday = monday + timedelta(days=4)
        WorkLogFactory(worker=self.worker, project_activity_step=self.step, work_date=monday.date())
        WorkLogFactory(worker=self.worker, project_activity_step=self.step, work_date=wednesday.date())
        WorkLogFactory(worker=self.worker, project_activity_step=self.step, work_date=friday.date())
        assert self.step.real_working_days == 3


class TestProjectActivityStepRealCost(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestProjectActivityStepRealCost, cls).setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.worker = UserFactory()
        cls.activity = ProjectActivityFactory(project=cls.project)
        cls.step_hourly = ProjectActivityStepFactory(activity=cls.activity, planned_effort=2,
                                                     unit=ProjectActivityStep.HOURS)
        cls.step_daily = ProjectActivityStepFactory(activity=cls.activity, planned_effort=2,
                                                    unit=ProjectActivityStep.DAYS)
        WorkLogFactory.create_batch(size=3, worker=cls.worker, project_activity_step=cls.step_daily, tracked_time=1)

    def test_real_cost_when_no_work_logs(self):
        assert self.step_hourly.real_cost.amount == 0

    def test_real_cost_hourly(self):
        WorkLogFactory.create_batch(size=3, worker=self.worker, project_activity_step=self.step_hourly, tracked_time=1)
        amount = (self.step_hourly.real_effort_in_hours * self.step_hourly.get_rate(self.activity.currency)).amount
        assert self.step_hourly.real_cost.amount == amount

    def test_real_cost_daily(self):
        amount = (self.step_daily.real_working_days * self.step_daily.get_rate(self.activity.currency)).amount
        assert self.step_daily.real_cost.amount == amount


class TestProjectActivityStepRate(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestProjectActivityStepRate, cls).setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.step = StepFactory(rates=None)
        cls.rate = RateFactory(currency='CHF', step=cls.step)
        cls.activity = ProjectActivityFactory(project=cls.project)
        cls.activity_step = ProjectActivityStepFactory(
            activity=cls.activity, planned_effort=2, used_step=cls.step
        )

    def test_get_rate_unknown_currency(self):
        with self.assertRaisesMessage(
                ValidationError,
                "No rates defined for GBP, please define those or use a different Currency!"
        ):
            assert self.activity_step.get_rate(currency='GBP')

    def test_get_rate_when_blank_unit(self):
        self.activity_step.unit = ''
        return self.activity_step.get_rate(currency=self.activity.currency) == 0.0

    def test_hourly_rate(self):
        self.activity_step.unit = ProjectActivityStep.HOURS
        assert self.activity_step.get_rate(currency=self.activity.currency) == self.rate.hourly_rate

    def test_daily_rate(self):
        self.activity_step.unit = ProjectActivityStep.DAYS
        assert self.activity_step.get_rate(currency=self.activity.currency) == self.rate.daily_rate


class TestProjectTotalProperties(TestProjectSetupMixin):
    def test_total_planned_budget_of_activities(self):
        total_planned_budget = sum([x.total_planned_budget for x in self.project.activities.all()])
        assert self.project.total_planned_budget_of_activities.amount == total_planned_budget.amount

    def test_total_planned_budget_of_articles(self):
        assert self.project.total_planned_budget_of_articles.amount == self.budget.amount * self.project.articles.count()

    def test_total_planned_budget(self):
        total_planned_budget_of_activities = sum([x.total_planned_budget for x in self.project.activities.all()])
        total_planned_budget_of_articles = self.budget.amount * self.project.articles.count()

        assert self.project.total_planned_budget.amount == sum(
            [total_planned_budget_of_activities.amount, total_planned_budget_of_articles]
        )

    def test_total_planned_budget_of_finished_activities(self):
        total_planned_budget = sum(
            [x.total_planned_budget for x in self.project.activities.filter(status=ACTIVITY_STATUS.DONE)]
        )
        assert self.project.total_planned_budget_of_finished_activities.amount == total_planned_budget.amount

    # TODO: add test for test_total_planned_budget_of_finished_articles when ProjectArticle.status field is available

    def test_total_planned_budget_of_finished_items(self):
        self.project.refresh_from_db()
        total_planned_budget_of_finished_activities = sum(
            [x.total_planned_budget for x in self.project.activities.filter(status=ACTIVITY_STATUS.DONE)]
        )
        # TODO: filter articles based on status=Done when ProjectArticle.status field is available
        total_planned_budget_of_articles = self.budget.amount * self.project.articles.count()

        assert self.project.total_planned_budget_of_finished_items.amount == sum(
            [total_planned_budget_of_finished_activities.amount, total_planned_budget_of_articles]
        )

    def test_total_real_cost_of_activities_without_work_logs(self):
        assert self.project.total_real_cost_of_activities.amount == 0

    def test_total_real_cost_of_activities(self):
        for step in [self.step1, self.step2, self.step3, self.step4]:
            WorkLogFactory.create_batch(size=3, worker=self.worker, project_activity_step=step, tracked_time=4)

        total_real_cost_of_activities = sum([x.total_real_cost for x in self.project.activities.all()])
        assert self.project.total_real_cost_of_activities.amount == total_real_cost_of_activities.amount
        assert self.project.total_real_cost_of_activities.amount > 0

    def test_total_real_cost_of_articles(self):
        total_real_cost_of_articles = sum([x.total_real_cost for x in self.project.articles.all()])
        assert self.project.total_real_cost_of_articles.amount == total_real_cost_of_articles.amount

    def total_real_cost(self):
        total_real_cost = sum([self.project.total_real_cost_of_activities, self.project.total_real_cost_of_articles])
        assert self.project.total_real_cost.amount == total_real_cost.amount

    def test_total_real_cost_of_finished_activities(self):
        for step in [self.step1, self.step2, self.step3, self.step4]:
            WorkLogFactory.create_batch(size=3, worker=self.worker, project_activity_step=step, tracked_time=4)

        total_real_cost_of_finished_activities = sum(
            [x.total_real_cost for x in self.project.activities.filter(status=ACTIVITY_STATUS.DONE)]
        )
        assert self.project.total_real_cost_of_finished_activities.amount == total_real_cost_of_finished_activities.amount
        assert self.project.total_real_cost_of_finished_activities.amount > 0

    # TODO: add test for total_real_cost_of_finished_articles when ProjectArticle.status field is available

    def test_total_real_cost_of_finished_items(self):
        for step in [self.step1, self.step2, self.step3, self.step4]:
            WorkLogFactory.create_batch(size=3, worker=self.worker, project_activity_step=step, tracked_time=4)

        total_real_cost_of_finished_items = sum(
            [self.project.total_real_cost_of_finished_activities, self.project.total_real_cost_of_finished_articles])
        assert self.project.total_real_cost_of_finished_items.amount == total_real_cost_of_finished_items.amount
        assert self.project.total_real_cost_of_finished_items.amount > 0

    def test_total_number_of_activities(self):
        assert self.project.activities.count() == 4

    def test_total_number_of_finished_activities(self):
        assert self.project.activities.filter(status=ACTIVITY_STATUS.DONE).count() == 2


class TestCampaignTotalProperties(TestCampaignSetupMixin):
    def test_total_planned_budget(self):
        total_planned_budget = sum([x.total_planned_budget for x in self.campaign.projects.all()])
        assert self.campaign.total_planned_budget.amount == total_planned_budget.amount
        assert self.campaign.total_planned_budget.amount > 0

    def test_total_real_cost(self):
        total_real_cost = sum([x.total_real_cost for x in self.campaign.projects.all()])
        assert self.campaign.total_real_cost.amount == total_real_cost.amount
        assert self.campaign.total_real_cost.amount > 0


class TestActivityStatusChanges(TestCase):

    def setUp(self):
        self.currency = 'CHF'
        self.effort_hours = Decimal("100.00")
        self.date1 = date.today() - timedelta(days=30)
        self.date2 = date.today() - timedelta(days=20)
        self.date3 = date.today() - timedelta(days=10)
        self.discount = Money(Decimal("0.00"), self.currency)
        self.project = ProjectFactory(billing_currency=self.currency)
        self.project_activity = ProjectActivityFactory(project=self.project)
        self.project_activity3 = ProjectActivityFactory(project=self.project)
        self.offer1 = OfferFactory(project=self.project, offer_date=self.date1)
        self.offer2 = OfferFactory(project=self.project, offer_date=self.date2)
        self.offer3 = OfferFactory(project=self.project, offer_date=self.date3)
        self.activity_step1 = ProjectActivityStepFactory(activity=self.project_activity)
        self.activity_step2 = ProjectActivityStepFactory(activity=self.project_activity)
        self.activity_step3 = ProjectActivityStepFactory(activity=self.project_activity3)
        self.activity_offered1 = ActivityOfferedFactory(
            project_activity=self.project_activity,
            offer=self.offer1
        )
        self.activity_offered2 = ActivityOfferedFactory(
            project_activity=self.project_activity,
            offer=self.offer2
        )
        self.activity_offered3 = ActivityOfferedFactory(
            project_activity=self.project_activity, offer=self.offer3
        )

    def test_activity_change_status_to_(self):
        local_project_activity = ProjectActivityFactory(project=self.project)
        assert local_project_activity.offer_status == ActivityArticleOfferedStatus.CREATED
        local_project_activity.change_status_to_sent()
        assert local_project_activity.offer_status == ActivityArticleOfferedStatus.SENT

        local_project_activity.change_status_to_declined()
        assert local_project_activity.offer_status == ActivityArticleOfferedStatus.DECLINED

        local_project_activity.change_status_to_sent()

        assert local_project_activity.offer_status == ActivityArticleOfferedStatus.DECLINED

        local_project_activity2 = ProjectActivityFactory(project=self.project)
        local_project_activity2.change_status_to_sent()
        local_project_activity2.change_status_to_declined()
        assert local_project_activity2.offer_status == ActivityArticleOfferedStatus.DECLINED

        local_project_activity3 = ProjectActivityFactory(project=self.project)
        local_project_activity3.change_status_to_sent()
        assert local_project_activity3.offer_status == ActivityArticleOfferedStatus.SENT
        local_project_activity3.change_status_to_replaced()

        assert local_project_activity3.offer_status == ActivityArticleOfferedStatus.REPLACED
        replacer = local_project_activity3.replaced_by
        assert local_project_activity3.display_name == replacer.display_name

        assert replacer.offer_status == ActivityArticleOfferedStatus.IMPROVABLE

        replacer.change_status_to_improved()
        assert replacer.offer_status == ActivityArticleOfferedStatus.IMPROVED

        assert local_project_activity3.version == 1
        assert replacer.version == local_project_activity3.version + 1

    def test_sent_in_project_offers(self):
        self.offer1.offer_status = OfferStatus.SENT
        self.offer2.offer_status = OfferStatus.SENT
        self.offer1.save()
        self.offer2.save()
        # response is a list
        assert len(self.project_activity.sent_in_project_offers) == 2  # [self.offer1, self.offer2]
        assert self.offer1 in self.project_activity.sent_in_project_offers

    def test_start_stop_activity(self):
        self.project_activity.start_activity()
        assert self.project_activity.status == ACTIVITY_STATUS.WORK
        self.activity_step1.refresh_from_db()
        self.activity_step2.refresh_from_db()
        self.activity_step3.refresh_from_db()
        assert self.activity_step1.status == ACTIVITY_STATUS.WORK
        assert self.activity_step2.status == ACTIVITY_STATUS.WORK
        assert self.activity_step3.status == ACTIVITY_STATUS.PLAN

        self.project_activity.stop_activity()
        self.activity_step1.refresh_from_db()
        self.activity_step2.refresh_from_db()
        self.activity_step3.refresh_from_db()
        assert self.project_activity.status == ACTIVITY_STATUS.CANCEL
        assert self.activity_step1.status == ACTIVITY_STATUS.CANCEL
        assert self.activity_step2.status == ACTIVITY_STATUS.CANCEL
        assert self.activity_step3.status == ACTIVITY_STATUS.PLAN


class TestArticleStatusChanges(TestCase):

    def setUp(self):
        self.currency = 'CHF'
        self.effort_hours = Decimal("100.00")
        self.date1 = date.today() - timedelta(days=30)
        self.date2 = date.today() - timedelta(days=20)
        self.date3 = date.today() - timedelta(days=10)
        self.discount = Money(Decimal("0.00"), self.currency)
        self.project = ProjectFactory(billing_currency="CHF")
        self.project_article = ProjectArticleFactory(project=self.project)
        self.offer1 = OfferFactory(project=self.project, offer_date=self.date1)
        self.offer2 = OfferFactory(project=self.project, offer_date=self.date2)
        self.offer3 = OfferFactory(project=self.project, offer_date=self.date3)

        self.article_offered1 = ArticleOfferedFactory(
            project_article=self.project_article,
            offer=self.offer1
        )
        self.article_offered2 = ArticleOfferedFactory(
            project_article=self.project_article,
            offer=self.offer2
        )
        self.article_offered3 = ArticleOfferedFactory(
            project_article=self.project_article,
            offer=self.offer3
        )

    def test_article_change_status_to_(self):
        local_project_article = ProjectArticleFactory(project=self.project)
        assert local_project_article.offer_status == ActivityArticleOfferedStatus.CREATED
        local_project_article.change_status_to_sent()
        assert local_project_article.offer_status == ActivityArticleOfferedStatus.SENT

        local_project_article.change_status_to_declined()
        assert local_project_article.offer_status == ActivityArticleOfferedStatus.DECLINED

        local_project_article.change_status_to_sent()
        assert local_project_article.offer_status == ActivityArticleOfferedStatus.DECLINED

        local_project_article2 = ProjectArticleFactory(project=self.project)
        local_project_article2.change_status_to_sent()
        local_project_article2.change_status_to_declined()
        assert local_project_article2.offer_status == ActivityArticleOfferedStatus.DECLINED

        local_project_article3 = ProjectArticleFactory(project=self.project)
        local_project_article3.change_status_to_sent()
        local_project_article3.change_status_to_replaced()

        assert local_project_article3.offer_status == ActivityArticleOfferedStatus.REPLACED
        replacer = local_project_article3.replaced_by
        assert local_project_article3.display_name == replacer.display_name

        assert replacer.offer_status == ActivityArticleOfferedStatus.IMPROVABLE

        replacer.change_status_to_improved()
        assert replacer.offer_status == ActivityArticleOfferedStatus.IMPROVED

        assert local_project_article3.version == 1
        assert replacer.version == local_project_article3.version + 1

    def test_sent_in_project_offers(self):
        self.offer1.offer_status = OfferStatus.SENT
        self.offer2.offer_status = OfferStatus.SENT
        self.offer1.save()
        self.offer2.save()
        # response is a list
        assert len(self.project_article.sent_in_project_offers) == 2  # [self.offer1, self.offer2]
        assert self.offer1 in self.project_article.sent_in_project_offers


class TestProjectActivityUseCount(TestCase):
    def setUp(self):
        super().setUp()
        self.use_count = 4
        self.activity_group = ActivityGroupFactory()
        self.template_activity = TemplateActivityFactory(activity_group=self.activity_group)
        self.project1 = ProjectFactory(billing_currency="CHF")
        self.used_project_activity = ProjectActivityFactory(project=self.project1)
        self.project_activities = ProjectActivityFactory.create_batch(
            size=self.use_count, project=self.project1, used_project_activity=self.used_project_activity
        )

    def test_used_project_activity_count(self):
        assert self.used_project_activity.use_count == self.use_count


class TestAllActivityStepsTheSameActivityChangeStatus(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.activity = ProjectActivityFactory(project=cls.project)
        cls.step1 = ProjectActivityStepFactory(activity=cls.activity)
        cls.step2 = ProjectActivityStepFactory(activity=cls.activity)
        cls.step3 = ProjectActivityStepFactory(activity=cls.activity)

    def test_all_steps_and_activity_plan(self):
        self.step1.refresh_from_db()
        self.step2.refresh_from_db()
        self.step3.refresh_from_db()
        self.activity.refresh_from_db()
        assert self.step1.status == ACTIVITY_STATUS.PLAN
        assert self.step2.status == ACTIVITY_STATUS.PLAN
        assert self.step3.status == ACTIVITY_STATUS.PLAN
        assert self.activity.status == ACTIVITY_STATUS.PLAN

    def test_some_steps_are_work_activity_plan(self):
        self.step1.status = ACTIVITY_STATUS.WORK
        self.step2.status = ACTIVITY_STATUS.WORK
        self.step1.save()
        self.step2.save()
        self.step1.refresh_from_db()
        self.step2.refresh_from_db()
        self.step3.refresh_from_db()
        self.activity.refresh_from_db()
        assert self.step1.status == ACTIVITY_STATUS.WORK
        assert self.step2.status == ACTIVITY_STATUS.WORK
        assert self.step3.status == ACTIVITY_STATUS.PLAN
        assert self.activity.status == ACTIVITY_STATUS.PLAN

    def test_all_steps_and_activity_work(self):
        self.step1.status = ACTIVITY_STATUS.WORK
        self.step2.status = ACTIVITY_STATUS.WORK
        self.step3.status = ACTIVITY_STATUS.WORK
        self.step1.save()
        self.step2.save()
        self.step3.save()
        self.step1.refresh_from_db()
        self.step2.refresh_from_db()
        self.step3.refresh_from_db()
        self.activity.refresh_from_db()
        assert self.step1.status == ACTIVITY_STATUS.WORK
        assert self.step2.status == ACTIVITY_STATUS.WORK
        assert self.step3.status == ACTIVITY_STATUS.WORK

        # this should automaticaly be changed to WORK
        assert self.activity.status == ACTIVITY_STATUS.WORK

    def test_some_steps_are_done_activity_work(self):
        self.step1.status = ACTIVITY_STATUS.DONE
        self.step2.status = ACTIVITY_STATUS.DONE
        self.activity.status = ACTIVITY_STATUS.WORK
        self.activity.save()
        self.step1.save()
        self.step2.save()
        self.step1.refresh_from_db()
        self.step2.refresh_from_db()
        self.step3.refresh_from_db()
        self.activity.refresh_from_db()
        assert self.step1.status == ACTIVITY_STATUS.DONE
        assert self.step2.status == ACTIVITY_STATUS.DONE
        assert self.step3.status == ACTIVITY_STATUS.PLAN
        assert self.activity.status == ACTIVITY_STATUS.WORK

    def test_all_steps_and_activity_done(self):
        self.step1.status = ACTIVITY_STATUS.DONE
        self.step2.status = ACTIVITY_STATUS.DONE
        self.step3.status = ACTIVITY_STATUS.DONE
        self.step1.save()
        self.step2.save()
        self.step3.save()
        self.step1.refresh_from_db()
        self.step2.refresh_from_db()
        self.step3.refresh_from_db()
        self.activity.refresh_from_db()
        assert self.step1.status == ACTIVITY_STATUS.DONE
        assert self.step2.status == ACTIVITY_STATUS.DONE
        assert self.step3.status == ACTIVITY_STATUS.DONE

        # this should automaticaly be changed to WORK
        assert self.activity.status == ACTIVITY_STATUS.DONE

    def test_some_steps_are_on_hold_activity_work(self):
        self.step1.status = ACTIVITY_STATUS.DONE
        self.step2.status = ACTIVITY_STATUS.DONE
        self.activity.status = ACTIVITY_STATUS.WORK
        self.step1.save()
        self.step2.save()
        self.activity.save()
        self.step1.refresh_from_db()
        self.step2.refresh_from_db()
        self.step3.refresh_from_db()
        self.activity.refresh_from_db()
        assert self.step1.status == ACTIVITY_STATUS.DONE
        assert self.step2.status == ACTIVITY_STATUS.DONE
        assert self.step3.status == ACTIVITY_STATUS.PLAN
        assert self.activity.status == ACTIVITY_STATUS.WORK

    def test_all_steps_and_activity_on_hold(self):
        self.step1.status = ACTIVITY_STATUS.ON_HOLD
        self.step2.status = ACTIVITY_STATUS.ON_HOLD
        self.step3.status = ACTIVITY_STATUS.ON_HOLD
        self.step1.save()
        self.step2.save()
        self.step3.save()
        self.step1.refresh_from_db()
        self.step2.refresh_from_db()
        self.step3.refresh_from_db()
        self.activity.refresh_from_db()
        assert self.step1.status == ACTIVITY_STATUS.ON_HOLD
        assert self.step2.status == ACTIVITY_STATUS.ON_HOLD
        assert self.step3.status == ACTIVITY_STATUS.ON_HOLD

        # this should automaticaly be changed to WORK
        assert self.activity.status == ACTIVITY_STATUS.ON_HOLD


class TestProjectArticleExpense(TestCase):
    def setUp(self):
        super().setUp()
        self.company = CompanyWithOfficeFactory()
        self.project = ProjectFactory(company=self.company)
        self.article = ProjectArticleFactory(project=self.project)
        self.expense = ProjectArticleExpenseFactory(article=self.article)
        self.format = '%Y-%m-%d'

    def test_cost_in_default_currency_mock_backend(self):
        money_rates_settings.DEFAULT_BACKEND = TestRateBackend
        call_command('update_rates')
        self.expense.cost_in_receipt_currency = Money(Decimal("500"), currency=CURRENCY.EUR)
        self.expense.receipt_currency = CURRENCY.EUR

        cost = convert_money(
            self.expense.cost_in_receipt_currency.amount, self.expense.receipt_currency, self.article.currency
        )
        assert self.expense.cost_in_default_currency.amount == cost.amount
        assert self.expense.cost_in_default_currency.currency == cost.currency
        assert self.expense.cost_in_receipt_currency.currency != self.article.currency


class TestMoveProjectActivityStatusChanges(TestCase):

    def setUp(self):
        self.project = ProjectFactory(billing_currency="CHF")
        self.project_activity = ProjectActivityFactory(project=self.project)
        self.activity_step1 = ProjectActivityStepFactory(activity=self.project_activity)
        self.activity_step2 = ProjectActivityStepFactory(activity=self.project_activity)
        self.worklog1 = WorkLogFactory(project_activity_step=self.activity_step1)
        self.assignment1 = ActivityAssignmentFactory(project_activity_step=self.activity_step1)
        self.worklog2 = WorkLogFactory(project_activity_step=self.activity_step2)
        self.assignment2 = ActivityAssignmentFactory(project_activity_step=self.activity_step2)

    def test_activity_project_replace(self):
        self.project_activity.change_status_to_sent()
        self.project_activity.change_status_to_replaced()
        self.worklog1.refresh_from_db()
        self.assignment1.refresh_from_db()
        self.worklog2.refresh_from_db()
        self.assignment2.refresh_from_db()
        self.activity_step1.refresh_from_db()
        self.activity_step2.refresh_from_db()
        assert self.project_activity.offer_status == 'replaced'
        assert self.project_activity.replaced_by.offer_status == 'improvable'
        assert self.activity_step1.status == ACTIVITY_STATUS.CANCEL
        assert self.activity_step1.replaced_by.status == ACTIVITY_STATUS.PLAN
        assert self.activity_step2.status == ACTIVITY_STATUS.CANCEL
        assert self.activity_step2.replaced_by.status == ACTIVITY_STATUS.PLAN
        assert self.worklog1.project_activity_step.pk == self.activity_step1.replaced_by.pk
        assert self.assignment1.project_activity_step.pk == self.activity_step1.replaced_by.pk
        assert self.worklog2.project_activity_step.pk == self.activity_step2.replaced_by.pk
        assert self.assignment2.project_activity_step.pk == self.activity_step2.replaced_by.pk


class TestCloneProjects(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.start_date = date.today() - timedelta(days=20)
        cls.finishing_date = date.today() - timedelta(days=10)
        cls.delivery_date = date.today() - timedelta(days=5)
        cls.project_owner = UserFactory(first_name="Steve")
        cls.new_project_owner = UserFactory(first_name="Kevin")
        cls.original_project = ProjectFactory(
            title="this is the Original Project",
            billing_currency="CHF",
            start_date=cls.start_date,
            finishing_date=cls.finishing_date,
            delivery_date=cls.delivery_date,
            project_owner=cls.project_owner
        )
        cls.project_activity = ProjectActivityFactory(
            project=cls.original_project,
            start_date=cls.start_date,
            end_date=cls.finishing_date,
            version=2
        )
        cls.project_article = ProjectArticleFactory(project=cls.original_project)
        cls.project_activity_step = ProjectActivityStepFactory(
            activity=cls.project_activity,
            planned_effort=4,
            unit=ProjectActivityStep.HOURS,
            start_date=cls.start_date,
            end_date=cls.finishing_date
        )

    def test_simple_clone_project_method(self):
        new_project = self.original_project.get_cloned_project(self.new_project_owner)

        self.assertEqual(new_project.title, f'{self.original_project.title} (Copy)')
        self.assertNotEqual(new_project.start_date, self.original_project.start_date)
        self.assertNotEqual(new_project.finishing_date, self.original_project.finishing_date)
        self.assertNotEqual(new_project.delivery_date, self.original_project.delivery_date)
        self.assertNotEqual(new_project.project_owner, self.original_project.project_owner)

        self.assertEqual(new_project.start_date, date.today())
        self.assertEqual(new_project.finishing_date, date.today() + (self.finishing_date - self.start_date))
        self.assertEqual(new_project.delivery_date, date.today() + (self.delivery_date - self.start_date))
        self.assertEqual(new_project.project_owner, self.new_project_owner)

        self.assertEqual(new_project.activities.count(), self.original_project.activities.count())
        self.assertEqual(new_project.articles.count(), self.original_project.articles.count())
        self.assertEqual(new_project.activities.count(), 1)
        self.assertEqual(new_project.articles.count(), 1)

    def test_project_activity_duplicate_under_new_project(self):
        self.project_activity.change_status_to_sent()
        self.project_activity.refresh_from_db()
        new_project = self.original_project.get_cloned_project(self.new_project_owner)

        self.assertEqual(new_project.activities.count(), self.original_project.activities.count())
        new_project_activity = new_project.activities.first()

        self.assertNotEqual(new_project_activity.offer_status, self.project_activity.offer_status)
        self.assertEqual(new_project_activity.offer_status, ActivityArticleOfferedStatus.CREATED)
        self.assertEqual(new_project_activity.project, new_project)
        self.assertEqual(new_project_activity.start_date, date.today())
        self.assertEqual(new_project_activity.end_date, date.today() + (self.finishing_date - self.start_date))
        self.assertEqual(self.project_activity.end_date, self.finishing_date)
        self.assertEqual(new_project_activity.version, 1)
        self.assertEqual(self.project_activity.version, 2)
        self.assertEqual(self.project_activity.activity_steps.count(), new_project_activity.activity_steps.count())

        new_activity_step = new_project_activity.activity_steps.first()
        self.assertNotEqual(new_activity_step.start_date, self.project_activity_step.start_date)
        self.assertEqual(new_activity_step.start_date, date.today())
        self.assertNotEqual(new_project_activity.end_date, self.project_activity_step.end_date)
        self.assertEqual(new_project_activity.end_date, date.today() + (self.finishing_date - self.start_date))

    def test_project_article_duplicate_under_new_project(self):
        self.project_article.change_status_to_sent()
        self.project_article.refresh_from_db()
        new_project = self.original_project.get_cloned_project(self.new_project_owner)
        new_article = new_project.articles.first()

        self.assertEqual(new_article.offer_status, ActivityArticleOfferedStatus.CREATED)


class TestModelProjectSoftDelete(TestCase):

    def test_delete_project(self):
        project1 = ProjectFactory()
        project2 = ProjectFactory()
        self.assertEqual(Project.objects.count(), 2)
        project2.delete()
        self.assertEqual(Project.objects.count(), 1)
        self.assertEqual(Project.all_objects.count(), 2)

    def test_delete_project_public_id(self):
        project1 = ProjectFactory()
        project2 = ProjectFactory()
        self.assertEqual(Project.objects.count(), 2)
        project2.delete()
        project3 = ProjectFactory()
        final_id = int(project2.public_id[-1])
        self.assertEqual(final_id + 1, int(project3.public_id[-1]))
        project2.save()
        self.assertEqual(Project.objects.count(), 3)


class TestModelCampaignSoftDelete(TestCase):

    def test_delete_campaign(self):
        campain1 = CampaignFactory()
        campain2 = CampaignFactory()
        self.assertEqual(Campaign.objects.count(), 2)
        campain2.delete()
        self.assertEqual(Campaign.objects.count(), 1)
        self.assertEqual(Campaign.all_objects.count(), 2)

    def test_delete_campaign_public_id(self):
        campaign1 = CampaignFactory()
        campaign2 = CampaignFactory()
        self.assertEqual(Campaign.objects.count(), 2)
        campaign2.delete()
        campaign3 = CampaignFactory()
        final_id = int(campaign2.public_id[-1])
        self.assertEqual(final_id + 1, int(campaign3.public_id[-1]))
        campaign2.save()
        self.assertEqual(Campaign.objects.count(), 3)


class TestProjectActivityModelSoftDelete(TestCase):

    def test_project_activity_delete(self):
        project = ProjectFactory()
        pa1 = ProjectActivityFactory(project=project)
        pa2 = ProjectActivityFactory(project=project)
        self.assertEqual(ProjectActivity.objects.count(), 2)
        pa2.delete()
        self.assertEqual(ProjectActivity.objects.count(), 1)
        self.assertEqual(ProjectActivity.all_objects.count(), 2)

    def test_project_activity_project_cascade_deletion(self):
        project = ProjectFactory()
        pa1 = ProjectActivityFactory(project=project)
        pa2 = ProjectActivityFactory(project=project)
        self.assertEqual(ProjectActivity.objects.count(), 2)
        project.delete()
        self.assertEqual(ProjectActivity.objects.count(), 0)
        self.assertEqual(ProjectActivity.all_objects.count(), 2)
        project.save()
        self.assertEqual(ProjectActivity.objects.count(), 0)

    def test_project_activity_position_change(self):
        project = ProjectFactory()
        pa1 = ProjectActivityFactory(project=project)
        pa2 = ProjectActivityFactory(project=project)
        self.assertEqual(ProjectActivity.objects.count(), 2)
        pa2.delete()
        self.assertEqual(ProjectActivity.objects.count(), 1)
        self.assertEqual(ProjectActivity.all_objects.count(), 2)
        pa3 = ProjectActivityFactory(project=project)
        position = pa2.position
        self.assertEqual(position + 1, pa3.position)
        pa2.save()
        pa2.refresh_from_db()
        self.assertEqual(position, pa2.position)


class TestProjectActivityStepModelSoftDelete(TestCase):

    def test_deletion(self):
        project = ProjectFactory()
        pa1 = ProjectActivityFactory(project=project)
        pas1 = ProjectActivityStepFactory(activity=pa1)
        pas2 = ProjectActivityStepFactory(activity=pa1)
        self.assertEqual(ProjectActivityStep.objects.count(), 2)
        pas2.delete()
        self.assertEqual(ProjectActivityStep.objects.count(), 1)
        self.assertEqual(ProjectActivityStep.all_objects.count(), 2)

    def test_project_activity_step_cascade_deletion(self):
        project = ProjectFactory()
        pa1 = ProjectActivityFactory(project=project)
        pas1 = ProjectActivityStepFactory(activity=pa1)
        pas2 = ProjectActivityStepFactory(activity=pa1)
        self.assertEqual(ProjectActivityStep.objects.count(), 2)
        pa1.delete()
        self.assertEqual(ProjectActivityStep.objects.count(), 0)
        self.assertEqual(ProjectActivityStep.all_objects.count(), 2)

    def test_position_change(self):
        project = ProjectFactory()
        pa1 = ProjectActivityFactory(project=project)
        pas1 = ProjectActivityStepFactory(activity=pa1)
        pas2 = ProjectActivityStepFactory(activity=pa1)
        pas2.delete()
        position = pas2.position
        pas3 = ProjectActivityStepFactory(activity=pa1)
        self.assertEqual(
            pas3.position,
            position + 1
        )


class TestProjectArticleModelSoftDelete(TestCase):
    def test_project_activity_delete(self):
        project = ProjectFactory()
        pa1 = ProjectArticleFactory(project=project)
        pa2 = ProjectArticleFactory(project=project)
        self.assertEqual(ProjectArticle.objects.count(), 2)
        pa2.delete()
        self.assertEqual(ProjectArticle.objects.count(), 1)
        self.assertEqual(ProjectArticle.all_objects.count(), 2)

    def test_project_activity_project_cascade_deletion(self):
        project = ProjectFactory()
        pa1 = ProjectArticleFactory(project=project)
        pa2 = ProjectArticleFactory(project=project)
        self.assertEqual(ProjectArticle.objects.count(), 2)
        project.delete()
        self.assertEqual(ProjectArticle.objects.count(), 0)
        self.assertEqual(ProjectArticle.all_objects.count(), 2)
        project.save()
        self.assertEqual(ProjectArticle.objects.count(), 0)

    def test_project_activity_position_change(self):
        project = ProjectFactory()
        pa1 = ProjectArticleFactory(project=project)
        pa2 = ProjectArticleFactory(project=project)
        self.assertEqual(ProjectArticle.objects.count(), 2)
        pa2.delete()
        self.assertEqual(ProjectArticle.objects.count(), 1)
        self.assertEqual(ProjectArticle.all_objects.count(), 2)
        pa3 = ProjectArticleFactory(project=project)
        position = pa2.position
        self.assertEqual(position + 1, pa3.position)
        pa2.save()
        pa2.refresh_from_db()
        self.assertEqual(position, pa2.position)

    def test_mixed_deletion(self):
        project = ProjectFactory()
        pa1 = ProjectArticleFactory(project=project)
        pa2 = ProjectArticleFactory(project=project)
        self.assertEqual(ProjectArticle.objects.count(), 2)
        pa3 = ProjectActivityFactory(project=project)
        pa4 = ProjectActivityFactory(project=project)
        self.assertEqual(ProjectActivity.objects.count(), 2)
        pa2.delete()
        pa3.delete()
        pa5 = ProjectArticleFactory(project=project)
        position = pa3.position
        self.assertEqual(position + 2, pa5.position)
        pa3.save()
        pa3.refresh_from_db()
        self.assertEqual(position, pa3.position)


class TestWorkLogModelSoftDelete(TestCase):

    def test_deletion(self):
        project = ProjectFactory()
        activity = ProjectActivityFactory(project=project)
        pas1 = ProjectActivityStepFactory(activity=activity)
        ws = WorkLogFactory(project_activity_step=pas1)
        ws = WorkLogFactory(project_activity_step=pas1)
        self.assertEqual(WorkLog.objects.count(), 2)
        ws.delete()
        self.assertEqual(WorkLog.objects.count(), 1)
        self.assertEqual(WorkLog.all_objects.count(), 2)
        ws = WorkLogFactory(project_activity_step=pas1)
        self.assertEqual(WorkLog.objects.count(), 2)
        self.assertEqual(WorkLog.all_objects.count(), 3)

    def test_cascade_deletion(self):
        project = ProjectFactory()
        activity = ProjectActivityFactory(project=project)
        pas1 = ProjectActivityStepFactory(activity=activity)
        ws = WorkLogFactory(project_activity_step=pas1)
        ws = WorkLogFactory(project_activity_step=pas1)
        self.assertEqual(WorkLog.objects.count(), 2)
        activity.delete()
        self.assertEqual(WorkLog.objects.count(), 0)
        self.assertEqual(WorkLog.all_objects.count(), 2)
