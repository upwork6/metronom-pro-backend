""" This module exists for testing activity step API handling
"""

import copy
import datetime
from copy import deepcopy
import pytest
from django.contrib.auth.models import Group
from django.urls import reverse
from djmoney.money import Money

from apps.activities.tests.factories import (ActivityGroupFactory, RateFactory,
                                             TemplateActivityFactory,
                                             TemplateActivityStepFactory)
from apps.metronom_commons.data import CURRENCY
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.models import ProjectActivityStep, Project
from apps.projects.tests.factories import (ActivityAssignmentFactory,
                                           ProjectActivityFactory,
                                           ProjectActivityStepFactory,
                                           ProjectFactory, WorkLogFactory)
from apps.users.tests.factories import UserFactory
from rest_framework import status
from apps.metronom_commons.models import ROLE, ACTIVITY_STATUS
from apps.metronom_commons.test import APIRestAuthJWTClient


class TestProjectActivityStepList(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project1 = ProjectFactory(billing_currency=CURRENCY.CHF)
        cls.project_activity1 = ProjectActivityFactory(project=cls.project1)
        cls.project_activity_step1 = ProjectActivityStepFactory(
            activity=cls.project_activity1, planned_effort=2, unit="daily")
        cls.endpoint = reverse('api_projects:activity-list', kwargs={'project_pk': cls.project1.id})

        cls.response = cls.admin_client.get(cls.endpoint)

    def test_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_step_data(self):
        ids_check = [x['uuid'] for x in self.response.data[0].get("activity_steps")]
        self.assertIn(str(self.project_activity_step1.id), ids_check)
        self.assertIn('dependency_position', self.response.data[0].get("activity_steps")[0].keys())

    def test_unit_status_is_present(self):
        self.assertTrue('unit' in self.response.data[0].get("activity_steps")[0].keys())
        self.assertTrue('status' in self.response.data[0].get("activity_steps")[0].keys())

    @pytest.mark.skip(reason="handling for None needed")  # TODO #386
    def test_planned_budget(self):
        planned_budgets = [x['planned_budget'] for x in self.response.data[0].get("activity_steps")]
        self.assertIn(0, planned_budgets)
        self.project_activity_step1.used_step.rates.all().delete()
        rate = RateFactory(step=self.project_activity_step1.used_step, currency='CHF')
        response = self.admin_client.get(self.endpoint)
        planned_budgets = [x['planned_budget'] for x in response.data[0].get("activity_steps")]
        self.assertIn(rate.daily_rate.amount * 2, planned_budgets)
        self.project_activity_step1.unit = "hourly"
        self.project_activity_step1.save()
        response = self.admin_client.get(self.endpoint)
        planned_budgets = [x['planned_budget'] for x in response.data[0].get("activity_steps")]
        self.assertIn(rate.hourly_rate.amount * 2, planned_budgets)

    @pytest.mark.skip(reason="No info from how this supposed to work with days unit yet")
    def test_real_cost(self):
        real_costs = [x['real_cost'] for x in self.response.data[0].get("activity_steps")]
        self.assertIn(0, real_costs)

    def test_real_effort(self):
        real_effort_in_hours = [x['real_effort_in_hours'] for x in self.response.data[0].get("activity_steps")]
        self.assertIn(0, real_effort_in_hours)
        worker = UserFactory()
        ActivityAssignmentFactory(worker=worker, project_activity_step=self.project_activity_step1)
        WorkLogFactory(worker=worker, project_activity_step=self.project_activity_step1, tracked_time=1)
        response = self.admin_client.get(self.endpoint)
        real_effort_in_hours = [x['real_effort_in_hours'] for x in response.data[0].get("activity_steps")]
        self.assertIn(1, real_effort_in_hours)


class TestActivityAssignmentList(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project1 = ProjectFactory(billing_currency=CURRENCY.CHF)
        cls.project_activity1 = ProjectActivityFactory(project=cls.project1)
        cls.project_activity_step1 = ProjectActivityStepFactory(activity=cls.project_activity1)
        cls.assignment1 = ActivityAssignmentFactory(project_activity_step=cls.project_activity_step1)
        cls.assignment2 = ActivityAssignmentFactory(project_activity_step=cls.project_activity_step1)
        cls.endpoint = reverse('api_projects:activity-list', kwargs={'project_pk': cls.project1.id})
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_assignments_data(self):
        assignments = [x["assignments"] for x in self.response.data[0].get("activity_steps")]
        ids_check = [x[0]['uuid'] for x in assignments]
        self.assertIn(str(self.assignment1.id), ids_check)
        self.assertIn(str(self.assignment1.id), ids_check)

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        self.login_as_user()
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)

        self.login_as_accounting_user()
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)

        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)

        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)

        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class BaseAloneCreationClass(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project1 = ProjectFactory(billing_currency=CURRENCY.CHF, )
        cls.project_activity1 = ProjectActivityFactory(project=cls.project1)
        cls.project_activity_step1 = ProjectActivityStepFactory(activity=cls.project_activity1)
        cls.endpoint = reverse('api_projects:activity-detail',
                               kwargs={'project_pk': cls.project1.id, 'pk': cls.project_activity1.id})
        cls.template_activity_step = TemplateActivityStepFactory()
        cls.step = cls.template_activity_step.step


class BaseAssigmentUpdate(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project1 = ProjectFactory(billing_currency=CURRENCY.CHF)
        cls.project_activity1 = ProjectActivityFactory(project=cls.project1)
        cls.project_activity_step1 = ProjectActivityStepFactory(activity=cls.project_activity1)
        cls.assignment1 = ActivityAssignmentFactory(project_activity_step=cls.project_activity_step1)
        cls.assignment2 = ActivityAssignmentFactory(project_activity_step=cls.project_activity_step1)
        cls.endpoint = reverse('api_projects:activity-detail',
                               kwargs={'project_pk': cls.project1.id, 'pk': cls.project_activity1.id})
        cls.template_activity_step = TemplateActivityStepFactory()
        cls.step = cls.template_activity_step.step


class BaseCreateWithActivity(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project1 = ProjectFactory(billing_currency=CURRENCY.CHF)
        cls.project_activity1 = ProjectActivityFactory(project=cls.project1)
        cls.project_activity_step1 = ProjectActivityStepFactory(activity=cls.project_activity1)
        cls.assignment1 = ActivityAssignmentFactory(project_activity_step=cls.project_activity_step1)
        cls.assignment2 = ActivityAssignmentFactory(project_activity_step=cls.project_activity_step1)
        cls.endpoint = reverse('api_projects:activity-list', kwargs={'project_pk': cls.project1.id})
        cls.template_activity_step = TemplateActivityStepFactory()
        cls.step = cls.template_activity_step.step
        cls.start_date = datetime.date.today() + datetime.timedelta(weeks=10)
        cls.end_date = datetime.date.today() + datetime.timedelta(weeks=12)
        cls.activity_group = ActivityGroupFactory()
        cls.template_activity = TemplateActivityFactory(activity_group=cls.activity_group)


class TestProjectActivityStepCreateAlone(BaseAloneCreationClass):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project1 = ProjectFactory(billing_currency=CURRENCY.CHF)
        cls.project_activity1 = ProjectActivityFactory(project=cls.project1)
        cls.project_activity_step1 = ProjectActivityStepFactory(activity=cls.project_activity1)
        cls.endpoint = reverse('api_projects:activity-detail',
                               kwargs={'project_pk': cls.project1.id, 'pk': cls.project_activity1.id})

    def setUp(self):
        super().setUp()
        self.patch_data = {
            "activity_steps": [{
                'used_template_activity_step': {'uuid': self.template_activity_step.pk},
                'planned_effort': 5.0,
                'used_step': {'uuid': self.step.pk},
                'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
                'unit': 'hourly',
                'position': ProjectActivityStep.objects.order_by("-position")[0].position + 1,
            }]
        }
        self.response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(self.response.status_code, 200)
        self.last_step = ProjectActivityStep.objects.filter(
            position=self.patch_data["activity_steps"][0]['position'],
            used_template_activity_step=self.template_activity_step,
            planned_effort=self.patch_data["activity_steps"][0]['planned_effort'],
            unit='hourly'
        ).exclude(id=self.project_activity_step1.id).last()

    def test_response(self):
        self.assertTrue("position" in self.response.data["activity_steps"][0])

    def test_created_object_is_correct(self):
        data_to_check = self.patch_data["activity_steps"][0]
        self.last_step.refresh_from_db()
        for key in data_to_check:
            data = getattr(self.last_step, key)
            if hasattr(data, "id"):
                data = data.id
            elif hasattr(data, "strftime"):
                data = data.strftime('%Y-%m-%d')
            if isinstance(data_to_check[key], dict) and 'uuid' in data_to_check[key]:
                continue
            else:
                self.assertEqual(data, data_to_check[key], key)

    def test_project_activity_step_check_end_date_less_than_start_date(self):
        patch_data = copy.deepcopy(self.patch_data)
        copied_change_data = copy.deepcopy(patch_data["activity_steps"][0])
        copied_change_data['end_date'] = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')
        patch_data["activity_steps"][0] = copied_change_data
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_project_activity_step_create_with_dependency(self):
        depends_on = ProjectActivityStepFactory(activity=self.project_activity1, position=6)
        patch_data = copy.deepcopy(self.patch_data)
        patch_data['activity_steps'][0]['dependency_position'] = depends_on.position
        patch_data['activity_steps'][0]['position'] = ProjectActivityStep.objects.order_by("-position")[0].position + 1
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200, response.data)
        response = self.admin_client.get(self.endpoint)
        position_check = [x['dependency_position']
                          for x in response.data.get("activity_steps") if x.get('dependency_position') is not None]
        self.assertIn(depends_on.position, position_check)

    def test_wrong_dependency(self):
        patch_data = copy.deepcopy(self.patch_data)
        patch_data['activity_steps'][0]['dependency_position'] = 12345
        patch_data['activity_steps'][0]['position'] = 4
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {"__all__": ['There is no activity step with such position to depends on']}
        )

    def test_no_possibility_to_depends_on_themself(self):
        patch_data = copy.deepcopy(self.patch_data)
        patch_data['activity_steps'][0]['dependency_position'] = 5
        patch_data['activity_steps'][0]['position'] = 5
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {"__all__": ["Activity step can't depends on itself"]}
        )

    def test_simultaneous_creation_with_position_depending(self):
        project1 = ProjectFactory(billing_currency=CURRENCY.CHF)
        project_activity1 = ProjectActivityFactory(project=project1)
        project_activity_step1 = ProjectActivityStepFactory(activity=project_activity1)
        endpoint = reverse('api_projects:activity-detail',
                           kwargs={'project_pk': project1.id, 'pk': project_activity1.id})
        latest_pa_step = ProjectActivityStep.objects.order_by("-position").first()
        if latest_pa_step:
            latest_position = latest_pa_step.position
        else:
            latest_position = 11
        patch_data = {
            "activity_steps": [
                {
                    'used_template_activity_step': {'uuid': self.template_activity_step.pk},
                    'planned_effort': 5.0,
                    'used_step': {'uuid': self.step.pk},
                    'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                    'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
                    'unit': 'hourly',
                    'position': latest_position + 1
                },
                {
                    'used_template_activity_step': {'uuid': self.template_activity_step.pk},
                    'planned_effort': 5.0,
                    'used_step': {'uuid': self.step.pk},
                    'dependency_position': latest_position + 1,
                    'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                    'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
                    'unit': 'hourly',
                    'position': latest_position + 2
                }
            ]
        }
        response = self.admin_client.patch(endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(ProjectActivityStep.objects.filter(
            position=latest_position + 2, planned_effort=5.0).first().dependency_position, latest_position + 1)

    def test_no_duplicate_position_during_creation(self):
        patch_data = {
            "activity_steps": [
                {
                    'used_template_activity_step': {'uuid': self.template_activity_step.pk},
                    'planned_effort': 5.0,
                    'used_step': {'uuid': self.step.pk},
                    'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                    'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
                    'unit': 'hourly',
                    'position': 1
                },
                {
                    'used_template_activity_step': {'uuid': self.template_activity_step.pk},
                    'planned_effort': 5.0,
                    'used_step': {'uuid': self.step.pk},
                    'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                    'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
                    'unit': 'hourly',
                    'position': 1
                }
            ]
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {"__all__": ['Position should be unique']}
        )

    def test_no_duplication_exists_position(self):
        depends_on = ProjectActivityStepFactory(activity=self.project_activity1, position=36)
        patch_data = {
            "activity_steps": [{
                'used_template_activity_step': {'uuid': self.template_activity_step.pk},
                'planned_effort': 5.0,
                'used_step': {'uuid': self.step.pk},
                'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
                'unit': 'hourly',
                'position': depends_on.position
            }]
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {"__all__": ['You need to send all Steps, where a position was changed by another Step']}
        )

    def test_project_activity_step_used_rate(self):
        self.last_step.refresh_from_db()
        self.assertIsNotNone(self.last_step.used_rate)
        before = self.last_step.used_rate
        rate = self.last_step.used_step.rates.get(currency=self.last_step.used_rate.currency)
        rate.hourly_rate = "5.00"
        rate.save()
        self.last_step.refresh_from_db()
        self.assertEqual(before, self.last_step.used_rate)

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        self.login_as_user()
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])
        response = self.admin_client.patch(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 403)

        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.admin_client.patch(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 200)

        self.login_as_accounting_user()
        response = self.admin_client.patch(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 403)

        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.admin_client.patch(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 403)

        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.admin_client.patch(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 200)

        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])

    def test_validation_error_while_creating(self):
        self.step.rates.all().update(daily_rate=None)
        patch_data = copy.deepcopy(self.patch_data)
        copied_change_data = copy.deepcopy(patch_data["activity_steps"][0])
        copied_change_data['unit'] = "daily"
        copied_change_data['position'] = 23
        patch_data["activity_steps"][0] = copied_change_data
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {'activity_steps': [{'error': ['There is no rate for such a unit for this step']}]}
        )

        self.step.rates.all().update(daily_rate=Money("5.00", "CHF"))
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_validation_currency_error_while_creating(self):
        Project.objects.filter(pk=self.project1.pk).update(billing_currency="GBP")
        self.step.rates.filter(currency="GBP").delete()
        patch_data = copy.deepcopy(self.patch_data)
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {'activity_steps': [{self.step.name: ['There is no rate for such project currency for this step']}]}
        )


class TestProjectActivityStepCreateWithActivity(BaseCreateWithActivity):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.post_data = {
            "display_name": "Displayed name",
            "start_date": cls.start_date,
            "end_date": cls.end_date,
            "description": "Displayed description different from something",
            "used_template_activity": {'uuid': cls.template_activity.id},
            "activity_group": {'uuid': cls.activity_group.id},
            "activity_steps": [{
                'used_template_activity_step': {'uuid': cls.template_activity_step.pk},
                'unit': 'daily',
                'position': 1,
                'used_step': {'uuid': cls.step.pk},
                'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
            }]
        }
        cls.response = cls.admin_client.post(cls.endpoint, cls.post_data, format="json")

    def test_response(self):
        self.assertEqual(self.response.status_code, 201)
        self.assertTrue("position" in self.response.data["activity_steps"][0])

    def test_project_activity_step_created_object_is_correct(self):
        last_step = ProjectActivityStep.objects.exclude(id=self.project_activity_step1.id).last()
        data_to_check = self.post_data["activity_steps"][0]
        for key in data_to_check:
            data = getattr(last_step, key)
            if hasattr(data, "id"):
                data = data.id
            elif hasattr(data, "strftime"):
                data = data.strftime('%Y-%m-%d')
            if isinstance(data_to_check[key], dict) and 'uuid' in data_to_check[key]:
                self.assertEqual(data, data_to_check[key]['uuid'])
            else:
                self.assertEqual(data, data_to_check[key], f"Error with key {key}")

    def test_check_end_date_less_than_start_date(self):
        post_data = copy.deepcopy(self.post_data)
        copied_change_data = copy.deepcopy(post_data["activity_steps"][0])
        copied_change_data['end_date'] = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')
        post_data["activity_steps"][0] = copied_change_data
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_project_activity_step_dependency_position(self):
        before = ProjectActivityStep.objects.filter(dependency__isnull=False).count()
        post_data = copy.deepcopy(self.post_data)
        post_data['activity_steps'].append(
            {
                'used_template_activity_step': {'uuid': self.template_activity_step.pk},
                'unit': 'daily',
                'position': 2,
                'dependency_position': 1,
                'used_step': {'uuid': self.step.pk},
                'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
            }
        )
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(before + 1, ProjectActivityStep.objects.filter(dependency__isnull=False).count())

    def test_dependency_position_wrong(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['activity_steps'].append(
            {
                'used_template_activity_step': {'uuid': self.template_activity_step.pk},
                'unit': 'daily',
                'position': 2,
                'dependency_position': 111,
                'used_step': {'uuid': self.step.pk},
                'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
            }
        )
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertErrorResponse(response, {'__all__': ['There is no activity step with such position to depends on']})

    def test_position_duplicate(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['activity_steps'].append(
            {
                'used_template_activity_step': {'uuid': self.template_activity_step.pk},
                'unit': 'daily',
                'position': 1,
                'used_step': {'uuid': self.step.pk},
                'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
            }
        )
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertErrorResponse(response, {'__all__': ['Position should be unique']})

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        self.login_as_user()
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])
        response = self.admin_client.post(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 403)

        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.admin_client.post(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 400)

        self.login_as_accounting_user()
        response = self.admin_client.post(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 403)

        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.admin_client.post(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 403)

        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.admin_client.post(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 400)

        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])

    def test_create_with_effort(self):
        post_data = copy.deepcopy(self.post_data)
        post_data["activity_steps"] = [
            {
                'used_template_activity_step': {'uuid': self.template_activity_step.pk},
                'unit': 'daily',
                'planned_effort': 15.0,
                'position': 23,
                'used_step': {'uuid': self.step.pk},
                'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
            }
        ]
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(float(response.data['activity_steps'][0]['planned_effort']),
                         float(post_data['activity_steps'][0]['planned_effort']))


class TestProjectActivityStepUpdate(BaseAloneCreationClass):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project2 = ProjectFactory(billing_currency="CHF")
        cls.project_activity2 = ProjectActivityFactory(project=cls.project2)
        cls.project_activity_step2 = ProjectActivityStepFactory(activity=cls.project_activity2)
        cls.project3 = ProjectFactory(billing_currency="CHF")
        cls.project_activity3 = ProjectActivityFactory(project=cls.project3)
        cls.project_activity_step3 = ProjectActivityStepFactory(activity=cls.project_activity3)
        cls.endpoint2 = reverse('api_projects:activity-detail',
                                kwargs={'project_pk': cls.project2.id, 'pk': cls.project_activity2.id})
        cls.endpoint3 = reverse('api_projects:activity-detail',
                                kwargs={'project_pk': cls.project3.id, 'pk': cls.project_activity3.id})
        cls.patch_data = {
            "activity_steps": [{
                'uuid': cls.project_activity_step2.id,
                'unit': 'hourly',
                'used_step': {'uuid': cls.step.id}
            }]
        }
        cls.response = cls.admin_client.patch(cls.endpoint2, cls.patch_data, format="json")

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_data_changed(self):
        unit = self.patch_data['activity_steps'][0]['unit']
        self.project_activity_step2.refresh_from_db()
        self.assertEqual(self.project_activity_step2.unit, unit)

    def test_create_update_delete_dependency(self):
        depends_on = ProjectActivityStepFactory(activity=self.project_activity3)
        patch_data = {
            "activity_steps": [{
                'uuid': self.project_activity_step3.id,
                'dependency_position': depends_on.position,
            }]
        }
        response = self.admin_client.patch(self.endpoint3, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint3)
        position_check = [x['dependency_position']
                          for x in response.data.get("activity_steps") if x.get('dependency_position') is not None]
        self.assertIn(depends_on.position, position_check)
        # Now let's try to delete this
        patch_data = {
            "activity_steps": [{
                'uuid': self.project_activity_step3.id,
                'dependency_position': None,
            }]
        }
        response = self.admin_client.patch(self.endpoint3, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        position_check = [x['dependency_position']
                          for x in response.data.get("activity_steps") if x.get('dependency_position') is not None]
        self.assertNotIn(depends_on.position, position_check)

    def test_activity_step_update_validation_error_while_creating(self):
        self.step.rates.all().update(daily_rate=None)
        patch_data = copy.deepcopy(self.patch_data)
        copied_change_data = copy.deepcopy(patch_data["activity_steps"][0])
        copied_change_data['unit'] = "daily"
        patch_data["activity_steps"][0] = copied_change_data
        response = self.admin_client.patch(self.endpoint2, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {'activity_steps': [{'error': ['There is no rate for such a unit for this step']}]}
        )
        self.step.rates.all().update(daily_rate=Money("5.00", "CHF"))
        response = self.admin_client.patch(self.endpoint2, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    @pytest.mark.skip("Needs more attention ")
    def test_activity_step_update_validation_currency_error_while_creating(self):
        project = ProjectFactory(billing_currency="CHF")

        project.billing_currency = "GBP"
        project.save(update_fields=['billing_currency'])

        self.step.rates.filter(currency="GBP").delete()

        patch_data = copy.deepcopy(self.patch_data)
        response = self.admin_client.patch(self.endpoint2, patch_data, format="json")

        self.assertErrorResponse(response,
                                 {'activity_steps':
                                  [{self.step.name: ['There is no rate for such project currency for this step']}]})

    def test_activity_step_update_lot_of_nested_changes(self):
        patch_data = {
            "activity_steps": [{
                'uuid': self.project_activity_step2.id,
                'unit': 'hourly',
                'used_step': {'uuid': self.step.id},
                'name': "Programming",
                'choosenRate': 700,
                'end_date': "2100-02-15",
                'planned_effort': 1,
                'start_date': "2100-01-15",
                'status': 'plan',
                'assignments': [
                    {
                        'worker': {'uuid': self.test_user.id}
                    }
                ]
            }]
        }
        response = self.admin_client.patch(self.endpoint2, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_update_with_the_same_positions(self):
        project_activity_step2 = ProjectActivityStepFactory(activity=self.project_activity2)
        patch_data = {
            "activity_steps": [
                {
                    'uuid': self.project_activity_step2.id,
                    'name': "Programming",
                    'position': self.project_activity_step2.position
                },
                {
                    'uuid': project_activity_step2.id,
                    'name': "Programming",
                    'position': project_activity_step2.position
                }
            ]
        }
        response = self.admin_client.patch(self.endpoint2, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_update_with_the_swap_positions(self):
        project_activity_step2 = ProjectActivityStepFactory(activity=self.project_activity2)
        patch_data = {
            "activity_steps": [
                {
                    'uuid': self.project_activity_step2.id,
                    'name': "Programming",
                    'position': project_activity_step2.position
                },
                {
                    'uuid': project_activity_step2.id,
                    'name': "Programming",
                    'position': self.project_activity_step2.position
                }
            ]
        }
        response = self.admin_client.patch(self.endpoint2, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_update_with_the_swap_positions_error(self):
        project_activity_step2 = ProjectActivityStepFactory(activity=self.project_activity2)
        patch_data = {
            "activity_steps": [
                {
                    'uuid': self.project_activity_step2.id,
                    'name': "Programming",
                    'position': project_activity_step2.position
                },
            ]
        }
        response = self.admin_client.patch(self.endpoint2, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {"__all__": ['You need to send all Steps, where a position was changed by another Step']}
        )

    def test_mixed_update_and_create(self):
        project_activity_step2 = ProjectActivityStepFactory(activity=self.project_activity2)
        patch_data = {
            "activity_steps": [
                {
                    'uuid': self.project_activity_step2.id,
                    'name': "Programming",
                    'position': project_activity_step2.position
                },
                {
                    'uuid': project_activity_step2.id,
                    'name': "Programming",
                    'position': self.project_activity_step2.position
                },
                {
                    'unit': 'hourly',
                    'used_step': {'uuid': self.step.id},
                    'name': "Programming",
                    'choosenRate': 700,
                    'end_date': "2100-02-15",
                    'planned_effort': 1,
                    'position': ProjectActivityStep.objects.order_by("-position")[0].position + 1,
                    'start_date': "2100-01-15",
                    'status': 'plan',
                    'assignments': [
                        {
                            'worker': {'uuid': self.test_user.id}
                        }
                    ]
                }
            ]
        }
        response = self.admin_client.patch(self.endpoint2, patch_data, format="json")
        self.assertEqual(response.status_code, 200, response.data)

    def test_project_activity_step_updates_for_the_used_rate(self):
        """ We should not allow the used_rate change apart from situation when we change the step
        """
        self.project_activity_step2.refresh_from_db()
        self.assertIsNotNone(self.project_activity_step2.used_rate)
        before = self.project_activity_step2.used_rate
        rate = self.project_activity_step2.used_step.rates.get(currency=self.project_activity_step2.used_rate.currency)
        rate.hourly_rate = "5.00"
        rate.save()
        response = self.admin_client.patch(self.endpoint2, self.patch_data, format="json")
        self.project_activity_step2.refresh_from_db()
        self.assertEqual(before, self.project_activity_step2.used_rate)
        template_activity_step = TemplateActivityStepFactory()
        # now let's check that we recalculate rate when changing the step
        step2 = template_activity_step.step
        rate = step2.rates.get(currency=self.project_activity_step2.used_rate.currency)
        rate.hourly_rate = "5.00"
        rate.save()
        patch_data = {
            "activity_steps": [{
                'uuid': self.project_activity_step2.id,
                'unit': 'hourly',
                'used_step': {'uuid': step2.id}
            }]
        }
        response = self.admin_client.patch(self.endpoint2, patch_data, format="json")
        self.project_activity_step2.refresh_from_db()
        self.assertNotEqual(before, self.project_activity_step2.used_rate)

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        self.login_as_user()
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])
        response = self.admin_client.patch(self.endpoint2, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)

        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.admin_client.patch(self.endpoint2, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)

        self.login_as_accounting_user()
        response = self.admin_client.patch(self.endpoint2, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)

        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.admin_client.patch(self.endpoint2, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)

        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.admin_client.patch(self.endpoint2, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)

        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])


class TestProjectActivityStepCancel(BaseAloneCreationClass):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.patch_data = {
            "activity_steps": [{
                'uuid': cls.project_activity_step1.id,
                'status': 'canceled',
            }]
        }
        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format="json")

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_data_changed(self):
        status = self.patch_data['activity_steps'][0]['status']
        self.project_activity_step1.refresh_from_db()
        self.assertEqual(self.project_activity_step1.status, status)


class TestProjectActivityStepDelete(BaseAssigmentUpdate):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project_activity_step2 = ProjectActivityStepFactory(activity=cls.project_activity1)

    def test_delete(self):
        before = ProjectActivityStep.objects.count()
        patch_data = {
            "activity_steps": [{
                'uuid': self.project_activity_step2.id,
                'deleted': True,
            }]
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(before - 1, ProjectActivityStep.objects.count())

    def test_error_delete_if_worklog_exists(self):
        worker = UserFactory()
        ActivityAssignmentFactory(worker=worker, project_activity_step=self.project_activity_step1)
        WorkLogFactory(worker=worker, project_activity_step=self.project_activity_step1, tracked_time=1)
        patch_data = {
            "activity_steps": [{
                'uuid': self.project_activity_step1.id,
                'deleted': True,
            }]
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {'activity_steps': [{'error': ["You can't delete step with logged hours"]}]}
        )


class TestProjectActivityStepPATCHPermissions(BaseAloneCreationClass):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        # `project_manager` is has ROLE.USER but is the user who creates a Project.
        cls.project_manager = UserFactory(email='offer_manager@test.com', first_name="John",
                                          password=cls.default_password, role=ROLE.USER)
        cls.verify_user_email(cls.project_manager)
        cls.project_manager_client = APIRestAuthJWTClient()
        cls.login_as_user(cls.project_manager_client, cls.project_manager)
        cls.base_patch_data = {
            "activity_steps": [{
                'uuid': cls.project_activity_step1.id,
                'name': 'Base Patch',
            }]
        }

    def test_admin_permissions(self):
        patch_data_for_admin = deepcopy(self.base_patch_data)
        patch_data_for_admin['activity_steps'][0]['name'] = 'Admin Patch'
        admin_response = self.admin_client.patch(
            self.endpoint, patch_data_for_admin, format="json"
        )
        self.assertEqual(admin_response.status_code, status.HTTP_200_OK)

    def test_hr_permissions(self):
        patch_data_for_hr = deepcopy(self.base_patch_data)
        patch_data_for_hr['activity_steps'][0]['name'] = 'Patch Name for HR'
        hr_response = self.hr_client.patch(
            self.endpoint, patch_data_for_hr, format="json"
        )
        self.assertEqual(hr_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_accountant_permissions(self):
        patch_data_for_accountant = deepcopy(self.base_patch_data)
        patch_data_for_accountant['activity_steps'][0]['name'] = 'Patch Name for Accountant'
        accountant_response = self.accounting_client.patch(
            self.endpoint, patch_data_for_accountant, format="json"
        )
        self.assertEqual(accountant_response.status_code, status.HTTP_200_OK)

    def test_backoffice_permissions(self):
        patch_data_for_backoffice = deepcopy(self.base_patch_data)
        patch_data_for_backoffice['activity_steps'][0]['name'] = 'Patch Name for Backoffice'
        backoffice_response = self.backoffice_client.patch(
            self.endpoint, patch_data_for_backoffice, format="json"
        )
        self.assertEqual(backoffice_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user_permissions(self):
        patch_data_for_user = deepcopy(self.base_patch_data)
        patch_data_for_user['activity_steps'][0]['name'] = 'Patch Name for user'
        user_response = self.user_client.patch(
            self.endpoint, patch_data_for_user, format="json"
        )
        self.assertEqual(user_response.status_code, status.HTTP_200_OK)

    def test_project_manager_permissions(self):
        patch_data_for_project_manager = deepcopy(self.base_patch_data)
        patch_data_for_project_manager['activity_steps'][0]['name'] = 'Patch Name for project_manager'
        project_manager_response = self.project_manager_client.patch(
            self.endpoint, patch_data_for_project_manager, format="json"
        )
        self.assertEqual(project_manager_response.status_code, status.HTTP_200_OK)


class TestProjectActivityStepStatusPermissions(BaseAloneCreationClass):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        # `project_manager` is has ROLE.USER but is the user who creates a Project.
        cls.project_manager = UserFactory(email='project_manager@test.com', first_name="John",
                                          password=cls.default_password, role=ROLE.USER)
        cls.key_account_manager = UserFactory(email='key_account_manager@test.com', first_name="Kevin",
                                          password=cls.default_password, role=ROLE.USER)
        cls.verify_user_email(cls.project_manager)
        cls.verify_user_email(cls.key_account_manager)
        cls.project1.project_owner = cls.project_manager
        cls.project1.save()
        cls.company = cls.project1.company
        cls.company.key_account_manager = cls.key_account_manager
        cls.company.save()

        cls.project_manager_client = APIRestAuthJWTClient()
        cls.key_account_manager_client = APIRestAuthJWTClient()
        cls.login_as_user(cls.project_manager_client, cls.project_manager)
        cls.login_as_user(cls.key_account_manager_client, cls.key_account_manager)
        cls.base_patch_data = {
            "activity_steps": [{
                'uuid': cls.project_activity_step1.id,
                'status': ACTIVITY_STATUS.PLAN,
            }]
        }

    def test_user_permissions_status_to_work(self):
        patch_work_status_data_for_user = deepcopy(self.base_patch_data)
        patch_work_status_data_for_user['activity_steps'][0]['status'] = ACTIVITY_STATUS.WORK
        patch_work_status_for_user_response = self.user_client.patch(
            self.endpoint, patch_work_status_data_for_user, format="json"
        )
        self.assertEqual(patch_work_status_for_user_response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            patch_work_status_for_user_response.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.WORK
        )

        patch_plan_status_data_for_user = deepcopy(self.base_patch_data)
        patch_plan_status_data_for_user['activity_steps'][0]['status'] = ACTIVITY_STATUS.PLAN
        patch_plan_status_data_for_user_response = self.user_client.patch(
            self.endpoint, patch_plan_status_data_for_user, format="json"
        )
        self.assertEqual(patch_plan_status_data_for_user_response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            patch_plan_status_data_for_user_response.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.PLAN
        )

    def test_user_permissions_status_to_done(self):
        patch_done_status_data_for_user = deepcopy(self.base_patch_data)
        patch_done_status_data_for_user['activity_steps'][0]['status'] = ACTIVITY_STATUS.DONE
        patch_done_status_data_for_user = self.user_client.patch(
            self.endpoint, patch_done_status_data_for_user, format="json"
        )
        self.assertEqual(patch_done_status_data_for_user.status_code, status.HTTP_200_OK)
        self.assertEqual(
            patch_done_status_data_for_user.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.DONE
        )

    def test_project_manager_permissions_status_to_done(self):
        patch_billable_status_data_for_project_manager = deepcopy(self.base_patch_data)
        patch_billable_status_data_for_project_manager['activity_steps'][0]['status'] = ACTIVITY_STATUS.DONE
        patch_billable_status_data_for_project_manager = self.project_manager_client.patch(
            self.endpoint, patch_billable_status_data_for_project_manager, format="json"
        )
        self.assertEqual(patch_billable_status_data_for_project_manager.status_code, status.HTTP_200_OK)

        self.assertEqual(
            patch_billable_status_data_for_project_manager.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.DONE
        )

    def test_key_account_manager_permissions_status_to_billable(self):
        patch_billable_status_data_for_key_account_manager = deepcopy(self.base_patch_data)
        patch_billable_status_data_for_key_account_manager['activity_steps'][0]['status'] = ACTIVITY_STATUS.BILLABLE
        patch_billable_status_data_for_key_account_manager = self.key_account_manager_client.patch(
            self.endpoint, patch_billable_status_data_for_key_account_manager, format="json"
        )
        self.assertEqual(patch_billable_status_data_for_key_account_manager.status_code, status.HTTP_200_OK)

        self.assertEqual(
            patch_billable_status_data_for_key_account_manager.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.BILLABLE
        )

    def test_user_permissions_status_to_billable(self):
        patch_billable_status_data_for_user = deepcopy(self.base_patch_data)
        patch_billable_status_data_for_user['activity_steps'][0]['status'] = ACTIVITY_STATUS.BILLABLE
        patch_billable_status_data_for_user = self.user_client.patch(
            self.endpoint, patch_billable_status_data_for_user, format="json"
        )
        self.assertEqual(patch_billable_status_data_for_user.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin_permissions_status_to_billable(self):
        patch_billable_status_data_for_admin = deepcopy(self.base_patch_data)
        patch_billable_status_data_for_admin['activity_steps'][0]['status'] = ACTIVITY_STATUS.BILLABLE
        patch_billable_status_data_for_admin = self.admin_client.patch(
            self.endpoint, patch_billable_status_data_for_admin, format="json"
        )
        self.assertEqual(patch_billable_status_data_for_admin.status_code, status.HTTP_200_OK)

        self.assertEqual(
            patch_billable_status_data_for_admin.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.BILLABLE
        )

    def test_accountant_permissions_status_to_billable(self):
        patch_billable_status_data_for_accountant = deepcopy(self.base_patch_data)
        patch_billable_status_data_for_accountant['activity_steps'][0]['status'] = ACTIVITY_STATUS.BILLABLE
        patch_billable_status_data_for_accountant = self.accounting_client.patch(
            self.endpoint, patch_billable_status_data_for_accountant, format="json"
        )
        self.assertEqual(patch_billable_status_data_for_accountant.status_code, status.HTTP_200_OK)

        self.assertEqual(
            patch_billable_status_data_for_accountant.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.BILLABLE
        )

    def test_hr_permissions_status_to_billable(self):
        patch_billable_status_data_for_hr = deepcopy(self.base_patch_data)
        patch_billable_status_data_for_hr['activity_steps'][0]['status'] = ACTIVITY_STATUS.BILLABLE
        patch_billable_status_data_for_hr = self.hr_client.patch(
            self.endpoint, patch_billable_status_data_for_hr, format="json"
        )
        self.assertEqual(patch_billable_status_data_for_hr.status_code, status.HTTP_403_FORBIDDEN)

    def test_project_manager_permissions_status_to_billable(self):
        patch_billable_status_data_for_accountant = deepcopy(self.base_patch_data)
        patch_billable_status_data_for_accountant['activity_steps'][0]['status'] = ACTIVITY_STATUS.BILLABLE
        patch_billable_status_data_for_accountant = self.project_manager_client.patch(
            self.endpoint, patch_billable_status_data_for_accountant, format="json"
        )
        self.assertEqual(patch_billable_status_data_for_accountant.status_code, status.HTTP_200_OK)

        self.assertEqual(
            patch_billable_status_data_for_accountant.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.BILLABLE
        )

    def test_user_permissions_status_to_on_hold(self):
        patch_on_hold_status_data_for_user = deepcopy(self.base_patch_data)
        patch_on_hold_status_data_for_user['activity_steps'][0]['status'] = ACTIVITY_STATUS.ON_HOLD
        patch_on_hold_status_data_for_user = self.user_client.patch(
            self.endpoint, patch_on_hold_status_data_for_user, format="json"
        )
        self.assertEqual(patch_on_hold_status_data_for_user.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin_permissions_status_to_on_hold(self):
        patch_on_hold_status_data_for_admin = deepcopy(self.base_patch_data)
        patch_on_hold_status_data_for_admin['activity_steps'][0]['status'] = ACTIVITY_STATUS.ON_HOLD
        patch_on_hold_status_data_for_admin = self.admin_client.patch(
            self.endpoint, patch_on_hold_status_data_for_admin, format="json"
        )
        self.assertEqual(patch_on_hold_status_data_for_admin.status_code, status.HTTP_200_OK)

        self.assertEqual(
            patch_on_hold_status_data_for_admin.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.ON_HOLD
        )

    def test_accountant_permissions_status_to_on_hold(self):
        patch_on_hold_status_data_for_accountant = deepcopy(self.base_patch_data)
        patch_on_hold_status_data_for_accountant['activity_steps'][0]['status'] = ACTIVITY_STATUS.ON_HOLD
        patch_on_hold_status_data_for_accountant = self.accounting_client.patch(
            self.endpoint, patch_on_hold_status_data_for_accountant, format="json"
        )
        self.assertEqual(patch_on_hold_status_data_for_accountant.status_code, status.HTTP_200_OK)

        self.assertEqual(
            patch_on_hold_status_data_for_accountant.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.ON_HOLD
        )

    def test_project_manager_permissions_status_to_on_hold(self):
        patch_on_hold_status_data_for_project_manager = deepcopy(self.base_patch_data)
        patch_on_hold_status_data_for_project_manager['activity_steps'][0]['status'] = ACTIVITY_STATUS.ON_HOLD
        patch_on_hold_status_data_for_project_manager = self.project_manager_client.patch(
            self.endpoint, patch_on_hold_status_data_for_project_manager, format="json"
        )
        self.assertEqual(patch_on_hold_status_data_for_project_manager.status_code, status.HTTP_200_OK)

        self.assertEqual(
            patch_on_hold_status_data_for_project_manager.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.ON_HOLD
        )

    def test_user_permissions_status_to_closed(self):
        patch_closed_status_data_for_user = deepcopy(self.base_patch_data)
        patch_closed_status_data_for_user['activity_steps'][0]['status'] = ACTIVITY_STATUS.CLOSED
        patch_closed_status_data_for_user = self.user_client.patch(
            self.endpoint, patch_closed_status_data_for_user, format="json"
        )
        self.assertEqual(patch_closed_status_data_for_user.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin_permissions_status_to_closed(self):
        patch_closed_status_data_for_admin = deepcopy(self.base_patch_data)
        patch_closed_status_data_for_admin['activity_steps'][0]['status'] = ACTIVITY_STATUS.CLOSED
        patch_closed_status_data_for_admin = self.admin_client.patch(
            self.endpoint, patch_closed_status_data_for_admin, format="json"
        )
        self.assertEqual(patch_closed_status_data_for_admin.status_code, status.HTTP_200_OK)

        self.assertEqual(
            patch_closed_status_data_for_admin.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.CLOSED
        )

    def test_accountant_permissions_status_to_closed(self):
        patch_closed_status_data_for_accountant = deepcopy(self.base_patch_data)
        patch_closed_status_data_for_accountant['activity_steps'][0]['status'] = ACTIVITY_STATUS.CLOSED
        patch_closed_status_data_for_accountant = self.accounting_client.patch(
            self.endpoint, patch_closed_status_data_for_accountant, format="json"
        )
        self.assertEqual(patch_closed_status_data_for_accountant.status_code, status.HTTP_403_FORBIDDEN)

    def test_project_manager_permissions_status_to_closed(self):
        patch_closed_status_data_for_accountant = deepcopy(self.base_patch_data)
        patch_closed_status_data_for_accountant['activity_steps'][0]['status'] = ACTIVITY_STATUS.CLOSED
        patch_closed_status_data_for_accountant = self.project_manager_client.patch(
            self.endpoint, patch_closed_status_data_for_accountant, format="json"
        )
        self.assertEqual(patch_closed_status_data_for_accountant.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin_permissions_status_to_irreversible_canceled(self):
        patch_canceled_status_data_for_admin = deepcopy(self.base_patch_data)
        patch_canceled_status_data_for_admin['activity_steps'][0]['status'] = ACTIVITY_STATUS.CANCEL
        patch_canceled_status_data_for_admin = self.admin_client.patch(
            self.endpoint, patch_canceled_status_data_for_admin, format="json"
        )
        self.assertEqual(patch_canceled_status_data_for_admin.status_code, status.HTTP_200_OK)

        self.assertEqual(
            patch_canceled_status_data_for_admin.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.CANCEL
        )

        patch_plan_from_cancel_status_data_for_admin = deepcopy(self.base_patch_data)
        patch_plan_from_cancel_status_data_for_admin['activity_steps'][0]['status'] = ACTIVITY_STATUS.PLAN
        patch_plan_from_cancel_status_data_for_admin = self.admin_client.patch(
            self.endpoint, patch_plan_from_cancel_status_data_for_admin, format="json"
        )
        self.assertEqual(patch_plan_from_cancel_status_data_for_admin.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin_permissions_status_to_canceled_with_hour_logs(self):
        patch_canceled_status_data_with_hour_logs = deepcopy(self.base_patch_data)
        patch_canceled_status_data_with_hour_logs['activity_steps'][0]['status'] = ACTIVITY_STATUS.CANCEL
        work_log = WorkLogFactory(project_activity_step=self.project_activity_step1)
        patch_canceled_status_data_with_hour_logs_response = self.admin_client.patch(
            self.endpoint, patch_canceled_status_data_with_hour_logs, format="json"
        )
        self.assertEqual(patch_canceled_status_data_with_hour_logs_response.status_code, status.HTTP_400_BAD_REQUEST)

        work_log.delete()
        patch_canceled_status_data_without_hour_logs_response = self.admin_client.patch(
            self.endpoint, patch_canceled_status_data_with_hour_logs, format="json"
        )
        self.assertEqual(patch_canceled_status_data_without_hour_logs_response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            patch_canceled_status_data_without_hour_logs_response.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.CANCEL
        )

    def test_accountant_permissions_status_to_irreversible_canceled(self):
        patch_canceled_status_data_for_accountant = deepcopy(self.base_patch_data)
        patch_canceled_status_data_for_accountant['activity_steps'][0]['status'] = ACTIVITY_STATUS.CANCEL
        patch_canceled_status_data_for_accountant = self.accounting_client.patch(
            self.endpoint, patch_canceled_status_data_for_accountant, format="json"
        )
        self.assertEqual(patch_canceled_status_data_for_accountant.status_code, status.HTTP_200_OK)

        self.assertEqual(
            patch_canceled_status_data_for_accountant.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.CANCEL
        )

        patch_from_canceled_to_done_status_data_for_accountant = deepcopy(self.base_patch_data)
        patch_from_canceled_to_done_status_data_for_accountant['activity_steps'][0]['status'] = ACTIVITY_STATUS.DONE
        patch_from_canceled_to_done_status_data_for_accountant = self.accounting_client.patch(
            self.endpoint, patch_from_canceled_to_done_status_data_for_accountant, format="json"
        )
        self.assertEqual(patch_from_canceled_to_done_status_data_for_accountant.status_code, status.HTTP_403_FORBIDDEN)

    def test_project_manager_permissions_status_to_irreversible_canceled(self):
        patch_canceled_status_data_for_accountant = deepcopy(self.base_patch_data)
        patch_canceled_status_data_for_accountant['activity_steps'][0]['status'] = ACTIVITY_STATUS.CANCEL
        patch_canceled_status_data_for_accountant = self.project_manager_client.patch(
            self.endpoint, patch_canceled_status_data_for_accountant, format="json"
        )
        self.assertEqual(patch_canceled_status_data_for_accountant.status_code, status.HTTP_200_OK)

        self.assertEqual(
            patch_canceled_status_data_for_accountant.data['activity_steps'][0]['status'],
            ACTIVITY_STATUS.CANCEL
        )

        patch_from_canceled_to_work_status_data_for_accountant = deepcopy(self.base_patch_data)
        patch_from_canceled_to_work_status_data_for_accountant['activity_steps'][0]['status'] = ACTIVITY_STATUS.WORK
        patch_from_canceled_to_work_status_data_for_accountant = self.project_manager_client.patch(
            self.endpoint, patch_from_canceled_to_work_status_data_for_accountant, format="json"
        )
        self.assertEqual(patch_from_canceled_to_work_status_data_for_accountant.status_code, status.HTTP_403_FORBIDDEN)
