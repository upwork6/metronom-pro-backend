from decimal import Decimal

from django.test import TestCase
from djmoney.money import Money
from djmoney_rates.backends import BaseRateBackend

from apps.metronom_commons.data import ACTIVITY_STATUS, CURRENCY
from apps.projects.models import ProjectActivityStep
from apps.projects.tests.factories import (CampaignFactory,
                                           ProjectActivityFactory,
                                           ProjectActivityStepFactory,
                                           ProjectArticleFactory,
                                           ProjectFactory, WorkLogFactory)
from apps.users.tests.factories import UserFactory


class TestProjectSetupMixin(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestProjectSetupMixin, cls).setUpTestData()
        cls.worker = UserFactory()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.activity1 = ProjectActivityFactory(project=cls.project)
        cls.activity2 = ProjectActivityFactory(project=cls.project)
        cls.activity3 = ProjectActivityFactory(project=cls.project, status=ACTIVITY_STATUS.DONE)
        cls.activity4 = ProjectActivityFactory(project=cls.project, status=ACTIVITY_STATUS.DONE)

        cls.step1 = ProjectActivityStepFactory(activity=cls.activity1, planned_effort=2, unit=ProjectActivityStep.HOURS)
        cls.step2 = ProjectActivityStepFactory(activity=cls.activity2, planned_effort=4, unit=ProjectActivityStep.DAYS)
        cls.step3 = ProjectActivityStepFactory(
            activity=cls.activity3,
            planned_effort=2,
            unit=ProjectActivityStep.HOURS,
            status=ACTIVITY_STATUS.DONE
        )
        cls.step4 = ProjectActivityStepFactory(
            activity=cls.activity4,
            planned_effort=4,
            unit=ProjectActivityStep.DAYS,
            status=ACTIVITY_STATUS.DONE
        )

        cls.budget = Money(Decimal('100.00'), 'CHF')
        cls.articles = ProjectArticleFactory.create_batch(
            size=4, project=cls.project, budget_in_project_currency=cls.budget
        )


class TestCampaignSetupMixin(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestCampaignSetupMixin, cls).setUpTestData()
        worker = UserFactory()
        cls.campaign = CampaignFactory(billing_currency="CHF")
        project1 = ProjectFactory(campaign=cls.campaign, billing_currency="CHF")
        project2 = ProjectFactory(campaign=cls.campaign, billing_currency="CHF")
        activity1 = ProjectActivityFactory(project=project1)
        activity2 = ProjectActivityFactory(project=project2)
        activity3 = ProjectActivityFactory(project=project1, status=ACTIVITY_STATUS.DONE)
        activity4 = ProjectActivityFactory(project=project2, status=ACTIVITY_STATUS.DONE)

        step1 = ProjectActivityStepFactory(activity=activity1, planned_effort=2, unit=ProjectActivityStep.HOURS)
        step2 = ProjectActivityStepFactory(activity=activity2, planned_effort=4, unit=ProjectActivityStep.DAYS)
        step3 = ProjectActivityStepFactory(
            activity=activity3,
            planned_effort=2,
            unit=ProjectActivityStep.HOURS,
            status=ACTIVITY_STATUS.DONE
        )
        step4 = ProjectActivityStepFactory(
            activity=activity4,
            planned_effort=4,
            unit=ProjectActivityStep.DAYS,
            status=ACTIVITY_STATUS.DONE
        )

        for step in [step1, step2, step3, step4]:
            WorkLogFactory.create_batch(size=3, worker=worker, project_activity_step=step, tracked_time=4)

        budget = Money(Decimal('100.00'), 'CHF')
        ProjectArticleFactory.create_batch(size=4, project=project1, budget_in_project_currency=budget)
        ProjectArticleFactory.create_batch(size=4, project=project2, budget_in_project_currency=budget)


class TestRateBackend(BaseRateBackend):
    source_name = 'test-rate-backend'
    base_currency = 'USD'

    def get_rates(self):
        return {
            CURRENCY.CHF: 0.950781,
            CURRENCY.EUR: 0.809984,
            CURRENCY.GBP: 0.710994,
            CURRENCY.USD: 1
        }
