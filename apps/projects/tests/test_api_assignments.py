""" This module exists for testing user assignment checks and testing status changes of those assignments
"""

import datetime

import pytest
from django.contrib.auth.models import Group

from apps.projects.models import ActivityAssignment
from apps.projects.tests.test_api_steps import (BaseAloneCreationClass,
                                                BaseAssigmentUpdate,
                                                BaseCreateWithActivity)


class TestProjectActivityStepAssignUserCreateAlone(BaseAloneCreationClass):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.before = ActivityAssignment.objects.count()
        cls.patch_data = {
            "activity_steps": [
                {
                    'used_template_activity_step': {'uuid': cls.template_activity_step.pk, },
                    'planned_effort': 5.0,
                    'position': 11,
                    'used_step': {'uuid': cls.step.pk, },
                    'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                    'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
                    'assignments': [
                        {
                            'worker': {'uuid': cls.test_user.id, }
                        }
                    ]
                },
                {
                    'uuid': cls.project_activity_step1.id,
                    'assignments': [
                        {
                            'worker': {'uuid': cls.test_user.id, }
                        },
                        {
                            'worker': {'uuid': cls.test_admin.id, }
                        }
                    ]
                }
            ]
        }
        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format="json")

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_changes(self):
        self.assertEqual(self.before + 3, ActivityAssignment.objects.count())
        self.assertEqual(self.project_activity_step1.assignments.count(), 2)

    def test_changes_display(self):
        response = self.admin_client.get(self.endpoint)
        assignments = [x["assignments"] for x in response.data.get("activity_steps")]
        self.assertNotEqual(len(assignments), 0)

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        self.login_as_user()
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])
        response = self.admin_client.patch(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 403)

        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.admin_client.patch(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 200)

        self.login_as_accounting_user()
        response = self.admin_client.patch(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 403)

        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.admin_client.patch(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 403)

        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.admin_client.patch(self.endpoint, {}, format="json")
        self.assertEqual(response.status_code, 200)

        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])


class UniqueAssignmentValidator(BaseAloneCreationClass):

    def test_validation_error_on_duplicate(self):
        patch_data = {
            "activity_steps": [
                {
                    'uuid': self.project_activity_step1.id,
                    'assignments': [
                        {
                            'worker': {'uuid': self.test_user.id, }
                        },
                        {
                            'worker': {'uuid': self.test_user.id, }
                        }
                    ]
                },
            ]
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_validation_error_on_duplicate_already_created(self):
        patch_data = {
            "activity_steps": [
                {
                    'uuid': self.project_activity_step1.id,
                    'assignments': [
                        {
                            'worker': {'uuid': self.test_user.id, }
                        },
                    ]
                },
            ]
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 400)


class TestProjectActivityStepAssignUserCreateWithActivity(BaseCreateWithActivity):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.before = ActivityAssignment.objects.count()
        cls.post_data = {
            "display_name": "Displayed name",
            "start_date": cls.start_date,
            "end_date": cls.end_date,
            "description": "Displayed description different from something",
            "used_template_activity": {'uuid': cls.template_activity.id},
            "activity_group": {'uuid': cls.activity_group.id},
            "activity_steps": [{
                'used_template_activity_step': {'uuid': cls.template_activity_step.pk, },
                "position": 22,
                'name': "Name3s",
                'used_step': {'uuid': cls.step.pk, },
                'start_date': datetime.datetime.now().strftime('%Y-%m-%d'),
                'end_date': (datetime.datetime.now() + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
                'assignments': [
                        {
                            'worker': {'uuid': cls.test_user.id, }
                        }
                ]
            }]
        }
        cls.response = cls.admin_client.post(cls.endpoint, cls.post_data, format="json")

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, 201)

    def test_changes(self):
        self.assertEqual(self.before + 1, ActivityAssignment.objects.count())

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        self.login_as_user()
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])
        response = self.admin_client.post(self.endpoint, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)

        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.admin_client.post(self.endpoint, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)

        self.login_as_accounting_user()
        response = self.admin_client.post(self.endpoint, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)

        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.admin_client.post(self.endpoint, self.post_data, format="json")
        self.assertEqual(response.status_code, 403)

        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.admin_client.post(self.endpoint, self.post_data, format="json")
        self.assertEqual(response.status_code, 201)

        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])


class TestActivityAssignmentUpdate(BaseAssigmentUpdate):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.before = ActivityAssignment.objects.count()
        cls.patch_data = {
            "activity_steps": [
                {
                    'uuid': cls.project_activity_step1.id,
                    'assignments': [
                        {
                            'uuid': cls.assignment1.id,
                            'worker': {'uuid': cls.test_admin.id, }
                        },
                        {
                            'uuid': cls.assignment2.id,
                            'worker':  {'uuid': cls.test_user.id, }
                        }
                    ]
                }
            ]
        }
        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format="json")

    def test_response_code(self):
        self.assertErrorResponse(
            self.response,
             {'activity_steps': [
                 {'assignments': [{'error': ["You can't edit assignment, only change status"]}, 
                 {'error': ["You can't edit assignment, only change status"]}]}]}
        )

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        self.login_as_user()
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])
        response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)

        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['activity_steps'][0]['assignments'][0]['error'],
                         ["You can't edit assignment, only change status"])

        self.login_as_accounting_user()
        response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)

        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)

        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['activity_steps'][0]['assignments'][0]['error'],
                         ["You can't edit assignment, only change status"])

        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])


class TestActivityAssignmentArchive(BaseAssigmentUpdate):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.before = ActivityAssignment.objects.count()
        cls.patch_data = {
            "activity_steps": [
                {
                    'uuid': cls.project_activity_step1.id,
                    'assignments': [
                        {
                            'uuid': cls.assignment1.id,
                            'status': 0
                        },
                        {
                            'uuid': cls.assignment2.id,
                            'status': 1
                        }
                    ]
                }
            ]
        }
        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format="json")

    def test_response_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_data_changed(self):
        self.assignment1.refresh_from_db()
        self.assignment2.refresh_from_db()
        self.assertEqual(self.assignment1.status, 0)
        self.assertEqual(self.assignment2.status, 1)

    @pytest.mark.skip(reason="No permissions for now")
    def test_permissions(self):
        self.login_as_user()
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])
        response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)

        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)

        self.login_as_accounting_user()
        response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)

        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 403)

        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.admin_client.patch(self.endpoint, self.patch_data, format="json")
        self.assertEqual(response.status_code, 200)

        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])


class TestActivityAssignmentDelete(BaseAssigmentUpdate):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.before = ActivityAssignment.objects.count()
        cls.patch_data = {
            "activity_steps": [
                {
                    'uuid': cls.project_activity_step1.id,
                    'assignments': [
                        {
                            'uuid': cls.assignment1.id,
                            'deleted': True
                        },
                        {
                            'uuid': cls.assignment2.id,
                            'status': 1
                        }
                    ]
                }
            ]
        }
        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format="json")

    def test_response_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_data_changed(self):
        self.assertEqual(self.before - 1, ActivityAssignment.objects.count())

    def test_some_interesting_situation(self):
        """ This requires some explanation. We need to check this rare but possible scenario:
        - Frontend adds a user with assignment A and User A
        - Backend saves Assignment A with User A
        - Frontend now deletes the assignment A with User A 
        - User decides to add the user back and now  posts User A (to get a new assignment)
        """
        before = ActivityAssignment.objects.count()
        patch_data = {
            "activity_steps": [
                {
                    'uuid': self.project_activity_step1.id,
                    'assignments': [
                        {
                            'uuid': self.assignment2.id,
                            'deleted': True
                        },
                        {
                            'worker': {'uuid': self.assignment2.worker.id, }
                        },
                    ]
                }
            ]
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(before, ActivityAssignment.objects.count())
