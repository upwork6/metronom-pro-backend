from django.test import TestCase
from django.urls import resolve, reverse
from rest_framework import status

from apps.companies.tests.factories import CompanyFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.tests.factories import (ActivityAssignmentFactory,
                                           CampaignFactory,
                                           ProjectActivityFactory,
                                           ProjectActivityStepFactory,
                                           ProjectFactory, WorkLogFactory,
                                           ProjectArticleFactory,
                                           ProjectArticleExpenseFactory)
from apps.users.tests.factories import UserFactory


class TestCampaignURLs(TestCase):
    def setUp(self):
        self.campaign = CompanyFactory()

    def test_campaign_base_reverse(self):
        assert reverse('api:campaign-list') == '/api/campaigns/'

    def test_campaign_base_resolve(self):
        assert resolve('/api/campaigns/').view_name == 'api:campaign-list'

    def test_campaign_detail_reverse(self):
        endpoint = reverse('api:campaign-detail', kwargs={'pk': self.campaign.id})
        assert endpoint == f'/api/campaigns/{self.campaign.id}/'

    def test_campaign_detail_resolve(self):
        assert resolve(f'/api/campaigns/{self.campaign.id}/').view_name == 'api:campaign-detail'


class TestProjectURLs(TestCase):
    def setUp(self):
        self.project = ProjectFactory(billing_currency="CHF")

    def test_project_base_reverse(self):
        assert reverse('api:project-list') == '/api/projects/'

    def test_project_base_resolve(self):
        assert resolve('/api/projects/').view_name == 'api:project-list'

    def test_project_detail_reverse(self):
        endpoint = reverse('api:project-detail', kwargs={'pk': self.project.id})
        assert endpoint == f'/api/projects/{self.project.id}/'

    def test_project_detail_resolve(self):
        assert resolve(f'/api/projects/{self.project.id}/').view_name == 'api:project-detail'


class BaseWithActivityStep(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.worker = UserFactory()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_activity_step = ProjectActivityStepFactory(activity=cls.project_activity)
        ActivityAssignmentFactory(worker=cls.worker, project_activity_step=cls.project_activity_step)


class TestWorkLogURLs(BaseWithActivityStep):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.worklog = WorkLogFactory(worker=cls.worker, project_activity_step=cls.project_activity_step)

    def test_worklog_list_reverse(self):
        assert reverse('api_users:user-worklog-list', kwargs={'user_pk': 'me'}) == f'/api/users/me/worklogs/'

    def test_worklog_list_resolve(self):
        assert resolve(f'/api/users/me/worklogs/').view_name == 'api_users:user-worklog-list'

    def test_worklog_detail_reverse(self):
        worklog_url = reverse('api_users:user-worklog-detail', kwargs={'user_pk': 'me', 'pk': self.worklog.pk})
        assert worklog_url == f'/api/users/me/worklogs/{self.worklog.pk}/'

    def test_worklog_detail_resolve(self):
        worklog_url = f'/api/users/me/worklogs/{self.worklog.pk}/'
        assert resolve(worklog_url).view_name == 'api_users:user-worklog-detail'


class TestWorkLogHRURLs(BaseWithActivityStep):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.worklog = WorkLogFactory(worker=cls.worker, project_activity_step=cls.project_activity_step)

    def test_worklog_list_reverse(self):
        assert reverse('api_users:user-worklog-list',
                       kwargs={'user_pk': self.worker.id}) == f'/api/users/{self.worker.id}/worklogs/'

    def test_worklog_list_resolve(self):
        assert resolve(f'/api/users/{self.worker.id}/worklogs/').view_name == 'api_users:user-worklog-list'

    def test_worklog_detail_reverse(self):
        worklog_url = reverse('api_users:user-worklog-detail',
                              kwargs={'user_pk': self.worker.id, 'pk': self.worklog.pk})
        assert worklog_url == f'/api/users/{self.worker.id}/worklogs/{self.worklog.pk}/'

    def test_worklog_detail_resolve(self):
        worklog_url = f'/api/users/{self.worker.id}/worklogs/{self.worklog.pk}/'
        assert resolve(worklog_url).view_name == 'api_users:user-worklog-detail'


class TestCampaignProjectURLs(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestCampaignProjectURLs, cls).setUpTestData()
        cls.campaign = CampaignFactory(billing_currency="CHF")
        cls.project = ProjectFactory(campaign=cls.campaign)

    def test_campaign_project_list_reverse(self):
        assert reverse('api_campaigns:project-list', kwargs={
            'campaign_pk': self.campaign.pk
        }) == f'/api/campaigns/{self.campaign.pk}/projects/'

    def test_campaign_project_list_resolve(self):
        assert resolve(f'/api/campaigns/{self.campaign.pk}/projects/').view_name == 'api_campaigns:project-list'

    def test_campaign_project_detail_reverse(self):
        project_url = reverse('api_campaigns:project-detail', kwargs={
            'campaign_pk': self.campaign.pk,
            'pk': self.project.pk
        })
        assert project_url == f'/api/campaigns/{self.campaign.pk}/projects/{self.project.pk}/'

    def test_campaign_project_detail_resolve(self):
        project_url = f'/api/campaigns/{self.campaign.pk}/projects/{self.project.pk}/'
        assert resolve(project_url).view_name == 'api_campaigns:project-detail'


class TestCampaignProjectURLsReadOnly(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        campaign = CampaignFactory(billing_currency="CHF")
        project = ProjectFactory(campaign=campaign)
        cls.detail_endpoint = reverse('api_campaigns:project-detail', kwargs={
            'campaign_pk': campaign.pk,
            'pk': project.pk
        })
        cls.list_endpoint = reverse('api_campaigns:project-list', kwargs={
            'campaign_pk': campaign.pk
        })

        cls.methods = (
            cls.user_client.post,
            cls.user_client.put,
            cls.user_client.delete,
            cls.user_client.patch,
        )
        cls.endpoints = (
            cls.detail_endpoint,
            cls.list_endpoint,
        )

    def test_methods_not_allowed(self):
        for method in self.methods:
            for endpoint in self.endpoints:
                print('%s,%s' % (method, endpoint))
                response = method(endpoint)
                self.assertIn(response.status_code, [status.HTTP_403_FORBIDDEN, status.HTTP_405_METHOD_NOT_ALLOWED])


class TestProjectActivityURLs(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestProjectActivityURLs, cls).setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.activity = ProjectActivityFactory(project=cls.project)

    def test_project_article_list_reverse(self):
        assert reverse('api_projects:activity-list', kwargs={
            'project_pk': self.project.pk
        }) == f'/api/projects/{self.project.pk}/activities/'

    def test_project_article_list_resolve(self):
        view_name = resolve(f'/api/projects/{self.project.pk}/activities/').view_name
        assert view_name == 'api_projects:activity-list'

    def test_project_article_detail_reverse(self):
        assert reverse('api_projects:activity-detail', kwargs={
            'project_pk': self.project.pk,
            'pk': self.activity.pk
        }) == f'/api/projects/{self.project.pk}/activities/{self.activity.pk}/'

    def test_project_article_detail_resolve(self):
        view_name = resolve(f'/api/projects/{self.project.pk}/activities/{self.activity.pk}/').view_name
        assert view_name == 'api_projects:activity-detail'


class TestProjectArticleURLs(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestProjectArticleURLs, cls).setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.article = ProjectArticleFactory(project=cls.project)

    def test_project_article_list_reverse(self):
        assert reverse('api_projects:article-list', kwargs={
            'project_pk': self.project.pk
        }) == f'/api/projects/{self.project.pk}/articles/'

    def test_project_article_list_resolve(self):
        view_name = resolve(f'/api/projects/{self.project.pk}/articles/').view_name
        assert view_name == 'api_projects:article-list'

    def test_project_article_detail_reverse(self):
        assert reverse('api_projects:article-detail', kwargs={
            'project_pk': self.project.pk,
            'pk': self.article.pk
        }) == f'/api/projects/{self.project.pk}/articles/{self.article.pk}/'

    def test_project_article_detail_resolve(self):
        view_name = resolve(f'/api/projects/{self.project.pk}/articles/{self.article.pk}/').view_name
        assert view_name == 'api_projects:article-detail'


class TestProjectArticleExpenseURLs(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestProjectArticleExpenseURLs, cls).setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.article = ProjectArticleFactory(project=cls.project)
        cls.expense = ProjectArticleExpenseFactory(article=cls.article)

    def test_project_article_list_reverse(self):
        assert reverse('api_articles:expense-list', kwargs={
            'project_pk': self.project.pk,
            'article_pk': self.article.pk
        }) == f'/api/projects/{self.project.pk}/articles/{self.article.pk}/expenses/'

    def test_project_article_list_resolve(self):
        view_name = resolve(f'/api/projects/{self.project.pk}/articles/{self.article.pk}/expenses/').view_name
        assert view_name == 'api_articles:expense-list'

    def test_project_article_detail_reverse(self):
        assert reverse('api_articles:expense-detail', kwargs={
            'project_pk': self.project.pk,
            'article_pk': self.article.pk,
            'pk': self.expense.pk
        }) == f'/api/projects/{self.project.pk}/articles/{self.article.pk}/expenses/{self.expense.pk}/'

    def test_project_article_detail_resolve(self):
        assert resolve(
            f'/api/projects/{self.project.pk}/articles/{self.article.pk}/expenses/{self.expense.pk}/'
        ).view_name == 'api_articles:expense-detail'
