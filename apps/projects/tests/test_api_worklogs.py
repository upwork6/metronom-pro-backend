import copy
from datetime import timedelta

from django.http import QueryDict
from django.utils import timezone
from rest_framework import status
from rest_framework.reverse import reverse

from apps.companies.tests.factories import CompanyWithOfficeFactory
from apps.metronom_commons.data import WORKLOGS_STATUS, ACTIVITY_STATUS
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.models import WorkLog, Project, Campaign
from apps.projects.tests.factories import (ActivityAssignmentFactory,
                                           ProjectActivityFactory,
                                           ProjectActivityStepFactory,
                                           ProjectFactory, WorkLogFactory, CampaignFactory)
from apps.users.tests.factories import UserFactory


class BaseWithActivityStep(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_activity_step = ProjectActivityStepFactory(activity=cls.project_activity)
        cls.assignment = ActivityAssignmentFactory(
            worker=cls.test_user, project_activity_step=cls.project_activity_step)


# todo: cross testing: NormalUser trying to access to an another user's worklog: status code: 40* (405 or 403 or etc)
# todo: cross testing: HRuser trying to access to the me endpoint: status code: OK


class TestUserWorkLogList(BaseWithActivityStep):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.worklogs = WorkLogFactory.create_batch(
            size=4, project_activity_step=cls.project_activity_step, worker=cls.test_admin
        )
        cls.endpoint = reverse('api_users:user-worklog-list', kwargs={'user_pk': 'me'})
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_user_worklog_list_response_code(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_user_worklog_list_response_size(self):
        assert isinstance(self.response.data, list)
        assert len(self.response.data) == 4

    def test_user_worklog_list_response_project_activity_step(self):
        assert all(x.get('project_activity_step', None) == self.project_activity_step.pk for x in self.response.data)

    def test_user_worklog_list_response_malformed_user(self):
        endpoint = reverse('api_users:user-worklog-list', kwargs={'user_pk': 'undefined_user'})
        response = self.admin_client.get(endpoint)

        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestUserWorkLogPost(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_activity_step = ProjectActivityStepFactory(activity=cls.project_activity)

        cls.work_date = timezone.now().strftime('%Y-%m-%d')
        cls.note = 'work log note 1'
        cls.satisfaction = 4

        cls.endpoint = reverse('api_users:user-worklog-list', kwargs={'user_pk': 'me'})
        cls.post_data = dict(
            project=cls.project.pk,
            project_activity=cls.project_activity.pk,
            project_activity_step=cls.project_activity_step.pk,
            tracked_time=2.00,
            work_date=cls.work_date,
            note=cls.note,
            satisfaction=cls.satisfaction
        )

        cls.response = cls.user_client.post(cls.endpoint, cls.post_data, format='json')

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_response_project_activity_step(self):
        field = self.response.data.get('project_activity_step', None)
        self.assertIsNotNone(field)
        assert field == self.project_activity_step.pk

    def test_response_tracked_time(self):
        field = self.response.data.get('tracked_time', None)
        self.assertIsNotNone(field)
        assert field == '2.00'

    def test_response_work_date(self):
        field = self.response.data.get('work_date', None)
        self.assertIsNotNone(field)
        assert field == self.work_date

    def test_response_note(self):
        field = self.response.data.get('note', None)
        self.assertIsNotNone(field)
        assert field == self.note

    def test_response_satisfaction(self):
        field = self.response.data.get('satisfaction', None)
        self.assertIsNotNone(field)
        assert field == self.satisfaction

    def test_response_malformed_user(self):
        endpoint = reverse('api_users:user-worklog-list', kwargs={'user_pk': 'undefined_user'})
        response = self.user_client.post(endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 404)

    def test_worklog_creation_permission(self):
        """ Working logs are added by the User itself. Only the User can add Working logs ,
        when the Step is in planning OR work state.
        """
        response = self.hr_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 201)
        response = self.accounting_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 201)
        response = self.backoffice_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 201)
        response = self.admin_client.post(self.endpoint, self.post_data, format='json')
        self.assertEqual(response.status_code, 201)
        self.project_activity_step.status = ACTIVITY_STATUS.ON_HOLD
        self.project_activity_step.save(update_fields=["status"])
        response = self.user_client.post(self.endpoint, self.post_data, format='json')
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can create new worklog only for steps with plan or work status"]
            }
        )
        response = self.hr_client.post(self.endpoint, self.post_data, format='json')
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can create new worklog only for steps with plan or work status"]
            }
        )
        response = self.accounting_client.post(self.endpoint, self.post_data, format='json')
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can create new worklog only for steps with plan or work status"]
            }
        )
        response = self.backoffice_client.post(self.endpoint, self.post_data, format='json')
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can create new worklog only for steps with plan or work status"]
            }
        )
        response = self.admin_client.post(self.endpoint, self.post_data, format='json')
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can create new worklog only for steps with plan or work status"]
            }
        )


class TestUserWorkLogPatch(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_activity_step = ProjectActivityStepFactory(activity=cls.project_activity)
        cls.worker = cls.test_user
        cls.assignment = ActivityAssignmentFactory(
            worker=cls.worker, project_activity_step=cls.project_activity_step)
        cls.worklog = WorkLogFactory(project_activity_step=cls.project_activity_step, worker=cls.worker)
        cls.project_activity_step2 = ProjectActivityStepFactory(activity=cls.project_activity)

        cls.patch_data = dict(
            project=cls.project.pk,
            project_activity=cls.project_activity.pk,
            project_activity_step=cls.project_activity_step.pk,
        )

        cls.endpoint = reverse('api_users:user-worklog-detail',
                               kwargs={'user_pk': cls.worklog.worker.id, 'pk': cls.worklog.pk})
        cls.response = cls.user_client.patch(cls.endpoint, cls.patch_data, format='json')

        cls.new_user = UserFactory(role="user", password=cls.default_password)
        cls.verify_user_email(cls.new_user)
        cls.new_user_client = cls.client_class()
        cls.login_as_user(client=cls.new_user_client, user=cls.new_user)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_patch_project_activity_step(self):
        patch_data = copy.copy(self.patch_data)
        patch_data['project_activity_step'] = self.project_activity_step2.pk
        response = self.user_client.patch(self.endpoint, patch_data, format='json')
        field = response.data.get('project_activity_step', None)
        self.assertIsNotNone(field)
        assert field == self.project_activity_step2.pk

    def test_patch_tracked_time(self):
        patch_data = copy.copy(self.patch_data)
        patch_data['tracked_time'] = 4.00
        response = self.user_client.patch(self.endpoint, patch_data, format='json')
        field = response.data.get('tracked_time', None)
        self.assertIsNotNone(field)
        assert field == '4.00'

    def test_patch_work_date(self):
        patch_data = copy.copy(self.patch_data)
        patch_data['work_date'] = (timezone.now() + timedelta(days=2)).strftime('%Y-%m-%d')
        response = self.user_client.patch(self.endpoint, patch_data, format='json')
        field = response.data.get('work_date', None)
        self.assertIsNotNone(field)
        assert field == patch_data.get('work_date')

    def test_patch_note(self):
        patch_data = copy.copy(self.patch_data)
        patch_data['note'] = 'a new note patch'
        response = self.user_client.patch(self.endpoint, patch_data, format='json')
        field = response.data.get('note', None)
        self.assertIsNotNone(field)
        assert field == patch_data.get('note')

    def test_patch_satisfaction(self):
        patch_data = copy.copy(self.patch_data)
        patch_data['satisfaction'] = 3
        response = self.user_client.patch(self.endpoint, patch_data, format='json')
        field = response.data.get('satisfaction', None)
        self.assertIsNotNone(field)
        assert field == patch_data.get('satisfaction')

    def test_permissions_patch_worklog(self):
        client_list = [self.hr_client, self.backoffice_client]
        for client in client_list:
            response = client.patch(self.endpoint, self.patch_data, format='json')
            self.assertEqual(response.status_code, 403)

    def test_user_cant_patch_worklog(self):
        self.project_activity_step.status = ACTIVITY_STATUS.DONE
        self.project_activity_step.save(update_fields=["status"])
        response = self.user_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_pm_permissions_to_patch(self):
        original_owner = self.project.project_owner
        self.project.project_owner = self.new_user
        self.project.save(update_fields=["project_owner"])
        patch_data = {
            'note': "New note"
        }
        response = self.new_user_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        # now let's check that PM can patch even in DONE status
        self.project_activity_step.status = ACTIVITY_STATUS.DONE
        self.project_activity_step.save(update_fields=["status"])
        response = self.new_user_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        # but can't on the billable
        self.project_activity_step.status = ACTIVITY_STATUS.BILLABLE
        self.project_activity_step.save(update_fields=["status"])
        response = self.new_user_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 403)

        # also we need to make sure, that PM cant patch original hours
        self.project_activity_step.status = ACTIVITY_STATUS.WORK
        self.project_activity_step.save(update_fields=["status"])
        previous_tracked_time = self.worklog.tracked_time
        patch_data = {
            "tracked_time": 5.00
        }
        response = self.new_user_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't change tracked_time or work_date"]
            }
        )
        self.worklog.refresh_from_db()
        self.assertEqual(self.worklog.tracked_time, previous_tracked_time)
        # but can step, satisfaction and note
        project_activity_step3 = ProjectActivityStepFactory(activity=self.project_activity)
        original_step = self.worklog.project_activity_step
        patch_data = {
            "satisfaction": 0,
            "note": "My new note",
            "project_activity_step": project_activity_step3.pk
        }
        response = self.new_user_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.worklog.refresh_from_db()
        self.assertEqual(self.worklog.satisfaction, patch_data["satisfaction"])
        self.assertEqual(self.worklog.note, patch_data["note"])
        self.assertEqual(self.worklog.project_activity_step.pk, patch_data["project_activity_step"])

        # revert our changes
        self.project.project_owner = original_owner
        self.project.save(update_fields=["project_owner"])
        self.worklog.project_activity_step = original_step
        self.worklog.save(update_fields=["project_activity_step"])

    def test_kam_permissions_to_patch(self):
        original_owner = self.project.company.key_account_manager
        self.project.company.key_account_manager = self.new_user
        self.project.company.save(update_fields=["key_account_manager"])
        patch_data = {
            'note': "New note"
        }
        response = self.new_user_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        # now let's check that KAM can patch even in DONE status
        self.project_activity_step.status = ACTIVITY_STATUS.DONE
        self.project_activity_step.save(update_fields=["status"])
        response = self.new_user_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        # but can't on the billable
        self.project_activity_step.status = ACTIVITY_STATUS.BILLABLE
        self.project_activity_step.save(update_fields=["status"])
        response = self.new_user_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 403)

        # also we need to make sure, that KAM cant patch original hours
        self.project_activity_step.status = ACTIVITY_STATUS.WORK
        self.project_activity_step.save(update_fields=["status"])
        previous_tracked_time = self.worklog.tracked_time
        patch_data = {
            "tracked_time": 5.00
        }
        response = self.new_user_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't change tracked_time or work_date"]
            }
        )
        self.worklog.refresh_from_db()
        self.assertEqual(self.worklog.tracked_time, previous_tracked_time)
        # but can step, satisfaction and note
        project_activity_step3 = ProjectActivityStepFactory(activity=self.project_activity)
        original_step = self.worklog.project_activity_step
        patch_data = {
            "satisfaction": 0,
            "note": "My new note",
            "project_activity_step": project_activity_step3.pk
        }
        response = self.new_user_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.worklog.refresh_from_db()
        self.assertEqual(self.worklog.satisfaction, patch_data["satisfaction"])
        self.assertEqual(self.worklog.note, patch_data["note"])
        self.assertEqual(self.worklog.project_activity_step.pk, patch_data["project_activity_step"])

        # revert our changes
        self.project.company.key_account_manager = original_owner
        self.project.company.save(update_fields=["key_account_manager"])
        self.worklog.project_activity_step = original_step
        self.worklog.save(update_fields=["project_activity_step"])

    def test_accounting_permission_to_patch(self):
        patch_data = {
            'note': "New note"
        }
        # now let's check that accounting can patch even in DONE status
        self.project_activity_step.status = ACTIVITY_STATUS.DONE
        self.project_activity_step.save(update_fields=["status"])
        response = self.accounting_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        # even on the billable
        self.project_activity_step.status = ACTIVITY_STATUS.BILLABLE
        self.project_activity_step.save(update_fields=["status"])
        response = self.accounting_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        # but not when closed
        self.project_activity_step.status = ACTIVITY_STATUS.CLOSED
        self.project_activity_step.save(update_fields=["status"])
        response = self.accounting_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 403)

        self.project_activity_step.status = ACTIVITY_STATUS.WORK
        self.project_activity_step.save(update_fields=["status"])
        # even accounting can't  change tracked time
        previous_tracked_time = self.worklog.tracked_time
        patch_data = {
            "tracked_time": 5.00
        }
        response = self.accounting_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't change tracked_time or work_date"]
            }
        )
        self.worklog.refresh_from_db()
        self.assertEqual(self.worklog.tracked_time, previous_tracked_time)
        # but can step, satisfaction and note
        project_activity_step3 = ProjectActivityStepFactory(activity=self.project_activity)
        original_step = self.worklog.project_activity_step
        patch_data = {
            "satisfaction": 0,
            "note": "My new note",
            "project_activity_step": project_activity_step3.pk
        }
        response = self.accounting_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.worklog.refresh_from_db()
        self.assertEqual(self.worklog.satisfaction, patch_data["satisfaction"])
        self.assertEqual(self.worklog.note, patch_data["note"])
        self.assertEqual(self.worklog.project_activity_step.pk, patch_data["project_activity_step"])
        self.worklog.project_activity_step = original_step
        self.worklog.save(update_fields=["project_activity_step"])

    def test_admin_permission_to_patch(self):
        patch_data = {
            'note': "New note"
        }
        # now let's check that accounting can patch even in DONE status
        self.project_activity_step.status = ACTIVITY_STATUS.DONE
        self.project_activity_step.save(update_fields=["status"])
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        # even on the billable
        self.project_activity_step.status = ACTIVITY_STATUS.BILLABLE
        self.project_activity_step.save(update_fields=["status"])
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        # but not when closed
        self.project_activity_step.status = ACTIVITY_STATUS.CLOSED
        self.project_activity_step.save(update_fields=["status"])
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        self.assertEqual(response.status_code, 403)

        self.project_activity_step.status = ACTIVITY_STATUS.WORK
        self.project_activity_step.save(update_fields=["status"])
        # even accounting can't  change tracked time
        previous_tracked_time = self.worklog.tracked_time
        patch_data = {
            "tracked_time": 5.00
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't change tracked_time or work_date"]
            }
        )
        self.worklog.refresh_from_db()
        self.assertEqual(self.worklog.tracked_time, previous_tracked_time)
        # but can step, satisfaction and note
        project_activity_step3 = ProjectActivityStepFactory(activity=self.project_activity)
        original_step = self.worklog.project_activity_step
        patch_data = {
            "satisfaction": 0,
            "note": "My new note",
            "project_activity_step": project_activity_step3.pk
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.worklog.refresh_from_db()
        self.assertEqual(self.worklog.satisfaction, patch_data["satisfaction"])
        self.assertEqual(self.worklog.note, patch_data["note"])
        self.assertEqual(self.worklog.project_activity_step.pk, patch_data["project_activity_step"])
        self.worklog.project_activity_step = original_step
        self.worklog.save(update_fields=["project_activity_step"])

    def test_not_own_permission_to_patch(self):
        response = self.new_user_client.patch(self.endpoint, self.patch_data, format='json')
        self.assertEqual(response.status_code, 403)


class TestUserWorkLogDelete(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_activity_step = ProjectActivityStepFactory(activity=cls.project_activity)
        cls.worker = cls.test_user
        cls.assignment = ActivityAssignmentFactory(
            worker=cls.worker, project_activity_step=cls.project_activity_step)
        cls.new_user = UserFactory(role="user", password=cls.default_password)
        cls.verify_user_email(cls.new_user)
        cls.new_user_client = cls.client_class()
        cls.login_as_user(client=cls.new_user_client, user=cls.new_user)

    def setUp(self):
        super().setUp()
        self.worklog = WorkLogFactory(project_activity_step=self.project_activity_step, worker=self.worker)
        self.endpoint = reverse('api_users:user-worklog-detail',
                                kwargs={'user_pk': self.worklog.worker.id, 'pk': self.worklog.pk})

    def test_own_delete(self):
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)

    def test_not_own_delete(self):
        response = self.new_user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)

    def test_delete_permissions(self):
        client_list = [self.hr_client, self.backoffice_client, self.accounting_client, self.admin_client]
        for client in client_list:
            response = client.delete(self.endpoint)
            self.assertEqual(response.status_code, 403)

    def test_own_delete_forbidden(self):
        self.project_activity_step.status = ACTIVITY_STATUS.DONE
        self.project_activity_step.save(update_fields=["status"])
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)


class TestWorkLogBase(MetronomBaseAPITestCase):
    """
    Base class to test the work logs list
    """
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.date_format = '%Y-%m-%d'
        cls.datetime_format = '%Y-%m-%d %H:%M'
        cls.endpoint = reverse('api_worklogs:worklog-list')

    def get_response(self, query_string):
        endpoint = f'{self.endpoint}?{query_string.urlencode()}'
        return self.user_client.get(endpoint)


class TestWorkLogList(TestWorkLogBase):
    """
    Test the work logs filters in different campaigns, projects, activities, and activity steps
    """
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.worker = UserFactory()
        cls.company1 = CompanyWithOfficeFactory()
        cls.company2 = CompanyWithOfficeFactory()
        cls.campaign1 = CampaignFactory(company=cls.company1, billing_currency="CHF")
        cls.campaign2 = CampaignFactory(company=cls.company2, billing_currency="CHF")
        cls.project1 = ProjectFactory(billing_currency="CHF", campaign=cls.campaign1, company=cls.company1)
        cls.project2 = ProjectFactory(billing_currency="CHF", campaign=cls.campaign2, company=cls.company2)
        cls.project3 = ProjectFactory(billing_currency="CHF", campaign=cls.campaign2, company=cls.company2)
        cls.activity1 = ProjectActivityFactory(project=cls.project3)
        cls.activity2 = ProjectActivityFactory(project=cls.project2)
        cls.activity3 = ProjectActivityFactory(project=cls.project1)
        cls.activity_step1 = ProjectActivityStepFactory(activity=cls.activity1)
        cls.activity_step2 = ProjectActivityStepFactory(activity=cls.activity1)
        cls.activity_step3 = ProjectActivityStepFactory(activity=cls.activity2)
        cls.activity_step4 = ProjectActivityStepFactory(activity=cls.activity3)
        cls.activity_step5 = ProjectActivityStepFactory(activity=cls.activity3)

        WorkLogFactory.create_batch(
            size=3, project_activity_step=cls.activity_step1, worker=cls.worker, satisfaction=1,
            work_date=timezone.now().today(),
        )
        WorkLogFactory.create_batch(
            size=4, project_activity_step=cls.activity_step2, worker=cls.test_admin, satisfaction=2,
            work_date=timezone.now().today() + timedelta(days=1)
        )
        WorkLogFactory.create_batch(
            size=5, project_activity_step=cls.activity_step3, worker=cls.worker, satisfaction=3,
            work_date=timezone.now().today() + timedelta(days=3)
        )
        WorkLogFactory.create_batch(
            size=6, project_activity_step=cls.activity_step3, worker=cls.test_admin, satisfaction=4,
            work_date=timezone.now().today() + timedelta(days=5)
        )
        WorkLogFactory.create_batch(
            size=7, project_activity_step=cls.activity_step4, worker=cls.worker, satisfaction=1,
            work_date=timezone.now().today() + timedelta(days=7)
        )
        WorkLogFactory.create_batch(
            size=8, project_activity_step=cls.activity_step5, worker=cls.test_admin, satisfaction=2,
            work_date=timezone.now().today() + timedelta(days=9)
        )

        cls.response = cls.user_client.get(cls.endpoint)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK
        assert isinstance(self.response.data, list)

    def test_all_work_logs(self):
        assert len(self.response.data) == WorkLog.objects.count()

    def test_user1_work_logs(self):
        response = self.get_response(QueryDict(f'user_uuid={self.worker.pk}'))
        assert len(response.data) == WorkLog.objects.filter(worker=self.worker).count()

    def test_user2_work_logs(self):
        response = self.get_response(QueryDict(f'user_uuid={self.test_admin.pk}'))
        assert len(response.data) == WorkLog.objects.filter(worker=self.test_admin).count()

    def test_activity_step_work_logs(self):
        for activity_step in [
            self.activity_step1, self.activity_step2, self.activity_step3, self.activity_step4, self.activity_step5
        ]:
            response = self.get_response(QueryDict(f'project_activity_step_uuid={activity_step.pk}'))
            assert len(response.data) == WorkLog.objects.filter(
                project_activity_step=activity_step
            ).count()

    def test_activity_work_logs(self):
        for activity in [self.activity1, self.activity2]:
            response = self.get_response(QueryDict(f'project_activity_uuid={activity.pk}'))
            assert len(response.data) == WorkLog.objects.filter(
                project_activity_step__activity=activity
            ).count()

    def test_project_work_logs(self):
        for project in [self.project1, self.project2]:
            response = self.get_response(QueryDict(f'project_uuid={project.pk}'))
            assert len(response.data) == WorkLog.objects.filter(
                project_activity_step__activity__project=project
            ).count()

    def test_campaign_work_logs(self):
        for campaign in [self.campaign1, self.campaign2]:
            response = self.get_response(QueryDict(f'campaign_uuid={campaign.pk}'))
            projects = Project.objects.filter(campaign=campaign)
            queryset = WorkLog.objects.filter(project_activity_step__activity__project__in=projects)
            assert len(response.data) == queryset.count()

    def test_company_work_logs(self):
        for company in [self.company1, self.company2]:
            response = self.get_response(QueryDict(f'company_uuid={company.pk}'))
            campaigns = Campaign.objects.filter(company=company)
            projects = Project.objects.filter(company=company, campaign__in=campaigns)
            queryset = WorkLog.objects.filter(project_activity_step__activity__project__in=projects)
            assert len(response.data) == queryset.count()

    def test_satisfaction_work_logs(self):
        for satisfaction in [k for k, v in WORKLOGS_STATUS.CHOICES]:
            response = self.get_response(QueryDict(f'satisfaction={satisfaction}'))
            assert len(response.data) == WorkLog.objects.filter(satisfaction=satisfaction).count()

    def test_wrong_uuid(self):
        response = self.get_response(QueryDict(f'user_uuid=abcdhefc'))
        self.assertErrorResponse(
            response,
            {"__all__": ['badly formed hexadecimal UUID string']}
        )

    def test_wrong_satisfaction(self):
        response = self.get_response(QueryDict(f'satisfaction=happy'))
        self.assertErrorResponse(
            response,
            {"__all__": ["invalid literal for int() with base 10: 'happy'"]}
        )

    def test_work_date_work_logs(self):
        work_date = timezone.now().strftime(self.date_format)
        response = self.get_response(QueryDict(f'start_work_date={work_date}&end_work_date={work_date}'))
        assert len(response.data) == WorkLog.objects.filter(work_date=work_date).count()

    def test_work_date_range_work_logs(self):
        start_work_date = timezone.now().strftime(self.date_format)
        end_work_date = (timezone.now() + timedelta(days=6)).strftime(self.date_format)
        response = self.get_response(QueryDict(f'start_work_date={start_work_date}&end_work_date={end_work_date}'))
        assert len(response.data) == WorkLog.objects.filter(work_date__range=(start_work_date, end_work_date)).count()

    def test_wrong_work_date_range_work_logs(self):
        end_work_date = timezone.now().strftime('%Y-%m-%d')
        start_work_date = (timezone.now() + timedelta(days=6)).strftime(self.date_format)
        response = self.get_response(QueryDict(f'start_work_date={start_work_date}&end_work_date={end_work_date}'))
        self.assertErrorResponse(
            response,
            {"__all__": ['The "end work date" cannot be earlier than the "start work date"']}
        )

    def test_log_datetime_work_logs(self):
        log_datetime = timezone.now().strftime(self.datetime_format)
        response = self.get_response(QueryDict(f'start_log_datetime={log_datetime}&end_log_datetime={log_datetime}'))
        assert len(response.data) == WorkLog.objects.filter(created_at=log_datetime).count()

    def test_log_datetime_range_work_logs(self):
        start_log_datetime = timezone.now().strftime(self.datetime_format)
        end_log_datetime = (timezone.now() + timedelta(days=6)).strftime(self.datetime_format)
        response = self.get_response(
            QueryDict(f'start_log_datetime={start_log_datetime}&end_log_datetime={end_log_datetime}'))
        assert len(response.data) == WorkLog.objects.filter(
            created_at__range=(start_log_datetime, end_log_datetime)).count()

    def test_wrong_log_datetime_range_work_logs(self):
        end_log_datetime = timezone.now().strftime(self.datetime_format)
        start_log_datetime = (timezone.now() + timedelta(days=6)).strftime(self.datetime_format)
        response = self.get_response(
            QueryDict(f'start_log_datetime={start_log_datetime}&end_log_datetime={end_log_datetime}'))
        self.assertErrorResponse(
            response,
            {"__all__": ['The "end log datetime" cannot be earlier than the "start log datetime"']}
        )


class TestWorkLogFilterBackend(TestWorkLogBase):
    """
    Tests the functionality of WorkLogFilterBackend
    """
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def setUp(self):
        """
        Setup with 10 work logs for each of the next 10 days, with a total of 100 work logs
        """
        super().setUp()

        self.worker = UserFactory(first_name="John", last_name="Doe", username="john.doe")
        self.project = ProjectFactory(billing_currency="CHF")
        self.activity = ProjectActivityFactory(project=self.project)
        self.activity_step = ProjectActivityStepFactory(activity=self.activity)

        self.start_date = timezone.now().date()
        self.end_date = (timezone.now() + timedelta(days=9)).date()

        self.other_worker = UserFactory(first_name="Martina", last_name="Mustermann", username="mm")
        self.other_project = ProjectFactory(billing_currency="CHF")
        self.other_activity = ProjectActivityFactory(project=self.other_project)
        self.other_activity_step = ProjectActivityStepFactory(activity=self.other_activity)

        # Generate 10 Worklogs with different work_dates
        while self.start_date <= self.end_date:
            WorkLogFactory.create_batch(
                size=1, work_date=self.start_date, project_activity_step=self.activity_step, worker=self.worker
            )
            self.start_date += timedelta(days=1)
        self.queryset = WorkLog.objects.all()

        self.other_start_date = self.start_date - timedelta(days=1)

        # Generate 1 Worklog for another project + worker
        WorkLogFactory.create_batch(size=1, work_date=self.other_start_date, project_activity_step=self.other_activity_step, worker=self.other_worker)

    def test_work_log_count(self):
        """
        Test that a total of 100 work logs are present in database
        """

        response = self.user_client.get(self.endpoint)
        assert len(response.data) == 10 + 1

    def test_filter_only_on_start_date(self):
        """
        Test work logs from given 'start_date' till the end
        """
        start_date = (timezone.now() + timedelta(days=3)).strftime(self.date_format)
        response = self.get_response(QueryDict(f'start_work_date={start_date}'))

        assert len(response.data) == 7 + 1
        assert len(response.data) == self.queryset.filter(work_date__gte=start_date).count()

    def test_filter_only_on_end_date(self):
        """
        Test work logs from the begining till the 'end_date'
        """
        end_date = (timezone.now() + timedelta(days=3)).strftime(self.date_format)
        response = self.get_response(QueryDict(f'end_work_date={end_date}'))

        assert len(response.data) == 4
        assert len(response.data) == self.queryset.filter(work_date__lte=end_date).count()

    def test_identical_start_end_dates(self):
        """
        Test work logs on a same start and end date
        """
        start_date = (timezone.now() + timedelta(days=4)).strftime(self.date_format)
        end_date = (timezone.now() + timedelta(days=4)).strftime(self.date_format)
        response = self.get_response(QueryDict(f'start_work_date={start_date}&end_work_date={end_date}'))

        assert len(response.data) == 1
        assert len(response.data) == self.queryset.filter(work_date__range=(start_date, end_date)).count()

    def test_different_start_end_dates(self):
        """
        Test work logs on a date range which is a subset of the original date range in setup method
        """
        start_date = (timezone.now() + timedelta(days=2)).strftime(self.date_format)
        end_date = (timezone.now() + timedelta(days=7)).strftime(self.date_format)
        response = self.get_response(QueryDict(f'start_work_date={start_date}&end_work_date={end_date}'))

        assert len(response.data) == 6
        assert len(response.data) == self.queryset.filter(work_date__range=(start_date, end_date)).count()

    def test_project_uuid_filtering_on_worklogs(self):
        """
        Filter by project uuid
        """

        response = self.get_response(QueryDict(f'projects={self.project.id}'))
        assert len(response.data) == 10
        assert len(response.data) == self.queryset.filter(project_activity_step__activity__project=self.project).count()

        response = self.get_response(QueryDict(f'projects={self.other_project.id}'))
        assert len(response.data) == 1

    def test_project_activity_uuid_filtering_on_worklogs(self):
        """
        Filter by project uuid
        """

        response = self.get_response(QueryDict(f'project_activities={self.activity.id}'))
        assert len(response.data) == 10
        assert len(response.data) == self.queryset.filter(project_activity_step__activity=self.activity).count()

        response = self.get_response(QueryDict(f'project_activities={self.other_activity.id}'))
        assert len(response.data) == 1

    def test_project_activity_step_uuid_filtering_on_worklogs(self):
        """
        Filter by project uuid
        """

        response = self.get_response(QueryDict(f'project_activity_steps={self.activity_step.id}'))
        assert len(response.data) == 10
        assert len(response.data) == self.queryset.filter(project_activity_step=self.activity_step).count()

        response = self.get_response(QueryDict(f'project_activity_steps={self.other_activity_step.id}'))
        assert len(response.data) == 1

    def test_project_filtering_on_worklogs(self):
        """ Filter by project, can be title, public_id or the first part of the uuid """

        response = self.get_response(QueryDict(f'projects=none'))
        assert len(response.data) == 0

        response = self.get_response(QueryDict(f'projects={self.project.title}'))
        assert len(response.data) == 10

        response = self.get_response(QueryDict(f'projects={self.project.public_id}'))
        assert len(response.data) == 10

        response = self.get_response(QueryDict(f'projects={str(self.project.id)[:8]}'))
        assert len(response.data) == 10

        response = self.get_response(QueryDict(f'projects={self.other_project.title}'))
        assert len(response.data) == 1

        response = self.get_response(QueryDict(f'projects={self.other_project.public_id}'))
        assert len(response.data) == 1

        response = self.get_response(QueryDict(f'projects={str(self.other_project.id)[:8]}'))
        assert len(response.data) == 1

    def test_worker_filtering_on_worklogs(self):
        """ Filter by project, can be title, public_id or the first part of the uuid """

        response = self.get_response(QueryDict(f'workers=notexisting'))
        assert len(response.data) == 0

        response = self.get_response(QueryDict(f'workers={self.worker.first_name}'))
        assert len(response.data) == 10

        response = self.get_response(QueryDict(f'workers={self.worker.last_name}'))
        assert len(response.data) == 10

        response = self.get_response(QueryDict(f'workers={self.worker.person_in_crm.public_id}'))
        assert len(response.data) == 10

        response = self.get_response(QueryDict(f'workers={self.worker.username}'))
        assert len(response.data) == 10

        response = self.get_response(QueryDict(f'workers={self.other_worker.first_name}'))
        assert len(response.data) == 1

        response = self.get_response(QueryDict(f'workers={self.other_worker.last_name}'))
        assert len(response.data) == 1

        response = self.get_response(QueryDict(f'workers={self.other_worker.person_in_crm.public_id}'))
        assert len(response.data) == 1

        response = self.get_response(QueryDict(f'workers={self.other_worker.username}'))
        assert len(response.data) == 1

    def test_project_activity_filtering_on_worklogs(self):
        """ Filter by project, can be title, public_id or the first part of the uuid """

        response = self.get_response(QueryDict(f'project_activities=none'))
        assert len(response.data) == 0

        response = self.get_response(QueryDict(f'project_activities={self.activity.name}'))
        assert len(response.data) == 10

        response = self.get_response(QueryDict(f'project_activities={str(self.activity.id)[:8]}'))
        assert len(response.data) == 10

        response = self.get_response(QueryDict(f'project_activities={self.other_activity.name}'))
        assert len(response.data) == 1

        response = self.get_response(QueryDict(f'project_activities={str(self.other_activity.id)[:8]}'))
        assert len(response.data) == 1

    def test_project_activity_step_filtering_on_worklogs(self):
        """ Filter by project, can be title, public_id or the first part of the uuid """

        response = self.get_response(QueryDict(f'project_activity_steps=none'))
        assert len(response.data) == 0

        response = self.get_response(QueryDict(f'project_activity_steps={self.activity_step.name}'))
        assert len(response.data) == 10

        response = self.get_response(QueryDict(f'project_activity_steps={str(self.activity_step.id)[:8]}'))
        assert len(response.data) == 10

        response = self.get_response(QueryDict(f'project_activity_steps={self.other_activity_step.name}'))
        assert len(response.data) == 1

        response = self.get_response(QueryDict(f'project_activity_steps={str(self.other_activity_step.id)[:8]}'))
        assert len(response.data) == 1
