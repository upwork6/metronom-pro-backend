from django.contrib.contenttypes.models import ContentType
from rest_framework.decorators import detail_route

from .views import CommentViewSet


class CommentsMixin:
    """Comments by a user to an Activity-Stream"""

    @detail_route(methods=['get', 'post'], url_path='comments')
    def comments(self, request, *args, **kwargs):
        """Comments of the user"""
        kwargs['content_type'] = ContentType.objects.get_for_model(self.queryset.model)
        if request.method == 'GET':
            return CommentViewSet.as_view({'get': 'list'})(request, *args, **kwargs)
        elif request.method == 'POST':
            return CommentViewSet.as_view({'post': 'create'})(request, *args, **kwargs)
