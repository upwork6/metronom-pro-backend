from django.contrib import admin

from apps.bankaccounts.models import Bank, BankAccount


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ('id', 'bank_name', 'account_name', 'owner_name', 'bic', 'iban')
    list_filter = ('bic', 'bank_name')


@admin.register(Bank)
class BankAdmin(admin.ModelAdmin):
    list_display = ('id', 'country_code', 'bank_name', 'bank_code', 'bic')
    list_filter = ('bank_name', 'bank_code', 'bic')
    search_fields = ('country_code', 'bank_name', 'bank_code', 'bic')
