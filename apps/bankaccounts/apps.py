from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class BankAccountsConfig(AppConfig):
    name = 'apps.bankaccounts'
    verbose_name = _('Bank accounts')
    bank_standard_countries = ['DE', 'CH']
