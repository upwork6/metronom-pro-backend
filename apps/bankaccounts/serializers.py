from django.core.cache import caches
from internationalflavor.iban.validators import BICValidator, IBANValidator
from rest_framework import serializers

from .apps import BankAccountsConfig
from .models import Bank, BankAccount

cache = caches['default']


class BankAccountThinSerializer(serializers.ModelSerializer):
    """Serialize only account_name from the BankAccount."""
    public_name = serializers.SerializerMethodField(read_only=True)

    def get_public_name(self, obj):
        return obj.public_name

    class Meta:
        model = BankAccount
        fields = (
            'id',
            'public_name',
        )


class AbstractBankAccountSerializer(serializers.ModelSerializer):
    """ This is abstract serializer for the BankAccount and backoffice.AgencyBankAccount models
    """
    bic = serializers.CharField(validators=[
        BICValidator(),
    ])
    iban = serializers.CharField(validators=[
        IBANValidator(countries=BankAccountsConfig.bank_standard_countries),
    ])
    public_name = serializers.SerializerMethodField(read_only=True)

    class Meta:
        fields = [
            'id',
            'bank_name',
            'account_name',
            'owner_name',
            'bic',
            'iban',
            'public_name',
        ]

    def get_public_name(self, obj):
        return obj.public_name

    @classmethod
    def many_init(cls, *args, **kwargs):
        kwargs['child'] = BankAccountThinSerializer()
        return serializers.ListSerializer(*args, **kwargs)


class BankAccountSerializer(AbstractBankAccountSerializer):
    """Serialize data from the BankAccount."""

    class Meta(AbstractBankAccountSerializer.Meta):
        model = BankAccount


class CheckIBANSerializer(serializers.Serializer):
    """
    Serialize data from the BankAccount.
    """
    iban = serializers.CharField(validators=[
        IBANValidator(countries=BankAccountsConfig.bank_standard_countries),
    ], required=True)


class BankSerializer(serializers.ModelSerializer):
    """
    Serialize data from Bank Model
    """
    class Meta:
        model = Bank
        fields = [
            'bank_name',
            'bank_code',
            'bic',
        ]
