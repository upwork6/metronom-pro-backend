from collections import OrderedDict

from ddt import data, ddt, unpack
from django.core.cache import caches
from rest_framework import status
from rest_framework.reverse import reverse
from schwifty import IBAN

from apps.bankaccounts.models import Bank, BankAccount
from apps.bankaccounts.tests.factories import BankFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase

cache = caches['default']


class TestBankAccountViewSet(MetronomBaseAPITestCase):

    def _create_bank_accounts(self):
        self.accounts = []
        valid_iban_list = [
            'DE44500105175407324931',
            'DE89370400440532013000',
        ]
        i = 1
        for iban in valid_iban_list:
            account = BankAccount(
                bank_name='Bank %s' % i,
                account_name='Private%s' % i,
                owner_name='Owner %s' % i,
                bic='AAAAGE23000',
                iban=iban
            )
            self.accounts.append(account)
            i += 1
        BankAccount.objects.bulk_create(self.accounts)

    def _get_right_structure_of_list_accounts(self, accounts):
        right_list = [OrderedDict([
            ('id', str(x.id)),
            ('public_name', x.public_name),
        ]) for x in accounts]
        return right_list

    def test_get_method_is_not_allowed(self):
        url = reverse('api:bankaccount-list')
        response = self.user_client.get(url)
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_creation(self):
        url = reverse('api:bankaccount-list')
        valid_data = {
            'bank_name': 'Bank 10',
            'account_name': 'Private',
            'owner_name': 'Owner 10',
            'bic': 'AAAAGE23000',
            'iban': 'DE89370400440532013000',
        }
        response = self.user_client.post(url, valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BankAccount.objects.count(), 1)
        self.assertEqual(BankAccount.objects.last().account_name, 'Private')

        invalid_data = dict.copy(valid_data)
        invalid_data.update({
            'bic': '000',
        })
        response = self.user_client.post(url, invalid_data, format='json')
        self.assertEqual(BankAccount.objects.count(), 1)
        self.assertErrorResponse(response, {'bic': ['This Bank Identifier Code (BIC) is invalid.']})

    def test_update(self):
        self._create_bank_accounts()
        account = BankAccount.objects.all().first()
        url = reverse('api:bankaccount-detail', kwargs={'id': account.id})

        data = {
            'bank_name': account.bank_name,
            'account_name': 'Business',
            'owner_name': account.owner_name,
            'bic': account.bic,
            'iban': account.iban,
        }
        response = self.user_client.patch(url, data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_deleting(self):
        self._create_bank_accounts()
        url = reverse('api:bankaccount-detail', kwargs={'id': self.accounts[0].id})
        response = self.user_client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertTrue(BankAccount.objects.filter(id=self.accounts[0].id).exists())

        response = self.admin_client.delete(url)
        self.assertEqual(response.status_code, 204)
        self.assertFalse(BankAccount.objects.filter(id=self.accounts[0].id).exists())


@ddt
class TestCheckIBAN(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api_helpers:check_iban')

    @unpack
    @data(
        {'iban': 'DE89370400440532013000', 'bic': 'COBADEFFXXX'},
        {'iban': 'CH3908704016075473007', 'bic': 'AEKTCH22XXX'}
    )
    def test_check_iban_correct(self, iban, bic):
        iban = IBAN(iban)
        self.bank = BankFactory(country_code=iban.country_code, bank_code=iban.bank_code, bic=bic)
        post_data = {'iban': str(iban)}

        response = self.user_client.post(self.endpoint, post_data)
        self.assertEqual(response.data['bic'], bic)

    def test_check_iban_incorrect(self):
        response = self.user_client.post(self.endpoint, {'iban': 'incorrect_iban'})
        self.assertEqual(
            response.data['error_message'],
            'This IBAN does not start with a country code and checksum, or contains invalid characters.'
        )

    def test_check_iban_required(self):
        response = self.user_client.post(self.endpoint)
        self.assertEqual(response.data['error_message'], 'This field is required.')

    def test_check_iban_not_blank(self):
        response = self.user_client.post(self.endpoint, {'iban': ''})
        self.assertEqual(response.data['error_message'], 'This field may not be blank.')

    @unpack
    @data(
        {'iban': 'DE89370400440532013000'}
    )
    def test_check_no_bank_for_iban(self, iban):
        Bank.objects.all().delete()
        response = self.user_client.post(self.endpoint, {'iban': iban})
        self.assertEqual(response.data['error_message'], 'This IBAN does not have a Bank')
