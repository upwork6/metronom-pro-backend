from django.test import TestCase
from django.urls import resolve, reverse


class TestBankAccountURLs(TestCase):
    def test_contact_base_reverse(self):
        assert reverse('api:bankaccount-list') == '/api/bankaccounts/'

    def test_contact_base_resolve(self):
        assert resolve('/api/bankaccounts/').view_name == 'api:bankaccount-list'
