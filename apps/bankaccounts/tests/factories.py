import factory

from ..models import Bank, BankAccount


class BankFactory(factory.DjangoModelFactory):
    country_code = factory.Iterator(['DE', 'CH'])
    bank_name = factory.Iterator(["Credit Suisse", "UBS", "Deutsche Bank", "Commerzbank", "Postbank"])
    bank_code = factory.Sequence(lambda n: '{:05d}'.format(n))
    bic = factory.Sequence(lambda n: 'AEKTCH2{:04d}'.format(n))

    class Meta:
        model = Bank


class BankAccountFactory(factory.DjangoModelFactory):

    account_name = factory.Iterator(["Privatkonto", "Standard-Konto", "Neues Konto"])
    bank_name = factory.SubFactory(BankFactory)
    bic = factory.Sequence(lambda n: 'AEKTCH2{:04d}'.format(n))
    iban = factory.Sequence(lambda n: 'CH4450010517540732493{0}'.format(n))

    class Meta:
        model = BankAccount
