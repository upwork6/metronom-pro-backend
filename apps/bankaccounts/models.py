from django.db import models
from django.utils.translation import ugettext_lazy as _
from internationalflavor.iban import BICField, IBANField

from apps.bankaccounts.apps import BankAccountsConfig
from apps.metronom_commons.models import UserAccessMetronomBaseModel, BackofficeBaseModel


class AbstractBankAccountModel(models.Model):
    bank_name = models.CharField(max_length=150, verbose_name=_('Bank name'))
    account_name = models.CharField(
        max_length=150,
        verbose_name=_('Account name'),
        blank=True,
        default='',
    )
    owner_name = models.CharField(max_length=150, blank=True, default='', verbose_name=_('Owner name'))
    bic = BICField(verbose_name=_("BIC"))
    iban = IBANField(countries=BankAccountsConfig.bank_standard_countries, verbose_name=_("IBAN"))

    class Meta:
        abstract = True

    def __str__(self):
        return f'{self.public_name}'

    @property
    def public_name(self):
        name = self.account_name if self.account_name else self.bank_name
        public_name = '{name} (xxxx{last_4_numbers_of_iban})'.format(
            name=name,
            last_4_numbers_of_iban=str(self.iban)[-4:],
        )
        return public_name


class BankAccount(BackofficeBaseModel, AbstractBankAccountModel):
    class Meta:
        verbose_name = _('Bank account')
        verbose_name_plural = _('Bank accounts')


class Bank(BackofficeBaseModel):
    country_code = models.CharField(max_length=2, verbose_name=_('Country code'), default='')
    bank_name = models.CharField(max_length=150, verbose_name=_('Bank name'))
    bank_code = models.CharField(verbose_name=_('Bank code'), max_length=150, blank=True)
    bic = BICField(max_length=150, verbose_name=_("BIC"), unique=True)

    class Meta:
        verbose_name = _('Bank')
        verbose_name_plural = _('Banks')

    def __str__(self):
        return f'{self.bank_name}'
