from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from schwifty import IBAN

from apps.bankaccounts.models import Bank, BankAccount
from apps.bankaccounts.serializers import (BankAccountSerializer,
                                           BankSerializer, CheckIBANSerializer)


class BankAccountViewSet(ModelViewSet):
    """Bank Accounts of companies or users"""
    lookup_url_kwarg = 'id'
    queryset = BankAccount.objects.all().order_by("bank_name")
    serializer_class = BankAccountSerializer
    http_method_names = ['post', 'patch', 'delete', 'head', 'options', 'trace']

    def get_queryset(self):
        # TODO Will be added the filtering logic by user permission.
        queryset = BankAccount.objects.all()
        return queryset


class CheckIBAN(CreateAPIView):
    """
    IBAN validation check + Bank & BIC calculation

    Received the IBAN:  {'iban': 'DE12500105170648489890'}

    """
    serializer_class = CheckIBANSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            error_message = {
                'error_message': serializer.errors['iban'][0]
            }
            return Response(error_message, status=status.HTTP_400_BAD_REQUEST)

        iban = IBAN(serializer.initial_data['iban'])
        try:
            bank = Bank.objects.get(country_code=iban.country_code, bank_code=iban.bank_code)
            serializer = BankSerializer(instance=bank)

            return Response(serializer.data, status=status.HTTP_200_OK)
        except Bank.DoesNotExist:
            error_message = {
                'error_message': 'This IBAN does not have a Bank'
            }
            return Response(error_message, status=status.HTTP_400_BAD_REQUEST)
