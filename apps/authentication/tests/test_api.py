
import pytest

from django.urls import reverse
from rest_framework_jwt.settings import api_settings

from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.profile_images.tests.factories import ProfileImageFactory
from apps.users.models import ROLE
from apps.users.tests.factories import UserFactory


class TestJWT(MetronomBaseAPITestCase):

    def test_image_is_None(self):
        profile = self.test_user.profile
        profile.image = None
        profile.save()

        self.login_as_user()

        self.assertTrue(self.login_response)
        self.assertFalse(self.login_response.data['user']['has_image'])

    def test_profile_is_None(self):
        user = self.test_user
        user.profile = None
        user.save()

        self.login_as_user()
        self.assertTrue(self.login_response)

        self.assertFalse(self.login_response.data['user']['has_image'])

    def test_login_successful(self):
        person = self.test_user.person_in_crm
        person.image = ProfileImageFactory()
        person.image.save()
        person.save()

        self.login_as_user()
        self.assertTrue(self.login_response)

        # print(self.login_response.data)
        self.assertTrue(self.login_response.data['user']['has_image'])

    @pytest.mark.skip(reason="Cant make workable for now")
    def test_decoding_of_jwt_without_login_response(self):
        with self.assertRaises(AssertionError):
            self.decode_jwt()

    def test_decoding_of_jwt(self):
        self.login_as_user()
        assert type(self.decode_jwt()) is dict

    def test_jwt_basics(self):
        self.login_as_user()
        data = self.decode_jwt()
        assert data['iss'] == 'metronom-pro'

    @classmethod
    def check_user_response(cls, user, expected_is_superuser, expected_role):

        cls.login_as_user(user=user)

        assert cls.login_response.status_code == 200
        data = cls.decode_jwt()

        assert data['user_uuid'] == str(user.id)
        assert data['person_id'] == str(user.person_in_crm.public_id)
        assert data['email'] == user.email
        assert data['role'] == user.role
        assert data['sub'] == user.name
        assert data['is_superuser'] is expected_is_superuser
        assert data['role'] == expected_role

    def test_sign_in_as_user(self):
        self.check_user_response(self.test_user, False, ROLE.USER)

    def test_sign_in_as_admin(self):
        self.check_user_response(self.test_admin, True, ROLE.ADMIN)

    def test_sign_in_as_accounting(self):
        self.check_user_response(self.test_accounting_user, False, ROLE.ACCOUNTING)

    def test_sign_in_as_hr(self):
        self.check_user_response(self.test_hr_user, False, ROLE.HR)

    def test_sign_in_as_backoffice(self):
        self.check_user_response(self.test_backoffice_user, False, ROLE.BACKOFFICE)

    def test_malformed_jwt_token(self):
        new_client = self.client_class()
        self.login_as_user(new_client)
        new_client.credentials(
            HTTP_AUTHORIZATION="{0} {1}".format(api_settings.JWT_AUTH_HEADER_PREFIX,
                                                self.login_response.data['token'][:-5])
        )
        test_url = reverse('api:project-list')
        response = new_client.get(test_url)
        self.assertErrorResponse(
            response,
            {
                "detail": 'Error decoding signature.'
            },
            expected_status_code=401
        )
        self.login_as_user()
        test_user = UserFactory(email='user2@test.com', first_name="John",
                                password=self.default_password, role=ROLE.USER)
        self.verify_user_email(test_user)
        self.login_as_user(new_client, user=test_user)
        test_user.delete()
        response = new_client.get(test_url)
        self.assertErrorResponse(
            response,
            {
                "detail": 'Error decoding signature.'
            },
            expected_status_code=401
        )
