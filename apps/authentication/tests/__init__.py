import collections

from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.utils import jwt_decode_handler

from allauth.account.models import EmailAddress

from apps.users.tests.factories import UserFactory
from apps.users.models import ROLE

User = get_user_model()


class APIRestAuthJWTClient(APIClient):

    def login(self, login_name='rest_login', **credentials):
        """
        Returns True if login is possible; False if the provided credentials
        are incorrect, or the user is inactive.
        """
        login_endpoint = reverse(login_name)
        login_response = self.post(login_endpoint, credentials, format='json')
        if login_response.status_code == status.HTTP_200_OK:
            self.credentials(
                HTTP_AUTHORIZATION="{0} {1}".format(api_settings.JWT_AUTH_HEADER_PREFIX, login_response.data['token']))
            return login_response
        else:
            return None


class MetronomBaseAPITestCase(APITestCase):
    """ Adds authenticated by admin method to test classes """

    client_class = APIRestAuthJWTClient

    @classmethod
    def verify_user_email(cls, user):
        verification, created = EmailAddress.objects.get_or_create(user=user, email=user.email)
        verification.verified = True
        verification.set_as_primary()
        verification.save()

    @classmethod
    def setUpTestData(cls):
        """
            Load initial data for the TestCase
        """

        # Some defaults
        cls.default_password = 'secret123'

        # Create test users
        cls.test_user = UserFactory(email='user@test.com', first_name="John",
                                    password=cls.default_password, role=ROLE.USER)
        cls.test_hr_user = UserFactory(email='hr_user@test.com', first_name="John",
                                       password=cls.default_password, role=ROLE.HR)
        cls.test_accounting_user = UserFactory(email='accounting_user@test.com',
                                               first_name="John", password=cls.default_password, role=ROLE.ACCOUNTING)
        cls.test_backoffice_user = UserFactory(email='backoffice_user@test.com',
                                               first_name="John",  password=cls.default_password, role=ROLE.BACKOFFICE)

        cls.test_admin = get_user_model().objects.create_superuser(
            username="admin",
            first_name="John",
            email="admin@test.com",
            password=cls.default_password,
        )

        # Verification of the Users

        cls.verify_user_email(cls.test_user)
        cls.verify_user_email(cls.test_hr_user)
        cls.verify_user_email(cls.test_accounting_user)
        cls.verify_user_email(cls.test_backoffice_user)

    def setUp(self):
        """
            Running tests will modify class attributes. We need to reload attributes, which could have been modified in tests.
            As those are read operations, it still has a better performance.
        """
        self.test_user.refresh_from_db()
        # TODO: Is this still needed?

    @classmethod
    def login_as_user(cls, user=None):

        if not user:
            user = cls.test_user

        credentials = {"email": user.email, "password": cls.default_password}
        cls.login_response = cls.client.login(**credentials)

        print(cls.login_response)

    @classmethod
    def login_as_hr_user(cls):
        cls.login_as_user(cls.test_hr_user)

    @classmethod
    def login_as_accounting_user(cls):
        cls.login_as_user(cls.test_accounting_user)

    @classmethod
    def login_as_backoffice_user(cls):
        cls.login_as_user(cls.test_backoffice_user)

    @classmethod
    def login_as_admin(cls):
        cls.login_as_user(cls.test_admin)

    @classmethod
    def decode_jwt(cls):

        if not hasattr(cls, 'login_response'):
            raise AssertionError('You must call a login-method before accessing the decoded JWT')

        decoded_data = jwt_decode_handler(cls.login_response.data['token'])
        print(decoded_data)

        return decoded_data
