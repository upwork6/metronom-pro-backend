from django.conf.urls import url
from rest_auth.views import (LoginView, LogoutView, PasswordChangeView,
                             PasswordResetConfirmView, PasswordResetView)

urlpatterns = [
    # Former Authentication service, TODO: understand if they are still needed, or still work

    # URLs that do not require a session or valid token
    url(r'^password/reset/$',
        PasswordResetView.as_view(),
        name='rest_password_reset'),

    url(r'^password/reset/confirm/$',
        PasswordResetConfirmView.as_view(),
        name='rest_password_reset_confirm'),

    url(r'^login/$', LoginView.as_view(), name='rest_login'),

    # URLs that require a user to be logged in with a valid session / token.
    url(r'^logout/$', LogoutView.as_view(), name='rest_logout'),

    url(r'^password/change/$',
        PasswordChangeView.as_view(),
        name='rest_password_change'),

    # TODO: #50
    # url(r'^sign-up/', include('apps.tenants.registration.urls')),
]
