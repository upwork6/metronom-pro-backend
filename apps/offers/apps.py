from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class OffersConfig(AppConfig):
    name = 'apps.offers'
    verbose_name = _('Offers')