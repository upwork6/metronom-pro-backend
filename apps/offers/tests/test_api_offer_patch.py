from decimal import Decimal

import pytest
from ddt import data, ddt, unpack
from django.core.exceptions import ValidationError
from django.urls import reverse
from rest_framework import status

from apps.backoffice.tests.factories import TermsCollectionFactory
from apps.offers.models import OfferStatus, Offer
from apps.offers.tests.test_api_offer_get import OfferBaseClass


@ddt
class TestOfferPatch(OfferBaseClass):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.patched_data = {
            "name": "New Offer Name Patched",
            "offer_date": "2018-01-10",
            "offer_description": "description patched for this Offer",
            "valid_until_date": "2018-03-16",
            "callback_date": "2018-01-20",
            "contact_person": str(cls.contact_person2.id),
            "offer_manager": str(cls.project_owner2.id),
            "is_tax_included": False,
            "activities": [
                {
                    "position": 6,
                    "show_steps": True,
                    "is_optional": True,
                    "offered_sum": str(Decimal(1000)),
                    "has_subtotal": False,
                    "discount": str(Decimal(350)),
                },
                {
                    "position": 7,
                    "show_steps": False,
                    "is_optional": False,
                    "offered_sum": str(Decimal(1500)),
                    "has_subtotal": True,
                    "accepted_offer": None,
                    "discount": str(Decimal(150)),
                }
            ],
            "articles": [
                {
                    "position": 8,
                    "is_external_cost": False,
                    "is_optional": False,
                    "offered_sum": str(Decimal(5000)),
                    "has_subtotal": True,
                    "accepted_offer": {"uuid": 12345678 - 12345678 - 12345678},
                    "discount": str(Decimal(500)),
                },
                {
                    "position": 9,
                    "is_external_cost": False,
                    "is_optional": False,
                    "offered_sum": str(Decimal(2500)),
                    "has_subtotal": True,
                    "accepted_offer": {"uuid": "1234-1234"},
                    "discount": str(Decimal(500)),
                }
            ]
        }
        cls.offers_url = reverse('api_projects:offer-list', args=[cls.project.id])
        cls.response = cls.admin_client.post(
            cls.offers_url,
            data=cls.data,
            format='json'
        )
        cls.new_offers_url = reverse(
            'api_projects:offer-detail', args=[cls.project.id, cls.response.data['uuid']]
        )
        cls.get_response = cls.admin_client.get(
            cls.new_offers_url,
            format='json'
        )
        cls.patch_offer_list_and_post_page = cls.admin_client.patch(
            cls.offers_url,
            data={"name": cls.patched_data['name']}
        )
        cls.offer = Offer.objects.get(id=cls.response.data['uuid'])

    def test_get_response_from_post(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_get_response_instead_patch(self):
        assert self.get_response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_try_patch_title_on_get_list_and_post_page(self):
        self.assertErrorResponse(
            self.patch_offer_list_and_post_page,
            {'detail': 'Method "PATCH" not allowed.'},
            expected_status_code=405
        )

    def test_patch_title_response(self):
        data_to_patch = {"name": self.patched_data["name"]}
        patch_title_response = self.admin_client.patch(self.new_offers_url, data=data_to_patch)
        assert patch_title_response.data['uuid'] == self.response.data['uuid']
        assert patch_title_response.data['name'] == self.patched_data["name"]

    def test_patch_offer_date(self):
        patch_offer_date = {"offer_date": self.patched_data["offer_date"]}
        patch_response = self.admin_client.patch(self.new_offers_url, data=patch_offer_date)
        assert patch_response.data['uuid'] == self.response.data['uuid']
        assert patch_response.data['offer_date'] == self.patched_data["offer_date"]

    def test_patch_offer_description(self):
        patch_offer_description = {"offer_description": self.patched_data["offer_description"]}
        patch_response = self.admin_client.patch(self.new_offers_url, data=patch_offer_description)
        assert patch_response.data['uuid'] == self.response.data['uuid']
        assert patch_response.data['offer_description'] == self.patched_data["offer_description"]

    def test_patch_valid_until_date(self):
        patch_valid_until_date = {"valid_until_date": self.patched_data["valid_until_date"]}
        patch_response = self.admin_client.patch(self.new_offers_url, data=patch_valid_until_date)
        assert patch_response.data['uuid'] == self.response.data['uuid']
        assert patch_response.data['valid_until_date'] == self.patched_data["valid_until_date"]

    def test_patch_callback_date(self):
        patch_callback_date = {"callback_date": self.patched_data["callback_date"]}
        patch_response = self.admin_client.patch(self.new_offers_url, data=patch_callback_date)
        assert patch_response.data['uuid'] == self.response.data['uuid']
        assert patch_response.data['callback_date'] == self.patched_data["callback_date"]

    def test_patch_contact_person(self):
        patch_contact_person = {"contact_person": self.patched_data["contact_person"]}
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_contact_person,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        assert patch_response.data['contact_person']["uuid"] == self.patched_data["contact_person"]

    def test_patch_offer_manager(self):
        patch_offer_manager = {"offer_manager": self.patched_data["offer_manager"]}
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_offer_manager,
            format='json'
        )

        assert patch_response.data['uuid'] == self.response.data['uuid']
        assert patch_response.data['offer_manager']['uuid'] == self.patched_data["offer_manager"]

    def test_patch_is_tax_included(self):
        patch_is_tax_included = {"is_tax_included": self.patched_data["is_tax_included"]}
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_is_tax_included,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        assert patch_response.data['is_tax_included'] == self.patched_data["is_tax_included"]

    def test_patch_activities_1_position(self):
        patch_activities_1 = {
            "activities": [
                {
                    "uuid": self.response.data['activities'][0]["uuid"],
                    "position": self.patched_data['activities'][0]["position"]
                }
            ]
        }
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_activities_1,
            format='json'
        )
        actual_response = 0
        assert patch_response.data['uuid'] == self.response.data['uuid']
        for activity in patch_response.data['activities']:
            if activity['uuid'] == patch_activities_1['activities'][0]["uuid"]:
                actual_response = activity["position"]
        expected = self.patched_data['activities'][0]["position"]
        assert actual_response == expected

    def test_patch_activities_1_is_optional(self):
        patch_activities_1_is_optional = {
            "activities": [
                {
                    "uuid": self.response.data['activities'][0]["uuid"],
                    "is_optional": self.patched_data['activities'][0]["is_optional"]
                }
            ]
        }
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_activities_1_is_optional,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        response = None
        for activity in patch_response.data['activities']:
            if activity['uuid'] == patch_activities_1_is_optional['activities'][0]["uuid"]:
                response = activity["is_optional"]
        expected = self.patched_data['activities'][0]["is_optional"]
        assert response == expected

    def test_patch_activities_1_show_steps(self):
        patch_activities_1_show_steps = {
            "activities": [
                {
                    "uuid": self.response.data['activities'][0]["uuid"],
                    "show_steps": self.patched_data['activities'][0]["show_steps"]
                }
            ]
        }
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_activities_1_show_steps,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        response = None
        for activity in patch_response.data['activities']:
            if activity['uuid'] == patch_activities_1_show_steps['activities'][0]["uuid"]:
                response = activity["show_steps"]
        expected = self.patched_data['activities'][0]["show_steps"]
        assert response == expected

    def test_patch_activities_1_has_subtotal(self):
        patch_activities_1_has_subtotal = {
            "activities": [
                {
                    "uuid": self.response.data['activities'][0]["uuid"],
                    "has_subtotal": self.patched_data['activities'][0]["has_subtotal"]
                }
            ]
        }
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_activities_1_has_subtotal,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        response = None
        for activity in patch_response.data['activities']:
            if activity['uuid'] == patch_activities_1_has_subtotal['activities'][0]["uuid"]:
                response = activity["has_subtotal"]
        expected = self.patched_data['activities'][0]["has_subtotal"]
        assert response == expected

    def test_patch_activities_1_delete(self):
        patch_activities_1_delete = {
            "activities": [
                {
                    "uuid": self.response.data['activities'][0]["uuid"],
                    "deleted": True
                }
            ]
        }
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_activities_1_delete,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        response = len(patch_response.data['activities'])
        expected = len(self.response.data['activities']) - 1
        assert response == expected

    def test_patch_articles_1_position(self):
        patch_articles_1 = {
            "articles": [
                {
                    "uuid": self.response.data['articles'][0]["uuid"],
                    "position": self.patched_data['articles'][0]["position"]
                }
            ]
        }
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_articles_1,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        response = None
        for article in patch_response.data['articles']:
            if article['uuid'] == patch_articles_1['articles'][0]["uuid"]:
                response = article["position"]
        expected = self.patched_data['articles'][0]["position"]
        assert response == expected

    def test_patch_articles_1_is_optional(self):
        patch_articles_1_is_optional = {
            "articles": [
                {
                    "uuid": self.response.data['articles'][0]["uuid"],
                    "is_optional": self.patched_data['articles'][0]["is_optional"]
                }
            ]
        }
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_articles_1_is_optional,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        response = None
        for article in patch_response.data['articles']:
            if article['uuid'] == patch_articles_1_is_optional['articles'][0]["uuid"]:
                response = article["is_optional"]
        expected = self.patched_data['articles'][0]["is_optional"]
        assert response == expected

    def test_patch_articles_1_is_external_cost(self):
        patch_articles_1_is_external_cost = {
            "articles": [
                {
                    "uuid": self.response.data['articles'][0]["uuid"],
                    "is_external_cost": self.patched_data['articles'][0]["is_external_cost"]
                }
            ]
        }
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_articles_1_is_external_cost,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        for article in patch_response.data['articles']:
            if article['uuid'] == patch_articles_1_is_external_cost['articles'][0]["uuid"]:
                response = article["is_external_cost"]
        expected = self.patched_data['articles'][0]["is_external_cost"]
        assert response == expected

    def test_patch_articles_1_has_subtotal(self):
        patch_articles1_has_subtotal = {
            "articles": [
                {
                    "uuid": self.response.data['articles'][0]["uuid"],
                    "has_subtotal": self.patched_data['articles'][0]["has_subtotal"]
                }
            ]
        }
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_articles1_has_subtotal,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        response = None
        for article in patch_response.data['articles']:
            if article['uuid'] == patch_articles1_has_subtotal['articles'][0]["uuid"]:
                response = article["has_subtotal"]
        expected = self.patched_data['articles'][0]["has_subtotal"]
        assert response == expected

    def test_patch_articles_1_discount(self):
        new_discount = Decimal(50)

        patch_articles_1_discount = {
            "articles": [
                {
                    "uuid": self.response.data['articles'][0]["uuid"],
                    "discount": str(new_discount)
                }
            ]
        }

        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_articles_1_discount,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        response = None
        for article in patch_response.data['articles']:
            if article['uuid'] == patch_articles_1_discount['articles'][0]["uuid"]:
                response = article["discount"]

        assert Decimal(response) == new_discount

    def test_patch_activity_1_discount(self):
        new_discount = Decimal(150)

        patch_activity_1_discount = {
            "activities": [
                {
                    "uuid": self.response.data['activities'][0]["uuid"],
                    "discount": str(new_discount)
                }
            ]
        }
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_activity_1_discount,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        response = None
        for activity in patch_response.data['activities']:
            if activity['uuid'] == patch_activity_1_discount['activities'][0]["uuid"]:
                response = activity["discount"]

        assert Decimal(response) == new_discount

    def test_patch_article_1_delete(self):
        patch_article_1_delete = {
            "articles": [
                {
                    "uuid": self.response.data['articles'][0]["uuid"],
                    "deleted": True
                }
            ]
        }
        patch_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_article_1_delete,
            format='json'
        )
        assert patch_response.data['uuid'] == self.response.data['uuid']
        response = len(patch_response.data['articles'])
        expected = len(self.response.data['articles']) - 1
        assert response == expected

    def test_patch_delete_article_after_sent_offer(self):
        self.offer.offer_status = OfferStatus.SENT
        self.offer.save()
        patch_delete_article_offer_sent = {
            "articles": [
                {
                    "uuid": self.response.data['articles'][0]["uuid"],
                    "deleted": True
                }
            ]
        }
        assert self.offer.offer_status == OfferStatus.SENT
        uuid = self.response.data['articles'][0]["uuid"]
        article_sent_offer_uuid = self.response.data['uuid']
        try_delete_article_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_delete_article_offer_sent,
            format='json'
        )
        self.assertErrorResponse(
            try_delete_article_response,
            {"__all__":
                [f"'{uuid}' can't be deleted while the Offer: '{article_sent_offer_uuid}' is 'sent'."]
             }
        )

        uuid = self.response.data['activities'][0]["uuid"]
        activity_sent_offer_uuid = self.response.data['uuid']
        patch_delete_activities_offer_sent = {
            "activities": [
                {
                    "uuid": uuid,
                    "deleted": True
                }
            ]
        }
        try_delete_activity_response = self.admin_client.patch(
            self.new_offers_url,
            data=patch_delete_activities_offer_sent,
            format='json'
        )
        self.assertErrorResponse(
            try_delete_activity_response,
            {"__all__":
                [f"'{uuid}' can't be deleted while the Offer: '{activity_sent_offer_uuid}' is 'sent'."]
             }
        )

    def test_response_terms_collection_uuid_when_offer_sent_or_accepted(self):
        self.offer.offer_status = OfferStatus.SENT
        self.offer.save()
        terms_collection = TermsCollectionFactory()
        wrong_patch_data = dict(
            terms_collection_uuid=terms_collection.pk
        )
        sent_offer_response = self.admin_client.patch(
            self.new_offers_url, data=wrong_patch_data, format='json'
        )
        self.assertEqual(sent_offer_response.status_code, 400)

        self.offer.offer_status = OfferStatus.ACCEPTED
        self.offer.save()
        accepted_offer_response = self.admin_client.patch(self.new_offers_url, data=wrong_patch_data, format='json')
        self.assertEqual(accepted_offer_response.status_code, 400)

    def test_response_terms_collection_uuid_when_offer_declined(self):
        self.offer.offer_status = OfferStatus.SENT
        self.offer.save()
        self.offer.offer_status = OfferStatus.DECLINED
        self.offer.save()
        terms_collection2 = TermsCollectionFactory()
        wrong_patch_data2 = dict(
            terms_collection_uuid=terms_collection2.pk
        )
        sent_offer_response = self.admin_client.patch(
            self.new_offers_url, data=wrong_patch_data2, format='json'
        )
        self.assertEqual(sent_offer_response.status_code, 400)

        declined_offer_response = self.admin_client.patch(self.new_offers_url, data=wrong_patch_data2, format='json')
        self.assertEqual(declined_offer_response.status_code, 400)

    def test_response_terms_collection_uuid_when_offer_not_sent(self):
        terms_collection = TermsCollectionFactory()
        patch_data = dict(
            terms_collection_uuid=terms_collection.pk
        )
        response = self.admin_client.patch(self.new_offers_url, data=patch_data, format='json')
        assert response.data.get('offer_status') == OfferStatus.CREATED
        assert str(response.data.get('terms_collection_uuid')) == str(terms_collection.pk)
