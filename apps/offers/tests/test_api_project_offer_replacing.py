from django.urls import reverse
from rest_framework import status
from apps.metronom_commons.choices_flow import OfferStatus
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.tests.factories import (ProjectFactory, ProjectActivityFactory,
                                           ProjectArticleFactory, CampaignFactory)
from apps.offers.models import Offer, ActivityOffered, ArticleOffered
from apps.offers.tests.factories import (OfferFactory, ArticleOfferedFactory,
                                         ActivityOfferedFactory, CampaignOfferFactory)
from apps.metronom_commons.choices_flow import ActivityArticleOfferedStatus
import pytest
from copy import deepcopy


class TestProjectOfferReplacement(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super(TestProjectOfferReplacement, cls).setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_article = ProjectArticleFactory(project=cls.project)
        cls.offer = OfferFactory(project=cls.project)
        cls.activity_offered = ActivityOfferedFactory(offer=cls.offer, project_activity=cls.project_activity)
        cls.article_offered = ArticleOfferedFactory(offer=cls.offer, project_article=cls.project_article)

        cls.status_url = reverse('offer-status', kwargs={'project_pk': cls.offer.project.id, "id": cls.offer.id})
        cls.data_to_patch = {
            "activities": [
                {
                    "uuid": str(cls.activity_offered.pk),
                    "offer_status": OfferStatus.REPLACED
                }
            ],
            "articles": [
                {
                    "uuid": str(cls.article_offered.pk),
                    "offer_status": OfferStatus.REPLACED
                }
            ]
        }
        cls.offer.change_status_to_sent()

        # if not refreshing the offer will still have the status of CREATED
        cls.offer.refresh_from_db()

    @classmethod
    def assertQuerysetsEqual(cls, qs1, qs2):
        assert sorted(list(qs1)) == sorted(list(qs2))

    @classmethod
    def check_offer_is_replaced(cls, offer_response):
        assert offer_response.status_code == status.HTTP_200_OK
        assert offer_response.data['uuid'] == str(cls.offer.pk)
        assert offer_response.data['offer_status'] == OfferStatus.REPLACED

    @classmethod
    def check_for_new_offer(cls):
        all_new_offers = Offer.objects.exclude(id=cls.offer.id)
        assert len(all_new_offers) == 1

        # Keeping this, we could use it instead the Offer.objects.get
        # cls.offer.refresh_from_db()

        new_offer = all_new_offers[0]
        old_offer = Offer.objects.get(id=cls.offer.id)

        assert old_offer.replaced_by == new_offer
        assert new_offer.replaced_item == old_offer

        # A new Offer must contain all accepted activites & articles
        cls.assertQuerysetsEqual(
            list(old_activity.project_activity for old_activity in
                 old_offer.activities.filter(offer_status=OfferStatus.ACCEPTED)),
            list(new_activity.project_activity for new_activity in
                 new_offer.activities.filter(offer_status=OfferStatus.ACCEPTED))
        )
        cls.assertQuerysetsEqual(
            list(old_article.project_article for old_article in
                 old_offer.articles.filter(offer_status=OfferStatus.ACCEPTED)),
            list(new_article.project_article for new_article in
                 new_offer.articles.filter(offer_status=OfferStatus.ACCEPTED))
        )

        # A new Offer must not contain any declined activities & articles
        assert new_offer.activities.filter(offer_status=OfferStatus.DECLINED).exists() is False
        assert new_offer.articles.filter(offer_status=OfferStatus.DECLINED).exists() is False

        return new_offer

    @classmethod
    def check_replaced_articles(cls, old_articles, new_offer, ):
        # A new Offer should contain newer versions for the replaced articles
        for old_article in old_articles:
            # The new Article should have the same Project Article, we also check if this is true with this
            old_article.project_article.refresh_from_db()
            old_article.refresh_from_db()
            new_offer.refresh_from_db()
            if old_article.project_article.offer_status == ActivityArticleOfferedStatus.REPLACED:
                new_article = new_offer.articles.filter(
                    project_article=old_article.project_article.replaced_by
                )
            else:
                new_article = new_offer.articles.filter(project_article=old_article.project_article)
            assert old_article.offer_status == ActivityArticleOfferedStatus.REPLACED
            assert new_article.first().offer_status == ActivityArticleOfferedStatus.IMPROVABLE

    @classmethod
    def check_replaced_activities(cls, old_activities, new_offer):
        # A new Offer should contain newer versions for the replaced activities
        for old_activity in old_activities:
            # The new Activity should have the same Project Activity, we also check if this is true with this
            old_activity.project_activity.refresh_from_db()
            old_activity.refresh_from_db()
            new_offer.refresh_from_db()
            if old_activity.project_activity.offer_status == ActivityArticleOfferedStatus.REPLACED:
                new_activity = new_offer.activities.filter(project_activity=old_activity.project_activity.replaced_by)
            else:
                new_activity = new_offer.activities.filter(project_activity=old_activity.project_activity)
            assert old_activity.offer_status == ActivityArticleOfferedStatus.REPLACED
            assert new_activity.first().offer_status == ActivityArticleOfferedStatus.IMPROVABLE

    def test_one_replaced_article_brings_a_new_offer(self):
        replaced_article_patch_data = deepcopy(self.data_to_patch)
        replaced_article_patch_data['activities'][0]['offer_status'] = OfferStatus.ACCEPTED

        one_replaced_article_offer_response = self.admin_client.patch(
            self.status_url,
            replaced_article_patch_data,
            format="json"
        )

        assert one_replaced_article_offer_response.data['uuid'] == str(self.offer.pk)
        assert one_replaced_article_offer_response.data['offer_status'] == OfferStatus.REPLACED
        assert one_replaced_article_offer_response.data['articles'][0]['offer_status'] == OfferStatus.REPLACED
        assert one_replaced_article_offer_response.data['activities'][0]['offer_status'] == OfferStatus.ACCEPTED

        accepted_new_activity_uuid = one_replaced_article_offer_response.data[
            'activities'][0]['replaced_by']['uuid']
        replaced_new_article_uuid = one_replaced_article_offer_response.data[
            'articles'][0]['replaced_by']['uuid']
        accepted_new_activity = ActivityOffered.objects.get(id=accepted_new_activity_uuid)
        replaced_new_article = ArticleOffered.objects.get(id=replaced_new_article_uuid)
        assert replaced_new_article.offer_status == ActivityArticleOfferedStatus.IMPROVABLE
        assert accepted_new_activity.offer_status == ActivityArticleOfferedStatus.ACCEPTED

        self.check_for_new_offer()

    def test_one_replaced_activity_brings_a_new_offer_patch(self):
        replaced_activity_patch_data = deepcopy(self.data_to_patch)
        replaced_activity_patch_data['articles'][0]['offer_status'] = OfferStatus.ACCEPTED

        one_replaced_activity_offer_patch_response = self.admin_client.patch(
            self.status_url,
            replaced_activity_patch_data,
            format="json"
        )

        assert one_replaced_activity_offer_patch_response.status_code == status.HTTP_200_OK
        assert one_replaced_activity_offer_patch_response.data['uuid'] == str(self.offer.pk)
        assert one_replaced_activity_offer_patch_response.data['offer_status'] == OfferStatus.REPLACED
        assert one_replaced_activity_offer_patch_response.data['articles'][0]['offer_status'] == OfferStatus.ACCEPTED
        assert one_replaced_activity_offer_patch_response.data['activities'][0]['offer_status'] == OfferStatus.REPLACED
        new_replaced_activity_uuid = one_replaced_activity_offer_patch_response.data['activities'][0]['replaced_by'][
            'uuid']
        new_accepted_article_uuid = one_replaced_activity_offer_patch_response.data['articles'][0]['replaced_by'][
            'uuid']
        accepted_new_activity = ActivityOffered.objects.get(id=new_replaced_activity_uuid)
        replaced_new_article = ArticleOffered.objects.get(id=new_accepted_article_uuid)
        assert accepted_new_activity.offer_status == ActivityArticleOfferedStatus.IMPROVABLE
        assert replaced_new_article.offer_status == ActivityArticleOfferedStatus.ACCEPTED

        self.check_for_new_offer()

    def test_one_replacing_all_items_brings_a_new_offer_patch(self):
        replaced_all_patch_data = deepcopy(self.data_to_patch)

        replacing_all_items_offer_response = self.admin_client.patch(
            self.status_url,
            replaced_all_patch_data,
            format="json"
        )

        assert replacing_all_items_offer_response.data['uuid'] == str(self.offer.pk)
        assert replacing_all_items_offer_response.data['offer_status'] == OfferStatus.REPLACED
        assert replacing_all_items_offer_response.data['articles'][0]['offer_status'] == OfferStatus.REPLACED
        assert replacing_all_items_offer_response.data['activities'][0]['offer_status'] == OfferStatus.REPLACED
        new_activity_uuid = replacing_all_items_offer_response.data['activities'][0]['replaced_by']['uuid']
        new_article_uuid = replacing_all_items_offer_response.data['articles'][0]['replaced_by']['uuid']
        new_activity = ActivityOffered.objects.get(id=new_activity_uuid)
        new_article = ArticleOffered.objects.get(id=new_article_uuid)
        assert new_activity.offer_status == ActivityArticleOfferedStatus.IMPROVABLE
        assert new_article.offer_status == ActivityArticleOfferedStatus.IMPROVABLE

        self.check_for_new_offer()

    def test_one_replacing_and_one_declined_brings_a_new_offer_patch(self):
        one_replaced_one_declined_patch_data = deepcopy(self.data_to_patch)
        one_replaced_one_declined_patch_data['articles'][0]['offer_status'] = OfferStatus.DECLINED
        one_replaced_one_declined_patch_response = self.admin_client.patch(
            self.status_url,
            one_replaced_one_declined_patch_data,
            format="json"
        )

        assert one_replaced_one_declined_patch_response.data['uuid'] == str(self.offer.pk)
        assert one_replaced_one_declined_patch_response.data['offer_status'] == OfferStatus.REPLACED

        assert one_replaced_one_declined_patch_response.data['articles'][0]['offer_status'] == OfferStatus.DECLINED
        assert one_replaced_one_declined_patch_response.data['activities'][0]['offer_status'] == OfferStatus.REPLACED
        new_activity_uuid = one_replaced_one_declined_patch_response.data['activities'][0]['replaced_by']['uuid']
        new_activity = ActivityOffered.objects.get(id=new_activity_uuid)
        new_offer = new_activity.offer
        assert new_activity.offer_status == ActivityArticleOfferedStatus.IMPROVABLE
        # The New Cloned ProjectOffer doesn't have now ArticlesOffered inside since was previously DECLINED
        assert not new_offer.articles.count()

    def test_one_accepted_and_one_declined_keep_the_offer_patch(self):
        one_accepted_and_one_declined_patch_data = deepcopy(self.data_to_patch)
        one_accepted_and_one_declined_patch_data['articles'][0]['offer_status'] = OfferStatus.DECLINED
        one_accepted_and_one_declined_patch_data['activities'][0]['offer_status'] = OfferStatus.ACCEPTED
        one_accepted_one_declined_response = self.admin_client.patch(
            self.status_url,
            one_accepted_and_one_declined_patch_data,
            format="json"
        )

        assert one_accepted_one_declined_response.data['uuid'] == str(self.offer.pk)
        assert one_accepted_one_declined_response.data['offer_status'] == OfferStatus.PARTLY_ACCEPTED

        assert one_accepted_one_declined_response.data['articles'][0]['offer_status'] == OfferStatus.DECLINED
        assert one_accepted_one_declined_response.data['activities'][0]['offer_status'] == OfferStatus.ACCEPTED

        replace_offer_patch_data = {"offer_status": OfferStatus.REPLACED}
        replacing_partly_accepted_offer_patch_response = self.admin_client.patch(
            self.status_url,
            replace_offer_patch_data,
            format="json"
        )
        assert replacing_partly_accepted_offer_patch_response.data['uuid'] == str(self.offer.pk)
        assert replacing_partly_accepted_offer_patch_response.data['offer_status'] == OfferStatus.REPLACED
        assert replacing_partly_accepted_offer_patch_response.data['activities'][0]['offer_status'] == OfferStatus.ACCEPTED
        new_accepted_uuid = replacing_partly_accepted_offer_patch_response.data['activities'][0]['replaced_by']['uuid']
        new_accepted_activity = ActivityOffered.objects.get(id=new_accepted_uuid)
        new_offer = new_accepted_activity.offer
        assert new_accepted_activity.offer_status == ActivityArticleOfferedStatus.ACCEPTED
        # The New Cloned ProjectOffer doesn't have now ArticlesOffered inside since was previously DECLINED
        assert not new_offer.articles.count()

    def test_replace_project_with_all_positions_improvable_offer_patch(self):
        all_positions_improvable_offer_patch_data = deepcopy(self.data_to_patch)
        all_positions_improvable_offer_patch_response = self.admin_client.patch(
            self.status_url,
            all_positions_improvable_offer_patch_data,
            format="json"
        )

        assert all_positions_improvable_offer_patch_response.data['uuid'] == str(self.offer.pk)
        assert all_positions_improvable_offer_patch_response.data['offer_status'] == OfferStatus.REPLACED

        assert all_positions_improvable_offer_patch_response.data['articles'][0]['offer_status'] == OfferStatus.REPLACED
        assert all_positions_improvable_offer_patch_response.data['activities'][0]['offer_status'] == OfferStatus.REPLACED

        activity_uuid = all_positions_improvable_offer_patch_response.data['activities'][0]['replaced_by']['uuid']
        article_uuid = all_positions_improvable_offer_patch_response.data['articles'][0]['replaced_by']['uuid']
        new_improvable_activity = ActivityOffered.objects.get(id=activity_uuid)
        new_improvable_article = ArticleOffered.objects.get(id=article_uuid)
        self.assertEqual(new_improvable_activity.offer_status, ActivityArticleOfferedStatus.IMPROVABLE)
        self.assertEqual(new_improvable_article.offer_status, ActivityArticleOfferedStatus.IMPROVABLE)
        new_url_for_basic_patch = reverse(
            'api_projects:offer-detail',
            args=[self.offer.project.id, new_improvable_activity.offer.id]
        )
        simple_data_patch = {
            "activities": [
                {
                    "uuid": str(new_improvable_activity.pk),
                    "is_optional": True
                }
            ]
        }
        mixed_positions_status_offer_patch_response = self.admin_client.patch(
            new_url_for_basic_patch,
            simple_data_patch,
            format="json"
        )
        assert mixed_positions_status_offer_patch_response.data['uuid'] == str(new_improvable_activity.offer.pk)
        assert mixed_positions_status_offer_patch_response.data['offer_status'] == OfferStatus.CREATED

        self.assertEqual(
            mixed_positions_status_offer_patch_response.data['articles'][0]['offer_status'],
            ActivityArticleOfferedStatus.IMPROVABLE
        )
        self.assertEqual(
            mixed_positions_status_offer_patch_response.data['activities'][0][
                   'offer_status'],
            ActivityArticleOfferedStatus.IMPROVED
        )
        new_improvable_activity.refresh_from_db()
        new_improvable_article.refresh_from_db()
        self.assertEqual(
            new_improvable_activity.project_activity.offer_status,
            ActivityArticleOfferedStatus.IMPROVED
        )
        self.assertEqual(
            new_improvable_article.project_article.offer_status,
            ActivityArticleOfferedStatus.IMPROVABLE
        )

        # PATCH ArticleOffered the other way around from ProjectArticle => ArticleOffered
        project_article_patch_data = {
            "display_name": "Displayed name",
        }

        project_article_patch_endpoint = reverse(
            'api_projects:article-detail',
            args=[new_improvable_article.project_article.project.id, new_improvable_article.project_article.id]
        )
        self.admin_client.patch(
            project_article_patch_endpoint, project_article_patch_data, format="json"
        )

        new_improvable_article.project_article.refresh_from_db()
        new_improvable_article.refresh_from_db()

        self.assertEqual(
            new_improvable_article.project_article.offer_status,
            ActivityArticleOfferedStatus.IMPROVED
        )
        self.assertEqual(
            new_improvable_article.offer_status,
            ActivityArticleOfferedStatus.IMPROVED
        )

    def test_replace_project_with_all_positions_improvable_offer_patch_2(self):
        self.offer.change_status_to_replaced()
        self.offer.refresh_from_db()

        new_offer = self.offer.replaced_by
        new_improvable_activity = new_offer.activities.first()
        new_improvable_article = new_offer.articles.first()
        self.assertEqual(new_improvable_activity.offer_status, ActivityArticleOfferedStatus.IMPROVABLE)
        self.assertEqual(new_improvable_article.offer_status, ActivityArticleOfferedStatus.IMPROVABLE)
        new_url_for_basic_patch = reverse(
            'api_projects:offer-detail',
            args=[self.offer.project.id, new_improvable_activity.offer.id]
        )
        simple_article_data_patch = {
            "articles": [
                {
                    "uuid": str(new_improvable_article.pk),
                    "is_external": True
                }
            ]
        }
        mixed_positions_status_offer_patch_response_2 = self.admin_client.patch(
            new_url_for_basic_patch,
            simple_article_data_patch,
            format="json"
        )
        assert mixed_positions_status_offer_patch_response_2.data['uuid'] == str(new_offer.pk)
        assert mixed_positions_status_offer_patch_response_2.data['offer_status'] == OfferStatus.CREATED

        self.assertEqual(
            mixed_positions_status_offer_patch_response_2.data['activities'][0]['offer_status'],
            ActivityArticleOfferedStatus.IMPROVABLE
        )
        self.assertEqual(
            mixed_positions_status_offer_patch_response_2.data['articles'][0][
                   'offer_status'],
            ActivityArticleOfferedStatus.IMPROVED
        )
        new_improvable_activity.refresh_from_db()
        new_improvable_article.refresh_from_db()
        self.assertEqual(
            new_improvable_activity.project_activity.offer_status,
            ActivityArticleOfferedStatus.IMPROVABLE
        )
        self.assertEqual(
            new_improvable_article.project_article.offer_status,
            ActivityArticleOfferedStatus.IMPROVED
        )

        # PATCH ArticleOffered the other way arround from ProjectArticle => ArticleOffered
        project_activity_patch_data = {
            "display_name": "Displayed name",
        }
        project_activity_patch_endpoint = reverse(
            'api_projects:activity-detail',
            args=[new_improvable_activity.project_activity.project.id, new_improvable_activity.project_activity.id]
        )
        self.admin_client.patch(
            project_activity_patch_endpoint, project_activity_patch_data, format="json"
        )

        new_improvable_activity.project_activity.refresh_from_db()
        new_improvable_activity.refresh_from_db()

        self.assertEqual(
            new_improvable_activity.project_activity.offer_status,
            ActivityArticleOfferedStatus.IMPROVED
        )
        self.assertEqual(
            new_improvable_activity.offer_status,
            ActivityArticleOfferedStatus.IMPROVED
        )


class TestCampaignOfferReplacement(MetronomBaseAPITestCase):
    """Test STATUS Handling from the Campaign Offer Level"""

    @classmethod
    def setUpTestData(cls):
        super(TestCampaignOfferReplacement, cls).setUpTestData()
        cls.campaign = CampaignFactory(billing_currency="CHF")
        cls.project = ProjectFactory(campaign=cls.campaign)
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_article = ProjectArticleFactory(project=cls.project)
        cls.project_offer = OfferFactory(project=cls.project)
        cls.activity_offered = ActivityOfferedFactory(
            offer=cls.project_offer, project_activity=cls.project_activity)
        cls.article_offered = ArticleOfferedFactory(
            offer=cls.project_offer, project_article=cls.project_article)
        cls.campaign_offer = CampaignOfferFactory(campaign=cls.campaign)
        cls.campaign_offer.project_offers.add(cls.project_offer)
        cls.campaign_status_url = reverse(
            'campaign-status',
            kwargs={'campaign_pk': cls.campaign.id, "id": cls.campaign_offer.id}
        )
        cls.data_to_patch = {
            "campaign_status": OfferStatus.ACCEPTED,
            "project_offers": [
                {
                    "uuid": str(cls.project_offer.pk),
                    "offer_status": OfferStatus.ACCEPTED,
                    "activities": [
                        {
                            "uuid": str(cls.activity_offered.pk),
                            "offer_status": OfferStatus.ACCEPTED
                        }
                    ],
                    "articles": [
                        {
                            "uuid": str(cls.article_offered.pk),
                            "offer_status": OfferStatus.ACCEPTED
                        }
                    ]
                }
            ]
        }
        cls.campaign_offer.change_status_to_sent()
        # if not refreshing the offer will still have the status of CREATED
        cls.campaign_offer.refresh_from_db()
        cls.project_offer.refresh_from_db()
        cls.article_offered.refresh_from_db()
        cls.activity_offered.refresh_from_db()
        cls.project_article.refresh_from_db()
        cls.project_activity.refresh_from_db()

    def test_campaign_offer_patch_accepted_campaign_status_fully_accepted(self):
        """Send PATCH with All the Items Accepted"""
        campaign_offer_fully_accepted_data = deepcopy(self.data_to_patch)
        campaign_offer_fully_accepted_patch_response = self.admin_client.patch(
            self.campaign_status_url,
            campaign_offer_fully_accepted_data,
            format="json"
        )
        assert campaign_offer_fully_accepted_patch_response.status_code == status.HTTP_200_OK
        self.assertEqual(
            campaign_offer_fully_accepted_patch_response.data['campaign_status'],
            OfferStatus.ACCEPTED
        )
        self.assertEqual(
            campaign_offer_fully_accepted_patch_response.data['project_offers'][0]['offer_status'],
            OfferStatus.ACCEPTED
        )
        self.assertEqual(
            campaign_offer_fully_accepted_patch_response.data['project_offers'][0]['activities'][0]['offer_status'],
            OfferStatus.ACCEPTED
        )
        self.assertEqual(
            campaign_offer_fully_accepted_patch_response.data['project_offers'][0]['articles'][0]['offer_status'],
            OfferStatus.ACCEPTED
        )

    def test_campaign_offer_patch_accepted_simplified_accepted(self):
        """Send PATCH with campaign_offer = ACCEPTED => All it's children are ACCEPTED too"""
        campaign_offer_simplified_accepted_data = deepcopy(self.data_to_patch)
        campaign_offer_simplified_accepted_data.pop('project_offers')
        campaign_offer_simplified_accepted_patch_response = self.admin_client.patch(
            self.campaign_status_url,
            campaign_offer_simplified_accepted_data,
            format="json"
        )
        assert campaign_offer_simplified_accepted_patch_response.status_code == status.HTTP_200_OK
        self.assertEqual(
            campaign_offer_simplified_accepted_patch_response.data['campaign_status'],
            OfferStatus.ACCEPTED
        )
        self.assertEqual(
            campaign_offer_simplified_accepted_patch_response.data['project_offers'][0]['offer_status'],
            OfferStatus.ACCEPTED
        )
        self.assertEqual(
            campaign_offer_simplified_accepted_patch_response.data['project_offers'][0]['activities'][0]['offer_status'],
            OfferStatus.ACCEPTED
                         )
        self.assertEqual(
            campaign_offer_simplified_accepted_patch_response.data['project_offers'][0]['articles'][0]['offer_status'],
            OfferStatus.ACCEPTED
        )

    def test_campaign_offer_patch_with_one_accepted_decline_the_entire_campaign_offer(self):
        """Send PATCH with campaign_offer = DECLINED => All it's children are DECLINED too"""
        campaign_offer_simplified_accepted_data = deepcopy(self.data_to_patch)
        campaign_offer_simplified_accepted_data.pop('project_offers')
        campaign_offer_simplified_accepted_data['campaign_status'] = OfferStatus.DECLINED
        # We simulate we have an ACCEPTED ActivityOffered (from Previously Created CampaignOffer) and
        #  a SENT ArticleOffered in a SENT ProjectOffer
        self.activity_offered.change_status_to_accepted()
        self.activity_offered.refresh_from_db()
        assert self.activity_offered.offer_status == OfferStatus.ACCEPTED
        assert self.activity_offered.project_activity.offer_status == OfferStatus.ACCEPTED
        assert self.article_offered.offer_status == OfferStatus.SENT
        assert self.article_offered.project_article.offer_status == OfferStatus.SENT
        campaign_offer_simplified_accepted_patch_response = self.admin_client.patch(
            self.campaign_status_url,
            campaign_offer_simplified_accepted_data,
            format="json"
        )

        assert campaign_offer_simplified_accepted_patch_response.status_code == status.HTTP_200_OK
        self.assertEqual(
            campaign_offer_simplified_accepted_patch_response.data['campaign_status'],
            OfferStatus.DECLINED
        )
        self.assertEqual(
            campaign_offer_simplified_accepted_patch_response.data['project_offers'][0]['offer_status'],
            OfferStatus.DECLINED
        )
        self.assertEqual(
            campaign_offer_simplified_accepted_patch_response.data['project_offers'][0]['articles'][0]['offer_status'],
            OfferStatus.DECLINED
        )

    def test_force_a_partly_accepted_status_for_campaign_offer_patch(self):
        """Send PATCH with ActivityOffered inside ProjectOffer = DECLINED
         AND with ArticleOffered inside ProjectOffer = ACCEPTED => PARTLY_ACCEPTED status"""
        partly_accepted_status_data = {
            "project_offers": [
                {
                    "uuid": str(self.project_offer.pk),
                    "activities": [
                        {
                            "uuid": str(self.activity_offered.pk),
                            "offer_status": OfferStatus.ACCEPTED
                        }
                    ],
                    "articles": [
                        {
                            "uuid": str(self.article_offered.pk),
                            "offer_status": OfferStatus.DECLINED
                        }
                    ]
                }
            ]
        }
        self.campaign_offer.change_status_to_sent()
        self.campaign_offer.refresh_from_db()
        self.activity_offered.refresh_from_db()
        self.article_offered.refresh_from_db()
        assert self.activity_offered.offer_status == OfferStatus.SENT
        assert self.article_offered.offer_status == OfferStatus.SENT
        partly_accepted_campaign_offer_patch_response = self.admin_client.patch(
            self.campaign_status_url,
            partly_accepted_status_data,
            format="json"
        )

        assert partly_accepted_campaign_offer_patch_response.status_code == status.HTTP_200_OK
        self.assertEqual(
            partly_accepted_campaign_offer_patch_response.data['campaign_status'],
            OfferStatus.PARTLY_ACCEPTED
        )
        self.assertEqual(
            partly_accepted_campaign_offer_patch_response.data['project_offers'][0]['offer_status'],
            OfferStatus.PARTLY_ACCEPTED
        )
        self.assertEqual(
            partly_accepted_campaign_offer_patch_response.data['project_offers'][0]['articles'][0]['offer_status'],
            OfferStatus.DECLINED
        )
        self.assertEqual(
            partly_accepted_campaign_offer_patch_response.data['project_offers'][0]['activities'][0]['offer_status'],
            OfferStatus.ACCEPTED
        )
