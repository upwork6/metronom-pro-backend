from datetime import timedelta
from decimal import Decimal
import pytest
from ddt import data, unpack
from django.core.exceptions import ValidationError
from django.utils import timezone
from djmoney.money import Money

from apps.backoffice.tests.factories import TermsCollectionFactory
from apps.offers.models import Offer, OfferStatus
from apps.offers.tests.factories import (ActivityOfferedFactory,
                                         ArticleOfferedFactory,
                                         CampaignOfferFactory, OfferFactory)
from apps.projects.tests.factories import (CampaignFactory,
                                           ProjectActivityFactory,
                                           ProjectActivityStepFactory,
                                           ProjectArticleFactory,
                                           ProjectFactory, TemplateActivityStepFactory)
from apps.users.tests.factories import UserFactory
from django.test import TestCase


class TestOffer(TestCase):

    def setUp(self):
        self.project = ProjectFactory(billing_currency="CHF")
        self.offer = OfferFactory(project=self.project)

    def test_basic_save_offer(self):
        self.offer1 = OfferFactory(name='sample', project=self.project)

        assert self.offer1.name == 'sample'

    def test_offer_date_after_valid_until_date(self):
        self.offer.offer_date = timezone.now() + timedelta(days=10)
        self.offer.valid_until_date = timezone.now() + timedelta(days=5)

        with self.assertRaises(ValidationError) as error:
            assert self.offer.save() == error

    def test_offer_date_after_callback_date(self):
        self.offer.offer_date = timezone.now() + timedelta(days=10)
        self.offer.valid_until_date = timezone.now() + timedelta(days=20)
        self.offer.callback_date = timezone.now() + timedelta(days=5)

        with self.assertRaises(ValidationError) as error:
            assert self.offer.save() == error

    def test_offer_status_created(self):
        offer = OfferFactory(project=self.project)
        assert offer.offer_status == OfferStatus.CREATED
        offer.offer_status = OfferStatus.ACCEPTED

        with self.assertRaises(ValidationError) as error:
            assert offer.save() == error
        offer1 = Offer.objects.get(pk=offer.id)
        assert offer1.offer_status != OfferStatus.ACCEPTED
        assert offer1.offer_status == OfferStatus.CREATED

    def test_offer_status_sent(self):
        offer = OfferFactory(project=self.project)
        offer.offer_status = OfferStatus.SENT
        offer.save()
        assert offer.offer_status != OfferStatus.CREATED
        assert offer.offer_status == OfferStatus.SENT

        offer.offer_status = OfferStatus.CREATED

        with self.assertRaises(ValidationError) as error:
            assert offer.save() == error

    def test_offer_status_accepted(self):
        offer = OfferFactory(project=self.project)
        offer.offer_status = OfferStatus.SENT
        offer.save()
        offer.offer_status = OfferStatus.ACCEPTED
        offer.save()
        assert offer.offer_status == OfferStatus.ACCEPTED
        assert offer.offer_status != OfferStatus.SENT

        offer.offer_status = OfferStatus.CREATED

        with self.assertRaises(ValidationError) as error:
            assert offer.save() == error

    def test_offer_status_declined(self):
        declined_offer = OfferFactory(project=self.project)
        declined_offer.offer_status = OfferStatus.SENT
        declined_offer.save()
        declined_offer.offer_status = OfferStatus.DECLINED
        declined_offer.save()
        assert declined_offer.offer_status == OfferStatus.DECLINED
        assert declined_offer.offer_status != OfferStatus.SENT

        declined_offer.offer_status = OfferStatus.CREATED
        with self.assertRaises(ValidationError) as error:
            assert declined_offer.save() == error

    def test_offer_public_id(self):
        project = ProjectFactory(billing_currency="CHF")
        offer1 = OfferFactory(project=project)
        self.assertEqual(offer1.public_id.split('-')[-1], '1')
        offer2 = OfferFactory(project=project)
        self.assertEqual(offer2.public_id.split('-')[-1], '2')
        offer3 = OfferFactory(project=project)
        self.assertEqual(offer3.public_id.split('-')[-1], '3')
        offer1.save()
        offer1.refresh_from_db()
        self.assertEqual(offer1.public_id.split('-')[-1], '1')


class TestOfferPropertiesBase(TestCase):
    def setUp(self):
        self.currency = 'CHF'
        self.effort_hours = Decimal("100.00")
        self.discount = Money(Decimal("1.00"), self.currency)
        self.user = UserFactory()
        self.total_offer_cost = Money(Decimal("1000.00"), self.currency)
        self.project = ProjectFactory(billing_currency="CHF")
        self.offer = OfferFactory(project=self.project)
        self.project_activity1 = ProjectActivityFactory(project=self.project)
        self.project_activity2 = ProjectActivityFactory(project=self.project)
        self.project_article1 = ProjectArticleFactory(project=self.project)
        self.project_article2 = ProjectArticleFactory(project=self.project)
        self.template_activity_step = TemplateActivityStepFactory()
        self.step = self.template_activity_step.step
        self.project_activity_step = ProjectActivityStepFactory(
            activity=self.project_activity1,
            planned_effort=self.effort_hours,
            used_step=self.step
        )
        ProjectActivityStepFactory(
            activity=self.project_activity2,
            planned_effort=self.effort_hours,
        )


class TestOfferProperties(TestOfferPropertiesBase):
    def setUp(self):
        super(TestOfferProperties, self).setUp()

    def test_offer_total_optional_costs(self):
        offer_activity1 = ActivityOfferedFactory(
            project_activity=self.project_activity1,
            offer=self.offer,
            discount=self.discount,
            is_optional=True
        )
        ActivityOfferedFactory(
            project_activity=self.project_activity2,
            offer=self.offer,
            discount=self.discount
        )
        assert offer_activity1.offered_sum == self.offer.total_optional_costs

    def test_offer_total_external_costs(self):
        offer_article1 = ArticleOfferedFactory(
            project_article=self.project_article1,
            offer=self.offer,
            discount=self.discount,
            is_external_cost=True
        )
        ArticleOfferedFactory(
            project_article=self.project_article2,
            offer=self.offer,
            discount=self.discount
        )

        assert offer_article1.offered_sum == self.offer.total_external_costs

    def test_total_offer_without_tax(self):
        offer_article1 = ArticleOfferedFactory(
            project_article=self.project_article1,
            offer=self.offer,
            discount=self.discount,
            is_external_cost=False,
            is_optional=False,

        )
        offer_article2 = ArticleOfferedFactory(
            project_article=self.project_article2,
            offer=self.offer,
            discount=self.discount,
            is_external_cost=False,
            is_optional=False,
        )
        offer_activity1 = ActivityOfferedFactory(
            project_activity=self.project_activity1,
            offer=self.offer,
            discount=self.discount,
            is_optional=False
        )
        offer_activity2 = ActivityOfferedFactory(
            project_activity=self.project_activity2,
            offer=self.offer,
            discount=self.discount,
            is_optional=False
        )
        expected = sum([offer_activity1.offered_sum_with_discount, offer_activity2.offered_sum_with_discount,
                        offer_article1.offered_sum_with_discount, offer_article2.offered_sum_with_discount])
        result = self.offer.total_offer_without_tax

        assert expected == result

    def test_total_offer_with_tax(self):
        offer_article1 = ArticleOfferedFactory(
            project_article=self.project_article1,
            offer=self.offer,
            discount=self.discount,
            is_external_cost=False,
            is_optional=False,

        )
        offer_article2 = ArticleOfferedFactory(
            project_article=self.project_article2,
            offer=self.offer,
            discount=self.discount,
            is_external_cost=False,
            is_optional=False,
        )
        offer_activity1 = ActivityOfferedFactory(
            project_activity=self.project_activity1,
            offer=self.offer,
            discount=self.discount,
            is_optional=False
        )
        offer_activity2 = ActivityOfferedFactory(
            project_activity=self.project_activity2,
            offer=self.offer,
            discount=self.discount,
            is_optional=False
        )
        self.offer.is_tax_included = True
        self.offer.save()
        total_offer_without_tax = sum(
            [offer_activity1.offered_sum_with_discount, offer_activity2.offered_sum_with_discount,
             offer_article1.offered_sum_with_discount, offer_article2.offered_sum_with_discount])
        tax = total_offer_without_tax * Decimal(self.offer.used_tax_rate) / 100
        expected = total_offer_without_tax + tax
        result = self.offer.total_offer_with_tax

        assert expected == result

    @pytest.mark.skip(reason="Should check why it fails")
    @unpack
    @data(
        {'offer_status': OfferStatus.SENT},
        {'offer_status': OfferStatus.ACCEPTED},
        {'offer_status': OfferStatus.DECLINED}
    )
    def test_terms_collection_when_offer_already_sent(self, offer_status):
        terms_collection = TermsCollectionFactory()
        self.offer.offer_status = offer_status
        self.offer.terms_collection = terms_collection

        with self.assertRaisesMessage(
                ValidationError, 'The field "terms_collection" cannot be updated for already SENT Offer'
        ):
            self.offer.save()

    @pytest.mark.skip(reason="Should check why it fails")
    @unpack
    @data(
        {'offer_status': OfferStatus.CREATED}
    )
    def test_terms_collection_when_offer_not_sent(self, offer_status):
        terms_collection = TermsCollectionFactory()
        self.offer.offer_status = offer_status
        self.offer.terms_collection = terms_collection
        self.offer.save()
        assert self.offer.terms_collection.pk == terms_collection.pk
        assert self.offer.offer_status == offer_status


class ActivityOfferedTest(TestCase):

    def setUp(self):
        self.currency = 'CHF'
        self.effort_hours = Decimal("100.00")
        self.user = UserFactory()
        self.discount = Money(Decimal("1.00"), self.currency)
        self.project = ProjectFactory(billing_currency=self.currency)
        self.project_activity = ProjectActivityFactory(project=self.project)
        self.project_activity_step = ProjectActivityStepFactory(
            activity=self.project_activity,
            planned_effort=self.effort_hours,
        )
        self.offer = OfferFactory(project=self.project)

    def test_activity_offered_save(self):
        activity_offered = ActivityOfferedFactory(
            project_activity=self.project_activity,
            discount=self.discount,
            used_name='TestSample',
            offer=self.offer
        )
        assert activity_offered.used_name == 'TestSample'

    @pytest.mark.skip(reason="not completed yest the protection")
    def test_activity_offered_save_already_approved(self):
        sample_activity_name = "Sample Test Name"
        changed_activity_name = "Another Test Name"

        project_activity = ProjectActivityFactory(project=self.project)

        activity_offered = ActivityOfferedFactory(
            used_name=sample_activity_name,
            offer=self.offer,
            project_activity=project_activity
        )

        assert activity_offered.used_name == sample_activity_name
        activity_offered.change_status_to_sent()
        activity_offered.change_status_to_accepted()

        activity_offered.used_name = changed_activity_name
        with self.assertRaises(ValidationError) as error:
            error_response = activity_offered.save()
            assert error_response == error

    def test_activity_offered_offered_sum_with_discount(self):
        activity_offered = ActivityOfferedFactory(
            offer=self.offer,
            project_activity=self.project_activity,
            discount=self.discount
        )
        discounted_offer = activity_offered.offered_sum_with_discount
        calculated = activity_offered.project_activity.total_planned_budget - self.discount
        assert discounted_offer == calculated

    def test_activity_offered_offered_sum_with_wrong_discount(self):
        wrong_discount = self.project_activity.total_planned_budget + Money(100, self.currency)
        error_message_part = "can't be smaller than 0 and not bigger that offered_sum"
        with self.assertRaises(ValidationError) as error:
            assert ActivityOfferedFactory(project_activity=self.project_activity,
                                          offer=self.offer, discount=wrong_discount) == error
        assert error_message_part in error.exception.messages[0]

    def test_activity_offered_offered_sum(self):
        activity_offered = ActivityOfferedFactory(
            offer=self.offer,
            project_activity=self.project_activity,
            discount=Money(0, self.currency)
        )
        calculated_offer = self.project_activity.total_planned_budget
        activity_offered_sum = activity_offered.offered_sum
        assert activity_offered_sum == calculated_offer


class ArticleOfferedTest(TestCase):
    def setUp(self):
        self.user = UserFactory()
        self.currency = 'CHF'
        self.project = ProjectFactory(billing_currency=self.currency)
        self.discount = Money(Decimal('1.00'), self.currency)
        self.budget = Money(Decimal('100.00'), self.currency)

        self.project_article = ProjectArticleFactory(
            budget_in_project_currency=self.budget,
            project=self.project,
        )
        self.offer = OfferFactory(project=self.project)

    def test_article_offered_offered_sum(self):
        self.article_offered = ArticleOfferedFactory(
            project_article=self.project_article,
            discount=Money(0, self.currency),
            offer=self.offer
        )
        assert self.budget == self.article_offered.offered_sum

    def test_article_offered_offered_sum_with_discount(self):
        expected_amount = self.budget - self.discount
        article_offered = ArticleOfferedFactory(
            project_article=self.project_article,
            discount=self.discount,
            offer=self.offer
        )
        result_amount = article_offered.offered_sum_with_discount
        assert result_amount == expected_amount

    def test_article_offered_offered_sum_with_wrong_discount(self):
        wrong_discount = self.budget + Money(100, self.currency)
        error_message_part = "can't be smaller than 0 and not bigger that offered_sum"
        with self.assertRaises(ValidationError) as error:
            assert ArticleOfferedFactory(
                project_article=self.project_article,
                discount=wrong_discount,
                offer=self.offer
            ) == error
            assert error_message_part in error.exception.message

    def test_article_offered_save(self):
        activity_offered = ArticleOfferedFactory(
            project_article=self.project_article,
            discount=self.discount,
            offer=self.offer
        )
        assert activity_offered.discount == self.discount

    @pytest.mark.skip(reason="not completed yet the protection")
    def test_article_offered_save_already_approved(self):
        offer_approved = OfferFactory(project=self.project)
        sample_article_used_name = "Sample Used Name For Article"
        changed_article_used_name = "Changed Used Name For Article"
        protected_article = ArticleOfferedFactory(
            used_name=sample_article_used_name,
            offer=offer_approved
        )
        assert protected_article.used_name == sample_article_used_name
        protected_article.change_status_to_sent()
        protected_article.change_status_to_accepted()

        with self.assertRaises(ValidationError) as error:
            protected_article.used_name = changed_article_used_name
            expected_error = protected_article.save()
            assert expected_error == error


class TestCampaignOffersBasics(TestCase):

    def setUp(self):
        self.used_customer_reference = "Lorem Iprum Dolorem"
        self.title = 'Campaign Title'
        self.campaign = CampaignFactory(customer_reference=self.used_customer_reference, billing_currency="CHF")
        self.campaign_offer = CampaignOfferFactory(
            campaign=self.campaign,
            title=self.title
        )

    def test_basic_save_offer(self):
        assert self.campaign_offer.title == self.title

    def test_number_is_incremented_when_save_campaign_offer_public_id(self):
        """Saving a New CampaignOffer must have incremented number inside public_id"""
        assert self.campaign_offer.public_id.split('-')[-1] == '1'
        assert self.campaign_offer.public_id.split('-')[-1] == str(self.campaign_offer.campaign.offers.count())
        campaign_offer2 = CampaignOfferFactory(campaign=self.campaign)

        assert campaign_offer2.public_id.split('-')[-1] == '2'
        assert campaign_offer2.public_id.split('-')[-1] == str(self.campaign_offer.campaign.offers.count())

        campaign_offer3 = CampaignOfferFactory(campaign=self.campaign)

        assert campaign_offer3.public_id.split('-')[-1] == '3'
        assert campaign_offer3.public_id.split('-')[-1] == str(self.campaign_offer.campaign.offers.count())

    def test_CampaignOffer_used_customer_reference_empty(self):
        assert not self.campaign_offer.used_customer_reference

    def test_CampaignOffer_sent_used_customer_reference(self):
        self.campaign_offer1 = CampaignOfferFactory(
            campaign=self.campaign,
            title=self.title
        )
        self.campaign_offer1.change_status_to_sent()

        assert self.campaign_offer1.used_customer_reference == self.used_customer_reference

    def test_CampaignOffer_accepted_used_customer_reference(self):
        self.campaign_offer1 = CampaignOfferFactory(
            campaign=self.campaign,
            title=self.title
        )
        self.campaign_offer1.change_status_to_sent()
        self.campaign_offer1.change_status_to_accepted()

        assert self.campaign_offer1.used_customer_reference == self.used_customer_reference

    def test_CampaignOffer_declined_used_customer_reference(self):
        self.campaign_offer.change_status_to_sent()

        assert self.campaign_offer.used_customer_reference == self.used_customer_reference

        self.campaign_offer.change_status_to_declined()
        assert self.campaign_offer.used_customer_reference == self.used_customer_reference


class TestCampaignOfferProperties(TestCase):

    def setUp(self):
        self.currency = 'CHF'
        self.tax = Decimal("8.00")
        self.effort_hours = Decimal("500.00")
        self.discount1 = Money(Decimal("100.00"), self.currency)
        self.discount2 = Money(Decimal("150.00"), self.currency)
        self.budget_cost1 = Money(Decimal("1000.00"), self.currency)
        self.budget_cost2 = Money(Decimal("2000.00"), self.currency)
        self.campaign = CampaignFactory(billing_currency=self.currency)
        self.project = ProjectFactory(billing_currency=self.currency, campaign=self.campaign)

        self.offer1 = OfferFactory(
            is_tax_included=True,
            project=self.project
        )
        self.offer2 = OfferFactory(
            is_tax_included=True,
            project=self.project
        )
        self.offer3 = OfferFactory(
            is_tax_included=True,
            project=self.project
        )

        self.project_activity1 = ProjectActivityFactory(project=self.project)
        self.project_activity2 = ProjectActivityFactory(project=self.project)
        ProjectActivityStepFactory(
            activity=self.project_activity1,
            planned_effort=self.effort_hours,
        )
        ProjectActivityStepFactory(
            activity=self.project_activity2,
            planned_effort=self.effort_hours,
        )
        self.activity_offered1 = ActivityOfferedFactory(
            project_activity=self.project_activity1,
            discount=self.discount1,
            offer=self.offer1,
            is_optional=False
        )
        self.activity_offered2 = ActivityOfferedFactory(
            project_activity=self.project_activity2,
            discount=self.discount2,
            offer=self.offer2,
            is_optional=False
        )

        self.project_article1 = ProjectArticleFactory(
            budget_in_project_currency=self.budget_cost1,
            project=self.project
        )
        self.project_article2 = ProjectArticleFactory(
            budget_in_project_currency=self.budget_cost2,
            project=self.project
        )
        self.article_offered1 = ArticleOfferedFactory(
            project_article=self.project_article1,
            discount=self.discount1,
            offer=self.offer1,
            is_optional=False,
            is_external_cost=False
        )
        self.article_offered2 = ArticleOfferedFactory(
            project_article=self.project_article2,
            discount=self.discount2,
            offer=self.offer2,
            is_optional=False,
            is_external_cost=False
        )
        self.article_offered3 = ArticleOfferedFactory(
            project_article=self.project_article1,
            discount=self.discount1,
            offer=self.offer3,
            is_optional=False,
            is_external_cost=False
        )
        terms = TermsCollectionFactory()

        self.campaign_offer = CampaignOfferFactory(
            tax=self.tax,
            is_tax_included=True,
            terms_collection=terms,
            campaign=self.campaign,
        )
        self.campaign_offer.project_offers.add(self.offer1, self.offer2, self.offer3)
        self.campaign_offer.save()

    def test_total_campaign_offer_without_tax(self):
        expected = sum([
            self.offer1.get_total_offer_without_tax(self.campaign_offer.currency),
            self.offer2.get_total_offer_without_tax(self.campaign_offer.currency),
            self.offer3.get_total_offer_without_tax(self.campaign_offer.currency)
        ])
        response = self.campaign_offer.total_campaign_offer_without_tax
        assert expected == response

    def test_total_campaign_offer_with_tax(self):
        expected_without_tax = sum([
            self.offer1.get_total_offer_without_tax(self.campaign_offer.currency),
            self.offer2.get_total_offer_without_tax(self.campaign_offer.currency),
            self.offer3.get_total_offer_without_tax(self.campaign_offer.currency)
        ])
        tax = expected_without_tax.amount * self.tax / 100
        expected = sum([expected_without_tax.amount, tax])
        response = self.campaign_offer.total_campaign_offer_with_tax
        assert Money(expected, self.campaign_offer.currency) == response

    def test_total_offer_send_to_customer(self):
        expected = self.campaign_offer.total_campaign_offer_with_tax
        response = self.campaign_offer.total_offer_send_to_customer
        assert expected == response

        self.campaign_offer.is_tax_included = False
        self.campaign_offer.save()
        expected2 = self.campaign_offer.total_campaign_offer_without_tax
        response2 = self.campaign_offer.total_offer_send_to_customer
        assert expected2 == response2

    def test_total_optional_costs(self):
        self.article_offered1.is_optional = True
        self.article_offered3.is_optional = True
        self.article_offered1.save()
        self.article_offered3.save()

        expected = sum([
            self.article_offered1.offered_sum.amount,
            self.article_offered3.offered_sum.amount
        ])
        response = self.campaign_offer.total_optional_costs
        assert Money(expected, self.campaign_offer.currency) == response

    def test_total_external_costs(self):
        self.article_offered1.is_external_cost = True
        self.article_offered3.is_external_cost = True
        self.article_offered1.save()
        self.article_offered3.save()

        expected = sum([
            self.article_offered1.offered_sum.amount,
            self.article_offered3.offered_sum.amount
        ])
        response = self.campaign_offer.total_external_costs
        assert Money(expected, self.campaign_offer.currency) == response

    def test_total_discounts(self):
        expected = sum([
            self.article_offered1.discount,
            self.article_offered2.discount,
            self.article_offered3.discount,
            self.activity_offered1.discount,
            self.activity_offered2.discount
        ])
        response = self.campaign_offer.total_discounts

        assert Money(expected.amount, self.campaign_offer.currency) == response

    def test_terms_collection_when_offer_already_sent(self):
        terms_collection = TermsCollectionFactory()
        self.campaign_offer.campaign_status = OfferStatus.SENT
        self.campaign_offer.save()

        with self.assertRaises(ValidationError) as error:
            self.campaign_offer.terms_collection = terms_collection
            self.campaign_offer.save()

        with self.assertRaises(ValidationError) as error:
            self.campaign_offer.title = "Another Title"
            self.campaign_offer.save()

    def test_terms_collection_when_offer_not_sent(self, campaign_status=OfferStatus.CREATED):
        terms_collection = TermsCollectionFactory()
        self.campaign_offer.campaign_status = campaign_status
        self.campaign_offer.terms_collection = terms_collection
        self.campaign_offer.save()
        assert self.campaign_offer.terms_collection.pk == terms_collection.pk
        assert self.campaign_offer.campaign_status == campaign_status


class TestFinalStateActivityOffered(TestCase):

    @classmethod
    def setUp(cls):
        cls.project = ProjectFactory()
        cls.offer = OfferFactory(project=cls.project)
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_article = ProjectArticleFactory(project=cls.project)
        cls.activity_offered = ActivityOfferedFactory(project_activity=cls.project_activity, offer=cls.offer)
        cls.article_offered = ArticleOfferedFactory(project_article=cls.project_article, offer=cls.offer)
        cls.offer.change_status_to_sent()
        cls.activity_offered.refresh_from_db()
        cls.article_offered.refresh_from_db()
        cls.project_activity.refresh_from_db()
        cls.project_article.refresh_from_db()

    def test_get_final_state(self):
        assert not self.project_activity.get_final_state()
        assert not self.activity_offered.get_final_state()

        assert not self.project_article.get_final_state()
        assert not self.article_offered.get_final_state()

        self.offer.change_status_to_accepted()
        self.activity_offered.refresh_from_db()
        self.article_offered.refresh_from_db()
        self.project_activity.refresh_from_db()
        self.project_article.refresh_from_db()

        assert self.project_activity.get_final_state()
        assert self.activity_offered.get_final_state()

        assert self.project_article.get_final_state()
        assert self.article_offered.get_final_state()

    def test_get_final_state_information_when_accepted(self):
        self.offer.change_status_to_accepted()
        self.activity_offered.refresh_from_db()
        self.article_offered.refresh_from_db()
        self.project_activity.refresh_from_db()
        self.project_article.refresh_from_db()

        # With `.get_final_state_information()` from ActivityOffered and ArticleOffered we TEST:
        # `.get_final_state_information()` AND `get_accepted_or_declined_item_offered()`
        # from ProjectActivity and ProjectArticle
        activity_final_state_information_when_accepted = self.activity_offered.get_final_state_information()
        article_final_state_information_when_accepted = self.article_offered.get_final_state_information()

        assert activity_final_state_information_when_accepted['date'] == self.offer.offer_date
        assert activity_final_state_information_when_accepted['offer']['public_id'] == self.offer.public_id
        assert activity_final_state_information_when_accepted['offer_status'] == self.offer.offer_status
        assert activity_final_state_information_when_accepted['offer_type'] == "project_offer"

        assert article_final_state_information_when_accepted['date'] == self.offer.offer_date
        assert article_final_state_information_when_accepted['offer']['public_id'] == self.offer.public_id
        assert article_final_state_information_when_accepted['offer_status'] == self.offer.offer_status
        assert article_final_state_information_when_accepted['offer_type'] == "project_offer"

    def test_get_final_state_information_when_declined(self):
        self.offer.change_status_to_declined()
        self.activity_offered.refresh_from_db()
        self.article_offered.refresh_from_db()
        self.project_activity.refresh_from_db()
        self.project_article.refresh_from_db()

        # With `.get_final_state_information()` from ActivityOffered and ArticleOffered we TEST:
        # `.get_final_state_information()` AND `get_accepted_or_declined_item_offered()`
        # from ProjectActivity and ProjectArticle
        activity_final_state_information_when_declined = self.activity_offered.get_final_state_information()
        article_final_state_information_when_declined = self.article_offered.get_final_state_information()

        assert activity_final_state_information_when_declined['date'] == self.offer.offer_date
        assert activity_final_state_information_when_declined['offer']['public_id'] == self.offer.public_id
        assert activity_final_state_information_when_declined['offer_status'] == self.offer.offer_status
        assert activity_final_state_information_when_declined['offer_type'] == "project_offer"

        assert article_final_state_information_when_declined['date'] == self.offer.offer_date
        assert article_final_state_information_when_declined['offer']['public_id'] == self.offer.public_id
        assert article_final_state_information_when_declined['offer_status'] == self.offer.offer_status
        assert article_final_state_information_when_declined['offer_type'] == "project_offer"


class TestFinalStateProjectOffer(TestCase):

    @classmethod
    def setUp(cls):
        cls.campaign = CampaignFactory()
        cls.project = ProjectFactory(campaign=cls.campaign)
        cls.project_offer = OfferFactory(project=cls.project)
        cls.campaign_offer = CampaignOfferFactory(campaign=cls.campaign)
        cls.campaign_offer.project_offers.add(cls.project_offer)
        cls.campaign_offer.change_status_to_sent()
        cls.project_offer.refresh_from_db()

    def test_get_final_state(self):
        assert self.project_offer.offer_status == OfferStatus.SENT
        assert not self.project_offer.get_final_state()

        self.campaign_offer.change_status_to_accepted()
        self.project_offer.refresh_from_db()

        assert self.project_offer.offer_status == OfferStatus.ACCEPTED
        assert self.project_offer.get_final_state()

    def test_get_final_state_information_when_accepted(self):
        assert self.project_offer.offer_status == OfferStatus.SENT
        assert not self.project_offer.get_final_state_information()

        self.campaign_offer.change_status_to_accepted()
        self.project_offer.refresh_from_db()

        assert self.project_offer.offer_status == OfferStatus.ACCEPTED
        final_state_information_response = self.project_offer.get_final_state_information()
        assert final_state_information_response['date'] == self.campaign_offer.offer_date
        assert final_state_information_response['offer']['public_id'] == self.campaign_offer.public_id
        assert final_state_information_response['offer_status'] == self.campaign_offer.campaign_status
        assert final_state_information_response['offer_type'] == "campaign_offer"

    def test_get_final_state_information_when_declined(self):
        assert self.project_offer.offer_status == OfferStatus.SENT
        assert not self.project_offer.get_final_state_information()

        self.campaign_offer.change_status_to_declined()
        self.project_offer.refresh_from_db()

        assert self.project_offer.offer_status == OfferStatus.DECLINED
        final_state_information_when_declined = self.project_offer.get_final_state_information()
        assert final_state_information_when_declined['date'] == self.campaign_offer.offer_date
        assert final_state_information_when_declined['offer']['public_id'] == self.campaign_offer.public_id
        assert final_state_information_when_declined['offer_status'] == self.campaign_offer.campaign_status
        assert final_state_information_when_declined['offer_type'] == "campaign_offer"


class TestOfferModelSoftDelete(TestCase):

    def test_delete(self):
        project = ProjectFactory(billing_currency="CHF")
        offer = OfferFactory(project=project)
        offer2 = OfferFactory(project=project)
        self.assertEqual(Offer.objects.count(), 2)
        offer2.delete()
        self.assertEqual(Offer.objects.count(), 1)
        self.assertEqual(Offer.all_objects.count(), 2)

    def test_public_id_after_deletion(self):
        project = ProjectFactory(billing_currency="CHF")
        offer = OfferFactory(project=project)
        offer2 = OfferFactory(project=project)
        old_public_id = int(offer2.public_id[-1])
        offer2.delete()
        offer3 = OfferFactory(project=project)
        self.assertEqual(old_public_id + 1, int(offer3.public_id[-1]))
        # restore the deleted
        offer2.save()
        self.assertEqual(Offer.objects.count(), 3)
        self.assertEqual(Offer.all_objects.count(), 3)

    def test_cascade_deletion_because_of_project(self):
        project = ProjectFactory(billing_currency="CHF")
        offer = OfferFactory(project=project)
        offer2 = OfferFactory(project=project)
        self.assertEqual(Offer.objects.count(), 2)
        project.delete()
        self.assertEqual(Offer.objects.count(), 0)
        self.assertEqual(Offer.all_objects.count(), 2)
