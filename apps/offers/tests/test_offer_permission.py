import pytest
from django.urls import reverse
from rest_framework import status
from apps.offers.tests.factories import OfferFactory, CampaignOfferFactory
from apps.metronom_commons.test import APIRestAuthJWTClient
from apps.offers.tests.test_api_offer_get import OfferBaseClass
from apps.offers.tests.test_api_campaign_offer_post import TestCampaignOffersBase
from copy import deepcopy


class TestProjectOfferPermission(OfferBaseClass):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        # `project_owner` is also the Offer Manager
        cls.verify_user_email(cls.project_owner)
        cls.offer_manager_client = APIRestAuthJWTClient()
        cls.login_as_user(cls.offer_manager_client, cls.project_owner)

    def test_create_permission(self):
        offer_data = deepcopy(self.data)
        default_user_offer_data = deepcopy(self.data)
        default_user_offer_data['offer_manager'] = str(self.user.id)
        create_offer_url = reverse('api_projects:offer-list', args=[self.project.id])

        admin_post_response = self.admin_client.post(create_offer_url, data=offer_data, format='json')
        hr_post_response = self.hr_client.post(create_offer_url, data=offer_data, format='json')
        backoffice_post_response = self.backoffice_client.post(create_offer_url, data=offer_data, format='json')
        accounting_post_response = self.accounting_client.post(create_offer_url, data=offer_data, format='json')
        offer_manager_post_response = self.offer_manager_client.post(create_offer_url, data=offer_data, format='json')
        user_post_response = self.user_client.post(create_offer_url, data=default_user_offer_data, format='json')

        # HTTP_201_CREATED: ADMIN, ACCOUNTING,
        assert admin_post_response.status_code == status.HTTP_201_CREATED
        assert accounting_post_response.status_code == status.HTTP_201_CREATED
        assert offer_manager_post_response.status_code == status.HTTP_201_CREATED


        # HTTP_403_FORBIDDEN: default USER
        assert hr_post_response.status_code == status.HTTP_403_FORBIDDEN
        assert backoffice_post_response.status_code == status.HTTP_403_FORBIDDEN
        assert user_post_response.status_code == status.HTTP_403_FORBIDDEN

    def test_read_permission(self):
        already_created_offer = OfferFactory(project=self.project, offer_manager=self.project_owner)
        already_created_offer_url = reverse('api_projects:offer-list', args=[self.project.id])

        admin_response = self.admin_client.get(already_created_offer_url)
        hr_response = self.hr_client.get(already_created_offer_url)
        backoffice_response = self.backoffice_client.get(already_created_offer_url)
        accounting_response = self.accounting_client.get(already_created_offer_url)
        user_response = self.user_client.get(already_created_offer_url)
        offer_manager_response = self.offer_manager_client.get(already_created_offer_url)

        # HTTP_200_OK
        assert admin_response.status_code == status.HTTP_200_OK
        assert hr_response.status_code == status.HTTP_200_OK
        assert backoffice_response.status_code == status.HTTP_200_OK
        assert accounting_response.status_code == status.HTTP_200_OK
        assert user_response.status_code == status.HTTP_200_OK
        assert offer_manager_response.status_code == status.HTTP_200_OK

    def test_update_permission(self):
        offer_to_patch = OfferFactory(project=self.project, offer_manager=self.project_owner)
        offer_to_patch_url = reverse('api_projects:offer-detail', args=[self.project.id, offer_to_patch.id])

        admin_patch_response = self.admin_client.patch(
            offer_to_patch_url, data={"name": "Admin Permissions"}, format='json'
        )
        hr_patch_response = self.hr_client.patch(
            offer_to_patch_url, data={"name": "HR Permissions"}, format='json'
        )
        backoffice_patch_response = self.backoffice_client.patch(
            offer_to_patch_url, data={"name": "Backoffice Permissions"}, format='json'
        )
        accounting_patch_response = self.accounting_client.patch(
            offer_to_patch_url, data={"name": "Accounting Permissions"}, format='json'
        )
        offer_manager_patch_response = self.offer_manager_client.patch(
            offer_to_patch_url, data={"name": "OfferManager Permissions"}, format='json'
        )
        user_patch_response = self.user_client.patch(
            offer_to_patch_url, data={"name": "User Permissions"}, format='json'
        )

        # HTTP_200_OK: ADMIN, ACCOUNTING,
        assert admin_patch_response.status_code == status.HTTP_200_OK
        assert accounting_patch_response.status_code == status.HTTP_200_OK
        assert offer_manager_patch_response.status_code == status.HTTP_200_OK

        # HTTP_403_FORBIDDEN: default USER
        assert hr_patch_response.status_code == status.HTTP_403_FORBIDDEN
        assert backoffice_patch_response.status_code == status.HTTP_403_FORBIDDEN
        assert self.user != self.project_owner
        assert user_patch_response.status_code == status.HTTP_403_FORBIDDEN

    def test_admin_destroy_permission(self):
        offer_for_admin = OfferFactory(project=self.project, offer_manager=self.project_owner)
        offer_for_admin_url = reverse('api_projects:offer-detail', args=[self.project.id, offer_for_admin.id])
        admin_delete_response = self.admin_client.delete(offer_for_admin_url, format='json')
        assert admin_delete_response.status_code == status.HTTP_204_NO_CONTENT

    def test_hr_destroy_permission(self):
        offer_for_hr = OfferFactory(project=self.project, offer_manager=self.project_owner)
        offer_for_hr_url = reverse('api_projects:offer-detail', args=[self.project.id, offer_for_hr.id])
        hr_delete_response = self.hr_client.delete(offer_for_hr_url, format='json')
        assert hr_delete_response.status_code == status.HTTP_403_FORBIDDEN

    def test_backoffice_destroy_permission(self):
        offer_for_backoffice = OfferFactory(project=self.project, offer_manager=self.project_owner)
        offer_for_backoffice_url = reverse('api_projects:offer-detail', args=[self.project.id, offer_for_backoffice.id])
        backoffice_delete_response = self.backoffice_client.delete(offer_for_backoffice_url, format='json')
        assert backoffice_delete_response.status_code == status.HTTP_403_FORBIDDEN

    def test_accounting_destroy_permission(self):
        offer_for_accounting = OfferFactory(project=self.project, offer_manager=self.project_owner)
        offer_for_accounting_url = reverse('api_projects:offer-detail', args=[self.project.id, offer_for_accounting.id])
        accounting_delete_response = self.accounting_client.delete(offer_for_accounting_url, format='json')
        assert accounting_delete_response.status_code == status.HTTP_204_NO_CONTENT

    def test_offer_manager_destroy_permission(self):
        offer_for_offer_manager = OfferFactory(project=self.project, offer_manager=self.project_owner)
        offer_for_offer_manager_url = reverse('api_projects:offer-detail', args=[self.project.id, offer_for_offer_manager.id])
        offer_manager_delete_response = self.offer_manager_client.delete(offer_for_offer_manager_url, format='json')
        assert offer_manager_delete_response.status_code == status.HTTP_204_NO_CONTENT

    def test_user_destroy_permission(self):
        offer_for_user = OfferFactory(project=self.project, offer_manager=self.project_owner)
        offer_for_user_url = reverse('api_projects:offer-detail', args=[self.project.id, offer_for_user.id])
        user_delete_response = self.user_client.delete(offer_for_user_url, format='json')
        assert user_delete_response.status_code == status.HTTP_403_FORBIDDEN


class TestCampaignOfferPermission(TestCampaignOffersBase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        # `project_owner` is also the Offer Manager
        cls.verify_user_email(cls.project_owner)
        cls.offer_manager_client = APIRestAuthJWTClient()
        cls.login_as_user(cls.offer_manager_client, cls.project_owner)

    def test_create_permission(self):
        campaign_offer_data = deepcopy(self.campaign_data)
        default_user_campaign_offer_data = deepcopy(self.campaign_data)
        default_user_campaign_offer_data['offer_manager'] = str(self.test_user.id)
        create_campaign_offer_url = reverse('api_campaigns:offer-list', args=[self.campaign.id])

        admin_post_response = self.admin_client.post(
            create_campaign_offer_url, data=campaign_offer_data, format='json'
        )
        hr_post_response = self.hr_client.post(
            create_campaign_offer_url, data=campaign_offer_data, format='json'
        )
        backoffice_post_response = self.backoffice_client.post(
            create_campaign_offer_url, data=campaign_offer_data, format='json'
        )
        accounting_post_response = self.accounting_client.post(
            create_campaign_offer_url, data=campaign_offer_data, format='json'
        )
        offer_manager_post_response = self.offer_manager_client.post(
            create_campaign_offer_url, data=campaign_offer_data, format='json'
        )
        user_post_response = self.user_client.post(
            create_campaign_offer_url, data=default_user_campaign_offer_data, format='json'
        )
        default_user_post_response = self.user_client.post(
            create_campaign_offer_url, data=campaign_offer_data, format='json'
        )

        # HTTP_201_CREATED: ADMIN, ACCOUNTING,
        assert admin_post_response.status_code == status.HTTP_201_CREATED
        assert accounting_post_response.status_code == status.HTTP_201_CREATED
        assert offer_manager_post_response.status_code == status.HTTP_201_CREATED

        # HTTP_403_FORBIDDEN: default USER
        assert hr_post_response.status_code == status.HTTP_403_FORBIDDEN
        assert backoffice_post_response.status_code == status.HTTP_403_FORBIDDEN
        assert user_post_response.status_code == status.HTTP_403_FORBIDDEN
        assert default_user_post_response.status_code == status.HTTP_403_FORBIDDEN

    def test_update_permission(self):
        campaign_offer_to_patch = CampaignOfferFactory(campaign=self.campaign, offer_manager=self.project_owner)
        campaign_offer_to_patch_url = reverse(
            'api_campaigns:offer-detail', args=[self.campaign.id, campaign_offer_to_patch.id]
        )
        admin_patch_response = self.admin_client.patch(
            campaign_offer_to_patch_url, data={"name": "Admin Campaign Offer Permissions"}, format='json'
        )
        hr_patch_response = self.hr_client.patch(
            campaign_offer_to_patch_url, data={"name": "HR Campaign Offer Permissions"}, format='json'
        )
        backoffice_patch_response = self.backoffice_client.patch(
            campaign_offer_to_patch_url, data={"name": "Backoffice Campaign Offer Permissions"}, format='json'
        )
        accounting_patch_response = self.accounting_client.patch(
            campaign_offer_to_patch_url, data={"name": "Accounting Campaign Offer Permissions"}, format='json'
        )
        offer_manager_patch_response = self.offer_manager_client.patch(
            campaign_offer_to_patch_url, data={"name": "OfferManager Campaign Offer Permissions"}, format='json'
        )
        user_patch_response = self.user_client.patch(
            campaign_offer_to_patch_url, data={"name": "User Campaign Offer Permissions"}, format='json'
        )

        # HTTP_200_OK: ADMIN, ACCOUNTING,
        assert admin_patch_response.status_code == status.HTTP_200_OK
        assert accounting_patch_response.status_code == status.HTTP_200_OK
        assert offer_manager_patch_response.status_code == status.HTTP_200_OK

        # HTTP_403_FORBIDDEN: default USER
        assert hr_patch_response.status_code == status.HTTP_403_FORBIDDEN
        assert backoffice_patch_response.status_code == status.HTTP_403_FORBIDDEN
        assert self.test_user != self.project_owner
        assert user_patch_response.status_code == status.HTTP_403_FORBIDDEN

    def test_admin_destroy_permission(self):
        campaign_offer_for_admin_to_delete = CampaignOfferFactory(
            campaign=self.campaign, offer_manager=self.project_owner
        )
        campaign_offer_for_admin_to_delete_url = reverse(
            'api_campaigns:offer-detail', args=[self.campaign.id, campaign_offer_for_admin_to_delete.id]
        )
        admin_delete_response = self.admin_client.delete(campaign_offer_for_admin_to_delete_url, format='json')
        assert admin_delete_response.status_code == status.HTTP_204_NO_CONTENT

    def test_hr_destroy_permission(self):
        campaign_offer_for_hr_to_delete = CampaignOfferFactory(
            campaign=self.campaign, offer_manager=self.project_owner
        )
        campaign_offer_for_hr_to_delete_url = reverse(
            'api_campaigns:offer-detail', args=[self.campaign.id, campaign_offer_for_hr_to_delete.id]
        )
        hr_delete_response = self.hr_client.delete(campaign_offer_for_hr_to_delete_url, format='json')
        assert hr_delete_response.status_code == status.HTTP_403_FORBIDDEN

    def test_backoffice_destroy_permission(self):
        campaign_offer_for_backoffice_to_delete = CampaignOfferFactory(
            campaign=self.campaign, offer_manager=self.project_owner
        )
        campaign_offer_for_backoffice_to_delete_url = reverse(
            'api_campaigns:offer-detail', args=[self.campaign.id, campaign_offer_for_backoffice_to_delete.id]
        )
        backoffice_delete_response = self.backoffice_client.delete(
            campaign_offer_for_backoffice_to_delete_url, format='json'
        )
        assert backoffice_delete_response.status_code == status.HTTP_403_FORBIDDEN

    def test_accounting_destroy_permission(self):
        campaign_offer_for_accounting_to_delete = CampaignOfferFactory(
            campaign=self.campaign, offer_manager=self.project_owner
        )
        campaign_offer_for_accounting_to_delete_url = reverse(
            'api_campaigns:offer-detail', args=[self.campaign.id, campaign_offer_for_accounting_to_delete.id]
        )
        accounting_delete_response = self.accounting_client.delete(
            campaign_offer_for_accounting_to_delete_url, format='json'
        )
        assert accounting_delete_response.status_code == status.HTTP_204_NO_CONTENT

    def test_offer_manager_destroy_permission(self):
        campaign_offer_for_offer_manager_to_delete = CampaignOfferFactory(
            campaign=self.campaign, offer_manager=self.project_owner
        )
        campaign_offer_for_offer_manager_to_delete_url = reverse(
            'api_campaigns:offer-detail', args=[self.campaign.id, campaign_offer_for_offer_manager_to_delete.id]
        )
        offer_manager_delete_response = self.offer_manager_client.delete(
            campaign_offer_for_offer_manager_to_delete_url, format='json'
        )
        assert offer_manager_delete_response.status_code == status.HTTP_204_NO_CONTENT

    def test_user_destroy_permission(self):
        campaign_offer_for_user_to_delete = CampaignOfferFactory(
            campaign=self.campaign, offer_manager=self.project_owner
        )
        campaign_offer_for_user_to_delete_url = reverse(
            'api_campaigns:offer-detail', args=[self.campaign.id, campaign_offer_for_user_to_delete.id]
        )
        user_delete_response = self.user_client.delete(campaign_offer_for_user_to_delete_url, format='json')
        assert user_delete_response.status_code == status.HTTP_403_FORBIDDEN
