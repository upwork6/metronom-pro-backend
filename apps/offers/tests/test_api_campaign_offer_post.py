from apps.metronom_commons.models import ROLE
import copy

from django.urls import reverse
from rest_framework import status

from apps.backoffice.tests.factories import (TermsCollectionFactory,
                                             TermsParagraphCollectionFactory,
                                             TermsParagraphFactory)
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.offers.models import OfferStatus
from apps.offers.tests.factories import OfferFactory
from apps.persons.tests.factories import PersonFactory
from apps.projects.tests.factories import CampaignFactory, ProjectFactory
from apps.users.tests.factories import UserFactory


class TestCampaignOffersBase(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project_owner = UserFactory(email='offer_manager@test.com', first_name="John",
                                        password=cls.default_password, role=ROLE.USER)
        cls.campaign = CampaignFactory(billing_currency="CHF", campaign_owner=cls.project_owner)
        cls.project = ProjectFactory(campaign=cls.campaign)
        cls.offer1 = OfferFactory(project=cls.project)
        cls.offer2 = OfferFactory(project=cls.project)
        cls.contact_person = PersonFactory()
        cls.contact_person2 = PersonFactory()
        cls.project_owner2 = UserFactory()
        cls.campaign_title = "test"
        cls.used_customer_reference = "Know avoid raise high your family fight."
        cls.terms_collection = TermsCollectionFactory()

        paragraphs = TermsParagraphFactory.create_batch(size=3)
        TermsParagraphCollectionFactory(
            terms_paragraph=paragraphs[0], terms_collection=cls.terms_collection
        )
        TermsParagraphCollectionFactory(
            terms_paragraph=paragraphs[1], terms_collection=cls.terms_collection
        )
        TermsParagraphCollectionFactory(
            terms_paragraph=paragraphs[2], terms_collection=cls.terms_collection
        )
        cls.campaign_data = {
            "uuid": "c2c80382-2727-4bfd-91e5-99e11837aa9d",
            "terms_collection_uuid": str(cls.terms_collection.pk),
            "title": cls.campaign_title,
            "description": "description",
            # campaign_status will be ignored on POST ->
            # avoid sending wrong status.
            "campaign_status": OfferStatus.ACCEPTED,
            "used_customer_reference": cls.used_customer_reference,
            "contact_person": str(cls.contact_person.id),
            "offer_manager": str(cls.project_owner.id),
            "offer_date": "2018-01-13",
            "valid_until_date": "2018-03-13",
            "callback_date": "2018-02-13",
            "project_offers": [
                {
                    "uuid": str(cls.offer1.id),
                    "name": cls.offer1.name
                },
                {
                    "uuid": str(cls.offer2.id),
                    "name": cls.offer2.name
                },
            ],
            "tax": "8.0",
            "is_tax_included": False
        }
        cls.campaign_offer_endpoint = reverse('api_campaigns:offer-list', args=[cls.campaign.id])


class TestCampaignOfferPost(TestCampaignOffersBase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.campaign_response = cls.admin_client.post(
            cls.campaign_offer_endpoint,
            data=cls.campaign_data,
            format='json'
        )

    def test_post_campaign_offer_status(self):
        assert self.campaign_response.status_code == status.HTTP_201_CREATED

    def test_post_campaign_offer_title(self):
        expected = self.campaign_title
        response = self.campaign_response.data['title']
        assert expected == response

    def test_post_campaign_offer_project_offers_length(self):
        expected = 2
        response = len(self.campaign_response.data['project_offers'])
        assert expected == response

    def test_post_campaign_offer_project_offers_1_name(self):
        expected = self.offer1.name
        response = self.campaign_response.data['project_offers']
        for offer in response:
            if offer['uuid'] == str(self.offer1.id):
                assert expected == offer['name']

    def test_post_campaign_offer_is_optional_and_position(self):
        response = self.campaign_response.data['project_offers']
        for offer in response:
            self.assertIsNotNone(offer.get("is_optional"))

    def test_response_terms_collection_is_required(self):
        post_data = copy.copy(self.campaign_data)
        post_data.pop('terms_collection_uuid')
        response = self.admin_client.post(self.campaign_offer_endpoint, data=post_data, format='json')
        assert response.data.get('terms_collection_uuid') == ['This field is required.']

    def test_post_internal_project_offer_serializer_restriction(self):
        project = ProjectFactory(accounting_type='internal')
        offer1 = OfferFactory(project=project)
        campaign_data = copy.deepcopy(self.campaign_data)
        campaign_data["project_offers"] = [
            {
                "uuid": str(offer1.id),
                "name": offer1.name
            },
        ]
        response = self.admin_client.post(self.campaign_offer_endpoint, data=campaign_data, format='json')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data['error'],
            ["You can't add project offer to the campaign offer if project has internal accounting_type"]
        )
