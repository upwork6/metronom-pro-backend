import copy
from decimal import Decimal

import pytest
from django.urls import reverse
from rest_framework import status

from apps.offers.models import Offer
from apps.offers.tests.test_api_offer_get import OfferBaseClass


class TestOfferPost(OfferBaseClass):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.offers_url = reverse('api_projects:offer-list', args=[cls.project.id])
        cls.response = cls.admin_client.post(
            cls.offers_url,
            data=cls.data,
            format='json'
        )

        cls.no_title_data = cls.data.copy()
        cls.response_without_offer_manager = cls.data.copy()
        cls.empty_activities_data = cls.data.copy()
        cls.empty_articles_data = cls.data.copy()
        cls.without_activities_data = cls.data.copy()
        cls.without_articles_data = cls.data.copy()
        cls.response_without_offer_manager.pop('offer_manager')
        cls.no_title_data['name'] = ""
        cls.empty_activities_data['activities'] = []
        cls.empty_articles_data['articles'] = []
        cls.without_activities_data.pop('activities')
        cls.without_articles_data.pop('articles')
        cls.bad_response = cls.admin_client.post(cls.offers_url, data=cls.bad_data, format='json')
        cls.response_without_offer_manager = cls.admin_client.post(
            cls.offers_url,
            data=cls.response_without_offer_manager,
            format='json'
        )
        cls.empty_articles_response = cls.admin_client.post(
            cls.offers_url,
            data=cls.empty_articles_data,
            format='json'
        )
        cls.without_articles_response = cls.admin_client.post(
            cls.offers_url,
            data=cls.without_articles_data,
            format='json'
        )

    def test_post_offer_status_page(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_article_offered_name(self):
        self.project_activity1.display_name = ""
        self.project_activity1.save()
        for activity in self.response.data['activities']:
            if activity['project_activity'] == str(self.project_activity1.id):
                assert self.project_activity1.name == ''
                assert activity['name'] == self.project_activity1.name

    def test_post_offer_total_offer_without_tax(self):

        expected_content = sum(
            [Decimal(activity['offered_sum']) - Decimal(activity['discount']) for activity in self.data['activities']]
        ) + sum(
            [Decimal(article['offered_sum']) - Decimal(article['discount']) for article in self.data['articles']]
        )
        data_content = self.response.data['total_offer_without_tax']
        assert data_content == str(round(expected_content, 2))

    def test_post_offer_total_offer_with_tax(self):

        expected_value_without_tax = sum(
            [Decimal(activity['offered_sum']) - Decimal(activity['discount']) for activity in self.data['activities']]
        ) + sum(
            [Decimal(article['offered_sum']) - Decimal(article['discount']) for article in self.data['articles']]
        )

        expected_total_tax = expected_value_without_tax * Decimal(self.response.data['used_tax_rate']) / 100
        expected_value_with_tax = expected_value_without_tax + expected_total_tax

        assert self.response.data['total_tax'] == str(round(expected_total_tax, 2))
        assert self.response.data['total_offer_with_tax'] == str(round(expected_value_with_tax, 2))

    def test_bad_post_offer_status_code(self):
        bad_response = self.admin_client.post(self.offers_url, data=self.bad_data, format='json')
        assert bad_response.status_code == status.HTTP_400_BAD_REQUEST

    def test_post_offer_match_title(self):
        assert self.response.data['name'] == self.data['name']

    def test_post_offer_match_project_uuid(self):
        offer = Offer.objects.get(pk=self.response.data['uuid'])
        assert str(offer.project.id) == str(self.project.id)

    def test_post_offer_match_contact_person(self):
        data_content = self.response.data['contact_person']['uuid']
        expected_content = self.data['contact_person']
        assert data_content == expected_content

    def test_post_offer_match_offer_manager(self):
        data_content = self.response.data['offer_manager']['uuid']
        expected_content = self.data['offer_manager']
        assert data_content == expected_content

    def test_post_offer_match_activities(self):
        data_content = len(self.response.data['activities'])
        expected_content = len(self.data['activities'])
        assert data_content == expected_content

    def test_post_offer_match_articles(self):
        data_content = len(self.response.data['articles'])
        expected_content = len(self.data['articles'])
        assert data_content == expected_content

    def test_post_without_title(self):
        no_title_response = self.admin_client.post(
            self.offers_url,
            data=self.no_title_data,
            format='json'
        )
        assert no_title_response.data['name'] == self.empty_error_response['name']

    def test_post_empty_activities(self):
        empty_activities_response = self.admin_client.post(
            self.offers_url,
            data=self.empty_activities_data,
            format='json'
        )
        assert empty_activities_response.data['activities'] == list()

    def test_post_empty_articles(self):
        assert self.empty_articles_response.data['articles'] == list()

    def test_post_without_activities(self):
        without_activities_response = self.admin_client.post(
            self.offers_url,
            data=self.without_activities_data,
            format='json'
        )
        assert without_activities_response.data['activities'] == list()

    def test_post_without_articles(self):
        assert self.without_articles_response.data['articles'] == list()

    @pytest.mark.skip(reason="Decide the Logic when Create Offer with existent activity/article")
    def test_postOffer_with_existent_articles_activities(self):
        old_activityoffered = self.response.data['activities'][0].copy()
        old_articleoffered = self.response.data['articles'][0].copy()
        data2 = self.data.copy()
        data2['activities'] = [old_activityoffered]
        data2['articles'] = [old_articleoffered]
        response2 = self.admin_client.post(
            self.offers_url,
            data=data2,
            format='json'
        )
        assert response2.status_code == status.HTTP_201_CREATED
        assert response2.data['articles'][0]['uuid'] == old_articleoffered['uuid']

    def test_response_terms_collection_is_correct(self):
        assert str(self.response.data.get('terms_collection_uuid')) == str(self.terms_collection.pk)

    def test_response_terms_collection_is_required(self):
        post_data = copy.copy(self.data)
        post_data.pop('terms_collection_uuid')
        response = self.admin_client.post(self.offers_url, data=post_data, format='json')
        assert response.data.get('terms_collection_uuid') == self.error_response['terms_collection_uuid']
