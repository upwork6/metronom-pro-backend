
import random
from datetime import date, timedelta

import factory

from apps.backoffice.tests.factories import TermsCollectionFactory
from apps.offers.models import (ActivityOffered, ArticleOffered, CampaignOffer,
                                Offer, OfferStatus)
from apps.persons.tests.factories import PersonFactory
from apps.projects.tests.factories import (CampaignFactory,
                                           ProjectActivityFactory,
                                           ProjectArticleFactory,
                                           ProjectFactory)
from apps.users.tests.factories import UserFactory


class OfferFactory(factory.DjangoModelFactory):
    """
        Creates an Offer.

        Must be called explicit with a Project, e.g.

        self.project = ProjectFactory(billing_currency="CHF")
        self.offer = OfferFactory(project=self.project)

    """

    terms_collection = factory.SubFactory(TermsCollectionFactory)
    name = factory.Faker("sentence")
    offer_date = date.today()
    valid_until_date = date.today() + timedelta(weeks=2)
    callback_date = date.today() + timedelta(weeks=1)
    contact_person = factory.SubFactory(PersonFactory)
    offer_manager = factory.SubFactory(UserFactory)
    offer_description = factory.Faker("sentence")
    is_tax_included = random.choice([True, False])
    offer_status = OfferStatus.CREATED
    project = factory.SubFactory(ProjectFactory)

    class Meta:
        model = Offer


class ActivityOfferedFactory(factory.DjangoModelFactory):
    """
        Creates an Activity Offered.

        Must be called explicit with a Offer and a Project Activity, e.g.

        self.project = ProjectFactory(billing_currency="CHF")
        self.offer = OfferFactory(project=self.project)
        self.activity_offered = ProjectActivityFactory(project_activity=self.project_activity, offer=self.offer)
    """

    show_steps = True
    is_optional = False
    used_description = factory.Faker("sentence")
    project_activity = factory.SubFactory(ProjectActivityFactory)

    class Meta:
        model = ActivityOffered


class ArticleOfferedFactory(factory.DjangoModelFactory):
    """
        Creates an Article Offered.

        Must be called explicit with a Offer and a Project Article, e.g.

        self.project = ProjectFactory(billing_currency="CHF")
        self.offer = OfferFactory(project=self.project)
        self.article_offered = ProjectArticleFactory(project_activity=self.project_article, offer=self.offer)
    """

    is_external_cost = False
    is_optional = False
    project_article = factory.SubFactory(ProjectArticleFactory)

    class Meta:
        model = ArticleOffered


class CampaignOfferFactory(factory.DjangoModelFactory):
    """
        Creates an Campaign Offer

        Must be called explicit with a Offer and a Project Article, e.g.

        self.campaign = CampaignFactory(billing_currency="CHF")
        self.campaign_offer = CampaignOfferFactory(campaign=self.campaign)
    """

    title = factory.Faker("sentence")
    description = factory.Faker("sentence")
    offer_date = date.today()
    valid_until_date = date.today() + timedelta(weeks=2)
    callback_date = date.today() + timedelta(weeks=1)
    contact_person = factory.SubFactory(PersonFactory)
    offer_manager = factory.SubFactory(UserFactory)
    terms_collection = factory.SubFactory(TermsCollectionFactory)
    campaign_status = OfferStatus.CREATED
    campaign = factory.SubFactory(CampaignFactory)

    class Meta:
        model = CampaignOffer
