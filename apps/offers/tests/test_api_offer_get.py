from datetime import date, timedelta
from decimal import Decimal
from django.urls import reverse
from djmoney.money import Money
from rest_framework import status

from apps.backoffice.tests.factories import (TermsCollectionFactory,
                                             TermsParagraphCollectionFactory,
                                             TermsParagraphFactory)
from apps.companies.tests.factories import CompanyWithOfficeFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.offers.tests.factories import (ActivityOfferedFactory, ArticleOfferedFactory,
                                         OfferFactory, CampaignOfferFactory)
from apps.persons.tests.factories import PersonFactory
from apps.projects.tests.factories import (CampaignFactory,
                                           ProjectActivityFactory,
                                           ProjectActivityStepFactory,
                                           ProjectArticleFactory,
                                           ProjectFactory)
from apps.users.tests.factories import UserFactory
from apps.metronom_commons.models import ROLE
from apps.metronom_commons.choices_flow import OfferStatus
from apps.offers.models import Offer
import pytest


class OfferBaseClass(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company = CompanyWithOfficeFactory()
        cls.finishing_date = date.today() + timedelta(weeks=4)
        cls.delivery_date = date.today() + timedelta(weeks=6)
        cls.contact_person = PersonFactory()
        cls.project_owner = UserFactory(email='offer_manager@test.com', first_name="John",
                                        password=cls.default_password, role=ROLE.USER)
        cls.contact_person2 = PersonFactory()
        cls.project_owner2 = UserFactory()
        cls.billing_address = cls.company.offices.all()[0].address
        cls.currency = 'CHF'
        cls.effort_hours = Decimal("100.00")
        cls.budget1 = Money(Decimal("100.00"), cls.currency)
        cls.budget2 = Money(Decimal("50.00"), cls.currency)
        cls.user = UserFactory()
        cls.discount = Money(Decimal("1.00"), cls.currency)
        cls.campaign = CampaignFactory(company=cls.company)
        cls.project = ProjectFactory(
            title="Fantastic new Project",
            campaign=cls.campaign,
            project_owner=cls.project_owner
        )
        cls.project_activity1 = ProjectActivityFactory(project=cls.project)
        cls.project_activity2 = ProjectActivityFactory(project=cls.project)
        cls.project_article1 = ProjectArticleFactory(
            project=cls.project, budget_in_project_currency=cls.budget1
        )
        cls.project_article2 = ProjectArticleFactory(
            project=cls.project, budget_in_project_currency=cls.budget2
        )
        cls.project_activity_step = ProjectActivityStepFactory(
            activity=cls.project_activity1,
            planned_effort=cls.effort_hours,
        )
        cls.project_activity_step = ProjectActivityStepFactory(
            activity=cls.project_activity2,
            planned_effort=cls.effort_hours,
        )

        cls.terms_collection = TermsCollectionFactory()

        paragraphs = TermsParagraphFactory.create_batch(size=3)
        TermsParagraphCollectionFactory(
            terms_paragraph=paragraphs[0], terms_collection=cls.terms_collection
        )
        TermsParagraphCollectionFactory(
            terms_paragraph=paragraphs[1], terms_collection=cls.terms_collection
        )
        TermsParagraphCollectionFactory(
            terms_paragraph=paragraphs[2], terms_collection=cls.terms_collection
        )

        cls.bad_data = {}
        cls.offer_name = "Fantastic new Offer"
        cls.data = {
            'terms_collection_uuid': str(cls.terms_collection.pk),
            "name": cls.offer_name,
            "offer_date": "2018-01-13",
            "offer_description": "description for this Offer",
            "valid_until_date": "2018-02-12",
            "callback_date": "2018-01-23",
            "contact_person": str(cls.contact_person.id),
            "offer_manager": str(cls.project_owner.id),
            "is_tax_included": True,
            "activities": [
                {
                    "position": 1,
                    "show_steps": False,
                    "is_optional": False,
                    "offered_sum": str(cls.project_activity1.total_planned_budget.amount),
                    "has_subtotal": False,
                    "accepted_offer": {
                        "uuid": "d2b94830-65d1-4ab4-9b37-6d4d96b41cc3",
                        "name": "Testing Name"
                    },
                    "discount": str(Decimal(300)),
                    "project_activity": str(cls.project_activity1.id)
                },
                {
                    "position": 2,
                    "show_steps": False,
                    "is_optional": False,
                    "offered_sum": str(cls.project_activity2.total_planned_budget.amount),
                    "has_subtotal": False,
                    "accepted_offer": None,
                    "discount": str(Decimal(100)),
                    "project_activity": str(cls.project_activity2.id)
                }
            ],
            "articles": [
                {
                    "position": 3,
                    "is_external_cost": False,
                    "is_optional": False,
                    "offered_sum": str(cls.project_article1.total_planned_budget.amount),
                    "has_subtotal": False,
                    "accepted_offer": None,
                    "discount": str(Decimal(20)),
                    "project_article": str(cls.project_article1.id)
                },
                {
                    "position": 4,
                    "is_external_cost": False,
                    "is_optional": False,
                    "offered_sum": str(cls.project_article2.total_planned_budget.amount),
                    "has_subtotal": False,
                    "accepted_offer": None,
                    "discount": str(Decimal(20)),
                    "project_article": str(cls.project_article2.id)

                }
            ]
        }
        cls.error_response = {
            'terms_collection_uuid': ['This field is required.'],
            'name': ['This field is required.'],
            'contact_person': ['This field is required.'],
            'offer_manager': ['This field is required.'],
            'offer_description': ['This field is required.'],
            'activities': ['This field is required.'],
            'articles': ['This field is required.']
        }
        cls.empty_error_response = {
            "name": [
                "This field may not be blank."
            ],
            "offer_date": [
                "Date has wrong format. Use one of these formats instead: YYYY[-MM[-DD]]."
            ],
            "valid_until_date": [
                "Date has wrong format. Use one of these formats instead: YYYY[-MM[-DD]]."
            ],
            "callback_date": [
                "Date has wrong format. Use one of these formats instead: YYYY[-MM[-DD]]."
            ],
            "contact_person": [
                "This field may not be None."
            ],
            "offer_manager": [
                "This field may not be None."
            ]
        }


class TestOfferListGet(OfferBaseClass):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.offer1_name = "Offer 1"
        cls.offer2_name = "Offer 2"
        cls.title = "Fantastic new Project"
        cls.offer2 = OfferFactory(project=cls.project, name=cls.offer2_name)
        cls.offer1 = OfferFactory(project=cls.project, name=cls.offer1_name, terms_collection=cls.terms_collection)
        cls.offer_activity2 = ActivityOfferedFactory(
            offer=cls.offer1, project_activity=cls.project_activity2
        )
        cls.offer_activity1 = ActivityOfferedFactory(
            offer=cls.offer1, project_activity=cls.project_activity1
        )
        cls.offer_article2 = ArticleOfferedFactory(
            offer=cls.offer1,
            project_article=cls.project_article2,

        )
        cls.offer_article1 = ArticleOfferedFactory(
            offer=cls.offer1, project_article=cls.project_article1,
        )

        cls.offers_url = reverse('api_projects:offer-list', args=[cls.project.id])
        cls.response = cls.admin_client.get(cls.offers_url)
        cls.bad_response = cls.admin_client.put(cls.offers_url, data={'name': 'Sample'}, format='json')

    def test_get_offer_status_page(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_offer_bad_response(self):
        assert self.bad_response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_get_offers_page_content(self):
        names = [self.response.data[0]['name'], self.response.data[1]['name']]
        assert self.offer1_name in names
        assert self.offer2_name in names

    def test_get_offers_activity_list(self):
        activities_count = [len(self.response.data[0]['activities']), len(self.response.data[1]['activities'])]
        assert self.offer1.activities.count() in activities_count
        assert self.offer2.activities.count() in activities_count

    def test_get_offers_article_list(self):
        articles_count = [len(self.response.data[0]['articles']), len(self.response.data[1]['articles'])]
        assert self.offer1.articles.count() in articles_count
        assert self.offer2.articles.count() in articles_count

    def test_get_offers_activity_offered_sum(self):
        offer1, content_from_request = None, None
        for offer in self.response.data:
            if offer['uuid'] == str(self.offer1.pk):
                offer1 = offer
        for activity in offer1['activities']:
            if activity['uuid'] == str(self.offer_activity1.pk):
                content_from_request = Decimal(activity['offered_sum'])
        content_from_db = Decimal(self.offer_activity1.offered_sum.amount)
        assert content_from_request == content_from_db

    def test_get_offers_article_offered_sum(self):
        offer1, content_from_request = None, None
        for offer in self.response.data:
            if offer['uuid'] == str(self.offer1.pk):
                offer1 = offer
        for article in offer1['articles']:
            if article['uuid'] == str(self.offer_article1.pk):
                content_from_request = Decimal(article['offered_sum'])
        content_from_db = Decimal(self.offer_article1.offered_sum.amount)
        assert content_from_request == content_from_db

    def test_public_id(self):
        self.assertTrue(all([x.get('public_id') for x in self.response.data]))

    def test_response_terms_collection_present(self):
        for item in self.response.data:
            self.assertTrue(all([term is not None for term in str(item.get('terms_collection_uuid'))]))

    def test_response_terms_collection_is_correct(self):
        terms = [str(self.response.data[0].get('terms_collection_uuid')),
                 str(self.response.data[1].get('terms_collection_uuid'))]
        assert str(self.offer1.terms_collection.pk) in terms
        assert str(self.offer2.terms_collection.pk) in terms


class TestActivitySentAcceptedInOfferAndDate(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_activity1 = ProjectActivityFactory(project=cls.project)
        cls.project_activity2 = ProjectActivityFactory(project=cls.project)
        cls.project_article1 = ProjectArticleFactory(project=cls.project)
        cls.project_article2 = ProjectArticleFactory(project=cls.project)
        cls.offer1 = OfferFactory(project=cls.project)
        cls.activity_offered1_1 = ActivityOfferedFactory(
            project_activity=cls.project_activity1,
            offer=cls.offer1
        )
        cls.activity_offered1_2 = ActivityOfferedFactory(
            project_activity=cls.project_activity2,
            offer=cls.offer1
        )
        cls.article_offered1_1 = ArticleOfferedFactory(
            project_article=cls.project_article1,
            offer=cls.offer1
        )
        cls.article_offered1_2 = ArticleOfferedFactory(
            project_article=cls.project_article2,
            offer=cls.offer1
        )
        cls.offer2 = OfferFactory(project=cls.project)
        cls.activity_offered2_1 = ActivityOfferedFactory(
            project_activity=cls.project_activity1,
            offer=cls.offer2
        )
        cls.article_offered2_1 = ArticleOfferedFactory(
            project_article=cls.project_article1,
            offer=cls.offer2
        )
        cls.endpoint = reverse('api_projects:offer-list', args=[cls.project.id])

    def test_no_offer_sent_no_accepted_no_accepted_date(self):
        response = self.admin_client.get(self.endpoint)
        assert not response.data[0]['activities'][0]['sent_in_project_offers']
        assert not response.data[0]['articles'][0]['sent_in_project_offers']

    def test_sent_in_offer_no_accepted_no_accepted_date(self):
        self.offer1.change_status_to_sent()
        response = self.admin_client.get(self.endpoint)
        assert len(response.data[0]['activities'][0]['sent_in_project_offers']) == 1
        assert len(response.data[0]['articles'][0]['sent_in_project_offers']) == 1

        offer_uuid = str(self.offer1.pk)
        sent_in_offer_uuid = response.data[0]['activities'][0]['sent_in_project_offers'][0]['uuid']

        assert offer_uuid == sent_in_offer_uuid

    def test_offer_sent_offer_accepted_accepted_date(self):
        self.offer1.change_status_to_sent()
        self.offer2.change_status_to_sent()

        self.offer2.change_status_to_accepted()
        self.offer2.refresh_from_db()

        response = self.admin_client.get(self.endpoint)
        offer_1_response = [offer for offer in response.data if offer['uuid'] == str(self.offer1.pk)][0]
        offer_2_response = [offer for offer in response.data if offer['uuid'] == str(self.offer2.pk)][0]

        activity_to_check = offer_2_response['activities'][0]
        article_to_check = offer_2_response['articles'][0]

        assert len(activity_to_check['sent_in_project_offers']) == 2
        assert len(article_to_check['sent_in_project_offers']) == 2

        activity_sent_in_offer_uuids = [item['uuid'] for item in activity_to_check['sent_in_project_offers']]
        article_sent_in_offer_uuids = [item['uuid'] for item in article_to_check['sent_in_project_offers']]
        assert offer_1_response['uuid'] in activity_sent_in_offer_uuids
        assert offer_1_response['uuid'] in article_sent_in_offer_uuids
        assert offer_2_response['uuid'] in activity_sent_in_offer_uuids
        assert offer_2_response['uuid'] in article_sent_in_offer_uuids

        assert article_to_check['final_state']
        assert activity_to_check['final_state']

        assert activity_to_check['final_state_information']['date'] == self.offer2.offer_date
        assert activity_to_check['final_state_information']['offer']['public_id'] == self.offer2.public_id
        assert activity_to_check['final_state_information']['offer_status'] == self.offer2.offer_status
        assert activity_to_check['final_state_information']['offer_type'] == "project_offer"

        assert article_to_check['final_state_information']['date'] == self.offer2.offer_date
        assert article_to_check['final_state_information']['offer']['public_id'] == self.offer2.public_id
        assert article_to_check['final_state_information']['offer_status'] == self.offer2.offer_status
        assert article_to_check['final_state_information']['offer_type'] == "project_offer"


class TestOfferSentAcceptedInCampaignOfferAndDate(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.campaign = CampaignFactory(billing_currency="CHF")
        cls.project = ProjectFactory(billing_currency="CHF", campaign=cls.campaign)
        cls.offer1 = OfferFactory(project=cls.project)
        cls.offer2 = OfferFactory(project=cls.project)
        cls.campaign_offer2 = CampaignOfferFactory(campaign=cls.campaign)
        cls.campaign_offer1 = CampaignOfferFactory(campaign=cls.campaign)
        cls.campaign_offer2.project_offers.add(cls.offer1)
        cls.campaign_offer1.project_offers.add(cls.offer1, cls.offer2)
        cls.endpoint = reverse('api:campaign-detail', args=[cls.campaign.id])

    def test_no_campaign_offer_sent_no_accepted_no_accepted_date(self):
        response = self.admin_client.get(self.endpoint)
        campaign_offer1 = None
        for campaign in response.data['campaign_offers']:
            if campaign['uuid'] == str(self.campaign_offer1.pk):
                campaign_offer1 = campaign
        offer1 = campaign_offer1['project_offers'][0].copy()
        offer2 = campaign_offer1['project_offers'][1].copy()

        assert not offer1['sent_in_campaign_offers']
        assert not offer2['sent_in_campaign_offers']
        assert not offer2['final_state']

    def test_sent_in_campaign_offer_no_accepted_no_accepted_date(self):
        self.campaign_offer1.change_status_to_sent()
        self.campaign_offer2.change_status_to_sent()
        sent_response = self.admin_client.get(self.endpoint)
        sent_offer1, sent_offer2 = None, None
        campaign_offer1 = [campaign for campaign in sent_response.data['campaign_offers']
                           if campaign['uuid'] == str(self.campaign_offer1.pk)][0]
        for offer in campaign_offer1['project_offers']:
            if offer['uuid'] == str(self.offer1.pk):
                sent_offer1 = offer
            else:
                sent_offer2 = offer

        assert len(sent_offer1['sent_in_campaign_offers']) == 2
        assert len(sent_offer2['sent_in_campaign_offers']) == 1

        assert str(self.campaign_offer1.pk) in [campaign['uuid'] for campaign in sent_offer1['sent_in_campaign_offers']]
        assert str(self.campaign_offer2.pk) in [campaign['uuid'] for campaign in
                                                sent_offer1['sent_in_campaign_offers']]
        assert str(self.campaign_offer1.pk) in [campaign['uuid'] for campaign in sent_offer2['sent_in_campaign_offers']]

    def test_campaign_offer_sent_campaign_offer_accepted_accepted_date(self):

        self.campaign_offer2.change_status_to_sent()
        self.campaign_offer1.change_status_to_sent()
        self.campaign_offer1.refresh_from_db()
        self.campaign_offer1.change_status_to_accepted()
        accepted_response = self.admin_client.get(self.endpoint)
        accepted_campaign_offer1 = [campaign for campaign in accepted_response.data['campaign_offers'] if
                                    campaign['uuid'] == str(self.campaign_offer1.pk)][0]
        accepted_offer1, accepted_offer2 = None, None
        for offer in accepted_campaign_offer1['project_offers']:
            if offer['uuid'] == str(self.offer1.pk):
                accepted_offer1 = offer
            else:
                accepted_offer2 = offer

        assert len(accepted_offer1['sent_in_campaign_offers']) == 2
        assert len(accepted_offer2['sent_in_campaign_offers']) == 1

        campaign_offer_uuids1 = [i['uuid'] for i in accepted_offer1['sent_in_campaign_offers']]
        campaign_offer_uuids2 = [i['uuid'] for i in accepted_offer2['sent_in_campaign_offers']]
        assert str(self.campaign_offer1.pk) in campaign_offer_uuids1
        assert str(self.campaign_offer2.pk) in campaign_offer_uuids1
        assert str(self.campaign_offer1.pk) in campaign_offer_uuids2
        assert str(self.campaign_offer2.pk) not in campaign_offer_uuids2

        assert accepted_offer2['final_state']
        assert accepted_offer2['final_state_information']['date'] == self.campaign_offer1.offer_date
        assert accepted_offer2['final_state_information']['offer']['public_id'] == self.campaign_offer1.public_id
        assert accepted_offer2['final_state_information']['offer_status'] == self.campaign_offer1.campaign_status
        assert accepted_offer2['final_state_information']['offer_type'] == "campaign_offer"


class TestOfferStatesGet(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_article = ProjectArticleFactory(project=cls.project)
        cls.offer = OfferFactory(project=cls.project)
        cls.activity_offered = ActivityOfferedFactory(
            project_activity=cls.project_activity,
            offer=cls.offer
        )
        cls.article_offered = ArticleOfferedFactory(
            project_article=cls.project_article,
            offer=cls.offer
        )
        cls.offer_endpoint = reverse('api_projects:offer-list', args=[cls.project.id])

    def test_offer_created(self):
        response = self.admin_client.get(self.offer_endpoint)
        assert response.data[0]['activities'][0]['offer_status'] == 'created'
        assert response.data[0]['articles'][0]['offer_status'] == 'created'
        assert response.data[0]['offer_status'] == 'created'

    def test_offer_sent(self):
        self.offer.refresh_from_db()
        self.offer.change_status_to_sent()
        sent_response = self.admin_client.get(self.offer_endpoint)
        assert sent_response.data[0]['offer_status'] == 'sent'
        assert sent_response.data[0]['activities'][0]['offer_status'] == 'sent'
        assert sent_response.data[0]['articles'][0]['offer_status'] == 'sent'

    def test_offer_accepted(self):
        self.offer.change_status_to_sent()
        self.offer.change_status_to_accepted()
        sent_response = self.admin_client.get(self.offer_endpoint)
        assert sent_response.data[0]['offer_status'] == 'accepted'
        assert sent_response.data[0]['activities'][0]['offer_status'] == 'accepted'
        assert sent_response.data[0]['articles'][0]['offer_status'] == 'accepted'

    def test_offer_declined(self):
        self.offer.refresh_from_db()
        self.offer.change_status_to_sent()
        self.offer.change_status_to_declined()
        sent_response = self.admin_client.get(self.offer_endpoint)
        assert sent_response.data[0]['offer_status'] == 'declined'
        assert sent_response.data[0]['activities'][0]['offer_status'] == 'declined'
        assert sent_response.data[0]['articles'][0]['offer_status'] == 'declined'

    def test_offer_replaced(self):
        self.offer.refresh_from_db()
        self.offer.change_status_to_sent()
        self.offer.change_status_to_replaced()
        replaced_response = self.admin_client.get(self.offer_endpoint)
        new_offer, replaced_offer = None, None
        for offer in replaced_response.data:
            if offer['uuid'] == str(self.offer.pk):
                replaced_offer = offer
            else:
                new_offer = offer
        assert self.offer.activities.count() == 1
        assert new_offer['offer_status'] == 'created'
        assert new_offer['activities'][0]['offer_status'] == 'improvable'
        assert new_offer['articles'][0]['offer_status'] == 'improvable'

        assert replaced_offer['offer_status'] == 'replaced'
        assert self.offer.activities.first().offer_status == 'replaced'
        assert self.offer.articles.first().offer_status == 'replaced'

    def test_offer_partly_accepted(self):
        self.offer.refresh_from_db()
        self.offer.change_status_to_sent()
        self.activity_offered.refresh_from_db()
        self.article_offered.refresh_from_db()
        self.activity_offered.change_status_to_declined()
        self.article_offered.change_status_to_accepted()
        sent_response = self.admin_client.get(self.offer_endpoint)
        assert sent_response.data[0]['offer_status'] == 'sent'
        assert sent_response.data[0]['activities'][0]['offer_status'] == 'declined'
        assert sent_response.data[0]['articles'][0]['offer_status'] == 'accepted'


class TestCampaignOfferStatesGet(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.campaign = CampaignFactory(billing_currency="CHF")
        cls.project = ProjectFactory(billing_currency="CHF", campaign=cls.campaign)
        cls.campaign_offer = CampaignOfferFactory(campaign=cls.campaign)
        cls.offer = OfferFactory(project=cls.project)
        cls.campaign_offer.project_offers.add(cls.offer)
        cls.campaign_endpoint = reverse('api:campaign-detail', args=[cls.campaign.id])

    def test_offer_created(self):
        response = self.admin_client.get(self.campaign_endpoint)
        assert response.data['campaign_offers'][0]['campaign_status'] == 'created'
        assert response.data['campaign_offers'][0]['project_offers'][0]['offer_status'] == 'created'

    def test_campaign_offer_sent(self):
        self.campaign_offer.change_status_to_sent()
        sent_response = self.admin_client.get(self.campaign_endpoint)
        assert sent_response.data['campaign_offers'][0]['campaign_status'] == 'sent'
        assert sent_response.data['campaign_offers'][0]['project_offers'][0]['offer_status'] == 'sent'

    def test_campaign_offer_accepted(self):
        self.campaign_offer.change_status_to_sent()
        self.campaign_offer.change_status_to_accepted()
        accepted_response = self.admin_client.get(self.campaign_endpoint)
        assert accepted_response.data['campaign_offers'][0]['campaign_status'] == 'accepted'
        assert accepted_response.data['campaign_offers'][0]['project_offers'][0]['offer_status'] == 'accepted'

    def test_campaign_offer_declined(self):
        self.campaign_offer.change_status_to_sent()
        self.campaign_offer.change_status_to_declined()
        declined_response = self.admin_client.get(self.campaign_endpoint)
        assert declined_response.data['campaign_offers'][0]['campaign_status'] == 'declined'
        assert declined_response.data['campaign_offers'][0]['project_offers'][0][
            'offer_status'] == 'declined'

    def test_campaign_offer_replaced(self):
        self.campaign_offer.change_status_to_sent()
        self.campaign_offer.change_status_to_replaced()
        replaced_response = self.admin_client.get(self.campaign_endpoint)
        replaced_campaign, new_campaign = None, None
        for campaign in replaced_response.data['campaign_offers']:
            if campaign['uuid'] == str(self.campaign_offer.pk):
                replaced_campaign = campaign
            else:
                new_campaign = campaign

        assert new_campaign['campaign_status'] == 'created'
        assert new_campaign['project_offers'][0][
            'offer_status'] == 'created'

        assert replaced_campaign['campaign_status'] == 'replaced'
        assert len(replaced_campaign['project_offers']) == 1
        assert replaced_campaign['project_offers'][0][
            'offer_status'] == 'replaced'

    def test_campaign_offer_partly_accepted(self):
        offer2 = OfferFactory(project=self.project)
        self.campaign_offer.project_offers.add(offer2)
        self.campaign_offer.change_status_to_sent()
        offer2.change_status_to_declined()
        self.offer.change_status_to_accepted()
        partly_response = self.admin_client.get(self.campaign_endpoint)
        assert partly_response.data['campaign_offers'][0]['campaign_status'] == 'sent'
        offer_1, offer_2 = None, None
        for offer in partly_response.data['campaign_offers'][0]['project_offers']:
            if offer['uuid'] == str(self.offer.pk):
                offer_1 = offer
            if offer['uuid'] == str(offer2.pk):
                offer_2 = offer
        assert offer_1['offer_status'] == 'accepted'
        assert offer_2['offer_status'] == 'declined'


class TestProjectOfferSearch(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company1 = CompanyWithOfficeFactory()
        cls.company2 = CompanyWithOfficeFactory()
        cls.company3 = CompanyWithOfficeFactory()
        cls.project1 = ProjectFactory(company=cls.company1)
        cls.project2 = ProjectFactory(company=cls.company2)
        cls.project3 = ProjectFactory(company=cls.company3)
        cls.offer1 = OfferFactory(project=cls.project1)
        cls.offer2 = OfferFactory(project=cls.project2)
        cls.offer3 = OfferFactory(project=cls.project3)
        cls.search_offer_endpoint = reverse('search-offers')

    def test_status_page(self):
        status_page_response = self.admin_client.get(
            self.search_offer_endpoint
        )
        self.assertEqual(status_page_response.status_code, status.HTTP_200_OK)

    def test_search_offer_by_company_uuid(self):
        search_offer_by_company_2_uuid_response = self.admin_client.get(
            f"{self.search_offer_endpoint}?company_uuid={self.company2.id}"
            f"&has_status={OfferStatus.CREATED},{OfferStatus.SENT}&exclude_status={OfferStatus.DECLINED}"
        )
        self.assertEqual(search_offer_by_company_2_uuid_response.status_code, status.HTTP_200_OK)
        self.assertEqual(search_offer_by_company_2_uuid_response.data['count'], 1)
        self.assertEqual(
            search_offer_by_company_2_uuid_response.data['results'][0]['name'],
            self.offer2.name
        )
        self.assertEqual(
            search_offer_by_company_2_uuid_response.data['results'][0]['uuid'],
            str(self.offer2.pk)
        )

        search_offer_by_company_1_uuid_response = self.admin_client.get(
            f"{self.search_offer_endpoint}?company_uuid={self.company1.id}"
            f"&has_status={OfferStatus.CREATED},{OfferStatus.SENT}"
        )
        self.assertEqual(search_offer_by_company_1_uuid_response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            search_offer_by_company_1_uuid_response.data['count'],
            Offer.objects.filter(project__company=self.company1).count()
        )
        self.assertEqual(
            search_offer_by_company_1_uuid_response.data['results'][0]['uuid'],
            str(self.offer1.id)
        )

    def test_search_offer_by_company_exclude_status_sent(self):
        search_offer_by_company_1_uuid_response_1 = self.admin_client.get(
            f"{self.search_offer_endpoint}?company_uuid={self.company1.id}"
            f"&has_status={OfferStatus.CREATED}&exclude_status={OfferStatus.SENT}"
        )

        self.assertEqual(
            search_offer_by_company_1_uuid_response_1.data['count'],
            Offer.objects.filter(project__company=self.company1, offer_status=OfferStatus.CREATED).count()
        )
        self.offer1.change_status_to_sent()
        search_offer_by_company_1_uuid_response_2 = self.admin_client.get(
            f"{self.search_offer_endpoint}?company_uuid={self.company1.id}"
            f"&has_status={OfferStatus.CREATED}&exclude_status={OfferStatus.SENT}"
        )
        self.assertEqual(
            search_offer_by_company_1_uuid_response_2.data['count'],
            Offer.objects.filter(project__company=self.company1).exclude(offer_status=OfferStatus.SENT).count()
        )
