from django.core.exceptions import ValidationError
from django.urls import reverse
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.offers.tests.factories import (ActivityOfferedFactory,
                                         ArticleOfferedFactory, OfferFactory)
from apps.projects.tests.factories import ProjectFactory, ProjectActivityFactory, ProjectArticleFactory
import pytest


class TestChoiceLimitations(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.offer = OfferFactory(project=cls.project)
        cls.endpoint = reverse('api_projects:offer-detail', args=[cls.project.id, cls.offer.id])

        project_activity = ProjectActivityFactory(project=cls.project)
        project_article = ProjectArticleFactory(project=cls.project)

        cls.offered_activity1 = ActivityOfferedFactory(offer=cls.offer,
                                                       used_name="sample activity",
                                                       project_activity=project_activity)
        cls.offered_article1 = ArticleOfferedFactory(offer=cls.offer,
                                                     used_description="sample article",
                                                     project_article=project_article)
        cls.activity1 = cls.offered_activity1.project_activity
        cls.article1 = cls.offered_article1.project_article

    @pytest.mark.skip(reason="Find a way to test it after I removed PATCH of OfferStatus ")
    def test_data_copy_on_sent(self):
        self.assertNotEqual(self.offered_activity1.used_description, self.activity1.description)
        self.assertNotEqual(self.offered_article1.used_description, self.article1.description)
        self.assertNotEqual(self.offered_activity1.used_name, self.activity1.name)
        self.assertNotEqual(self.offered_article1.used_name, self.article1.name)
        self.offer.change_status_to_sent()
        self.offer.refresh_from_db()
        self.offered_activity1.refresh_from_db()
        self.offered_article1.refresh_from_db()
        self.assertEqual(self.offered_activity1.used_description, self.activity1.description)
        self.assertEqual(self.offered_article1.used_description, self.article1.description)
        self.assertEqual(self.offered_activity1.used_name, self.activity1.name)
        self.assertEqual(self.offered_article1.used_name, self.article1.name)

    def test_data_copy_on_accepted(self):
        self.assertNotEqual(self.offered_activity1.used_description, self.activity1.description)
        self.assertNotEqual(self.offered_article1.used_description, self.article1.description)
        self.assertNotEqual(self.offered_activity1.used_name, self.activity1.name)
        self.assertNotEqual(self.offered_article1.used_name, self.article1.name)
        self.offer.change_status_to_sent()
        self.offer.refresh_from_db()
        self.activity1.display_name = "One new name"
        self.article1.display_name = "Two new name"
        with self.assertRaises(ValidationError) as error:
            on_accepted_expected_error = self.activity1.save()
            on_accepted_expected_error2 = self.article1.save()
            assert on_accepted_expected_error == error
            assert on_accepted_expected_error2 == error

        self.offer.change_status_to_accepted()
        self.offer.refresh_from_db()
        self.offered_activity1.refresh_from_db()
        self.offered_article1.refresh_from_db()
        self.assertEqual(self.offered_activity1.used_description, self.activity1.description)
        self.assertEqual(self.offered_article1.used_description, self.article1.description)
        self.assertNotEqual(self.offered_activity1.used_name, self.activity1.display_name)
        self.assertNotEqual(self.offered_article1.used_name, self.article1.display_name)

    @pytest.mark.skip(reason="Find a way to test it after I removed PATCH of OfferStatus ")
    def test_no_patch_after_sent(self):
        self.offer.refresh_from_db()
        self.offer.change_status_to_sent()
        self.offer.refresh_from_db()
        patch_data1 = {
            "activities": [{
                "uuid": self.offered_activity1.id,
                "used_description": "Here will be a lot of text one day"
            }]
        }
        patch_data2 = {
            "articles": [
                {
                    "uuid": self.offered_article1.id,
                    "used_description": "Here will be a lot of text one day"
                }
            ]
        }
        with self.assertRaises(ValidationError) as error:
            expected_error = self.admin_client.patch(self.endpoint, patch_data1)
            expected_error2 = self.article1.save()
            assert expected_error == error
            assert expected_error2 == error

        self.offered_activity1.refresh_from_db()
        self.offered_article1.refresh_from_db()
        self.assertEqual(self.offered_activity1.used_description, self.activity1.description)
        self.assertEqual(self.offered_article1.used_description, self.article1.description)
        self.assertNotEqual(self.offered_activity1.used_description, patch_data1["activities"][0]['used_description'])
        self.assertNotEqual(self.offered_article1.used_description, patch_data2["articles"][0]['used_description'])

    @pytest.mark.skip(reason="Find a way to test it after I removed PATCH of OfferStatus ")
    def test_no_patch_after_accepted(self):
        self.offer.refresh_from_db()
        self.offer.change_status_to_sent()
        self.offer.refresh_from_db()
        self.activity1.display_name = "Again one new name"
        self.article1.display_name = "Second new name"
        with self.assertRaises(ValidationError) as error:
            after_accepted_expected_error = self.activity1.save()
            after_accepted_expected_error2 = self.article1.save()
            assert after_accepted_expected_error == error
            assert after_accepted_expected_error2 == error

        self.offer.change_status_to_accepted()
        self.offer.refresh_from_db()
        patch_accepted1 = {
            "activities": [{
                "uuid": self.offered_activity1.id,
                "used_description": "Here will be a lot of text one day"
            }
            ]
        }
        patch_accepted2 = {
            "articles": [
                {
                    "uuid": self.offered_article1.id,
                    "used_description": "Here will be a lot of text one day"
                }
            ]
        }
        self.offered_activity1.refresh_from_db()
        self.offered_article1.refresh_from_db()
        self.assertEqual(self.offered_activity1.used_description, self.activity1.description)
        self.assertEqual(self.offered_article1.used_description, self.article1.description)
        self.assertNotEqual(self.offered_activity1.used_description,
                            patch_accepted1["activities"][0]['used_description'])
        self.assertNotEqual(self.offered_article1.used_description, patch_accepted2["articles"][0]['used_description'])
