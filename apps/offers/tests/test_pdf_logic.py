from datetime import timedelta
from decimal import Decimal
from django.utils.translation import ugettext_lazy as _
import pytest
from django.core.exceptions import ValidationError
from django.utils import timezone
from djmoney.money import Money

from apps.backoffice.tests.factories import (TermsCollectionFactory, TermsParagraphFactory,
                                             TermsParagraphCollectionFactory)
from apps.offers.models import Offer, OfferStatus, ProjectOfferPDF, TermsCollectionPDF
from apps.offers.tests.factories import (ActivityOfferedFactory,
                                         ArticleOfferedFactory,
                                         CampaignOfferFactory, OfferFactory)
from apps.projects.tests.factories import (CampaignFactory,
                                           ProjectActivityFactory,
                                           ProjectActivityStepFactory,
                                           ProjectArticleFactory,
                                           ProjectFactory)
from apps.companies.tests.factories import CompanyWithOfficeFactory
from apps.users.tests.factories import UserFactory
from apps.metronom_commons.pdf_utils import ProjectOfferPDFUtils, get_amount_from_money, TermsCollectionPDFUtils
from django.test import TestCase
from apps.backoffice.utils import get_setting


class TestProjectOfferPDFBase(TestCase):

    @classmethod
    def setUpTestData(cls):
        offer_name = "My New Offer"
        cls.project = ProjectFactory()
        cls.project_offer = OfferFactory(name=offer_name, project=cls.project)
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.activity_step_1 = ProjectActivityStepFactory(
            activity=cls.project_activity, planned_effort=15.00,
            used_rate=Money(45.00, 'CHF'), unit="hourly"
        )
        cls.activity_step_2 = ProjectActivityStepFactory(
            activity=cls.project_activity, planned_effort=5.00,
            used_rate=Money(65.00, 'CHF'), unit="hourly"
        )
        cls.activity_step_3 = ProjectActivityStepFactory(
            activity=cls.project_activity, planned_effort=2.00,
            used_rate=Money(100.00, 'CHF'), unit="daily"
        )
        cls.project_article = ProjectArticleFactory(project=cls.project)
        cls.activity_offered = ActivityOfferedFactory(
            offer=cls.project_offer, project_activity=cls.project_activity, position=1
        )
        cls.article_offered = ArticleOfferedFactory(
            offer=cls.project_offer, project_article=cls.project_article, position=2
        )
        cls.project_offer_pdf_object = ProjectOfferPDFUtils(cls.project_offer)


class TestProjectOfferPDFUtils(TestProjectOfferPDFBase):

    def test_get_amount_from_money(self):
        money_amount = Money(100, 'CHF')
        assert get_amount_from_money(money_amount) == "100.00"

    def test_get_project_offer_positions_list(self):
        positions_list = self.project_offer_pdf_object.get_project_offer_positions_list()
        # `ProjectOffer.all_items` property returns a chained list of all the Activities and Articles Offered
        assert len(positions_list) == len(self.project_offer.all_items)

    def test_get_project_offer_dictionary(self):
        project_offer_dictionary = self.project_offer_pdf_object.get_project_offer_dictionary()
        assert project_offer_dictionary['title'] == self.project_offer.name
        assert project_offer_dictionary['public_id'] == self.project_offer.public_id
        nice_format_total_offer = get_amount_from_money(
            self.project_offer.get_total_offer_send_to_customer()
        )
        assert project_offer_dictionary['total_offer'] == nice_format_total_offer

    def test_get_position_steps(self):
        position_steps = self.project_offer_pdf_object.get_position_steps(self.activity_offered)

        assert "Non Binding Estimate: " in position_steps
        assert f"{self.activity_step_1.name} {self.activity_step_1.planned_effort_in_hours}h " \
               f"at {self.project_offer.currency}" in position_steps
        assert f"{self.activity_step_2.name} {self.activity_step_2.planned_effort_in_hours}h " \
               f"at {self.project_offer.currency}" in position_steps
        assert f"{self.activity_step_3.name} {self.activity_step_3.planned_effort_in_hours}h " \
               f"at {self.project_offer.currency}" in position_steps

    def test_get_offer_metadata(self):
        project_offer_metadata = self.project_offer_pdf_object.get_offer_metadata()
        assert project_offer_metadata['header']['image'] == "html2pdf/img/pp-logo.svg"
        assert project_offer_metadata['footer']['text'] == get_setting('PDF_FOOTER')
        assert project_offer_metadata['agency_address_header'] == get_setting('PDF_ADDRESS_HEADLINE')
        assert project_offer_metadata['currency'] == self.project_offer.currency

    def test_get_project_offer_complete_dictionary(self):
        get_project_offer_complete_dictionary = self.project_offer_pdf_object.get_project_offer_complete_dictionary()
        assert get_project_offer_complete_dictionary['title'] == self.project_offer.name
        assert get_project_offer_complete_dictionary['public_id'] == self.project_offer.public_id
        nice_format_total_offer = get_amount_from_money(
            self.project_offer.get_total_offer_send_to_customer()
        )
        assert get_project_offer_complete_dictionary['total_offer'] == nice_format_total_offer
        assert get_project_offer_complete_dictionary['header']['image'] == "html2pdf/img/pp-logo.svg"
        assert get_project_offer_complete_dictionary['footer']['text'] == get_setting('PDF_FOOTER')
        assert get_project_offer_complete_dictionary['agency_address_header'] == get_setting('PDF_ADDRESS_HEADLINE')
        assert get_project_offer_complete_dictionary['currency'] == self.project_offer.currency


class TestProjectOfferPDF(TestProjectOfferPDFBase):

    @classmethod
    def setUp(cls):
        cls.project_offer_pdf = ProjectOfferPDF(
            project_offer = cls.project_offer,
            context_data=str(cls.project_offer_pdf_object.get_project_offer_complete_dictionary())
        )
        cls.project_offer_pdf.save()

    def test_save_project_offer_pdf_instance(self):

        expected_string = f"Project Offer PDF {self.project_offer_pdf.project_offer.name} " \
                          f"({self.project_offer_pdf.project_offer.public_id})"
        assert self.project_offer_pdf.__str__() == expected_string
        assert self.project_offer_pdf.version == 1
        company_data = self.project_offer_pdf.get_company_data()
        assert company_data['company'] == self.project_offer.project.company.name
        assert company_data['address'] == self.project_offer.project.company.address1
        assert company_data['postal_code'] == self.project_offer.project.company.postal_code
        assert company_data['city'] == self.project_offer.project.company.city
        assert company_data['country_code'] == self.project_offer.project.company.country_code

    def test_project_offer_pdf_is_valid(self):
        self.project_offer_pdf.refresh_from_db()
        assert self.project_offer_pdf.is_valid

        self.project_offer.name = f"{self.project_offer.name} Updated Now"
        self.project_offer.save()

        assert not self.project_offer_pdf.is_valid

    def test_create_new_version(self):
        self.project_offer_pdf.create_new_version()
        self.project_offer_pdf.refresh_from_db()
        new_version_of_project_offer_pdf = self.project_offer_pdf.replaced_by

        assert new_version_of_project_offer_pdf.project_offer == self.project_offer_pdf.project_offer
        assert new_version_of_project_offer_pdf.version == self.project_offer_pdf.version + 1
        assert new_version_of_project_offer_pdf.context_data == self.project_offer_pdf.context_data

    def test_get_context_data_as_dictionary(self):
        context_data_as_dictionary = self.project_offer_pdf.get_context_data_as_dictionary()

        assert isinstance(context_data_as_dictionary, dict)
        assert context_data_as_dictionary['currency'] == self.project_offer.currency

    def test_generate_context_data_from_project_offer(self):
        generated_context_data_from_project_offer = self.project_offer_pdf.generate_context_data_from_project_offer()
        assert generated_context_data_from_project_offer == self.project_offer_pdf.get_context_data_as_dictionary()

    # This actually Tests TWO methods in ONE
    def test_get_the_latest_version(self):
        latest_version_of_project_offer_pdf = ProjectOfferPDF.get_the_latest_version(self.project_offer)
        assert latest_version_of_project_offer_pdf == self.project_offer_pdf

        self.project_offer.offer_description = f"{self.project_offer.name} some fancy NAME"
        self.project_offer.save()
        self.project_offer.refresh_from_db()
        second_latest_version_of_project_offer_pdf = ProjectOfferPDF.get_the_latest_version(self.project_offer)

        assert second_latest_version_of_project_offer_pdf != self.project_offer_pdf
        assert second_latest_version_of_project_offer_pdf == self.project_offer_pdf.replaced_by
        assert self.project_offer_pdf == self.project_offer_pdf.replaced_by.replaced_item

    def test_append_company_data_to_context_dictionary(self):
        context_data_as_dictionary = self.project_offer_pdf.get_context_data_as_dictionary()

        with self.assertRaises(KeyError) as key_error:
            assert context_data_as_dictionary['customer']['company']
            assert context_data_as_dictionary['customer']['address']

        new_context_data_as_dictionary = self.project_offer_pdf.append_company_data_to_context_dictionary()
        assert new_context_data_as_dictionary['customer']['company'] == \
               self.project_offer.project.company.name
        assert new_context_data_as_dictionary['customer']['address'] == \
               self.project_offer.project.company.address1
        assert new_context_data_as_dictionary['customer']['postal_code'] == \
               self.project_offer.project.company.postal_code
        assert new_context_data_as_dictionary['customer']['city'] == \
               self.project_offer.project.company.city
        assert new_context_data_as_dictionary['customer']['country_code'] == \
               self.project_offer.project.company.country_code

    def test_get_post_data_for_pdf_service(self):
        post_data_fro_pdf_service = self.project_offer_pdf.get_post_data_for_pdf_service()

        assert post_data_fro_pdf_service['customer']['company'] == self.project.company.name
        assert post_data_fro_pdf_service['customer']['address'] == self.project.company.address1
        assert post_data_fro_pdf_service['customer']['postal_code'] == self.project.company.postal_code

        assert post_data_fro_pdf_service['public_id'] == self.project_offer.public_id
        assert post_data_fro_pdf_service['title'] == self.project_offer.name
        assert post_data_fro_pdf_service['customer_reference'] == self.project_offer.get_used_customer_reference()
        assert post_data_fro_pdf_service['title'] == self.project_offer.name

    @pytest.mark.skip(reason="PDFDockerImage is not configured for Jenkins, calling http://pdfengine:8200 FAILS")
    def test_get_pdf_document(self):
        pdf_document_response = self.project_offer_pdf.get_pdf_document({})

        assert pdf_document_response.status_code == 200
        assert pdf_document_response.headers['Content-Type'] == 'application/pdf'


class TestTermsCollectionPDFUtils(TestCase):

    def setUp(self):
        self.terms_collection = TermsCollectionFactory()

    def test__init__terms_collection_pdf_utils(self):
        terms_collection_pdf_utils = TermsCollectionPDFUtils(self.terms_collection)
        self.assertEqual(terms_collection_pdf_utils.terms, self.terms_collection)

    def test_get_terms_collection_object(self):
        terms_collection_pdf_utils = TermsCollectionPDFUtils(self.terms_collection)

        with self.assertRaises(AttributeError) as error:
            self.assertEqual(terms_collection_pdf_utils.get_terms_collection_object(), error)

        with self.assertRaises(AttributeError) as error:
            offer = OfferFactory()
            self.assertEqual(terms_collection_pdf_utils.get_terms_collection_object(offer), error)

    def test_get_terms_collection_dictionary(self):
        terms_collection_pdf_utils = TermsCollectionPDFUtils(self.terms_collection)
        terms_collection_dict = terms_collection_pdf_utils.get_terms_collection_dictionary()

        paragraph_dictionary = terms_collection_pdf_utils.get_terms_paragraphs()
        dummy_terms_dictionary = {
            "title": _("General Terms of Business"),
            "terms_date": self.terms_collection.created_at.strftime("%Y-%m-%d %H:%M:%S"),
            "paragraphs": paragraph_dictionary
        }
        self.assertDictEqual(terms_collection_dict, dummy_terms_dictionary)


class TestTermsCollectionPDF(TestCase):

    def setUp(self):
        self.terms_collection = TermsCollectionFactory()
        self.terms_paragraph_1 = TermsParagraphFactory()
        self.terms_paragraph_2 = TermsParagraphFactory()
        TermsParagraphCollectionFactory(
            terms_paragraph=self.terms_paragraph_1, terms_collection=self.terms_collection, position=1
        )
        TermsParagraphCollectionFactory(
            terms_paragraph=self.terms_paragraph_2, terms_collection=self.terms_collection, position=2
        )
        self.terms_collection_pdf_utils = TermsCollectionPDFUtils(self.terms_collection)
        self.terms_collection_pdf = TermsCollectionPDF(
            terms_collection=self.terms_collection,
            context_data=str(self.terms_collection_pdf_utils.get_terms_collection_dictionary())
        )
        self.terms_collection_pdf.save()

    def test_terms_collection__str__(self):
        assert self.terms_collection.name in self.terms_collection_pdf.__str__()

    def test_get_context_data_as_string(self):
        self.assertEqual(
            self.terms_collection_pdf.get_context_data_as_string(),
            str(self.terms_collection_pdf_utils.get_terms_collection_dictionary())
        )

    def test_get_context_data_as_dictionary(self):
        self.assertDictEqual(
            self.terms_collection_pdf.get_context_data_as_dictionary(),
            self.terms_collection_pdf_utils.get_terms_collection_dictionary()
        )
        self.assertEqual(
            self.terms_collection_pdf.terms_collection.terms.all().count(),
            len(self.terms_collection_pdf_utils.get_terms_collection_dictionary().get('paragraphs', []))
        )

    def test_generate_context_data_from_terms_collection(self):

        self.assertDictEqual(
            self.terms_collection_pdf.generate_context_data_from_terms_collection(),
            self.terms_collection_pdf_utils.get_terms_collection_dictionary()
        )

    def test_terms_collection_pdf_is_valid(self):
        assert self.terms_collection_pdf.is_valid

        self.terms_collection.name = "Changed Name To force update"
        self.terms_collection.save()

        assert self.terms_collection_pdf.is_valid == False

    # This actually Tests 3 methods in ONE: `get_the_latest_version`, `create_terms_collection_pdf`
    def test_classmethod_get_the_latest_version(self):
        local_terms_collection_pdf = TermsCollectionPDF.get_the_latest_version(self.terms_collection)

        self.assertEqual(local_terms_collection_pdf, self.terms_collection_pdf)

        terms_collection_2 = TermsCollectionFactory()

        local_terms_collection_pdf_2 = TermsCollectionPDF.get_the_latest_version(terms_collection_2)

        self.assertNotEqual(local_terms_collection_pdf_2, self.terms_collection_pdf)
        self.assertEqual(local_terms_collection_pdf_2.terms_collection, terms_collection_2)

    def test_terms_collection_pdf_create_new_version(self):
        local_terms_collection_pdf_1 = TermsCollectionPDF.get_the_latest_version(self.terms_collection)
        self.assertEqual(
            local_terms_collection_pdf_1.context_data,
            str(self.terms_collection_pdf_utils.get_terms_collection_dictionary())
        )
        assert TermsCollectionPDF.objects.all().count() == 1

        self.terms_collection.name = "Changed Name To force update"
        self.terms_collection.save()
        local_terms_collection_pdf_2 = TermsCollectionPDF.get_the_latest_version(self.terms_collection)

        self.assertEqual(local_terms_collection_pdf_2.terms_collection, self.terms_collection)
        assert local_terms_collection_pdf_2.is_valid
        assert not local_terms_collection_pdf_1.is_valid
        assert TermsCollectionPDF.objects.all().count() == 2
