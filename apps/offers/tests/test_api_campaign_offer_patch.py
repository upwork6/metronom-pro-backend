
import pytest
from ddt import ddt
from django.urls import reverse

from apps.backoffice.tests.factories import TermsCollectionFactory
from apps.offers.models import CampaignOffer, OfferStatus
from apps.offers.tests.factories import OfferFactory
from apps.offers.tests.test_api_campaign_offer_post import \
    TestCampaignOffersBase
from apps.projects.tests.factories import ProjectFactory


@ddt
class TestCampaignOfferPatch(TestCampaignOffersBase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.response = cls.admin_client.post(
            cls.campaign_offer_endpoint,
            data=cls.campaign_data,
            format='json'
        )
        cls.campaign_patch_endpoint = reverse(
            'api_campaigns:offer-detail',
            args=[cls.campaign.id, cls.response.data['uuid']]
        )

    def test_patch_campaign_title(self):
        title_data = {
            "title": "Campaign Title Patched"
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=title_data,
            format='json'
        )
        assert patched_campaign_response.data['title'] == title_data['title']

    def test_patch_campaign_offer_is_optional_and_position(self):
        title_data = {
            "title": "Campaign Title Patched"
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=title_data,
            format='json'
        )
        response = patched_campaign_response.data['project_offers']
        for offer in response:
            self.assertIsNotNone(offer.get("is_optional"))

    def test_patch_campaign_description(self):
        description_data = {
            "description": "Lorem Ipsum Campaign Description Patched"
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=description_data,
            format='json'
        )
        expected = description_data['description']
        response = patched_campaign_response.data['description']
        assert expected == response

    def test_patch_campaign_used_customer_reference(self):
        used_customer_reference_data = {
            "used_customer_reference": "Lorem Ipsum Campaign used_customer_reference Patched"
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=used_customer_reference_data,
            format='json'
        )
        expected = ""
        response = patched_campaign_response.data['used_customer_reference']
        assert expected == response

        campaign_offer = CampaignOffer.objects.get(pk=self.response.data.get('uuid'))
        campaign_offer.change_status_to_sent()

        expected2 = self.campaign.customer_reference
        response2 = campaign_offer.used_customer_reference
        assert expected2 == response2

        # Once the Campaign is SENT is BLOCKED for PATCH except: `callback_date`
        no_effect_title_data = {
            "title": "Campaign Title Second Patched"
        }
        bad_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=no_effect_title_data,
            format='json'
        )
        campaign_offer.refresh_from_db()
        self.assertErrorResponse(
            bad_response,
            {"campaign_status": [
                f'<CampaignOffer>: "Campaign Title Second Patched" is not editable while'
                f' "campaign_status" is "{campaign_offer.campaign_status}".'
            ]}
        )

    def test_patch_campaign_offer_date(self):
        offer_date_data = {
            "offer_date": "2018-01-16"
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=offer_date_data,
            format='json'
        )
        expected = offer_date_data['offer_date']
        response = patched_campaign_response.data['offer_date']
        assert expected == response

    def test_patch_campaign_valid_until_date(self):
        valid_until_date_data = {
            "valid_until_date": "2018-03-10"
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=valid_until_date_data,
            format='json'
        )
        expected = valid_until_date_data['valid_until_date']
        response = patched_campaign_response.data['valid_until_date']
        assert expected == response

    def test_patch_campaign_tax(self):
        tax_data = {
            "tax": "12.00"
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=tax_data,
            format='json'
        )
        expected = tax_data['tax']
        response = patched_campaign_response.data['tax']
        assert expected == response

    def test_patch_campaign_is_tax_included(self):
        is_tax_included_data = {
            "is_tax_included": True
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=is_tax_included_data,
            format='json'
        )
        expected = is_tax_included_data['is_tax_included']
        response = patched_campaign_response.data['is_tax_included']
        assert expected == response

    @pytest.mark.skip(reason="Will be refactored complete after finishing Protection for Model")
    def test_patch_campaign_callback_date(self):
        callback_date_data = {
            "callback_date": "2018-02-19"
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=callback_date_data,
            format='json'
        )
        expected = callback_date_data['callback_date']
        response = patched_campaign_response.data['callback_date']
        assert expected == response

        # even if Campaign is SENT this can be PATCHED
        sent_status_data = {
            "campaign_status": OfferStatus.SENT
        }
        patched_campaign_status_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=sent_status_data,
            format='json'
        )
        assert patched_campaign_status_response.data['campaign_status'] == OfferStatus.SENT

        callback_date_data2 = {
            "callback_date": "2018-02-10"
        }
        patched_campaign_response2 = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=callback_date_data2,
            format='json'
        )
        expected2 = callback_date_data2['callback_date']
        response2 = patched_campaign_response2.data['callback_date']
        assert expected2 == response2

    def test_patch_campaign_contact_person(self):
        contact_person_data = {
            "contact_person": str(self.contact_person2.id)
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=contact_person_data,
            format='json'
        )
        expected = contact_person_data['contact_person']
        response = patched_campaign_response.data['contact_person']['uuid']

        assert expected == response

    def test_patch_campaign_offer_manager(self):
        offer_manager_data = {
            "offer_manager": str(self.project_owner2.id)
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=offer_manager_data,
            format='json'
        )
        expected = offer_manager_data['offer_manager']
        response = patched_campaign_response.data['offer_manager']['uuid']
        assert expected == response

    def test_patch_campaign_offers(self):
        # `name` inside project_offers is just optional
        project_offers_data = {
            "project_offers": [
                {
                    "uuid": str(self.offer1.id),
                    "deleted": True
                }
            ]
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=project_offers_data,
            format='json'
        )
        expected = len(self.response.data['project_offers']) - 1
        response = len(patched_campaign_response.data['project_offers'])
        assert expected == response

    def test_patch_internal_project_offer_serializer_restriction(self):
        project = ProjectFactory(accounting_type='internal')
        offer1 = OfferFactory(project=project)
        project_offers_data = {
            "project_offers": [
                {
                    "uuid": str(offer1.id),
                    "name": offer1.name
                }
            ]
        }
        response = self.admin_client.patch(self.campaign_patch_endpoint, data=project_offers_data, format='json')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data['error'],
            ["You can't add project offer to the campaign offer if project has internal accounting_type"]
        )

    def test_patch_campaign_empty_project_offers(self):
        empty_project_offers_data = {
            "project_offers": []
        }
        patched_campaign_response = self.admin_client.patch(
            self.campaign_patch_endpoint,
            data=empty_project_offers_data,
            format='json'
        )
        # sending Empty list will have no effect Only with deleted flag will be deleted
        response = len(patched_campaign_response.data['project_offers'])
        expected = len(self.response.data['project_offers'])
        assert expected == response

    def test_response_terms_collection_uuid_when_offer_sent(self):
        terms_collection = TermsCollectionFactory()
        sent_campaign_offer = CampaignOffer.objects.get(pk=self.response.data.get('uuid'))
        sent_campaign_offer.change_status_to_sent()
        patch_data = dict(
            terms_collection_uuid=terms_collection.pk
        )
        response = self.admin_client.patch(self.campaign_patch_endpoint, data=patch_data, format='json')
        self.assertErrorResponse(
            response,
            {"campaign_status": [
                f'<CampaignOffer>: "{sent_campaign_offer.title}" is not editable while'
                f' "campaign_status" is "{sent_campaign_offer.campaign_status}".'
            ]}
        )
