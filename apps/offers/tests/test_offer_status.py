from apps.offers.tests.factories import (ActivityOfferedFactory,
                                         ArticleOfferedFactory,
                                         CampaignOfferFactory, OfferFactory)
from apps.projects.tests.factories import ProjectFactory, CampaignFactory, ProjectArticleFactory, ProjectActivityFactory
from django.test import TestCase


class TestActivityAndArticleOfferedStatus(TestCase):

    @classmethod
    def setUpTestData(cls):
        super(TestActivityAndArticleOfferedStatus, cls).setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.offer = OfferFactory(project=cls.project)

    def test_activity_created_change_status_to_sent(self):
        created_activity_offered = ActivityOfferedFactory(offer=self.offer, project_activity__project=self.project)
        assert created_activity_offered.offer_status == 'created'
        created_activity_offered.change_status_to_sent()
        assert created_activity_offered.offer_status == 'sent'
        assert created_activity_offered.project_activity.offer_status == 'sent'

    def test_article_created_change_status_to_sent(self):
        created_article_offered = ArticleOfferedFactory(offer=self.offer, project_article__project=self.project)
        assert created_article_offered.offer_status == 'created'
        created_article_offered.change_status_to_sent()
        assert created_article_offered.offer_status == 'sent'
        assert created_article_offered.project_article.offer_status == 'sent'

    def test_activity_sent_change_status_to_accepted(self):
        accepted_activity_offered = ActivityOfferedFactory(offer=self.offer, project_activity__project=self.project)
        accepted_activity_offered.change_status_to_sent()
        accepted_activity_offered.change_status_to_accepted()
        assert accepted_activity_offered.offer_status == 'accepted'
        assert accepted_activity_offered.project_activity.offer_status == 'accepted'

    def test_article_sent_change_status_to_accepted(self):
        accepted_article_offered = ArticleOfferedFactory(offer=self.offer, project_article__project=self.project)
        accepted_article_offered.change_status_to_sent()
        accepted_article_offered.change_status_to_accepted()
        assert accepted_article_offered.offer_status == 'accepted'
        assert accepted_article_offered.project_article.offer_status == 'accepted'

    def test_activity_sent_change_status_to_declined(self):
        declined_activity_offered = ActivityOfferedFactory(offer=self.offer, project_activity__project=self.project)
        declined_activity_offered.change_status_to_sent()
        declined_activity_offered.change_status_to_declined()
        assert declined_activity_offered.offer_status == 'declined'
        assert declined_activity_offered.project_activity.offer_status == 'declined'

    def test_article_sent_change_status_to_declined(self):
        declined_article_offered = ArticleOfferedFactory(offer=self.offer, project_article__project=self.project)
        declined_article_offered.change_status_to_sent()
        declined_article_offered.change_status_to_declined()
        assert declined_article_offered.offer_status == 'declined'
        assert declined_article_offered.project_article.offer_status == 'declined'

    def test_activity_sent_change_status_to_replaced(self):
        replaced_activity_offered = ActivityOfferedFactory(offer=self.offer, project_activity__project=self.project)
        replaced_activity_offered.change_status_to_sent()
        replaced_activity_offered.change_status_to_replaced()
        assert replaced_activity_offered.offer_status == 'replaced'
        assert replaced_activity_offered.project_activity.offer_status == 'replaced'

        assert replaced_activity_offered.replaced_by.offer_status == 'improvable'
        assert replaced_activity_offered.project_activity.replaced_by.offer_status == 'improvable'

        cloned_activity_offered = replaced_activity_offered.replaced_by
        assert replaced_activity_offered == cloned_activity_offered.replaced_item

        cloned_project_activity = replaced_activity_offered.project_activity.replaced_by
        assert replaced_activity_offered.project_activity == cloned_project_activity.replaced_item

    def test_article_sent_change_status_to_replaced(self):
        replaced_article_offered = ArticleOfferedFactory(offer=self.offer, project_article__project=self.project)
        replaced_article_offered.change_status_to_sent()
        replaced_article_offered.change_status_to_replaced()
        assert replaced_article_offered.offer_status == 'replaced'
        assert replaced_article_offered.project_article.offer_status == 'replaced'

        assert replaced_article_offered.replaced_by.offer_status == 'improvable'
        assert replaced_article_offered.project_article.replaced_by.offer_status == 'improvable'

        cloned_article_offered = replaced_article_offered.replaced_by
        assert replaced_article_offered == cloned_article_offered.replaced_item

        cloned_project_article = replaced_article_offered.project_article.replaced_by
        assert replaced_article_offered.project_article == cloned_project_article.replaced_item


class TestOfferForOfferStatus(TestCase):

    def setUp(self):
        self.project = ProjectFactory(billing_currency="CHF")
        self.offer = OfferFactory(project=self.project)

        project_activity = ProjectActivityFactory(project=self.project)
        project_article = ProjectArticleFactory(project=self.project)
        self.activity_offered = ActivityOfferedFactory(offer=self.offer, project_activity=project_activity)
        self.article_offered = ArticleOfferedFactory(offer=self.offer, project_article=project_article)

    def test_offer_created_change_status_to_sent(self):
        assert self.offer.offer_status == 'created'
        assert self.activity_offered.offer_status == 'created'
        assert self.activity_offered.project_activity.offer_status == 'created'
        self.offer.change_status_to_sent()

        assert self.offer.offer_status == 'sent'

        self.activity_offered.refresh_from_db()
        self.activity_offered.project_activity.refresh_from_db()
        self.article_offered.refresh_from_db()
        self.article_offered.project_article.refresh_from_db()

        assert self.activity_offered.offer_status == 'sent'
        assert self.activity_offered.project_activity.offer_status == 'sent'

        assert self.article_offered.offer_status == 'sent'
        assert self.article_offered.project_article.offer_status == 'sent'

    def test_offer_sent_change_status_to_accepted(self):
        self.offer.change_status_to_sent()
        self.offer.change_status_to_accepted()
        assert self.offer.offer_status == 'accepted'
        self.activity_offered.refresh_from_db()
        self.activity_offered.project_activity.refresh_from_db()
        self.article_offered.refresh_from_db()
        self.article_offered.project_article.refresh_from_db()

        assert self.activity_offered.offer_status == 'accepted'
        assert self.activity_offered.project_activity.offer_status == 'accepted'
        assert self.article_offered.offer_status == 'accepted'
        assert self.article_offered.project_article.offer_status == 'accepted'

    def test_offer_sent_change_status_to_replaced(self):
        self.offer.change_status_to_sent()
        self.offer.change_status_to_replaced()
        assert self.offer.offer_status == 'replaced'
        self.activity_offered.refresh_from_db()
        self.activity_offered.project_activity.refresh_from_db()
        self.article_offered.refresh_from_db()
        self.article_offered.project_article.refresh_from_db()

        assert self.activity_offered.offer_status == 'replaced'
        assert self.activity_offered.project_activity.offer_status == 'replaced'
        assert self.article_offered.offer_status == 'replaced'
        assert self.article_offered.project_article.offer_status == 'replaced'

        assert self.activity_offered.replaced_by in self.offer.replaced_by.activities.all()
        assert self.article_offered.replaced_by in self.offer.replaced_by.articles.all()


class TestCampaignOfferForOfferStatus(TestCase):

    def setUp(self):
        self.campaign = CampaignFactory(billing_currency="CHF")
        self.project = ProjectFactory(billing_currency="CHF", campaign=self.campaign)
        self.offer = OfferFactory(project=self.project)

        project_activity = ProjectActivityFactory(project=self.project)
        project_article = ProjectArticleFactory(project=self.project)

        self.activity_offered = ActivityOfferedFactory(offer=self.offer, project_activity=project_activity)
        self.article_offered = ArticleOfferedFactory(offer=self.offer, project_article=project_article)
        self.campaign_offer = CampaignOfferFactory(campaign=self.campaign)
        self.campaign_offer.project_offers.add(self.offer)

    def test_offer_created_change_status_to_sent(self):
        assert self.campaign_offer.campaign_status == 'created'
        assert self.offer.offer_status == 'created'
        self.campaign_offer.change_status_to_sent()

        assert self.campaign_offer.campaign_status == 'sent'

        self.offer.refresh_from_db()
        self.offer.activities.first().refresh_from_db()
        self.offer.articles.first().refresh_from_db()

        assert self.offer.offer_status == 'sent'
        assert self.offer.activities.first().offer_status == 'sent'
        assert self.offer.articles.first().offer_status == 'sent'

    def test_offer_created_change_status_to_accepted(self):
        self.campaign_offer.change_status_to_sent()
        self.campaign_offer.change_status_to_accepted()

        assert self.campaign_offer.campaign_status == 'accepted'

        self.offer.refresh_from_db()
        self.offer.activities.first().refresh_from_db()
        self.offer.articles.first().refresh_from_db()

        assert self.offer.offer_status == 'accepted'
        assert self.offer.activities.first().offer_status == 'accepted'
        assert self.offer.articles.first().offer_status == 'accepted'

    def test_offer_created_change_status_to_declined(self):
        self.campaign_offer.change_status_to_sent()
        self.campaign_offer.change_status_to_declined()

        assert self.campaign_offer.campaign_status == 'declined'
        offer2 = OfferFactory(offer_status='accepted', project=self.project)

        self.offer.refresh_from_db()
        self.offer.activities.first().refresh_from_db()
        self.offer.articles.first().refresh_from_db()

        assert self.offer.offer_status == 'declined'
        assert self.offer.activities.first().offer_status == 'declined'
        assert self.offer.articles.first().offer_status == 'declined'

    def test_offer_created_change_status_to_replaced(self):
        self.campaign_offer.change_status_to_sent()
        self.campaign_offer.change_status_to_replaced()

        assert self.campaign_offer.campaign_status == 'replaced'

        self.offer.refresh_from_db()
        self.offer.activities.first().refresh_from_db()
        self.offer.articles.first().refresh_from_db()

        assert self.offer.offer_status == 'replaced'
        assert self.offer.activities.first().offer_status == 'replaced'
        assert self.offer.articles.first().offer_status == 'replaced'

        assert self.offer.replaced_by in self.campaign_offer.replaced_by.project_offers.all()
        assert self.offer.activities.first().replaced_by in self.offer.replaced_by.activities.all()
        assert self.offer.articles.first().replaced_by in self.offer.replaced_by.articles.all()
