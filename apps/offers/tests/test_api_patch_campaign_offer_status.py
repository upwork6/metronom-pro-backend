from django.urls import reverse
from rest_framework import status
from apps.metronom_commons.choices_flow import OfferStatus, ActivityArticleOfferedStatus
import pytest
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.tests.factories import (ProjectFactory, ProjectActivityFactory,
                                           ProjectArticleFactory, CampaignFactory)
from apps.offers.tests.factories import (OfferFactory, CampaignOfferFactory,
                                         ArticleOfferedFactory, ActivityOfferedFactory)


class TestCampaignOfferChangeStatus(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super(TestCampaignOfferChangeStatus, cls).setUpTestData()
        cls.campaign = CampaignFactory(billing_currency="CHF")
        cls.project = ProjectFactory(campaign=cls.campaign)
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_article = ProjectArticleFactory(project=cls.project)
        cls.offer = OfferFactory(project=cls.project)
        cls.activity_offered = ActivityOfferedFactory(offer=cls.offer, project_activity=cls.project_activity)
        cls.article_offered = ArticleOfferedFactory(offer=cls.offer, project_article=cls.project_article)
        cls.campaign_offer = CampaignOfferFactory(campaign=cls.campaign)
        cls.campaign_offer.project_offers.add(cls.offer)
        cls.status_url = reverse('campaign-status', kwargs={'campaign_pk': cls.campaign.id, "id": cls.campaign_offer.id})

    def test_change_campaign_offer_to_sent(self):
        campaign_offer_data = {
            "campaign_status": OfferStatus.SENT
        }
        offer_response = self.admin_client.patch(self.status_url, campaign_offer_data, format="json")
        assert offer_response.status_code == status.HTTP_200_OK
        assert offer_response.data['uuid'] == str(self.campaign_offer.pk)
        assert offer_response.data['campaign_status'] == OfferStatus.SENT

    def test_change_items_to_sent_switch_campaign_offer_to_sent(self):
        sent_campaign_offer_data = {
            "project_offers": [
                {
                    "uuid": str(self.offer.pk),
                    "offer_status": OfferStatus.SENT
                }
            ]
        }
        sent_campaign_offer_response = self.admin_client.patch(
            self.status_url,
            sent_campaign_offer_data,
            format="json"
        )
        assert sent_campaign_offer_response.status_code == status.HTTP_200_OK
        assert sent_campaign_offer_response.data['uuid'] == str(self.campaign_offer.pk)

        assert sent_campaign_offer_response.data['campaign_status'] == OfferStatus.SENT

        assert sent_campaign_offer_response.data['project_offers'][0]['offer_status'] == OfferStatus.SENT
        assert sent_campaign_offer_response.data['project_offers'][0]['activities'][0]['offer_status'] == OfferStatus.SENT
        assert sent_campaign_offer_response.data['project_offers'][0]['articles'][0][
                   'offer_status'] == OfferStatus.SENT

    def test_change_campaign_offer_to_partly_accepted_case_1(self):
        case_1_second_offer = OfferFactory(project=self.project)
        self.campaign_offer.project_offers.add(case_1_second_offer)
        self.campaign_offer.change_status_to_sent()
        partly_accepted_campaign_offer_data = {
            "project_offers": [
                {
                    "uuid": str(self.offer.pk),
                    "offer_status": OfferStatus.ACCEPTED
                },
                {
                    "uuid": str(case_1_second_offer.pk),
                    "offer_status": OfferStatus.DECLINED
                }
            ]
        }
        partly_accepted_offer_response = self.admin_client.patch(
            self.status_url,
            partly_accepted_campaign_offer_data,
            format="json"
        )
        assert partly_accepted_offer_response.status_code == status.HTTP_200_OK
        assert partly_accepted_offer_response.data['uuid'] == str(self.campaign_offer.pk)
        assert partly_accepted_offer_response.data['campaign_status'] == OfferStatus.PARTLY_ACCEPTED

    def test_change_campaign_offer_to_partly_accepted_case_2(self):
        case_2_second_offer = OfferFactory(project=self.project)
        self.campaign_offer.project_offers.add(case_2_second_offer)
        self.campaign_offer.change_status_to_sent()
        case_2_second_offer.offer_status = OfferStatus.PARTLY_ACCEPTED
        case_2_second_offer.save(update_fields=["offer_status"])
        partly_accepted_campaign_offer_data = {
            "project_offers": [
                {
                    "uuid": str(self.offer.pk),
                    "offer_status": OfferStatus.ACCEPTED
                }
            ]
        }
        case_2_partly_accepted_offer_response = self.admin_client.patch(
            self.status_url,
            partly_accepted_campaign_offer_data,
            format="json"
        )
        assert case_2_partly_accepted_offer_response.status_code == status.HTTP_200_OK
        assert case_2_partly_accepted_offer_response.data['uuid'] == str(self.campaign_offer.pk)
        assert case_2_partly_accepted_offer_response.data['campaign_status'] == OfferStatus.PARTLY_ACCEPTED

    def test_change_campaign_offer_to_partly_accepted_case_3(self):
        case_3_second_offer = OfferFactory(project=self.project)
        self.campaign_offer.project_offers.add(case_3_second_offer)
        self.campaign_offer.change_status_to_sent()
        case_3_second_offer.offer_status = OfferStatus.PARTLY_ACCEPTED
        case_3_second_offer.save(update_fields=["offer_status"])
        partly_accepted_campaign_offer_data = {
            "project_offers": [
                {
                    "uuid": str(self.offer.pk),
                    "offer_status": OfferStatus.DECLINED
                }
            ]
        }
        case_3_partly_accepted_offer_response = self.admin_client.patch(
            self.status_url,
            partly_accepted_campaign_offer_data,
            format="json"
        )
        assert case_3_partly_accepted_offer_response.status_code == status.HTTP_200_OK
        assert case_3_partly_accepted_offer_response.data['uuid'] == str(self.campaign_offer.pk)
        assert case_3_partly_accepted_offer_response.data['campaign_status'] == OfferStatus.PARTLY_ACCEPTED

    def test_change_campaign_offer_to_partly_accepted_case_4(self):
        case_4_second_offer = OfferFactory(project=self.project)
        self.campaign_offer.project_offers.add(case_4_second_offer)
        self.campaign_offer.change_status_to_sent()
        case_4_second_offer.offer_status = OfferStatus.PARTLY_ACCEPTED
        case_4_second_offer.save(update_fields=["offer_status"])
        self.offer.offer_status = OfferStatus.PARTLY_ACCEPTED
        self.offer.save(update_fields=["offer_status"])
        partly_accepted_campaign_offer_data = {
            "project_offers": [
                {
                    "uuid": str(self.offer.pk),
                    "offer_status": OfferStatus.PARTLY_ACCEPTED
                },
                {
                    "uuid": str(case_4_second_offer.pk),
                    "offer_status": OfferStatus.PARTLY_ACCEPTED
                }
            ]
        }
        # This PATCH DATA don't have any effect the PROJECT OFFER
        # just Simulating that 2 ProjectOffers have status of PARTLY_ACCEPTED status
        case_4_partly_accepted_offer_response = self.admin_client.patch(
            self.status_url,
            partly_accepted_campaign_offer_data,
            format="json"
        )
        assert case_4_partly_accepted_offer_response.status_code == status.HTTP_200_OK
        assert case_4_partly_accepted_offer_response.data['uuid'] == str(self.campaign_offer.pk)
        assert case_4_partly_accepted_offer_response.data['campaign_status'] == OfferStatus.PARTLY_ACCEPTED

    def test_simple_change_campaign_offer_to_replaced(self):
        self.campaign_offer.change_status_to_sent()
        simple_replaced_campaign_offer_data = {
            "project_offers": [
                {
                    "uuid": str(self.offer.pk),
                    "offer_status": OfferStatus.REPLACED
                }
            ]
        }
        simple_replaced_offer_response = self.admin_client.patch(
            self.status_url,
            simple_replaced_campaign_offer_data,
            format="json"
        )
        assert simple_replaced_offer_response.status_code == status.HTTP_200_OK
        assert simple_replaced_offer_response.data['uuid'] == str(self.campaign_offer.pk)
        assert simple_replaced_offer_response.data['campaign_status'] == OfferStatus.REPLACED
        assert simple_replaced_offer_response.data['project_offers'][0]['offer_status'] == OfferStatus.REPLACED
        assert self.campaign_offer.campaign.offers.count() == 2
        assert self.campaign_offer.replaced_by.campaign_status == OfferStatus.CREATED

    # This is a slow test but it needs to handle the entire dynamic of changing
    # CampaignOffer, Offer, ArticleOffer, ActivityOffered, ProjectActivity and ProjectArticle
    def test_complex_change_campaign_offer_to_replaced(self):
        self.campaign_offer.change_status_to_sent()
        complex_replaced_campaign_offer_data = {
            "project_offers": [
                {
                    "uuid": str(self.offer.pk),
                    "activities": [
                        {
                            "uuid": str(self.activity_offered.pk),
                            "offer_status": OfferStatus.REPLACED
                        }
                    ],
                    "articles": [
                        {
                            "uuid": str(self.article_offered.pk),
                            "offer_status": OfferStatus.ACCEPTED
                        }
                    ]
                }
            ]
        }
        complex_replaced_offer_response = self.admin_client.patch(
            self.status_url,
            complex_replaced_campaign_offer_data,
            format="json"
        )
        assert complex_replaced_offer_response.status_code == status.HTTP_200_OK
        assert complex_replaced_offer_response.data['uuid'] == str(self.campaign_offer.pk)
        assert complex_replaced_offer_response.data['campaign_status'] == OfferStatus.REPLACED
        assert complex_replaced_offer_response.data['project_offers'][0]['offer_status'] == OfferStatus.REPLACED
        assert self.campaign_offer.campaign.offers.count() == 2
        assert self.campaign_offer.replaced_by.campaign_status == OfferStatus.CREATED

        new_campaign_offer = self.campaign_offer.replaced_by

        new_status_url = reverse('campaign-status', kwargs={'campaign_pk': self.campaign.id,
                                                            "id": new_campaign_offer.id})
        new_campaign_offer_data = {
            "campaign_status": OfferStatus.SENT
        }
        new_campaign_offer_response = self.admin_client.patch(
            new_status_url,
            new_campaign_offer_data,
            format="json"
        )
        assert new_campaign_offer_response.status_code == status.HTTP_200_OK
        assert new_campaign_offer_response.data['uuid'] == str(new_campaign_offer.pk)
        assert new_campaign_offer_response.data['campaign_status'] == OfferStatus.SENT
        assert len(new_campaign_offer_response.data['project_offers']) == 1
        assert new_campaign_offer_response.data['project_offers'][0][
                   "offer_status"] == OfferStatus.SENT

        new_offer = new_campaign_offer.project_offers.first()

        accepted_campaign_offer_data = {
            "project_offers": [
                {
                    "uuid": str(new_offer.pk),
                    "activities": [
                        {
                            "uuid": str(new_offer.activities.first().pk),
                            "offer_status": OfferStatus.ACCEPTED
                        }
                    ]
                }
            ]
        }
        accepted_campaign_offer_response = self.admin_client.patch(
            new_status_url,
            accepted_campaign_offer_data,
            format="json"
        )
        assert accepted_campaign_offer_response.status_code == status.HTTP_200_OK
        assert accepted_campaign_offer_response.data['uuid'] == str(new_campaign_offer.pk)
        assert accepted_campaign_offer_response.data['campaign_status'] == OfferStatus.ACCEPTED
        assert len(accepted_campaign_offer_response.data['project_offers']) == 1
        assert accepted_campaign_offer_response.data['project_offers'][0]["offer_status"] == OfferStatus.ACCEPTED
