from django.urls import reverse
from rest_framework import status
from apps.metronom_commons.choices_flow import OfferStatus
import pytest
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.tests.factories import ProjectFactory, ProjectActivityFactory, ProjectArticleFactory
from apps.offers.tests.factories import (OfferFactory,
                                         ArticleOfferedFactory, ActivityOfferedFactory)


class TestOfferChangeStatus(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super(TestOfferChangeStatus, cls).setUpTestData()
        cls.project = ProjectFactory(billing_currency="CHF")
        cls.project_activity = ProjectActivityFactory(project=cls.project)
        cls.project_article = ProjectArticleFactory(project=cls.project)
        cls.offer = OfferFactory(project=cls.project)
        cls.activity_offered = ActivityOfferedFactory(offer=cls.offer, project_activity=cls.project_activity)
        cls.article_offered = ArticleOfferedFactory(offer=cls.offer, project_article=cls.project_article)
        cls.status_url = reverse('offer-status', kwargs={'project_pk': cls.project.id, "id": cls.offer.id})

    def test_change_offer_to_sent(self):
        offer_data = {
            "offer_status": OfferStatus.SENT
        }
        offer_response = self.admin_client.patch(self.status_url, offer_data, format="json")
        assert offer_response.status_code == status.HTTP_200_OK
        assert offer_response.data['uuid'] == str(self.offer.pk)
        assert offer_response.data['offer_status'] == OfferStatus.SENT
        # It is only one activity
        assert offer_response.data['activities'][0]['offer_status'] == OfferStatus.SENT
        assert offer_response.data['articles'][0]['offer_status'] == OfferStatus.SENT

    def test_change_items_to_sent_switch_offer_to_sent(self):
        offer_data = {
            "activities": [
                {
                    "uuid": str(self.activity_offered.pk),
                    "offer_status": OfferStatus.SENT
                }
            ],
            "articles": [
                {
                    "uuid": str(self.article_offered.pk),
                    "offer_status": OfferStatus.SENT
                }
            ]
        }
        offer_response = self.admin_client.patch(self.status_url, offer_data, format="json")
        assert offer_response.status_code == status.HTTP_200_OK
        assert offer_response.data['uuid'] == str(self.offer.pk)

        assert offer_response.data['offer_status'] == OfferStatus.SENT
        # It is only one activity
        assert offer_response.data['activities'][0]['offer_status'] == OfferStatus.SENT
        assert offer_response.data['articles'][0]['offer_status'] == OfferStatus.SENT

    def test_change_offer_to_partly_accepted(self):
        self.offer.change_status_to_sent()
        partly_accepted_offer_data = {
            "activities": [
                {
                    "uuid": str(self.activity_offered.pk),
                    "offer_status": OfferStatus.ACCEPTED
                }
            ],
            "articles": [
                {
                    "uuid": str(self.article_offered.pk),
                    "offer_status": OfferStatus.DECLINED
                }
            ]
        }
        offer_response = self.admin_client.patch(self.status_url, partly_accepted_offer_data, format="json")
        assert offer_response.status_code == status.HTTP_200_OK
        assert offer_response.data['uuid'] == str(self.offer.pk)
        assert offer_response.data['offer_status'] == OfferStatus.PARTLY_ACCEPTED
        # It is only one activity
        assert offer_response.data['activities'][0]['offer_status'] == OfferStatus.ACCEPTED
        assert offer_response.data['articles'][0]['offer_status'] == OfferStatus.DECLINED

    def test_one_item_replaced_change_offer_to_replaced(self):
        self.offer.change_status_to_sent()
        replaced_offer_data = {
            "activities": [
                {
                    "uuid": str(self.activity_offered.pk),
                    "offer_status": OfferStatus.ACCEPTED
                }
            ],
            "articles": [
                {
                    "uuid": str(self.article_offered.pk),
                    "offer_status": OfferStatus.REPLACED
                }
            ]
        }
        offer_response = self.admin_client.patch(self.status_url, replaced_offer_data, format="json")
        assert offer_response.status_code == status.HTTP_200_OK
        assert offer_response.data['uuid'] == str(self.offer.pk)
        assert offer_response.data['offer_status'] == OfferStatus.REPLACED
        # It is only one activity
        assert offer_response.data['articles'][0]['offer_status'] == OfferStatus.REPLACED

        assert offer_response.data['activities'][0]['offer_status'] == OfferStatus.ACCEPTED
        assert offer_response.data['replaced_by']

        all_offers_response = self.admin_client.get(reverse('api_projects:offer-list', args=[self.project.id]))
        assert len(all_offers_response.data) == 2
        new_offer = None
        self.offer.refresh_from_db()
        for offer in all_offers_response.data:
            if offer['uuid'] == offer_response.data['replaced_by']['uuid']:
                new_offer = offer
        assert new_offer['replaced_item']['uuid'] == offer_response.data['uuid']
        assert new_offer['uuid'] == offer_response.data['replaced_by']['uuid']

    def test_change_items_to_accepted_switch_offer_to_accepted(self):
        self.offer.change_status_to_sent()
        offer_data = {
            "activities": [
                {
                    "uuid": str(self.activity_offered.pk),
                    "offer_status": OfferStatus.ACCEPTED
                }
            ],
            "articles": [
                {
                    "uuid": str(self.article_offered.pk),
                    "offer_status": OfferStatus.ACCEPTED
                }
            ]
        }
        offer_response = self.admin_client.patch(self.status_url, offer_data, format="json")
        assert offer_response.status_code == status.HTTP_200_OK
        assert offer_response.data['uuid'] == str(self.offer.pk)

        assert offer_response.data['offer_status'] == OfferStatus.ACCEPTED
        # It is only one activity
        assert offer_response.data['activities'][0]['offer_status'] == OfferStatus.ACCEPTED
        assert offer_response.data['articles'][0]['offer_status'] == OfferStatus.ACCEPTED

    def test_change_offer_to_accepted(self):
        self.offer.change_status_to_sent()
        offer_data = {
            "offer_status": OfferStatus.ACCEPTED
        }
        offer_response = self.admin_client.patch(self.status_url, offer_data, format="json")
        assert offer_response.status_code == status.HTTP_200_OK
        assert offer_response.data['uuid'] == str(self.offer.pk)

        assert offer_response.data['offer_status'] == OfferStatus.ACCEPTED
        # It is only one activity
        assert offer_response.data['activities'][0]['offer_status'] == OfferStatus.ACCEPTED
        assert offer_response.data['articles'][0]['offer_status'] == OfferStatus.ACCEPTED

    def test_change_items_to_declined_switch_offer_to_declined(self):
        self.offer.change_status_to_sent()
        offer_data = {
            "activities": [
                {
                    "uuid": str(self.activity_offered.pk),
                    "offer_status": OfferStatus.DECLINED
                }
            ],
            "articles": [
                {
                    "uuid": str(self.article_offered.pk),
                    "offer_status": OfferStatus.DECLINED
                }
            ]
        }
        offer_response = self.admin_client.patch(self.status_url, offer_data, format="json")
        assert offer_response.status_code == status.HTTP_200_OK
        assert offer_response.data['uuid'] == str(self.offer.pk)

        assert offer_response.data['offer_status'] == OfferStatus.DECLINED
        # It is only one activity
        assert offer_response.data['activities'][0]['offer_status'] == OfferStatus.DECLINED
        assert offer_response.data['articles'][0]['offer_status'] == OfferStatus.DECLINED

    def test_change_offer_to_declined(self):
        self.offer.change_status_to_sent()
        offer_data = {
            "offer_status": OfferStatus.DECLINED
        }
        offer_response = self.admin_client.patch(self.status_url, offer_data, format="json")
        assert offer_response.status_code == status.HTTP_200_OK
        assert offer_response.data['uuid'] == str(self.offer.pk)

        assert offer_response.data['offer_status'] == OfferStatus.DECLINED
        # It is only one activity
        assert offer_response.data['activities'][0]['offer_status'] == OfferStatus.DECLINED
        assert offer_response.data['articles'][0]['offer_status'] == OfferStatus.DECLINED
