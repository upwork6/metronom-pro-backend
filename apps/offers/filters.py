from uuid import UUID

from django.utils.translation import ugettext_lazy as _
from rest_framework import filters
from rest_framework.exceptions import ValidationError

from apps.metronom_commons.data import WORKLOGS_STATUS
from apps.offers.models import Offer, CampaignOffer
from apps.metronom_commons.choices_flow import OfferStatus
from apps.metronom_commons.utils import cast_bool


def check_uuids(uuids):

    for uuid in uuids:
        try:
            UUID(uuid, version=4)
        except ValueError as ve:
            raise ValidationError(detail=ve)


def check_offer_status(offer_status_list):
    valid_offer_status_list = [OfferStatus.CREATED, OfferStatus.SENT, OfferStatus.ACCEPTED, OfferStatus.DECLINED,
                               OfferStatus.REPLACED]
    for status in offer_status_list:
        if status not in valid_offer_status_list:
            raise ValidationError(
                {
                    "offer_status": _(f"'{status}' is not a valid `offer_status`")
                }
            )


class ProjectOfferFilterBackend(filters.BaseFilterBackend):
    """
        This filter set will filter ProjectOffer (used in /api/offers/)
    """

    def filter_queryset(self, request, queryset, view):

        company_uuid = request.query_params.get('company_uuid')
        exclude_status = request.query_params.get('exclude_status')
        has_status = request.query_params.get('has_status')

        # Parameters below: not covered by tests and are not included in Swagger Docs to filter by yet.
        offer_name = request.query_params.get('offer_name')
        offer_date_lte = request.query_params.get('offer_date_lte')
        offer_date_gte = request.query_params.get('offer_date_gte')
        valid_until_date_gte = request.query_params.get('valid_until_date_gte')
        valid_until_date_lte = request.query_params.get('valid_until_date_lte')

        if offer_name:
            queryset = queryset.filter(name__icontains=offer_name)

        if company_uuid:
            company_uuid_list = company_uuid.split(',')
            check_uuids(company_uuid_list)
            queryset = queryset.filter(project__company__id__in=company_uuid_list)

        if offer_date_gte:
            queryset = queryset.filter(start_date__gte=offer_date_gte)

        if offer_date_lte:
            queryset = queryset.filter(start_date__lte=offer_date_lte)

        if valid_until_date_gte:
            queryset = queryset.filter(valid_until_date__gte=valid_until_date_gte)

        if valid_until_date_lte:
            queryset = queryset.filter(valid_until_date__lte=valid_until_date_lte)

        if has_status:
            has_status_list = has_status.split(',')
            check_offer_status(has_status_list)
            queryset = queryset.filter(offer_status__in=has_status_list)

        if exclude_status:
            exclude_status_list = exclude_status.split(',')
            check_offer_status(exclude_status_list)
            queryset = queryset.exclude(offer_status__in=exclude_status_list)

        return queryset
