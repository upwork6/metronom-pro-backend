# -*- coding: utf-8 -*-
from django.contrib import admin

from apps.offers.models import (ActivityOffered, ArticleOffered, CampaignOffer,
                                Offer, ProjectOfferPDF, CampaignOfferPDF, TermsCollectionPDF)


@admin.register(Offer)
class OfferAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'offer_status',
        'project',
        'name',
        'offer_date',
        'valid_until_date',
        'callback_date',
        'contact_person',
        'offer_description',
        'is_tax_included',
        'used_tax_rate',
    )
    list_filter = (
        'offer_status',
        'created_at',
        'updated_at',
        'offer_date',
        'valid_until_date',
        'callback_date',
        'is_tax_included'
    )
    search_fields = ('name', 'project', 'contact_person', 'offer_manager',)


@admin.register(ActivityOffered)
class ActivityOfferedAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'offer',
        'offer_status',
        'position',
        'discount',
        'show_steps',
        'is_optional',
        'has_subtotal',
        'used_description',
        'project_activity',
    )
    list_filter = (
        'offer_status',
        'created_at',
        'updated_at',
        'offer',
        'show_steps',
        'is_optional',
        'has_subtotal'
    )
    search_fields = ('project_activity',)


@admin.register(ArticleOffered)
class ArticleOfferedAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'offer',
        'offer_status',
        'position',
        'discount',
        'has_subtotal',
        'is_external_cost',
        'is_optional',
        'used_description',
        'project_article',
    )
    list_filter = (
        'offer_status',
        'created_at',
        'updated_at',
        'offer',
        'is_external_cost',
        'is_optional',
    )
    search_fields = ('project_article',)


@admin.register(CampaignOffer)
class CampaignOfferAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'title',
        'campaign_status',
        'description',
        'used_customer_reference',
        'offer_date',
        'valid_until_date',
        'callback_date',
        'campaign_status',
        'tax',
        'is_tax_included',
        'offer_manager',
        'contact_person',
        'campaign',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'offer_date',
        'campaign_status',
        'valid_until_date',
        'callback_date',
        'is_tax_included',
        'offer_manager',
    )
    # raw_id_fields = ('project_offers', )


@admin.register(TermsCollectionPDF)
class TermsCollectionPDFAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'version',
        'terms_collection',
        'context_data'
    )


@admin.register(ProjectOfferPDF)
class ProjectOfferPDFAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'version',
        'project_offer',
        'context_data'
    )


@admin.register(CampaignOfferPDF)
class CampaignOfferPDFAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'version',
        'campaign_offer',
        'context_data'
    )
