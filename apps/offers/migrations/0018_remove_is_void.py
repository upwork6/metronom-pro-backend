# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-15 09:57
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0017_auto_20180516_0942'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='activityoffered',
            name='is_void',
        ),
        migrations.RemoveField(
            model_name='articleoffered',
            name='is_void',
        ),
        migrations.RemoveField(
            model_name='campaignoffer',
            name='is_void',
        ),
        migrations.RemoveField(
            model_name='campaignofferpdf',
            name='is_void',
        ),
        migrations.RemoveField(
            model_name='offer',
            name='is_void',
        ),
        migrations.RemoveField(
            model_name='projectofferpdf',
            name='is_void',
        ),
        migrations.RemoveField(
            model_name='termscollectionpdf',
            name='is_void',
        ),
    ]
