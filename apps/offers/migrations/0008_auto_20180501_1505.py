# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-01 15:05
from __future__ import unicode_literals

from django.db import migrations, models
from django.db.models.query import F


def prefill_fields(apps, schema_migration):
    for name, model in apps.all_models['offers'].items():
        if name != "campaignoffer_project_offers":
            model.objects.all().update(
                created_at=F("created_date"),
                updated_at=F("updated_date")
            )


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0007_campaignofferpdf_projectofferpdf'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='offer',
            options={'ordering': ('-created_at',), 'verbose_name': 'Offer', 'verbose_name_plural': 'Offers'},
        ),
        migrations.AddField(
            model_name='activityoffered',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Created'),
        ),
        migrations.AddField(
            model_name='activityoffered',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True, verbose_name='Modified'),
        ),
        migrations.AddField(
            model_name='articleoffered',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Created'),
        ),
        migrations.AddField(
            model_name='articleoffered',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True, verbose_name='Modified'),
        ),
        migrations.AddField(
            model_name='campaignoffer',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Created'),
        ),
        migrations.AddField(
            model_name='campaignoffer',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True, verbose_name='Modified'),
        ),
        migrations.AddField(
            model_name='campaignofferpdf',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Created'),
        ),
        migrations.AddField(
            model_name='campaignofferpdf',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True, verbose_name='Modified'),
        ),
        migrations.AddField(
            model_name='offer',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Created'),
        ),
        migrations.AddField(
            model_name='offer',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True, verbose_name='Modified'),
        ),
        migrations.AddField(
            model_name='projectofferpdf',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Created'),
        ),
        migrations.AddField(
            model_name='projectofferpdf',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True, verbose_name='Modified'),
        ),
        migrations.RunPython(
            prefill_fields,
            migrations.RunPython.noop
        )
    ]
