# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-04-25 15:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0005_auto_20180417_2122'),
    ]

    operations = [
        migrations.AddField(
            model_name='offer',
            name='position',
            field=models.PositiveSmallIntegerField(default=1),
        ),
    ]
