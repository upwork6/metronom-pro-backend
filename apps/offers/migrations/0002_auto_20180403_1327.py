# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-04-03 13:27
from __future__ import unicode_literals

import apps.metronom_commons.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campaignoffer',
            name='campaign_status',
            field=apps.metronom_commons.fields.ChoicesCharField(choices=[('created', 'Created'), ('sent', 'Sent'), ('accepted', 'Accepted'), ('partly_accepted', 'Partly Accepted'), ('declined', 'Declined'), ('replaced', 'Replaced'), ('canceled', 'Canceled')], default='created', max_length=15),
        ),
        migrations.AlterField(
            model_name='offer',
            name='offer_status',
            field=apps.metronom_commons.fields.ChoicesCharField(choices=[('created', 'Created'), ('sent', 'Sent'), ('accepted', 'Accepted'), ('partly_accepted', 'Partly Accepted'), ('declined', 'Declined'), ('replaced', 'Replaced'), ('canceled', 'Canceled')], default='created', max_length=15),
        ),
    ]
