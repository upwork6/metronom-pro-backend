# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-16 09:42
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0016_termscollectionpdf'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='campaignofferpdf',
            options={'ordering': ('-created_at',), 'verbose_name': 'Campaign Offer PDF', 'verbose_name_plural': 'Campaign Offers PDF'},
        ),
        migrations.AlterModelOptions(
            name='projectofferpdf',
            options={'ordering': ('-created_at',), 'verbose_name': 'Project Offer PDF', 'verbose_name_plural': 'Project Offers PDF'},
        ),
        migrations.AlterModelOptions(
            name='termscollectionpdf',
            options={'ordering': ('-created_at',), 'verbose_name': 'PDF Terms Collection', 'verbose_name_plural': 'PDF Terms Collections'},
        ),
    ]
