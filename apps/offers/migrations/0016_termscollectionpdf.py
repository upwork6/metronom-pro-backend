# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-12 12:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('backoffice', '0009_auto_20180502_1218'),
        ('offers', '0015_auto_20180511_1600'),
    ]

    operations = [
        migrations.CreateModel(
            name='TermsCollectionPDF',
            fields=[
                ('is_void', models.BooleanField(default=False, verbose_name='Is in void?')),
                ('id', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True, verbose_name='UUID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('context_data', models.TextField()),
                ('version', models.PositiveSmallIntegerField(default=1, verbose_name='Version of the current PDF Terms Collection')),
                ('replaced_item', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='replaced_by', to='offers.TermsCollectionPDF')),
                ('terms_collection', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='pdf_terms', to='backoffice.TermsCollection')),
            ],
            options={
                'verbose_name': 'PDF Terms Collection',
                'verbose_name_plural': 'PDF Terms Collections',
            },
        ),
    ]
