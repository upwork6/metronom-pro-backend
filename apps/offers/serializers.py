
from copy import deepcopy
from datetime import date

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from djmoney.contrib.django_rest_framework.fields import MoneyField
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from apps.backoffice.models import TermsCollection
from apps.metronom_commons.data import ACCOUNTING_TYPES
from apps.metronom_commons.serializers import SoftDeletionSerializer
from apps.offers.models import (ActivityArticleOfferedStatus, ActivityOffered,
                                ArticleOffered, CampaignOffer, Offer,
                                OfferStatus)
from apps.persons.models import Person
from apps.projects.base_serializers import SimpleUserSerializer

User = get_user_model()


class ContactPersonSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)

    class Meta:
        model = Person
        fields = ['uuid']


class OfferListSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id")

    class Meta:
        model = Offer
        fields = [
            'uuid',
            'name',
            'public_id',
            'position',
        ]


class CampaignOfferListSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id")

    class Meta:
        model = CampaignOffer
        fields = [
            'uuid',
            'title',
            'public_id',
        ]


class ActivityOfferedListSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id")

    class Meta:
        model = ActivityOffered
        fields = [
            'uuid',
            'name',
            'version',
        ]


class ArticleOfferedListSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id")

    class Meta:
        model = ArticleOffered
        fields = [
            'uuid',
            'name',
            'version',
        ]


class ActivityOfferedSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", read_only=True, required=False)
    offered_sum = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    offered_sum_currency = serializers.CharField(source='offered_sum.currency', read_only=True)
    name = serializers.CharField(read_only=True)
    sent_in_project_offers = OfferListSerializer(many=True, read_only=True)
    replaced_by = ActivityOfferedListSerializer(read_only=True)
    replaced_item = ActivityOfferedListSerializer(read_only=True)
    final_state = serializers.BooleanField(source='get_final_state')
    final_state_information = serializers.DictField(source='get_final_state_information')

    class Meta:
        model = ActivityOffered
        fields = [
            'uuid',
            'position',
            "version",
            'offer_status',
            'final_state',
            'final_state_information',
            "sent_in_project_offers",
            'name',
            'show_steps',
            'is_optional',
            'has_subtotal',
            'discount',
            'offered_sum',
            'offered_sum_currency',
            'project_activity',
            'replaced_by',
            'replaced_item'
        ]
        extra_kwargs = {
            'position': {'read_only': True},
            'version': {'read_only': True},
            'accepted_date': {'read_only': True}
        }


class ArticleOfferedSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", read_only=True, required=False)
    name = serializers.CharField(read_only=True)
    offered_sum = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    offered_sum_currency = serializers.CharField(source='offered_sum.currency', read_only=True)
    sent_in_project_offers = OfferListSerializer(many=True, read_only=True)
    replaced_by = ArticleOfferedListSerializer(read_only=True)
    replaced_item = ArticleOfferedListSerializer(read_only=True)
    final_state = serializers.BooleanField(source='get_final_state')
    final_state_information = serializers.DictField(source='get_final_state_information')

    class Meta:
        model = ArticleOffered
        fields = (
            'uuid',
            'position',
            "version",
            'offer_status',
            'final_state',
            'final_state_information',
            "sent_in_project_offers",
            'name',
            'is_external_cost',
            'is_optional',
            'has_subtotal',
            'offered_sum',
            'offered_sum_currency',
            'discount',
            'project_article',
            'replaced_by',
            'replaced_item'
        )
        extra_kwargs = {
            'position': {'read_only': True},
            'version': {'read_only': True},
            'accepted_date': {'read_only': True}
        }


class OfferSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id")
    terms_collection_uuid = serializers.PrimaryKeyRelatedField(queryset=TermsCollection.objects.all(),
                                                               source='terms_collection')
    contact_person = ContactPersonSerializer(read_only=True)
    offer_manager = SimpleUserSerializer(read_only=True)
    articles = ArticleOfferedSerializer(many=True, read_only=True)
    activities = ActivityOfferedSerializer(many=True, read_only=True)
    total_offer_without_tax = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_offer_without_tax_currency = serializers.CharField(source='total_offer_without_tax.currency', read_only=True)
    total_offer_with_tax = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_offer_with_tax_currency = serializers.CharField(source='total_offer_with_tax.currency', read_only=True)
    total_offer_send_to_customer = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_offer_send_to_customer_currency = serializers.CharField(
        source='total_offer_send_to_customer.currency', read_only=True)
    total_optional_costs = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_optional_costs_currency = serializers.CharField(source='total_optional_costs.currency', read_only=True)
    total_external_costs = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_external_costs_currency = serializers.CharField(source='total_external_costs.currency', read_only=True)
    total_discounts = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_discounts_currency = serializers.CharField(source='total_discounts.currency', read_only=True)
    used_tax_rate = serializers.FloatField(read_only=True)
    total_tax = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_tax_currency = serializers.CharField(source='total_tax.currency', read_only=True)
    sent_in_campaign_offers = CampaignOfferListSerializer(many=True, read_only=True)
    replaced_by = OfferListSerializer(read_only=True)
    replaced_item = OfferListSerializer(read_only=True)
    description = serializers.CharField(source="offer_description", required=False)
    final_state = serializers.BooleanField(source='get_final_state')
    final_state_information = serializers.DictField(source='get_final_state_information')

    class Meta:
        model = Offer
        fields = [
            'uuid',
            'terms_collection_uuid',
            'name',
            'is_optional',
            'public_id',
            'offer_status',
            'final_state',
            'final_state_information',
            'pdf',
            'description',
            'sent_in_campaign_offers',
            'total_offer_without_tax',
            'total_offer_without_tax_currency',
            'total_offer_with_tax',
            'total_offer_with_tax_currency',
            'total_offer_send_to_customer',
            'total_offer_send_to_customer_currency',
            'total_discounts',
            'total_discounts_currency',
            'total_optional_costs',
            'total_optional_costs_currency',
            'total_external_costs',
            'total_external_costs_currency',
            'offer_date',
            'valid_until_date',
            'callback_date',
            'contact_person',
            'offer_manager',
            'is_tax_included',
            'offer_description',
            'used_tax_rate',
            'total_tax',
            'total_tax_currency',
            'activities',
            'articles',
            'replaced_by',
            'replaced_item',
            'position',
        ]


class EditableActivityOfferedSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)
    offer = serializers.UUIDField(source="id", required=False)
    deleted = serializers.BooleanField(required=False)

    class Meta:
        model = ActivityOffered
        fields = [
            'uuid',
            'position',
            'is_optional',
            'has_subtotal',
            'discount',
            'show_steps',
            'offer',
            'deleted',
            'project_activity'
        ]
        extra_kwargs = {
            'version': {'read_only': True}
        }


class EditableArticleOfferedSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)
    offer = serializers.UUIDField(source="id", required=False)
    deleted = serializers.BooleanField(required=False)
    is_external_cost = serializers.BooleanField(read_only=True)

    class Meta:
        model = ArticleOffered
        fields = (
            'uuid',
            'position',
            'is_optional',
            'has_subtotal',
            'discount',
            'is_external_cost',
            'deleted',
            'offer',
            'project_article'
        )


class EditableOfferSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)
    terms_collection_uuid = serializers.PrimaryKeyRelatedField(queryset=TermsCollection.objects.all())
    offer_description = serializers.CharField(required=False)
    articles = EditableArticleOfferedSerializer(many=True, required=False)
    activities = EditableActivityOfferedSerializer(many=True, required=False)
    offer_manager = PrimaryKeyRelatedField(
        queryset=User.objects.all(),
        write_only=True
    )

    class Meta:
        model = Offer
        fields = [
            'uuid',
            'terms_collection_uuid',
            'offer_status',
            'name',
            'offer_description',
            'project',
            'offer_date',
            'valid_until_date',
            'callback_date',
            'contact_person',
            'offer_manager',
            'is_tax_included',
            'activities',
            'articles',
            'position',
        ]

    def create(self, data):
        offer = Offer.objects.create(
            **{
                'terms_collection': data.get('terms_collection_uuid'),
                'name': data.get('name'),
                'offer_description': data.get('offer_description', ''),
                'project': data.get('project'),
                'offer_status': OfferStatus.CREATED,
                'offer_date': data.get('offer_date', date.today()),
                'valid_until_date': data.get('valid_until_date'),
                'callback_date': data.get('callback_date'),
                'contact_person': data.get('contact_person'),
                'offer_manager': data.get('offer_manager'),
                'is_tax_included': data.get('is_tax_included', False),
            }
        )
        if data.get('activities', None):
            self.create_activities(data.pop('activities'), offer)
        if data.get('articles', None):
            self.create_articles(data.pop('articles'), offer)
        return offer

    @staticmethod
    def create_activities(activities, offer):
        for activity in activities:
            ActivityOffered.objects.create(offer=offer, **activity)

    @staticmethod
    def create_articles(articles, offer):
        for article in articles:
            project_article = article.get('project_article')
            if article.get('is_external_cost') == None:
                article['is_external_cost'] = project_article.is_external_cost
            ArticleOffered.objects.create(offer=offer, **article)

    def update(self, instance, data):
        keys = [
            'name',
            'offer_description',
            'project',
            'offer_date',
            'valid_until_date',
            'callback_date',
            'contact_person',
            'offer_manager',
            'is_tax_included',
            'terms_collection'
        ]
        for key in keys:
            if key == 'terms_collection':
                setattr(instance, key, data.get('terms_collection_uuid', getattr(instance, key)))
            setattr(instance, key, data.get(key, getattr(instance, key)))
        instance.save()
        items_keys = [
            'position',
            'is_optional',
            'has_subtotal',
            'discount',
        ]
        # Send `activities` List to update them
        if data.get('activities') is not None:
            self.update_activities(
                data.pop('activities'),
                instance,
                deepcopy(items_keys)
            )
        # Send `articles` List to update them
        if data.get('articles') is not None:
            self.update_articles(
                data.pop('articles'),
                instance,
                deepcopy(items_keys)
            )
        return instance

    @staticmethod
    def update_articles(articles, offer, keys):
        keys.append('is_external_cost')
        for article in articles:
            if article.get('id', None):
                # Retrieve ArticleOffered instance from DB
                instance = ArticleOffered.objects.get(pk=article['id'])
                # Check if the ArticleOffer should be deleted
                if article.get('deleted', None):
                    instance.delete()
                else:
                    # Update the ArticleOffer Instance
                    for key in keys:
                        setattr(instance, key, article.get(key, getattr(instance, key)))
                    instance.save()
                    if instance.offer_status == ActivityArticleOfferedStatus.IMPROVABLE:
                        instance.change_status_to_improved()
            else:
                # Create New ArticleOffer
                ArticleOffered.objects.create(offer=offer, **article)

    @staticmethod
    def update_activities(activities, offer, keys):
        keys.append('show_steps')
        for activity in activities:
            if activity.get('id', None):
                # Retrieve the ActivityOffered instance from DB
                instance = ActivityOffered.objects.get(pk=activity['id'])
                # Check if the activity should be deleted
                if activity.get('deleted', None):
                    instance.delete()
                else:
                    # Update the ActivityOffer Instance
                    for key in keys:
                        setattr(instance, key, activity.get(key, getattr(instance, key)))
                    instance.save()
                    if instance.offer_status == ActivityArticleOfferedStatus.IMPROVABLE:
                        instance.change_status_to_improved()
            else:
                # Create New Activities
                ActivityOffered.objects.create(offer=offer, **activity)


class CampaignOfferSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)
    terms_collection_uuid = serializers.PrimaryKeyRelatedField(queryset=TermsCollection.objects.all(),
                                                               source='terms_collection')
    contact_person = ContactPersonSerializer()
    offer_manager = SimpleUserSerializer(read_only=True)
    project_offers = OfferSerializer(many=True)
    total_campaign_offer_without_tax = MoneyField(
        max_digits=10, decimal_places=2, read_only=True
    )
    total_campaign_offer_without_tax_currency = serializers.CharField(
        source='total_offer_without_tax.currency', read_only=True
    )
    total_campaign_offer_with_tax = MoneyField(
        max_digits=10, decimal_places=2, read_only=True
    )
    total_campaign_offer_with_tax_currency = serializers.CharField(
        source='total_offer_with_tax.currency', read_only=True
    )
    total_campaign_offer_send_to_customer = MoneyField(
        max_digits=10, decimal_places=2, read_only=True
    )
    total_campaign_offer_send_to_customer_currency = serializers.CharField(
        source='total_offer_send_to_customer.currency', read_only=True
    )
    total_optional_costs = MoneyField(
        max_digits=10, decimal_places=2, read_only=True
    )
    total_optional_costs_currency = serializers.CharField(
        source='total_optional_costs.currency', read_only=True
    )
    total_external_costs = MoneyField(
        max_digits=10, decimal_places=2, read_only=True
    )
    total_external_costs_currency = serializers.CharField(
        source='total_external_costs.currency', read_only=True
    )
    total_discounts = MoneyField(
        max_digits=10, decimal_places=2, read_only=True
    )
    total_discounts_currency = serializers.CharField(
        source='total_discounts.currency', read_only=True
    )
    name = serializers.CharField(source="title", required=False)
    offer_status = serializers.CharField(source="campaign_status", required=False)

    class Meta:
        model = CampaignOffer
        fields = (
            'uuid',
            'public_id',
            'terms_collection_uuid',
            'title',
            'name',
            'pdf',
            'description',
            'offer_status',
            'campaign_status',
            'used_customer_reference',
            'offer_date',
            'valid_until_date',
            'callback_date',
            'contact_person',
            'offer_manager',
            'project_offers',
            'tax',
            'is_tax_included',
            'total_campaign_offer_without_tax',
            'total_campaign_offer_without_tax_currency',
            'total_campaign_offer_with_tax',
            'total_campaign_offer_with_tax_currency',
            'total_campaign_offer_send_to_customer',
            'total_campaign_offer_send_to_customer_currency',
            'total_optional_costs',
            'total_optional_costs_currency',
            'total_external_costs',
            'total_external_costs_currency',
            'total_discounts',
            'total_discounts_currency',
            'replaced_by',
            'replaced_item'
        )


class ShortOfferSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=True)
    deleted = serializers.BooleanField(required=False)
    accepted_in_campaign_offer = CampaignOfferListSerializer(read_only=True)

    class Meta:
        model = Offer
        fields = ['uuid', 'is_optional', 'accepted_in_campaign_offer', 'accepted_date', 'deleted', 'position']


class EditableCampaignOfferSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)
    terms_collection_uuid = serializers.PrimaryKeyRelatedField(queryset=TermsCollection.objects.all())
    project_offers = ShortOfferSerializer(many=True)
    offer_manager = PrimaryKeyRelatedField(
        queryset=User.objects.all(),
        write_only=True
    )

    class Meta:
        model = CampaignOffer
        fields = (
            'uuid',
            'terms_collection_uuid',
            'title',
            'description',
            'campaign_status',
            'offer_date',
            'campaign',
            'valid_until_date',
            'callback_date',
            'contact_person',
            'offer_manager',
            'project_offers',
            'tax',
            'is_tax_included'
        )

    def validate(self, data):
        if data.get('project_offers'):
            uuids = [x.get("id") for x in data.get('project_offers')]
            if Offer.objects.filter(id__in=uuids, project__accounting_type=ACCOUNTING_TYPES.INTERNAL).exists():
                raise serializers.ValidationError(
                    "You can't add project offer to the campaign offer if project has internal accounting_type"
                )
        return data

    def create(self, data):
        campaign = CampaignOffer.objects.create(
            **{
                'title': data.get('title'),
                'description': data.get('description', ''),
                'campaign': data.get('campaign'),
                'offer_date': data.get('offer_date', date.today()),
                'valid_until_date': data.get('valid_until_date'),
                'callback_date': data.get('callback_date'),
                'contact_person': data.get('contact_person'),
                'offer_manager': data.get('offer_manager'),
                'tax': data.get('tax', 0.0),
                'is_tax_included': data.get('is_tax_included', 0.0),
                'terms_collection': data.get('terms_collection_uuid')
            }
        )
        if data.get('project_offers', None):
            campaign.project_offers.add(*[offer["id"] for offer in data.get('project_offers', [])])
        return campaign

    def update(self, instance, data):
        keys = [
            'title',
            'description',
            'offer_date',
            'valid_until_date',
            'callback_date',
            'contact_person',
            'offer_manager',
            'tax',
            'is_tax_included',
            'terms_collection'
        ]
        for key in keys:
            if key == 'terms_collection':
                setattr(instance, key, data.get('terms_collection_uuid', getattr(instance, key)))
            setattr(instance, key, data.get(key, getattr(instance, key)))
        # Manage project_offers
        if data.get('project_offers', None) is not None:
            for offer in data.get('project_offers', []):
                # take Offer instance from db to change it or delete it
                db_offer = Offer.objects.get(pk=offer['id'])
                # Edit Offer if `is_optional`
                if offer.get('is_optional', None) is not None:
                    db_offer.is_optional = offer.get('is_optional')
                    db_offer.save()
                # Remove Offer from Campaign if `deleted` flag is present
                if offer.get('deleted', None):
                    instance.project_offers.remove(db_offer)
        instance.save()
        return instance


class ActivityOfferedStatusSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id")

    class Meta:
        model = ActivityOffered
        fields = [
            'uuid',
            'offer_status'
        ]


class ArticleOfferedStatusSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id")

    class Meta:
        model = ArticleOffered
        fields = [
            'uuid',
            'offer_status'
        ]


class OfferStatusSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)
    articles = ArticleOfferedStatusSerializer(many=True, required=False)
    activities = ActivityOfferedStatusSerializer(many=True, required=False)

    class Meta:
        model = Offer
        fields = [
            'uuid',
            'offer_status',
            'activities',
            'articles',
        ]

    def update(self, instance, data, from_campaign=False):
        """If at leas ONE Item inside the ProjectOffer receives status of REPLACED then
        all the Offer has to be replaced and all its articles/activities from inside:
        The previously ACCEPTED are CLONED with the status of ACCEPTED and will be part of the new instance as well
        The previously DECLINED are left behind"""
        if not from_campaign and instance.campaign_offers.count():
            raise ValidationError(
                {"offer_status": _("The Project Offer can not be  edited "
                                   "alone when is part of a Campaign Offer.")})
        else:
            activities, articles = [], []
            if data.get('activities', None):
                activities = data.pop('activities')
                self.update_activities(instance, activities)
            if data.get('articles', None):
                articles = data.pop('articles')
                self.update_articles(instance, articles)
            # Collect all the offer_status to check and filter for later
            items_status_list = [
                *[activity.get('offer_status') for activity in activities],
                *[article.get('offer_status') for article in articles],
                data.get('offer_status', None)]
            # The PROJECT_OFFER is changed alone only when receiving Explicit `offer_status` OR at least 1 `REPLACED`
            if data.get('offer_status', None) and data.get('offer_status') != OfferStatus.REPLACED \
                    and instance.offer_status in [OfferStatus.SENT, OfferStatus.CREATED]:
                # Explicit Status Change
                change_offer_status(instance, data.get('offer_status'))
            elif OfferStatus.REPLACED in items_status_list and \
                    instance.offer_status in [OfferStatus.SENT, OfferStatus.CREATED, OfferStatus.PARTLY_ACCEPTED]:
                # There is at least ONE Item that has to be replaced which means All ProjectOffer will be REPLACED
                instance.change_status_to_replaced()
            else:
                # if None Of the above Conditions are met, we review the ProjectOffer to give it a status
                # This will check for a Unanimity in Accepted, Decline OR mixed of Partly_Accepted

                instance.review_the_project_offer()
            instance.refresh_from_db()
            return instance

    @staticmethod
    def update_activities(instance, activities):
        for db_activity in instance.activities.filter(offer_status__in=[OfferStatus.SENT, OfferStatus.CREATED]):
            for activity in activities:
                if activity.get('id') == db_activity.id and \
                        activity.get('offer_status') != OfferStatus.REPLACED:
                    change_offer_status(db_activity, activity.get('offer_status'))

    @staticmethod
    def update_articles(instance, articles):
        for db_article in instance.articles.filter(offer_status__in=[OfferStatus.SENT, OfferStatus.CREATED]):
            for article in articles:
                if article.get('id') == db_article.id and \
                        article.get('offer_status') != OfferStatus.REPLACED:
                    change_offer_status(db_article, article.get('offer_status'))


class CampaignOfferStatusSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)
    project_offers = OfferStatusSerializer(many=True, required=False)

    class Meta:
        model = CampaignOffer
        fields = [
            'uuid',
            'campaign_status',
            'project_offers'
        ]

    def update(self, instance, data):
        offers, items_status = [], []
        items_status.append(data.get('campaign_status'))
        if data.get('project_offers'):
            offers = data.pop('project_offers')
        if offers:
            for offer in offers:
                items_status.append(offer.get('offer_status'))
                if offer.get('activities'):
                    for activity in offer.get('activities'):
                        items_status.append(activity.get('offer_status'))
                if offer.get('articles'):
                    for article in offer.get('articles'):
                        items_status.append(article.get('offer_status'))
            self.update_offers(instance, offers)
        if OfferStatus.REPLACED not in items_status and data.get('campaign_status', None) and \
                instance.campaign_status in [OfferStatus.CREATED, OfferStatus.SENT]:
            change_offer_status(instance, data.get('campaign_status'))
        elif OfferStatus.REPLACED in items_status and \
                instance.campaign_status in [OfferStatus.CREATED, OfferStatus.SENT, OfferStatus.PARTLY_ACCEPTED]:
            instance.change_status_to_replaced()
        else:
            # if None Of the above Conditions are met, we review the CampaignOffer to give it a status
            # This will check for a Unanimity in Accepted, Decline OR mixed of Partly_Accepted
            instance.review_the_campaign_offer()
        instance.refresh_from_db()
        return instance

    def update_offers(self, instance, project_offers):
        if project_offers:
            for db_offer in instance.project_offers.filter(offer_status__in=[OfferStatus.CREATED, OfferStatus.SENT]):
                for offer in project_offers:
                    if offer.get('offer_status') != OfferStatus.REPLACED and db_offer.id == offer.get('id'):
                        offer_serializer = OfferStatusSerializer()
                        offer_serializer.update(db_offer, offer, True)


def change_offer_status(instance=None, status=None):
    if status == OfferStatus.SENT:
        instance.change_status_to_sent()
    elif status == OfferStatus.ACCEPTED:
        instance.change_status_to_accepted()
    elif status == OfferStatus.DECLINED:
        instance.change_status_to_declined()
    else:
        pass


class ProjectOfferSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)
    total_offer_without_tax = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_offer_without_tax_currency = serializers.CharField(
        source='total_offer_without_tax.currency', read_only=True)
    total_offer_with_tax = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_offer_with_tax_currency = serializers.CharField(source='total_offer_with_tax.currency',
                                                          read_only=True)
    total_offer_send_to_customer = MoneyField(max_digits=10, decimal_places=2, read_only=True)
    total_offer_send_to_customer_currency = serializers.CharField(
        source='total_offer_send_to_customer.currency', read_only=True)

    improvable_count = serializers.SerializerMethodField(read_only=True)
    improved_count = serializers.SerializerMethodField(read_only=True)
    final_state = serializers.BooleanField(source='get_final_state')
    final_state_information = serializers.DictField(source='get_final_state_information')

    class Meta:
        model = Offer
        fields = [
            'uuid',
            'name',
            'public_id',
            'offer_status',
            'final_state',
            'final_state_information',
            'total_offer_without_tax',
            'total_offer_without_tax_currency',
            'total_offer_with_tax',
            'total_offer_with_tax_currency',
            'total_offer_send_to_customer',
            'total_offer_send_to_customer_currency',
            'improvable_count',
            'improved_count',
            'position',
        ]

    def get_improved_count(self, obj):
        return obj.activities.filter(offer_status="improved").count() + obj.articles.filter(offer_status="improved").count()

    def get_improvable_count(self, obj):
        return obj.activities.filter(offer_status="improvable").count() + obj.articles.filter(offer_status="improvable").count()
