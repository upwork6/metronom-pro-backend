from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils.translation import ugettext_lazy as _
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework.mixins import CreateModelMixin, ListModelMixin, UpdateModelMixin, DestroyModelMixin
from apps.api.filters import MetronomPermissionsFilter
from apps.api.mixins import LoggerMixin
from apps.metronom_commons.models import ROLE
from apps.metronom_commons.choices_flow import OfferStatus
from apps.metronom_commons.mixins.views import DeleteForbiddenIfUsedMixin
from dry_rest_permissions.generics import DRYPermissions
from django.views import View
from apps.offers.models import CampaignOffer, Offer, ProjectOfferPDF, CampaignOfferPDF
from apps.offers.serializers import (CampaignOfferSerializer,
                                     EditableCampaignOfferSerializer,
                                     EditableOfferSerializer, OfferSerializer,
                                     OfferStatusSerializer, CampaignOfferStatusSerializer)
from apps.metronom_commons.pagination import MetronomPageNumberPagination
from apps.offers.filters import ProjectOfferFilterBackend
from apps.projects.models import Project, Campaign
from django.shortcuts import get_object_or_404
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.db.models.query import Prefetch


class OffersViewSet(LoggerMixin, GenericViewSet, CreateModelMixin, ListModelMixin, UpdateModelMixin, DestroyModelMixin):
    http_method_names = ['get', 'post', 'patch', 'delete', 'head', 'options', 'trace', ]
    filter_backends = (MetronomPermissionsFilter,)
    permission_classes = (DRYPermissions,)

    def get_serializer_class(self):
        if self.action == 'create' or self.action == 'partial_update':
            return EditableOfferSerializer
        return OfferSerializer

    def get_queryset(self):
        project_pk = self.kwargs.get('project_pk')
        offer_pk = self.kwargs.get('pk')
        project = Project.objects.get(pk=project_pk)
        if not project:
            return Project.objects.none()
        if offer_pk:
            return project.offer_set.get(pk=offer_pk)
        return project.offer_set.all().order_by("position")

    # This is just to keep Swagger pleased
    def list(self, request, *args, **kwargs):
        """ All the Offers from Project {uuid} with Activities & Articles inside"""
        uuid = kwargs.get("project_pk")
        try:
            project = Project.objects.get(id=uuid)
            offers = Offer.objects.filter(project_id__in=[uuid]).order_by("position")#.select_related(
                #'project').select_related('offer_manager').select_related('contact_person')
        except Project.DoesNotExist:
            return Response([_(f"No Project Found with this uuid: {uuid}")],
                            status=status.HTTP_400_BAD_REQUEST)
        return Response(OfferSerializer(offers, many=True).data)

    def create(self, request, *args, **kwargs):
        """ Create new Offer for Project {uuid} with Activities & Articles inside """
        project = Project.objects.get(id=kwargs['project_pk'])
        if request.user.role not in [ROLE.ADMIN, ROLE.ACCOUNTING, ROLE.OWNER]:
            if request.user != project.project_owner:
                raise PermissionDenied
        serializer = self.get_serializer(data=request.data)
        serializer.initial_data['project'] = kwargs['project_pk']
        if serializer.is_valid():
            offer = serializer.create(serializer.validated_data)
            headers = self.get_success_headers(serializer.data)
            return Response(
                OfferSerializer(offer).data,
                status=status.HTTP_201_CREATED,
                headers=headers
            )
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )

    def partial_update(self, request, *args, **kwargs):
        """ Partial Update Offer {id} from {project_pk} Project with Activities & Articles inside.
        To `PATCH` activity/article `object` you need to send together the `uuid` of that object:
        ```json
        {
          "activities": [
            {
              "uuid": "cb71c71a-195b-4137-915f-a9da71d031e1",
              "discount": 2000,
            }
          ]
        }
        ```
        To `DELETE` an activity/article `object` you need to send together the `uuid` of that object with `delete` flag set to `True`:
        ```json
        {
          "activities": [
            {
              "uuid": "cb71c71a-195b-4137-915f-a9da71d031e1",
              "delete": true,
            }
          ]
        }
        ```
        """
        instance = self.get_queryset()
        if request.user.role == ROLE.USER and request.user != instance.offer_manager:
            raise PermissionDenied
        serializer = self.get_serializer(instance=instance, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.update(instance=instance, data=serializer.validated_data)
            return Response(OfferSerializer(instance=instance).data, status=status.HTTP_200_OK)
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )

    def destroy(self, request, *args, **kwargs):
        """Delete ProjectOffer {id} from {project_pk} Project with Activities & Articles inside it is not SENT """

        instance = self.get_queryset()
        if request.user.role == ROLE.USER and request.user != instance.offer_manager:
            raise PermissionDenied
        if instance.offer_status != OfferStatus.CREATED:
            raise ValidationError(
                {"__all__": _(f"Offer {instance.name} can't be deleted while it is {instance.offer_status}")}
            )
        else:
            instance.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)


class CampaignOffersViewSet(LoggerMixin, ModelViewSet):
    """Campaign Offers"""

    queryset = CampaignOffer.objects.all()
    http_method_names = ['post', 'patch', 'delete', 'head', 'options', 'trace']
    filter_backends = (MetronomPermissionsFilter,)
    permission_classes = (DRYPermissions,)

    def get_serializer_class(self):
        if self.action == 'create' or 'partial_update':
            return EditableCampaignOfferSerializer
        return CampaignOfferSerializer

    def create(self, request, *args, **kwargs):
        """ Create new Campaign Offer for Campaign Project {uuid} Project Offers inside """

        campaign = Campaign.objects.get(id=kwargs['campaign_pk'])
        if request.user.role not in [ROLE.ADMIN, ROLE.ACCOUNTING, ROLE.OWNER]:
            if request.user != campaign.campaign_owner:
                raise PermissionDenied
        serializer = self.get_serializer(data=request.data)
        serializer.initial_data['campaign'] = kwargs['campaign_pk']
        if serializer.is_valid():
            # let the EditableCampaignOfferSerializer.create() do the work
            campaign = serializer.create(serializer.validated_data)
            headers = self.get_success_headers(serializer.data)
            return Response(
                CampaignOfferSerializer(instance=campaign).data, status=status.HTTP_201_CREATED, headers=headers
            )
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )

    def partial_update(self, request, *args, **kwargs):
        """ Partial Update Offer {id} from {uuid} Project with Activities & Articles inside.
        """
        try:
            instance = CampaignOffer.objects.get(id=kwargs['pk'])
            if request.user.role == ROLE.USER and request.user != instance.offer_manager:
                raise PermissionDenied
            serializer = self.get_serializer(instance=instance, data=request.data, partial=True)
            if serializer.is_valid():
                # let the EditableCampaignOfferSerializer.update() do the work
                campaign = serializer.update(instance, serializer.validated_data)
                return Response(CampaignOfferSerializer(instance=campaign).data, status=status.HTTP_200_OK)
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )
        except ObjectDoesNotExist:
            return Response(
                _("Campaign Offer not found."),
                status=status.HTTP_400_BAD_REQUEST
            )

    def destroy(self, request, *args, **kwargs):
        """Delete ProjectOffer {id} from {project_pk} Project with Activities & Articles inside it is not SENT """
        try:
            instance = CampaignOffer.objects.get(id=kwargs['pk'])
            if request.user.role == ROLE.USER and request.user != instance.offer_manager:
                raise PermissionDenied
            if instance.campaign_status != OfferStatus.CREATED:
                raise ValidationError(
                    {"__all__": _(
                        f"Campaign Offer {instance.name} can't be deleted while it is {instance.offer_status}")}
                )
            else:
                instance.delete()
                return Response(status=status.HTTP_204_NO_CONTENT)
        except ObjectDoesNotExist:
            return Response(
                _("Campaign Offer not found."),
                status=status.HTTP_400_BAD_REQUEST
            )


class OffersStatusViewSet(GenericViewSet, UpdateModelMixin, LoggerMixin):
    """Change The Offer Status Only """
    queryset = Offer.objects.all()
    serializer_class = OfferStatusSerializer
    filter_backends = (MetronomPermissionsFilter,)
    permission_classes = [IsAuthenticated, ]

    def partial_update(self, request, *args, **kwargs):
        offer = get_object_or_404(self.queryset, pk=kwargs['id'])
        serializer = self.get_serializer(instance=offer, data=request.data, partial=True)
        if serializer.is_valid():
            offer = serializer.update(offer, serializer.validated_data)
            return Response(OfferSerializer(instance=offer).data, status=status.HTTP_200_OK)
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class CampaignOfferStatusViewSet(GenericViewSet, UpdateModelMixin, LoggerMixin):
    """Change The Campaign Offer Status Only """
    queryset = CampaignOffer.objects.all()
    serializer_class = CampaignOfferStatusSerializer
    filter_backends = (MetronomPermissionsFilter,)
    permission_classes = [IsAuthenticated, ]

    def partial_update(self, request, *args, **kwargs):
        campaign = get_object_or_404(self.queryset, pk=kwargs['id'])
        serializer = self.get_serializer(instance=campaign, data=request.data, partial=True)
        if serializer.is_valid():
            offer = serializer.update(campaign, serializer.validated_data)
            return Response(CampaignOfferSerializer(instance=offer).data, status=status.HTTP_200_OK)
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class LatestProjectOfferPDFView(View):
    http_method_names = ['get', 'patch', 'head', 'options', 'trace']

    def get(self, request, project_pk, offer_pk):
        pdf_parameters = {}
        if request.GET:
            pdf_parameters = request.GET
        project_offer = Offer.objects.get(id=offer_pk)
        project_offer_pdf = ProjectOfferPDF.get_the_latest_version(project_offer=project_offer)
        alt_response = project_offer_pdf.get_pdf_document(pdf_parameters)
        response_pdf = HttpResponse(alt_response.content, content_type='application/pdf')
        return response_pdf


class ProjectOfferPDFView(View):
    http_method_names = ['get', 'patch', 'head', 'options', 'trace']

    def get(self, request, project_pk, offer_pk, id):
        pdf_get_url__parameters = {}
        if request.GET:
            pdf_get_url__parameters = request.GET
        project_offer_pdf = ProjectOfferPDF.objects.get(id=id)
        proxy_response = project_offer_pdf.get_pdf_document(pdf_get_url__parameters)
        response_pdf = HttpResponse(proxy_response.content, content_type='application/pdf')
        return response_pdf


class LatestCampaignOfferPDFView(View):
    http_method_names = ['get', 'patch', 'head', 'options', 'trace']

    def get(self, request, campaign_pk, offer_pk):
        pdf_parameters = {}
        if request.GET:
            pdf_parameters = request.GET
        campaign_offer = CampaignOffer.objects.get(id=offer_pk)
        campaign_offer_pdf = CampaignOfferPDF.get_the_latest_version(campaign_offer=campaign_offer)
        proxy_response = campaign_offer_pdf.get_pdf_document(pdf_parameters)
        campaign_offer_response_pdf = HttpResponse(proxy_response.content, content_type='application/pdf')
        return campaign_offer_response_pdf


class CampaignOfferPDFView(View):
    http_method_names = ['get', 'patch', 'head', 'options', 'trace']

    def get(self, request, campaign_pk, offer_pk, id):
        pdf_get_url_parameters = {}
        if request.GET:
            pdf_get_url_parameters = request.GET
        campaign_offer_pdf = CampaignOfferPDF.objects.get(id=id)
        proxy_response = campaign_offer_pdf.get_pdf_document(pdf_get_url_parameters)
        campaign_offer_response_pdf = HttpResponse(proxy_response.content, content_type='application/pdf')
        return campaign_offer_response_pdf


class ProjectOffersSearchView(ModelViewSet):
    """ProjectOffer Search: /api/offers/?parameter_1={value_1}&#38;parameter_2={value_2}

        Filter parameters:
        - `company_uuid`
        - `offer_name`
        - `exclude_status`
        - `has_status`
        - ...more...later
    """
    http_method_names = ['get','head', 'options', 'trace', ]
    serializer_class = OfferSerializer
    queryset = Offer.objects.all().order_by("created_at").prefetch_related('activities').prefetch_related(
        'articles').select_related('project').select_related('offer_manager').select_related(
        'contact_person').select_related('terms_collection')
    pagination_class = MetronomPageNumberPagination
    filter_backends = (ProjectOfferFilterBackend,)
    search_fields = ('offer_description', 'public_id', 'name', 'project__company__name')
    permission_classes = (DRYPermissions,)
