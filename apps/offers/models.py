
from ast import literal_eval
from decimal import Decimal
from itertools import chain

import requests
from django.conf import settings
from django.contrib.postgres.fields import HStoreField
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from djmoney.models.fields import MoneyField
from djmoney.money import Money
from dry_rest_permissions.generics import authenticated_users

from apps.backoffice.models import TermsCollection
from apps.backoffice.utils import get_setting
from apps.metronom_commons.choices_flow import (ActivityArticleOfferedStatus,
                                                OfferStatus)
from apps.metronom_commons.fields import ChoicesCharField
from apps.metronom_commons.mixins import ProtectedFieldsMixin, ReplaceMixin
from apps.metronom_commons.models import (ROLE,
                                          OfferAndInvoiceMetronomBaseModel,
                                          OrderingMixinWithoutSignal,
                                          SoftDeleteMixin,
                                          UserCRUAccountingDeletePermission,
                                          UserReadOnlyAccountingMetronomBaseModel)
from apps.metronom_commons.pdf_utils import (CampaignOfferPDFUtils,
                                             ProjectOfferPDFUtils,
                                             TermsCollectionPDFUtils)
from apps.metronom_commons.utils import all_statuses_are_the_same
from apps.persons.models import Person
from apps.projects.models import (Campaign, Project, ProjectActivity,
                                  ProjectArticle)
from apps.users.models import User

# TODO: Use djmoney_rates to to the conversion
# from djmoney_rates.utils import convert_money
# brl_money = convert_money(10, "EUR", "BRL")


def convert_money_object(money, old_currency, new_currency):
    """ This is a placeholder method, we will use later """
    return money


def convert_money_value(value, old_currency, new_currency):
    """ This is a placeholder method, we will use later """
    return Money(value, new_currency)


class OfferOrCampaignOffer(OfferAndInvoiceMetronomBaseModel, ProtectedFieldsMixin, ReplaceMixin):
    """Abstract Model to offer the common fields necessary for Offer(ProjectOffer) and CampaignOffer"""

    offer_date = models.DateField(_('offer date'))
    valid_until_date = models.DateField(_('valid until date'))
    callback_date = models.DateField(_('callback date'))
    offer_manager = models.ForeignKey(
        User,
        verbose_name=_('offer manager'),
        null=True,
        on_delete=models.SET_NULL,
    )
    contact_person = models.ForeignKey(
        Person,
        verbose_name=_('contact person'),
        null=True,
        on_delete=models.SET_NULL,
    )
    terms_collection = models.ForeignKey(TermsCollection, default=None, null=True)
    used_customer_reference = models.TextField(_('customer reference'), blank=True, default="")
    is_tax_included = models.BooleanField(_('calculate tax in total'), default=False)

    class Meta:
        abstract = True


class Offer(SoftDeleteMixin, OfferOrCampaignOffer, OrderingMixinWithoutSignal):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    is_optional = models.BooleanField(_('This item is optional'), default=False)
    name = models.CharField(_('offer name'), max_length=128)
    offer_description = models.TextField(_('description'))
    number = models.PositiveSmallIntegerField(_("Offer number for project"), default=1)
    public_id = models.CharField(
        _('Project Offer Public ID'),
        max_length=14,
        unique=True,
        editable=False,
    )
    offer_status = ChoicesCharField(max_length=15, choices=OfferStatus, default=OfferStatus.CREATED)

    class Meta:
        verbose_name = _("Offer")
        verbose_name_plural = _("Offers")
        ordering = ('-created_at',)

    def __str__(self):
        return f"{self.name}"

    @cached_property
    def key_account_manager(self):
        """Return Project.Company.key_account_manager User or None"""
        if self.project.company.key_account_manager:
            return self.project.company.key_account_manager
        return None

    @cached_property
    def owner(self):
        """Return The Owner of this object => Offer_Manager"""
        return self.offer_manager

    @property
    def pdf(self):
        latest_pdf_url = f"{settings.BACKEND_URL}{reverse('latest-project-offer-pdf', args=[self.project.id, self.id])}"
        all_pdf_versions = [
            {
                "pdf_url": f"{settings.BACKEND_URL}"
                           f"{reverse('project-offer-pdf', args=[offer_pdf.project_offer.project.id, offer_pdf.project_offer.id, offer_pdf.id])}",
                "created_at": offer_pdf.created_at.strftime("%Y-%m-%d %H:%M:%S"),
                "pdf_version": offer_pdf.version
            } for offer_pdf in self.pdf_documents.all()
        ]
        return {
            "latest_pdf_version": latest_pdf_url,
            "all_pdf_versions": all_pdf_versions
        }

    def get_sent_in_campaign_offers(self):
        """Return:
        None or a list of Campaign Offers with UUID+Name,
        where the Offer was sent in
        """
        return self.campaign_offers.exclude(campaign_status=OfferStatus.CREATED)

    @property
    def sent_in_campaign_offers(self):
        """Return:
        None or a list of Project offers with UUID+Name,
        where the Project activity/article was sent in
        """
        if self.get_sent_in_campaign_offers():
            return self.get_sent_in_campaign_offers()
        return None

    @property
    def accepted_date(self):
        """Return:
        None or the Date, when the Campaign Offer was accepted
        """
        if self.accepted_in_campaign_offer:
            return self.accepted_in_campaign_offer.offer_date
        return None

    @property
    def accepted_in_campaign_offer(self):
        """Return:
         None or a Serializer with the Public_ID, UUID and name of the Campaign Offer,
         where the Project activity was accepted in
        """
        if self.get_accepted_in_campaign_offer():
            return self.get_accepted_in_campaign_offer()
        return None

    def get_final_state(self):
        if self.get_final_state_information():
            return True
        return False

    def get_final_state_information(self):
        if self.get_accepted_or_declined_in_campaign_offer():
            accepted_or_declined_in_campaign_offer = self.get_accepted_or_declined_in_campaign_offer()
            final_state_information = {
                "offer_status": accepted_or_declined_in_campaign_offer.campaign_status,
                "offer_type": "campaign_offer",
                "date": accepted_or_declined_in_campaign_offer.offer_date,
                "offer": {
                    "public_id": accepted_or_declined_in_campaign_offer.public_id
                }
            }
            return final_state_information
        return None

    def get_accepted_or_declined_in_campaign_offer(self):
        # Filter for the first ACCEPTED One
        result = self.campaign_offers.filter(
            campaign_status__in=[OfferStatus.ACCEPTED, OfferStatus.DECLINED]
        ).order_by('-updated_at').first()
        return result

    # DRY: this is necessary in both methods: `accepted_in_offer()` and `accepted_date()`
    def get_accepted_in_campaign_offer(self):
        # Filter for the first ACCEPTED One
        result = self.campaign_offers.filter(campaign_status=OfferStatus.ACCEPTED).order_by('-updated_at').first()
        return result

    @cached_property
    def used_tax_rate(self):
        if self.project.company.liable_for_taxation is True:
            return float(get_setting('DEFAULT_TAX'))
        else:
            return float(0.0)

    @cached_property
    def project_parent(self):
        """Return Project || Campaign depending if is ProjectOffer or CampaignOffer"""
        return self.project

    @cached_property
    def currency(self):
        return self.project.billing_currency

    @property
    def all_items(self):
        return list(chain(self.activities.all().select_related('project_activity'), self.articles.select_related('project_article').all()))

    def get_total_offer_without_tax(self, currency=None):
        """ The total of the offer including discounts but without tax """
        value = sum(item.offered_sum_with_discount.amount for item in self.all_items)

        if currency != self.currency and currency is not None:
            return convert_money_value(value, self.currency, currency)

        return Money(value, self.currency)

    @property
    def total_offer_without_tax(self):
        return self.get_total_offer_without_tax(self.currency)

    def get_total_tax(self, currency=None):
        """ The total of the offer, also including tax"""

        tax = self.total_offer_without_tax.amount * Decimal(self.used_tax_rate) / 100

        if currency != self.currency and currency is not None:
            return convert_money_value(tax, self.currency, currency)
        return Money(tax, self.currency)

    @property
    def total_tax(self):
        return self.get_total_tax(self.currency)

    def get_total_offer_with_tax(self, currency=None):
        """ The total of the offer, also including tax"""

        value = self.total_offer_without_tax.amount + self.total_tax.amount

        if currency != self.currency and currency is not None:
            return convert_money_value(value, self.currency, currency)
        return Money(value, self.currency)

    @property
    def total_offer_with_tax(self):
        return self.get_total_offer_with_tax(self.currency)

    @property
    def total_offer_send_to_customer(self):
        return self.get_total_offer_send_to_customer(self.currency)

    def get_total_offer_send_to_customer(self, currency=None):
        """ The total of the offer, how it was send to the customer"""

        if self.is_tax_included:
            money_value = self.total_offer_with_tax
        else:
            money_value = self.total_offer_without_tax

        if currency != self.currency and currency is not None:
            return convert_money_object(money_value, self.currency, currency)

        return money_value

    def get_total_optional_costs(self, currency=None):
        """ Total optional costs in the Offer"""

        value = sum(item.offered_sum.amount for item in self.all_items if item.is_optional == True)

        if currency != self.currency and currency is not None:
            return convert_money_object(value, self.currency, currency)
        return Money(value, self.currency)

    @property
    def total_optional_costs(self):
        return self.get_total_optional_costs(self.currency)

    def get_total_external_costs(self, currency=None):
        """ Total external costs in the Offer"""
        offer_value = sum(article.offered_sum.amount for article in self.all_items if article.is_external_cost)

        if currency != self.currency and currency is not None:
            return convert_money_value(offer_value, self.currency, currency)
        return Money(offer_value, self.currency)

    @property
    def total_external_costs(self):
        return self.get_total_external_costs(self.currency)

    def get_total_discounts(self, currency=None):
        """ The total of the offer including discounts but without tax """

        if not currency:
            currency = self.currency
        value = sum(item.discount.amount for item in self.all_items)

        if currency != self.currency and currency is not None:
            return convert_money_value(value, self.currency, currency)
        return Money(value, self.currency)

    @property
    def total_discounts(self):
        return self.get_total_discounts(self.currency)

    def change_status_to_sent(self):
        """PROJECT OFFER"""
        if not self.offer_description:
            self.offer_description = self.project.description
        if self.offer_status == OfferStatus.CREATED:
            self.offer_status = OfferStatus.SENT
        self.save()
        list(item.change_status_to_sent() for item in self.all_items)

    def change_status_to_accepted(self):
        """PROJECT OFFER"""
        list(item.change_status_to_accepted() for item in self.all_items)
        # If ProjectArticle can be ACCEPTED then mark the ArticleOffered as ACCEPTED
        self.offer_status = OfferStatus.ACCEPTED
        self.save()

    def change_status_to_declined(self):
        """PROJECT OFFER"""
        list(item.change_status_to_declined() for item in self.all_items)
        # If ProjectArticle can be ACCEPTED then mark the ArticleOffered as ACCEPTED
        self.offer_status = OfferStatus.DECLINED
        self.save()

    def replace_activities_and_articles(self, clone):
        for item in self.all_items:
            item.change_status_to_replaced(offer=clone)

    def has_to_be_replaced(self):
        if OfferStatus.REPLACED in self.activity_and_article_status_list:
            return True
        return False

    def change_status_to_replaced(self):
        """PROJECT OFFER"""
        # All Offer Activities and Articles not DECLINED or ACCEPTED
        # will be Cloned and added to the New Cloned Offer together with already
        #  ACCEPTED Activities and Articles
        self.offer_status = OfferStatus.REPLACED
        self.save()
        modified_kwargs = dict([
            ("public_id", ''),
            ('offer_status', OfferStatus.CREATED),
            ('replaced_item', self),
            ('created_at', now())
        ])
        clone = self.clone(**modified_kwargs)
        self.replace_activities_and_articles(clone)
        return self

    @property
    def activity_and_article_status_list(self):
        return list(item.offer_status for item in self.all_items)

    def review_the_project_offer(self):
        """The offer has only accepted AND declined Project offers or
        activities / articles (green with dots icon) --> partly_accepted
        """
        # If at least one item inside Offer is not accepted OR declined
        # it can not have status of 'partly_accepted'
        if self.activity_and_article_status_list and all_statuses_are_the_same(self.activity_and_article_status_list):
            if self.offer_status != self.activity_and_article_status_list[0]:
                self.offer_status = self.activity_and_article_status_list[0]
                self.save()
        else:
            if OfferStatus.DECLINED in self.activity_and_article_status_list and \
                    OfferStatus.ACCEPTED in self.activity_and_article_status_list and \
                    OfferStatus.REPLACED not in self.activity_and_article_status_list:
                self.offer_status = OfferStatus.PARTLY_ACCEPTED
                self.save()

    def save(self, *args, **kwargs):
        if (not self.project.offer_set.all(force_visibility=True).filter(id=self.id).exists() and
                self.project.offer_set.all(force_visibility=True).exists() and self.number == 1):
            # we need to increase number according to the offers
            self.number = self.project.offer_set.all(force_visibility=True).count() + 1
        if not self.public_id:
            # we need to increase number according to the offers
            number = self.project.offer_set.all(force_visibility=True).count() + 1
            public_id = f'O-{self.project.public_id.split("-")[1]}-{self.project.public_id.split("-")[2]}-{number}'
            self.public_id = public_id
        # Validate the `valid_until_date` and `callback_date`
        if self.offer_date > self.valid_until_date:
            raise ValidationError(
                _('The valid_until_date cannot be earlier than the offer_date.'),
                code='invalid_valid_until_date',
            )
        if self.offer_date > self.callback_date:
            raise ValidationError(
                _('The callback_date cannot be earlier than the offer_date.'),
                code='invalid_callback_date',
            )
        self.full_clean()
        super(Offer, self).save(*args, **kwargs)

    def get_used_customer_reference(self):
        """ Retrieve used_customer_reference from Campaign Offer or customer_reference from Campaign
        """
        if self.used_customer_reference:
            return self.used_customer_reference
        return self.project.customer_reference

    @staticmethod
    def recalculate_position(sender, instance, created, *args, **kwargs):
        """ This is post_save signal to proceed the position for the Offer
        """
        if created:
            position = 0
            qs = sender.objects.exclude(id=instance.id).filter(project=instance.project)
            # in some cases we allow to create objects with pre-defined positions
            if not instance.position or qs.filter(position=instance.position).exists():
                last_instance = qs.order_by('-position').first()
                if last_instance:
                    position = last_instance.position
                instance.position = position + 1
                instance.save(update_fields=['position'])

    PROTECTED_STATES = [OfferStatus.SENT, OfferStatus.ACCEPTED, OfferStatus.DECLINED]
    PROTECTED_LIST = ['project', 'is_optional', 'name', 'offer_description', 'offer_date',
                      'valid_until_date', 'terms_collection', 'used_customer_reference',
                      'is_tax_included']
    STATUS_FIELD_NAME = 'offer_status'


class CampaignOffer(OfferOrCampaignOffer):
    """ Campaign Offers are concentrating Project Offers together."""

    title = models.CharField(max_length=150, verbose_name=_('title'))
    description = models.TextField(_('description'), blank=True)
    tax = models.DecimalField(
        _("Tax Percentage"),
        default=0.00,
        max_digits=4,
        decimal_places=2
    )
    campaign_status = ChoicesCharField(
        max_length=15,
        choices=OfferStatus,
        default=OfferStatus.CREATED
    )
    project_offers = models.ManyToManyField(
        Offer,
        verbose_name=_('project offers'),
        related_name='campaign_offers',
        blank=True
    )
    campaign = models.ForeignKey(
        Campaign,
        verbose_name=_('campaign project'),
        related_name='offers'
    )
    public_id = models.CharField(
        _('Campaign Offer Public ID'),
        max_length=14,
        unique=True,
        editable=False
    )
    number = models.PositiveSmallIntegerField(_("Campaign Offer number for Campaign"), default=1)

    class Meta:
        verbose_name = _('campaign offer')
        verbose_name_plural = _('campaigns offer')

    def __str__(self):
        return f"{self.title}"

    PROTECTED_STATES = [OfferStatus.SENT, OfferStatus.ACCEPTED, OfferStatus.DECLINED]
    PROTECTED_LIST = ['campaign', 'description', 'title',
                      'tax', 'offer_date', 'public_id',
                      'valid_until_date', 'terms_collection',
                      'used_customer_reference', 'offer_date', 'is_tax_included']
    STATUS_FIELD_NAME = 'campaign_status'

    @property
    def pdf(self):
        latest_pdf_url = f"{settings.BACKEND_URL}{reverse('latest-campaign-offer-pdf', args=[self.campaign.id, self.id])}"
        all_pdf_versions = [
            {
                "pdf_url": f"{settings.BACKEND_URL}"
                           f"{reverse('campaign-offer-pdf', args=[campaign_offer_pdf.campaign_offer.campaign.id, campaign_offer_pdf.campaign_offer.id, campaign_offer_pdf.id])}",
                "created_at": campaign_offer_pdf.created_at.strftime("%Y-%m-%d %H:%M:%S"),
                "pdf_version": campaign_offer_pdf.version
            } for campaign_offer_pdf in self.pdf_documents.all()
        ]
        return {
            "latest_pdf_version": latest_pdf_url,
            "all_pdf_versions": all_pdf_versions
        }

    @cached_property
    def currency(self):
        return self.campaign.billing_currency

    @cached_property
    def key_account_manager(self):
        """Return Campaign.Company.key_account_manager User or None"""
        if self.campaign.company.key_account_manager:
            return self.campaign.company.key_account_manager
        return None

    @cached_property
    def owner(self):
        """Return The Owner of this object => Offer_Manager"""
        return self.offer_manager

    def get_total_campaign_offer_without_tax(self, currency=None):
        """ The sum of all `offer.get_total_offer_without_tax` in the Campaign Offer
        """
        campaign_offer_value = sum(
            [offer.total_offer_without_tax.amount for offer in self.project_offers.all()]
        )
        if currency != self.currency and currency is not None:
            return convert_money_value(campaign_offer_value, self.currency, currency)

        return Money(campaign_offer_value, self.currency)

    def get_used_customer_reference(self):
        """ Retrieve used_customer_reference from Campaign Offer or customer_reference from Campaign
        """
        if self.used_customer_reference:
            return self.used_customer_reference
        return self.campaign.customer_reference

    @property
    def total_campaign_offer_without_tax(self):
        return self.get_total_campaign_offer_without_tax(self.currency)

    def get_total_tax(self):
        total_tax = self.total_campaign_offer_without_tax.amount * self.tax / 100
        return total_tax

    def get_total_campaign_offer_with_tax(self, currency=None):
        """ The sum of all `offer.get_total_offer_with_tax` in the Campaign Offer
        """
        total_tax = self.get_total_tax()
        total_campaign_offer_with_tax = self.total_campaign_offer_without_tax.amount + total_tax

        if currency != self.currency and currency is not None:
            return convert_money_value(total_campaign_offer_with_tax, self.currency, currency)
        return Money(total_campaign_offer_with_tax, self.currency)

    @property
    def total_campaign_offer_with_tax(self):
        return self.get_total_campaign_offer_with_tax(self.currency)

    def get_total_campaign_offer_send_to_customer(self, currency=None):
        """ The total of the Campaign Offer, how it was send to the customer"""

        if self.is_tax_included:
            money_value = self.total_campaign_offer_with_tax
        else:
            money_value = self.total_campaign_offer_without_tax

        if currency != self.currency and currency is not None:
            return convert_money_object(money_value, self.currency, currency)

        return money_value

    @property
    def total_offer_send_to_customer(self):
        return self.get_total_campaign_offer_send_to_customer(self.currency)

    def get_total_optional_costs(self, currency=None):
        """ Total optional costs in the Campaign Offer"""

        money_value = sum(
            [offer.total_optional_costs for offer in self.project_offers.all()]
        )

        if currency != self.currency and currency is not None:
            return convert_money_object(money_value, self.currency, currency)
        return money_value

    @property
    def total_optional_costs(self):
        return self.get_total_optional_costs(self.currency)

    def get_total_external_costs(self, currency=None):
        """ Total external costs in the Campaign Offer"""

        value = sum(
            [offer.total_external_costs.amount for offer in self.project_offers.all()]
        )

        if currency != self.currency and currency is not None:
            return convert_money_value(value, self.currency, currency)
        return Money(value, self.currency)

    @property
    def total_external_costs(self):
        return self.get_total_external_costs(self.currency)

    def get_total_discounts(self, currency=None):
        """ The total of the offer including discounts but without tax """

        if not currency:
            currency = self.currency

        value = sum(
            [offer.total_discounts.amount for offer in self.project_offers.all()]
        )

        if currency != self.currency and currency is not None:
            return convert_money_value(value, self.currency, currency)
        return Money(value, self.currency)

    @property
    def total_discounts(self):
        return self.get_total_discounts(self.currency)

    def change_status_to_sent(self):
        """CAMPAIGN OFFER"""
        if not self.used_customer_reference:
            self.used_customer_reference = self.campaign.customer_reference
        self.campaign_status = OfferStatus.SENT
        self.save()
        for offer in self.project_offers.all():
            if offer.offer_status != OfferStatus.SENT:
                offer.change_status_to_sent()

    def change_status_to_accepted(self):
        """CAMPAIGN OFFER
        When a CampaignOffer is ACCEPTED all Offers, are accepted as well
        and all its Activity/ArticleOffers.
        """
        for offer in self.project_offers.all():
            offer.change_status_to_accepted()
        self.campaign_status = OfferStatus.ACCEPTED
        self.save(update_fields=["campaign_status"])

    def change_status_to_declined(self):
        """CAMPAIGN OFFER
        When a CampaignOffer is DECLINED all Offers, are accepted as well and
         all its Activity/Article Offers.
        """
        for offer in self.project_offers.all():
            offer.change_status_to_declined()
        self.campaign_status = OfferStatus.DECLINED
        self.save(update_fields=["campaign_status"])

    def change_status_to_replaced(self):
        """CAMPAIGN OFFER"""
        # All Offers that are not DECLINED or ACCEPTED
        # will be Cloned and added to the New Cloned CampaignOffer together with already
        #  ACCEPTED Activities and Articles
        self.campaign_status = OfferStatus.REPLACED
        self.save()
        modified_kwargs = dict([
            ("public_id", ""),
            ('campaign_status', OfferStatus.CREATED),
            ('replaced_item', self),
            ('project_offers', list()),
            ('used_customer_reference', ""),
            ('created_at', now())
        ])
        clone = self.clone(**modified_kwargs)
        for offer in self.project_offers.filter(offer_status__in=[OfferStatus.SENT, OfferStatus.ACCEPTED]):
            offer.change_status_to_replaced()
        # Check and clear (just in case) to be sure the Campaign Clone doesn't have any offers
        if clone.project_offers.count():
            clone.project_offers.clear()
        # Add to New CampaignOffer the clones of the REPLACED Offers
        clone.project_offers.add(*[offer.replaced_by for offer in
                                   self.project_offers.filter(offer_status=OfferStatus.REPLACED)])
        # Add to New CampaignOffer the ACCEPTED Offers
        clone.project_offers.add(*[offer for offer in
                                   self.project_offers.filter(offer_status=OfferStatus.ACCEPTED)])

    def review_the_campaign_offer(self):
        all_project_offers_status_list = self.project_offers.values_list('offer_status', flat=True)
        if all_project_offers_status_list and all_statuses_are_the_same(all_project_offers_status_list):
            if self.campaign_status != all_project_offers_status_list[0]:
                self.campaign_status = all_project_offers_status_list[0]
                self.save(update_fields=["campaign_status"])
        else:
            # reference to Issue #258
            # https://github.com/jensneuhaus/metronom-pro-backend/issues/258#issuecomment-381618611
            case_1 = (OfferStatus.DECLINED in all_project_offers_status_list and
                      OfferStatus.ACCEPTED in all_project_offers_status_list)
            case_2 = (OfferStatus.DECLINED in all_project_offers_status_list and
                      OfferStatus.PARTLY_ACCEPTED in all_project_offers_status_list)
            case_3 = (OfferStatus.ACCEPTED in all_project_offers_status_list and
                      OfferStatus.PARTLY_ACCEPTED in all_project_offers_status_list)
            case_4 = (OfferStatus.ACCEPTED not in all_project_offers_status_list and
                      OfferStatus.DECLINED not in all_project_offers_status_list and
                      OfferStatus.PARTLY_ACCEPTED in all_project_offers_status_list)
            if case_1 or case_2 or case_3 or case_4 and OfferStatus.REPLACED not in all_project_offers_status_list:
                self.campaign_status = OfferStatus.PARTLY_ACCEPTED
                self.save()

    def save(self, *args, **kwargs):
        if self.campaign and (not self.campaign.offers.filter(
            id=self.id).exists() and self.campaign.offers.exists()
                and self.number == 1):
            # we need to increase number according to the offers
            self.number = self.campaign.offers.count() + 1
        if not self.public_id:
            # we need to increase number according to the offers
            number = 0
            if self.campaign:
                number = self.campaign.offers.count() + 1
            public_id = f'O-{self.campaign.public_id.split("-")[1]}-{self.campaign.public_id.split("-")[2]}-{number}'
            self.public_id = public_id
        # self.clean_fields()
        # To validate Choice Flow in one single step: when Calling `save()`
        self.full_clean()
        super(CampaignOffer, self).save(*args, **kwargs)


class ActivityOrArticleOffer(UserReadOnlyAccountingMetronomBaseModel,
                             ReplaceMixin, ProtectedFieldsMixin):
    """Abstract Model to offer the common fields necessary for ActivityOffer and ArticleOffer"""

    offer_status = ChoicesCharField(
        max_length=10,
        choices=ActivityArticleOfferedStatus,
        default=ActivityArticleOfferedStatus.CREATED
    )
    position = models.PositiveSmallIntegerField(_('position'), default=1)
    is_optional = models.BooleanField(_('This item is optional'), default=False)
    has_subtotal = models.BooleanField(_('Has subtotal'), default=False)
    discount = MoneyField(blank=True, null=True, decimal_places=2, max_digits=10, default=0)
    used_name = models.CharField(_('display name'), max_length=128, null=True, blank=True)
    used_description = models.TextField(_('used description'), null=True, blank=True)

    class Meta:
        abstract = True

    PROTECTED_STATES = [ActivityArticleOfferedStatus.SENT,
                        ActivityArticleOfferedStatus.ACCEPTED,
                        ActivityArticleOfferedStatus.REPLACED]
    STATUS_FIELD_NAME = 'offer_status'

    def __str__(self):
        return f"{self.offer}: {self.name}"

    def get_final_state(self):
        if self.get_final_state_information():
            return True
        return False

    @property
    def offered_sum_with_discount(self):
        return Money(self.offered_sum.amount - self.discount.amount, self.offer.currency)

    def validate_discount(self):
        if self.discount.amount < 0:
            raise ValidationError({"negative_discount": _("Negative "
                                                          "Discounts are not valid values.")})
        if self.discount.amount > self.offered_sum.amount:
            raise ValidationError(
                {"invalid_discount": _(
                    f"Discount {self.discount.amount} can't be bigger"
                    f" than Total Budget of: {self.offered_sum.amount}.")}
            )
        return True

    def clean(self):
        self.validate_discount()
        super().clean()

    def save(self, *args, **kwargs):
        # self.full_clean() => Necessary to be called Outside Django (for API requests)
        # self.full_clean()
        """ Some validation to prevent any `ActivityOffered` instances to be saved if
                `project_activity` is part of another instance of `ActivityOffered` which:
                 `ActivityOffered.offer.offer_status=ACCEPTED_OFFER`
                 """
        # When sending Save from Offer with Pre_Save Signal
        # `self.offer.offer_status` check will fail if we don't get the Offer from DB
        # db_offer = Offer.objects.get(pk=self.offer.pk)
        # if not self.is_editable:
        #     raise ValidationError(
        #         {"offer_status":
        #          _(f"'{self.name}' is not editable while it's"
        #            f" 'offer_status' is '{self.offer_status}'.")}
        #     )
        # Prevent creating a Discount bigger than Activity_offered.offered_sum
        if 0 < self.discount.amount > self.offered_sum.amount:
            raise ValidationError(
                {"discount": _(f"{self.name} Discount can't be "
                               f"smaller than 0 and not bigger that offered_sum.")},
                code='invalid_activity_discount',
            )
        super(ActivityOrArticleOffer, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.offer.offer_status in [OfferStatus.SENT, OfferStatus.ACCEPTED]:
            raise ValidationError(
                {"__all__":
                 _(f"'{str(self.id)}' can't be deleted while the Offer:"
                   f" '{str(self.offer.id)}' is '{self.offer.offer_status}'.")},
                code='deleting_protected_activity_offered',
            )
        super(ActivityOrArticleOffer, self).delete(*args, **kwargs)


class ActivityOffered(ActivityOrArticleOffer):
    offer = models.ForeignKey(Offer, related_name='activities', on_delete=models.CASCADE)
    show_steps = models.BooleanField(_('show activity steps'), default=False)
    project_activity = models.ForeignKey(ProjectActivity, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Activity Offered")
        verbose_name_plural = _("Activities Offered")

    @property
    def name(self):
        if self.used_name:
            return self.used_name
        return self.project_activity.name

    @cached_property
    def is_external_cost(self):
        return False

    @property
    def description(self):
        if self.used_description:
            return self.used_description
        return self.project_activity.description

    @property
    def offered_sum(self):
        return self.project_activity.total_planned_budget

    @property
    def sent_in_project_offers(self):
        """Return:
        None or a list of Project offers with UUID+Name,
        where the Project activity/article was sent in
        """
        return self.project_activity.sent_in_project_offers

    def get_final_state_information(self):
        return self.project_activity.get_final_state_information()

    @cached_property
    def version(self):
        return self.project_activity.version

    def change_status_to_sent(self):
        valid_states_to_send = [ActivityArticleOfferedStatus.CREATED,
                                ActivityArticleOfferedStatus.IMPROVABLE,
                                ActivityArticleOfferedStatus.IMPROVED]
        if self.offer_status in valid_states_to_send:
            self.used_name = self.project_activity.name
            self.used_description = self.project_activity.description
            self.offer_status = ActivityArticleOfferedStatus.SENT
            self.save()
            self.project_activity.change_status_to_sent()

    def change_status_to_accepted(self):
        """When an Activity is accepted in Offer A, it will be shown as
        accepted in Offer B as well. You could add it to a  new Offer C,
        but it still would be accepted.
        Call first `.change_status_to_accepted()` from ProjectActivity
        """
        self.project_activity.change_status_to_accepted()
        self.offer_status = ActivityArticleOfferedStatus.ACCEPTED
        self.save()

    def change_status_to_declined(self):
        """ When an Activity was declined in Offer A and there is
        another Offer B already, the Project Activity will keep the status SENT
        It will only move to `declined` if it was declined in all Offers (!!!).
        As soon as it is `declined`, it can not be used for an Offer C.
        """
        if self.project_activity.can_be_replaced_or_declined(offer_item=self):
            # call first .change_status_to_declined() from ProjectActivity
            self.project_activity.change_status_to_declined()
        # Only this ActivityOffered will be DECLINED if `can_be_replaced_or_declined` != TRUE
        self.offer_status = ActivityArticleOfferedStatus.DECLINED
        self.save()

    def change_status_to_replaced(self, offer=None):
        """You are not allowed to REPLACE an Activity in Offer_A if
        it is still SENT in an Offer_B (and not yet `accepted` or `declined`).
        if the Offer Activity of Offer_A is still in another Offer_B (via its Project Activity)
        """
        if offer:
            new_offer = offer
        else:
            new_offer = self.offer
        if self.project_activity.offer_status == ActivityArticleOfferedStatus.ACCEPTED:
            modified_kwargs = dict([
                ("project_activity", self.project_activity),
                ('offer_status', ActivityArticleOfferedStatus.ACCEPTED),
                ('replaced_item', self),
                ('offer', new_offer),
                ('created_at', now())
            ])
            clone = self.clone(**modified_kwargs)
        elif self.project_activity.offer_status == ActivityArticleOfferedStatus.SENT:
            if self.project_activity.can_be_replaced_or_declined(offer_item=self):
                self.offer_status = ActivityArticleOfferedStatus.REPLACED
                self.save()
                self.project_activity.change_status_to_replaced()
                modified_kwargs = dict([
                    ("project_activity", self.project_activity.replaced_by),
                    ('offer_status', ActivityArticleOfferedStatus.IMPROVABLE),
                    ('replaced_item', self),
                    ('offer', new_offer),
                    ('created_at', now())
                ])
                clone = self.clone(**modified_kwargs)

    def change_status_to_improved(self, from_project_activity=False):
        self.offer_status = ActivityArticleOfferedStatus.IMPROVED
        self.save()
        if not from_project_activity:
            self.project_activity.change_status_to_improved(True)

    PROTECTED_LIST = ['is_optional', 'has_subtotal', 'discount', 'used_name',
                      'used_description', 'show_steps', 'project_activity']


class ArticleOffered(ActivityOrArticleOffer):
    offer = models.ForeignKey(Offer, related_name='articles', on_delete=models.CASCADE)
    is_external_cost = models.BooleanField(_('is external cost'), default=False)
    project_article = models.ForeignKey(ProjectArticle, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Article Offered")
        verbose_name_plural = _("Articles Offered")

    @property
    def name(self):
        if self.used_name:
            return self.used_name
        return self.project_article.name

    @property
    def description(self):
        if self.used_description:
            return self.used_description
        return self.project_article.description

    @property
    def offered_sum(self):
        return self.project_article.total_planned_budget

    @property
    def sent_in_project_offers(self):
        """Return:
        None or a list of Project offers with UUID+Name,
        where the Project activity/article was sent in
        """
        return self.project_article.sent_in_project_offers

    def get_final_state_information(self):
        return self.project_article.get_final_state_information()

    @property
    def version(self):
        return self.project_article.version

    def change_status_to_sent(self):
        if self.offer_status != ActivityArticleOfferedStatus.ACCEPTED:
            self.used_name = self.project_article.name
            self.used_description = self.project_article.description
            self.offer_status = ActivityArticleOfferedStatus.SENT
            self.save()
        self.project_article.change_status_to_sent()

    def change_status_to_accepted(self):
        """When an Article is accepted in Offer_A, it will be shown as accepted in Offer_B as well.
        You could add it again to a  new Offer_C,
        but it still would be accepted.
        Call first `.change_status_to_accepted()` from ProjectArticle
        """
        self.project_article.change_status_to_accepted()
        # If ProjectArticle can be ACCEPTED then mark the ArticleOffered as ACCEPTED
        self.offer_status = ActivityArticleOfferedStatus.ACCEPTED
        self.save()

    def change_status_to_declined(self):
        """ When an Article was declined in Offer_A and there is
        another Offer_B already, the Project Article will keep the status SENT
        It will only move to `declined` if it was declined in all Offers (!!!).
        As soon as it is `declined`, it can not be used for an Offer_C.
        """
        if self.project_article.can_be_replaced_or_declined(offer_item=self):
            # call first .change_status_to_declined() from ProjectArticle
            self.project_article.change_status_to_declined()
        # Only this ArticleOffered will be DECLINED if `can_be_replaced_or_declined` != TRUE
        self.offer_status = ActivityArticleOfferedStatus.DECLINED
        self.save()

    def change_status_to_replaced(self, offer=None):
        """You are not allowed to REPLACE an Article in Offer_A if
        it is still SENT in an Offer_B (and not yet `accepted` or `declined`).
        if the Offer Article of Offer_A is still in another Offer_B (via its Project Article)
        """
        if offer:
            new_offer = offer
        else:
            new_offer = self.offer
        if self.project_article.offer_status == ActivityArticleOfferedStatus.ACCEPTED:
            modified_kwargs = dict([
                ("project_article", self.project_article),
                ('offer_status', ActivityArticleOfferedStatus.ACCEPTED),
                ('replaced_item', self),
                ('offer', new_offer),
                ('created_at', now())
            ])
            self.clone(**modified_kwargs)
        elif self.project_article.offer_status == ActivityArticleOfferedStatus.SENT:
            if self.project_article.can_be_replaced_or_declined(offer_item=self):
                self.project_article.change_status_to_replaced()
                self.offer_status = ActivityArticleOfferedStatus.REPLACED
                self.save()
                modified_kwargs = dict([
                    ("project_article", self.project_article.replaced_by),
                    ('offer_status', ActivityArticleOfferedStatus.IMPROVABLE),
                    ('replaced_item', self),
                    ('offer', new_offer),
                    ('created_at', now())
                ])
                self.clone(**modified_kwargs)

    def change_status_to_improved(self, from_project_article=False):
        self.offer_status = ActivityArticleOfferedStatus.IMPROVED
        self.save()
        if not from_project_article:
            self.project_article.change_status_to_improved(True)

    PROTECTED_LIST = ['is_optional', 'has_subtotal', 'discount', 'used_name',
                      'used_description', 'is_external_cost', 'project_article']


class ProjectOfferPDF(UserReadOnlyAccountingMetronomBaseModel, ReplaceMixin):
    project_offer = models.ForeignKey(
        Offer,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='pdf_documents'
    )
    context_data = models.TextField()
    version = models.PositiveSmallIntegerField(_("Version of the current PDF Project Offer"), default=1)

    class Meta:
        verbose_name = _("Project Offer PDF")
        verbose_name_plural = _("Project Offers PDF")
        ordering = ('-created_at',)

    def __str__(self):
        return f"Project Offer PDF {self.project_offer.name} ({self.project_offer.public_id})"

    def get_company_data(self):
        if self.project_offer:
            company = self.project_offer.project.company
            company_dictionary = {
                "company": company.name,
                "address": company.address1,
                "country_code": company.country_code,
                "postal_code": company.postal_code,
                "city": company.city,
            }
            return company_dictionary
        return {}

    @property
    def is_valid(self):
        if self.project_offer.updated_at < self.created_at:
            return True
        return False

    def create_new_version(self, project_offer_pdf=None):
        """Method to Clone the ProjectOfferPDF to create a New Version when the current one is not VALID
        :returns ProjectOfferPDF object instance
        """
        current_project_offer_pdf = self
        if project_offer_pdf:
            current_project_offer_pdf = project_offer_pdf
        string_format_data = self.get_context_data_as_string()
        modified_kwargs = dict([
            ('replaced_item', current_project_offer_pdf),
            ('context_data', string_format_data),
            ('version', self.version + 1),
            ('created_at', now())
        ])
        new_project_offer_pdf = self.clone(**modified_kwargs)
        return new_project_offer_pdf

    def get_context_data_as_string(self):
        context_data_as_dictionary = self.generate_context_data_from_project_offer()
        return str(context_data_as_dictionary)

    def get_context_data_as_dictionary(self, context_data_as_string=None):
        if context_data_as_string:
            return literal_eval(context_data_as_string)
        return literal_eval(self.context_data)

    def generate_context_data_from_project_offer(self):
        """Create on the spod the Project Offer Context Dictionary necessary for PDF Rendering"""
        project_offer_pdf_utils_instance = ProjectOfferPDFUtils(self.project_offer)
        context_data = project_offer_pdf_utils_instance.get_project_offer_complete_dictionary()
        return context_data

    @classmethod
    def create_project_offer_pdf(cls, project_offer=None):
        if project_offer:
            project_offer_pdf_object = ProjectOfferPDFUtils(project_offer)
            new_project_offer_pdf = cls(
                project_offer=project_offer,
                context_data=str(project_offer_pdf_object.get_project_offer_complete_dictionary())
            )
            new_project_offer_pdf.save()
            return new_project_offer_pdf
        return None

    @classmethod
    def get_the_latest_version(cls, project_offer=None):
        if project_offer:
            all_project_offer_pdf = project_offer.pdf_documents.all()
            if not all_project_offer_pdf:
                current_project_offer_pdf = cls.create_project_offer_pdf(project_offer)
            else:
                current_project_offer_pdf = all_project_offer_pdf.first()

            if current_project_offer_pdf.is_valid:
                return current_project_offer_pdf
            else:
                new_version = current_project_offer_pdf.create_new_version()
            return new_version
        return cls.objects.none()

    def append_company_data_to_context_dictionary(self, context_data_as_dictionary=None):
        if not context_data_as_dictionary:
            context_data_as_dictionary = self.get_context_data_as_dictionary()
        if self.get_company_data():
            for key, value in self.get_company_data().items():
                context_data_as_dictionary['customer'][key] = value
        return context_data_as_dictionary

    def attach_terms_collection_to_project_offer_pdf(self, post_data_for_pdf_service):
        if not post_data_for_pdf_service:
            post_data_for_pdf_service = self.get_context_data_as_dictionary()
        terms_collection_pdf = TermsCollectionPDF.get_the_latest_version(self.project_offer.terms_collection)
        post_data_for_pdf_service['terms'] = terms_collection_pdf.context_data_as_dictionary
        return post_data_for_pdf_service

    def get_post_data_for_pdf_service(self):
        context_data_as_dictionary = self.get_context_data_as_dictionary()
        post_data_for_pdf_service = self.append_company_data_to_context_dictionary(context_data_as_dictionary)
        return post_data_for_pdf_service

    def get_pdf_document(self, pdf_parameters):
        # /?lang=de-ch&doc_type=project&font=Proxima&filename=My%20Lovely%20PDF
        pdf_parameters_keys = ['lang', 'font', 'filename']
        get_parameters_url = "/?doc_type=project&"
        for key, value in pdf_parameters.items():
            if key in pdf_parameters_keys:
                get_parameters_url += key + '=' + value + '&'

        post_data_for_pdf_service = self.get_post_data_for_pdf_service()
        post_data_for_pdf_service = self.attach_terms_collection_to_project_offer_pdf(post_data_for_pdf_service)
        data = {"context_data": str(post_data_for_pdf_service)}
        offer_pdf_url = f"{settings.PDF_URL}{get_parameters_url}"
        post_pdf_service_response = requests.post(offer_pdf_url, data=data)
        return post_pdf_service_response


class CampaignOfferPDF(UserReadOnlyAccountingMetronomBaseModel, ReplaceMixin):
    campaign_offer = models.ForeignKey(
        CampaignOffer,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='pdf_documents'
    )
    context_data = models.TextField()
    version = models.PositiveSmallIntegerField(_("Version of the current PDF Project Offer"), default=1)

    class Meta:
        verbose_name = _("Campaign Offer PDF")
        verbose_name_plural = _("Campaign Offers PDF")
        ordering = ('-created_at',)

    def __str__(self):
        return f"Project Offer PDF {self.campaign_offer.title} ({self.campaign_offer.public_id})"

    def get_company_data(self):
        if self.campaign_offer:
            company = self.campaign_offer.campaign.company
            company_dictionary = {
                "company": company.name,
                "address": company.address1,
                "country_code": company.country_code,
                "postal_code": company.postal_code,
                "city": company.city,
            }
            return company_dictionary
        return {}

    @property
    def is_valid(self):
        if self.campaign_offer.updated_at < self.created_at:
            return True
        return False

    def create_new_version(self, campaign_offer_pdf=None):
        """Method to Clone the CampaignOfferPDF to create a New Version when the current one is not VALID
        :returns ProjectOfferPDF object instance
        """
        current_campaign_offer_pdf = self
        if campaign_offer_pdf:
            current_campaign_offer_pdf = campaign_offer_pdf
        string_format_data = self.get_context_data_as_string()
        modified_kwargs = dict([
            ('replaced_item', current_campaign_offer_pdf),
            ('context_data', string_format_data),
            ('version', self.version + 1),
            ('created_at', now())
        ])
        new_campaign_offer_pdf = self.clone(**modified_kwargs)
        return new_campaign_offer_pdf

    def get_context_data_as_string(self):
        context_data_as_dictionary = self.generate_context_data_from_campaign_offer()
        return str(context_data_as_dictionary)

    def get_context_data_as_dictionary(self, context_data_as_string=None):
        if context_data_as_string:
            return literal_eval(context_data_as_string)
        return literal_eval(self.context_data)

    def generate_context_data_from_campaign_offer(self):
        """Create on the spot the Campaign Offer Context Dictionary necessary for PDF Rendering"""
        campaign_offer_pdf_utils_instance = CampaignOfferPDFUtils(self.campaign_offer)
        context_data = campaign_offer_pdf_utils_instance.get_campaign_offer_complete_dictionary()
        return context_data

    @classmethod
    def create_project_offer_pdf(cls, campaign_offer=None):
        if campaign_offer:
            campaign_offer_pdf_object = CampaignOfferPDFUtils(campaign_offer)
            new_campaign_offer_pdf = cls(
                campaign_offer=campaign_offer,
                context_data=str(campaign_offer_pdf_object.get_campaign_offer_complete_dictionary())
            )
            new_campaign_offer_pdf.save()
            return new_campaign_offer_pdf
        return None

    @classmethod
    def get_the_latest_version(cls, campaign_offer=None):
        if campaign_offer:
            all_campaign_offer_pdf = campaign_offer.pdf_documents.all()
            if not all_campaign_offer_pdf:
                current_project_offer_pdf = cls.create_project_offer_pdf(campaign_offer)
            else:
                current_project_offer_pdf = all_campaign_offer_pdf.first()

            if current_project_offer_pdf.is_valid:
                return current_project_offer_pdf
            else:
                new_version = current_project_offer_pdf.create_new_version()
            return new_version
        return cls.objects.none()

    def append_company_data_to_context_dictionary(self, context_data_as_dictionary=None):
        if not context_data_as_dictionary:
            context_data_as_dictionary = self.get_context_data_as_dictionary()
        if self.get_company_data():
            for key, value in self.get_company_data().items():
                context_data_as_dictionary['customer'][key] = value
        return context_data_as_dictionary

    def get_post_data_for_pdf_service(self):
        context_data_as_dictionary = self.get_context_data_as_dictionary()
        post_data_for_pdf_service = self.append_company_data_to_context_dictionary(context_data_as_dictionary)
        return post_data_for_pdf_service

    def attach_terms_collection_to_campaign_offer_pdf(self, post_data_for_pdf_service=None):
        if not post_data_for_pdf_service:
            post_data_for_pdf_service = self.get_context_data_as_dictionary()
        terms_collection_pdf = TermsCollectionPDF.get_the_latest_version(self.campaign_offer.terms_collection)
        post_data_for_pdf_service['terms'] = terms_collection_pdf.context_data_as_dictionary
        return post_data_for_pdf_service

    def get_pdf_document(self, pdf_parameters):
        # /?lang=de-ch&doc_type=project&font=Proxima&filename=My%20Lovely%20PDF
        pdf_parameters_keys = ['lang', 'font', 'filename']
        get_parameters_url = "/?doc_type=campaign&"
        for key, value in pdf_parameters.items():
            if key in pdf_parameters_keys:
                get_parameters_url += key + '=' + value + '&'
        post_data_for_pdf_service = self.get_post_data_for_pdf_service()
        post_data_for_pdf_service = self.attach_terms_collection_to_campaign_offer_pdf(post_data_for_pdf_service)
        data = {"context_data": str(post_data_for_pdf_service)}
        offer_pdf_url = f"{settings.PDF_URL}{get_parameters_url}"
        post_pdf_service_response = requests.post(offer_pdf_url, data=data)
        return post_pdf_service_response


class TermsCollectionPDF(UserReadOnlyAccountingMetronomBaseModel, ReplaceMixin):
    """
    PDF Class to generate and handle the ContextData for Terms Collection.
    Important Methods/Proprieties:
        - is_valid =>               @property
        - get_the_latest_version => @classmethod
    This Class is recommended to be called through its @classmethod `get_the_latest_version(cls)` if when you want
    the latest version of the Context_data to render.
    """
    terms_collection = models.ForeignKey(
        TermsCollection,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='pdf_terms'
    )
    context_data = models.TextField()
    version = models.PositiveSmallIntegerField(_("Version of the current PDF Terms Collection"), default=1)

    class Meta:
        verbose_name = _("PDF Terms Collection")
        verbose_name_plural = _("PDF Terms Collections")
        ordering = ('-created_at',)

    def __str__(self):
        return f"PDF Terms Collection {self.terms_collection.name} v.{self.version}"

    @property
    def is_valid(self):
        """
        Detects if the Context Data to render the PDF is Valid or not.
        If the TermsCollection was changed since this TermsCollectionPDF was created
        this instance will not be valid anymore
        :return: True || False
        :rtype: Boolean
        """
        if self.terms_collection.updated_at and self.terms_collection.updated_at > self.created_at:
            return False
        for term_paragraph in self.terms_collection.terms.all():
            if term_paragraph.updated_at and term_paragraph.updated_at > self.created_at:
                return False
        return True

    def create_new_version(self, terms_collection_pdf=None):
        """Method to Clone the TermsCollectionPDF to create a New Version when the current one is not VALID
        :return: <ProjectOfferPDF: object instance>
        :rtype: object
        """
        current_terms_collection_pdf = self
        if terms_collection_pdf:
            current_terms_collection_pdf = terms_collection_pdf
        string_format_data = self.get_context_data_as_string()
        modified_kwargs = dict([
            ('replaced_item', current_terms_collection_pdf),
            ('context_data', string_format_data),
            ('version', self.version + 1),
            ('created_at', now())
        ])
        new_terms_collection_pdf = self.clone(**modified_kwargs)
        return new_terms_collection_pdf

    def get_context_data_as_string(self):
        """
        Convert python dictionary into a string format
        :return: "{"name": "some name"}"
        :rtype: str
        """
        context_data_as_dictionary = self.generate_context_data_from_terms_collection()
        return str(context_data_as_dictionary)

    def get_context_data_as_dictionary(self, context_data_as_string=None):
        """Convert a received dictionary as a String OR `self.context_data`

        :param context_data_as_string: "{'is_name': True}"
        :type context_data_as_string:
        :return: {'is_name': True}
        :rtype: dict
        """
        if context_data_as_string:
            return literal_eval(context_data_as_string)
        return literal_eval(self.context_data)

    @cached_property
    def context_data_as_dictionary(self):
        return self.get_context_data_as_dictionary()

    def generate_context_data_from_terms_collection(self):
        """Create on the spot the TermsCollection Context Dictionary necessary for PDF Rendering"""
        terms_collection_pdf_utils_instance = TermsCollectionPDFUtils(self.terms_collection)
        context_data = terms_collection_pdf_utils_instance.get_terms_collection_dictionary()
        return context_data

    @classmethod
    def create_terms_collection_pdf(cls, terms_collection=None):
        """ If self (<TermsCollectionPDF : instance>) is not VALID anymore we create a new version of it.

        :param terms_collection: Old Version that is not Valid anymore <TermsCollection : instance>
        :type terms_collection: object
        :return: New version of <TermsCollectionPDF : instance>
        :rtype: object
        """
        if terms_collection:
            terms_collection_pdf_object = TermsCollectionPDFUtils(terms_collection)
            new_terms_collection_pdf = cls(
                terms_collection=terms_collection,
                context_data=str(terms_collection_pdf_object.get_terms_collection_dictionary())
            )
            new_terms_collection_pdf.save()
            return new_terms_collection_pdf
        return None

    @classmethod
    def get_the_latest_version(cls, terms_collection=None):
        """Check and return from DB the latest version of the terms_collection_pdf

        :param terms_collection: <TermsCollection : instance>
        :type terms_collection: object
        :return: <TermsCollectionPDF : instance>
        :rtype: object
        """
        if terms_collection:
            all_terms_collection_pdf = terms_collection.pdf_terms.all()
            if not all_terms_collection_pdf:
                current_terms_collection_pdf = cls.create_terms_collection_pdf(terms_collection)
            else:
                current_terms_collection_pdf = all_terms_collection_pdf.first()

            if current_terms_collection_pdf.is_valid:
                return current_terms_collection_pdf
            else:
                new_version = current_terms_collection_pdf.create_new_version()
            return new_version
        return cls.objects.none()


post_save.connect(sender=Offer, receiver=Offer.recalculate_position,
                  dispatch_uid="offer_recalculate_position_signal")
