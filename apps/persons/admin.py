from django.contrib import admin

from apps.employments.models import Employment

from .models import Person, Promotion


@admin.register(Promotion)
class PromotionAdmin(admin.ModelAdmin):
    """ Admin should be able to add new Promotions """
    list_display = (
        'id',
        'name'
    )


class EmploymentInline(admin.TabularInline):
    model = Employment
    extra = 0


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    """ Persons in the CRM """
    list_display = (
        'id',
        'public_id',
        'status',
        'first_name',
        'last_name',
        'private_address',
        'gender',
        'language',
        'manner_of_address',
        'birth_date',
    )

    list_filter = ('status', 'gender', 'language', )

    inlines = (EmploymentInline, )
