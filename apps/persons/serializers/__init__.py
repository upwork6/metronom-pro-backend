
from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from apps.employments.models import Employment
from apps.addresses.models import Address
from apps.addresses.serializers import AddressSerializer
from apps.emails.models import Email
from apps.metronom_commons.serializers import SoftDeletionSerializer
from apps.metronom_commons.utils import pop_create
from apps.persons.models import Person, Promotion
from apps.profile_images.serializers import ProfileImageSerializer
from apps.profile_images.models import ProfileImage
from django.db import transaction

User = get_user_model()


class PersonBaseSerializer(SoftDeletionSerializer, serializers.ModelSerializer):

    email = serializers.EmailField(source='email.email', required=False)
    private_address = AddressSerializer(required=False)
    contact_persons_uuids = PrimaryKeyRelatedField(
        many=True,
        queryset=User.objects.all(),
        source="contact_persons",
        required=False,
    )

    latest_employment_uuid = PrimaryKeyRelatedField(
        many=False,
        queryset=Employment.objects.all(),
        source="latest_employment",
        required=False,
    )

    user_uuid = PrimaryKeyRelatedField(
        many=False,
        queryset=User.objects.all(),
        source="user",
        required=False,
    )
    image = ProfileImageSerializer(required=False, allow_null=True)

    class Meta:
        model = Person
        fields = (
            'id',
            'birth_date',
            'first_name',
            'last_name',
            'image',
            'manner_of_address',
            'contact_persons_uuids',
            'email',
            'gender',
            'landline_phone',
            'language',
            'mobile_phone',
            'private_address',
            'public_id',
            'status',
            'latest_employment_uuid',
            'user_uuid'
        )
        read_only_fields = (
            'id',
            'public_id',
            'latest_employment_uuid'
        )

    def create(self, validated_data):
        """Use the respective IDs for writing Many2Many relationships and write nested data."""
        contact_persons = validated_data.pop('contact_persons', [])

        email = pop_create(validated_data, 'email', Email)
        private_address = pop_create(validated_data, 'private_address', Address)

        person = Person.objects.create(email=email, private_address=private_address, **validated_data)

        person.contact_persons = contact_persons

        person.save()
        return person

    def update(self, instance, validated_data):
        """Use the respective IDs for updating Many2Many relationships and write nested data."""

        with transaction.atomic():
            # possible cases:
            # 1. image field not present:
            #       image object not updated;
            # 2. image field present and null:
            #       image object deleted (if already present);
            # 3. image field present, not null, with id:
            #       new image created, past is deleted (if present);
            # 4. image field present, not null, without id:
            #       image object updated, created if not present.
            update_image = 'image' in validated_data.keys()
            image_data = validated_data.pop('image', {})

            if not update_image:
                # case 1.
                pass
            elif image_data is None:
                # case 2.
                if instance.image is not None:
                    image_to_del = instance.image
                    instance.image = None
                    instance.save()
                    image_to_del.delete()
            elif 'id' in image_data.keys():
                # case 3.
                image_to_del = instance.image
                instance.image = ProfileImage.objects.create(**image_data)
                instance.save()
                if image_to_del is not None:
                    image_to_del.delete()
            else:
                # case 4.
                if instance.image is None:
                    instance.image = ProfileImage.objects.create(**image_data)
                    instance.save()
                else:
                    for attr, value in image_data.items():
                        setattr(instance.image, attr, value)
                    instance.image.save()

            # updating the rest of the profile attributes
            for attr, value in validated_data.items():

                if attr == "private_address":

                    private_address_instance = getattr(instance, "private_address")

                    if private_address_instance is None:
                        value = Address.objects.create(**value)
                    else:
                        for address_attr, address_value in value.items():
                            setattr(private_address_instance, address_attr, address_value)
                        value = None

                        private_address_instance.save()

                if attr == "email":

                    email_instance = getattr(instance, "email")

                    if email_instance is None:
                        value = pop_create(value, 'email', Email)
                    else:
                        for email_attr, email_value in value.items():
                            setattr(email_instance, email_attr, email_value)
                        value = None

                        email_instance.save()

                if value:
                    setattr(instance, attr, value)

            instance.save()

            return instance


class PromotionSerializer(serializers.ModelSerializer):
    """Serialize data from the Promotion."""
    uuid = serializers.UUIDField(source="id", required=False)

    class Meta:
        model = Promotion
        fields = (
            'uuid',
            'name',
            'description'
        )
