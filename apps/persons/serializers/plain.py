
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers

from apps.cache_controller.serializer_cache_controllers import \
    ModelSerializerCacheControllerRegister
from apps.employments.serializers import (EmploymentSerializerForPerson,
                                          EmploymentSerializerForPersonInContactList)
from apps.persons.models import Person
from apps.persons.serializers import PersonBaseSerializer
from apps.metronom_commons.serializers import SoftDeletionSerializer


class PersonSerializer(PersonBaseSerializer):
    employments = EmploymentSerializerForPerson(many=True, read_only=True)

    class Meta(PersonBaseSerializer.Meta):
        fields = PersonBaseSerializer.Meta.fields + (
            'employments',
        )


class PersonContactListSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    """Serialize data of a person."""

    full_name = serializers.CharField(read_only=True)
    phone = PhoneNumberField(source="main_phone", read_only=True)
    email = serializers.EmailField(source='main_email', read_only=True)
    latest_employment = EmploymentSerializerForPersonInContactList(read_only=True)

    address1 = serializers.CharField(read_only=True)
    postal_code = serializers.CharField(read_only=True)
    city = serializers.CharField(read_only=True)
    country_code = serializers.CharField(read_only=True)

    class Meta:
        model = Person
        fields = (
            'address1',
            'city',
            'latest_employment',
            'country_code',
            'email',
            'full_name',
            'id',
            'phone',
            'postal_code',
            'public_id',
            'status',
        )


class UserContactListSerializer(PersonContactListSerializer):
    """Serialize data of a person."""

    has_image = serializers.SerializerMethodField(read_only=True)

    def get_has_image(self, obj):
        return obj.user.has_image

    class Meta:
        model = Person
        fields = (
            'address1',
            'city',
            'latest_employment',
            'country_code',
            'email',
            'full_name',
            'id',
            'phone',
            'postal_code',
            'public_id',
            'status',
            'has_image'
        )


# TODO: This one was really bad with caching structure of changes - it did not use CACHE_TIMEOUT etc.
ModelSerializerCacheControllerRegister.register_serializer(PersonContactListSerializer)
