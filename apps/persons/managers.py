
from safedelete.managers import SafeDeleteManager
from safedelete.queryset import SafeDeleteQueryset


class PersonQueryset(SafeDeleteQueryset):
    def updated_later_than(self, dt):
        return self.filter(updated_at__gte=dt)


class PersonManager(SafeDeleteManager):
    def get_queryset(self):
        queryset = PersonQueryset(self.model, using=self._db)
        queryset._safedelete_visibility = self._safedelete_visibility
        queryset._safedelete_visibility_field = self._safedelete_visibility_field
        return queryset
