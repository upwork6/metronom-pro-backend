
from django.contrib.auth import get_user_model
from django.core.cache import caches
from django.db import models
from django.db.models import Q
from django.db.models.signals import m2m_changed, post_delete, post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

from apps.addresses.models import Address
from apps.cache_controller.signals import model_instances_changed
from apps.contacts.models import Contact
from apps.emails.models import Email
from apps.metronom_commons.fields import (LandlinePhoneNumberField,
                                          MobilePhoneNumberField)
from apps.metronom_commons.models import (UserAccessBackofficeMetronomBaseModel,
                                          UserAccessMetronomBaseModel)
from apps.persons.managers import PersonManager
from apps.urls.models import URL
from config import settings
from apps.profile_images.models import ProfileImage

cache = caches['autocompletes']


class Promotion(UserAccessBackofficeMetronomBaseModel):
    name = models.CharField(_('Promotion name'), max_length=256, unique=True)
    description = models.CharField(_('Description'), max_length=1000, null=True, blank=True)

    class Meta:
        verbose_name = _('Promotion')
        verbose_name_plural = _('Promotions')

    def __init__(self, *args, **kwargs):
        super(Promotion, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.name


class Person(Contact):
    """Stores a single person entry, related to :model:`contacts.Group` and inheritance form :model:`contacts.Contact`.

    gender: Representation of human sexes - https://en.m.wikipedia.org/wiki/ISO/IEC_5218
    """

    GERMAN = 'de'
    FRENCH = 'fr'
    ITALIAN = 'it'
    ENGLISH = 'en'

    LANGUAGE_CHOICES = (
        (GERMAN, _('German')),
        (FRENCH, _('French')),
        (ITALIAN, _('Italian')),
        (ENGLISH, _('English')),
    )

    GENDER_NOT_KNOWN = 'unknown'
    GENDER_MALE = 'male'
    GENDER_FEMALE = 'female'

    GENDER_CHOICES = (
        (GENDER_MALE, _('male')),
        (GENDER_FEMALE, _('female')),
    )

    first_name = models.CharField(_('First name'), max_length=100, blank=True, default='')
    last_name = models.CharField(_('Last name'), max_length=100, default='')

    gender = models.CharField(_('Gender'), max_length=30, choices=GENDER_CHOICES, default=GENDER_NOT_KNOWN)

    language = models.CharField(_('Language'),
                                max_length=2,
                                choices=LANGUAGE_CHOICES,
                                default=GERMAN,
                                blank=True,
                                help_text=_('What language is this person speaking?')
                                )

    manner_of_address = models.CharField(
        _('Manner of address'),
        max_length=30,
        blank=True,
        default='',
        help_text=_('e.g. Mr., Ms., Dr.')
    )

    birth_date = models.DateField(_('Date of birth'), blank=True, null=True)

    private_address = models.ForeignKey(Address, related_name='person',
                                        verbose_name=_('Address'), null=True, blank=True)

    landline_phone = LandlinePhoneNumberField(null=True, blank=True)
    mobile_phone = MobilePhoneNumberField(null=True, blank=True)
    urls = models.ManyToManyField(URL, related_name='person', blank=True)

    email = models.OneToOneField(Email, related_name='person', verbose_name=_('Email'), null=True, blank=True)

    contact_persons = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                             related_name='persons',
                                             verbose_name=_('Contact persons'),
                                             blank=True,
                                             help_text=_('Contact persons in the agency for this person')
                                             )

    promotions = models.ManyToManyField(Promotion,
                                        related_name='promotions',
                                        verbose_name=_('Promotions'),
                                        blank=True,
                                        help_text=_('Promotions for this persons')
                                        )
    latest_employment = models.ForeignKey('employments.Employment', blank=True, null=True,
                                          related_name="person_latest_employment")
    image = models.OneToOneField(
        ProfileImage,
        help_text=_("Image and profile's avatar information."),
        null=True,
        on_delete=models.CASCADE,
    )

    objects = PersonManager()

    class Meta:
        verbose_name = _('Person')
        verbose_name_plural = _('Persons')
        permissions = (
            ('view_person', 'View Person'),
        )

    def __init__(self, *args, **kwargs):
        super(Person, self).__init__(*args, **kwargs)
        # The attribute `latest_employment` are populated in PersonManager (methods `__iter__`
        # and `populate_latest_employment`)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f'/persons/{self.public_id}/'

    @property
    def name(self):
        return f'{self.first_name} {self.last_name}'

    @cached_property
    def full_name(self):
        return self.name

    @cached_property
    def main_phone(self):
        # TODO: Should we get the private phone or the phone of the company where the person works?
        try:
            employment = self.latest_employment
            main_phone = employment.landline_phone or employment.mobile_phone
            return main_phone
        except AttributeError:
            return None

    @cached_property
    def address1(self):
        try:
            return self.private_address.address1
        except AttributeError:
            return None

    @cached_property
    def postal_code(self):
        try:
            return self.private_address.postal_code
        except AttributeError:
            return None

    @cached_property
    def city(self):
        try:
            return self.private_address.city
        except AttributeError:
            return None

    @cached_property
    def country_code(self):
        try:
            return self.private_address.country_code
        except AttributeError:
            return None

    @cached_property
    def main_email(self):
        # TODO: Should we get the private email or the phone of the company where the person works?
        try:
            main_email = str(self.latest_employment.email)
            return main_email
        except AttributeError:
            return None

    @cached_property
    def company(self):
        try:
            return self.latest_employment.company
        except AttributeError:
            return None


@receiver(post_save, sender=Person)
@receiver(post_delete, sender=Person)
@receiver(m2m_changed, sender=Person)
def clear_person_autocomplete_view_output_cache(sender, instance=None, **kwargs):
    from .views import PersonAutocompleteView
    PersonAutocompleteView.clear_cache()


@receiver(post_save)
@receiver(post_delete)
@receiver(m2m_changed)
def actualize_person_updated_date(sender, instance, **kwargs):
    # TODO Dmitry Koldunov: Lets think about reliability of this solution. It is potential place of mistake.
    # In future can change dependencies between models but a developer will forget to change this code.
    # Maybe better invalidate cache when ANY model instance has been changed.
    from apps.employments.models import Employment
    from apps.companies.models import Company

    User = get_user_model()
    now = timezone.now()

    persons = Person.objects.none()
    if isinstance(instance, Person):
        persons = Person.objects.filter(pk=instance.pk)
    elif isinstance(instance, Email):
        email = instance
        persons = Person.objects.filter(
            Q(email=email) |
            Q(employments__email=email)
        )
    elif isinstance(instance, URL):
        url = instance
        persons = Person.objects.filter(urls=url)
    elif isinstance(instance, User):
        user = instance
        persons = Person.objects.filter(contact_persons=user)
    elif isinstance(instance, Employment):
        employment = instance
        persons = Person.objects.filter(employments=employment)
    elif isinstance(instance, Company):
        company = instance
        persons = Person.objects.filter(employments__company=company)

    if persons.exists():
        persons.update(updated_at=now)
        model_instances_changed.send(sender=Person, instances=persons)
