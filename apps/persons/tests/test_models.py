import datetime

from django.db.utils import IntegrityError
from django.test import TestCase
from django.utils import timezone

from apps.contacts.models import Contact
from apps.employments.tests.factories import EmploymentFactory
from apps.metronom_commons.tests.test_helpers import validate_uuid4_string
from apps.persons.models import Person
from apps.persons.tests.factories import PersonFactory
from apps.addresses.tests.factories import AddressFactory


class PhoneNumberTest(TestCase):

    validNumbers = """
    0783268674
    41783268674
    041783268674
    0041783268674
    +41783268674
    ++41783268674
    ++41(0)783268674
    """.split('\n')

    invalidNumbers = """
    783268674
    00783268674
    1783268674
    +041783268674
    00041783268674
    +++41783268674
    04178326867
    (0)783268674
    ++410783268674
    """.split('\n')


class PersonWithMinimalAttributesCreateTest(TestCase):

    def setUp(self):
        self.person = PersonFactory()

    def test_correct_creation(self):
        self.assertEqual(Person.objects.get(first_name="John", last_name="Doe"), self.person)

    def test_new_person_should_have_an_uuid(self):
        self.assertEqual(validate_uuid4_string(str(self.person.id)), True)

    def test_new_person_should_have_an_public_id_integer(self):
        self.assertEqual(isinstance(self.person.public_id, int), True)

    def test_new_person_should_get_the_next_public_id(self):
        self.person2 = PersonFactory()
        self.assertEqual(self.person.public_id + 1, self.person2.public_id)

    def test_new_person_should_be_active_by_default(self):
        self.assertEqual(self.person.status, Contact.ACTIVE)

    def test_new_person_should_have_an_unknown_gender_by_default(self):
        self.assertEqual(self.person.gender, Person.GENDER_NOT_KNOWN)

    def test_new_person_should_have_german_language_by_default(self):
        self.assertEqual(self.person.language, Person.GERMAN)

    def test_creation_of_user(self):
        self.assertEqual(Person.objects.get(first_name="John", last_name="Doe"), self.person)

    def test_creation_of_another_person_with_same_name_works(self):
        self.person2 = PersonFactory()
        self.assertNotEqual(self.person.id, self.person2.id)

    def test_creation_with_a_given_uuid_str_works_without_given_datetime(self):
        PersonFactory(
            id="d34935c9-b72e-4aa8-8fd8-6f8a2e57266c"
        )

    def test_creation_with_a_given_uuid_str_works(self):
        PersonFactory(
            id="d34935c9-b72e-4aa8-8fd8-6f8a2e57266c",
            created_at=timezone.now(),
            updated_at=timezone.now()
        )

    def test_creation_with_same_uuid_does_not_work(self):
        with self.assertRaises(IntegrityError):
            PersonFactory(
                id=str(self.person.id),
                created_at=timezone.now(),
                updated_at=timezone.now()
            )


class PersonCreateTest(TestCase):

    def setUp(self):
        self.person = PersonFactory(
            language=Person.ENGLISH,
            gender=Person.GENDER_FEMALE,
            manner_of_address='Dr. Phil.',
            birth_date=datetime.date(1981, 1, 1),
            email__email="john.doe@example.org",
            landline_phone='+410783268674',
        )

    def test_correct_creation(self):
        self.assertEqual(Person.objects.get(first_name="John", last_name="Doe"), self.person)

    def test_new_person_should_have_an_uuid(self):
        self.assertEqual(validate_uuid4_string(str(self.person.id)), True)


class TestLatestEmploymentAttrOfPerson(TestCase):

    def setUp(self):
        self._create_persons()

    def _create_persons(self, count=3):

        for i in range(count):
            person = PersonFactory()
            employment = EmploymentFactory(person=person)

    def test_latest_employment(self):
        for i in Person.objects.all():
            self.assertIsNotNone(i.latest_employment)


class PersonSoftDeleteTest(TestCase):

    def test_soft_deletion(self):
        person1 = PersonFactory()
        person2 = PersonFactory()
        person3 = PersonFactory()
        self.assertEqual(Person.objects.count(), 3)
        person3.delete()
        self.assertEqual(Person.objects.count(), 2)
        self.assertEqual(Person.all_objects.count(), 3)

    def test_soft_deletion_public_id(self):
        person1 = PersonFactory()
        person2 = PersonFactory()
        person3 = PersonFactory()
        self.assertEqual(Person.objects.count(), 3)
        person3.delete()
        self.assertEqual(Person.objects.count(), 2)
        self.assertEqual(Person.all_objects.count(), 3)
        person4 = PersonFactory()
        self.assertEqual(Person.objects.count(), 3)
        self.assertEqual(Person.all_objects.count(), 4)
        public_id = person3.public_id
        self.assertEqual(person4.public_id, public_id + 1)
        person3.save()
        self.assertEqual(Person.objects.count(), 4)


class TestCreatePersonWithoutSomeFields(TestCase):

    def test_add_person_without_email(self):
        address = AddressFactory()
        person_without_email = Person(
            language=Person.ENGLISH,
            gender=Person.GENDER_FEMALE,
            manner_of_address='Dr. Phil.',
            birth_date=datetime.date(1981, 1, 1),
            landline_phone='+410783268674',
            private_address=address
        )
        person_without_email.save()
        self.assertEqual(person_without_email.id, Person.objects.first().id)
        self.assertEqual(person_without_email.email, None)

    def test_add_person_without_address(self):
        person_without_email = Person(
            language=Person.ENGLISH,
            gender=Person.GENDER_FEMALE,
            manner_of_address='Dr. Phil.',
            birth_date=datetime.date(1981, 1, 1),
            landline_phone='+410783268674',
        )
        person_without_email.save()
        self.assertEqual(person_without_email.id, Person.objects.first().id)
        self.assertEqual(person_without_email.private_address, None)
