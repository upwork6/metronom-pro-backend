import datetime
import random
import uuid

import factory
from django.contrib.auth import get_user_model
from factory.fuzzy import FuzzyDateTime

from apps.addresses.tests.factories import AddressFactory
from apps.emails.tests.factories import EmailFactory
from apps.persons.models import Person, Promotion
from apps.users.models import User


class PromotionSimpleFactory(factory.DjangoModelFactory):

    name = factory.Faker('sentence')
    description = factory.Faker('sentence')

    class Meta:
        model = Promotion


class PromotionFactory(factory.DjangoModelFactory):
    name = factory.Iterator(Promotion.objects.all())

    class Meta:
        model = Promotion
        django_get_or_create = ('name',)


class PersonFactory(factory.django.DjangoModelFactory):
    email = factory.SubFactory(EmailFactory)
    first_name = "John"
    last_name = "Doe"
    private_address = factory.SubFactory(AddressFactory)
    birth_date = FuzzyDateTime(datetime.datetime(1950, 1, 1, tzinfo=datetime.timezone.utc),
                               datetime.datetime(2000, 1, 1, tzinfo=datetime.timezone.utc),
                               force_day=3, force_second=42)
    landline_phone = factory.Sequence(lambda n: "+494854073{}{}".format(n, random.choice(range(10, 99))))
    mobile_phone = factory.Sequence(lambda n: "+498097909{}{}".format(n, random.choice(range(10, 99))))

    class Meta:
        model = Person


class UserFactory(factory.django.DjangoModelFactory):
    first_name = factory.Iterator(["John", "Peter", "Jakub", "Mia", "Emma", "Sofia", "Alex", "Petra",
                                   "Jess", "Torben", "Richard", "Martin", "Andrea", "Julia", "Martin"])
    last_name = factory.Iterator(["Doe", "Schmidt", "Müller", "Rosenheim", "Neumann", "Stelter", "Li",
                                  "Meier", "Keller", "Huber", "Murphy", "Gruber", "Bianchi", "Gerber",
                                  "Jäger", "Schweitzer", "Buchli", "Winzeler", "Bürgler"])
    is_active = True
    email = factory.Faker('email')
    username = factory.Sequence(lambda n: uuid.uuid4())

    class Meta:
        model = User


class PersonNiceFactory(factory.django.DjangoModelFactory):
    first_name = factory.Iterator(["John", "Peter", "Jakub", "Mia", "Emma", "Sofia", "Alex", "Petra",
                                   "Jess", "Torben", "Richard", "Martin", "Andrea", "Julia", "Martin"])
    last_name = factory.Iterator(["Doe", "Schmidt", "Müller", "Rosenheim", "Neumann", "Stelter", "Li",
                                  "Meier", "Keller", "Huber", "Murphy", "Gruber", "Bianchi", "Gerber",
                                  "Jäger", "Schweitzer", "Buchli", "Winzeler", "Bürgler"])
    language = factory.Iterator([x[0] for x in Person.LANGUAGE_CHOICES])
    gender = factory.Iterator([x[0] for x in Person.GENDER_CHOICES])
    manner_of_address = factory.Iterator(["", "Herr", "Frau", "Frau Dr.", "Herr Dr.", "Prof.", "Herr Dr. Dr."])
    birth_date = FuzzyDateTime(datetime.datetime(1950, 1, 1, tzinfo=datetime.timezone.utc),
                               datetime.datetime(2000, 1, 1, tzinfo=datetime.timezone.utc),
                               force_day=3, force_second=42)

    email = factory.SubFactory(EmailFactory, email=factory.LazyAttribute(
        lambda o: "%s.%s@gmail.com" % (o.factory_parent.first_name.lower(), o.factory_parent.last_name.lower()))
    )

    private_address = factory.SubFactory(AddressFactory)

    # Can't use the factory.Faker('phone_number', locale="de_DE") because for some reasons (didn't check them though)
    # it has weird behaviour with the PhoneNumberField. To be more accurate it sometimes generate empty instances of
    # the PhoneNumberField and assign them to the Person model instance. And when this happen the string representation
    # of the PhoneNumberField becomes really weird - "+NoneNone"
    landline_phone = factory.Sequence(lambda n: "+494854073{}{}".format(n, random.choice(range(10, 99))))
    mobile_phone = factory.Sequence(lambda n: "+498097909{}{}".format(n, random.choice(range(10, 99))))

    class Meta:
        model = Person

    @factory.post_generation
    def promotions(self, create, extracted, **kwargs):
        if create:
            for i in range(0, random.randint(0, 3)):
                promotion = PromotionFactory()
                self.promotions.add(promotion)

    @factory.post_generation
    def contact_persons(self, create, extracted, **kwargs):
        User = get_user_model()
        if create:
            user = User.objects.all().order_by('?').first()
            if user:
                self.contact_persons.add(user)
