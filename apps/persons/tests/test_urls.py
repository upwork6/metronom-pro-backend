from django.test import TestCase
from django.urls import resolve, reverse

from apps.persons.tests.factories import PersonFactory


class TestPersonURLs(TestCase):
    def setUp(self):
        self.person = PersonFactory()

    def test_person_base_reverse(self):
        assert reverse('api:person-list') == '/api/persons/'

    def test_person_base_resolve(self):
        assert resolve('/api/persons/').view_name == 'api:person-list'

    def test_person_detail_reverse(self):
        endpoint = reverse('api:person-detail', kwargs={'id': self.person.id})
        assert endpoint == f'/api/persons/{self.person.id}/'

    def test_person_detail_resolve(self):
        assert resolve(f'/api/persons/{self.person.id}/').view_name == 'api:person-detail'
