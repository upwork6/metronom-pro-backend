from django.urls import reverse
from rest_framework import status

from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.persons.tests.factories import PersonFactory
from apps.users.tests.factories import UserFactory


class TestPersonUserDetails(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.person = PersonFactory()
        cls.person_with_user = PersonFactory(first_name="John", last_name="User")
        cls.user = UserFactory(person_in_crm=cls.person_with_user)

        cls.person_endpoint = reverse('api:person-detail', kwargs={'id': cls.person.pk})
        cls.person_with_user_endpoint = reverse('api:person-detail', kwargs={'id': cls.person_with_user.pk})
        cls.person_endpoint_response = cls.user_client.get(cls.person_endpoint)
        cls.person_with_user_endpoint_response = cls.user_client.get(cls.person_with_user_endpoint)

    def test_responses_should_return_ok_status(self):
        assert self.person_endpoint_response.status_code == status.HTTP_200_OK
        assert self.person_with_user_endpoint_response.status_code == status.HTTP_200_OK

    def test_person_should_contain_an_empty_user_uuid(self):
        assert self.person_endpoint_response.data['user_uuid'] is None

    def test_person_with_user_should_contain_the_user_uuid(self):
        assert self.person_with_user_endpoint_response.data['user_uuid'] == self.person_with_user.user.pk

    def test_result_person_should_still_contain_the_correct_person_uuid(self):
        assert self.person_with_user_endpoint_response.data['id'] == str(self.person_with_user.pk)

    def test_result_person_should_still_contain_the_public_id(self):
        assert self.person_with_user_endpoint_response.data['public_id'] == self.person_with_user.public_id
