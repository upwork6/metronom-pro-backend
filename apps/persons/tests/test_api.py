
import copy
import json

import pytest
from django.contrib.auth.models import Group
from django.core.cache import caches
from django.urls import reverse
from rest_framework import status

from apps.addresses.tests.factories import AddressFactory
from apps.emails.tests.factories import EmailFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.persons.models import Promotion
from apps.persons.tests.factories import PersonFactory, PromotionSimpleFactory
from apps.persons.views import PersonAutocompleteView
from apps.users.tests.factories import UserFactory
from apps.offers.tests.factories import OfferFactory, OfferStatus, CampaignOfferFactory

cache = caches['autocompletes']


class TestPersonList(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.person1 = PersonFactory()
        cls.person2 = PersonFactory(first_name="Second", last_name="User")
        cls.endpoint = reverse('api:person-list')
        cls.response = cls.user_client.get(cls.endpoint)

    def test_get_should_return_ok_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_get_should_contain_the_list_of_users(self):
        data = [x['first_name'] for x in self.response.data]
        self.assertIn(self.person1.first_name, data)
        self.assertIn(self.person2.first_name, data)

    def test_get_should_contain_two_persons(self):
        # plus five added by setUpTestData
        assert len(self.response.data) == 7

    def test_result_should_contain_the_correct_uuid(self):
        data = [x['id'] for x in self.response.data]
        self.assertIn(str(self.person1.pk), data)
        self.assertIn(str(self.person2.pk), data)

    def test_result_should_contain_a_public_id(self):
        data = [x['public_id'] for x in self.response.data]
        self.assertIn(self.person1.public_id, data)
        self.assertIn(self.person2.public_id, data)

    def test_result_should_contain_a_latest_employment(self):
        assert 'latest_employment_uuid' in self.response.data[0].keys()
        assert 'latest_employment_uuid' in self.response.data[1].keys()

    @pytest.mark.skip("TODO #87 - What was this test checking? All of them should of access")
    def test_permissions(self):
        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        self.login_as_accounting_user()
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])

    @pytest.mark.skip("TODO #87 - you should check for all ROLE.ADMIN_ROLES + ROLE.USER_ROLES")
    def test_users_and_admins_can_see_list(self):
        # TODO: Check for status code and correct list
        pass

    @pytest.mark.skip("TODO #87 - you should check for all ROLE.LIMITED_ROLES")
    def test_limited_users_can_not_see_list(self):
        pass


class TestPersonDetails(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.person = PersonFactory()
        cls.endpoint = reverse('api:person-detail', kwargs={'id': cls.person.id})
        cls.response = cls.user_client.get(cls.endpoint)

    def test_get_should_return_ok_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_get_should_contain_the_name_of_the_user(self):
        assert self.response.data['first_name'] == self.person.first_name

    def test_result_should_contain_the_correct_uuid(self):
        assert self.response.data['id'] == str(self.person.pk)

    def test_result_should_contain_a_public_id(self):
        assert self.response.data['public_id'] == self.person.public_id

    def test_request_should_work_both_with_the_uuid_and_public_id(self):
        endpoint = reverse('api:person-detail', kwargs={'id': self.person.public_id})
        response = self.user_client.get(endpoint)
        assert response.status_code == status.HTTP_200_OK
        assert response.data['first_name'] == self.person.first_name
        assert response.data['id'] == str(self.person.pk)

    def test_persong_wrong_id_raise_404(self):
        endpoint = reverse('api:person-detail', kwargs={'id': 000})
        response = self.user_client.get(endpoint)
        assert response.status_code == status.HTTP_404_NOT_FOUND
        endpoint = reverse('api:person-detail', kwargs={'id': str(self.person.id)[:-4] + "bbcd"})
        response = self.user_client.get(endpoint)
        assert response.status_code == status.HTTP_404_NOT_FOUND
        endpoint = reverse('api:person-detail', kwargs={'id': "12345"})
        response = self.user_client.get(endpoint)
        assert response.status_code == status.HTTP_404_NOT_FOUND
        response = self.user_client.get("/persons/123asdqq2bdfsg45/")
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.skip("TODO #87 - all users / admins can see the Person details")
    def test_permissions(self):
        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        self.login_as_accounting_user()
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])


class TestCreatePerson(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:person-list')
        cls.private_address = AddressFactory()
        cls.last_name = 'Doe'
        cls.post_data = {
            'last_name': cls.last_name,
            'private_address': {
                'city': cls.private_address.city,
                'postal_code': cls.private_address.postal_code
            },
            'email': 'john.doe@example.com',
            'landline_phone': '+41555111141'
        }
        cls.response = cls.user_client.post(cls.endpoint, data=cls.post_data, format='json')

    def test_only_minimal_data_post(self):
        post_data = copy.deepcopy(self.post_data)
        del post_data['private_address']
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data['last_name'] == self.last_name
        self.assertEqual(response.data['private_address'], None)

    def test_post_person_without_private_address(self):
        post_data = copy.copy(self.post_data)
        del post_data['email']
        del post_data['landline_phone']
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data['last_name'] == self.last_name
        self.assertEqual(response.data['landline_phone'], None)
        self.assertEqual(response.data['mobile_phone'], None)

    def test_post_person_without_email(self):
        post_data = copy.copy(self.post_data)
        del post_data['email']
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data['last_name'] == self.last_name
        self.assertEqual(response.data.get('email'), None)

    def test_email_response(self):
        assert self.response.status_code == status.HTTP_201_CREATED
        assert self.response.data['email'] == self.post_data['email']

    def test_phone_number_response(self):
        assert self.response.status_code == status.HTTP_201_CREATED
        assert self.response.data['landline_phone'] == self.post_data['landline_phone']

    def test_address_response(self):
        assert self.response.status_code == status.HTTP_201_CREATED
        assert self.response.data['private_address']['city'] == self.private_address.city

    @pytest.mark.skip("TODO #87 - all users / admins can create Persons")
    def test_permissions(self):
        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        response = self.user_client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.user_client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        self.login_as_accounting_user()
        response = self.user_client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 403)
        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.user_client.post(self.endpoint, data=self.post_data, format='json')
        self.assertEqual(response.status_code, 201)
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])


class TestUpdatePerson(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.person = PersonFactory()
        cls.contact_person1 = UserFactory()
        cls.contact_person2 = UserFactory()
        cls.contact_person3 = UserFactory()
        cls.endpoint = reverse('api:person-detail', kwargs={'id': cls.person.id})

    def test_change_last_name_response(self):
        last_name = 'Smith'
        patch_data = {'last_name': last_name}
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert response.data['last_name'] == last_name

    def test_change_email_response(self):
        self.person.email = EmailFactory()
        new_email = 'john.doe@example.com'
        patch_data = {'email': new_email}
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert response.data['email'] == new_email

    def test_add_phone_number_response(self):
        phone = '+41555111141'
        patch_data = {'landline_phone': phone}
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert response.data['landline_phone'] == phone

    def test_add_contact_persons_response(self):
        patch_data = {'contact_persons_uuids': [self.contact_person1.id, self.contact_person2.id]}
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        contact_persons_last_uuids = response.data['contact_persons_uuids']
        assert response.status_code == status.HTTP_200_OK
        assert self.contact_person1.id in contact_persons_last_uuids
        assert self.contact_person2.id in contact_persons_last_uuids

    def test_change_contact_persons_response(self):
        self.person.contact_persons = [self.contact_person1]
        self.person.save()
        patch_data = {'contact_persons_uuids': [self.contact_person2.id, self.contact_person3.id]}
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        contact_persons_last_uuids = response.data['contact_persons_uuids']
        assert response.status_code == status.HTTP_200_OK
        assert self.contact_person1.id not in contact_persons_last_uuids
        assert self.contact_person2.id in contact_persons_last_uuids
        assert self.contact_person3.id in contact_persons_last_uuids

    @pytest.mark.skip("TODO #87 - all users / admins can edit Person details")
    def test_permissions(self):
        hr = Group.objects.get(name="HR")
        self.test_user.groups.set([hr])
        last_name = 'Smith'
        patch_data = {'last_name': last_name}
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        self.login_as_accounting_user()
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        backoffice = Group.objects.get(name="Backoffice")
        self.test_user.groups.set([backoffice])
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        admin = Group.objects.get(name="Admin")
        self.test_user.groups.set([admin])
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        users = Group.objects.get(name="Users")
        self.test_user.groups.set([users])


class TestUpdatePersonAddress(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.person1 = PersonFactory()
        cls.person2 = PersonFactory(private_address=None)
        cls.endpoint = reverse('api:person-detail', kwargs={'id': cls.person2.id})

    def test_person_address_exists_and_updated(self):
        self.endpoint = reverse('api:person-detail', kwargs={'id': self.person1.id})
        city = 'Zurich'
        assert city != self.person1.private_address.city
        patch_data = {'private_address': {'city': city, 'postal_code': '1234'}}
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert response.data['private_address']['city'] == city

    def test_person_address_not_exists_but_updated(self):
        city = 'Zurich'
        patch_data = {'private_address': {'city': city, 'postal_code': '1234'}}
        self.assertIsNone(self.person2.private_address)
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        self.person2.refresh_from_db()
        self.assertIsNotNone(self.person2.private_address)
        assert response.data['private_address']['city'] == self.person2.private_address.city
        assert response.data['private_address']['city'] == city

    def test_person_address_neither_exists_nor_updated(self):
        last_name = 'Smith'
        patch_data = {'last_name': last_name}
        self.assertIsNone(self.person2.private_address)
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.person2.refresh_from_db()
        assert response.status_code == status.HTTP_200_OK
        assert response.data['last_name'] == last_name
        self.assertIsNone(self.person2.private_address)
        self.assertIsNone(response.data['private_address'])


class TestUpdatePersonEmail(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.person1 = PersonFactory()
        cls.person2 = PersonFactory(email=None)
        cls.endpoint = reverse('api:person-detail', kwargs={'id': cls.person2.id})

    def test_person_email_exists_and_updated(self):
        self.endpoint = reverse('api:person-detail', kwargs={'id': self.person1.id})
        email = 'john.doe@example.com'
        patch_data = {'email': email}
        assert email != self.person1.email.email
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert response.data['email'] == email

    def test_person_email_not_exists_but_updated(self):
        email = 'john.doe@example.com'
        patch_data = {'email': email}
        self.assertIsNone(self.person2.email)
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        self.person2.refresh_from_db()
        self.assertIsNotNone(self.person2.email)
        assert response.data['email'] == self.person2.email.email
        assert response.data['email'] == email

    def test_person_email_neither_exists_nor_updated(self):
        last_name = 'Smith'
        patch_data = {'last_name': last_name}
        self.assertIsNone(self.person2.email)
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.person2.refresh_from_db()
        assert response.status_code == status.HTTP_200_OK
        assert response.data['last_name'] == last_name
        self.assertIsNone(self.person2.email)


class TestDeletePerson(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def setUp(self):
        self.person = PersonFactory()
        self.endpoint = reverse('api:person-detail', kwargs={'id': self.person.public_id})

    def test_delete_person_should_be_allowed_only_for_admins(self):
        response = self.user_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_403_FORBIDDEN
        response = self.admin_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_permissions(self):
        response = self.hr_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)

        response = self.backoffice_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)

        response = self.accounting_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)

        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)

        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)

    def test_delete_person_in_created_offer_permissions(self):
        offer = OfferFactory(contact_person=self.person)

        assert offer.offer_status == OfferStatus.CREATED
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_person_in_sent_offer_permissions(self):
        offer = OfferFactory(contact_person=self.person)
        offer.change_status_to_sent()
        offer.refresh_from_db()

        assert offer.offer_status == OfferStatus.SENT
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        offer.change_status_to_accepted()
        offer.refresh_from_db()

        assert offer.offer_status == OfferStatus.ACCEPTED
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_person_in_created_camopaign_offer_permissions(self):
        campaign_offer = CampaignOfferFactory(contact_person=self.person)

        assert campaign_offer.campaign_status == OfferStatus.CREATED
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_person_in_sent_campaign_offer_permissions(self):
        campaign_offer = CampaignOfferFactory(contact_person=self.person)
        campaign_offer.change_status_to_sent()
        campaign_offer.refresh_from_db()

        assert campaign_offer.campaign_status == OfferStatus.SENT
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        campaign_offer.change_status_to_accepted()
        campaign_offer.refresh_from_db()

        assert campaign_offer.campaign_status == OfferStatus.ACCEPTED
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestPromotionApi(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.promotion = PromotionSimpleFactory()
        cls.list_endpoint = reverse('api:promotion-list')
        cls.detail_endpoint = reverse('api:promotion-detail', args=[cls.promotion.id])

    def test_list_promotion_categories(self):
        response = self.admin_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]['name'], self.promotion.name)
        self.assertEqual(response.data[0]['description'], self.promotion.description)

    def test_create_promotion_categories(self):
        before = Promotion.objects.count()
        post_data = {
            'name': "New name"
        }
        response = self.admin_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(before + 1, Promotion.objects.count())

    def test_retrieve(self):
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], self.promotion.name)

    def test_patch(self):
        patch_data = {
            'name': "New name"
        }
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], patch_data['name'])

    def test_delete(self):
        promotion = PromotionSimpleFactory()
        detail_endpoint = reverse('api:promotion-detail', args=[promotion.id])
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_promotion_list(self):
        """ Check that only groups that have access to this endpoint are Admin, User, Backoffice
        """
        response = self.user_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)

    def test_permissions_promotion_detail(self):
        """ Check that only groups that have access to this endpoint are Admin, User, Backoffice
        """
        response = self.user_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)

    def test_permissions_promotion_create(self):
        """ Check that only groups that have access to this endpoint are Admin, Backoffice
        """
        post_data = {
            'name': "New name"
        }
        response = self.user_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        post_data = {
            'name': "New name2"
        }
        response = self.admin_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)

    def test_permissions_promotion_patch_not_used(self):
        """ Check that only groups that have access to this endpoint are Admin, Backoffice
        """
        patch_data = {
            'name': "New name"
        }
        response = self.user_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        patch_data = {
            'name': "New name2"
        }
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_promotion_patch_used(self):
        """ Check that only groups that have access to this endpoint are Admin, Backoffice
        """
        person = PersonFactory()
        person.promotions.add(self.promotion)
        patch_data = {
            'name': "New name"
        }
        response = self.backoffice_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_delete_not_used(self):
        promotion = PromotionSimpleFactory()
        detail_endpoint = reverse('api:promotion-detail', args=[promotion.id])
        response = self.user_client.delete(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(self.detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)
        promotion = PromotionSimpleFactory()
        detail_endpoint = reverse('api:promotion-detail', args=[promotion.id])
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_delete_used(self):
        promotion = PromotionSimpleFactory()
        detail_endpoint = reverse('api:promotion-detail', args=[promotion.id])
        person = PersonFactory()
        person.promotions.add(promotion)
        response = self.backoffice_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used Promotion"]
            }
        )


class TestPersonAutocomplete(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.person1 = PersonFactory()
        cls.person2 = PersonFactory(email=None)
        cls.url = reverse("autocompletes:persons")
        cls.response = cls.user_client.get(cls.url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_data(self):
        self.assertIn(str(self.person1.id), json.loads(self.response.content)["persons:persons"].keys())
        self.assertIn(str(self.person2.id), json.loads(self.response.content)["persons:persons"].keys())

    def test_response_update(self):
        uuid = str(self.person1.id)
        etag_cache_key = PersonAutocompleteView.get_etag_cache_key()
        etag = cache.get(etag_cache_key, '')
        self.person1.delete()
        response = self.user_client.get(self.url, HTTP_IF_NONE_MATCH=etag)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(uuid, json.loads(response.content)["persons:persons"].keys())
        self.assertIn(str(self.person2.id), json.loads(response.content)["persons:persons"].keys())
