
from dry_rest_permissions.generics import DRYPermissions
from rest_framework.viewsets import ModelViewSet

from apps.api.filters import MetronomPermissionsFilter
from apps.api.mixins import LoggerMixin, PublicIDLookupMixin
from apps.autocompletes.sources import ModelAutocompleteSource
from apps.autocompletes.views import BaseAutocompleteView
from apps.metronom_commons.mixins.views import DeleteForbiddenIfUsedMixin
from apps.persons.models import Person, Promotion
from apps.persons.serializers import PromotionSerializer
from apps.persons.serializers.plain import PersonSerializer
from apps.offers.models import Offer, CampaignOffer, OfferStatus


class PersonViewSet(DeleteForbiddenIfUsedMixin, LoggerMixin, PublicIDLookupMixin, ModelViewSet):
    """Persons in the CRM"""
    serializer_class = PersonSerializer
    queryset = Person.objects.all()
    lookup_field = 'id'
    http_method_names = ['get', 'post', 'patch', 'delete', 'head', 'options', 'trace']
    # filter_backends = (MetronomPermissionsFilter,)
    forbid_delete_if_used_in = {
        Offer: ["contact_person"],
        CampaignOffer: ["contact_person"]
    }

    def get_forbid_delete_qs_campaignoffer(self):
        """ This is method of the DeleteForbiddenIfUsedMixin to provide additional filtration for the CampaignOffer qs
        we'll use during the deletion check
        """
        return CampaignOffer.objects.exclude(campaign_status=OfferStatus.CREATED)

    def get_forbid_delete_qs_offer(self):
        """ This is method of the DeleteForbiddenIfUsedMixin to provide additional filtration for the Offer qs
        we'll use during the deletion check
        """
        return Offer.objects.exclude(offer_status=OfferStatus.CREATED)


class PersonAutocompleteView(BaseAutocompleteView):
    """A View for the autocomplete function of the persons."""
    sources = {
        'persons:persons': ModelAutocompleteSource(Person.objects.all(), id_prop='id', name_prop='name')
    }


class PromotionViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    queryset = Promotion.objects.order_by('name')
    serializer_class = PromotionSerializer
    filter_backends = (MetronomPermissionsFilter,)
    permission_classes = (DRYPermissions,)
    forbid_delete_if_used_in = {
        Person: ["promotions"]
    }
