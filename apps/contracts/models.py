import calendar
from datetime import date, timedelta
from decimal import Decimal

from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from djmoney.models.fields import MoneyField
from djmoney.money import Currency, Money

from apps.workinghours.models import FlexWork
from apps.metronom_commons.data import CURRENCY, SALARY_EXTRA
from apps.metronom_commons.models import UserReadOnlyHRMetronomBaseModel
from apps.users.models import User


class Contract(UserReadOnlyHRMetronomBaseModel):
    # When did or when will the contract start?
    start_date = models.DateField(
        _("Starting day for the contract"),
    )

    # When did or when will the contract end? Can be empty
    end_date = models.DateField(
        _("Ending day for the contract (not mandatory)"),
        blank=True,
        null=True
    )

    monthly_pre_tax_salary = MoneyField(
        _("Monthly Before taxes Salary."),
        default=Money(Decimal(0), Currency(code=CURRENCY.CHF)),
        decimal_places=2,
        max_digits=10,
        help_text=_('Monthly salary pre-tax')
    )

    # wage costs / h
    full_costs_hourly = MoneyField(
        _("Hourly full costs"),
        default=Money(Decimal(0), Currency(code=CURRENCY.CHF)),
        decimal_places=2,
        max_digits=10,
        help_text=_('The hourly full costs including wage costs and office costs'),
        null=True
    )

    monthly_expenses = MoneyField(
        _("Monthly Expenses."),
        default=Money(Decimal(0), Currency(code=CURRENCY.CHF)),
        decimal_places=2,
        max_digits=10,
        help_text=_('Monthly support for expenses')
    )

    # equipment costs / h
    wage_costs_hourly = MoneyField(
        _("Hourly wage costs"),
        default=Money(Decimal(0), Currency(code=CURRENCY.CHF)),
        decimal_places=2,
        max_digits=10,
        help_text=_('The hourly wage costs for a person'),
        null=True
    )

    # can be full or half days
    vacation_days = models.DecimalField(
        _('Number of Annual Vacation Days'),
        decimal_places=2,
        max_digits=10,
    )

    flex_model = models.ForeignKey(FlexWork, on_delete=models.PROTECT)

    monthly_salary_extras = models.IntegerField(
        _('Number of Salaries per Year.'),
        choices=SALARY_EXTRA.CHOICES,
        default=SALARY_EXTRA.THIRTEENTH_SALARY,
        help_text=_('Number of monthly salaries paid annually.')
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="contracts", editable=False, null=False)

    def __str__(self):
        return f'{self.user} | {self.start_date} - {self.end_date}, {self.flex_model}'

    def get_overall_vacation_days(self, year):
        end_date_year = self.end_date.year if self.end_date and self.end_date.year >= self.start_date.year else year
        if self.start_date.year <= year <= end_date_year:
            total_days_in_year = 366 if calendar.isleap(year=year) else 365
            start_date = self.start_date if self.start_date >= date(year, 1, 1) else date(year, 1, 1)
            end_date = self.end_date if self.end_date and self.end_date <= date(start_date.year, 12, 31) else date(
                start_date.year, 12, 31
            )
            contract_days = (end_date - start_date).days + 1
            return self.vacation_days / total_days_in_year * contract_days

        return 0

    def clean(self):
        # if end date is None take the maximum date
        clever_end_date = self.end_date if self.end_date else date.max

        if self.start_date > clever_end_date:
            raise ValidationError(
                message=_('Contract cannot start after its end date.'),
                code='contract-bad-dates',
            )

        queryset = self.user.contracts.filter(
            Q(start_date__lt=self.start_date, end_date=None) |
            Q(start_date__gt=self.start_date, end_date=None)
        ).exclude(pk=self.pk)

        if queryset:
            existing_contract = queryset.first()
            if existing_contract.start_date < self.start_date:
                existing_contract.end_date = self.start_date + timedelta(days=-1)
                existing_contract.save()
            if existing_contract.start_date > self.start_date:
                clever_end_date = self.end_date = existing_contract.start_date + timedelta(days=-1)

        overlapping_contracts = self.user.contracts.filter(
            Q(start_date__lte=self.start_date, end_date__gte=clever_end_date) |
            Q(start_date__gte=self.start_date, start_date__lt=clever_end_date) |
            Q(end_date__gt=self.start_date, end_date__lte=clever_end_date)
        ).exclude(pk=self.pk)

        if overlapping_contracts.count() > 0:
            over_contract = overlapping_contracts.first()
            raise ValidationError(
                message=_('The "start_date" or "end_date" cannot overlap existing contract.'),
                code='contracts-overlapping-dates',
                params=dict(
                    start_date=over_contract.start_date,
                    end_date=over_contract.end_date,
                )
            )

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = _('Contract')
        verbose_name_plural = _('Contracts')
        indexes = [
            models.Index(fields=['start_date', 'end_date']),
        ]
