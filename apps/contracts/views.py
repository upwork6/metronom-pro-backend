from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from apps.api.mixins import LoggerMixin
from apps.contracts.serializers import (ContractRetrieveSerializer,
                                        ContractSerializer,
                                        ContractUpdateSerializer)
from apps.metronom_commons.mixins.views import SerializerDispatcherMixin
from apps.users.views import MeURLMixin

User = get_user_model()


class UserContractViewSet(MeURLMixin, LoggerMixin, SerializerDispatcherMixin, ModelViewSet):
    serializer_class = ContractSerializer
    permission_classes = (IsAuthenticated,)

    restricted_http_method_names = ['get', 'head', 'options', 'trace']
    http_method_names = ['get', 'post', 'patch', 'head', 'options', 'trace']

    serializers_dispatcher = {
        'retrieve': ContractRetrieveSerializer,
        'update': ContractUpdateSerializer,
        'partial_update': ContractUpdateSerializer,
    }

    def perform_create(self, serializer):
        serializer.save(user=self.get_user())

    def perform_update(self, serializer):
        serializer.save(user=self.get_user())

    def get_queryset(self):
        return self.get_user().contracts
