# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Contract


@admin.register(Contract)
class ContractAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'updated_at',
        'start_date',
        'end_date',
        'monthly_pre_tax_salary_currency',
        'monthly_pre_tax_salary',
        'full_costs_hourly_currency',
        'full_costs_hourly',
        'monthly_expenses_currency',
        'monthly_expenses',
        'wage_costs_hourly_currency',
        'wage_costs_hourly',
        'vacation_days',
        'flex_model',
        'monthly_salary_extras',
        'user',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'start_date',
        'end_date',
        'flex_model',
        'user',
    )
