# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-04-26 12:34
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contracts', '0007_auto_20180425_1016'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contract',
            name='tenant_employment',
        ),
        migrations.AddField(
            model_name='contract',
            name='user',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='contracts', to=settings.AUTH_USER_MODEL),
        ),
    ]
