# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-03-13 14:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import model_utils.fields
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('contracts', '0002_auto_20180302_1148'),
    ]

    operations = [
        migrations.AddField(
            model_name='contract',
            name='is_void',
            field=models.BooleanField(default=False, verbose_name='Is in void?'),
        ),
        migrations.AlterField(
            model_name='contract',
            name='created_date',
            field=model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='contract',
            name='id',
            field=models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True, verbose_name='UUID'),
        ),
        migrations.AlterField(
            model_name='contract',
            name='updated_date',
            field=model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified'),
        ),
    ]
