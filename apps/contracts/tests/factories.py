from datetime import date, timedelta

import factory

from apps.contracts.models import Contract
from apps.metronom_commons.data import SALARY_EXTRA
from apps.workinghours.tests.factories import FlexWorkFactory


class ContractFactory(factory.DjangoModelFactory):
    start_date = factory.Sequence(lambda i: date.min + timedelta(days=(i * 30)))
    end_date = factory.Sequence(lambda i: date.min + timedelta(days=(i * 30) + 1))
    monthly_pre_tax_salary = factory.Iterator([2000, 7000])
    full_costs_hourly = factory.Iterator([0, 4, 5])
    monthly_expenses = factory.Iterator([0, 500])
    wage_costs_hourly = factory.Iterator([0, 10])

    vacation_days = factory.Iterator([0, 15, 25, 25.5, 365])
    flex_model = factory.SubFactory(FlexWorkFactory)
    monthly_salary_extras = factory.Iterator([SALARY_EXTRA.NO_EXTRA_SALARY, SALARY_EXTRA.THIRTEENTH_SALARY])
    user = factory.SubFactory('apps.users.tests.factories.UserFactory')

    class Meta:
        model = Contract

    class Params:
        ended = factory.Trait(
            end_date=date.today() - timedelta(days=1),
        )
        finish_tomorrow = factory.Trait(
            start_date=date.today() - timedelta(days=1),
            end_date=date.today() + timedelta(days=1),
        )
        stable = factory.Trait(
            start_date=date.today() + timedelta(days=1),
            end_date=None,
        )


