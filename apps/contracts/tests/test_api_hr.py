from datetime import timedelta

from django.urls import reverse
from django.utils import timezone
from rest_framework import status

from apps.contracts.models import Contract
from apps.contracts.tests.factories import ContractFactory
from apps.metronom_commons.data import SALARY_EXTRA
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.users.tests.factories import UserFactory
from apps.workinghours.tests.factories import FlexWorkFactory


# todo: test HR update contract: only for future


class BaseContractsAbstract(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.start_date = timezone.now().date() + timedelta(weeks=-5)
        cls.end_date = timezone.now().date()
        cls.monthly_pre_tax_salary = 1300.0
        cls.full_costs_hourly = 2.5
        cls.monthly_expenses = 350.0
        cls.wage_costs_hourly = 1.8
        cls.vacation_days = 12.5
        cls.monthly_salary_extras = SALARY_EXTRA.FOURTEENTH_SALARY

        cls.flex_model = FlexWorkFactory()

        cls.post_data = dict(
            start_date=cls.start_date,
            end_date=cls.end_date,
            monthly_pre_tax_salary=cls.monthly_pre_tax_salary,
            full_costs_hourly=cls.full_costs_hourly,
            monthly_expenses=cls.monthly_expenses,
            wage_costs_hourly=cls.wage_costs_hourly,
            vacation_days=cls.vacation_days,
            monthly_salary_extras=cls.monthly_salary_extras,

            flex_model_uuid=cls.flex_model.pk,
        )


class BaseContractsBaseHR(BaseContractsAbstract):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        # one brand new user, one tenant employment, one contract:
        cls.user = UserFactory()
        cls.contract = ContractFactory(user=cls.user)

        cls.my_contract = ContractFactory(user=cls.test_user)

        cls.list_endpoint = reverse('api_users:contract-list', kwargs={'user_pk': cls.user.id})
        cls.detail_endpoint = reverse('api_users:contract-detail',
                                      kwargs={'user_pk': cls.user.id, 'pk': cls.contract.pk})

        assert cls.test_user.contracts.all().count() == 1
        assert cls.test_user.contracts.all().count() == 1


class TestCreateContractHR(BaseContractsBaseHR):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.response = cls.hr_client.post(cls.list_endpoint, data=cls.post_data, format='json')
        cls.data = cls.response.data

        cls.contract = Contract.objects.get(pk=cls.data.get('uuid'))

    def test_create_contract_success(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED, msg=self.response.data)

        self.assertEqual(self.test_user.contracts.all().count(), 1)

    def test_create_contract_id(self):
        field_name = 'uuid'
        value = self.data.get(field_name, None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.contract.pk))

        db_value = getattr(self.contract, 'id', None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value, self.contract.pk)

    def test_create_contract_start_date(self):
        field_name = 'start_date'
        value = self.data.get(field_name, None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.start_date))

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value, self.start_date)

    def test_create_contract_end_date(self):
        field_name = 'end_date'
        value = self.data.get(field_name, None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.end_date))

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value, self.end_date)

    def test_create_contract_monthly_pre_tax_salary(self):
        field_name = 'monthly_pre_tax_salary'
        value = self.data.get(field_name, None)
        self.assertIsNotNone(value)
        self.assertEquals(float(value), self.monthly_pre_tax_salary)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value.amount, self.monthly_pre_tax_salary)

    def test_create_contract_monthly_expenses(self):
        field_name = 'monthly_expenses'
        value = self.data.get(field_name, None)
        self.assertIsNotNone(value)
        self.assertEquals(float(value), self.monthly_expenses)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value.amount, self.monthly_expenses)

    def test_create_contract_vacation_days(self):
        field_name = 'vacation_days'
        value = self.data.get(field_name, None)
        self.assertIsNotNone(value)
        self.assertEquals(float(value), self.vacation_days)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value, self.vacation_days)

    def test_create_contract_flex_model(self):
        field_name = 'flex_model'
        value = self.data.get(field_name, None)
        self.assertIsNotNone(value)

        self.assertEquals(value['id'], str(self.flex_model.id))
        self.assertEquals(value['given_name'], self.flex_model.given_name)

        db_value = getattr(self.contract, 'flex_model', None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value, self.flex_model)

    def test_create_contract_monthly_salary_extras(self):
        field_name = 'monthly_salary_extras'
        value = self.data.get(field_name, None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.monthly_salary_extras)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value, self.monthly_salary_extras)

    def test_create_contract(self):
        field_name = 'user'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)
        self.assertNotEquals(value, self.user)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value, self.user)


class TestUpdateContractHR(BaseContractsBaseHR):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.response = cls.hr_client.patch(cls.detail_endpoint, data=cls.post_data, format='json')
        cls.data = cls.response.data

        # reloading the modified contract
        cls.contract.refresh_from_db()

    def test_update_contract_OK_200(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK, msg=self.response.data)

        self.assertEqual(self.test_user.contracts.all().count(), 1)
        self.assertEqual(self.test_user.contracts.all().count(), 1)

    def test_update_contract_id(self):
        field_name = 'uuid'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

        db_value = getattr(self.contract, 'id', None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value, self.contract.pk)

    def test_update_contract_start_date(self):
        field_name = 'start_date'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertNotEquals(db_value, self.start_date)

    def test_update_contract_end_date(self):
        field_name = 'end_date'
        value = self.data.get(field_name, None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.end_date))

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value, self.end_date)

    def test_update_contract_monthly_pre_tax_salary(self):
        field_name = 'monthly_pre_tax_salary'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertNotEquals(db_value.amount, self.monthly_pre_tax_salary)

    def test_update_contract_full_costs_hourly(self):
        field_name = 'full_costs_hourly'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertNotEquals(float(db_value.amount), self.full_costs_hourly)

    def test_update_contract_monthly_expenses(self):
        field_name = 'monthly_expenses'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertNotEquals(db_value.amount, self.monthly_expenses)

    def test_update_contract_wage_costs_hourly(self):
        field_name = 'wage_costs_hourly'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertNotEquals(db_value.amount, self.wage_costs_hourly)

    def test_update_contract_vacation_days(self):
        field_name = 'vacation_days'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertNotEquals(db_value, self.vacation_days)

    def test_update_contract_flex_model(self):
        field_name = 'flex_model_uuid'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

        db_value = getattr(self.contract, 'flex_model', None)
        self.assertIsNotNone(db_value)
        #
        self.assertNotEquals(db_value, self.flex_model)

    def test_update_contract_monthly_salary_extras(self):
        field_name = 'monthly_salary_extras'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)
        self.assertNotEquals(value, self.monthly_salary_extras)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertNotEquals(db_value, self.monthly_salary_extras)

    def test_update_contract_tenant_employment(self):
        field_name = 'user'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

        db_value = getattr(self.contract, field_name, None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value, self.user)


# HR delete contract: forbidden
class TestDeleteContractHR(BaseContractsBaseHR):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def test_get_failure(self):
        response = self.user_client.delete(self.detail_endpoint, format='json')
        assert response.status_code == 405


class TestUpdateOtherUserContractHR(BaseContractsBaseHR):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.response = cls.hr_client.patch(cls.detail_endpoint, data=cls.post_data, format='json')
        cls.data = cls.response.data

        # reloading the (hopefully) not modified contract
        cls.contract.refresh_from_db()

    def test_update_contract_success(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

        self.assertEqual(self.test_user.contracts.all().count(), 1)

    def test_update_contract_id(self):
        field_name = 'uuid'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

        db_value = getattr(self.contract, 'id', None)
        self.assertIsNotNone(db_value)
        self.assertEquals(db_value, self.contract.pk)

    def test_update_contract_start_date(self):
        field_name = 'start_date'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

    def test_update_contract_end_date(self):
        field_name = 'end_date'
        value = self.data.get(field_name, None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.end_date))

    def test_update_contract_monthly_pre_tax_salary(self):
        field_name = 'monthly_pre_tax_salary'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

    def test_update_contract_full_costs_hourly(self):
        field_name = 'full_costs_hourly'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

    def test_update_contract_monthly_expenses(self):
        field_name = 'monthly_expenses'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

    def test_update_contract_wage_costs_hourly(self):
        field_name = 'wage_costs_hourly'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

    def test_update_contract_vacation_days(self):
        field_name = 'vacation_days'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

    def test_update_contract_flex_model(self):
        field_name = 'flex_model_uuid'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

    def test_update_contract_monthly_salary_extras(self):
        field_name = 'monthly_salary_extras'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)

    def test_update_contract_user(self):
        field_name = 'user'
        value = self.data.get(field_name, None)
        self.assertIsNone(value)
