from datetime import date, timedelta

import pytest
from ddt import ddt, unpack, data
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils import timezone

from apps.contracts.tests.factories import ContractFactory
from apps.users.tests.factories import UserFactory
from apps.workinghours.tests.mixins import TestFlexWorkMixin


class TestContract(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = UserFactory()
        cls.sut = ContractFactory(user=cls.user)

    def test_sut(self):
        self.assertTrue(hasattr(self, 'sut'))
        self.assertIsNotNone(self.sut)

    def test_start_date(self):
        self.assertTrue(hasattr(self.sut, 'start_date'))
        self.assertIsNotNone(self.sut.start_date)

    def test_end_date(self):
        self.assertTrue(hasattr(self.sut, 'end_date'))
        self.assertIsNotNone(self.sut.end_date)

    def test_monthly_pre_tax_salary(self):
        self.assertTrue(hasattr(self.sut, 'monthly_pre_tax_salary'))
        self.assertIsNotNone(self.sut.monthly_pre_tax_salary)

    def test_full_costs_hourly(self):
        self.assertTrue(hasattr(self.sut, 'full_costs_hourly'))
        self.assertIsNotNone(self.sut.full_costs_hourly)

    def test_monthly_expenses(self):
        self.assertTrue(hasattr(self.sut, 'monthly_expenses'))
        self.assertIsNotNone(self.sut.monthly_expenses)

    def test_wage_costs_hourly(self):
        self.assertTrue(hasattr(self.sut, 'wage_costs_hourly'))
        self.assertIsNotNone(self.sut.wage_costs_hourly)

    def test_vacation_days(self):
        self.assertTrue(hasattr(self.sut, 'vacation_days'))
        self.assertIsNotNone(self.sut.vacation_days)

    def test_flex_model(self):
        self.assertTrue(hasattr(self.sut, 'flex_model'))
        self.assertIsNotNone(self.sut.flex_model)

    def test_monthly_salary_extras(self):
        self.assertTrue(hasattr(self.sut, 'monthly_salary_extras'))
        self.assertIsNotNone(self.sut.monthly_salary_extras)


class TestContracts(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = UserFactory()
        cls.contract = ContractFactory(
            start_date=timezone.now().date() - timedelta(weeks=12),
            end_date=timezone.now().date() + timedelta(weeks=12),
            user=cls.user
        )

    def test_contract_right_start_date(self):
        self.assertEquals(self.user.contracts.count(), 1)
        ContractFactory(
            start_date=timezone.now().date() + timedelta(weeks=12),
            end_date=timezone.now().date() + timedelta(weeks=15),
            user=self.user
        )
        self.assertEquals(self.user.contracts.count(), 2)

    def test_contract_wrong_dates(self):
        self.assertEquals(self.user.contracts.count(), 1)
        with self.assertRaises(ValidationError):
            ContractFactory(user=self.user, finish_tomorrow=True)

        with self.assertRaises(ValidationError):
            ContractFactory(user=self.user, stable=True)

        self.assertEquals(self.user.contracts.count(), 1)

    def test_current_contract(self):
        end_date = self.user.current_contract.end_date
        start_date = self.user.current_contract.start_date

        self.assertTrue(start_date <= date.today() <= end_date)


class TestMultipleContracts(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.start_date = timezone.now().date()
        cls.user = UserFactory()

    def test_auto_end_previous_contract(self):
        contract1 = ContractFactory(
            user=self.user,
            start_date=self.start_date,
            end_date=None
        )

        assert self.user.contracts.count() == 1
        self.assertIsNone(contract1.end_date)
        start_date = self.start_date + timedelta(weeks=16)
        contract2 = ContractFactory(
            user=self.user,
            start_date=start_date, end_date=None
        )

        assert self.user.contracts.count() == 2
        contract1.refresh_from_db()
        assert contract1.end_date == start_date + timedelta(days=-1)
        self.assertIsNone(contract2.end_date)

    def test_auto_end_current_contract(self):
        ContractFactory(
            user=self.user,
            start_date=self.start_date,
            end_date=self.start_date + timedelta(weeks=12)
        )
        assert self.user.contracts.count() == 1

        contract2 = ContractFactory(
            user=self.user,
            start_date=self.start_date + timedelta(weeks=208),
            end_date=None
        )

        assert self.user.contracts.count() == 2

        contract3 = ContractFactory(
            user=self.user,
            start_date=self.start_date + timedelta(weeks=20),
            end_date=None
        )

        assert self.user.contracts.count() == 3

        self.assertIsNotNone(contract3.end_date)
        assert contract3.end_date < contract2.start_date
        assert contract3.end_date == contract2.start_date + timedelta(days=-1)

    def test_contract_start_date_after_end_date(self):
        with self.assertRaisesMessage(
                ValidationError, 'Contract cannot start after its end date.'
        ):
            ContractFactory(
                user=self.user,
                start_date=self.start_date,
                end_date=self.start_date + timedelta(weeks=-1)
            )


@ddt
class TestOverlappingContracts(TestCase):
    start_date = timezone.now().date()
    end_date = start_date + timedelta(weeks=12)

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = UserFactory()

        cls.error_message = 'The "start_date" or "end_date" cannot overlap existing contract.'

        ContractFactory(
            user=cls.user,
            start_date=cls.start_date,
            end_date=cls.end_date
        )

    @unpack
    @data(
        {'start_date': start_date, 'end_date': end_date},
        {'start_date': start_date, 'end_date': None},
        {'start_date': start_date, 'end_date': end_date + timedelta(weeks=-2)},
        {'start_date': start_date, 'end_date': end_date + timedelta(weeks=2)},
        {'start_date': start_date + timedelta(weeks=2), 'end_date': end_date},
        {'start_date': start_date + timedelta(weeks=2), 'end_date': None},
        {'start_date': start_date + timedelta(weeks=2), 'end_date': end_date + timedelta(weeks=-2)},
        {'start_date': start_date + timedelta(weeks=2), 'end_date': end_date + timedelta(weeks=2)},
        {'start_date': start_date + timedelta(weeks=-2), 'end_date': end_date},
        {'start_date': start_date + timedelta(weeks=-2), 'end_date': None},
        {'start_date': start_date + timedelta(weeks=-2), 'end_date': end_date + timedelta(weeks=2)},
    )
    def test_overlapping_contract(self, start_date, end_date):
        with self.assertRaisesMessage(ValidationError, self.error_message):
            ContractFactory(
                user=self.user,
                start_date=start_date,
                end_date=end_date
            )

    @unpack
    @data(
        {'start_date': start_date + timedelta(weeks=-4), 'end_date': start_date + timedelta(weeks=-2)},
        {'start_date': end_date + timedelta(weeks=2), 'end_date': end_date + timedelta(weeks=4)}
    )
    def test_non_overlapping_contract(self, start_date, end_date):
        contract = ContractFactory(
            user=self.user,
            start_date=start_date,
            end_date=end_date
        )

        assert contract.start_date == start_date
        assert contract.end_date == end_date


class TestOverlappingContractsDifferentUsers(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = UserFactory()

        cls.start_date = timezone.now().date()
        cls.end_date = cls.start_date + timedelta(weeks=12)

        cls.contract = ContractFactory(
            user=cls.user,
            start_date=cls.start_date,
            end_date=cls.end_date
        )

    def test_contract_overlap_existing_contract_in_another_user(self):
        user = UserFactory()
        contract = ContractFactory(
            user=user,
            start_date=self.start_date,
            end_date=self.end_date
        )

        assert contract.start_date == self.contract.start_date
        assert contract.end_date == self.contract.end_date

        assert self.user.contracts.count() == 1
        assert user.contracts.count() == 1


@ddt
class TestContractVacationDays(TestCase, TestFlexWorkMixin):
    year = 2018
    leap_year = 2020

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.flex_work = cls.get_full_day_full_week_flex_work()

    @unpack
    @data(
        {
            'start_date': date(year, 1, 1), 'end_date': date(year, 6, 30), 'vacation_days': 20,
            'expected_result': [0, 10, 0], 'year': year
        },
        {
            'start_date': date(year, 7, 1), 'end_date': date(year, 12, 31), 'vacation_days': 16,
            'expected_result': [0, 8, 0], 'year': year
        },
        {
            'start_date': date(year, 1, 1), 'end_date': date(year, 3, 31), 'vacation_days': 20,
            'expected_result': [0, 5, 0], 'year': year
        },
        {
            'start_date': date(year, 10, 1), 'end_date': date(year, 12, 31), 'vacation_days': 16,
            'expected_result': [0, 4, 0], 'year': year
        },
        {
            'start_date': date(year, 10, 1), 'end_date': None, 'vacation_days': 16,
            'expected_result': [0, 4, 16], 'year': year
        },
        {
            'start_date': date(year - 1, 10, 1), 'end_date': None, 'vacation_days': 20,
            'expected_result': [5, 20, 20], 'year': year
        },
        {
            'start_date': date(year - 2, 10, 1), 'end_date': date(year, 6, 30), 'vacation_days': 16,
            'expected_result': [16, 8, 0], 'year': year
        },
        {
            'start_date': date(year - 1, 10, 1), 'end_date': date(year + 1, 6, 30), 'vacation_days': 16,
            'expected_result': [4, 16, 8], 'year': year
        },
        {
            'start_date': date(leap_year - 1, 10, 1), 'end_date': date(leap_year + 1, 6, 30), 'vacation_days': 16,
            'expected_result': [4, 16, 8], 'year': leap_year
        },
    )
    def test_contract_vacation_days(self, start_date, end_date, vacation_days, expected_result, year):
        contract = ContractFactory(
            start_date=start_date,
            end_date=end_date,
            flex_model=self.flex_work,
            vacation_days=vacation_days
        )

        assert int(round(contract.get_overall_vacation_days(year - 1))) == expected_result[0]
        assert int(round(contract.get_overall_vacation_days(year))) == expected_result[1]
        assert int(round(contract.get_overall_vacation_days(year + 1))) == expected_result[2]
