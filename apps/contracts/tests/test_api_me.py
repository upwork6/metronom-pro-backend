from datetime import date, timedelta

import pytest
from django.urls import reverse
from rest_framework import status

from apps.contracts.models import Contract
from apps.contracts.tests.factories import ContractFactory
from apps.contracts.tests.test_api_hr import BaseContractsAbstract
from apps.metronom_commons.data import SALARY_EXTRA
# todo: test normal user create on other's user contracts
# todo: test normal user retrieve on other's user contracts
# todo: test normal user update on other's user contracts
# todo: test normal user delete on other's user contracts
from apps.users.tests.factories import UserFactory


class ContractsUsersAbstractMe(BaseContractsAbstract):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.contract = ContractFactory(user=cls.test_user)
        cls.list_endpoint = reverse('api_users:contract-list', kwargs={'user_pk': 'me'})

        cls.detail_endpoint = reverse('api_users:contract-detail',
                                      kwargs={'user_pk': 'me', 'pk': cls.contract.pk})


class TestContractsUsersNotHRForbidden(BaseContractsAbstract):
    """
    Setup one brand new user, one tenant employment, one contract:
    """
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = UserFactory()
        cls.contract = ContractFactory(user=cls.user)

        cls.list_endpoint = reverse('api_users:contract-list', kwargs={'user_pk': cls.user.id})

        cls.response = cls.user_client.get(cls.list_endpoint, format='json')
        cls.data = cls.response.data

    @pytest.mark.skip(reason="No permissions for now")
    def test_get_failure(self):
        self.assertEqual(self.response.status_code, status.HTTP_403_FORBIDDEN)


class TestListContractsUsersMe(ContractsUsersAbstractMe):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.response = cls.user_client.get(cls.list_endpoint, format='json')
        cls.data = cls.response.data

    def test_get_success(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_get_list(self):
        self.assertEqual(len(self.data), Contract.objects.filter(user__id=self.test_user.id).count())


class TestCreateContractsUserMe(ContractsUsersAbstractMe):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.start_date = date.today() - timedelta(weeks=5)
        cls.end_date = date.today()
        cls.monthly_pre_tax_salary = 1300.0
        cls.full_costs_hourly = 2.5
        cls.monthly_expenses = 350.0
        cls.wage_costs_hourly = 1.8
        cls.vacation_days = 12.5
        cls.monthly_salary_extras = SALARY_EXTRA.FOURTEENTH_SALARY

        cls.flex_model = cls.contract.flex_model

        cls.post_data = dict(
            start_date=cls.start_date,
            end_date=cls.end_date,
            monthly_pre_tax_salary=cls.monthly_pre_tax_salary,
            full_costs_hourly=cls.full_costs_hourly,
            monthly_expenses=cls.monthly_expenses,
            wage_costs_hourly=cls.wage_costs_hourly,
            vacation_days=cls.vacation_days,
            monthly_salary_extras=cls.monthly_salary_extras,

            flex_model_uuid=cls.flex_model.pk,
        )

        cls.response = cls.user_client.post(cls.list_endpoint, data=cls.post_data, format='json')
        cls.data = cls.response.data

    @pytest.mark.skip(reason="No permissions for now")
    def test_get_failure(self):
        self.assertEqual(self.response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


# test normal user update his contract: forbidden
class TestUpdateContractsUserMe(ContractsUsersAbstractMe):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.vacation_days = 12.5

        cls.patch_data = dict(
            vacation_days=cls.vacation_days,
        )

        cls.response = cls.user_client.patch(cls.detail_endpoint, data=cls.patch_data, format='json')
        cls.data = cls.response.data

    @pytest.mark.skip(reason="No permissions for now")
    def test_get_failure(self):
        self.assertEqual(self.response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class TestDeleteContractsUserMe(ContractsUsersAbstractMe):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def test_get_failure(self):
        response = self.user_client.delete(self.detail_endpoint, format='json')
        assert response.status_code == 405
