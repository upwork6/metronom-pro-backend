from django.urls import resolve, reverse

from apps.contracts.tests.factories import ContractFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase


class TestContractHRURLs(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.contract = ContractFactory(user=cls.test_hr_user)
        cls.user = cls.contract.user

    def test_contract_list_reverse(self):
        assert reverse('api_users:contract-list',
                       kwargs={'user_pk': self.user.id}) == f'/api/users/{self.user.id}/contracts/'

    def test_contract_list_resolve(self):
        assert resolve(f'/api/users/{self.user.id}/contracts/').view_name == 'api_users:contract-list'

    def test_contract_details_reverse(self):
        contract_url = reverse('api_users:contract-detail',
                               kwargs={'user_pk': self.user.id, 'pk': self.contract.pk})
        assert contract_url == f'/api/users/{self.user.id}/contracts/{self.contract.pk}/'

    def test_contract_details_resolve(self):
        contract_url = f'/api/users/{self.user.id}/contracts/{self.contract.pk}/'
        assert resolve(contract_url).view_name == 'api_users:contract-detail'


class TestContractMEURLs(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.contract = ContractFactory(user=cls.test_user)

    def test_contract_list_reverse(self):
        assert reverse('api_users:contract-list', kwargs={'user_pk': 'me'}) == f'/api/users/me/contracts/'

    def test_contract_list_resolve(self):
        assert resolve(f'/api/users/me/contracts/').view_name == 'api_users:contract-list'

    def test_contract_details_reverse(self):
        contract_url = reverse('api_users:contract-detail',
                               kwargs={'user_pk': 'me', 'pk': self.contract.pk})
        assert contract_url == f'/api/users/me/contracts/{self.contract.pk}/'

    def test_contract_details_resolve(self):
        contract_url = f'/api/users/me/contracts/{self.contract.pk}/'
        assert resolve(contract_url).view_name == 'api_users:contract-detail'
