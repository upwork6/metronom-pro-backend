from rest_framework import serializers

from apps.contracts.models import Contract
from apps.workinghours.models import FlexWork
from apps.workinghours.serializers import FlexWorkContractSerializer


class ContractAbstractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contract
        fields = (
            'end_date',
        )


class ContractUpdateSerializer(ContractAbstractSerializer):
    pass


class ContractBaseSerializer(ContractAbstractSerializer):
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)

    class Meta(ContractAbstractSerializer.Meta):
        fields = ContractAbstractSerializer.Meta.fields + (
            'uuid',
            'start_date',
            'monthly_pre_tax_salary',
            'full_costs_hourly',
            'monthly_expenses',
            'wage_costs_hourly',
            'vacation_days',
            'monthly_salary_extras',
        )

        read_only_fields = (
            'full_costs_hourly',
            'wage_costs_hourly'
        )


class ContractRetrieveSerializer(ContractBaseSerializer):
    flex_model = FlexWorkContractSerializer(required=False, read_only=True)

    class Meta(ContractBaseSerializer.Meta):
        fields = ContractBaseSerializer.Meta.fields + (
            'flex_model',
        )


class ContractSerializer(ContractRetrieveSerializer):
    flex_model_uuid = serializers.PrimaryKeyRelatedField(
        queryset=FlexWork.objects.all(),
        source="flex_model",
        write_only=True
    )

    class Meta(ContractRetrieveSerializer.Meta):
        fields = ContractRetrieveSerializer.Meta.fields + (
            'flex_model_uuid',
        )
