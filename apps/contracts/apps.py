from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ContractsConfig(AppConfig):
    name = 'apps.contracts'
    verbose_name = _('Contracts')

