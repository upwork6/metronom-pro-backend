
import datetime
import sys

from allauth.account.models import EmailAddress
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management import BaseCommand
from django.db.models import Q, Sum
from djmoney.money import Money

from apps.accounting.models import Allocation, AllocationCategory
from apps.activities.models import ActivityGroup, Rate, Step
from apps.addresses.models import Address
from apps.articles.models import ArticleGroup
from apps.backoffice.models import (AgencyBankAccount, AvailableSetting,
                                    Setting, TermsCollection)
from apps.bankaccounts.models import Bank, BankAccount
from apps.companies.models import Company, Office, Sector, Contact
from apps.contracts.models import Contract
from apps.employments.models import Employment
from apps.legacy.models import Absence as LegacyAbsence
from apps.legacy.models import Activity as LegacyActivity
from apps.legacy.models import AnnualWorkingTime as LegacyAnnualWorkingTime
from apps.legacy.models import Article as LegacyArticle
from apps.legacy.models import ArticleEntry as LegacyArticleEntry
from apps.legacy.models import AuthUser as LegacyUser
from apps.legacy.models import BankAccount as LegacyBankAccount
from apps.legacy.models import Employment as LegacyEmployment
from apps.legacy.models import Group as LegacyPromotion
from apps.legacy.models import InvoicingRate as LegacyRate
from apps.legacy.models import Job as LegacyProject
from apps.legacy.models import JobOffer as LegacyOffer
from apps.legacy.models import OfferedActivity as LegacyOfferedActivity
from apps.legacy.models import OfferedArticle as LegacyOfferedArticle
from apps.legacy.models import Person as LegacyPerson
from apps.legacy.models import Profile as LegacyProfile
from apps.legacy.models import Project as LegacyCampaign
from apps.legacy.models import ProjectOffer as LegacyCampaignOffer
from apps.legacy.models import WorklogEntry as LegacyWorklog
from apps.metronom_commons.data import (BILLING_AGREEMENT, BILLING_DELIVERY,
                                        BILLING_INTERVAL)
from apps.offers.models import CampaignOffer, Offer, OfferStatus
from apps.persons.models import Email, Person, Promotion
from apps.projects.models import (ACCOUNTING_TYPES, ActivityAssignment,
                                  Campaign, Project, ProjectActivity,
                                  ProjectActivityStep, ProjectArticle, WorkLog)
from apps.users.models import ROLE
from apps.workinghours.models import WEEKDAYS, Absence, FlexWork, WorkingHours, WorkYear

User = get_user_model()

PROFILE_LEVEL_TO_ROLE = {
    LegacyProfile.CUSTOMER: ROLE.CUSTOMER,
    LegacyProfile.FREELANCER: ROLE.FREELANCER,
    LegacyProfile.EMPLOYEE: ROLE.USER,
    LegacyProfile.ADMINISTRATION: ROLE.BACKOFFICE,
    LegacyProfile.MANAGEMENT: ROLE.ADMIN,
    LegacyProfile.NOT_SET: ROLE.NONE,
}

GENDER_DICT = {'m': Person.GENDER_MALE, 'f': Person.GENDER_FEMALE, '': Person.GENDER_NOT_KNOWN}

WORKLOG_SATISFACTION_DICT = {1: 1, 2: 1, 3: 2, 4: 2, 5: 3, 6: 3, 7: 4, 8: 4}

CAMPAIGN_OFFER_STATUS_DICT = {
    LegacyCampaignOffer.IN_PREPARATION: OfferStatus.CREATED,
    LegacyCampaignOffer.SENT: OfferStatus.SENT,
    LegacyCampaignOffer.PARTIALLY_CLOSED: OfferStatus.PARTLY_ACCEPTED,
    LegacyCampaignOffer.ACCEPTED: OfferStatus.ACCEPTED,
    LegacyCampaignOffer.REJECTED: OfferStatus.DECLINED
}

OFFER_STATUS_DICT = {
    LegacyOffer.IN_PREPARATION: OfferStatus.CREATED,
    LegacyOffer.SENT: OfferStatus.SENT,
    LegacyOffer.ACCEPTED: OfferStatus.ACCEPTED,
    LegacyOffer.REJECTED: OfferStatus.DECLINED,
    LegacyOffer.REPLACED: OfferStatus.REPLACED
}


class Command(BaseCommand):
    """
    TODO delete this from here after finish
    models_to_delete =
    [Contract, Offer, CampaignOffer, User, Person, Employment, EmailAddress, Address, Project,
    Campaign, Allocation, AllocationCategory, Company, Office, Sector, Email, Setting,
    ArticleGroup, Step, Rate, ProjectActivity, ProjectActivityStep, ActivityGroup, WorkLog, ActivityAssignment,
    Promotion, AgencyBankAccount, BankAccount, Bank, ProjectArticle, TermsCollection, WorkYear]

    """
    help = "This command imports the data from the legacy db"

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--name',
            action='store',
            type=str,
            default="Default",
            dest='flexwork_name',
            help='Name for the flexwork model instead of Default',
        )
        parser.add_argument(
            '--instance-name',
            action='store',
            type=str,
            default="Partner & Partner AG",
            dest='instance_name',
            help='Name of the INSTANCE NAME used in the fields with the lack of data. Default Partner & Partner AG',
        )
        parser.add_argument(
            '--instance-url',
            action='store',
            type=str,
            default="https://clean.fineware.ch/",
            dest='instance_url',
            help=('Url of the INSTANCE URL (with trailing slash) constant used in the fields '
                  'with the lack of data. Default https://clean.fineware.ch/'),
        )
        parser.add_argument(
            '--person_id',
            action='store',
            type=str,
            default="00002",
            dest='person_id',
            help=('Legacy person_id for the legacy company to be proceed and be '
                  'copied as a default agency company'),
        )

    def handle(self, *args, **kwargs):
        flexwork_name = kwargs.get("flexwork_name", "Default")
        instance_name = kwargs.get("instance_name", "Partner & Partner AG")
        instance_url = kwargs.get("instance_url", "https://clean.fineware.ch/")
        default_legacy_person_id = kwargs.get("person_id", "00002")
        default_password = settings.LEGACY_DEFAULT_PASSWORD
        default_company_public_id = settings.AGENCY_COMPANY_PUBLIC_ID
        # To filter legacy DB we have to change int to the 0000x format
        exclude_person_id = str(default_company_public_id).zfill(5)

        legacy_company_to_copy = LegacyPerson.objects.filter(company=True,
                                                             person_id=default_legacy_person_id).first()
        if legacy_company_to_copy is None:
            sys.stderr.write(f"There is no legacy company with person_id {default_legacy_person_id}! Aborting")
            return

        default_company, bank_account = self.process_company(
            legacy_company_to_copy, default_password, override_public_id=True)

        legacy_persons_and_persons, exclude_ids, person_rearrange = self._process_users_and_persons(default_password,
                                                                                                    exclude_person_id)
        (legacy_persons_and_companies,
         legacy_persons_and_persons,
         rearranged_public_id) = self._process_companies(default_password, exclude_ids,
                                                         person_rearrange, legacy_persons_and_persons,
                                                         exclude_person_id, default_legacy_person_id)
        # we need to process emails and phone numbers after companies creation
        for legacy_person, person in legacy_persons_and_persons:
            self._process_address_person(legacy_person, person)
            self._process_phone_number_for_person(legacy_person, person)
            self._process_email(legacy_person, person)
        # we need to process phone numbers and emails after all offices creation
        for legacy_company, company in legacy_persons_and_companies:
            self._process_phone_number_for_company(legacy_company, company)
            self._process_email_company(legacy_company, company)

        # Be aware: order matters in this calls.
        self._process_legacy_campaigns(instance_url, default_legacy_person_id)
        self._process_legacy_projects(instance_url, default_legacy_person_id)
        self._process_annual_working_time()
        self._process_absense()
        self._process_allocations()
        self._process_steps_and_rates()
        self._create_and_process_projects_activities_articles_worklog_assignments()
        self._process_promotions()
        self._process_legacy_bank_account(instance_name)
        self._process_legacy_employments()
        self._process_campaign_offers(exclude_person_id, rearranged_public_id)
        self._process_offers(instance_url, exclude_person_id, rearranged_public_id)
        flexwork = self._process_flexwork(flexwork_name)
        self._create_default_employments()
        sys.stdout.write(
            f"{instance_name} used as a default INSTANCE NAME constant! \n"
            f"{instance_url} used as a default INSTANCE URL constant! \n"
            f"Flexwork with name {flexwork.given_name} and uuid {flexwork.id} was created! \n"
            f"Default password {default_password} set for all users! \n"
        )

    def _create_default_employments(self):
        """ Create default employments between imported users and default agency company

        """
        default_company = Company.objects.get(public_id=settings.AGENCY_COMPANY_PUBLIC_ID)
        for user in User.objects.all():
            Employment.objects.get_or_create(
                person=user.person_in_crm,
                company=default_company,
            )

    def _verify_user_email_set_password(self, user, default_password):
        """ This function mark user EmailAddress as verified and set user password

        :param user: User instance
        :param default_password: str
            password to set for user
        """
        verification, created = EmailAddress.objects.get_or_create(user=user, email=user.email)
        verification.verified = True
        verification.set_as_primary()
        verification.save()
        user.set_password(default_password)
        user.save()

    def _create_person(self, legacy_person, default_password, override_public_id=False):
        """ This function creates person from the legacy one and call _verify_user_email_set_password for
        related user

        :param legacy_person: LegacyPerson instance
            person to proceed
        :param default_password: str
            password to set for user. Required to be passed into _verify_user_email_set_password
        :param override_public_id: bool
            If True - we change public_id from legacy one to the latest one public_id + 1
        :returns: Person instance
        """
        gender = GENDER_DICT.get(legacy_person.gender, Person.GENDER_NOT_KNOWN)
        if override_public_id:
            public_id = Contact.objects.order_by("public_id").last().public_id + 1
        else:
            public_id = legacy_person.person_id
        person = Person.objects.using('default').create(
            public_id=public_id,
            manner_of_address=legacy_person.manner_of_address,
            first_name=legacy_person.first_name,
            last_name=legacy_person.name,
            gender=gender,
            language=legacy_person.language,
            birth_date=legacy_person.dob,
        )
        if legacy_person.contact:
            legacy_contact = legacy_person.contact
            contact_person, created = self._get_or_create_user(legacy_contact)
            if created:
                contact_person.role = PROFILE_LEVEL_TO_ROLE.get(legacy_contact.profile.access_level)
                contact_person.created_at = legacy_contact.date_joined
                contact_person.save(update_fields=["created_at"])
                self._verify_user_email_set_password(contact_person, default_password)
            person.contact_persons.add(contact_person)
        return person

    def _update_person(self, legacy_person, person, default_password):
        """ This function exists to update person to the it's related legacy person data

        :param legacy_person: LegacyPerson instance
            person we use to get the data
        :param person: Person instance
            person to update
        :param default_password: str
            password to set for user. Required to be passed into _verify_user_email_set_password

        :returns: Person instance
        """
        default_company_public_id = settings.AGENCY_COMPANY_PUBLIC_ID
        exclude_person_id = str(default_company_public_id).zfill(5)
        gender = GENDER_DICT.get(legacy_person.gender, Person.GENDER_NOT_KNOWN)
        keys = [
            ('manner_of_address', 'manner_of_address'), ('first_name', 'first_name'),
            ('language', 'language'), ('birth_date', 'dob')
        ]
        for key in keys:
            setattr(person, key[0], getattr(legacy_person, key[1]))
        if legacy_person.person_id != exclude_person_id:
            person.public_id = legacy_person.person_id
        else:
            last_person_id = int(LegacyPerson.objects.order_by("person_id").last().person_id)
            person.public_id = last_person_id + 1
        person.gender = gender
        if legacy_person.contact:
            legacy_contact = legacy_person.contact
            contact_person, created = self._get_or_create_user(legacy_contact)
            person.contact_persons.add(contact_person)
            if created:
                contact_person.role = PROFILE_LEVEL_TO_ROLE.get(legacy_contact.profile.access_level)
                contact_person.created_at = legacy_contact.date_joined
                contact_person.save(update_fields=["created_at"])
                self._verify_user_email_set_password(contact_person, default_password)
        person.save()
        return person

    def _check_employments(self, person, process_object, legacy_person, name_object):
        """ Internal function to be called when we need to check if any employment exists for the person/
        Writes errors in the situations:
        1) when person has more than 1 employment in legacy data
        2) When person doesn't have any employment at all

        :param person: Person instace
            person whom employment we check
        :param process_object: instance
            Legacy object triggered the check
        :param legacy_person: LegacyPerson instance
            related LegacyPerson for our Person
        :param name_object: str
            Name of the process_object. eg email, phone
        :returns: Employment instance or None
        """
        employment_qs = Employment.objects.filter(person=person)
        employment_count = employment_qs.count()
        if employment_count > 1:
            sys.stderr.write(
                f"We can't process employment for legacy {name_object} "
                f"with pk {process_object.pk} for legacy person with person_id {legacy_person.person_id} "
                f"because this person with pk {person.pk} has more than 1 employment \n"
            )
        elif not employment_count:
            sys.stderr.write(
                f"We can't process employment for legacy {name_object} "
                f"with pk {process_object.pk} for legacy person with person_id {legacy_person.person_id} "
                f"because this person with pk {person.pk} has 0 employments \n"
            )
        else:
            employment = employment_qs.first()
            return employment
        return None

    def _process_phone_number_for_person(self, legacy_person, person):
        """ Write phone numbers for the provided person. Depending on the phone number type function writes it into
        appropriate field.
        Write errors in the next situations:
        1) When legacy phone number is empty
        2) When legacy phone number has unrecognized type

        :param legacy_person: LegacyPerson instance
            person we use to get the data
        :param person: Person instance
            person to update
        """
        phone_numbers = legacy_person.phone_numbers.all()
        for phone_number in phone_numbers:
            if not phone_number.number:
                sys.stderr.write(
                    f"Empty phone number with pk {phone_number.pk} for legacy person "
                    f"with person_id {legacy_person.person_id}. \n"
                )
                continue
            if phone_number.type == "Mobile":
                person.mobile_phone = phone_number.number
                person.save(update_fields=["mobile_phone"])
            elif phone_number.type == "Work":
                employment = self._check_employments(person, phone_number, legacy_person, "phone")
                if employment is not None:
                    employment.landline_phone = phone_number.number
                    employment.save(update_fields=["landline_phone"])
            else:
                sys.stderr.write(
                    f"Unrecognized type {phone_number.type} for legacy phone number "
                    f"with pk {phone_number.pk} for legacy person with person_id {legacy_person.person_id}. \n"
                )

    def _process_email(self, legacy_person, person):
        """ Create Email object for a person from the legacy data. If legacy email has work type - it fill the
        employment email field

        Write errors in the next situations:
        1) When legacy email has unrecognized type

        :param legacy_person: LegacyPerson instance
            person we use to get the data
        :param person: Person instance
            person to update
        """
        emails = legacy_person.emails.all()
        for email in emails:
            if email.type == "Home":
                email = Email.objects.get_or_create(email=email.email)[0]
                person.email = email
                person.save(update_fields=["email"])
            elif email.type == "Work":
                employment = self._check_employments(person, email, legacy_person, "email")
                if employment is not None:
                    email = Email.objects.get_or_create(email=email.email)[0]
                    employment.email = email
                    employment.save(update_fields=["email"])
            else:
                sys.stderr.write(
                    f"Unrecognized type {email.type} for legacy email "
                    f"with pk {email.pk} for legacy person with person_id {legacy_person.person_id}. \n"
                )

    def _process_address_person(self, legacy_person, new_object):
        """ Process addresses from the provided legacy data and create Address instance. 
        Depending on the legacy address type could create Offices and Employments as well

        :param legacy_person: LegacyPerson instance
            person we use to get the data
        :param new_object: instance
            Could be Company or Person instance
        """
        addresses = legacy_person.addresses.all()
        for address in addresses:
            if address.active:
                status = Address.ACTIVE
            else:
                status = Address.ARCHIVED
            if len(address.country) != 2:
                if address.country == "Schweiz":
                    country = "CH"
                elif address.country in ["Germany", "Deutschland"]:
                    country = "DE"
                else:
                    sys.stderr.write(
                        f"Legacy person {legacy_person.person_id} has address "
                        f"with pk {address.pk} and country {address.country}, but we can't recognize it. Skipping \n"
                    )
                    continue
            else:
                country = address.country
            # we can't use get_or_create here because below we create duplicates with the same data for the
            # offices at the same address, but different companies.
            created = False
            new_address = Address.objects.filter(
                raw=address.address,
                postal_code=address.zip_code,
                city=address.city,
                country=country,
                address_status=status).first()
            if new_address is None:
                created = True
                new_address = Address(
                    raw=address.address,
                    postal_code=address.zip_code,
                    city=address.city,
                    country=country,
                    address_status=status
                )
            if address.type.name == "Geschaeft":
                if isinstance(new_object, Person):
                    if address.employer:
                        company = Company.objects.filter(public_id=address.employer.person_id).first()
                        if company is not None:
                            # there can be several offices by one address, but because of relations
                            # we should create new address for each office of the company
                            # even if the address is the same
                            if not created:
                                office = Office.objects.filter(address=new_address).first()
                                if office is not None and office.company != company:
                                    new_address = Address(
                                        raw=address.address,
                                        postal_code=address.zip_code,
                                        city=address.city,
                                        country=country,
                                        address_status=status
                                    )
                            new_address.save()
                            employment = Employment.objects.get_or_create(
                                person=new_object,
                                company=company,
                                position_name=address.function,
                            )[0]
                            is_headquarter, name = False, ""
                            if address.main_address:
                                name = new_address.raw[:100]
                                is_headquarter = True
                                company.billing_address = new_address
                                company.save(update_fields=['billing_address'])
                            else:
                                name = new_address.raw[:100]
                                if not company.billing_address:
                                    company.billing_address = new_address
                                    company.save(update_fields=['billing_address'])
                            office = Office.objects.get_or_create(
                                address=new_address,
                                company=company,
                                defaults=dict(
                                    is_headquarter=is_headquarter,
                                    name=name
                                )
                            )[0]
                        else:
                            sys.stderr.write(
                                f"Legacy person {legacy_person.person_id} has address "
                                f"with pk {address.pk} and type `Geschaeft`, but this address employment "
                                f"{address.employer.person_id} doesn't exist as Company! Ignoring this address \n"
                            )
                    else:
                        sys.stderr.write(
                            f"Legacy person {legacy_person.person_id} has address "
                            f"with pk {address.pk} and type `Geschaeft`, but this legacy person is a person "
                            f"without employment! Ignoring this address \n"
                        )
                else:  # in other case it is a company
                    new_address.save()
                    is_headquarter, name = False, ""
                    if address.main_address:
                        is_headquarter = True
                    else:
                        name = new_address.raw[:100]
                    office = Office.objects.get_or_create(
                        address=new_address,
                        defaults=dict(
                            company=new_object,
                            is_headquarter=is_headquarter,
                            name=name
                        )
                    )[0]
            elif address.type.name == "Privat":
                if isinstance(new_object, Person):
                    new_address.save()
                    new_object.private_address = new_address
                    new_object.save(update_fields=['private_address'])
                else:  # in other case it is a company
                    sys.stderr.write(
                        f"Legacy person {legacy_person.person_id} has address "
                        f"with pk {address.pk} and type `Privat`, but this legacy person is a company! "
                        f"Ignoring this address \n"
                    )
            else:
                sys.stderr.write(
                    f"Unrecognized type {addresses.type.name} for legacy address "
                    f"with pk {address.pk} for legacy person with person_id {legacy_person.person_id}. \n"
                )

    def _check_headquarter(self, company, process_object, legacy_person, name_object):
        """ Check all offices of the provided company and returns headquarter

        Writes errors in the situations:
        1) When there are more than 1 headquarter for the company
        2) When there is no headquarters for company at all

        :param company: Company instace
            company whom offices we check
        :param process_object: instance
            Legacy object triggered the check
        :param legacy_person: LegacyPerson instance
            related LegacyPerson for our Company
        :param name_object: str
            Name of the process_object. eg email, phone

        :returns: Office instance or None
        """
        headquarter_qs = company.offices.filter(is_headquarter=True)
        headquarter_count = headquarter_qs.count()
        if headquarter_count > 1:
            sys.stderr.write(
                f"We can't process legacy {name_object} "
                f"with pk {process_object.pk} for legacy person with person_id {legacy_person.person_id} "
                f"because this company with pk {company.pk} has more than 1 headquarter \n"
            )
        elif not headquarter_count:
            sys.stderr.write(
                f"We can't process legacy {name_object} "
                f"with pk {process_object.pk} for legacy person with person_id {legacy_person.person_id} "
                f"because this company with pk {company.pk} has 0 headquarters \n"
            )
        else:
            headquarter = headquarter_qs.first()
            return headquarter
        return None

    def _process_phone_number_for_company(self, legacy_person, company):
        """ Write landline_phone for the provided company. 

        Write errors in the next situations:
        1) When legacy phone number is empty
        2) When legacy phone number has unrecognized type
        3) If legacy phone numver has Mobile type. We can proceed landline_phone only.

        :param legacy_person: LegacyPerson instance
            person we use to get the data
        :param company: Company instance
            company to update
        """
        phone_numbers = legacy_person.phone_numbers.all()
        for phone_number in phone_numbers:
            if not phone_number.number:
                sys.stderr.write(
                    f"Empty phone number with pk {phone_number.pk} for legacy person "
                    f"with person_id {legacy_person.person_id}. \n"
                )
                continue
            if phone_number.type == "Mobile":
                sys.stderr.write(
                    f"We can't process type Mobile for legacy person "
                    f"with pk {phone_number.pk} for legacy person with person_id {legacy_person.person_id} "
                    f"because it is associated with a company with pk {company.pk}, not a person! \n"
                )
            elif phone_number.type == "Work":
                headquarter = self._check_headquarter(company, phone_number, legacy_person, "phone")
                if headquarter is not None:
                    headquarter.landline_phone = phone_number.number
                    headquarter.save(update_fields=["landline_phone"])
            else:
                sys.stderr.write(
                    f"Unrecognized type {phone_number.type} for legacy phone number "
                    f"with pk {phone_number.pk} for legacy person with person_id {legacy_person.person_id}. \n"
                )

    def _process_legacy_campaigns(self, instance_url, default_legacy_person_id):
        """ Proceed legacy campaigns into Campaign  objects

        Write errors in the next situations:
        1) When there is no customer in new db
        2) When campaign manager doesn't exists in new db
        3) When contact_person doesn't exists in new db

        :param instance_url: str
            url to use, when we build link to the former object in the legacy platform
        :param default_legacy_person_id: str
        """
        campaigns = LegacyCampaign.objects.all()
        for legacy_campaign in campaigns:
            if legacy_campaign.customer.person_id == default_legacy_person_id:
                company_public_id = settings.AGENCY_COMPANY_PUBLIC_ID
            else:
                company_public_id = int(legacy_campaign.customer.person_id)
            company = Company.objects.filter(public_id=company_public_id).first()
            if company is None:
                sys.stderr.write(
                    f"We can't process legacy project with project_id {legacy_campaign.project_id} "
                    f"because it's customer with person_id {company_public_id} doesn't exists "
                    f"as a Company in our DB! \n"
                )
                continue
            campaign_owner_email = legacy_campaign.manager.email
            campaign_owner = User.objects.filter(email=campaign_owner_email).first()
            if campaign_owner is None:
                sys.stderr.write(
                    f"We can't process legacy project with project_id {legacy_campaign.project_id} "
                    f"because it's manager with email {campaign_owner_email} doesn't exists "
                    f"as a User in our DB! \n"
                )
                continue
            contact_person = None
            if legacy_campaign.contact:
                contact_person_public_id = legacy_campaign.contact.person.person_id
                contact_person = Person.objects.filter(public_id=contact_person_public_id).first()
                if contact_person is None:
                    sys.stderr.write(
                        f"We can't process contact_person for legacy project "
                        f"with project_id {legacy_campaign.project_id} because it's contact person "
                        f"with public_id {contact_person_public_id} doesn't exists as a Person in our DB! "
                        f"Setting contact_person as null \n"
                    )
            if legacy_campaign.archived:
                finish_date, delivery_date = datetime.date(2018, 12, 31), datetime.date(2018, 12, 31)
            else:
                finish_date, delivery_date = datetime.date(2018, 12, 31), datetime.date(2018, 12, 31)
            additional_string = ("<p><em>(</em> Importiert von <a href='{0}projects/{1}/' "
                                 "target='_blank'> {0}projects/{1}/</a>)</p>" .format(instance_url,
                                                                                      legacy_campaign.project_id))
            description = f"{legacy_campaign.description} {additional_string}"
            campaign = Campaign.objects.create(
                old_id=legacy_campaign.project_id,
                title=legacy_campaign.name,
                description=description,
                company=company,
                campaign_owner=campaign_owner,
                contact_person=contact_person,
                start_date=legacy_campaign.created.date(),
                finishing_date=finish_date,
                delivery_date=delivery_date,
            )
            # To save original data in this fields
            Campaign.objects.filter(id=campaign.id).update(
                created_at=legacy_campaign.created,
                updated_at=legacy_campaign.modified,
            )

    def _process_legacy_projects(self, instance_url, default_legacy_person_id):
        """ Proceed legacy projects into the Project objects. Also assign related campaign to the project.

        Write errors in the next situations:
        1) When there is mismatch between legacy manager and user who created the legacy project
        2) When there is no updates for the legacy project, so we can't proceed correct dates
        3) When legacy project company doesn't exists in new db
        4) When legacy project manager doesn't exists in new db

        :param instance_url: str
            url to use, when we build link to the former object in the legacy platform
        :param default_legacy_person_id: str
        """
        project_open_statuses = [LegacyProject.OPEN, LegacyProject.STANDBY]
        projects = LegacyProject.objects.all()
        for legacy_project in projects:
            if legacy_project.manager != legacy_project.created_by:
                sys.stderr.write(
                    f"For legacy job with job_id {legacy_project.job_id} "
                    f"manager ({legacy_project.manager.email}) and created_by ({legacy_project.created_by.email}) "
                    f"fields are different! Using manager as a project_owner. \n"
                )
            if legacy_project.state in project_open_statuses:
                start_date, finish_date, delivery_date = datetime.date(
                    2017, 12, 31), datetime.date(2018, 12, 31), datetime.date(2018, 12, 31)
            elif legacy_project.state == LegacyProject.CLOSED:
                start_date = legacy_project.created.date()
                job_update = legacy_project.updates.filter(state=LegacyProject.CLOSED).order_by("-created").first()
                if job_update is None:
                    finish_date, delivery_date = datetime.date(2018, 12, 31), datetime.date(2018, 12, 31)
                    sys.stderr.write(
                        f"Legacy job with job_id {legacy_project.job_id} is closed but "
                        f"there is no JobUpdate with state closed for this job. "
                        f"So we can't set correct delivery_date and finish_date for the project. "
                        f"Setting finish_date and delivery_date as 2018-12-31 \n"
                    )
                else:
                    delivery_date = job_update.created.date()
                    finish_date = delivery_date
            else:
                start_date, finish_date, delivery_date = legacy_project.created.date(
                ), datetime.date(2018, 12, 31), datetime.date(2018, 12, 31)

            if legacy_project.customer.person_id == default_legacy_person_id:
                company_public_id = settings.AGENCY_COMPANY_PUBLIC_ID
            else:
                company_public_id = int(legacy_project.customer.person_id)
            company = Company.objects.filter(public_id=company_public_id).first()
            if company is None:
                sys.stderr.write(
                    f"We can't process legacy job with job_id {legacy_project.job_id} "
                    f"because it's customer with person_id {company_public_id} doesn't exists "
                    f"as a Company in our DB! \n"
                )
                continue
            campaign = None
            if legacy_project.project:
                campaign_old_id = legacy_project.project.project_id
                campaign = Campaign.objects.filter(old_id=campaign_old_id).first()
                if campaign is None:
                    sys.stderr.write(
                        f"We can't process project for legacy job "
                        f"with job_id {legacy_project.job_id} because it's project "
                        f"with project_id {campaign_old_id} doesn't exists as a Campaign in our DB! "
                        f"Setting campaign as null \n"
                    )
                elif legacy_project.state == LegacyProject.CLOSED:
                    campaign.finishing_date = finish_date
                    campaign.delivery_date = delivery_date
                    campaign.save(update_fields=["finishing_date", "delivery_date"])
            contact_person = None
            if legacy_project.contact:
                contact_person_public_id = legacy_project.contact.person.person_id
                contact_person = Person.objects.filter(public_id=contact_person_public_id).first()
                if contact_person is None:
                    sys.stderr.write(
                        f"We can't process contact for legacy job "
                        f"with job_id {legacy_project.job_id} because it's contact person "
                        f"with public_id {contact_person_public_id} "
                        f"and with company={legacy_project.contact.person.company} "
                        f"doesn't exists as a Person in our DB!"
                        f"Setting contact_person as null \n"
                    )

            project_owner_email = legacy_project.manager.email
            project_owner = User.objects.filter(email=project_owner_email).first()
            if project_owner is None:
                sys.stderr.write(
                    f"We can't process legacy project with project_id {legacy_project.job_id} "
                    f"because it's manager with email {project_owner_email} doesn't exists "
                    f"as a User in our DB! \n"
                )
                continue
            additional_string = ("<p><em>(</em>Importiert von <a href='{0}jobs/{1}/'"
                                 "target='_blank'>{0}jobs/{1}/</a>)</p>".format(instance_url, legacy_project.project_id))
            description = f"{legacy_project.description} {additional_string}"
            project = Project.objects.create(
                old_id=legacy_project.job_id,
                title=legacy_project.name,
                description=description,
                billing_currency=legacy_project.master_currency,
                start_date=start_date,
                finishing_date=finish_date,
                delivery_date=delivery_date,
                billing_address=company.billing_address,
                company=company,
                campaign=campaign,
                contact_person=contact_person,
                project_owner=project_owner
            )
            if not legacy_project.no_profit:
                project.accounting_type = ACCOUNTING_TYPES.PROFIT
                project.save(update_fields=['accounting_type'])
            else:
                project.accounting_type = ACCOUNTING_TYPES.INTERNAL
                project.save(update_fields=['accounting_type'])
            # To save original data in this fields
            Project.objects.filter(id=project.id).update(
                created_at=legacy_project.created,
                updated_at=legacy_project.modified,
            )

    def _process_email_company(self, legacy_person, company):
        """ Proceed legacy company emails for the provided company

        Write errors in the next situations:
        1) When there is legacy email with type Home for the company
        2) When legacy email has unrecognizable type

        :param legacy_person: Person instance
            LegacyPerson with company=True to get data from
        :param company: Company instance
            instance to write emails
        """
        emails = legacy_person.emails.all()
        for email in emails:
            if email.type == "Home":
                sys.stderr.write(
                    f"We can't process type Home for legacy email "
                    f"with pk {email.pk} for legacy person with person_id {legacy_person.person_id} "
                    f"because it is associated with a company with pk {company.pk} not a person! \n"
                )
            elif email.type == "Work":
                headquarter = self._check_headquarter(company, email, legacy_person, "email")
                if headquarter is not None:
                    email = Email.objects.get_or_create(email=email.email)[0]
                    headquarter.email = email
                    headquarter.save(update_fields=["email"])
            else:
                sys.stderr.write(
                    f"Unrecognized type {email.type} for legacy email "
                    f"with pk {email.pk} for legacy person with person_id {legacy_person.person_id}. \n"
                )

    def _process_steps_and_rates(self):
        """ Proceed legacy activities of the first level into the steps and assign rates to those steps

        Write errors in the next situations:
        1) When there is no legacy rate with default rate group exists for the legacy activity

        """
        activities = LegacyActivity.objects.filter(level=1)
        for legacy_activity in activities:
            step = Step.objects.get_or_create(
                name=f"{legacy_activity.name} (imported)",
                deleted=datetime.datetime.now(),
                old_id=legacy_activity.activity_id
            )[0]
            legacy_rates = LegacyRate.objects.filter(rategroup__is_default=True, activity=legacy_activity)
            if not legacy_rates.exists():
                sys.stderr.write(
                    f"No default rate found for the legacy activity with name {legacy_activity.name} "
                    f"and pk {legacy_activity.pk}. Skipping rate creation \n"
                )
                continue
            else:
                for legacy_rate in legacy_rates:
                    Rate.objects.get_or_create(
                        step=step,
                        hourly_rate=legacy_rate.invoicing_hourly_rate,
                        daily_rate=legacy_rate.invoicing_hourly_rate * 8,  # TODO: check if need this in the future
                        currency=legacy_rate.invoicing_currency
                    )

    def _process_allocations(self):
        """ Creates Not set allocation category and dummy allocation
        """
        allocation_category = AllocationCategory.objects.create(name="Not set")
        allocation = Allocation.objects.create(
            public_id=0,
            name="0000",
            category=allocation_category,
        )

    def _process_annual_working_time(self):
        """ Proceed legacy annual working time objects and creates backoffice setting for the WORKDAY_LENGTH_IN_HOURS.
        This creation happen only if data was changed.
        """
        working_times = LegacyAnnualWorkingTime.objects.all()
        last_value = None
        for legacy_time in working_times:
            if last_value != legacy_time.working_time_per_day:
                last_value = legacy_time.working_time_per_day
                available_setting = AvailableSetting.objects.get_or_create(name="WORKDAY_LENGTH_IN_HOURS")[0]
                Setting.objects.create(
                    setting_to_override=available_setting,
                    value="%s" % legacy_time.working_time_per_day,
                    start_date=datetime.date.today()
                )

    def _process_absense(self):
        """ Process legacy absense into the absense objects.

        Write errors in the next situations:
        1) If legacy absense has user which doesn't exists in new db

        """
        absenses = LegacyAbsence.objects.all()
        for legacy_absense in absenses:
            user = User.objects.filter(email=legacy_absense.worker.email).first()
            if user is None:
                sys.stderr.write(
                    f"We can't process legacy absense with pk {legacy_absense.pk} "
                    f"because it's worker with email {legacy_absense.worker.email} doesn't exists "
                    f"as a User in our DB! \n"
                )
                continue
            legacy_reason = legacy_absense.reason
            reason_dict = {'vacation': 'vacation', 'sickness': 'sickness', 'service': 'civil_service',
                           'special': 'other', 'education': 'education', 'overtime': 'overtime', }
            reason = reason_dict.get(legacy_reason)
            absense = Absence(
                reason=reason,
                start_date=legacy_absense.date_from,
                end_date=legacy_absense.date_until,
                start_date_number=1,
                end_date_number=1,
                is_all_day=True,
                note=legacy_absense.notes,
                user=user,
            )
            absense._skip_model_check = True
            absense.save()

    def _process_flexwork(self, name):
        """ Creates default flexwork model for Mn-Fr with given name

        :param name: str

        :returns: FlexWork instance
        """
        flexwork = FlexWork.objects.create(
            given_name=name
        )
        for day_tuple in WEEKDAYS.CHOICES[:5]:  # exclude Saturday + Sunday
            weekday = day_tuple[0]
            WorkingHours.objects.create(flex_work=flexwork, weekday=weekday, from_hour='08:00', to_hour='12:00')
            WorkingHours.objects.create(flex_work=flexwork, weekday=weekday, from_hour='13:00', to_hour='17:00')
        return flexwork

    def _process_worklogs(self, worklog_qs, project_activity):
        """ Process legacy worklogs into ProjectActivityStep and WorkLog objects related to that ProjectActivityStep.
        Each WorkLog accumalates all user hours for that specific ProjectActivityStep. If there wasn't 
        ActivityAssignment for the ProjectActivityStep and user - creates one.

        Write errors in the next situations:
        1) When legacy worklog were created not by worker
        2) When legacy worker doesn't exists in our new db
        3) When legacy worklog satisfaction is > 4

        :param worklog_qs: LegacyWorklog queryset
            queryset for the specific project 
        :param project_activity: Projectactivity instance
            instance to assign ProjectActivityStep to

        """
        start_date = datetime.date.today()
        end_of_the_year = datetime.date.today().replace(month=12, day=31)
        for worklog_entry in worklog_qs:
            legacy_activity = worklog_entry.activity
            # we need process activities here: if it is level-2 we need to use it parent
            if legacy_activity.level == 2:
                legacy_activity = legacy_activity.parent
            step = Step.objects.get_or_create(
                old_id=legacy_activity.activity_id,
                defaults={'name': f"{legacy_activity.name} (imported)", 'deleted': datetime.datetime.now()}
            )[0]
            project_activity_step, created = ProjectActivityStep.objects.get_or_create(
                activity=project_activity,
                start_date=start_date,
                end_date=end_of_the_year,
                step=step,
            )
            if worklog_entry.created_by != worklog_entry.worker:
                sys.stderr.write(
                    f"Legacy worklog with pk {worklog_entry.pk} has different created_by and worker fields. "
                    f"Using worker with email {worklog_entry.worker.email} instead "
                    f"of created_by with email {worklog_entry.created_by.email} \n"
                )
            user = User.objects.filter(email=worklog_entry.worker.email).first()
            if user is None:
                sys.stderr.write(
                    f"Legacy worklog is created for the worker with email {worklog_entry.worker.email}. "
                    "But there is no User in the new metronom with sucn an email! Skipping worklog \n"
                )
            else:
                work_qs = WorkLog.objects.filter(
                    project_activity_step=project_activity_step, worker=user)
                if work_qs.exists():
                    new_worklog = work_qs.first()
                    new_worklog.tracked_time += worklog_entry.hours
                    new_worklog.save(update_fields=["tracked_time"])
                else:
                    satisfaction = worklog_entry.satisfaction
                    satisfaction = WORKLOG_SATISFACTION_DICT.get(satisfaction)
                    if satisfaction is not None and satisfaction > 4:
                        sys.stderr.write(
                            f"Legacy worklog is created with the satisfaction > 4. "
                            "But there is no such satisfaction in the new metronom. Set satisfaction to None \n"
                        )
                        satisfaction = None
                    new_worklog = WorkLog.objects.create(
                        project_activity_step=project_activity_step,
                        worker=user,
                        tracked_time=worklog_entry.hours,
                        work_date=worklog_entry.date,
                        note=worklog_entry.notes,
                        satisfaction=satisfaction if satisfaction is not None else 0,
                    )
                qs = ActivityAssignment.objects.filter(worker=user, project_activity_step=project_activity_step)
                if not qs.exists():
                    assignment = ActivityAssignment.objects.create(
                        worker=user,
                        project_activity_step=project_activity_step
                    )

    def _process_article_entries(self, articles_qs, project):
        """ Process legacy articles into ProjectArticle objects

        :param articles_qs: LegacyArticle queryset
            queryset related to the legacy job to proceed
        :param project: Project instance
            project to assign Article objects to
        """
        allocation = Allocation.objects.get(name="0000")
        article_group = ArticleGroup.objects.get_or_create(
            name="Imported Articles"
        )[0]
        for article_entry in articles_qs:
            legacy_article = article_entry.article
            name = legacy_article.name if legacy_article.level == 1 else legacy_article.parent.name
            ProjectArticle.objects.create(
                project=project,
                deleted=datetime.datetime.now(),
                display_name=f"{name} (imported)",
                budget_in_project_currency=Money(0, project.billing_currency),
                article_group=article_group,
                allocation=allocation,
                description="Imported from old Metronom",
            )

    def _create_and_process_projects_activities_articles_worklog_assignments(self):
        """ This is a general function which called other one for all imported projects:
        1. _process_worklogs
        2. _process_article_entries

        Also this function calculates planned_effort for recently created ProjectActivityStep objects and
        budget_in_project_currency for recently created ProjectArticle objects

        Write errors in the next situations:
        1) When there is no legacy worklogs exists for the specific project
        2) When there is no legacy articles exists for the specific project
        3) When project billing_currency and related legacy article offer currency are different
        """
        projects = Project.objects.all()
        activity_group = ActivityGroup.objects.get_or_create(
            name="Imported from old Metronom"
        )[0]
        start_date = datetime.date.today()
        end_of_the_year = datetime.date.today().replace(month=12, day=31)
        # template
        for project in projects:
            project_activity = ProjectActivity.objects.create(
                project=project,
                deleted=datetime.datetime.now(),
                start_date=start_date,
                end_date=end_of_the_year,
                activity_group=activity_group,
                description="Imported from old Metronom",
                display_name="Imported from old Metronom"
            )
            worklog_qs = LegacyWorklog.objects.filter(job__job_id=project.old_id)
            if not worklog_qs.exists():
                sys.stderr.write(
                    f"No legacy worklog exists for the legacy job_id {project.old_id} \n"
                )
            else:
                self._process_worklogs(worklog_qs, project_activity)
            # now it's time to process articles
            articles_qs = LegacyArticleEntry.objects.filter(job__job_id=project.old_id)
            if not articles_qs.exists():
                sys.stderr.write(
                    f"No legacy articles entries exists for the legacy job_id {project.old_id} \n"
                )
            else:
                self._process_article_entries(articles_qs, project)
            # now we need to calculate planned efforts for the ProjectActivityStep
            qs = ProjectActivityStep.objects.filter(activity__project=project)
            for pa_step in qs:
                legacy_activity = LegacyActivity.objects.get(activity_id=pa_step.step.old_id)
                legacy_acitivites = LegacyActivity.objects.filter(
                    Q(id=legacy_activity.id) | Q(parent_id=legacy_activity.id, level=2)
                )
                planned_effort = LegacyOfferedActivity.objects.filter(
                    joboffer__job__job_id=project.old_id, activity__in=legacy_acitivites)\
                    .aggregate(total_sum=Sum('hours'))['total_sum']
                pa_step.planned_effort = planned_effort
                pa_step.save(update_fields=["planned_effort"])
            # now we need to calculate budget_in_project_currency for the ProjectArticle
            qs = ProjectArticle.objects.filter(project=project)
            for project_article in qs:
                old_name = project_article.name[:-11]  # to exclude (imported) from the string
                legacy_article = LegacyArticle.objects.get(name=old_name, level=1)
                legacy_articles = LegacyArticle.objects.filter(
                    Q(id=legacy_article.id) | Q(parent_id=legacy_article.id, level=2)
                )
                total_cost = 0
                offer_qs = LegacyOfferedArticle.objects.filter(joboffer__job__job_id=project.old_id,
                                                               article__in=legacy_articles)
                for offer in offer_qs:
                    if offer.currency != project.billing_currency:
                        sys.stderr.write(
                            f"We can't add budget_in_project_currency for project article {project_article.id} "
                            f"because legacy offered article with id {offer.id} has currency {offer.currency} "
                            f"while the whole project has {project.billing_currency} currency."
                            f"Skipping calculation for this project article \n"
                        )
                        break
                    total_cost += offer.cost
                else:
                    budget_in_project_currency = Money(total_cost, project.billing_currency)
                    project_article.budget_in_project_currency = budget_in_project_currency or 0
                    project_article.save(update_fields=["budget_in_project_currency"])

    def _process_promotions(self):
        """ Proceed legacy promotions into the Promotion objects in the new db

        Write errors in the next situations:
        1) If there is no such user from legacy promotion exists in the new db
        """
        promotions = LegacyPromotion.objects.exclude(name="Mitarbeiter").all()
        for legacy_promotion in promotions:
            promotion = Promotion.objects.create(
                name=legacy_promotion.name,
            )
            legacy_promoted_people = legacy_promotion.person_members.all()
            for legacy_promoted in legacy_promoted_people:
                current_promoted = Person.objects.filter(
                    public_id=legacy_promoted.person_id
                )
                if current_promoted.exists():
                    current_promoted = current_promoted.first()
                    current_promoted.promotions.add(promotion)
                else:
                    sys.stderr.write(
                        f"Can't assign promotion for the legacy promotion with id {legacy_promotion.id} "
                        f"to the person {legacy_promoted.person_id}. "
                        f"No person with public_id {legacy_promoted.person_id} exists in a new system. "
                        "Maybe this was a company? \n"
                    )

    def _process_legacy_bank_account(self, instance_name):
        """ Process legacy bank accounts into AgencyBankAccount and Bank objects in new db
        """
        legacy_bank_account = LegacyBankAccount.objects.all()
        for legacy_bank in legacy_bank_account:
            AgencyBankAccount.objects.create(
                owner_name=instance_name,
                account_name=legacy_bank.name,
                iban=legacy_bank.iban,
                bic=legacy_bank.bic,
                deleted=datetime.datetime.now() if legacy_bank.archived else None,
                is_default=legacy_bank.is_default
            )
            bank = Bank.objects.create(
                bank_name=legacy_bank.bank,
                bank_code=legacy_bank.bic,
                bic=legacy_bank.bic,
                country_code=legacy_bank.iban[:2]
            )
            bank.created_at = legacy_bank.created
            bank.updated_at = legacy_bank.modified
            bank.save(update_fields=["created_at", "updated_at"])

    def _process_legacy_employments(self):
        """ Process legacy employments into the Contract objects. Also calculates medium_wage_cost, medium_full_cost
        for every contract,

        Write errors in the next situations:
        1) When legacy employment user doesn't exists in new db

        """
        legacy_employments = LegacyEmployment.objects.all()
        for legacy_employment in legacy_employments:
            email = legacy_employment.profile.user.email
            user = User.objects.filter(email=email).first()
            if user is None:
                sys.stderr.write(
                    f"Can't process legacy employment. There is no user with email {email} exists in new system! "
                    "Skipping the employment \n"
                )
                continue
            email_obj = Email.objects.get_or_create(email=email)[0]
            flexwork = FlexWork.objects.get_or_create(given_name=f"Altes Metronom ({legacy_employment.percent}%)",
                                                      defaults=dict(deleted=datetime.datetime.now()))[0]
            contract = Contract.objects.create(start_date=legacy_employment.date,
                                               vacation_days=legacy_employment.vacation_days,
                                               flex_model=flexwork,
                                               user=user
                                               )
            qs = LegacyEmployment.objects.filter(profile=legacy_employment.profile, date__gt=legacy_employment.date)

            def calculate_medium_costs(worklogs):
                """ Internal function for the calculation of the medium costs
                """
                accounting_hourly_rate_wage_costs, accounting_hourly_rate_full_costs = 0, 0
                total_hours = 0
                for worklog in worklogs:
                    total_hours += worklog.hours
                    accounting_hourly_rate_wage_costs += worklog.accounting_hourly_rate_wage_costs * worklog.hours
                    accounting_hourly_rate_full_costs += worklog.accounting_hourly_rate_full_costs * worklog.hours
                medium_wage_cost = accounting_hourly_rate_wage_costs / total_hours
                medium_full_cost = accounting_hourly_rate_full_costs / total_hours
                return medium_wage_cost, medium_full_cost

            if qs.exists():
                # let's get next employment date
                next_employment_date = qs.order_by('date')[0].date
                contract.end_date = next_employment_date - datetime.timedelta(days=1)
                contract.save(update_fields=['end_date'])
                worklogs = LegacyWorklog.objects.filter(
                    worker=legacy_employment.profile.user,
                    date__gte=legacy_employment.date,
                    date__lt=next_employment_date
                )
                medium_wage_cost, medium_full_cost = calculate_medium_costs(worklogs)
            else:
                worklogs = LegacyWorklog.objects.filter(worker=legacy_employment.profile.user)
                medium_wage_cost, medium_full_cost = calculate_medium_costs(worklogs)

            contract.full_costs_hourly = medium_full_cost
            contract.wage_costs_hourly = medium_wage_cost
            contract.save(update_fields=['full_costs_hourly', 'wage_costs_hourly'])

    def _get_or_create_user(self, legacy_instance):
        """ Internal function to get or create User from the provided instance.
        Also creates WorkYear for the 2017 for each user

        :param legacy_instance: LegacyUser instance

        :rtype: tuple
        :returns: (instance, bool)
            (user instance, created)
        """
        user, created = User.objects.using('default').get_or_create(
            email=legacy_instance.email,
            username=legacy_instance.username,
            first_name=legacy_instance.first_name,
            last_name=legacy_instance.last_name,
            is_active=legacy_instance.is_active,
            is_staff=legacy_instance.is_staff,
            is_superuser=legacy_instance.is_superuser,
        )
        WorkYear.objects.get_or_create(
            user=user,
            year=2017,
            vacation_days_moved_from_last_year=0
        )
        return user, created

    def _process_users_and_persons(self, default_password, exclude_person_id):
        """ Proceed legacy persons into the User and Person objects, calling
        _verify_user_email_set_password for every created user

        :param default_password: str
            default password to pass into default_password
        :param exclude_person_id: str
            perso_id in format 0000x to exclude from the proceeding. We need this to proceed this person later
            and make public_id free for default agency company

        :rtype: tuple
        :returns: (list, list, instance/None)
            legacy_persons_and_persons - list with tuples (legacy_person, new_person)
            We need this to proceed with proceed data from them later
            exclude_ids - list with the legacy person_id
            We need this to be sure, that we never proceed the same person twice
            legacy_person_rearrange - None or LegacyPerson instance
            We need this to proceed that instance as the last one and change it public_id
        """
        persons_qs = LegacyPerson.objects.using('legacy').filter(company=False)
        # if we have some company with the person_id equal to our default company we have to move it
        legacy_person_rearrange = persons_qs.filter(person_id=exclude_person_id).first()
        if legacy_person_rearrange is not None:
            persons_qs = persons_qs.exclude(person_id=exclude_person_id)
        users_qs = LegacyUser.objects.using('legacy').all()

        exclude_ids = []
        legacy_persons_and_persons = []
        for legacy_user in users_qs:
            user, created = self._get_or_create_user(legacy_user)
            legacy_person = legacy_user.profile.person
            person = self._update_person(legacy_person, user.person_in_crm, default_password)
            if legacy_person.person_id != exclude_person_id:
                exclude_ids.append(legacy_person.person_id)
                legacy_persons_and_persons.append((legacy_person, person))
            if created:
                user.role = PROFILE_LEVEL_TO_ROLE.get(legacy_user.profile.access_level)
                user.created_at = legacy_user.date_joined
                user.save(update_fields=["created_at"])
                self._verify_user_email_set_password(user, default_password)

        # because of the m2m relations we can't use bulk_create
        for legacy_person in persons_qs.exclude(person_id__in=exclude_ids):
            person = self._create_person(legacy_person, default_password)
            legacy_persons_and_persons.append((legacy_person, person))

        return legacy_persons_and_persons, exclude_ids, legacy_person_rearrange

    def process_company(self, legacy_company, default_password, override_public_id=False):
        """ This is function to create company from the legacy_company

        :param legacy_company: LegacyPerson instance
            company to process
        :param default_password: str
            company to process
        :param override_public_id: bool
            not import public_id but set settings.AGENCY_COMPANY_PUBLIC_ID one

        :rtype: tuple
        :returns: company, bank_account
            company - created Company instance
            bank_account - created BankAccount or None
        """
        sector, bank_account = None, None
        if legacy_company.sector:
            sector = Sector.objects.using('default').get_or_create(name=legacy_company.sector.name)[0]
        if legacy_company.standard_accounting_bank_account:
            legacy_bank_account = legacy_company.standard_accounting_bank_account
            bank_account = BankAccount.objects.using('default').get_or_create(
                bank_name=legacy_bank_account.bank,
                account_name=legacy_bank_account.account,
                bic=legacy_bank_account.bic,
                iban=legacy_bank_account.iban,
            )[0]
        if override_public_id:
            public_id = settings.AGENCY_COMPANY_PUBLIC_ID
        else:
            public_id = legacy_company.person_id
        company = Company.objects.using('default').create(
            public_id=public_id,
            name=legacy_company.name,
            sector=sector,
            billing_currency=legacy_company.master_currency,
            liable_for_taxation=legacy_company.taxation,
            default_payment_within=legacy_company.payment_within,
            billing_delivery=BILLING_DELIVERY.POST,
            billing_interval=BILLING_INTERVAL.PROJECT_BASED,
            billing_agreement=BILLING_AGREEMENT.ACCORDING_TO_OFFER,
        )
        if legacy_company.contact:
            legacy_contact = legacy_company.contact
            key_account_manager, created = self._get_or_create_user(legacy_contact)
            if created:
                key_account_manager.role = PROFILE_LEVEL_TO_ROLE.get(legacy_contact.profile.access_level)
                key_account_manager.created_at = legacy_contact.date_joined
                key_account_manager.save(update_fields=["created_at"])
                self._verify_user_email_set_password(key_account_manager, default_password)
        company.key_account_manager = key_account_manager
        company.save(update_fields=["key_account_manager"])
        return company, bank_account

    def _process_companies(self, default_password, exclude_ids, legacy_person_rearrange,
                           legacy_persons_and_persons, legacy_person_id, default_legacy_person_id):
        """ Proceed legacy persons with company=True into the Company objects. Create Sector object for every
        company from the legacy company. Also process legacy_person_rearrange into the actual object.

        :param default_password: str
        :param exclude_ids: list
            list of the legacy person_id to exclude from processing
        :param legacy_person_rearrange: instance/None
            Person instance to proceed as the last one object or None
        :param legacy_persons_and_persons: list
            list of tuples (legacy_person, person) to add legacy_person_rearrange after processing
        :param legacy_person_id: str
            person_id which we should rearrange because of default agency company creation
        :param default_legacy_person_id: str
            person_id we should exclude because it was processed already

        :rtype: tuple
        :returns:  (list, list, int)
            legacy_persons_and_companies - list of tuples
                (legacy_company, company) for the future processing
            legacy_persons_and_persons - list of tuples
                (legacy_person, person) for the future processing
            rearranged_public_id - public_id of the default agency company
        """
        companies_qs = LegacyPerson.objects.using('legacy').filter(
            company=True).exclude(person_id=default_legacy_person_id)
        # if we have some company with the person_id equal to our default company we have to move it
        legacy_company_rearrange = companies_qs.filter(person_id=legacy_person_id).first()
        if legacy_company_rearrange is not None:
            exclude_ids.append(legacy_company_rearrange.person_id)
        legacy_persons_and_companies = []

        # now it's time to create default company
        key_account_manager = User.objects.filter(is_superuser=True).first()
        Company.objects.filter(public_id=settings.AGENCY_COMPANY_PUBLIC_ID).update(
            key_account_manager=key_account_manager)

        for legacy_company in companies_qs.exclude(person_id__in=exclude_ids):
            company, bank_account = self.process_company(legacy_company, default_password)
            if bank_account:
                company.bank_accounts.add(bank_account)
            legacy_persons_and_companies.append((legacy_company, company))
            self._process_address_person(legacy_company, company)

        rearranged_public_id = None
        if legacy_company_rearrange is not None:
            company, bank_account = self.process_company(legacy_company_rearrange,
                                                         default_password, override_public_id=True)
            if bank_account:
                company.bank_accounts.add(bank_account)
            legacy_persons_and_companies.append((legacy_company_rearrange, company))
            rearranged_public_id = company.public_id
            self._process_address_person(legacy_company_rearrange, company)
        if legacy_person_rearrange is not None:
            person = self._create_person(legacy_person_rearrange, default_password, override_public_id=True)
            legacy_persons_and_persons.append((legacy_person_rearrange, person))
            rearranged_public_id = person.public_id
        return legacy_persons_and_companies, legacy_persons_and_persons, rearranged_public_id

    def _process_campaign_offers(self, exclude_person_id, rearranged_public_id):
        """ Process legacy campaign offer objects into CampaignOffer objects

        Write errors in the next situations:
        1) If there is no offer date in the legacy data
        2) If there is no contact in the legacy data
        3) If there is no campaign with such an old_id in new data

        :param exclude_person_id: str
            person_id of the rearranged item
        :param rearranged_public_id: int
            public_id of the rearranged item

        """
        qs = LegacyCampaignOffer.objects.all()
        terms_collection = TermsCollection.objects.get_or_create(name="Imported from old metronom")[0]
        for legacy_campaign_offer in qs:
            campaign = None
            if legacy_campaign_offer.project:
                old_id = legacy_campaign_offer.project.project_id
                campaign = Campaign.objects.filter(old_id=old_id).first()
                if campaign is None:
                    sys.stderr.write(
                        f"Legacy ProjectOffer with id {legacy_campaign_offer.id} has project with "
                        f"project_id {old_id} but we don't have such Campaign in new data! Skipping \n"
                    )
                    continue
            offer_manager = User.objects.get(email=legacy_campaign_offer.manager.email)
            if legacy_campaign_offer.contact:
                legacy_contact = legacy_campaign_offer.contact.person
                if legacy_contact.person_id == exclude_person_id:
                    person_id = rearranged_public_id
                else:
                    person_id = legacy_contact.person_id
                person = Person.objects.get(public_id=person_id)
                if not legacy_campaign_offer.offer_date or not legacy_campaign_offer.valid_until:
                    sys.stderr.write(
                        f"Legacy ProjectOffer with id {legacy_campaign_offer.id} has no offer_date "
                        f"or valid_until but this fields are required for the CampaignOffer! Skipping \n"
                    )
                    continue
                campaign_offer = CampaignOffer.objects.create(
                    offer_date=legacy_campaign_offer.offer_date,
                    callback_date=legacy_campaign_offer.offer_date,
                    terms_collection=terms_collection,
                    valid_until_date=legacy_campaign_offer.valid_until,
                    offer_manager=offer_manager,
                    contact_person=person,
                    title=legacy_campaign_offer.name,
                    description=legacy_campaign_offer.description,
                    campaign_status=CAMPAIGN_OFFER_STATUS_DICT.get(legacy_campaign_offer.state),
                    campaign=campaign,
                    number=legacy_campaign_offer._offer_id[2:]
                )
            else:
                sys.stderr.write(
                    f"Legacy ProjectOffer with id {legacy_campaign_offer.id} has no contact "
                    f"but contact_person is required for the CampaignOffer! Skipping \n"
                )

    def _process_offers(self, instance_url, exclude_person_id, rearranged_public_id):
        """ Process legacy offer objects into Offer objects

        Write errors in the next situations:
        1) If there is no offer_date in the legacy data
        2) If there is no contact in the legacy data
        3) If there is no job in the legacy data to get the project from

        :param instance_url: str
            url to use, when we build link to the former object in the legacy platform
        :param exclude_person_id: str
            person_id of the rearranged item
        :param rearranged_public_id: int
            public_id of the rearranged item

        """
        qs = LegacyOffer.objects.all()
        terms_collection = TermsCollection.objects.get_or_create(name="Imported from old metronom")[0]
        for legacy_offer in qs:
            legacy_description = legacy_offer.description
            additional_string = ("<p><em>(</em>Importiert von <a href='{0}offers/{1}/' "
                                 "target='_blank'>{0}offers/{1}/</a>)</p>"
                                 .format(instance_url, legacy_offer._offer_id)
                                 )
            legacy_project = legacy_offer.job
            if legacy_project:
                offer_manager = User.objects.get(email=legacy_project.manager.email)
                project = Project.objects.get(old_id=legacy_project.job_id)
                offer_manager = User.objects.get(email=legacy_project.manager.email)
                description = f"{legacy_description} {additional_string}"
                if legacy_offer.contact:
                    legacy_contact = legacy_offer.contact.person
                    if legacy_contact.person_id == exclude_person_id:
                        person_id = rearranged_public_id
                    else:
                        person_id = legacy_contact.person_id
                    person = Person.objects.get(public_id=person_id)
                    if not legacy_offer.offer_date or not legacy_offer.valid_until:
                        sys.stderr.write(
                            f"Legacy JobOffer with id {legacy_offer.id} has no offer_date "
                            f"or valid_until but this fields are required for the CampaignOffer! Skipping \n"
                        )
                        continue
                    offer = Offer.objects.create(
                        project=project,
                        is_optional=legacy_offer.optional,
                        name=legacy_offer.name,
                        offer_description=description,
                        number=legacy_offer._offer_id[2:],
                        offer_status=OFFER_STATUS_DICT.get(legacy_offer.state),
                        offer_date=legacy_offer.offer_date,
                        valid_until_date=legacy_offer.valid_until,
                        callback_date=legacy_offer.offer_date,
                        offer_manager=offer_manager,
                        contact_person=person,
                        terms_collection=terms_collection,
                    )
                    if legacy_offer.projectoffer:
                        campaign_offer = CampaignOffer.objects.get(
                            number=legacy_offer.projectoffer._offer_id[2:]
                        )
                        campaign_offer.project_offers.add(offer)
                else:
                    sys.stderr.write(
                        f"Legacy JobOffer with id {legacy_offer.id} has no contact "
                        f"but contact_person is required for the Offer! Skipping \n"
                    )
            else:
                sys.stderr.write(
                    f"Legacy JobOffer with id {legacy_offer.id} has no job to find the related project "
                    f"in new system, but project is required for the Offer! Skipping \n"
                )
