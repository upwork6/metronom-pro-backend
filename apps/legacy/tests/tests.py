import pytest

from django.core.management import call_command
from django.test import TestCase

from apps.companies.models import Company
from apps.legacy.models import Person as LegacyPerson
from apps.persons.models import Person


@pytest.mark.skip("This test are unworkable because of migrations issue -"
                  "no FK for the legacy detected by makemigrations")
class TestCommand(TestCase):

    @classmethod
    def setUpTestData(cls):
        LegacyPerson.objects.using("legacy").create(
            name="Name",
            company=False
        )
        LegacyPerson.objects.using("legacy").create(
            name="Name2",
            company=True
        )

    def test_import_legacy_command(self):
        """ Be aware that this test runs only if you have some data in your legacy db

        """
        persons_before = Person.objects.count()
        legacy_persons_before = LegacyPerson.objects.using('legacy').filter(company=False).count()
        call_command("import_legacy_data")
        self.assertEqual(persons_before + legacy_persons_before, Person.objects.count())
