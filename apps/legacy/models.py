# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

import datetime
import random
import re
import string
from decimal import Decimal

from django.contrib.contenttypes.fields import (GenericForeignKey,
                                                GenericRelation)
from django.core.validators import (MaxValueValidator, MinValueValidator,
                                    RegexValidator)
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator
from django.core.validators import MinValueValidator, MaxValueValidator


NUMBER_RE = re.compile(r'^([\d\+]+)@?')
COUNTRY_SUB_RE = re.compile(r'^(00)')
INLAND_SUB_RE = re.compile(r'^(0)')


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    user = models.ForeignKey('AuthUser', models.DO_NOTHING)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjangoSite(models.Model):
    domain = models.CharField(max_length=100)
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'django_site'


class Currency(object):
    CHF = 'CHF'
    EUR = 'EUR'
    USD = 'USD'

    CHOICES = (
        (CHF, CHF),
        (EUR, EUR),
        (USD, USD),
    )


def generate_apikey(length=40):
    return u''.join([random.choice(string.letters + string.digits) for i in range(length)])


class AccountingRateMixin(models.Model):
    """
    These fields are used to determine whether a job has been successful in
    terms of paid and received money.
    """

    accounting_currency = models.CharField(_('Accounting currency'), max_length=3, choices=Currency.CHOICES,
                                           default=Currency.CHF)
    accounting_hourly_rate_full_costs = models.DecimalField(_('Hourly rate by full cost calculation'), max_digits=10,
                                                            decimal_places=2)
    accounting_hourly_rate_wage_costs = models.DecimalField(_('Hourly rate by wage cost calculation'), max_digits=10,
                                                            decimal_places=2)

    class Meta:
        abstract = True


class CreateUpdateModel(models.Model):
    """
    Store timestamps for creation and last modification.
    """

    created = models.DateTimeField(_('Created'), auto_now_add=True)
    modified = models.DateTimeField(_('Modified'), auto_now=True)

    class Meta:
        abstract = True
        get_latest_by = 'created'
        ordering = ('created',)


class InvoiceBase(CreateUpdateModel):
    IN_PREPARATION = 10
    SENT = 20
    PAID = 30
    DELCREDERE = 35
    REPLACED = 40

    STATE_CHOICES = (
        (IN_PREPARATION, _('In preparation')),
        (SENT, _('Sent')),
        (PAID, _('Paid')),
        (DELCREDERE, _('Delcredere')),
        (REPLACED, _('Replaced')),
    )

    invoice_id = models.CharField(_('Invoice ID'), max_length=50,
                                  unique=True)

    reference_number = models.CharField(_('reference number'), max_length=50,
                                        unique=True)

    bankaccount = models.ForeignKey('BankAccount', verbose_name=_('Bank account'),
                                    limit_choices_to={'archived': False})

    created_by = models.ForeignKey('AuthUser', verbose_name=_('Created by'))
    manager = models.ForeignKey('AuthUser', related_name='%(class)ss',
                                verbose_name=_('Our reference'))  # related_name='invoices'

    contact = models.ForeignKey('Address', related_name='%(class)ss', blank=True,
                                null=True, verbose_name=_('Contact person'),
                                help_text=_('You may override the address entry used for the invoice address block.'))

    invoice_date = models.DateField(_('Invoice date'), default=datetime.date.today)
    payment_within = models.IntegerField(_('Payment within'),
                                         # default=default_payment_within
                                         )
    due_date = models.DateField(_('Due date'), default=datetime.date.today, editable=False)

    subtotal = models.DecimalField(_('Subtotal'), max_digits=10,
                                   decimal_places=2, default=Decimal('0.00'))
    discount = models.DecimalField(_('Discount'), max_digits=10,
                                   decimal_places=2, default=Decimal('0.00'))

    discount_in_percent = models.DecimalField(
        _('discount in percent'), max_digits=10, decimal_places=2,
        blank=True, null=True)

    tax = models.DecimalField(_('Tax'), max_digits=10, decimal_places=2,
                              #   default=default_tax_rate,
                              help_text=_('Set tax rate to 0.0 to create an invoice without a tax line.'))
    total = models.DecimalField(_('Total'), max_digits=10, decimal_places=2,
                                default=Decimal('0.00'))

    state = models.IntegerField(_('State'), choices=STATE_CHOICES,
                                default=IN_PREPARATION)

    name = models.CharField(_('Invoice name'), max_length=100)
    description = models.TextField(_('Invoice description'), blank=True)

    notes = models.TextField(_('Notes'), blank=True)

    thank_you_line = models.CharField(_('Thank you line'), max_length=100,
                                      blank=True,
                                      help_text=_('Will be shown above the bank account informations.'))

    class Meta:
        abstract = True


class InvoiceUpdateBase(CreateUpdateModel):
    created_by = models.ForeignKey('AuthUser', verbose_name=_('Created by'))
    notes = models.TextField(_('Notes'), blank=True)
    state = models.IntegerField(_('State'), choices=InvoiceBase.STATE_CHOICES,
                                default=InvoiceBase.IN_PREPARATION)

    date = models.DateField(_('Date'), default=datetime.date.today)

    class Meta:
        abstract = True


class PieceCostMixin(models.Model):
    """
    This mixin provides fields for piece costs.
    """

    currency = models.CharField(
        _('Currency'), max_length=3,
        choices=Currency.CHOICES, default=Currency.CHF
    )

    cost = models.DecimalField(_('Cost excl. tax'), max_digits=10, decimal_places=2)
    commission = models.DecimalField(_('Commission'), max_digits=5, decimal_places=2)

    down_payment = models.BooleanField(_('Down payment'), default=False)

    class Meta:
        abstract = True


class InvoicingRateMixin(models.Model):
    """
    These fields store the rate which is used for the invoices.
    """

    invoicing_currency = models.CharField(
        _('Invoicing currency'), max_length=3,
        choices=Currency.CHOICES, default=Currency.CHF)
    invoicing_hourly_rate = models.DecimalField(
        _('Hourly rate for invoicing'),
        max_digits=10, decimal_places=2)

    class Meta:
        abstract = True


class OfferBase(CreateUpdateModel):
    name = models.CharField(_('Offer name'), max_length=100)
    description = models.TextField(_('Offer description'))
    comments = models.TextField(_('Additional text'), blank=True,
                                help_text=_('Will be shown after the offer total.'))
    notes = models.TextField(_('Notes'), blank=True)
    offer_date = models.DateField(_('Offer date'), blank=True, null=True,
                                  help_text=_('The date the offer has been sent to the customer.'))
    valid_until = models.DateField(_('Valid until'), blank=True, null=True)
    inquiry_date = models.DateField(_('Inquire state on'), blank=True, null=True)
    closed_date = models.DateField(_('Closed on'), blank=True, null=True,
                                   help_text=_('The date this particular offer has been accepted or rejected.'))
    footer_texts = models.ManyToManyField('FooterText', blank=True, verbose_name=_('Footer texts')
                                          )
    manager = models.ForeignKey('AuthUser', related_name='%(class)ss',
                                verbose_name=_('Manager'))
    customer = models.ForeignKey('Person', related_name='%(class)ss',
                                 verbose_name=_('Customer'))
    contact = models.ForeignKey('Address', related_name='%(class)ss', blank=True,
                                null=True, verbose_name=_('Contact person'))
    subtotal = models.DecimalField(_('Subtotal'), max_digits=10,
                                   decimal_places=2, default=Decimal('0.00'))
    discount = models.DecimalField(_('Discount'), max_digits=10,
                                   decimal_places=2, default=Decimal('0.00'))
    tax = models.DecimalField(_('Tax'), max_digits=10, decimal_places=2,
                              #   default=default_tax_rate)
                              )
    total = models.DecimalField(_('Total'), max_digits=10, decimal_places=2,
                                default=Decimal('0.00'))

    class Meta:
        abstract = True


class Call(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    local = models.CharField(max_length=100)
    remote = models.CharField(max_length=100)
    call_type = models.CharField(max_length=20)

    class Meta:
        get_latest_by = 'created'
        ordering = ['-created']
        verbose_name = _('Call')
        verbose_name_plural = _('Calls')
        managed = False
        db_table = 'accounts_call'


class Employment(models.Model):
    profile = models.ForeignKey('Profile', verbose_name=_('Profile'))
    date = models.DateField(_('Date'))
    percent = models.IntegerField(_('Percent'),
                                  help_text=_('Use 0 if employment is not fixed.'))
    vacation_days = models.DecimalField(_('vacation days'),
                                        max_digits=4, decimal_places=2)

    class Meta:
        managed = False
        db_table = 'accounts_employment'
        get_latest_by = 'date'
        ordering = ['date']
        unique_together = (('profile', 'date'),)
        verbose_name = _('Employment')
        verbose_name_plural = _('Employments')


class Group(models.Model):
    number = models.IntegerField(_('number'), unique=True,
                                 validators=[
                                     MinValueValidator(1),
                                     MaxValueValidator(99),
    ])
    name = models.CharField(_('name'), max_length=100)
    ordering = models.IntegerField(_('Ordering'), default=0)
    manager = models.ForeignKey('Profile', verbose_name=_('Manager'), related_name='+')

    class Meta:
        managed = False
        db_table = 'accounts_group'


class Note(CreateUpdateModel):
    name = models.CharField(_('Title'), max_length=100)
    notes = models.TextField(_('Notes'), blank=True)
    content_type = models.ForeignKey(DjangoContentType, models.DO_NOTHING)
    object_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'notes_note'


def has_notes(self):
    return Note.has_note(self)


class Person(CreateUpdateModel):
    GERMAN = 'de'
    FRENCH = 'fr'
    ITALIAN = 'it'
    ENGLISH = 'en'

    LANGUAGE_CHOICES = (
        (GERMAN, _('German')),
        (FRENCH, _('French')),
        (ITALIAN, _('Italian')),
        (ENGLISH, _('English')),
    )

    GENDER_CHOICES = (
        ('f', _('female')),
        ('m', _('male')),
    )

    person_id = models.CharField(_('Person ID'), max_length=10, unique=True)
    name = models.CharField(_('Name'), max_length=300)
    first_name = models.CharField(_('First name'), max_length=100, blank=True, default='')
    gender = models.CharField(_('Gender'), max_length=2, choices=GENDER_CHOICES,
                              blank=True)

    first_name_terms = models.BooleanField(_('First name terms'), default=False,
                                           help_text=_('Whether the company or the contact is on first name terms with this person.'))

    company = models.BooleanField(_('Company'), default=False)

    letter_addressing = models.CharField(max_length=250, blank=True,
                                         verbose_name=_('letter addressing'))

    # fields relevant for non-companies
    manner_of_address = models.CharField(_('Manner of address'), max_length=30,
                                         blank=True, default='', help_text=_('e.g. Mr., Ms., Dr.'))
    title = models.CharField(_('Title/auxiliary'), max_length=100, blank=True,
                             default='', help_text=_('e.g. MSc ETH'))
    language = models.CharField(_('Language'), max_length=2,
                                choices=LANGUAGE_CHOICES, default=GERMAN)
    dob = models.DateField(_('Date of birth'), blank=True, null=True)
    organization_unit_code = models.CharField(_('Organization unit code'), max_length=30,
                                              blank=True)

    # fields relevant only for companies
    sector = models.ForeignKey('Sector', verbose_name=_('Sector'), on_delete=models.SET_NULL,
                               null=True, blank=True, default=None, help_text=_('The sector of the company'))
    master_currency = models.CharField(_('Master currency'), max_length=3,
                                       choices=Currency.CHOICES, default=Currency.CHF,
                                       help_text=_('This is the currency in which we work for the customer.'))
    taxation = models.BooleanField(_('Taxation'), default=True,
                                   help_text=_('Should the default tax be filled in when generating invoices?'))

    debitor_number = models.CharField(_('debitor number'), max_length=30,
                                      blank=True)

    standard_accounting_bank_account = models.ForeignKey('BankAccount',
                                                         verbose_name=_('Bank account for accounting'), null=True, blank=True, default=None,
                                                         help_text=_('Default Bank account for accounting'))

    standard_rate_group = models.ForeignKey('RateGroup',
                                            verbose_name=_('Rate group'), null=True, blank=True, default=None,
                                            help_text=_('This rate group is used as default'))

    payment_within = models.IntegerField(_('Individual Payment term'), blank=True, null=True,
                                         help_text=_('Individual payment term in days - standard value is used if not set'))

    # additional fields
    bank_account = models.TextField(_('Bank account'), blank=True, default='')
    notes = models.TextField(_('Notes'), blank=True)

    contact = models.ForeignKey('AuthUser', verbose_name=_('Contact'),
                                help_text=_('Preferred contact person'))
    groups = models.ManyToManyField(Group, blank=True, related_name='person_members',
                                    verbose_name=_('Groups'))

    sorting_field = models.CharField(_('Sorting field'), max_length=50, blank=True,
                                     editable=False)

    protected = models.BooleanField(_('Protected'), default=False,
                                    help_text=_('This person should not be visible to employees and freelancers.'))
    archived = models.BooleanField(_('Archived'), default=False,
                                   help_text=_('This person should not appear in the rest of the system.'))

    # cannot call them 'notes', because that field already exists
    has_notes = has_notes

    # fields related to sales_regions feature
    sales_region = models.CharField(_('Sales region'), max_length=3,
                                    blank=True)
    rd = models.IntegerField(_('RD'), blank=True, null=True)
    general_agency = models.IntegerField(_('General agency'), blank=True,
                                         null=True)
    agency = models.IntegerField(_('Agency'), blank=True, null=True)
    agency_manager = models.CharField(_('Agency manager'), max_length=100,
                                      blank=True)

    class Meta:
        managed = False
        db_table = 'addressbook_person'


class Profile(CreateUpdateModel, AccountingRateMixin):
    CUSTOMER = 10
    FREELANCER = 20
    EMPLOYEE = 30
    ADMINISTRATION = 35
    MANAGEMENT = 40
    NOT_SET = 50

    LEVEL_CHOICES = (
        (MANAGEMENT, _('Management')),
        (ADMINISTRATION, _('back office')),
        (EMPLOYEE, _('Employee')),
        (FREELANCER, _('Freelancer')),
        (CUSTOMER, _('Customer')),
    )

    user = models.OneToOneField('AuthUser', verbose_name=_('Worker'))
    person = models.OneToOneField(Person, verbose_name=_('Person'))

    notes = models.TextField(_('Notes'), blank=True)

    access_level = models.IntegerField(_('Access level'), choices=LEVEL_CHOICES)

    apikey = models.CharField(_('API Key'), max_length=40, unique=True,
                              default=generate_apikey)
    snom_url = models.CharField(_('snom URL'), max_length=50, blank=True)

    language = models.CharField(_('Language'), max_length=10, blank=True)

    percent = models.IntegerField(_('Percent'), default=100,
                                  help_text=_('Use 0 if employment is not fixed.'))
    group = models.ForeignKey(Group, related_name='profile_members', verbose_name=_('Team'),
                              blank=True, null=True)

    notification_by_mail = models.BooleanField(_('Notification by mail'),
                                               help_text=_('Receive notifications by e-mail.'), default=True)
    key_data_by_mail = models.BooleanField(_('Key data by mail'), default=False)

    class Meta:
        managed = False
        db_table = 'accounts_profile'


class Newsletter(models.Model):
    RECAPITULATION = 1
    OVERVIEW_OLD_JOBS = 2

    NEWSLETTER_TEMPLATES = (
        (RECAPITULATION, 'last_week_personal_report'),
        (OVERVIEW_OLD_JOBS, 'last_week_personal_report'),
    )

    NEWSLETTER_CHOICES = (
        (RECAPITULATION, _(u'Personal Report')),
        (OVERVIEW_OLD_JOBS, _(u'Overview of old jobs / not closed projects')),
    )

    WEEKDAYS = (
        (101, _('First of month')),
        (115, _('15th of month')),
        (125, _('25th of month')),
        (0, _('Monday')),
        (1, _('Tuesday')),
        (2, _('Wednesday')),
        (3, _('Thursday')),
        (4, _('Friday')),
        (5, _('Saturday')),
        (6, _('Sunday')),
    )

    HOURS = (
        (3, _('3am')),
        (6, _('6am')),
        (9, _('9am')),
        (12, _('12am')),
        (15, _('15am')),
        (18, _('18am')),
        (21, _('21am')),
    )

    group = models.ForeignKey(Group, verbose_name=_('Group'), related_name="newsletters")
    access_level = models.IntegerField(_('Access level'), choices=Profile.LEVEL_CHOICES)

    type = models.IntegerField(_('Newsletter type'), choices=NEWSLETTER_CHOICES)
    weekday = models.IntegerField(_('Weekday'), choices=WEEKDAYS)
    hour = models.IntegerField(_('Hour'), choices=HOURS, default=None, null=True, blank=True)

    last_sent = models.DateTimeField(_('Last sent'), default=None, null=True, blank=True)

    class Meta:
        verbose_name = _('Newsletter')
        verbose_name_plural = _('Newsletter')
        managed = False
        db_table = 'accounts_newsletter'


class AccountspayableAccount(models.Model):
    parent = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    account_id = models.CharField(unique=True, max_length=10)
    name = models.CharField(max_length=100)
    currency = models.CharField(max_length=3)
    notes = models.TextField()
    archived = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'accountspayable_account'


class AccountspayableEntry(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    created_by = models.ForeignKey('AuthUser', models.DO_NOTHING, related_name="+")
    responsible = models.ForeignKey('AuthUser', models.DO_NOTHING, related_name="+")
    job = models.ForeignKey('Job', models.DO_NOTHING, blank=True, null=True)
    supplier = models.CharField(max_length=100)
    invoice_id = models.CharField(max_length=100)
    invoice_date = models.DateField()
    due_date = models.DateField()
    notes = models.TextField()
    account = models.ForeignKey(AccountspayableAccount, models.DO_NOTHING)
    list = models.ForeignKey('AccountspayableList', models.DO_NOTHING, blank=True, null=True)
    to_pay_total = models.DecimalField(max_digits=10, decimal_places=2)
    invoice_total = models.CharField(max_length=100)
    approved_on = models.DateField(blank=True, null=True)
    paid_on = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'accountspayable_entry'


class AccountspayableList(models.Model):
    account = models.ForeignKey(AccountspayableAccount, models.DO_NOTHING)
    name = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'accountspayable_list'


class Address(CreateUpdateModel):
    person = models.ForeignKey('Person', related_name='addresses',
                               verbose_name=_('Person'))
    type = models.ForeignKey('AddressType', verbose_name=_('Type'),
                             on_delete=models.PROTECT)
    employer = models.ForeignKey('Person', related_name='employees',
                                 limit_choices_to={'company': True}, blank=True, null=True,
                                 verbose_name=_('Employer / Company'))

    main_address = models.BooleanField(_('Main address'), default=False)
    dispatch_address = models.BooleanField(_('dispatch address'), default=False)

    function = models.TextField(_('Function'), blank=True)
    address = models.TextField(_('Address'), blank=True)

    zip_code = models.CharField(_('ZIP Code'), max_length=100, blank=True)
    city = models.CharField(_('City'), max_length=100, blank=True)

    region = models.CharField(_('Region'), max_length=100, blank=True)
    country = models.CharField(_('Country'), max_length=100, blank=True,
                               default='CH')

    notes = models.TextField(_('Notes'), blank=True)

    active = models.BooleanField(_('Active'), default=True)

    full_override = models.TextField(_('Full override'), blank=True,
                                     help_text=_('Use the following content as letter address.'))

    class Meta:
        managed = False
        db_table = 'addressbook_address'


class AddressType(CreateUpdateModel):
    name = models.CharField(max_length=250, unique=True)

    class Meta:
        managed = False
        db_table = 'addressbook_addresstype'


class Email(CreateUpdateModel):
    person = models.ForeignKey('Person', related_name='emails',
                               verbose_name=_('Person'))
    type = models.CharField(_('Type'), max_length=20,
                            default='Work')
    email = models.EmailField(_('e-mail address'))

    class Meta:
        managed = False
        db_table = 'addressbook_email'


class AddressbookGroup(models.Model):
    name = models.CharField(max_length=100)
    generally_relevant = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'addressbook_group'


class IMContact(CreateUpdateModel):
    person = models.ForeignKey('Person', related_name='im_contacts',
                               verbose_name=_('Person'))
    type = models.CharField(_('Type'), max_length=20)
    contact = models.CharField(_('Contact'), max_length=100)

    class Meta:
        managed = False
        db_table = 'addressbook_imcontact'


class PersonGroups(models.Model):
    person = models.ForeignKey(Person, models.DO_NOTHING)
    group = models.ForeignKey(AddressbookGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'addressbook_person_groups'
        unique_together = (('person', 'group'),)


class PhoneNumber(CreateUpdateModel):
    person = models.ForeignKey(Person, related_name='phone_numbers',
                               verbose_name=_('Person'))
    type = models.CharField(_('Type'), max_length=20,
                            default='Work')
    number = models.CharField(_('Number'), max_length=30, blank=True,
                              help_text=_('Example: +41555111141'))

    class Meta:
        managed = False
        db_table = 'addressbook_phonenumber'


class Sector(CreateUpdateModel):
    name = models.CharField(max_length=100, unique=True)
    is_default = models.BooleanField(_('Is default'), help_text=_(
        'Default sector, if no other is suitable'), default=False)

    class Meta:
        verbose_name = _('Sector')
        verbose_name_plural = _('Sectors')
        ordering = ('is_default', 'name',)
        managed = False
        db_table = 'addressbook_sector'


class AddressbookSynchronization(models.Model):
    date = models.DateTimeField()
    handler_config_key = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'addressbook_synchronization'


class AddressbookSynclink(models.Model):
    person = models.ForeignKey(Person, models.DO_NOTHING)
    handler_config_key = models.CharField(max_length=20)
    identifier = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'addressbook_synclink'


class URL(CreateUpdateModel):
    person = models.ForeignKey(Person, related_name='urls',
                               verbose_name=_('Person'))
    type = models.CharField(_('Type'), max_length=20,
                            default='Website')
    url = models.URLField(_('URL'))

    class Meta:
        managed = False
        db_table = 'addressbook_url'


class ServiceBase(models.Model):
    INVOICE_DETAIL_GROUP = 10
    INVOICE_DETAIL_ITEMS = 20
    INVOICE_DETAIL_FULL = 30

    INVOICE_DETAIL_DEFAULT = INVOICE_DETAIL_GROUP

    INVOICE_DETAIL_CHOICES = (
        (INVOICE_DETAIL_GROUP, _(u'group')),
        (INVOICE_DETAIL_ITEMS, _(u'all items')),
        (INVOICE_DETAIL_FULL, _(u'full details')),
    )

    SERVICE_CATEGORY_SERVICE = 10
    SERVICE_CATEGORY_PRODUCTION = 20
    SERVICE_CATEGORY_EXTERN = 30

    SERVICE_CATEGORY_CHOICES = (
        (SERVICE_CATEGORY_SERVICE, _(u'Provision of services')),
        (SERVICE_CATEGORY_PRODUCTION, _(u'Production service')),
        (SERVICE_CATEGORY_EXTERN, _(u'External service')),
    )

    name = models.CharField(_('Name'), max_length=100)
    notes = models.TextField(_('Notes'), blank=True)
    parent = models.ForeignKey('self', related_name='children', blank=True, null=True,
                               verbose_name=_('Parent'), limit_choices_to={'level__lte': 1})
    archived = models.BooleanField(_('Archived'), default=False)

    default_invoice_detail = models.PositiveIntegerField(choices=INVOICE_DETAIL_CHOICES,
                                                         blank=True, null=True,
                                                         verbose_name=_(u"invoice detail"))

    service_category = models.PositiveIntegerField(choices=SERVICE_CATEGORY_CHOICES,
                                                   blank=True, null=True,
                                                   verbose_name=_(u"service category"))

    class Meta:
        abstract = True


class Activity(ServiceBase):
    service_id_attr = 'activity_id'
    activity_id = models.CharField(_('Activity ID'), max_length=10, unique=True)
    lft = models.IntegerField()
    rght = models.IntegerField()
    tree_id = models.IntegerField()
    level = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'articles_activity'


class Article(ServiceBase):
    service_id_attr = 'article_id'
    article_id = models.CharField(_('Article ID'), max_length=10, unique=True)
    lft = models.IntegerField()
    rght = models.IntegerField()
    tree_id = models.IntegerField()
    level = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'articles_article'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class BiddingBid(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    invitation = models.ForeignKey('BiddingInvitation', models.DO_NOTHING)
    bidder = models.ForeignKey(Address, models.DO_NOTHING)
    code = models.CharField(unique=True, max_length=50)
    currency = models.CharField(max_length=3)
    bid = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    comments = models.TextField()
    state = models.IntegerField()
    rating = models.IntegerField()
    notes = models.TextField()

    class Meta:
        managed = False
        db_table = 'bidding_bid'


class BiddingInvitation(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    invitation_id = models.CharField(unique=True, max_length=10)
    name = models.CharField(max_length=100)
    description = models.TextField()
    notes = models.TextField()
    manager = models.ForeignKey(AuthUser, models.DO_NOTHING)
    state = models.IntegerField()
    open_from = models.DateField()
    open_until = models.DateField()
    awarded_to = models.ForeignKey(BiddingBid, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bidding_invitation'


class BiddingInvitationcontext(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    object_id = models.IntegerField()
    invitation = models.ForeignKey(BiddingInvitation, models.DO_NOTHING)
    context = models.TextField()

    class Meta:
        managed = False
        db_table = 'bidding_invitationcontext'


class BiddingQuestion(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    invitation = models.ForeignKey(BiddingInvitation, models.DO_NOTHING)
    posed_by = models.CharField(max_length=75)
    question = models.TextField()
    is_published = models.BooleanField()
    are_notified = models.BooleanField()
    answer = models.TextField()

    class Meta:
        managed = False
        db_table = 'bidding_question'


class PaymentListImport(CreateUpdateModel):
    PENDING = 10
    FAILED = 20
    SUCCESS = 40

    STATE_CHOICES = (
        (PENDING, _('pending')),
        (FAILED, _('failed')),
        (SUCCESS, _('success')),
    )

    v11_file = models.FileField(
        _('v11 file'), upload_to='paymentlistimports/',
        # TODO storage=protected_storage,
    )
    log = models.TextField(_('log'), blank=True)
    state = models.SmallIntegerField(
        _('state'), choices=STATE_CHOICES, default=PENDING)
    import_created_on = models.DateField(
        _('import created on'), default=datetime.date.today)

    class Meta:
        managed = False
        db_table = 'bookkeeping_paymentlistimport'


def _upload_to(instance, filename):
    return (u'%s.%s/%s/%s' % (
        instance.content_type.app_label,
        instance.content_type.model,
        instance.object_id,
        str(filename),
    )).lower()


class File(CreateUpdateModel):
    name = models.CharField(_('Name'), max_length=100)

    file = models.FileField(_('File'),
                            upload_to=_upload_to,
                            max_length=500,
                            help_text=_('Files bigger than 10 MB will be ignored.'))

    created_by = models.ForeignKey(AuthUser,
                                   verbose_name=_('Created by'),
                                   related_name='+')
    content_type = models.ForeignKey(DjangoContentType, models.DO_NOTHING)
    object_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'filevault_file'


class ActivityInvoiceDetail(models.Model):
    activity = models.ForeignKey(Activity,
                                 limit_choices_to={'level': 1})
    jobinvoice = models.ForeignKey('JobInvoice')

    invoice_detail = models.PositiveIntegerField(
        choices=Activity.INVOICE_DETAIL_CHOICES,
        verbose_name=_(u"invoice detail"))

    class Meta:
        managed = False
        db_table = 'invoicing_activityinvoicedetail'
        unique_together = (('activity', 'jobinvoice'),)


class ArticleInvoiceDetail(models.Model):
    article = models.ForeignKey(Article,
                                limit_choices_to={'level': 1})
    jobinvoice = models.ForeignKey('JobInvoice')

    invoice_detail = models.PositiveIntegerField(
        choices=Article.INVOICE_DETAIL_CHOICES,
        verbose_name=_(u"invoice detail"))

    class Meta:
        managed = False
        db_table = 'invoicing_articleinvoicedetail'
        unique_together = (('article', 'jobinvoice'),)


class BankAccount(CreateUpdateModel):
    name = models.CharField(_('Name'), max_length=200)
    bank = models.TextField(_('Bank'), blank=True)
    account = models.CharField(_('Account number'), max_length=200)
    clearing_nr = models.CharField(_('Clearing number'), max_length=10)
    iban = models.CharField(_('IBAN-No.'), max_length=35)
    bic = models.CharField(_('BIC/SWIFT'), max_length=20)
    cost_center = models.CharField(_('Cost center'), max_length=100, blank=True)
    belongs_to = models.ForeignKey(
        Group, blank=True, null=True, verbose_name=_('Belongs to'),
        help_text=_('The group which mainly works for this specific bank account.'))

    archived = models.BooleanField(_('Archived'), default=False)
    is_default = models.BooleanField(_('Is default'), help_text=_(
        'Default bank account, if none is set for the user'), default=False)

    esr_member_id = models.CharField(
        _('ESR member id'), max_length=30, blank=True,
        validators=[
            RegexValidator('\d{2}-\d{6}-\d{1}',
                           _('please enter number in format XX-XXXXXX-X')),
            # validate_esr_number,
        ])

    postfinance = models.BooleanField(_('PostFinance'), default=False)

    esr_internal_number = models.CharField(
        _('internal member number'), max_length=6, blank=True,
    )

    class Meta:
        managed = False
        db_table = 'invoicing_bankaccount'


class InterpolatedArticleEntry(CreateUpdateModel, PieceCostMixin):
    invoice = models.ForeignKey('JobInvoice', related_name='articleentries', verbose_name=_('Invoice'))
    created_by = models.ForeignKey('AuthUser', verbose_name=_('Created by'))
    date = models.DateField(_('Date'), default=datetime.date.today)
    article = models.ForeignKey(Article, verbose_name=_('Article'))
    count = models.IntegerField(_('Count'))

    notes = models.TextField(_('Notes'))

    class Meta:
        managed = False
        db_table = 'invoicing_interpolatedarticleentry'


class InterpolatedWorklogEntry(CreateUpdateModel, InvoicingRateMixin):
    invoice = models.ForeignKey('JobInvoice', related_name='worklogentries',
                                verbose_name=_('Invoice'))
    created_by = models.ForeignKey(AuthUser, verbose_name=_('Created by'))
    worker = models.ForeignKey(AuthUser, related_name='interpolated_worklogentries',
                               verbose_name=_('Worker'))

    date = models.DateField(_('Date'), default=datetime.date.today)
    activity = models.ForeignKey(Activity, verbose_name=_('Activity'))
    hours = models.DecimalField(_('Hours'), max_digits=10, decimal_places=1)

    notes = models.TextField(_('Notes'))

    class Meta:
        managed = False
        db_table = 'invoicing_interpolatedworklogentry'


class JobInvoice(InvoiceBase):
    DOWN_PAYMENT_INVOICE = 1
    SINGLE_INVOICE = 2
    FINAL_INVOICE = 3

    TYPE_CHOICES = (
        (DOWN_PAYMENT_INVOICE, _('down payment invoice')),
        (SINGLE_INVOICE, _('Invoice')),
        (FINAL_INVOICE, _('final invoice')),
    )

    NO_INFO = None
    NO_VAT = 1
    VAT_INCLUDED = 2
    REVERSE_CHARGE = 3

    ADDITIONAL_TAX_INFORMATION_CHOICES = (
        (NO_INFO, _('-------------------------')),
        (NO_VAT, _('Services excluded from the VAT according to article 18.')),
        (VAT_INCLUDED, _('The total included 8% VAT.')),
        (REVERSE_CHARGE, _('The tax liability is shifted to the bill recipient article 13b UST, Reverse-Charge-System.')),
    )

    _invoice_id = models.CharField(_('Invoice ID'), max_length=10)

    invoice_type = models.IntegerField(verbose_name=_('Rechnungstyp'), choices=TYPE_CHOICES,
                                       default=SINGLE_INVOICE)

    job = models.ForeignKey('Job', verbose_name=_('Job'), related_name='invoices')
    projectinvoice = models.ForeignKey('ProjectInvoice', blank=True, null=True,
                                       related_name='jobinvoices', verbose_name=_('Project invoice'))

    show_work_performed_time = models.BooleanField(
        _('show work performed timespan'),
        default=True,
    )

    show_hour_count = models.BooleanField(
        _('show hour count'),
        default=True,
    )

    work_performed_from = models.DateField(_('Work performed from'),
                                           blank=True, null=True,
                                           help_text=_('Set these two dates if you want to override the '
                                                       'automatically determined worklog period.'))
    work_performed_until = models.DateField(_('Work performed until'),
                                            blank=True, null=True)

    activity_detail = models.ManyToManyField(Article,
                                             through='ArticleInvoiceDetail',
                                             blank=True)

    article_detail = models.ManyToManyField(Activity,
                                            through='ActivityInvoiceDetail',
                                            blank=True)

    additional_tax_information = models.IntegerField(verbose_name=_('Additional tax information'),
                                                     choices=ADDITIONAL_TAX_INFORMATION_CHOICES,
                                                     default=NO_INFO, null=True, blank=True)

    class Meta:
        managed = False
        db_table = 'invoicing_jobinvoice'
        unique_together = (('job', '_invoice_id'),)


class JobInvoiceUpdate(InvoiceUpdateBase):
    invoice = models.ForeignKey(JobInvoice, related_name='updates', verbose_name=_('Invoice'))

    class Meta:
        managed = False
        db_table = 'invoicing_jobinvoiceupdate'


class ProjectInvoice(InvoiceBase):
    """ Invoice Object for Projects, basicly the same like the Job """
    _invoice_id = models.CharField(_('Invoice ID'), max_length=10)
    project = models.ForeignKey('Project', blank=True, null=True,
                                verbose_name=_('Project'), related_name='invoices')

    invoice_type = 4  # fixed for projectinvoices

    class Meta:
        managed = False
        db_table = 'invoicing_projectinvoice'
        unique_together = (('project', '_invoice_id'),)


class ProjectInvoiceUpdate(InvoiceUpdateBase):
    invoice = models.ForeignKey(ProjectInvoice, related_name='updates', verbose_name=_('Invoice'))

    class Meta:
        managed = False
        db_table = 'invoicing_projectinvoiceupdate'


class Job(CreateUpdateModel):
    OPEN = 10
    STANDBY = 15
    FINISHED = 20
    CLOSED = 30

    STATE_CHOICES = (
        (OPEN, _('Open')),
        #(STANDBY, _('Standby')),
        (FINISHED, _('Closed, but not ready for invoicing yet')),
        (CLOSED, _('Closed')),
    )

    job_id = models.CharField(_('Job ID'), max_length=16,
                              unique=True)
    created_by = models.ForeignKey(AuthUser, verbose_name=_('Created by'))
    name = models.CharField(_('Job name'), max_length=100)
    description = models.TextField(_('Job description'), blank=True)
    notes = models.TextField(_('Notes'), blank=True)
    customer = models.ForeignKey(Person, related_name='jobs',
                                 verbose_name=_('Customer/Company'))
    contact = models.ForeignKey(Address, related_name='jobs', blank=True, null=True,
                                verbose_name=_('Contact person'))
    project = models.ForeignKey('Project', related_name='jobs', blank=True, null=True,
                                verbose_name=_('Project'))
    state = models.IntegerField(_('State'), choices=STATE_CHOICES,
                                default=OPEN)
    manager = models.ForeignKey(AuthUser, related_name='managed_jobs',
                                verbose_name=_('Manager'))
    workers = models.ManyToManyField(AuthUser, through='JobWorker', related_name='jobs',
                                     verbose_name=_('Workers'))
    no_profit = models.BooleanField(_('No profit'), default=False,
                                    help_text=_('No invoices will be generated for this job.'))
    prepayment = models.BooleanField(_('Prepayment'), default=False,
                                     help_text=_('The customer pays in advance.'))
    ongoing = models.BooleanField(_('Maintenance'), default=False,
                                  help_text=_('This is a maintenance job.'))
    archived_on = models.CharField(_('Archived on'), max_length=100, blank=True,
                                   help_text=_('Free-form text field to describe the place where you\'ve archived the data for this job.'))
    rategroup = models.ForeignKey('RateGroup', related_name='jobs',
                                  verbose_name=_('Rate group'))
    invoicing_rate_override = models.DecimalField(_('Invoicing rate override'),
                                                  max_digits=10, decimal_places=2, blank=True, null=True,
                                                  help_text=_('Override all invoicing rates for this object.'))
    master_currency = models.CharField(_('Master currency'), max_length=3,
                                       choices=Currency.CHOICES, default=Currency.CHF,
                                       help_text=_('This is the currency in which we work for the customer.'))
    # cannot call them 'notes', because that field already exists
    has_notes = has_notes
    scheduled_hours = models.DecimalField(_('Scheduled hours'), blank=True, null=True,
                                          max_digits=10, decimal_places=1,
                                          help_text=_('Scheduled hours for this job. Is not taken into account when job is connected to a job offer.'))

    class Meta:
        managed = False
        db_table = 'jobs_job'


class JobUpdate(CreateUpdateModel):
    job = models.ForeignKey(Job, related_name='updates', verbose_name=_('Job'))
    created_by = models.ForeignKey(AuthUser, verbose_name=_('Created by'))
    state = models.IntegerField(_('State'), choices=Job.STATE_CHOICES,
                                help_text=_('Setting the state to "closed" will put this job into the list of jobs ready to be charged for '
                                            '(if it isn\'t a no profit job).'))

    class Meta:
        managed = False
        db_table = 'jobs_jobupdate'


class JobWorker(models.Model):
    job = models.ForeignKey(Job, verbose_name=_('Job'))
    user = models.ForeignKey(AuthUser, verbose_name=_('Worker'))

    class Meta:
        managed = False
        db_table = 'jobs_jobworker'


class OldID(CreateUpdateModel):
    old_id = models.CharField(_('Old ID'), max_length=16, unique=True)
    content_type = models.ForeignKey(DjangoContentType, models.DO_NOTHING)
    object_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'jobs_oldid'


class Project(CreateUpdateModel):
    project_id = models.CharField(_('Project ID'), max_length=16,
                                  unique=True)
    name = models.CharField(_('Project name'), max_length=100)
    description = models.TextField(_('Project description'), blank=True)
    notes = models.TextField(_('Notes'), blank=True)
    manager = models.ForeignKey(AuthUser, related_name='managed_projects',
                                verbose_name=_('Manager'))
    customer = models.ForeignKey(Person, verbose_name=_('Customer'),
                                 related_name='projects')
    contact = models.ForeignKey(Address, related_name='projects', blank=True, null=True,
                                verbose_name=_('Contact person'))
    archived = models.BooleanField(_('Archived'), default=False)
    has_notes = has_notes

    class Meta:
        managed = False
        db_table = 'jobs_project'


class LegalizeAcceptance(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    job = models.ForeignKey(Job, models.DO_NOTHING)
    # Field renamed because it started with '_'.
    field_acceptance_id = models.CharField(db_column='_acceptance_id', max_length=10)
    name = models.CharField(max_length=100)
    description = models.TextField()
    notes = models.TextField()
    acceptance_notes = models.TextField()
    acceptance_date = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'legalize_acceptance'


class Confirmation(CreateUpdateModel):
    joboffer = models.ForeignKey('JobOffer', related_name='confirmations',
                                 verbose_name=_('Job offer'))
    _confirmation_id = models.CharField(_('Confirmation ID'), max_length=10)
    confirmation_notes = models.TextField(_('Confirmation notes'), blank=True)
    confirmation_date = models.DateField(_('Confirmation date'), blank=True, null=True,
                                         help_text=_('Date of signature.'))
    subtotal = models.DecimalField(_('Subtotal'), max_digits=10,
                                   decimal_places=2, default=Decimal('0.00'))
    discount = models.DecimalField(_('Discount'), max_digits=10,
                                   decimal_places=2, default=Decimal('0.00'))
    tax = models.DecimalField(_('Tax'), max_digits=10, decimal_places=2,
                              #   default=default_tax_rate
                              )
    total = models.DecimalField(_('Total'), max_digits=10, decimal_places=2,
                                default=Decimal('0.00'))

    class Meta:
        managed = False
        db_table = 'legalize_confirmation'


class LivesettingsLongsetting(models.Model):
    group = models.CharField(max_length=100)
    key = models.CharField(max_length=100)
    value = models.TextField()
    site = models.ForeignKey(DjangoSite, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'livesettings_longsetting'
        unique_together = (('site', 'group', 'key'),)


class LivesettingsSetting(models.Model):
    group = models.CharField(max_length=100)
    key = models.CharField(max_length=100)
    value = models.CharField(max_length=255)
    site = models.ForeignKey(DjangoSite, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'livesettings_setting'
        unique_together = (('site', 'group', 'key'),)


class Notification(models.Model):
    created = models.DateTimeField(_('Created'), default=datetime.datetime.now)
    recipient = models.ForeignKey(AuthUser, verbose_name=_('Recipient'))
    message = models.TextField(_('Message'), blank=True)
    link = models.CharField(_('Link'), blank=True, max_length=200)
    is_seen = models.BooleanField(_('Is seen'), default=False)

    class Meta:
        managed = False
        db_table = 'notifications_notification'


class FooterText(models.Model):
    notes = models.TextField(_('Notes'), blank=True)
    default = models.BooleanField(_('Activated by default'), default=False)
    ordering = models.IntegerField(_('Ordering'), default=0)

    class Meta:
        managed = False
        db_table = 'offers_footertext'


class JobOffer(OfferBase):
    IN_PREPARATION = 10
    SENT = 20
    ACCEPTED = 30
    REJECTED = 40
    REPLACED = 50

    STATE_CHOICES = (
        (IN_PREPARATION, _('In preparation')),
        (SENT, _('Sent')),
        (ACCEPTED, _('Accepted')),
        (REJECTED, _('Rejected')),
        (REPLACED, _('Replaced')),
    )
    _offer_id = models.CharField(_('Offer ID'), max_length=10)
    projectoffer = models.ForeignKey('ProjectOffer', related_name='joboffers', verbose_name=_('Project offer'))
    job = models.ForeignKey(Job, blank=True, null=True, verbose_name=_('Job'))
    state = models.IntegerField(_('State'), choices=STATE_CHOICES, default=IN_PREPARATION)
    optional = models.BooleanField(_('Optional'), help_text=_('Will be shown separately and not be added to the total.'),
                                   default=False)
    show_details = models.BooleanField(_('Show details'), help_text=_('Show hourly rates and hours in PDF.'),
                                       default=False)
    rategroup = models.ForeignKey('RateGroup', related_name='joboffers',
                                  verbose_name=_('Rate group'))
    invoicing_rate_override = models.DecimalField(_('Invoicing rate override'),
                                                  max_digits=10, decimal_places=2, blank=True, null=True,
                                                  help_text=_('Override all invoicing rates for this object.'))

    class Meta:
        managed = False
        db_table = 'offers_joboffer'
        unique_together = (('projectoffer', '_offer_id'),)


class OfferedActivity(CreateUpdateModel, InvoicingRateMixin):
    joboffer = models.ForeignKey(JobOffer, related_name='activities', verbose_name=_('Job offer'))
    activity = models.ForeignKey(Activity, related_name='offer_offeredactivities', verbose_name=_('Activity'))
    hours = models.DecimalField(_('Hours'), max_digits=10, decimal_places=1)
    optional = models.BooleanField(_('Optional'),
                                   help_text=_('Will be shown separately and not be added to the total.'),
                                   default=False)
    external = models.BooleanField(_('External'),
                                   help_text=_('This activity will be fulfilled by an external service provider.'),
                                   default=False)
    included = models.BooleanField(_('Included'), default=False, help_text=_('Included in created job.'))
    notes = models.TextField(_('Notes'), blank=True)
    ordering = models.IntegerField(_('Ordering'), default=0)
    confirmation = models.ForeignKey('Confirmation', related_name='activities',
                                     verbose_name=_('Confirmation'), blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'offers_offeredactivity'


class OfferedArticle(CreateUpdateModel, PieceCostMixin):
    joboffer = models.ForeignKey(JobOffer, related_name='articles', verbose_name=_('Job offer'))
    article = models.ForeignKey(Article, related_name='offer_offeredarticles', verbose_name=_('Article'))
    count = models.IntegerField(_('Count'))
    optional = models.BooleanField(_('Optional'),
                                   help_text=_('Will be shown separately and not be added to the total.'),
                                   default=False)
    included = models.BooleanField(_('Included'), default=False,
                                   help_text=_('Included in created job.'))
    notes = models.TextField(_('Notes'), blank=True)
    ordering = models.IntegerField(_('Ordering'), default=0)
    confirmation = models.ForeignKey('Confirmation', related_name='articles',
                                     verbose_name=_('Confirmation'), blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'offers_offeredarticle'


class ProjectOffer(OfferBase):
    IN_PREPARATION = 10
    SENT = 20
    PARTIALLY_CLOSED = 25
    ACCEPTED = 30
    REJECTED = 40

    STATE_CHOICES = (
        (IN_PREPARATION, _('In preparation')),
        (SENT, _('Sent')),
        (PARTIALLY_CLOSED, _('Partially closed')),
        (ACCEPTED, _('Accepted')),
        (REJECTED, _('Rejected / ignored')),
    )

    _offer_id = models.CharField(_('Offer ID'), max_length=10, unique=True)
    project = models.ForeignKey(Project, blank=True, null=True, verbose_name=_('Project'))
    state = models.IntegerField(_('State'), choices=STATE_CHOICES, default=IN_PREPARATION)
    include_tax = models.BooleanField(_('Include tax?'), default=False)
    variants = models.BooleanField(_('Variants?'), default=False,
                                   help_text=_('The job offers are different variants, no total should be shown for the project offer.'))
    template = models.BooleanField(_('Offer template'), default=False, help_text=_('Is an offer template.'))

    class Meta:
        managed = False
        db_table = 'offers_projectoffer'


class ProjectOfferFooterTexts(models.Model):
    projectoffer = models.ForeignKey(ProjectOffer, models.DO_NOTHING)
    footertext = models.ForeignKey(FooterText, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'offers_projectoffer_footer_texts'
        unique_together = (('projectoffer', 'footertext'),)


class ProcessJobprocess(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    # Field renamed because it started with '_'.
    field_process_id = models.CharField(db_column='_process_id', max_length=10)
    process = models.ForeignKey('ProcessProcess', models.DO_NOTHING)
    job = models.ForeignKey(Job, models.DO_NOTHING)
    state = models.ForeignKey('ProcessState', models.DO_NOTHING)
    name = models.CharField(max_length=100)
    notes = models.TextField()
    start_date = models.DateField()
    closed_date = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'process_jobprocess'
        unique_together = (('job', 'field_process_id'),)


class ProcessJobprocessstep(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    jobprocess = models.ForeignKey(ProcessJobprocess, models.DO_NOTHING)
    step = models.ForeignKey('ProcessStep', models.DO_NOTHING)
    ticket = models.OneToOneField('Ticket', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'process_jobprocessstep'


class ProcessProcess(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    process_id = models.CharField(unique=True, max_length=10)
    name = models.CharField(max_length=100)
    notes = models.TextField()
    active = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'process_process'


class ProcessState(models.Model):
    process = models.ForeignKey(ProcessProcess, models.DO_NOTHING)
    name = models.CharField(max_length=100)
    job_state = models.IntegerField(blank=True, null=True)
    ordering = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'process_state'


class ProcessStep(models.Model):
    process = models.ForeignKey(ProcessProcess, models.DO_NOTHING)
    state = models.ForeignKey(ProcessState, models.DO_NOTHING)
    name = models.CharField(max_length=1000)
    offset = models.IntegerField()
    estimated_hours = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'process_step'


class ProductionEdition(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    edition_id = models.CharField(unique=True, max_length=50)
    production = models.ForeignKey('ProductionProduction', models.DO_NOTHING)
    jobprocess = models.OneToOneField(ProcessJobprocess, models.DO_NOTHING, blank=True, null=True)
    number_of_copies = models.IntegerField()
    delivery_date = models.DateField()
    invoice_address = models.ForeignKey(Address, models.DO_NOTHING, blank=True, null=True, related_name="+")
    delivery_address_1 = models.ForeignKey(Address, models.DO_NOTHING, blank=True, null=True, related_name="+")
    number_of_copies_1 = models.IntegerField(blank=True, null=True)
    delivery_address_2 = models.ForeignKey(Address, models.DO_NOTHING, blank=True, null=True, related_name="+")
    number_of_copies_2 = models.IntegerField(blank=True, null=True)
    delivery_address_3 = models.ForeignKey(Address, models.DO_NOTHING, blank=True, null=True, related_name="+")
    number_of_copies_3 = models.IntegerField(blank=True, null=True)
    cost_center = models.CharField(max_length=100)
    notes = models.TextField()
    old_stock = models.CharField(max_length=20)
    color_matching = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'production_edition'


class ProductionProduction(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    production_id = models.CharField(unique=True, max_length=50)
    shipment_id = models.CharField(max_length=50)
    name = models.CharField(max_length=250)
    description = models.TextField()
    notes = models.TextField()
    language = models.CharField(max_length=100)
    type = models.ForeignKey('ProductionProductiontype', models.DO_NOTHING, blank=True, null=True)
    cost_center = models.CharField(max_length=100)
    reorder_level = models.IntegerField(blank=True, null=True)
    manager = models.ForeignKey(AuthUser, models.DO_NOTHING)
    customer = models.ForeignKey(Person, models.DO_NOTHING)
    contact = models.ForeignKey(Address, models.DO_NOTHING, blank=True, null=True)
    process = models.ForeignKey(ProcessProcess, models.DO_NOTHING)
    archived = models.BooleanField()
    specification = models.ForeignKey('SpecificationsSpecification', models.DO_NOTHING)
    latest_edition = models.ForeignKey(ProductionEdition, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'production_production'


class ProductionProductionfield(models.Model):
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=30)
    choices = models.TextField()
    help_text = models.CharField(max_length=100)
    required = models.BooleanField()
    ordering = models.IntegerField()
    field = models.ForeignKey('SpecificationsSpecificationfield', models.DO_NOTHING, blank=True, null=True)
    value = models.TextField()
    parent = models.ForeignKey(ProductionProduction, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'production_productionfield'


class ProductionProductioninquiry(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    production = models.ForeignKey(ProductionProduction, models.DO_NOTHING)
    state = models.IntegerField()
    code = models.CharField(unique=True, max_length=50)
    current_stock = models.IntegerField(blank=True, null=True)
    description = models.TextField()
    answer_expected_until = models.DateField()
    answer = models.CharField(max_length=30)
    number_of_copies = models.CharField(max_length=100)
    postpone_to_date = models.DateField(blank=True, null=True)
    postpone_to_date_ticket = models.ForeignKey('Ticket', models.DO_NOTHING, blank=True, null=True)
    postpone_to_stock = models.IntegerField(blank=True, null=True)
    notes = models.TextField()
    attachment = models.CharField(max_length=100, blank=True, null=True)
    archived = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'production_productioninquiry'


class ProductionProductiontype(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    name = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'production_productiontype'


class Product(models.Model):
    ONETIME = 'onetime'
    YEARLY = 'yearly'
    QUARTERLY = 'quarterly'
    MONTHLY = 'monthly'
    PERIOD_CHOICES = (
        (ONETIME, _('One time')),
        (YEARLY, _('Yearly')),
        (QUARTERLY, _('Quarterly')),
        (MONTHLY, _('Monthly')),
    )
    product_id = models.CharField(_('Product ID'), max_length=10, unique=True,)
    #   default=find_next_product_id)
    name = models.CharField(_('Name'), max_length=100)
    notes = models.TextField(_('Notes'), blank=True)
    default_period = models.CharField(_('Default periodicity'), max_length=10, choices=PERIOD_CHOICES)
    bankaccount = models.ForeignKey(BankAccount, verbose_name=_('Bank account'))

    class Meta:
        managed = False
        db_table = 'products_product'


class ProductArticle(CreateUpdateModel):
    product = models.ForeignKey(Product, verbose_name=_('Product'), related_name='articles')
    article = models.ForeignKey(Article, verbose_name=_('Article'), related_name='productarticles')
    count = models.IntegerField(_('Count'))
    notes = models.TextField(_('Notes'), blank=True)

    class Meta:
        managed = False
        db_table = 'products_productarticle'


class ProductLease(CreateUpdateModel):
    _lease_id = models.CharField(_('Lease ID'), max_length=10)
    product = models.ForeignKey(Product, verbose_name=_('Product'), related_name='productleases')
    customer = models.ForeignKey(Person, verbose_name=_('Customer'), related_name='productleases')
    contact = models.ForeignKey(Address, related_name='productleases', blank=True, null=True,
                                verbose_name=_('Contact person'))
    manager = models.ForeignKey(AuthUser, verbose_name=_('Manager'), related_name='productleases')
    name = models.CharField(_('Name'), max_length=100)
    notes = models.TextField(_('Notes'), blank=True)
    start_date = models.DateField(_('Start date'), default=datetime.date.today)
    period = models.CharField(_('Periodicity'), max_length=10, choices=Product.PERIOD_CHOICES)
    end_date = models.DateField(_('End date'), blank=True, null=True)
    rategroup = models.ForeignKey('RateGroup', related_name='productleases', verbose_name=_('Rate group'))
    currency = models.CharField(_('Currency'), max_length=3, choices=Currency.CHOICES, default=Currency.CHF)

    class Meta:
        managed = False
        db_table = 'products_productlease'


class ProductLeaseArticle(CreateUpdateModel, PieceCostMixin):
    productlease = models.ForeignKey(ProductLease, verbose_name=_('Product lease'), related_name='articles')
    article = models.ForeignKey(Article, verbose_name=_('Article'), related_name='productleasearticles')
    count = models.IntegerField(_('Count'))
    notes = models.TextField(_('Notes'), blank=True)

    class Meta:
        managed = False
        db_table = 'products_productleasearticle'


class ProductLeaseEntry(CreateUpdateModel):
    productlease = models.ForeignKey(ProductLease, related_name='entries', verbose_name=_('Product lease'))
    pending = models.BooleanField(_('Pending'), default=True)
    notes = models.TextField(_('Notes'), blank=True)
    date = models.DateField(_('Date'))

    class Meta:
        managed = False
        db_table = 'products_productleaseentry'
        unique_together = (('productlease', 'date'),)


class InvoicingRate(InvoicingRateMixin):
    activity = models.ForeignKey(Activity, related_name='rates', verbose_name=_('Activity'))
    rategroup = models.ForeignKey('RateGroup', related_name='invoicingrates', verbose_name=_('Rate group'))

    class Meta:
        managed = False
        db_table = 'rates_invoicingrate'
        unique_together = (('activity', 'rategroup', 'invoicing_currency'),)


class PieceCost(PieceCostMixin):
    article = models.ForeignKey(Article, related_name='costs', verbose_name=_('Article'),
                                limit_choices_to={'level': 2})

    class Meta:
        managed = False
        db_table = 'rates_piececost'
        unique_together = (('article', 'currency'),)


class RateGroup(CreateUpdateModel):
    name = models.CharField(_('Name'), max_length=100)
    notes = models.TextField(_('Notes'), blank=True)
    archived = models.BooleanField(_('Archived'), default=False)
    is_default = models.BooleanField(_('Is default'), help_text=_('Default rate group, if none is set for the user'),
                                     default=False)

    class Meta:
        managed = False
        db_table = 'rates_rategroup'


class UserInvoicingRate(InvoicingRateMixin):
    user = models.ForeignKey(AuthUser, related_name='rates', verbose_name=_('User'))
    rategroup = models.ForeignKey(RateGroup, related_name='userinvoicingrates', verbose_name=_('Rate group'))

    class Meta:
        managed = False
        db_table = 'rates_userinvoicingrate'
        unique_together = (('user', 'rategroup', 'invoicing_currency'),)


class Interaction(CreateUpdateModel):
    contact = models.ForeignKey(Address, related_name='interactions', verbose_name=_('Contact person'))
    created_by = models.ForeignKey(AuthUser, verbose_name=_('Created by'), related_name='+')
    worker = models.ForeignKey(AuthUser, verbose_name=_('Worker'), related_name='interactions')
    date = models.DateField(_('Date'), default=datetime.date.today)
    recall_date = models.DateField(_('Recall date'), blank=True, null=True)
    recall_worker = models.ForeignKey(AuthUser, verbose_name=_('Responsible'), related_name='+', blank=True, null=True)
    recall_ticket = models.ForeignKey('Ticket', verbose_name=_('Recall ticket'),
                                      related_name='+', blank=True, null=True)
    notes = models.TextField(_('Notes'))

    class Meta:
        managed = False
        db_table = 'relations_interaction'


class Absence(CreateUpdateModel):
    VACATION = 'vacation'
    SICKNESS = 'sickness'
    SERVICE = 'service'
    SPECIAL = 'special'
    EDUCATION = 'education'
    OVERTIME = 'overtime'

    REASON_CHOICES = (
        (VACATION, _('Vacation')),
        (SICKNESS, _('Sickness')),
        (SERVICE, _('Maternity, civil or military service')),
        (EDUCATION, _('Further education, school')),
        (SPECIAL, _('Special (death, relocation, etc.)')),
        (OVERTIME, _('Overtime correction')),
    )

    worker = models.ForeignKey(AuthUser, related_name='absences', verbose_name=_('Worker'))
    date_from = models.DateField(_('Date from'))
    date_until = models.DateField(_('Date until'))
    days = models.DecimalField(_('Days'), max_digits=5, decimal_places=2)
    reason = models.CharField(_('Reason'), max_length=16, choices=REASON_CHOICES)
    notes = models.TextField(_('Notes'))

    class Meta:
        managed = False
        db_table = 'resources_absence'


class AnnualWorkingTime(models.Model):
    MONTHS = ('january', 'february', 'march', 'april', 'may', 'june', 'july',
              'august', 'september', 'october', 'november', 'december')
    year = models.PositiveIntegerField(_('year'), unique=True)
    working_time_per_day = models.DecimalField(_('working time per day'), max_digits=6, decimal_places=2)
    january = models.DecimalField(_('days in january'), max_digits=6, decimal_places=2)
    february = models.DecimalField(_('days in february'), max_digits=6, decimal_places=2)
    march = models.DecimalField(_('days in march'), max_digits=6, decimal_places=2)
    april = models.DecimalField(_('days in april'), max_digits=6, decimal_places=2)
    may = models.DecimalField(_('days in may'), max_digits=6, decimal_places=2)
    june = models.DecimalField(_('days in june'), max_digits=6, decimal_places=2)
    july = models.DecimalField(_('days in july'), max_digits=6, decimal_places=2)
    august = models.DecimalField(_('days in august'), max_digits=6, decimal_places=2)
    september = models.DecimalField(_('days in september'), max_digits=6, decimal_places=2)
    october = models.DecimalField(_('days in october'), max_digits=6, decimal_places=2)
    november = models.DecimalField(_('days in november'), max_digits=6, decimal_places=2)
    december = models.DecimalField(_('days in december'), max_digits=6, decimal_places=2)

    class Meta:
        managed = False
        db_table = 'resources_annualworkingtime'


class SouthMigrationhistory(models.Model):
    app_name = models.CharField(max_length=255)
    migration = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'south_migrationhistory'


class SpecificationsSpecification(models.Model):
    name = models.CharField(max_length=100)
    notes = models.TextField()

    class Meta:
        managed = False
        db_table = 'specifications_specification'


class SpecificationsSpecificationfield(models.Model):
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=30)
    choices = models.TextField()
    help_text = models.CharField(max_length=100)
    required = models.BooleanField()
    ordering = models.IntegerField()
    specification = models.ForeignKey(SpecificationsSpecification, models.DO_NOTHING)
    group = models.ForeignKey('SpecificationsSpecificationfieldgroup', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'specifications_specificationfield'


class SpecificationsSpecificationfieldgroup(models.Model):
    specification = models.ForeignKey(SpecificationsSpecification, models.DO_NOTHING)
    name = models.CharField(max_length=100)
    ordering = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'specifications_specificationfieldgroup'


class Ticket(CreateUpdateModel):
    PUBLIC = 10
    TEAM = 20
    PRIVATE = 30

    VISIBILITY_CHOICES = (
        (PUBLIC, _('Public')),
        (TEAM, _('Team')),
        (PRIVATE, _('Private')),
    )

    job = models.ForeignKey('Job', related_name='tickets', blank=True, null=True, verbose_name=_('Job'))
    contact = models.ForeignKey('Address', related_name='tickets', blank=True, null=True,
                                verbose_name=_('Contact'))
    ticket_id = models.CharField(_('Ticket ID'), max_length=10)
    notes = models.CharField(_('Ticket name'), max_length=1000)
    created_by = models.ForeignKey(AuthUser, verbose_name=_('Created by'))
    assigned_to = models.ForeignKey(AuthUser, related_name='tickets', verbose_name=_('Assigned to'))
    important = models.BooleanField(_('Top priority'), default=False)
    is_open = models.BooleanField(_('Is open'), default=True)
    due = models.DateField(_('Due'), blank=True, null=True)
    estimated_hours = models.DecimalField(_('Estimated hours'), max_digits=10, decimal_places=2, blank=True, null=True)
    visibility = models.IntegerField(_('Visibility'), choices=VISIBILITY_CHOICES, default=TEAM)

    class Meta:
        managed = False
        db_table = 'tickets_ticket'


class TicketUpdate(CreateUpdateModel):
    ticket = models.ForeignKey('Ticket', related_name='updates', verbose_name=_('Ticket'))
    created_by = models.ForeignKey(AuthUser, verbose_name=_('Created by'))
    assigned_to = models.ForeignKey(AuthUser, related_name='assigned_tickets', verbose_name=_('Assigned to'))
    is_open = models.BooleanField(_('Is open'), default=False)
    comments = models.TextField(_('Comments'), blank=True)

    class Meta:
        managed = False
        db_table = 'tickets_ticketupdate'


class ArticleEntry(CreateUpdateModel, PieceCostMixin):
    created_by = models.ForeignKey(AuthUser, verbose_name=_('Created by'))
    job = models.ForeignKey(Job, related_name='articleentries', verbose_name=_('Job'))
    date = models.DateField(_('Date'), default=datetime.date.today)
    article = models.ForeignKey(Article, verbose_name=_('Article'),
                                help_text=_('The fields below will be filled automatically with default values.'))
    count = models.IntegerField(_('Count'), default=1)
    notes = models.TextField(_('Notes'), blank=True)
    archived = models.BooleanField(_('Archived'), default=False)
    invoice = models.ForeignKey('JobInvoice', blank=True, null=True,
                                verbose_name=_('JobInvoice'), related_name='job_articleentries')
    # fields related to sales_regions feature
    sales_region_company = models.ForeignKey(Person, verbose_name=_('Sales region company'),
                                             blank=True, null=True, related_name='+', on_delete=models.SET_NULL)
    sales_region = models.CharField(_('Sales region'), max_length=3, blank=True)
    rd = models.IntegerField(_('RD'), blank=True, null=True)
    general_agency = models.IntegerField(_('General agency'), blank=True, null=True)
    agency = models.IntegerField(_('Agency'), blank=True, null=True)
    agency_manager = models.CharField(_('Agency manager'), max_length=100, blank=True)

    class Meta:
        managed = False
        db_table = 'worklog_articleentry'


class WorklogEntry(CreateUpdateModel, AccountingRateMixin, InvoicingRateMixin):
    created_by = models.ForeignKey(AuthUser, verbose_name=_('Created by'))
    job = models.ForeignKey(Job, related_name='worklogentries', verbose_name=_('Job'))
    worker = models.ForeignKey(AuthUser, related_name='worklogentries', verbose_name=_('Worker'))
    date = models.DateField(_('Date'), default=datetime.date.today)
    hours = models.DecimalField(_('Hours'), max_digits=10, decimal_places=1)
    activity = models.ForeignKey(Activity, verbose_name=_('Activity'))
    notes = models.TextField(_('Notes'), blank=True)
    archived = models.BooleanField(_('Archived'), default=False)
    invoice = models.ForeignKey('JobInvoice', blank=True, null=True,
                                verbose_name=_('JobInvoice'), related_name='job_worklogentries')
    satisfaction = models.IntegerField(_('Satisfaction'), blank=True, null=True)
    # fields related to sales_regions feature
    sales_region_company = models.ForeignKey(Person, verbose_name=_('Sales region company'),
                                             blank=True, null=True, related_name='+', on_delete=models.SET_NULL)
    sales_region = models.CharField(_('Sales region'), max_length=3, blank=True)
    rd = models.IntegerField(_('RD'), blank=True, null=True)
    general_agency = models.IntegerField(_('General agency'), blank=True, null=True)
    agency = models.IntegerField(_('Agency'), blank=True, null=True)
    agency_manager = models.CharField(_('Agency manager'), max_length=100, blank=True)

    class Meta:
        managed = False
        db_table = 'worklog_worklogentry'
