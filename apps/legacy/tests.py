from django.test import TestCase
from django.core.management import call_command

from apps.companies.models import Company
from apps.legacy.models import Person as LegacyPerson
from apps.persons.models import Person


class TestCommand(TestCase):

    def test_import_legacy_command(self):
        """ Be aware that this test runs only if you have some data in your legacy db

        """
        if not LegacyPerson.objects.using('legacy').count():
            return
        persons_before = Person.objects.count()
        legacy_persons_before = LegacyPerson.objects.using('legacy').filter(company=False).count()
        call_command("import_legacy_data")
        self.assertEqual(persons_before + legacy_persons_before, Person.objects.count())
