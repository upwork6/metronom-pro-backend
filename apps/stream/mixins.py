from django.contrib.contenttypes.models import ContentType
from rest_framework.decorators import detail_route

from .views import ActivitiesViewSet


class ActivitiesMixin:
    """Activities used in Projects & campaigns"""

    @detail_route(url_path='activities')
    def activities(self, request, *args, **kwargs):
        """Activity stream of the user"""
        kwargs['content_type'] = ContentType.objects.get_for_model(self.queryset.model)
        return ActivitiesViewSet.as_view({'get': 'list'})(request, *args, **kwargs)
