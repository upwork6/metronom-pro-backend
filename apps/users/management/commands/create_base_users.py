from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand

User = get_user_model()



class Command(BaseCommand):
    """
        Createss users out of the env parameters BASE_USERS and BASE_SUPERUSERS

        Example:
            BASE_SUPERUSERS=[["admin@metronom-dev.ch", "test1234", "admin"]]

        Those need to exist in the environment.

        Existing users are not overwritten but skipped.
    """

    help = '''Creates users out of env parameter BASE_SUPERUSERS'''

    def create_superuser(self, email, password, username, first_name, last_name):
        """ Creates a superuser """

        try:
            user = User.objects.get(email=email)
            user.is_superuser = True
            user.is_staff = True
            user.is_active = True
            user.first_name = first_name
            user.last_name = last_name
            user.username = username
            user.set_password(password)
            user.save()
            print(f"The superuser with {email} already existed, some attributes may have been updated.")
        except User.DoesNotExist:
            user = User.objects.create_superuser(email=email, password=password, username=username, first_name=first_name, last_name=last_name)
            print(f"Created a superuser with the email {email}.")

        groups = Group.objects.all()

        for group in groups:
            user.groups.add(group)

    def handle(self, **options):

        for user in settings.BASE_SUPERUSERS:
            self.create_superuser(user[0], user[1], user[2], user[3], user[4])
