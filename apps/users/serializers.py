
import django.contrib.auth.password_validation as validators
from django.contrib.auth import get_user_model
from django.db import transaction
from rest_auth.registration.serializers import RegisterSerializer
from rest_framework import serializers

from apps.bankaccounts.serializers import BankAccountThinSerializer
from apps.contracts.serializers import ContractRetrieveSerializer
from apps.profile_images.models import ProfileImage
from apps.profile_images.serializers import ProfileImageSerializer
from apps.users.models import Family, Profile

try:
    from allauth.account import app_settings as allauth_settings
    from allauth.account.adapter import get_adapter
    from allauth.account.utils import setup_user_email
    from allauth.utils import get_username_max_length
except ImportError:
    raise ImportError("allauth needs to be added to INSTALLED_APPS.")

User = get_user_model()


class FamilySerializer(serializers.ModelSerializer):
    class Meta:
        model = Family
        fields = (
            'id',
            'relation',
            'first_name',
            'last_name',
            'birth_date',
        )


class ProfileSerializer(serializers.ModelSerializer):

    date_joined = serializers.SerializerMethodField()
    metronom_role = serializers.CharField(source='user.role', read_only=True)

    class Meta:
        model = Profile
        fields = (
            'birth_place',
            'civil_state',
            'ahv_number',
            'liable_with_witholding_tax',
            'timezone',
            'date_joined',
            'metronom_role'
        )

    def get_date_joined(self, obj):
        return obj.user.date_joined

    def update(self, instance, validated_data):
        with transaction.atomic():
            # possible cases:
            # 1. image field not present:
            #       image object not updated;
            # 2. image field present and null:
            #       image object deleted (if already present);
            # 3. image field present, not null, with id:
            #       new image created, past is deleted (if present);
            # 4. image field present, not null, without id:
            #       image object updated, created if not present.
            update_image = 'image' in validated_data.keys()

            image_data = validated_data.pop('image', {})

            if not update_image:
                # case 1.
                pass
            elif image_data is None:
                # case 2.
                if instance.image is not None:
                    image_to_del = instance.image
                    instance.image = None
                    instance.save()
                    image_to_del.delete()
            elif 'id' in image_data.keys():
                # case 3.
                image_to_del = instance.image
                instance.image = ProfileImage.objects.create(**image_data)
                instance.save()
                if image_to_del is not None:
                    image_to_del.delete()
            else:
                # case 4.
                if instance.image is None:
                    instance.image = ProfileImage.objects.create(**image_data)
                    instance.save()
                else:
                    for attr, value in image_data.items():
                        setattr(instance.image, attr, value)
                    instance.image.save()

            # updating the rest of the profile attributes
            for attr, value in validated_data.items():
                setattr(instance, attr, value)

            instance.save()
        return instance


class UserUUIDSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)

    class Meta:
        model = User
        fields = (
            'uuid',
            'public_id',
            'has_image',
            'first_name',
            'last_name',
        )


class UserSerializer(serializers.ModelSerializer):
    """Serialize data from the User."""

    tenant_personnel_data = ProfileSerializer(source="profile")
    uuid = serializers.UUIDField(source="id")
    person_profile_public_id = serializers.UUIDField(source="person_in_crm.public_id")
    family_members = FamilySerializer(source='profile.family_set', many=True, read_only=True)
    bank_account = BankAccountThinSerializer()
    tenant_employment = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = (
            'uuid',
            'first_name',
            'last_name',
            'is_active',
            'tenant_personnel_data',
            'person_profile_public_id',
            'family_members',
            'bank_account',
            'tenant_employment'
        )

        read_only_fields = (
            'is_active',
            'personnel_data',
            'bank_account'
        )

    def get_tenant_employment(self, obj):
        return {
            "public_id": "000",
            "name": "Partner & Partner AG"
        }


class JWTSignInSerializer(serializers.ModelSerializer):
    """Used for the JSON Web Token Response """

    class Meta:
        model = User
        fields = (
            'has_image',
        )

        has_image = serializers.SerializerMethodField()

    def get_has_image(self, obj):
        return obj.profile.has_image


class ResetPasswordSerializer(serializers.ModelSerializer):
    def validate_password(self, data):
        """initial_data has to be converted to an object for UserAttributeSimilarityValidator."""
        user = self.initial_data
        validators.validate_password(password=data, user=user)

    class Meta:
        model = User
        fields = ('id', 'email', 'password')
        read_only_fields = ('email',)
        extra_kwargs = {'password': {'write_only': True}}


class PasswordSerializer(serializers.Serializer):
    new_password = serializers.CharField(style={'input_type': 'password'})

    def validate_new_password(self, data):
        """initial_data has to be converted to an object for UserAttributeSimilarityValidator."""
        user = self.initial_data
        validators.validate_password(password=data, user=user)


class CurrentPasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(style={'input_type': 'password'})

    default_error_messages = {
        'invalid_password': 'The password is invalid'
    }

    def __init__(self, *args, **kwargs):
        super(CurrentPasswordSerializer, self).__init__(*args, **kwargs)

    def validate_old_password(self, value):
        user = self.initial_data
        if not self.context.get("request").user.check_password(value):
            raise serializers.ValidationError(self.error_messages['invalid_password'])
        return value


class ChangePasswordSerializer(PasswordSerializer, CurrentPasswordSerializer):
    pass


class CreateUserSerializer(RegisterSerializer):
    """Add first_name and last_name to the register_auth's RegisterSerializer."""
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)

    def get_cleaned_data(self):
        return {
            'username': self.validated_data.get('username', ''),
            'first_name': self.validated_data.get('first_name', ''),
            'last_name': self.validated_data.get('last_name', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
        }

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        adapter.clean_password(self.cleaned_data['password1'], user)
        adapter.save_user(request, user, self)
        self.custom_signup(request, user)
        setup_user_email(request, user, [])
        return user
