# -*- coding: utf-8 -*-
from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from apps.users.models import Family, Profile, User

from actstream.models import Action, Follow

admin.site.unregister(Action)
admin.site.unregister(Follow)


# TODO: #50
# class TenantInline(admin.TabularInline):
#     model = UserTenantRelationship
#     extra = 0


class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class MyUserCreationForm(UserCreationForm):
    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': 'This username has already been taken.'
    })

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


class FamilyInline(admin.TabularInline):
    model = Family
    extra = 0


class ProfileInline(admin.StackedInline):
    model = Profile

    inlines = (FamilyInline,)


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'civil_state',
        'birth_place',
    )
    list_filter = (
        'created_at',
        'updated_at',
    )

    inlines = (FamilyInline,)


@admin.register(User)
class UserAdmin(AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    list_display = (
        'id',
        'last_login',
        'is_superuser',
        'person_in_crm',
        'email',
        'username',
        'first_name',
        'last_name',
        'is_active',
        'is_staff',
        'date_joined',
        'signed_in',
        'profile',
    )

    list_editable = (
        'username',
        'first_name',
        'last_name',
        'is_active',
        'is_staff',
        'email',
    )

    list_filter = (
        'last_login',
        'is_superuser',
        'person_in_crm',
        'is_active',
        'is_staff',
        'date_joined',
        'signed_in',
        'profile',
    )

    search_fields = ['username', 'email', ]
    ordering = ('date_joined',)

    inlines = (
        # ProfileInline,
    )

    # TODO: #50
    # inlines = (TenantInline,)
    #
    # def get_tenants(self, obj):
    #     return "\n".join([t.name for t in obj.tenants.all()])
