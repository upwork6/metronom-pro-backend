from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UsersConfig(AppConfig):
    name = 'apps.users'
    verbose_name = _("Users")

    def ready(self):
        """If django-activity-stream is installed, register the User model for usage."""
        try:
            from actstream import registry
            registry.register(self.get_model('User'))
        except ImportError:
            pass
