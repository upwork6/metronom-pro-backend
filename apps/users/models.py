
from datetime import date

from allauth.account.models import EmailAddress
from django.conf import settings
from django.contrib.auth import user_logged_in, user_logged_out
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager,
                                        PermissionsMixin)
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models, transaction
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from localflavor.ch.forms import CHSocialSecurityNumberField

from apps.autocompletes.utils import get_timezone_choices
from apps.emails.models import Email
from apps.metronom_commons.models import ROLE, UserAccessHRMetronomBaseModel
from apps.persons.models import Person
from apps.profile_images.models import ProfileImage


def ahv_number_form_validator(value):
    CHSocialSecurityNumberField(max_length=16, min_length=16).run_validators(value)


class UserManager(BaseUserManager):
    use_in_migrations = True

    def find_next_available_username(self, wanted_username):
        counter = 1
        checked_username = wanted_username

        while True:

            try:
                self.model.objects.get(username=checked_username)
            except self.model.DoesNotExist:
                return checked_username

            counter += 1

            checked_username = "{}{}".format(wanted_username, counter)

    def _create_user(self, email, password, username=None, **extra_fields):
        """Create and save a User with the given username, email and password."""

        if email is None:
            raise ValueError('The email must be set')

        email = self.normalize_email(email)

        if username is None or username == '':
            username = self.find_next_available_username(email.split("@")[0])

        username = self.model.normalize_username(username)
        with transaction.atomic():
            profile = Profile()
            user = self.model(username=username, email=email, profile=profile, **extra_fields)
            user.set_password(password)
            user.save()
            profile.user = user
            profile.save()
        return user

    def create_user(self, email, username=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('role', ROLE.USER)

        return self._create_user(email, password, username, **extra_fields)

    def create_superuser(self, email, password, username=None, **extra_fields):

        if password is None:
            raise TypeError('Superusers must have a password.')

        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('role', ROLE.ADMIN)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        superuser = self._create_user(email, password, username, **extra_fields)

        # (Force-)Verify email of superuser
        verification, created = EmailAddress.objects.get_or_create(user=superuser, email=superuser.email)
        verification.verified = True
        verification.set_as_primary()
        verification.save()

        return superuser


class Profile(UserAccessHRMetronomBaseModel):
    SINGLE = 'single'
    MARRIED = 'married'
    DIVORCED = 'divorced'
    WIDOWED = 'widowed'

    CIVIL_STATE_CHOICES = (
        (SINGLE, _('single')),
        (MARRIED, _('married')),
        (DIVORCED, _('divorced')),
        (WIDOWED, _('widowed')),
    )

    civil_state = models.CharField(
        _('Relation type'),
        max_length=10,
        choices=CIVIL_STATE_CHOICES,
        blank=True,
        null=True,
    )

    birth_place = models.CharField(
        _('Birth Place'),
        help_text=_('Place where the user was born.'),
        max_length=80,
        blank=True,
        null=True,
    )

    ahv_number = models.CharField(
        _('Swiss Social Security number.'),
        validators=[ahv_number_form_validator],
        help_text=_('See: http://de.wikipedia.org/wiki/Sozialversicherungsnummer#Versichertennummer'),
        max_length=16,
        blank=True,
        null=True,
    )

    # liable --> boolean, Make it nullable and have this as default
    liable_with_witholding_tax = models.NullBooleanField(
        _('Liable with witholding tax'),
        help_text=_('Is the user liable with witholding tax?'),
    )

    # timezone --> we are using timezones already in the autocomplete. default: europe/zürich
    timezone = models.CharField(
        _("User's default timezone"),
        help_text=_('User default timezone'),
        max_length=32,
        choices=get_timezone_choices(),
        default='Europe/Zurich',
    )


class Family(UserAccessHRMetronomBaseModel):
    PARTNER = 'partner'
    SON = 'son'
    DAUGHTER = 'daughter'

    RELATION_CHOICES = (
        (PARTNER, _('partner')),
        (SON, _('son')),
        (DAUGHTER, _('daughter')),
    )

    relation = models.CharField(
        _('Relation type'),
        max_length=10,
        choices=RELATION_CHOICES,
        blank=True,
        null=True
    )

    profile = models.ForeignKey(Profile)

    first_name = models.CharField(
        _('first name'),
        help_text=_("First Name of the family member"),
        max_length=30,
        blank=True,
    )

    last_name = models.CharField(
        _('last name'),
        max_length=30,
        help_text=_("Last Name of the family member"),
        blank=True,
    )

    birth_date = models.DateField(_('Date of birth'), blank=True, null=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}, {self.birth_date}, {self.relation}'


class User(UserAccessHRMetronomBaseModel, AbstractBaseUser, PermissionsMixin):
    """ Be aware of the bug 
    https://code.djangoproject.com/ticket/25012
    Django m2m field doesn't detects the changes of the primary key type inside the models, so 
    because of this we need those custom RunSQL like here:
    https://github.com/jensneuhaus/metronom-pro-backend/pull/178/files#diff-2705f44f125a2b05bd348857952a1efc
    """

    @property
    def has_image(self):
        return self.person_in_crm.image is not None

    profile = models.OneToOneField(
        'users.Profile',
        verbose_name=_("User's Profile"),
        help_text=_("More information about the User"),
        on_delete=models.CASCADE,
        related_name='user',
        null=True,
    )

    bank_account = models.OneToOneField(
        'bankaccounts.BankAccount',
        null=True,
        blank=True,
        verbose_name=_('Bank account'))

    person_in_crm = models.OneToOneField(
        Person,
        on_delete=models.PROTECT,
        null=True,
        help_text=_("Person profile for User in the CRM")
    )

    email = models.EmailField(
        _('email address'),
        help_text=_("Email of the user"),
        null=False,
        blank=False,
        unique=True
    )

    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )

    first_name = models.CharField(
        _('first name'),
        help_text=_("First Name of the user"),
        max_length=30,
        blank=True
    )

    last_name = models.CharField(
        _('last name'),
        max_length=30,
        blank=True,
        help_text=_("Last Name of the user")
    )

    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )

    # is_instance_admin = models.BooleanField...

    date_joined = models.DateTimeField(
        _('date joined'),
        help_text=_("When did the user join?"),
        default=timezone.now
    )

    signed_in = models.DateTimeField(_('Signed in'), help_text=_("Is the user signed in?"), null=True)

    role = models.CharField(max_length=32, verbose_name=_('Role for Permission'),
                            choices=ROLE.CHOICES, default=ROLE.NONE)

    # TODO: #50

    # tenants = models.ManyToManyField(
    #     Tenant,
    #     help_text=_("Where is the user registered?"),
    #     through='UserTenantRelationship'
    # )

    USERNAME_FIELD = 'email'

    objects = UserManager()

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.get_full_name()

    def save(self, *args, **kwargs):
        """ We want to create a Person in the CRM as soon as the User is saved"""

        if not self.person_in_crm:
            email_obj = Email.objects.create(email=self.email)
            self.person_in_crm = Person.objects.create(first_name=self.first_name, last_name=self.last_name,
                                                       email=email_obj)
        if not self.profile:
            self.profile = Profile.objects.create()

        super(User, self).save(*args, **kwargs)

    @property
    def name(self):
        """This is used for the autocomplete united"""
        return self.get_full_name()

    @property
    def public_id(self):
        """ The public ID of that User """
        return self.person_in_crm.public_id if hasattr(self, 'person_in_crm') else None

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        if (self.first_name == "" or self.first_name is None) and (self.last_name == "" or self.last_name is None):
            return f'No Name'
        return f'{self.first_name} {self.last_name}'

    def get_absolute_url(self):
        return f"admin/users/user/{self.id}/change/"

    @property
    def current_contract(self):
        actual_day = date.today()
        current_contracts = self.contracts.filter(start_date__lte=actual_day, end_date__gte=actual_day)
        if current_contracts.count() > 1:
            print(f'Warning: there is more than one current_contract for the a User obj.'
                  f'One randomly will be chosen to avoid bad termination.')
        return current_contracts.first()

    @staticmethod
    def fill_default_employmemt(sender, instance, created, *args, **kwargs):
        if created:
            # we have to avoid circular imports
            from apps.companies.models import Company
            from apps.employments.models import Employment
            default_company_public_id = settings.AGENCY_COMPANY_PUBLIC_ID
            default_company = Company.objects.filter(public_id=default_company_public_id).first()
            if default_company:
                Employment.objects.create(
                    person=instance.person_in_crm,
                    company=default_company,
                )

    @property
    def is_admin_role(self):
        return self.role == ROLE.ADMIN


@receiver(user_logged_in)
def on_user_login(sender, **kwargs):
    user = kwargs.get('user')
    User.objects.filter(id=user.id).update(signed_in=timezone.now())


@receiver(user_logged_out)
def on_user_logout(sender, **kwargs):
    user = kwargs.get('user')
    # Do not process anonymous users
    if user:
        User.objects.filter(id=user.id).update(signed_in=None)


post_save.connect(sender=User, receiver=User.fill_default_employmemt,
                  dispatch_uid="user_fill_default_employment")
