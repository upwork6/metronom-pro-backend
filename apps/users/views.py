from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from apps.api.mixins import LoggerMixin
from apps.users.mixins import MeURLMixin, MeUserMixin
from apps.users.serializers import FamilySerializer, ProfileSerializer, UserSerializer


User = get_user_model()


class FamilyViewSet(MeURLMixin, LoggerMixin, ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = FamilySerializer

    def perform_create(self, serializer):
        serializer.save(profile=self.get_user().profile)

    def get_queryset(self):
        return self.get_user().profile.family_set.all()


class ProfileViewSet(MeURLMixin, LoggerMixin, ModelViewSet):
    serializer_class = ProfileSerializer
    permission_classes = (IsAuthenticated,)

    http_method_names = ['get', 'post', 'patch', 'head', 'options', 'trace']
    restricted_http_method_names = http_method_names

    def perform_create(self, serializer):
        serializer.save(user=self.get_user())

    def perform_update(self, serializer):
        serializer.save(user=self.get_user())

    def get_object(self):
        return self.get_user().profile

    def get_queryset(self):
        """
        Adding this method since it is sometimes called when using
        django-rest-swagger
        https://github.com/Tivix/django-rest-auth/issues/275
        """
        return [self.get_object()]


class UserViewSet(MeUserMixin, LoggerMixin, ReadOnlyModelViewSet):
    """Users (Workers in the agency)"""
    queryset = User.objects.all()
    serializer_class = UserSerializer
    # permission_classes = (IsHRPermission,) # TODO: fix this
    lookup_field = 'pk'
    me_placeholder = 'me'

    def get_object(self):
        return self.get_user()


