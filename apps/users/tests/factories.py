import datetime

import factory
from django.utils import timezone
from factory.fuzzy import FuzzyDateTime

from apps.bankaccounts.tests.factories import BankAccountFactory
from apps.persons.tests.factories import PersonFactory
from apps.users.models import Family, Profile, User


class FamilyFactory(factory.DjangoModelFactory):
    first_name = factory.Iterator(["Andreas", "Bettina", "Nadine", "Denise", "Rea", "Kaspar",
                                   "Jens", "Michael", "Matthias", "Markus"])
    last_name = factory.Iterator(["Baumann", "Zimmermann", "Reich", "Fetz", "Weber", "Lutz",
                                  "Nick", "Hofmann", "Schwander", "Eschmann"])
    birth_date = FuzzyDateTime(datetime.datetime(1950, 1, 1, tzinfo=datetime.timezone.utc),
                               datetime.datetime(2000, 1, 1, tzinfo=datetime.timezone.utc),
                               force_day=3, force_second=42)
    relation = factory.Iterator([x[0] for x in Family.RELATION_CHOICES])

    profile = factory.SubFactory('apps.users.tests.factories.ProfileFactory')

    class Meta:
        model = Family


class ProfileFactory(factory.DjangoModelFactory):
    civil_state = factory.Iterator([Profile.SINGLE, Profile.DIVORCED, Profile.WIDOWED])
    birth_place = factory.Iterator(('Berlin', 'Zurich', 'Milan'))
    user = factory.SubFactory('apps.users.tests.factories.UserFactory')

    family_member1 = factory.RelatedFactory(FamilyFactory, 'profile')
    family_member2 = factory.RelatedFactory(FamilyFactory, 'profile')

    ahv_number = '756.1234.5678.97'
    liable_with_witholding_tax = True
    timezone = 'Europe/Berlin'

    # in case is wanted a profile with an image, that's the code:
    # image = factory.RelatedFactory(ProfileImageFactory, 'profile')

    class Meta:
        model = Profile


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Sequence(lambda n: 'user-{0}'.format(n))
    email = factory.Sequence(lambda n: 'user-{0}@example.com'.format(n))
    password = factory.PostGenerationMethodCall('set_password', 'password')
    person_in_crm = factory.SubFactory(PersonFactory)
    profile = factory.RelatedFactory(ProfileFactory, 'user')
    date_joined = factory.LazyFunction(timezone.now)

    class Meta:
        model = User
        django_get_or_create = ('username',)


class UserNiceFactory(factory.DjangoModelFactory):
    bank_account = factory.SubFactory(BankAccountFactory,
                                      owner_name=factory.LazyAttribute(lambda o: "%s %s" % (
                                          o.factory_parent.first_name, o.factory_parent.last_name))
                                      )

    first_name = factory.Iterator(["Andreas", "Bettina", "Nadine", "Denise", "Rea", "Kaspar",
                                   "Jens", "Michael", "Matthias", "Markus"])
    last_name = factory.Iterator(["Baumann", "Zimmermann", "Reich", "Fetz", "Weber", "Lutz",
                                  "Nick", "Hofmann", "Schwander", "Eschmann"])

    username = factory.Sequence(lambda n: 'user-{0}'.format(n))
    email = factory.Sequence(lambda n: 'user-{0}@example.com'.format(n))
    # password = factory.PostGenerationMethodCall('set_password', 'password')
    person_in_crm = factory.SubFactory(PersonFactory)
    profile = factory.SubFactory(ProfileFactory)

    class Meta:
        model = User


