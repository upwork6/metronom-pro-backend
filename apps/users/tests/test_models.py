from django.db.utils import IntegrityError
from django.test import TestCase

from apps.users.models import Family, Profile, User
from apps.users.tests.factories import UserFactory


class TestUser(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.test_user = User.objects.create_user(
            email="existing_user@test.com",
            first_name='Existing',
            last_name='User',
            username="existing_user",
        )

    def test__str__(self):
        self.assertEqual(
            self.test_user.__str__(),
            'Existing User'
        )

    def test__short_name(self):
        self.assertEqual(
            self.test_user.get_short_name(),
            'existing_user'
        )

    def test__full_name(self):
        self.assertEqual(
            self.test_user.get_full_name(),
            'Existing User'
        )

    def test_get_absolute_url(self):
        self.assertEqual(
            self.test_user.get_absolute_url(),
            f"admin/users/user/{self.test_user.id}/change/"
        )

    def test_create_user_method(self):
        """We are checking, if we can create a user with just an email."""
        new_user = User.objects.create_user(email="new_user@test.com")
        self.assertEqual(User.objects.get(email="new_user@test.com"), new_user)
        self.assertEqual(new_user.is_staff, False)
        self.assertEqual(new_user.is_superuser, False)

    def test_create_user_with_ungiven_username(self):
        """We are checking, if we can create a user with just an email."""
        new_user = User.objects.create_user(email="username@test.com")
        self.assertEqual(new_user.username, "username")

    def test_create_user_with_existing_email(self):
        """We are checking, if we can create a user with just an email."""
        with self.assertRaises(IntegrityError):
            User.objects.create_user(email="existing_user@test.com")

    def test_create_user_with_existing_username(self):
        """We are checking, if we can create a user with just an email."""
        with self.assertRaises(IntegrityError):
            User.objects.create_user(email="existing_user2@test.com", username="existing_user")

    def test_create_user_with_existing_username_method(self):
        """We are checking, if we can create a user with just an email."""
        new_user = User.objects.create_user(email="new_user@test.com", password="test")
        self.assertEqual(User.objects.get(email="new_user@test.com"), new_user)
        self.assertEqual(new_user.has_usable_password(), True)

    def test_create_superuser_method(self):
        """We are checking, if we can create a user with just an email."""
        new_user = User.objects.create_superuser(email="new_user@test.com", password="test")
        self.assertEqual(User.objects.get(email="new_user@test.com"), new_user)
        self.assertEqual(new_user.has_usable_password(), True)
        self.assertEqual(new_user.is_staff, True)
        self.assertEqual(new_user.is_superuser, True)

    def test_find_next_available_username(self):
        """A unused and ungiven username should be given."""
        self.assertEqual(User.objects.find_next_available_username("not_used_yet"), "not_used_yet")

        i = 2

        # The next available existing_user should have a username with 2
        while i < 12:
            self.assertEqual(User.objects.find_next_available_username("existing_user"), "existing_user{}".format(i))
            User.objects.create_user(email="existing_user@test{}.com".format(i), password="test")
            i += 1

    def test_create_user_person(self):
        new_user = UserFactory()
        self.assertIsNotNone(new_user.person_in_crm)


class TestProfile(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.profile = User.objects.all().first().profile

    def test_profile(self):
        self.assertIsNotNone(self.profile)

    def test_civil_state(self):
        self.assertIn(self.profile.civil_state, (x[0] for x in Profile.CIVIL_STATE_CHOICES))

    def test_birth_place(self):
        self.assertTrue(self.profile.birth_place)

    def test_user(self):
        self.assertIsNotNone(self.profile.user)

    def test_ahv_number(self):
        self.assertIsNotNone(self.profile.ahv_number)

    def test_liable_with_witholding_tax(self):
        self.assertIsNotNone(self.profile.liable_with_witholding_tax)

    def test_timezone(self):
        self.assertIsNotNone(self.profile.timezone)


class TestFamily(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.profile = User.objects.all().first().profile
        cls.family = cls.profile.family_set.all()

    def test_family(self):
        self.assertEqual(self.family.count(), 2)
        self.assertEqual(Family.objects.count(), 2)
        self.assertNotEquals(self.family.first(), self.family.last())

    def test_profile(self):
        self.assertIsNotNone(self.profile)
        self.assertTrue(any(map(lambda f: f.profile, self.family)))

    def test_relation(self):
        for f in self.family:
            self.assertIn(f.relation, (x[0] for x in Family.RELATION_CHOICES))

    def test_first_name(self):
        for f in self.family:
            self.assertTrue(f.first_name)

    def test_last_name(self):
        for f in self.family:
            self.assertTrue(f.last_name)

    def test_birth_date(self):
        for f in self.family:
            self.assertTrue(f.birth_date)
