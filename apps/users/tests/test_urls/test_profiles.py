from django.urls import resolve, reverse

from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.users.tests.factories import UserFactory


class TestProfileHRURLs(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.login_as_hr_user()
        cls.other_user = UserFactory()
        cls.other_uuid = cls.other_user.id

    def test_profile_list_reverse(self):
        assert reverse('api_users:tenant_personnel_data-list',
                       kwargs={'user_pk': self.other_uuid}) == f'/api/users/{self.other_uuid}/tenant_personnel_data/'

    def test_profile_base_resolve(self):
        assert resolve(f'/api/users/{self.other_uuid}/tenant_personnel_data/').view_name == 'api_users:tenant_personnel_data-list'

    def test_profile_me_list_reverse(self):
        assert reverse('api_users:tenant_personnel_data-list',
                       kwargs={'user_pk': 'me'}) == f'/api/users/me/tenant_personnel_data/'

    def test_profile_me_base_resolve(self):
        assert resolve(f'/api/users/me/tenant_personnel_data/').view_name == 'api_users:tenant_personnel_data-list'


class TestProfileNUURLs(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.login_as_user()
        cls.other_user = UserFactory()
        cls.other_uuid = cls.other_user.id

    def test_profile_detail_reverse(self):
        assert reverse('api_users:tenant_personnel_data-list',
                       kwargs={'user_pk': 'me'}) == f'/api/users/me/tenant_personnel_data/'

    def test_profile_base_resolve(self):
        assert resolve(
            f'/api/users/{self.other_uuid}/tenant_personnel_data/').view_name == 'api_users:tenant_personnel_data-list'
