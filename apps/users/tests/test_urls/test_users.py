from django.test import TestCase
from django.urls import resolve, reverse

from apps.users.tests.factories import UserFactory


class TestUsersURLs(TestCase):
    def setUp(self):
        self.user = UserFactory()

    def test_user_list_reverse(self):
        assert reverse('api:user-list') == '/api/users/'

    def test_user_list_resolve(self):
        assert resolve('/api/users/').view_name == 'api:user-list'

    def test_user_detail_reverse(self):
        assert reverse('api:user-detail', kwargs={'pk': self.user.id}) == f'/api/users/{self.user.id}/'

    def test_user_detail_resolve(self):
        assert resolve(f'/api/users/{self.user.id}/').view_name == 'api:user-detail'

    def test_user_me_detail_reverse(self):
        assert reverse('api:user-detail', kwargs={'pk': 'me'}) == f'/api/users/me/'

    def test_user_me_detail_resolve(self):
        assert resolve(f'/api/users/me/').view_name == 'api:user-detail'
