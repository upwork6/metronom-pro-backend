from django.test import TestCase
from django.urls import resolve, reverse


class TestMeURL(TestCase):
    def test_me_base_reverse(self):
        assert reverse('api:user-detail', kwargs={"pk": 'me'}) == '/api/users/me/'

    def test_me_base_resolve(self):
        assert resolve('/api/users/me/').view_name == 'api:user-detail'
