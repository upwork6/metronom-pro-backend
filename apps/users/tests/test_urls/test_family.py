from django.urls import resolve, reverse

from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.users.tests.factories import UserFactory


class TestMeFamilyURLs(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.login_as_user()

    def test_family_list_reverse(self):
        assert reverse('api_users:family-list', kwargs={'user_pk': 'me'}) == '/api/users/me/family_members/'

    def test_family_list_resolve(self):
        assert resolve(f'/api/users/me/family_members/').view_name == 'api_users:family-list'

    def test_family_detail_reverse(self):
        family_pk = self.test_user.profile.family_set.all().first().pk
        assert reverse('api_users:family-detail', kwargs={
            'user_pk': 'me',
            'pk': family_pk,
        }) == f'/api/users/me/family_members/{family_pk}/'

    def test_family_base_resolve(self):
        family_pk = self.test_user.profile.family_set.all().first().pk
        assert resolve(f'/api/users/me/family_members/{family_pk}/').view_name == 'api_users:family-detail'


class TestHRFamilyURLs(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.login_as_hr_user()
        cls.family_pk = cls.test_user.profile.family_set.all().first().pk

        cls.other_user = UserFactory()
        cls.other_uuid = cls.other_user.id

    def test_family_list_reverse(self):
        assert reverse('api_users:family-list',
                       kwargs={'user_pk': self.other_uuid}) == f'/api/users/{self.other_uuid}/family_members/'

    def test_family_list_resolve(self):
        assert resolve(f'/api/users/{self.other_uuid}/family_members/').view_name == 'api_users:family-list'

    def test_family_detail_reverse(self):
        assert reverse('api_users:family-detail', kwargs={
            'user_pk': self.other_uuid,
            'pk': self.family_pk,
        }) == f'/api/users/{self.other_uuid}/family_members/{self.family_pk}/'

    def test_family_base_resolve(self):
        assert resolve(
            f'/api/users/{self.other_uuid}/family_members/{self.family_pk}/').view_name == 'api_users:family-detail'
