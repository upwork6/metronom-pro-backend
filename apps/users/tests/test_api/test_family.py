import datetime

import pytest
from rest_framework import status
from rest_framework.reverse import reverse

from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.users.models import Family
from apps.users.tests.factories import UserFactory


class AbstractTestHROtherFamily(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.other_user = UserFactory()
        cls.other_uuid = cls.other_user.id
        cls.other_family_set = cls.other_user.profile.family_set.all()
        cls.other_len_family_set = cls.other_family_set.count()
        cls.other_family_member = cls.other_family_set.first()

        cls.family_set = cls.test_user.profile.family_set.all()
        cls.len_family_set = cls.family_set.count()
        cls.family_member = cls.family_set.first()


class TestRetrieveHROtherFamily(AbstractTestHROtherFamily):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.endpoint = reverse('api_users:family-detail',
                               kwargs={'user_pk': cls.other_uuid, 'pk': cls.other_family_member.pk})

        cls.response = cls.hr_client.get(cls.endpoint, format='json')
        cls.data = cls.response.data

    def test_retrieve_family_success(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_create_family_relation(self):
        value = self.data.get('relation', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.other_family_member.relation)

    def test_create_family_profile(self):
        value = self.data.get('profile', None)
        self.assertIsNone(value)

    def test_create_family_first_name(self):
        value = self.data.get('first_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.other_family_member.first_name)

    def test_create_family_last_name(self):
        value = self.data.get('last_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.other_family_member.last_name)

    def test_create_family_birth_date(self):
        value = self.data.get('birth_date', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.other_family_member.birth_date))


class TestCreateHROtherFamily(AbstractTestHROtherFamily):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.endpoint = reverse('api_users:family-list', kwargs={'user_pk': cls.other_uuid})

        cls.relation = Family.PARTNER
        cls.profile = cls.other_user.profile.pk
        cls.first_name = 'Mario'
        cls.last_name = 'Rossi'
        cls.birth_date = datetime.date(1981, 1, 1)

        cls.post_data = dict(
            relation=cls.relation,
            # profile=cls.profile,
            first_name=cls.first_name,
            last_name=cls.last_name,
            birth_date=cls.birth_date,
        )
        cls.response = cls.hr_client.post(cls.endpoint, data=cls.post_data, format='json')
        cls.data = cls.response.data

    def test_create_family_success(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.assertEquals(self.test_user.profile.family_set.all().count(), self.len_family_set)
        self.assertEquals(self.other_user.profile.family_set.all().count(), self.other_len_family_set + 1)

    def test_create_family_relation(self):
        value = self.data.get('relation', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.relation)

    def test_create_family_profile(self):
        value = self.data.get('profile', None)
        self.assertIsNone(value)
        self.assertNotEquals(self.family_member.profile, self.profile)

    def test_create_family_first_name(self):
        value = self.data.get('first_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.first_name)

    def test_create_family_last_name(self):
        value = self.data.get('last_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.last_name)

    def test_create_family_birth_date(self):
        value = self.data.get('birth_date', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.birth_date))


class TestUpdateHROtherFamily(AbstractTestHROtherFamily):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        email = "other_user@test.com"
        cls.user_password = "test1234"

        new_user = UserFactory(
            email=email,
            password=cls.user_password,
        )

        cls.family_member = cls.other_family_set.first()
        cls.endpoint = reverse('api_users:family-detail',
                               kwargs=dict(user_pk=cls.other_uuid, pk=cls.family_member.pk))

        cls.relation = Family.PARTNER
        cls.profile = new_user.profile
        cls.first_name = 'Mario'
        cls.last_name = 'Rossi'
        cls.birth_date = datetime.date(1981, 1, 1)

    def test_update_family_relation(self):
        self.patch_data = dict(relation=self.relation)

        self.response = self.hr_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('relation', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.relation))

        self.family_member.refresh_from_db()
        self.assertEquals(self.family_member.relation, self.relation)

    def test_update_family_profile(self):
        self.patch_data = dict(profile=self.profile)

        self.response = self.hr_client.patch(self.endpoint, data=self.patch_data)
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('profile', None)
        self.assertIsNone(value)
        self.assertNotEquals(value, str(self.profile))

        self.family_member.refresh_from_db()
        self.assertNotEquals(self.family_member.profile, self.profile)

    def test_update_family_first_name(self):
        self.patch_data = dict(first_name=self.first_name)

        self.response = self.hr_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('first_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.first_name))

        self.family_member.refresh_from_db()
        self.assertEquals(self.family_member.first_name, self.first_name)

    def test_update_family_last_name(self):
        self.patch_data = dict(last_name=self.last_name)

        self.response = self.hr_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('last_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.last_name))

        self.family_member.refresh_from_db()
        self.assertEquals(self.family_member.last_name, self.last_name)

    def test_update_family_birth_date(self):
        self.patch_data = dict(birth_date=self.birth_date)

        self.response = self.hr_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('birth_date', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.birth_date))

        self.family_member.refresh_from_db()
        self.assertEquals(self.family_member.birth_date, self.birth_date)


class TestDeleteHROtherFamily(AbstractTestHROtherFamily):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.family_set = cls.other_user.profile.family_set.all()
        cls.len_family_set = cls.family_set.count()

        cls.family_member = cls.family_set.first()
        cls.family_pk = cls.family_member.pk

        cls.endpoint = reverse('api_users:family-detail',
                               kwargs=dict(user_pk=cls.other_user.id, pk=cls.family_member.pk))

    def test_delete_family(self):
        self.response = self.hr_client.delete(self.endpoint, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_204_NO_CONTENT)
        self.data = self.response.data

        self.assertEqual(Family.objects.filter(profile__pk=self.other_user.profile.pk).count(),
                         self.len_family_set - 1)

    def test_delete_family_not_permitted(self):
        family_pk = self.other_user.profile.family_set.all().first().pk
        self.endpoint = reverse('api_users:family-detail', kwargs=dict(user_pk='me', pk=family_pk))
        self.response = self.hr_client.delete(self.endpoint, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_404_NOT_FOUND)


class AbstractTestNUOther(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.other_user = UserFactory()
        cls.other_uuid = cls.other_user.id
        cls.other_family_set = cls.other_user.profile.family_set.all()
        cls.other_len_family_set = cls.other_family_set.count()
        cls.other_family_member = cls.other_family_set.first()


@pytest.mark.skip(reason="No permissions for now")
class TestRetrieveNUOtherFamily(AbstractTestNUOther):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.endpoint = reverse('api_users:family-detail',
                               kwargs={'user_pk': cls.other_uuid, 'pk': cls.other_family_member.pk})

        cls.response = cls.user_client.get(cls.endpoint, format='json')
        cls.data = cls.response.data

    def test_retrieve_family_failure(self):
        self.assertEqual(self.response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_family_relation(self):
        value = self.data.get('relation', None)
        self.assertIsNone(value)

    def test_create_family_profile(self):
        value = self.data.get('profile', None)
        self.assertIsNone(value)

    def test_create_family_first_name(self):
        value = self.data.get('first_name', None)
        self.assertIsNone(value)

    def test_create_family_last_name(self):
        value = self.data.get('last_name', None)
        self.assertIsNone(value)

    def test_create_family_birth_date(self):
        value = self.data.get('birth_date', None)
        self.assertIsNone(value)


@pytest.mark.skip(reason="No permissions for now")
class TestNUCreateOtherFamily(AbstractTestNUOther):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.family_set = cls.test_user.profile.family_set.all()
        cls.len_family_set = cls.family_set.count()
        cls.family_member = cls.family_set.first()

        cls.endpoint = reverse('api_users:family-list', kwargs={'user_pk': cls.other_uuid})

        cls.relation = Family.PARTNER
        cls.profile = cls.test_user.profile.pk
        cls.first_name = 'Mario'
        cls.last_name = 'Rossi'
        cls.birth_date = datetime.date(1981, 1, 1)

        cls.post_data = dict(
            relation=cls.relation,
            # profile=cls.profile,
            first_name=cls.first_name,
            last_name=cls.last_name,
            birth_date=cls.birth_date,
        )
        cls.response = cls.hr_client.post(cls.endpoint, data=cls.post_data, format='json')
        cls.data = cls.response.data

    def test_create_family_failure(self):
        self.assertEqual(self.response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEquals(self.test_user.profile.family_set.all().count(), self.len_family_set)
        self.assertEquals(self.other_family_set.count(), self.other_len_family_set)

    def test_create_family_relation(self):
        value = self.data.get('relation', None)
        self.assertIsNone(value)

    def test_create_family_profile(self):
        value = self.data.get('profile', None)
        self.assertIsNone(value)

    def test_create_family_first_name(self):
        value = self.data.get('first_name', None)
        self.assertIsNone(value)

    def test_create_family_last_name(self):
        value = self.data.get('last_name', None)
        self.assertIsNone(value)

    def test_create_family_birth_date(self):
        value = self.data.get('birth_date', None)
        self.assertIsNone(value)


class TestNUCreateFamily(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.family_set = cls.test_user.profile.family_set.all()
        cls.len_family_set = cls.family_set.count()
        cls.family_member = cls.family_set.first()

        cls.endpoint = reverse('api_users:family-list', kwargs={'user_pk': 'me'})

        cls.relation = Family.PARTNER
        cls.profile = cls.test_user.profile.pk
        cls.first_name = 'Mario'
        cls.last_name = 'Rossi'
        cls.birth_date = datetime.date(1981, 1, 1)

        cls.post_data = dict(
            relation=cls.relation,
            # profile=cls.profile,
            first_name=cls.first_name,
            last_name=cls.last_name,
            birth_date=cls.birth_date,
        )
        cls.response = cls.user_client.post(cls.endpoint, data=cls.post_data, format='json')
        cls.data = cls.response.data

    def test_create_family_success(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.assertEquals(self.test_user.profile.family_set.all().count(), self.len_family_set + 1)

    def test_create_family_relation(self):
        value = self.data.get('relation', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.relation)

    def test_create_family_profile(self):
        value = self.data.get('profile', None)
        self.assertIsNone(value)
        self.assertNotEquals(self.family_member.profile, self.profile)

    def test_create_family_first_name(self):
        value = self.data.get('first_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.first_name)

    def test_create_family_last_name(self):
        value = self.data.get('last_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.last_name)

    def test_create_family_birth_date(self):
        value = self.data.get('birth_date', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.birth_date))


class TestNUUpdateFamily(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        email = "other_user@test.com"
        cls.user_password = "test1234"

        new_user = UserFactory(
            email=email,
            password=cls.user_password,
        )

        family_set = cls.test_user.profile.family_set.all()
        cls.family_member = family_set.first()

        cls.endpoint = reverse('api_users:family-detail', kwargs=dict(user_pk='me', pk=cls.family_member.pk))

        cls.relation = Family.PARTNER
        cls.profile = new_user.profile
        cls.first_name = 'Mario'
        cls.last_name = 'Rossi'
        cls.birth_date = datetime.date(1981, 1, 1)

    def test_update_family_not_permitted(self):
        family_pk = self.profile.family_set.all().first().pk
        self.endpoint = reverse('api_users:family-detail', kwargs=dict(user_pk='me', pk=family_pk))

        self.patch_data = dict(relation=self.relation)

        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_404_NOT_FOUND)

    def test_update_family_relation(self):
        self.patch_data = dict(relation=self.relation)

        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('relation', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.relation))

        self.family_member.refresh_from_db()
        self.assertEquals(self.family_member.relation, self.relation)

    def test_update_family_profile(self):
        self.patch_data = dict(profile=self.profile)

        self.response = self.user_client.patch(self.endpoint, data=self.patch_data)
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('profile', None)
        self.assertIsNone(value)
        self.assertNotEquals(value, str(self.profile))

        self.family_member.refresh_from_db()
        self.assertNotEquals(self.family_member.profile, self.profile)

    def test_update_family_first_name(self):
        self.patch_data = dict(first_name=self.first_name)

        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('first_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.first_name))

        self.family_member.refresh_from_db()
        self.assertEquals(self.family_member.first_name, self.first_name)

    def test_update_family_last_name(self):
        self.patch_data = dict(last_name=self.last_name)

        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('last_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.last_name))

        self.family_member.refresh_from_db()
        self.assertEquals(self.family_member.last_name, self.last_name)

    def test_update_family_birth_date(self):
        self.patch_data = dict(birth_date=self.birth_date)

        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('birth_date', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.birth_date))

        self.family_member.refresh_from_db()
        self.assertEquals(self.family_member.birth_date, self.birth_date)


class TestNUDeleteFamily(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        email = "other_user@test.com"
        cls.user_password = "test1234"

        new_user = UserFactory(
            email=email,
            password=cls.user_password,
        )
        cls.another_profile = new_user.profile

        cls.family_set = cls.test_user.profile.family_set.all()
        cls.len_family_set = cls.family_set.count()

        cls.family_member = cls.family_set.first()
        cls.family_pk = cls.family_member.pk

        cls.endpoint = reverse('api_users:family-detail', kwargs=dict(user_pk='me', pk=cls.family_member.pk))

    def test_delete_family(self):
        self.response = self.user_client.delete(self.endpoint, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_204_NO_CONTENT)
        self.data = self.response.data

        self.assertEqual(Family.objects.filter(profile__pk=self.test_user.profile.pk).count(),
                         self.len_family_set - 1)

    def test_delete_family_not_permitted(self):
        family_pk = self.another_profile.family_set.all().first().pk
        self.endpoint = reverse('api_users:family-detail', kwargs=dict(user_pk='me', pk=family_pk))
        self.response = self.user_client.delete(self.endpoint, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_404_NOT_FOUND)


class TestHRMeCreateFamily(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.family_set = cls.test_user.profile.family_set.all()
        cls.len_family_set = cls.family_set.count()
        cls.family_member = cls.family_set.first()

        cls.endpoint = reverse('api_users:family-list', kwargs={'user_pk': 'me'})

        cls.relation = Family.PARTNER
        cls.profile = cls.test_user.profile.pk
        cls.first_name = 'Mario'
        cls.last_name = 'Rossi'
        cls.birth_date = datetime.date(1981, 1, 1)

        cls.post_data = dict(
            relation=cls.relation,
            # profile=cls.profile,
            first_name=cls.first_name,
            last_name=cls.last_name,
            birth_date=cls.birth_date,
        )
        cls.response = cls.user_client.post(cls.endpoint, data=cls.post_data, format='json')
        cls.data = cls.response.data

    def test_create_family_success(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.assertEquals(self.test_user.profile.family_set.all().count(), self.len_family_set + 1)

    def test_create_family_relation(self):
        value = self.data.get('relation', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.relation)

    def test_create_family_profile(self):
        value = self.data.get('profile', None)
        self.assertIsNone(value)
        self.assertNotEquals(self.family_member.profile, self.profile)

    def test_create_family_first_name(self):
        value = self.data.get('first_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.first_name)

    def test_create_family_last_name(self):
        value = self.data.get('last_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, self.last_name)

    def test_create_family_birth_date(self):
        value = self.data.get('birth_date', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.birth_date))


class TestHRMeUpdateFamily(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user_password = "test1234"

        new_user = UserFactory(
            email="email@email.com",
            password=cls.user_password,
        )

        family_set = cls.test_user.profile.family_set.all()
        cls.family_member = family_set.first()

        cls.endpoint = reverse('api_users:family-detail', kwargs=dict(user_pk='me', pk=cls.family_member.pk))

        cls.relation = Family.PARTNER
        cls.profile = new_user.profile
        cls.first_name = 'Mario'
        cls.last_name = 'Rossi'
        cls.birth_date = datetime.date(1981, 1, 1)

    def test_update_family_not_permitted(self):
        family_pk = self.profile.family_set.all().first().pk
        self.endpoint = reverse('api_users:family-detail', kwargs=dict(user_pk='me', pk=family_pk))

        self.patch_data = dict(relation=self.relation)

        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_404_NOT_FOUND)

    def test_update_family_relation(self):
        self.patch_data = dict(relation=self.relation)

        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('relation', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.relation))

        self.family_member.refresh_from_db()
        self.assertEquals(self.family_member.relation, self.relation)

    def test_update_family_profile(self):
        self.patch_data = dict(profile=self.profile)

        self.response = self.user_client.patch(self.endpoint, data=self.patch_data)
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('profile', None)
        self.assertIsNone(value)
        self.assertNotEquals(value, str(self.profile))

        self.family_member.refresh_from_db()
        self.assertNotEquals(self.family_member.profile, self.profile)

    def test_update_family_first_name(self):
        self.patch_data = dict(first_name=self.first_name)

        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('first_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.first_name))

        self.family_member.refresh_from_db()
        self.assertEquals(self.family_member.first_name, self.first_name)

    def test_update_family_last_name(self):
        self.patch_data = dict(last_name=self.last_name)

        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('last_name', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.last_name))

        self.family_member.refresh_from_db()
        self.assertEquals(self.family_member.last_name, self.last_name)

    def test_update_family_birth_date(self):
        self.patch_data = dict(birth_date=self.birth_date)

        self.response = self.user_client.patch(self.endpoint, data=self.patch_data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.data = self.response.data

        value = self.data.get('birth_date', None)
        self.assertIsNotNone(value)
        self.assertEquals(value, str(self.birth_date))

        self.family_member.refresh_from_db()
        self.assertEquals(self.family_member.birth_date, self.birth_date)


class TestHRMeDeleteFamily(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        email = "other_user@test.com"
        cls.user_password = "test1234"

        new_user = UserFactory(
            email=email,
            password=cls.user_password,
        )
        cls.another_profile = new_user.profile

        cls.family_set = cls.test_user.profile.family_set.all()
        cls.len_family_set = cls.family_set.count()

        cls.family_member = cls.family_set.first()
        cls.family_pk = cls.family_member.pk

        cls.endpoint = reverse('api_users:family-detail', kwargs=dict(user_pk='me', pk=cls.family_member.pk))

    def test_delete_family(self):
        self.response = self.user_client.delete(self.endpoint, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_204_NO_CONTENT)
        self.data = self.response.data

        self.assertEqual(Family.objects.filter(profile__pk=self.test_user.profile.pk).count(),
                         self.len_family_set - 1)

    def test_delete_family_not_permitted(self):
        family_pk = self.another_profile.family_set.all().first().pk
        self.endpoint = reverse('api_users:family-detail', kwargs=dict(user_pk='me', pk=family_pk))
        self.response = self.user_client.delete(self.endpoint, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_404_NOT_FOUND)
