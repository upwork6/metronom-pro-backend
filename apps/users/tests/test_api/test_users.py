
import pytest
from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status

from apps.metronom_commons.test import MetronomBaseAPITestCase


@pytest.mark.skip(reason="No permission system for now")
class TestListUsersNotHR(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        endpoint = reverse('api:user-list')
        cls.response = cls.user_client.get(endpoint, format='json')
        cls.data = cls.response.data

    def test_get_failure(self):
        self.assertEqual(self.response.status_code, status.HTTP_403_FORBIDDEN)


class TestListUsersHR(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.login_as_hr_user()

        endpoint = reverse('api:user-list')
        cls.response = cls.hr_client.get(endpoint, format='json')
        cls.data = cls.response.data

    def test_get_failure(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_get_list(self):
        # admin user + just created user:
        User = get_user_model()
        self.assertEqual(len(self.data), User.objects.all().count())

    def test_get_list_uuids(self):
        User = get_user_model()
        for usr in self.data:
            uuid = usr.get('uuid', None)
            self.assertIsNotNone(uuid)
            self.assertEqual(User.objects.filter(id=uuid).count(), 1)


@pytest.mark.skip(reason="No permission system for now")
class TestDetailUsersNotHR(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        endpoint = reverse('api:user-detail', kwargs={'pk': 'me'})
        cls.response = cls.user_client.get(endpoint, format='json')
        cls.data = cls.response.data

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_403_FORBIDDEN


class TestDetailUsersHR(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.login_as_hr_user()

        endpoint = reverse('api:user-detail', kwargs={'pk': 'me'})
        cls.response = cls.hr_client.get(endpoint, format='json')
        cls.data = cls.response.data

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_data(self):
        self.assertIsNotNone(self.response.data.get('uuid', None))

    def test_response_malformed_user(self):
        endpoint = reverse('api:user-detail', kwargs={'pk': 'undefined_user'})
        response = self.hr_client.get(endpoint, format='json')

        assert response.status_code == status.HTTP_404_NOT_FOUND
