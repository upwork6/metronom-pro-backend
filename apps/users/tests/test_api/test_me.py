from django.urls import reverse
from rest_framework import status

from apps.bankaccounts.tests.factories import BankAccountFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.profile_images.tests.factories import ProfileImageFactory


class BaseMe(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user =  cls.test_user
        bank_account = BankAccountFactory()
        cls.user.bank_account = bank_account
        cls.user.save()

        endpoint = reverse('api:user-detail', kwargs={"pk": 'me'})
        cls.response = cls.user_client.get(endpoint, format='json')
        assert cls.response.status_code == 200
        cls.data = cls.response.data

        cls.tenant_personnel_data = cls.data.get('tenant_personnel_data', None)
        cls.family_members = cls.data.get('family_members', None)
        cls.bank_account = cls.data.get('bank_account', None)

    def test_response_malformed_user(self):
        endpoint = reverse('api:user-detail', kwargs={'pk': 'undefined_user'})
        response = self.user_client.get(endpoint)

        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestMeTenantPersonnelData(BaseMe):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def test_get_profile(self):
        self.assertIsNotNone(self.tenant_personnel_data)

    def test_get_profile_birth_place(self):
        birth_place = self.tenant_personnel_data.get('birth_place', None)
        self.assertIsNotNone(birth_place)

    def test_get_profile_civil_state(self):
        civil_state = self.tenant_personnel_data.get('civil_state', None)
        self.assertIsNotNone(civil_state)

    def test_get_profile_family_members(self):
        self.assertIsNotNone(self.family_members)

    def test_get_profile_ahv_number(self):
        ahv_number = self.tenant_personnel_data.get('ahv_number', None)
        self.assertIsNotNone(ahv_number)
        self.assertEquals(ahv_number, '756.1234.5678.97')

    def test_get_profile_liable_with_witholding_tax(self):
        liable_with_witholding_tax = self.tenant_personnel_data.get('liable_with_witholding_tax', None)
        self.assertIsNotNone(liable_with_witholding_tax)

    def test_get_profile_timezone(self):
        timezone = self.tenant_personnel_data.get('timezone', None)
        self.assertIsNotNone(timezone)

    def test_get_profile_date_joined(self):
        date_joined = self.tenant_personnel_data.get('date_joined', None)
        self.assertIsNotNone(date_joined)
        self.assertEqual(self.test_user.date_joined, date_joined)


class TestMeBankAccount(BaseMe):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def test_get_bank_account(self):
        self.assertIsNotNone(self.bank_account)

    def test_get_bank_account_id(self):
        bank_account_id = self.bank_account.get('id', None)
        self.assertIsNotNone(bank_account_id)

    def test_get_bank_account_public_name(self):
        public_name = self.bank_account.get('public_name', None)
        self.assertIsNotNone(public_name)
