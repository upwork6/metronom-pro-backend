from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from rest_framework.exceptions import ValidationError

from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.users.models import Profile
from apps.users.tests.factories import UserFactory
import pytest


# todo: test HR get profile
# todo: test HR post profile?
# todo: test HR update profile

# todo: test NU get profile
# todo: test NU post profile?
# todo: test NU update profile

# todo: test cross cruds (move test_update_another_user_civil_state)


class AbstractMeProfileTest(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.id = '34'
        cls.birth_place = 'Venice'
        cls.civil_state = Profile.MARRIED
        cls.family_members = '{}'
        cls.ahv_number = '756.9217.0769.85'
        cls.ahv_number_invalid = '756.1234.5678.96'
        cls.ahv_number_invalid_short = '756.1234.5678'

        cls.liable_with_witholding_tax = False
        cls.timezone = 'Europe/Rome'
        cls.date_joined = timezone.now()

        cls.profile = cls.test_user.profile

        cls.endpoint = reverse('api_users:tenant_personnel_data-list', kwargs={'user_pk': 'me'})


class AbstractProfileHRTest(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()


class TestUpdateProfile(AbstractMeProfileTest):
    """
        /api/users/me/tenant_personnel_data/ is a special endpoint without a pk, we have some special handling therefore.
        We can only use a POST, this means we must expect HTTP_201_CREATED as a Response, even when something was updated
    """

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    @pytest.mark.skip("Needs to be cleaned up - something like api/users/me/tenant_personnel_data/00b22200-dc82-49fa-8e95-54f7c88f8023/ is possible")
    def test_update_another_user_civil_state(self):
        self.another_user = UserFactory()

        bad_endpoint = f'/api/users/me/tenant_personnel_data/{self.another_user.profile.pk}/'

        self.assertNotEquals(self.another_user.profile.civil_state, self.civil_state)

        self.patch_data = dict(civil_state=self.civil_state)
        self.response = self.user_client.post(bad_endpoint, data=self.patch_data, format='json')
        self.another_user.profile.refresh_from_db()
        self.test_user.profile.refresh_from_db()

        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self.response.data.get('civil_state', None), self.civil_state)
        self.assertNotEquals(self.another_user.profile.civil_state, self.civil_state)
        self.assertEquals(self.test_user.profile.civil_state, self.civil_state)

    def test_change_id(self):
        self.assertNotEquals(self.profile.id, self.id)

        self.patch_data = dict(id=self.id)
        self.response = self.user_client.post(self.endpoint, data=self.patch_data, format='json')

        self.assertNotEquals(self.response.data.get('id', None), self.id)
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_change_birth_place(self):
        self.assertNotEquals(self.profile.birth_place, self.birth_place)

        self.patch_data = dict(birth_place=self.birth_place)
        self.response = self.user_client.post(self.endpoint, data=self.patch_data, format='json')

        self.assertEqual(self.response.data.get('birth_place', None), self.birth_place)

        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_change_civil_state(self):
        self.assertNotEquals(self.profile.civil_state, self.civil_state)

        self.patch_data = dict(civil_state=self.civil_state)
        self.response = self.user_client.post(self.endpoint, data=self.patch_data, format='json')

        self.assertEqual(self.response.data.get('civil_state', None), self.civil_state)

        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_change_ahv_number_ok(self):
        self.assertNotEquals(self.profile.ahv_number, self.ahv_number)

        self.patch_data = dict(ahv_number=self.ahv_number)
        self.response = self.user_client.post(self.endpoint, data=self.patch_data, format='json')

        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self.response.data.get('ahv_number', None), self.ahv_number)

    def test_change_ahv_number_invalid(self):
        self.assertNotEquals(self.profile.ahv_number, self.ahv_number_invalid)
        patch_data = dict(ahv_number=self.ahv_number_invalid)
        response = self.user_client.post(self.endpoint, data=patch_data, format='json')
        self.assertErrorResponse(
            response,
            {'ahv_number': ['Enter a valid Swiss Social Security number in 756.XXXX.XXXX.XX format.']}
        )

    def test_change_ahv_number_invalid_short(self):
        self.assertNotEquals(self.profile.ahv_number, self.ahv_number_invalid_short)
        patch_data = dict(ahv_number=self.ahv_number_invalid_short)
        response = self.user_client.post(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 400)
        data = response.data['errors']['ahv_number']
        self.assertTrue(
            'Enter a valid Swiss Social Security number in 756.XXXX.XXXX.XX format.' in data
        )
        self.assertTrue(
            'Ensure this value has at least 16 characters (it has 13).' in data
        )

    def test_change_liable_with_witholding_tax(self):
        self.assertNotEquals(self.profile.liable_with_witholding_tax, self.liable_with_witholding_tax)

        self.patch_data = dict(liable_with_witholding_tax=self.liable_with_witholding_tax)
        self.response = self.user_client.post(self.endpoint, data=self.patch_data, format='json')

        self.assertEqual(self.response.data.get('liable_with_witholding_tax', None), self.liable_with_witholding_tax)

        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_change_timezone(self):
        self.assertNotEquals(self.profile.timezone, self.timezone)

        self.patch_data = dict(timezone=self.timezone)
        self.response = self.user_client.post(self.endpoint, data=self.patch_data, format='json')

        self.assertEqual(self.response.data.get('timezone', None), self.timezone)

        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_change_date_joined(self):
        self.assertNotEquals(self.profile.user.date_joined, self.date_joined)

        self.patch_data = dict(date_joined=self.date_joined)
        self.response = self.user_client.post(self.endpoint, data=self.patch_data, format='json')
        self.assertNotEqual(self.response.data.get('date_joined', None), self.date_joined)
        self.assertIsNotNone(self.response.data.get('date_joined', None))

        # strange but if trying to update a read_only field, by design, drf returns 200 status
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_change_family_members(self):
        self.patch_data = dict(family_members=self.family_members)
        self.response = self.user_client.post(self.endpoint, data=self.patch_data, format='json')

        self.assertNotEquals(self.response.data.get('family_members', None), self.family_members)

        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
