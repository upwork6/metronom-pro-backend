from uuid import UUID

from django.http import Http404
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from rest_framework.viewsets import (GenericViewSet)

from apps.users.models import User


class MeUserMixin(GenericViewSet):
    user_lookup_field = 'user_pk'
    me_placeholder = 'me'
    is_user_endpoint = True

    def get_user(self):
        if self.is_user_endpoint:
            lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
            queryset = self.filter_queryset(self.queryset)

            assert lookup_url_kwarg in self.kwargs, (
                'Expected view %s to be called with a URL keyword argument '
                'named "%s". Fix your URL conf, or set the `.lookup_field` '
                'attribute on the view correctly.' %
                (self.__class__.__name__, lookup_url_kwarg)
            )
        else:
            lookup_url_kwarg = self.user_lookup_field
            queryset = User.objects.all()

        user_lookup_field_value = self.kwargs.get(lookup_url_kwarg, None)

        if user_lookup_field_value == self.me_placeholder:
            # user_id = me
            user = self.request.user
        elif user_lookup_field_value:
            # user_id == uuid
            try:
                UUID(user_lookup_field_value, version=4)
            except ValueError:
                # If it's a value error, then the string
                # is not a valid hex code for a UUID.
                raise Http404(_('No User matches the given query.'))
            user = get_object_or_404(queryset, id=user_lookup_field_value)

            # May raise a permission denied
            # TODO: do we really need this check? It make no sense in context of worklogs
            # to check permission for performing an action on the user rather than WorkLog
            # And why we need extra check anyway?
            # self.check_object_permissions(self.request, user)
        else:
            # swagger, redoc or admin pages
            user = self.request.user
        return user


class MeURLMixin(MeUserMixin):
    user_lookup_field = 'user_pk'
    me_placeholder = 'me'
    is_user_endpoint = False
