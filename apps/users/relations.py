
# custom implementation of the PKOnlyObject, from rest_framework.relations


class PKUUIDOnlyObject(object):
    """
    This is a mock object, used for when we only need the uuid of the object
    instance, but still want to return an object with a .uuid attribute,
    in order to keep the same interface as a regular model instance.
    """

    def __init__(self, uuid):
        self.uuid = uuid

    def __str__(self):
        return "%s" % self.uuid
