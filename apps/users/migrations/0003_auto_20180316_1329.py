# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-03-16 13:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20180320_1449'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='role',
            field=models.CharField(choices=[('none', 'No role'), ('customer', 'Customer'), ('freelancer', 'Freelancer'), ('user', 'User'), ('hr', 'HR'), (
                'accounting', 'Accounting'), ('backoffice', 'Backoffice'), ('admin', 'Admin'), ('owner', 'Owner')], default='none', max_length=32, verbose_name='Role for Permission'),
        ),
    ]
