
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from djmoney.models.fields import MoneyField
from djmoney.money import Money

from apps.backoffice.utils import get_setting
from apps.metronom_commons.data import CURRENCY
from apps.metronom_commons.models import (OrderingMixin,
                                          OrderingMixinWithoutSignal,
                                          SoftDeleteUniqueMixin,
                                          UserAccessBackofficeMetronomBaseModel)


class ActivityGroup(UserAccessBackofficeMetronomBaseModel, OrderingMixin):
    """ Model to categorize different kind of activities
    """
    name = models.CharField(max_length=150, verbose_name=_('Name'), unique=True)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = _('Activity group')
        verbose_name_plural = _('Activity groups')

    def make_autocomplete_data(self):
        """ This method serialize data into the dict required by autocomplete

        :returns: list with the dicts
        """
        return_data = []
        for ta in self.templateactivity_set.order_by('position'):
            processed_dict = {str(ta.id): {"name": ta.name, "steps": []}}
            steps = [x.step for x in ta.template_activity_steps.order_by('position')]
            for step in steps:
                steps_data = processed_dict[str(ta.id)]["steps"]
                rate_data = []
                for rate in step.rates.order_by('id'):
                    rate_data.append({
                        'currency': rate.currency,
                        'hourly': str(rate.hourly_rate.amount),
                        'daily': str(rate.daily_rate.amount) if rate.daily_rate else ''
                    })
                data = {str(step.id): {"name": step.name, "rates": rate_data}}
                steps_data.append(data)
            return_data.append(processed_dict)
        return return_data


class Step(SoftDeleteUniqueMixin, UserAccessBackofficeMetronomBaseModel, OrderingMixin):
    """ General Step to achieve. Steps consist on TemplateActivitySteps.
    """
    _unique_fields_to_override_during_deletion = ["name"]

    old_id = models.CharField(max_length=16, blank=True, default="")
    name = models.CharField(max_length=150, verbose_name=_('Name'), unique=True)

    def __str__(self):
        return f'{self.name}'

    def get_default_rate(self):
        """ This method returns rate in the default currency for the step

        :returns: Rate instance or None
        """
        return self.rates.filter(currency=get_setting('DEFAULT_AGENCY_CURRENCY')).last()

    def get_used_currencies(self):
        """ This method returns all attached rate currencies for the step

        :returns: list with the currencies
        :rtype: list
        """
        # TODO: We should refactor this as it adds additional queries to the database
        return list(self.rates.all().values_list('currency', flat=True))

    def rates_in_default_currency(self):
        """ This method gets rates for the step in the default currency

        :returns: rate for the hour and for the day if exist
        :rtype: dict
        """
        default_rate = self.get_default_rate()
        if default_rate:
            return {'hourly_rate': str(default_rate.hourly_rate.amount),
                    'daily_rate': str(default_rate.daily_rate.amount) if default_rate.daily_rate else ''}
        return {}

    class Meta:
        verbose_name = _('Step')
        verbose_name_plural = _('Steps')


class ActivityStepGeneralModel(UserAccessBackofficeMetronomBaseModel, OrderingMixinWithoutSignal):

    HOURS, DAYS = "hourly", "daily"
    UNIT_CHOICES = (
        (HOURS, _("Hourly")),
        (DAYS, _("Daily")),
    )

    dependency = models.ForeignKey("self", blank=True, null=True, on_delete=models.SET_NULL)
    unit = models.CharField(choices=UNIT_CHOICES, default=HOURS, blank=True, max_length=12)

    @property
    def dependency_position(self):
        return self.dependency.position if self.dependency else None

    class Meta:
        abstract = True


class TemplateActivityStep(ActivityStepGeneralModel):
    """TemplateActivityStep for the TemplateActivity. TemplateActivity can have several template_activity_steps and each
    template_activity_step is related to the general Step and may be dependent from the other TemplateActivityStep
    """
    step = models.ForeignKey(Step)
    effort = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)

    class Meta:
        verbose_name = _('Template Activity Step')
        verbose_name_plural = _('Template Activity Steps')

    @staticmethod
    def recalculate_position(sender, instance, created, *args, **kwargs):
        if created:
            position = 0
            qs = sender.objects.exclude(id=instance.id).filter(step=instance.step)
            # in some cases we allow to create objects with pre-defined positions
            if not instance.position or qs.filter(position=instance.position).exists():
                last_instance = qs.order_by('-position').first()
                if last_instance:
                    position = last_instance.position
                instance.position = position + 1
                instance.save(update_fields=['position'])


class TemplateActivity(UserAccessBackofficeMetronomBaseModel, OrderingMixinWithoutSignal):
    """ The main component. Every activity can be assigned and should be done by assignee
    """
    activity_group = models.ForeignKey(ActivityGroup, blank=True, null=True,
                                       on_delete=models.PROTECT)
    name = models.CharField(max_length=150, verbose_name=_('Name'), unique=True)
    description = models.TextField(verbose_name=_('Description'))
    template_activity_steps = models.ManyToManyField(TemplateActivityStep, blank=True)
    last_use = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = _('Template Activity')
        verbose_name_plural = _('Template Activities')

    @property
    def use_count(self):
        return self.projectactivity_set.count()

    @staticmethod
    def recalculate_position(sender, instance, created, *args, **kwargs):
        if created:
            position = 0
            qs = sender.objects.exclude(id=instance.id).filter(activity_group=instance.activity_group)
            # in some cases we allow to create objects with pre-defined positions
            if not instance.position or qs.filter(position=instance.position).exists():
                last_instance = qs.order_by('-position').first()
                if last_instance:
                    position = last_instance.position
                instance.position = position + 1
                instance.save(update_fields=['position'])


class Rate(UserAccessBackofficeMetronomBaseModel):
    """ Model to handle rates for the step.

    """
    currency = models.CharField(choices=CURRENCY.CHOICES, max_length=12, default="CHF")
    daily_rate = MoneyField(blank=True, null=True, decimal_places=2, max_digits=10, default_currency="CHF")
    hourly_rate = MoneyField(decimal_places=2, max_digits=10, default_currency="CHF")
    step = models.ForeignKey(Step, on_delete=models.CASCADE, related_name="rates")

    class Meta:
        verbose_name = _('Rate')
        verbose_name_plural = _('Rates')
        unique_together = ['currency', 'step']

    def __str__(self):
        return f'{self.currency}:{self.hourly_rate}'


@receiver(pre_save, sender=Rate)
def update_currencies(sender, instance, *args, **kwargs):
    """ This signal makes sure that currency of the rate and currency of the MoneyField are always same
    """
    currency = instance.currency
    daily_rate = instance.daily_rate
    hourly_rate = instance.hourly_rate
    if daily_rate and str(daily_rate.currency) != currency:
        instance.daily_rate = Money(daily_rate.amount, currency)
    if str(hourly_rate.currency) != currency:
        instance.hourly_rate = Money(hourly_rate.amount, currency)


post_save.connect(sender=TemplateActivityStep, receiver=TemplateActivityStep.recalculate_position,
                  dispatch_uid="template_activity_step_recalculate_position_signal")
post_save.connect(sender=TemplateActivity, receiver=TemplateActivity.recalculate_position,
                  dispatch_uid="template_activity_recalculate_position_signal")
