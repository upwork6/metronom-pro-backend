
import copy
import uuid

import pytest
from django.urls import reverse
from rest_framework import status

from apps.activities.models import (ActivityGroup, Step, TemplateActivity,
                                    TemplateActivityStep)
from apps.activities.tests.factories import (RateFactory, StepFactory,
                                             TemplateActivityFactory,
                                             TemplateActivityStepFactory)
from apps.backoffice.utils import get_setting
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.tests.factories import ProjectActivityFactory, ProjectFactory


class BaseActivity(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.activity1 = TemplateActivityFactory()
        cls.activity2 = TemplateActivityFactory()
        step = StepFactory()
        RateFactory(currency=get_setting('DEFAULT_AGENCY_CURRENCY'), hourly_rate=40, daily_rate=500, step=step)
        cls.activity2.template_activity_steps.add(TemplateActivityStepFactory(
            dependency=TemplateActivityStepFactory(dependency=None, step=step)))
        cls.activity2.template_activity_steps.add(TemplateActivityStepFactory(dependency=None, step=step))

        cls.independent_workstep = cls.activity2.template_activity_steps.filter(dependency__isnull=True)[0]
        cls.dependent_workstep = cls.activity2.template_activity_steps.filter(dependency__isnull=False)[0]
        cls.example_template_activity_steps = [
            {
                "uuid": str(cls.independent_workstep.id),
                "step": {
                    'uuid': str(cls.independent_workstep.step.id),
                    'name': cls.independent_workstep.step.name,
                    "available_currencies": cls.independent_workstep.step.rates.values_list('currency', flat=True),
                    "used_currencies": cls.independent_workstep.step.get_used_currencies(),
                },
                "unit": cls.independent_workstep.unit,
                "dependency_position": None,
                "position": cls.independent_workstep.position,
                "effort": "5.00"
            },
            {
                "uuid": str(cls.dependent_workstep.id),
                "step": {
                    'uuid': str(cls.dependent_workstep.step.id),
                    'name': cls.dependent_workstep.step.name,
                    "available_currencies": cls.dependent_workstep.step.rates.values_list('currency', flat=True),
                    "used_currencies": cls.dependent_workstep.step.get_used_currencies(),
                },
                "unit": cls.dependent_workstep.unit,
                "dependency_position": cls.dependent_workstep.dependency.position,
                "position": cls.dependent_workstep.position,
                "effort": "5.00"
            }
        ]

        cls.example_template_activity_steps[0]['step']['rates_in_default_currency'] = \
            cls.independent_workstep.step.rates_in_default_currency()
        cls.example_template_activity_steps[0]['step']['rates_in_default_currency'] = \
            cls.dependent_workstep.step.rates_in_default_currency()

    def _compare_template_activity_steps(self, data, example_template_activity_steps=None):
        if example_template_activity_steps is None:
            example_template_activity_steps = self.example_template_activity_steps
        keys = ["unit", "dependency_uuid", "position", "effort"]
        step_keys = ['uuid', 'name',
                     'available_currencies', 'used_currencies']
        for workstep in data:
            for example in example_template_activity_steps:
                if workstep['uuid'] == example['uuid']:
                    step_data = workstep.get("step")
                    step_example = example.get("step")
                    for key in keys:
                        self.assertEqual(workstep.get(key), example.get(key), "Key {} is not the same".format(key))
                    for key in step_keys:
                        example_step_data = list(step_example.get(key))
                        compare_step_data = list(step_data.get(key))
                        self.assertEqual(compare_step_data, example_step_data,
                                         "Key {} is not the same".format(key))
                    break
            else:
                raise AssertionError("There is no example workstep with such uuid")


class TestActivityList(BaseActivity):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse("api:activity-list")
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_get_response_activity_list_status(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_get_response_activity_list_length(self):
        self.assertEqual(len(self.response.data), 2)

    def test_get_response_activity_list_uuid(self):
        ids = [str(self.activity1.id), str(self.activity2.id)]
        self.assertTrue(self.response.data[0]['uuid'] in ids)
        self.assertTrue(self.response.data[1]['uuid'] in ids)

    def test_get_response_activity_list_name(self):
        names = [str(self.activity1.name), str(self.activity2.name)]
        self.assertTrue(self.response.data[0]['name'] in names)
        self.assertTrue(self.response.data[1]['name'] in names)

    def test_get_response_activity_list_description(self):
        descriptions = [str(self.activity1.description), str(self.activity2.description)]
        self.assertTrue(self.response.data[0]['description'] in descriptions)
        self.assertTrue(self.response.data[1]['description'] in descriptions)

    def test_get_response_activity_list_activity_group(self):
        activity_groups = [
            {
                'uuid': str(self.activity1.activity_group.id),
                'name': self.activity1.activity_group.name
            },
            {
                'uuid': str(self.activity2.activity_group.id),
                'name': self.activity2.activity_group.name
            }
        ]
        self.assertTrue(self.response.data[0]['activity_group'] in activity_groups)
        self.assertTrue(self.response.data[1]['activity_group'] in activity_groups)

    def test_get_response_activity_list_updated_date(self):
        updated_dates = [self.activity1.updated_at.isoformat(), self.activity2.updated_at.isoformat()]
        updated_dates = [x[:-6] + 'Z' if x.endswith('+00:00') else x + 'Z' for x in updated_dates]
        self.assertIn(self.response.data[0]['updated_at'], updated_dates)
        self.assertIn(self.response.data[1]['updated_at'], updated_dates)

    def test_get_response_activity_list_use_count(self):
        use_counts = [self.activity1.use_count, self.activity2.use_count]
        self.assertIn(self.response.data[0]['use_count'], use_counts)
        self.assertIn(self.response.data[1]['use_count'], use_counts)

    def test_get_response_activity_list_last_use(self):
        last_uses = [self.activity1.last_use.isoformat(), self.activity2.last_use.isoformat()]
        last_uses = [x[:-6] + 'Z' if x.endswith('+00:00') else x + 'Z' for x in last_uses]
        self.assertIn(self.response.data[0]['last_use'], last_uses)
        self.assertIn(self.response.data[1]['last_use'], last_uses)

    def test_get_response_activity_list_template_activity_steps(self):
        if not len(self.response.data[0]['template_activity_steps']):
            self._compare_template_activity_steps(self.response.data[1]['template_activity_steps'])
        elif not len(self.response.data[1]['template_activity_steps']):
            self._compare_template_activity_steps(self.response.data[0]['template_activity_steps'])
        else:
            raise AssertionError("There should be at least one activity with empty template_activity_steps")

    def test_permissions(self):
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestActivityRetrieve(BaseActivity):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse("api:activity-detail", args=[cls.activity2.id])

        project = ProjectFactory()
        cls.real_use_case = ProjectActivityFactory(used_template_activity=cls.activity2, project=project)
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_get_response_activity_detail_status(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_get_response_activity_detail_uuid(self):
        self.assertEqual(self.response.data['uuid'], str(self.activity2.id))

    def test_get_response_activity_detail_name(self):
        self.assertEqual(self.response.data['name'], self.activity2.name)

    def test_use_count(self):
        self.assertEqual(self.response.data['use_count'], self.activity2.use_count)

    def test_get_response_activity_detail_description(self):
        self.assertEqual(self.response.data['description'], str(self.activity2.description))

    def test_get_response_activity_detail_activity_group(self):
        self.assertEqual(
            self.response.data['activity_group'],
            {
                'uuid': str(self.activity2.activity_group.id),
                'name': self.activity2.activity_group.name
            }
        )

    def test_get_response_activity_detail_template_activity_steps(self):
        self._compare_template_activity_steps(self.response.data['template_activity_steps'])

    def test_get_response_activity_detail_updated_date(self):
        updated_at = self.activity2.updated_at.isoformat()
        updated_at = updated_at[:-6] + 'Z' if updated_at.endswith('+00:00') else updated_at + 'Z'
        self.assertEqual(self.response.data['updated_at'], updated_at)

    def test_get_response_activity_detail_use_count(self):
        self.assertEqual(self.response.data['use_count'], self.activity2.use_count)

    def test_get_response_activity_detail_last_use(self):
        last_use = self.activity2.last_use.isoformat()
        last_use = last_use[:-6] + 'Z' if last_use.endswith('+00:00') else last_use + 'Z'
        self.assertEqual(self.response.data['last_use'], last_use)

    def test_real_use_cases(self):
        self.assertTrue('real_use_cases' in self.response.data.keys())
        real_use_cases = self.response.data['real_use_cases']
        self.assertEqual(len(real_use_cases), 1)
        real_use_case = real_use_cases[0]
        self.assertEqual(real_use_case['display_name'], self.real_use_case.display_name)
        self.assertEqual(real_use_case['description'], self.real_use_case.description)
        self.assertEqual(real_use_case['start_date'], self.real_use_case.start_date.strftime('%Y-%m-%d'))
        self.assertEqual(real_use_case['end_date'], self.real_use_case.end_date.strftime('%Y-%m-%d'))
        self.assertEqual(real_use_case['total_planned_effort_in_hours'],
                         str(self.real_use_case.total_planned_effort_in_hours))
        self.assertEqual(real_use_case['total_real_effort_in_hours'],
                         str(self.real_use_case.total_real_effort_in_hours))
        self.assertEqual(real_use_case['project']['uuid'], self.real_use_case.project.pk)
        self.assertEqual(real_use_case['project']['title'], self.real_use_case.project.title)
        self.assertEqual(real_use_case['project']['company']['uuid'], self.real_use_case.project.company.pk)
        self.assertEqual(real_use_case['project']['company']['name'], self.real_use_case.project.company.name)
        self.assertTrue('activity_steps' in real_use_case.keys())

    def test_permissions(self):
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestActivityCreate(BaseActivity):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse("api:activity-list")
        cls.post_data = {
            "name": "New one",
            "description": "New one",
            "template_activity_steps": [
                {
                    "position": 123,
                    "step_uuid": str(Step.objects.last().id),
                    "dependency_position": None,
                    "unit": "daily",
                },
                {
                    "position": 124,
                    "step_uuid": str(Step.objects.last().id),
                    "dependency_position": 123,
                    "unit": "hourly",
                    "effort": "5.00"
                },
            ],
            "activity_group_uuid": str(ActivityGroup.objects.last().id),
        }
        cls.response = cls.admin_client.post(cls.endpoint, cls.post_data, format="json")
        cls.new_activity = TemplateActivity.objects.exclude(
            id__in=[cls.activity1.id, cls.activity2.id]
        ).last()

    def test_post_response_activity_create_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_actual_dependnecy(self):
        self.assertTrue(any([x.dependency is not None for x in self.new_activity.template_activity_steps.all()]))
        self.assertEqual(self.new_activity.template_activity_steps.filter(
            dependency__isnull=False).last().dependency.position, 123)

    def test_post_response_activity_create_name(self):
        self.assertEqual(self.new_activity.name, self.post_data['name'])

    def test_post_response_activity_create_description(self):
        self.assertEqual(self.new_activity.name, self.post_data['description'])

    def test_post_response_activity_create_template_activity_steps(self):
        positions = [x['position'] for x in self.post_data['template_activity_steps']]
        units = [x['unit'] for x in self.post_data['template_activity_steps']]
        steps = [x['step_uuid'] for x in self.post_data['template_activity_steps']]
        for workstep in self.new_activity.template_activity_steps.all():
            self.assertTrue(workstep.position in positions)
            self.assertTrue(workstep.unit in units)
            self.assertTrue(str(workstep.step_id) in steps)

    def test_post_response_activity_create_activity_group(self):
        self.assertEqual(str(self.new_activity.activity_group.id), self.post_data['activity_group_uuid'])

    def test_post_response_activity_create_without_activity_group(self):
        post_data = copy.deepcopy(self.post_data)
        post_data.pop("activity_group_uuid")
        post_data['name'] = "New one2"
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_response_activity_create_without_template_activity_steps(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['name'] = "New one3"
        post_data.pop("template_activity_steps")
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_response_activity_create_with_wrong_activity_group_uuid(self):
        post_data = copy.deepcopy(self.post_data)
        post_data["name"] = "New original name"
        uuid_key = str(uuid.uuid4())
        post_data['activity_group_uuid'] = uuid_key
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertErrorResponse(
            response,
            {
                'activity_group_uuid': [
                    f'Invalid pk "{uuid_key}" - object does not exist.'
                ]
            }
        )

    def test_post_response_activity_create_with_wrong_workstep_uuid(self):
        post_data = copy.deepcopy(self.post_data)
        post_data["name"] = "New original name"
        uuid_key = str(uuid.uuid4())
        post_data['template_activity_steps'][0]['uuid'] = uuid_key
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertErrorResponse(
            response,
            {'template_activity_steps': [
                {'uuid':
                    {
                        uuid_key: ['There is no TemplateActivityStep with such an uuid']
                    }
                 },
                {}
            ]
            }
        )

    def test_post_response_activity_create_with_wrong_step_uuid(self):
        post_data = copy.deepcopy(self.post_data)
        post_data["name"] = "New original name"
        uuid_key = str(uuid.uuid4())
        post_data['template_activity_steps'][0]['step_uuid'] = uuid_key
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertErrorResponse(
            response,
            {'template_activity_steps':
                [
                    {'step_uuid':
                        [f'Invalid pk "{uuid_key}" - object does not exist.']
                     },
                    {}
                ]
             }
        )

    def test_no_same_position_in_one_activity(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['name'] = 'Some unique name1'
        post_data["template_activity_steps"] = [
            {
                "position": 1,
                "step_uuid": str(Step.objects.last().id),
                "dependency_position": TemplateActivityStep.objects.last().position,
                "unit": "hourly",
                "effort": "5"
            },
            {
                "position": 1,
                "step_uuid": str(Step.objects.last().id),
                "dependency_position": None,
                "unit": "daily",
                "effort": "5"
            }
        ]
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertErrorResponse(
            response,
            {"__all__": ['There is duplicate inside template_activity_steps positions']}
        )

    def test_wrong_position_dependency(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['name'] = 'Some unique name2'
        post_data["template_activity_steps"] = [
            {
                "position": 1234,
                "step_uuid": str(Step.objects.last().id),
                "dependency_position": None,
                "unit": "hourly",
                "effort": "5"
            },
            {
                "position": 1312,
                "step_uuid": str(Step.objects.last().id),
                "dependency_position": 555,
                "unit": "daily",
                "effort": "5"
            }
        ]
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertErrorResponse(
            response, {'__all__':
                       ['There is no Workstep with such position to depend on inside template_activity_steps positions']
                       }
        )

    def test_permissions(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['name'] = "Some unique name 3"
        response = self.user_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        post_data['name'] = "Some unique name 4"
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)

    def test_incorrect_data_processing(self):
        post_data = {
            "name": "New one",
            "description": "New one",
            "template_activity_steps": [{
                'choosenRate': '',
                'position': 1,
                'effort': 5
            }],
            "activity_group_uuid": str(ActivityGroup.objects.last().id),
        }
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestActivityUpdate(BaseActivity):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse("api:activity-detail", args=[cls.activity2.id])
        cls.patch_data = {
            "name": "New one",
            "description": "New one",
            "template_activity_steps": [
                {
                    "uuid": str(cls.activity2.template_activity_steps.all().order_by('created_at')[0].id)
                },
                {
                    "uuid": str(cls.activity2.template_activity_steps.all().order_by('created_at')[1].id)
                },
                {
                    "position": 123,
                    "step_uuid": str(Step.objects.last().id),
                    "dependency_position": None,
                    "unit": "hourly",
                    "effort": "5.00"
                },
                {
                    "position": 124,
                    "step_uuid": str(Step.objects.last().id),
                    "dependency_position": 123,
                    "unit": "daily",
                }
            ],
            "activity_group_uuid": str(ActivityGroup.objects.last().id),
        }
        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format="json")
        cls.updated_activity = TemplateActivity.objects.get(id=cls.activity2.id)

    def test_activity_update_response_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_activity_patch_dependency(self):
        self.assertTrue(any([x.dependency is not None for x in self.updated_activity.template_activity_steps.all()]))
        self.assertIn(123, [x.dependency.position for x
                            in self.updated_activity.template_activity_steps.filter(dependency__isnull=False)])

    def test_activity_patch_name(self):
        self.assertEqual(self.updated_activity.name, self.patch_data['name'])

    def test_activity_patch_description(self):
        self.assertEqual(self.updated_activity.name, self.patch_data['description'])

    def test_activity_patch_activity_group(self):
        self.assertEqual(str(self.updated_activity.activity_group.id), self.patch_data['activity_group_uuid'])

    def test_activity_patch_template_activity_steps(self):
        positions = [x.get('position', '') for x in self.patch_data['template_activity_steps']]
        units = [x.get('unit', '') for x in self.patch_data['template_activity_steps']]
        steps = [x.get('step_uuid') for x in self.patch_data['template_activity_steps']]
        uuids = [x.get('uuid', '') for x in self.patch_data['template_activity_steps']]
        self.assertEqual(self.updated_activity.template_activity_steps.count(), 4)
        for workstep in self.updated_activity.template_activity_steps.all():
            if not str(workstep.id) in uuids:
                self.assertTrue(workstep.position in positions)
                self.assertTrue(workstep.unit in units)
                self.assertTrue(str(workstep.step_id) in steps)

    def test_activity_patch_workstep_position_in_response(self):
        self.assertTrue('dependency_position' in self.response.data.get('template_activity_steps', [])[0])

    def test_activity_patch_template_activity_steps_removal(self):
        activity3 = TemplateActivityFactory()
        activity3.template_activity_steps.add(TemplateActivityStepFactory(
            dependency=TemplateActivityStepFactory(dependency=None)))
        activity3.template_activity_steps.add(TemplateActivityStepFactory(dependency=None))
        activity3.template_activity_steps.add(TemplateActivityStepFactory(dependency=None))
        removed_workstep = activity3.template_activity_steps.all().order_by('created_at')[2]
        patch_data = {
            "template_activity_steps": [
                {
                    "uuid": str(activity3.template_activity_steps.all().order_by('created_at')[2].id),
                    "deleted": True
                },
            ]
        }
        endpoint = reverse("api:activity-detail", args=[activity3.id])
        response = self.admin_client.patch(endpoint, patch_data, format="json")
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        activity3.refresh_from_db()
        self.assertEqual(activity3.template_activity_steps.count(), 2)
        self.assertNotEqual(activity3.template_activity_steps.all()[0].id, removed_workstep.id)
        self.assertNotEqual(activity3.template_activity_steps.all()[1].id, removed_workstep.id)

    def test_activity_patch_template_activity_steps_delete(self):
        activity3 = TemplateActivityFactory()
        activity3.template_activity_steps.add(TemplateActivityStepFactory(
            dependency=TemplateActivityStepFactory(dependency=None)))
        activity3.template_activity_steps.add(TemplateActivityStepFactory(dependency=None))
        activity3.template_activity_steps.add(TemplateActivityStepFactory(dependency=None))
        not_removed_workstep = activity3.template_activity_steps.all().order_by('created_at')[2]
        before = TemplateActivityStep.objects.count()
        patch_data = {
            "template_activity_steps": [
                {
                    "uuid": str(activity3.template_activity_steps.all().order_by('created_at')[0].id),
                    "deleted": True
                },
                {
                    "uuid": str(activity3.template_activity_steps.all().order_by('created_at')[1].id),
                    "deleted": True
                },
                {
                    "uuid": str(activity3.template_activity_steps.all().order_by('created_at')[2].id),
                },
            ]
        }
        endpoint = reverse("api:activity-detail", args=[activity3.id])
        response = self.admin_client.patch(endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        activity3.refresh_from_db()
        self.assertEqual(activity3.template_activity_steps.count(), 1)
        self.assertEqual(activity3.template_activity_steps.all()[0].id, not_removed_workstep.id)
        self.assertEqual(before - 2, TemplateActivityStep.objects.count())

    def test_activity_patch_workstep_patch_position(self):
        ws1 = self.activity2.template_activity_steps.all().order_by('created_at')[0]
        ws2 = self.activity2.template_activity_steps.all().order_by('created_at')[1]
        patch_data = dict()
        patch_data["template_activity_steps"] = [
            {
                "uuid": str(ws1.id),
                "position": 12
            },
            {
                "uuid": str(ws2.id),
                "position": 13
            },
        ]
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        ws1.refresh_from_db()
        ws2.refresh_from_db()
        self.assertEqual(ws1.position, patch_data["template_activity_steps"][0]["position"])
        self.assertEqual(ws2.position, patch_data["template_activity_steps"][1]["position"])

    def test_activity_patch_no_same_position_in_one_activity(self):
        patch_data = dict()
        patch_data["template_activity_steps"] = [
            {
                "uuid": str(self.activity2.template_activity_steps.all().order_by('created_at')[0].id),
                "position": 1
            },
            {
                "uuid": str(self.activity2.template_activity_steps.all().order_by('created_at')[1].id),
                "position": 1
            },
        ]
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(response, {'__all__': ['There is duplicate inside template_activity_steps positions']})

    def test_activity_patch_effort_update(self):
        ws1 = self.activity2.template_activity_steps.all().order_by('created_at')[0]
        ws2 = self.activity2.template_activity_steps.all().order_by('created_at')[1]
        patch_data = dict()
        patch_data["template_activity_steps"] = [
            {
                "uuid": str(ws1.id),
                "effort": "12.00"
            },
            {
                "uuid": str(ws2.id),
                "effort": "13.00"
            },
        ]
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        ws1.refresh_from_db()
        ws2.refresh_from_db()
        self.assertEqual(str(ws1.effort), patch_data["template_activity_steps"][0]["effort"])
        self.assertEqual(str(ws2.effort), patch_data["template_activity_steps"][1]["effort"])

    def test_activity_patch_wrong_dependency_update(self):
        ws1 = self.activity2.template_activity_steps.all().order_by('created_at')[0]
        patch_data = dict()
        patch_data["template_activity_steps"] = [
            {
                "uuid": str(ws1.id),
                "dependency_position": 12221
            },
        ]
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertErrorResponse(
            response,
            {'__all__':
             ['There is no Workstep with such position to depend on inside template_activity_steps positions']
             }
        )

    def test_activity_patch_correct_dependency_update(self):
        ws1 = self.activity2.template_activity_steps.all().order_by('created_at')[0]
        ws2 = self.activity2.template_activity_steps.all().order_by('created_at')[1]
        self.assertNotEqual(ws2.dependency, ws1)
        patch_data = dict()
        patch_data["template_activity_steps"] = [
            {
                "uuid": str(ws1.id),
            },
            {
                "uuid": str(ws2.id),
                "dependency_position": ws1.position
            },
        ]
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        ws2.refresh_from_db()
        self.assertEqual(ws2.dependency, ws1)

    def test_activity_patch_dependency_removal(self):
        ws1 = self.activity2.template_activity_steps.all().order_by('created_at')[0]
        ws2 = self.activity2.template_activity_steps.all().order_by('created_at')[1]
        self.assertNotEqual(ws2.dependency, ws1)
        patch_data = dict()
        patch_data["template_activity_steps"] = [
            {
                "uuid": str(ws1.id),
            },
            {
                "uuid": str(ws2.id),
                "dependency_position": ws1.position
            },
        ]
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        ws2.refresh_from_db()
        self.assertEqual(ws2.dependency, ws1)
        patch_data["template_activity_steps"] = [
            {
                "uuid": str(ws1.id),
            },
            {
                "uuid": str(ws2.id),
                "dependency_position": -1
            },
        ]
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        ws2.refresh_from_db()
        self.assertNotEqual(ws2.dependency, ws1)

    def test_permissions_patch_permissions_template_activity_used(self):
        patch_data = copy.deepcopy(self.patch_data)

        project = ProjectFactory()
        real_use_case = ProjectActivityFactory(used_template_activity=self.activity2, project=project)

        patch_data['name'] = "Some unique name 3"
        response = self.backoffice_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        patch_data['name'] = "Some unique name 4"
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_patch_permissions_template_activity_not_used(self):
        patch_data = copy.deepcopy(self.patch_data)
        patch_data['name'] = "Some unique name 3"
        response = self.user_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        patch_data['name'] = "Some unique name 4"
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_activity_patch_incorrect_data_processing(self):
        patch_data = {
            "description": "New one",
            'template_activity_steps': [{
                'choosenRate': '',
                'position': 1,
                'effort': 5
            }]
        }
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestActivityDelete(BaseActivity):

    def setUp(self):
        super().setUp()
        self.activity3 = TemplateActivityFactory()
        self.activity3.template_activity_steps.add(TemplateActivityStepFactory(dependency=None))
        self.activity3.template_activity_steps.add(TemplateActivityStepFactory(dependency=None))
        self.endpoint = reverse("api:activity-detail", args=[self.activity3.id])

    def test_response(self):
        before = TemplateActivity.objects.count()
        before_template_activity_steps = TemplateActivityStep.objects.count()
        self.response = self.admin_client.delete(self.endpoint, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(before - 1, TemplateActivity.objects.count())
        self.assertEqual(before_template_activity_steps, TemplateActivityStep.objects.count())

    def test_permissions_template_activity_delete_not_used(self):
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)
        activity3 = TemplateActivityFactory()
        endpoint = reverse("api:activity-detail", args=[activity3.id])
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_template_activity_delete_used(self):
        activity3 = TemplateActivityFactory()
        endpoint = reverse("api:activity-detail", args=[activity3.id])
        project = ProjectFactory(billing_currency="CHF")
        ProjectActivityFactory(used_template_activity=activity3, project=project)

        response = self.user_client.delete(endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used TemplateActivity"]
            }
        )
