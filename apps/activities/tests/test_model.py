
from django.db.utils import IntegrityError
from django.test import TestCase

from apps.projects.tests.factories import (ProjectActivityFactory,
                                           ProjectFactory)

from .factories import (ActivityGroupFactory, RateFactory, StepFactory,
                        TemplateActivityFactory, TemplateActivityStepFactory)


class TestActivityModel(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.activity = TemplateActivityFactory()
        cls.activity.template_activity_steps.add(TemplateActivityStepFactory(
            dependency=TemplateActivityStepFactory(dependency=None)))

    def test_use_count(self):
        """ Check that use_count returns 0
        """
        assert self.activity.use_count == 0


class TestRateModel(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.step = StepFactory()
        cls.rate = RateFactory(step=cls.step, currency="EUR")

    def test_factory(self):
        rate = RateFactory(step=self.step)
        self.assertEqual(str(rate.daily_rate.currency), rate.currency)
        rate = RateFactory(step=self.step)
        self.assertEqual(str(rate.daily_rate.currency), rate.currency)
        rate = RateFactory(step=self.step)
        self.assertEqual(str(rate.daily_rate.currency), rate.currency)

    def test_signal_for_existing_currency(self):
        """ Check that django money currency is always the same as currency field
        """

        with self.assertRaises(IntegrityError):
            self.rate.currency = "USD"
            self.rate.hourly_rate.currency = "USD"
            self.rate.daily_rate.currency = "USD"
            self.rate.save()

    def test_signal(self):
        """ Check that django money currency is always the same as currency field
        """

        self.rate.currency = "EUR"
        self.rate.hourly_rate.currency = "CHF"
        self.rate.daily_rate.currency = "CHF"
        self.rate.save()
        self.rate.refresh_from_db()
        self.assertEqual(str(self.rate.hourly_rate.currency), self.rate.currency)
        self.assertEqual(str(self.rate.daily_rate.currency), self.rate.currency)


class TestTemplateActivityUseCount(TestCase):
    def setUp(self):
        super(TestTemplateActivityUseCount, self).setUp()
        self.use_count = 2
        self.activity_group = ActivityGroupFactory()
        self.template_activity = TemplateActivityFactory(activity_group=self.activity_group)
        self.project1 = ProjectFactory(billing_currency="CHF")
        self.used_project_activity1 = ProjectActivityFactory(
            project=self.project1, used_template_activity=self.template_activity
        )
        self.used_project_activity2 = ProjectActivityFactory(
            project=self.project1, used_template_activity=self.template_activity
        )

    def test_used_template_activity_count(self):
        assert self.template_activity.use_count == self.use_count


class TestTemplateActivity(TestCase):

    def test_position_change_depending_on_activity_group_template_activity(self):
        group1 = ActivityGroupFactory()
        group2 = ActivityGroupFactory()
        group1_act1 = TemplateActivityFactory(activity_group=group1)
        group1_act2 = TemplateActivityFactory(activity_group=group1)
        group1_act3 = TemplateActivityFactory(activity_group=group1)
        group2_act1 = TemplateActivityFactory(activity_group=group2)
        group2_act2 = TemplateActivityFactory(activity_group=group2)
        assert group1_act1.position == 1
        assert group1_act2.position == 2
        assert group1_act3.position == 3
        assert group2_act1.position == 1
        assert group2_act2.position == 2


class TestTemplateActivityStep(TestCase):

    def test_position_change_depending_on_step_tempalte_activity_step(self):
        step1 = StepFactory()
        step2 = StepFactory()
        step1_activity1 = TemplateActivityStepFactory(step=step1)
        step1_activity2 = TemplateActivityStepFactory(step=step1)
        step1_activity3 = TemplateActivityStepFactory(step=step1)
        step2_activity1 = TemplateActivityStepFactory(step=step2)
        step2_activity2 = TemplateActivityStepFactory(step=step2)
        assert step1_activity1.position == 1
        assert step1_activity2.position == 2
        assert step1_activity3.position == 3
        assert step2_activity1.position == 1
        assert step2_activity2.position == 2
