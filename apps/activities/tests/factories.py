

import random

import factory
from django.utils import timezone

from apps.activities.models import (ActivityGroup, Rate, Step,
                                    TemplateActivity, TemplateActivityStep)
from apps.metronom_commons.data import CURRENCY


class ActivityGroupFactory(factory.DjangoModelFactory):
    name = factory.Sequence(lambda n: f'ActivityGroup-{n}')

    class Meta:
        model = ActivityGroup
        django_get_or_create = ('name',)


class RateFactory(factory.DjangoModelFactory):
    currency = factory.Iterator([CURRENCY.CHF, CURRENCY.EUR, CURRENCY.USD])
    hourly_rate = factory.LazyAttribute(lambda a: random.choice([70, 80, 90, 100, 150, 180]))
    daily_rate = factory.LazyAttribute(lambda a: random.choice([1000, 1250, 1500, 1800, 2400]))

    class Meta:
        model = Rate
        django_get_or_create = ('currency', 'step')


class StepFactory(factory.DjangoModelFactory):
    name = factory.Sequence(lambda n: f'Step-{n}')

    class Meta:
        model = Step

    @factory.post_generation
    def rates(self, create, extracted, **kwargs):
        if create:
            RateFactory(currency=CURRENCY.CHF, step=self)
            RateFactory(currency=CURRENCY.USD, step=self)
            RateFactory(currency=CURRENCY.EUR, step=self)


class TemplateActivityStepFactory(factory.DjangoModelFactory):
    step = factory.SubFactory(StepFactory)
    unit = factory.Iterator(["hourly", "daily"])
    effort = 5

    class Meta:
        model = TemplateActivityStep


class TemplateActivityStepNiceFactory(factory.DjangoModelFactory):
    position = factory.Sequence(lambda n: 0 + n)
    step = factory.Iterator(Step.objects.order_by("id"))
    unit = factory.Iterator(["hourly", "daily"])
    effort = 5

    class Meta:
        model = TemplateActivityStep


class TemplateActivityNiceFactory(factory.DjangoModelFactory):
    activity_group = factory.SubFactory(ActivityGroupFactory)
    name = factory.Sequence(lambda n: f'TemplateActivity-{n}')
    description = factory.Faker('sentence')
    last_use = timezone.now()

    class Meta:
        model = TemplateActivity

    @factory.post_generation
    def steps(self, create, extracted, **kwargs):
        if create:
            for i in range(0, random.randint(1, 6)):
                step = TemplateActivityStepNiceFactory()
                self.template_activity_steps.add(step)


class TemplateActivityFactory(factory.DjangoModelFactory):
    activity_group = factory.SubFactory(ActivityGroupFactory)
    name = factory.Sequence(lambda n: f'TemplateActivity-{n}')
    description = factory.Faker('sentence')
    last_use = timezone.now()

    class Meta:
        model = TemplateActivity
