import datetime
import json

from django.conf import settings
from django.urls import reverse

from apps.activities.models import Rate, Step
from apps.activities.tests.test_api_group_steps import BaseStep
from apps.backoffice.models import AvailableSetting, Setting
from apps.backoffice.utils import get_metronom_setting


class TestStepCurrencyOverrideSettings(BaseStep):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        AvailableSetting.objects.get_or_create(name="CURRENCY_CHOICES")
        AvailableSetting.objects.get_or_create(name="DEFAULT_AGENCY_CURRENCY")
        cls.setting1 = Setting.objects.create(
            setting_to_override=AvailableSetting.objects.filter(name="CURRENCY_CHOICES").last(),
            value="(('USD','USD'), ('GBP','GBP'))",
            start_date=datetime.date.today()
        )
        cls.setting2 = Setting.objects.create(
            setting_to_override=AvailableSetting.objects.filter(name="DEFAULT_AGENCY_CURRENCY").last(),
            value="'USD'",
            start_date=datetime.date.today()
        )
        for setting in [cls.setting1, cls.setting2]:
            setting.created_by = cls.test_admin
            setting.changed_by = cls.test_admin
            setting.save(update_fields=['created_by', 'changed_by'])
        endpoint = reverse('api:step-detail', args=[cls.step1.id])
        cls.response = cls.admin_client.get(endpoint)

    def test_currency_display_changed(self):
        overrided_currencies_choices = [x[0] for x in (('USD', 'USD'), ('GBP', 'GBP'))]
        self.assertEqual(self.response.data['available_currencies'], overrided_currencies_choices)

    def test_default_currency_changed_in_representation(self):
        new_default_rate = Rate.objects.filter(currency="USD", step=self.step1).last()
        self.assertEqual(self.response.data['rates_in_default_currency']
                         ['hourly_rate'], str(new_default_rate.hourly_rate.amount))

    def test_default_currency_changed_for_creation(self):
        post_data = {
            'name': "Step New Group",
            'public_id': "01001",
            "rates": [{
                'currency': get_metronom_setting('DEFAULT_AGENCY_CURRENCY'),
                'hourly_rate': "100.00"
            }]
        }
        endpoint = reverse('api:step-list')
        response = self.admin_client.post(endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(response, {'__all__': ['You need to provide rate in the default currency.']})
        post_data["rates"][0]["currency"] = 'USD'
        response = self.admin_client.post(endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 201)

    def _compare_data(self, step, data):
        rates = step.rates.all().order_by("-id")
        compare_data = {}
        for rate in rates:
            compare_data[rate.currency] = {
                'hourly': str(rate.hourly_rate.amount),
                'daily': str(rate.daily_rate.amount) if rate.daily_rate else ''
            }
        self.assertEqual(compare_data, data)

    def test_autocomplete_default_setting_overwrite(self):
        united_url = reverse('autocompletes:united')
        response = self.admin_client.get(united_url)
        response_body = json.loads(response.content)['agencies:available_step_rates']
        steps = Step.objects.all()
        for step in steps:
            self._compare_data(step, response_body.get(str(step.id)))
