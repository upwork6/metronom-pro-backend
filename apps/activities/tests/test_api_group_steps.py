
import copy

import pytest
from django.conf import settings
from django.urls import reverse
from rest_framework import status

from apps.activities.models import ActivityGroup, Step
from apps.activities.tests.factories import (ActivityGroupFactory, RateFactory,
                                             StepFactory, TemplateActivityFactory, TemplateActivityStepFactory)
from apps.backoffice.utils import get_metronom_setting, get_setting
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.projects.tests.factories import ProjectActivityStepFactory, ProjectFactory, ProjectActivityFactory


class BaseActivityGroup(MetronomBaseAPITestCase):
    # better have setUpTestData instead of setUpClass
    # https://stackoverflow.com/questions/28084683/accessing-django-test-client-in-setupclass/28100949
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.activity_group1 = ActivityGroupFactory()
        cls.activity_group2 = ActivityGroupFactory()


class TestActivityGroupList(BaseActivityGroup):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:activitygroup-list')
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_activity_groups_list(self):
        assert len(self.response.data) == 2
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        ids = [str(self.activity_group2.id), str(self.activity_group1.id)]
        names = [self.activity_group2.name, self.activity_group1.name]
        self.assertTrue(self.response.data[0]['uuid'] in ids)
        self.assertTrue(self.response.data[1]['uuid'] in ids)
        self.assertTrue(self.response.data[0]['name'] in names)
        self.assertTrue(self.response.data[1]['name'] in names)

    def test_permissions(self):
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestActivityGroupRetrieve(BaseActivityGroup):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:activitygroup-detail', args=[cls.activity_group1.id])
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_activity_group_retrieve(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.response.data['uuid'], str(self.activity_group1.id))
        self.assertEqual(self.response.data['name'], self.activity_group1.name)

    def test_permissions(self):
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestActivityGroupCreate(BaseActivityGroup):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:activitygroup-list')
        cls.post_data = {
            'name': "Activity New Group"
        }
        cls.response = cls.admin_client.post(cls.endpoint, data=cls.post_data, format='json')
        cls.new_activity_group = ActivityGroup.objects.exclude(
            id__in=[cls.activity_group1.id, cls.activity_group2.id]
        )[0]

    def test_activity_group_creation_response_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_new_activity_group_name(self):
        self.assertEqual(self.new_activity_group.name, self.post_data['name'])

    def test_new_activity_group_uuid(self):
        self.assertTrue(self.new_activity_group.id)

    def test_no_empty_name_allowed(self):
        post_data = {
            'name': ""
        }
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_permissions(self):
        post_data = {
            "name": "asd"
        }
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 201)
        post_data = {
            "name": "asd2"
        }
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 201)


class TestActivityGroupUpdate(BaseActivityGroup):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:activitygroup-detail', args=[cls.activity_group1.id])
        cls.patch_data = {
            'name': "Activity Updated Group"
        }
        cls.response = cls.admin_client.patch(cls.endpoint, data=cls.patch_data, format='json')
        cls.updated_activity_group = ActivityGroup.objects.get(id=cls.activity_group1.id)

    def test_activity_group_patch_response_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_updated_activity_group_name(self):
        self.assertEqual(self.updated_activity_group.name, self.patch_data['name'])

    def test_updated_activity_group_uuid(self):
        self.assertTrue(self.updated_activity_group.id)

    def test_no_empty_name_allowed(self):
        post_data = {
            'name': ""
        }
        response = self.admin_client.patch(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_permissions_patch_activity_group_not_used(self):
        patch_data = {
            "name": "asd"
        }
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        patch_data = {
            "name": "asd2"
        }
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_permissions_patch_activity_group_used(self):
        TemplateActivityFactory(activity_group=self.activity_group1)
        patch_data = {
            "name": "asd"
        }
        response = self.backoffice_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        patch_data = {
            "name": "asd"
        }
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)


class TestActivityGroupDelete(BaseActivityGroup):

    def setUp(self):
        super().setUp()
        self.to_delete_activity_group = ActivityGroupFactory()
        self.endpoint = reverse('api:activitygroup-detail', args=[self.to_delete_activity_group.id])

    def test_delete(self):
        before = ActivityGroup.objects.count()
        response = self.admin_client.delete(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(before - 1, ActivityGroup.objects.count())

    def test_permissions_delete_activity_group_not_used(self):
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)
        to_delete_activity_group = ActivityGroupFactory()
        endpoint = reverse('api:activitygroup-detail', args=[to_delete_activity_group.id])
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_delete_activity_group_used(self):
        to_delete_activity_group = ActivityGroupFactory()
        endpoint = reverse('api:activitygroup-detail', args=[to_delete_activity_group.id])
        TemplateActivityFactory(activity_group=to_delete_activity_group)
        response = self.user_client.delete(endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used ActivityGroup"]}
        )


class BaseStep(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.step1 = StepFactory()
        cls.step2 = StepFactory()
        cls.rate1 = RateFactory(step=cls.step1)
        cls.rate2 = RateFactory(step=cls.step1)
        cls.rate3 = RateFactory(step=cls.step1)
        cls.rate4 = RateFactory(step=cls.step1)
        cls.rate5 = RateFactory(step=cls.step2)
        cls.rate6 = RateFactory(step=cls.step2)
        cls.rate7 = RateFactory(step=cls.step2)
        cls.rate8 = RateFactory(step=cls.step2)


class TestStepList(BaseStep):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:step-list')
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_steps_list(self):
        assert len(self.response.data) == 2
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        ids = [str(self.step2.id), str(self.step1.id)]
        names = [self.step2.name, self.step1.name]
        self.assertTrue(self.response.data[0]['uuid'] in ids)
        self.assertTrue(self.response.data[1]['uuid'] in ids)
        self.assertTrue(self.response.data[0]['name'] in names)
        self.assertTrue(self.response.data[1]['name'] in names)
        rates = [x.rates_in_default_currency() for x in [self.step1, self.step2]]
        self.assertTrue(self.response.data[0]['rates_in_default_currency'] in rates)
        self.assertTrue(self.response.data[1]['rates_in_default_currency'] in rates)
        get_used_currencies = [self.step2.get_used_currencies(), self.step1.get_used_currencies()]
        self.assertTrue(self.response.data[0]['used_currencies'] in get_used_currencies)
        self.assertTrue(self.response.data[1]['used_currencies'] in get_used_currencies)

    def test_permissions(self):
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestStepRetrieve(BaseStep):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:step-detail', args=[cls.step1.id])
        cls.response = cls.admin_client.get(cls.endpoint)

    def test_step_group_retrieve(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.response.data['uuid'], str(self.step1.id))
        self.assertEqual(self.response.data['name'], self.step1.name)
        tenant_currencies_choices = [x[0] for x in get_metronom_setting('CURRENCY_CHOICES')]
        self.assertEqual(self.response.data['available_currencies'], tenant_currencies_choices)
        step_rates = self.step1.rates.all()
        rates = [{'id': str(x.id),
                  'currency': x.currency,
                  'hourly_rate': str(x.hourly_rate.amount),
                  'daily_rate': str(x.daily_rate.amount) if x.daily_rate else None}
                 for x in step_rates]
        returned_rates = self.response.data['rates']
        for rate in returned_rates:
            self.assertIn(rate, rates)

    def test_permissions(self):
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)


class TestStepCreate(BaseStep):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:step-list')
        cls.post_data = {
            'name': "Step New Group",
            "rates": [{
                'currency': get_setting('DEFAULT_AGENCY_CURRENCY'),
                'hourly_rate': "100.00"
            }]
        }
        cls.response = cls.admin_client.post(cls.endpoint, data=cls.post_data, format='json')
        cls.new_step = Step.objects.exclude(
            id__in=[cls.step1.id, cls.step2.id]
        )[0]

    def test_step_create_response_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_step_create_new_step_name(self):
        self.assertEqual(self.new_step.name, self.post_data['name'])

    def test_step_create_new_step_uuid(self):
        self.assertTrue(self.new_step.id)

    def test_step_create_rates(self):
        rate = self.new_step.rates.last()
        self.assertEqual(rate.currency, self.post_data['rates'][0]['currency'])
        self.assertEqual(str(rate.hourly_rate.amount), self.post_data['rates'][0]['hourly_rate'])

    def test_step_create_no_empty_name_allowed(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['name'] = ''
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertErrorResponse(response, {'name': ["This field may not be blank."]})

    def test_step_create_no_empty_default_rate(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['name'] = '01001b'
        post_data['rates'] = [{
            'currency': 'USD',
            'hourly_rate': "100.00"
        }]
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        self.assertErrorResponse(response, {'__all__': ["You need to provide rate in the default currency."]})

    def test_step_create_no_currency_duplication_in_rates(self):
        post_data = copy.deepcopy(self.post_data)
        post_data['name'] = '01001c'
        post_data['rates'] = [
            {
                'currency': get_setting('DEFAULT_AGENCY_CURRENCY'),
                'hourly_rate': "100.00"
            },
            {
                'currency': get_setting('DEFAULT_AGENCY_CURRENCY'),
                'hourly_rate': "100.00"
            }
        ]
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        self.assertErrorResponse(response, {'__all__': ["You can't duplicate rates currencies per Step."]})

    def test_step_create_permissions(self):
        post_data = {
            'name': "Step New Group0",
            "rates": [{
                'currency': get_setting('DEFAULT_AGENCY_CURRENCY'),
                'hourly_rate': "100.00"
            }]
        }
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 201)
        post_data["name"] = "Step New Group1"
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_step_create_with_daily_rate(self):
        post_data = {
            "name": 'Step #1',
            "rates": [{
                "currency": get_setting('DEFAULT_AGENCY_CURRENCY'),
                "daily_rate": '5000',
                "hourly_rate": '900'
            }]
        }
        response = self.admin_client.post(self.endpoint, data=post_data, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            float(response.data['rates_in_default_currency']['hourly_rate']),
            float(post_data['rates'][0]['hourly_rate'])
        )
        self.assertEqual(
            float(response.data['rates_in_default_currency']['daily_rate']),
            float(post_data['rates'][0]['daily_rate'])
        )


class TestStepUpdate(BaseStep):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:step-detail', args=[cls.step1.id])
        cls.patch_data = {
            'name': "Step Updated Group"
        }
        cls.response = cls.admin_client.patch(cls.endpoint, data=cls.patch_data, format='json')
        cls.updated_step = Step.objects.get(id=cls.step1.id)

    def test_step_update_response_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_step_update_updated_step_name(self):
        self.assertEqual(self.updated_step.name, self.patch_data['name'])

    def test_step_update_updated_step_uuid(self):
        self.assertTrue(self.updated_step.id)

    def test_step_update_no_empty_name_allowed(self):
        patch_data = {
            'name': ""
        }
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_step_update_add_rate(self):
        self.updated_step.rates.filter(currency="USD").delete()
        before = self.updated_step.rates.all().count()
        patch_data = {
            'rates': [
                {
                    'currency': "USD",
                    'hourly_rate': "121.00"
                },
            ]
        }
        endpoint = reverse('api:step-detail', args=[self.updated_step.id])
        response = self.admin_client.patch(endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.updated_step.refresh_from_db()
        rates = self.updated_step.rates.all().values_list('hourly_rate', flat=True)
        self.assertEqual(len(rates), before + 1)
        self.assertIn(patch_data['rates'][0]['hourly_rate'], [str(x) for x in rates])

    def test_step_update_no_currency(self):
        self.updated_step.rates.filter(currency="USD").delete()
        before = self.updated_step.rates.all().count()
        patch_data = {
            'rates': [
                {
                    'hourly_rate': "121.00"
                },
            ]
        }
        endpoint = reverse('api:step-detail', args=[self.updated_step.id])
        response = self.admin_client.patch(endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_step_update_no_duplication_for_patched_rates(self):
        patch_data = dict()

        patch_data['rates'] = [
            {
                'currency': get_setting('DEFAULT_AGENCY_CURRENCY'),
                'hourly_rate': "100.00"
            },
            {
                'currency': get_setting('DEFAULT_AGENCY_CURRENCY'),
                'hourly_rate': "100.00"
            }
        ]
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertErrorResponse(response, {'__all__': ["You can't duplicate rates currencies per Step."]})

    def test_rate_removing_from_the_step(self):
        rate = self.updated_step.rates.last()
        before = self.updated_step.rates.all().count()
        patch_data = {
            "rates": [
                {
                    "id": rate.id,
                    "remove": True
                }
            ]
        }
        endpoint = reverse('api:step-detail', args=[self.updated_step.id])
        response = self.admin_client.patch(endpoint, data=patch_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.updated_step.rates.all().count(), before - 1)

    def test_rate_change(self):
        rate = self.updated_step.rates.last()
        patch_data = {
            "rates": [
                {
                    "id": rate.id,
                    'currency': rate.currency,
                    'hourly_rate': rate.hourly_rate.amount,
                    'daily_rate': "1.32"
                }
            ]
        }
        endpoint = reverse('api:step-detail', args=[self.updated_step.id])
        response = self.admin_client.patch(endpoint, data=patch_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.updated_step.refresh_from_db()
        rates = self.updated_step.rates.all().values_list('daily_rate', flat=True)
        self.assertIn(patch_data['rates'][0]['daily_rate'], [str(x) for x in rates])

    def test_permissions_step_patch_not_used(self):
        patch_data = {
            'name': "Step Updated Group2"
        }
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_permissions_step_patch_used(self):
        project = ProjectFactory(billing_currency="CHF")
        project_activity = ProjectActivityFactory(project=project)
        project_activity_step = ProjectActivityStepFactory(used_step=self.step1, activity=project_activity)

        patch_data = {
            'name': "Step Updated Group2"
        }
        response = self.backoffice_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertEqual(response.status_code, 200)


class TestStepDelete(BaseStep):

    def setUp(self):
        super().setUp()
        to_delete_step = StepFactory()
        self.endpoint = reverse('api:step-detail', args=[to_delete_step.id])

    def test_response_code(self):
        before = Step.objects.count()
        response = self.admin_client.delete(self.endpoint, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(before - 1, Step.objects.count())

    def test_permissions_delete_step_not_used(self):
        response = self.user_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(self.endpoint)
        self.assertEqual(response.status_code, 204)
        to_delete_step = StepFactory()
        endpoint = reverse('api:step-detail', args=[to_delete_step.id])
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_delete_step_used(self):
        to_delete_step = StepFactory()
        endpoint = reverse('api:step-detail', args=[to_delete_step.id])

        project = ProjectFactory()
        project_activity = ProjectActivityFactory(project=project)
        project_activity_step = ProjectActivityStepFactory(used_step=to_delete_step, activity=project_activity)

        response = self.backoffice_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used Step"]}
        )
        project_activity_step.delete()
        ta_step = TemplateActivityStepFactory(step=to_delete_step)
        response = self.backoffice_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used Step"]}
        )
