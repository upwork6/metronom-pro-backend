import json

from django.core.cache import caches
from django.urls import reverse
from rest_framework import status

from apps.activities.models import Step
from apps.backoffice.utils import get_setting
from apps.metronom_commons.test import MetronomBaseAPITestCase

from .factories import (RateFactory, TemplateActivityFactory,
                        TemplateActivityStepFactory)

cache = caches['autocompletes']


class BaseAutocomplete(MetronomBaseAPITestCase):

    def setUp(self):
        self.activity1 = TemplateActivityFactory()
        self.activity2 = TemplateActivityFactory()
        self.activity2.template_activity_steps.add(TemplateActivityStepFactory(
            dependency=TemplateActivityStepFactory(dependency=None)))
        self.activity2.template_activity_steps.add(TemplateActivityStepFactory(dependency=None))
        self.activity2.template_activity_steps.add(TemplateActivityStepFactory(
            dependency=TemplateActivityStepFactory(dependency=None)))
        self.activity2.template_activity_steps.add(TemplateActivityStepFactory(dependency=None))
        self.activity_group1 = self.activity1.activity_group
        self.activity_group2 = self.activity2.activity_group
        TemplateActivityFactory(activity_group=self.activity_group1)
        TemplateActivityFactory(activity_group=self.activity_group2)
        self.united_url = reverse('autocompletes:united')


class TestActivityGroupAutocomplete(BaseAutocomplete):

    def setUp(self):
        super().setUp()
        cache.clear()
        self.response = self.user_client.get(self.united_url)
        self.response_body = json.loads(self.response.content)['agencies:activity_groups']

    def test_response(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_activity_group1_response(self):
        data = self.response_body.get(str(self.activity_group1.id))
        self.assertEqual(data, str(self.activity_group1.name))
        self.assertEqual(len(self.response_body), self.activity_group1.templateactivity_set.count())

    def test_activity_group2_response(self):
        data = self.response_body.get(str(self.activity_group2.id))
        self.assertEqual(data, str(self.activity_group2.name))
        self.assertEqual(len(self.response_body), self.activity_group2.templateactivity_set.count())


class TestActivityAutocomplete(BaseAutocomplete):

    def setUp(self):
        super().setUp()
        cache.clear()
        self.response = self.user_client.get(self.united_url)
        self.response_body = json.loads(self.response.content)['agencies:activities']

    def test_response(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def _compare_data(self, activity_group, data):
        self.assertEqual(activity_group.make_autocomplete_data(), data)

    def test_activity_group1_activities_response(self):
        data = self.response_body.get(str(self.activity_group1.id))
        self._compare_data(self.activity_group1, data)

    def test_activity_group2_activities_response(self):
        data = self.response_body.get(str(self.activity_group2.id))
        self._compare_data(self.activity_group2, data)


class TestStepAutocomplete(BaseAutocomplete):

    def setUp(self):
        super().setUp()
        cache.clear()
        self.response = self.user_client.get(self.united_url)
        self.response_body = json.loads(self.response.content)['agencies:steps']

    def test_response(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_steps_data(self):
        steps = Step.objects.all()
        for step in steps:
            self.assertEqual(step.name, self.response_body[str(step.id)])


class TestStepRatesAutocomplete(BaseAutocomplete):

    def setUp(self):
        super().setUp()
        for step in Step.objects.all():
            RateFactory(step=step, currency="USD")
            RateFactory(step=step, currency=get_setting('DEFAULT_AGENCY_CURRENCY'))
        cache.clear()
        self.response = self.user_client.get(self.united_url)
        self.response_body = json.loads(self.response.content)['agencies:available_step_rates']

    def test_response(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def _compare_data(self, step, data):
        rates = step.rates.all().order_by("-id")
        compare_data = {}
        for rate in rates:
            compare_data[rate.currency] = {
                'hourly': str(rate.hourly_rate.amount),
                'daily': str(rate.daily_rate.amount) if rate.daily_rate else ''
            }
        self.assertEqual(
            compare_data,
            data
        )

    def test_response_data(self):
        steps = Step.objects.all()
        for step in steps:
            self._compare_data(step, self.response_body.get(str(step.id)))
