# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-04-13 11:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0004_step_old_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='activitygroup',
            name='position',
            field=models.PositiveSmallIntegerField(default=1),
        ),
        migrations.AddField(
            model_name='step',
            name='position',
            field=models.PositiveSmallIntegerField(default=1),
        ),
        migrations.AddField(
            model_name='templateactivity',
            name='position',
            field=models.PositiveSmallIntegerField(default=1),
        ),
    ]
