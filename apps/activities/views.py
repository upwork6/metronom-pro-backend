
from dry_rest_permissions.generics import DRYPermissions
from rest_framework.viewsets import ModelViewSet

from apps.activities.models import ActivityGroup, Step, TemplateActivity, TemplateActivityStep
from apps.activities.serializers import (ActivityGroupSerializer,
                                         StepDetailSerializer, StepSerializer,
                                         TemplateActivitySerializer)
from apps.api.filters import MetronomPermissionsFilter
from apps.projects.models import ProjectActivity, ProjectActivityStep
from apps.metronom_commons.mixins.views import SerializerDispatcherMixin, DeleteForbiddenIfUsedMixin


class ActivityGroupViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    """Activity groups are pooling Activities"""
    queryset = ActivityGroup.objects.order_by("position")
    serializer_class = ActivityGroupSerializer
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    forbid_delete_if_used_in = {
        TemplateActivity: ["activity_group"]
    }


class StepViewSet(DeleteForbiddenIfUsedMixin, SerializerDispatcherMixin, ModelViewSet):
    """Steps for the use in an TemplateActivity"""
    queryset = Step.objects.order_by("position")
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    serializer_class = StepSerializer
    forbid_delete_if_used_in = {
        ProjectActivityStep: ["used_step"],
        TemplateActivityStep: ["step"]
    }

    serializers_dispatcher = {
        'retrieve': StepDetailSerializer,
        'create': StepDetailSerializer,
        'update': StepDetailSerializer,
        'partial_update': StepDetailSerializer,
    }


class TemplateActivityViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    """Activities for the use in a project"""
    serializer_class = TemplateActivitySerializer
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    forbid_delete_if_used_in = {
        ProjectActivity: ["used_template_activity"]
    }

    def get_queryset(self):
        return self.get_serializer().setup_eager_loading(TemplateActivity.objects.order_by('position'))
