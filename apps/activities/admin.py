# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import (ActivityGroup, Rate, Step, TemplateActivity,
                     TemplateActivityStep)


@admin.register(ActivityGroup)
class ActivityGroupAdmin(admin.ModelAdmin):
    list_display = ('created_at', 'updated_at', 'id', 'name')
    list_filter = ('created_at', 'updated_at')
    search_fields = ('name',)


class RateInline(admin.TabularInline):
    model = Rate
    extra = 0


@admin.register(Step)
class StepAdmin(admin.ModelAdmin):
    list_display = ('created_at', 'updated_at', 'id', 'name')
    list_filter = ('created_at', 'updated_at')
    search_fields = ('name',)
    inlines = (RateInline, )


@admin.register(TemplateActivityStep)
class TemplateActivityStepAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'position',
        'step',
        'dependency',
        'unit',
    )
    list_filter = ('created_at', 'updated_at', 'step', 'dependency')


@admin.register(TemplateActivity)
class TemplateActivityAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'id',
        'activity_group',
        'name',
        'description',
        'last_use',
    )
    list_filter = (
        'created_at',
        'updated_at',
        'activity_group',
        'last_use',
    )
    raw_id_fields = ('template_activity_steps',)
    search_fields = ('name',)
