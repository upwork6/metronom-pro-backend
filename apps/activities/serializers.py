""" Module for the serialziers inside activities library
"""
from django.utils.translation import ugettext_lazy as _
from djmoney.money import Money
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from apps.projects.base_serializers import BaseSimpleSerializer
from apps.activities.models import (ActivityGroup, Rate, Step,
                                    TemplateActivity, TemplateActivityStep)
from apps.backoffice.utils import get_setting
from apps.projects.base_serializers import (BaseProjectActivitySerializer,
                                            RateSerializer,
                                            SimpleStepSerializer)
from apps.projects.models import ProjectActivity, ProjectActivityStep
from apps.metronom_commons.serializers import SoftDeletionSerializer


class ActivityGroupSerializer(serializers.ModelSerializer):
    """ Serialzier for the ActivityGroup ViewSet
    """
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)
    name = serializers.CharField(required=False)

    class Meta:
        model = ActivityGroup
        fields = ['uuid', 'name']


class StepSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    """ Simple serializer for the list endpoint inside Step ViewSet
    """
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)
    name = serializers.CharField(required=False, validators=[UniqueValidator(queryset=Step.objects.all())])
    available_currencies = serializers.SerializerMethodField(read_only=True)
    rates_in_default_currency = serializers.SerializerMethodField(read_only=True)
    used_currencies = serializers.SerializerMethodField(read_only=True)

    def get_available_currencies(self, obj):
        return obj.rates.values_list('currency', flat=True)

    def get_rates_in_default_currency(self, obj):
        return obj.rates_in_default_currency()

    def get_used_currencies(self, obj):
        return obj.get_used_currencies()

    class Meta:
        model = Step
        fields = ['uuid', 'name',
                  'available_currencies', 'rates_in_default_currency', 'used_currencies']


class StepDetailSerializer(SoftDeletionSerializer, serializers.ModelSerializer):
    """ Simple serializer for the detailed routes inside Step ViewSet 
    """
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)
    name = serializers.CharField(required=True, validators=[UniqueValidator(queryset=Step.objects.all())])
    available_currencies = serializers.SerializerMethodField(read_only=True)
    rates_in_default_currency = serializers.SerializerMethodField(read_only=True)
    rates = RateSerializer(many=True)

    def get_available_currencies(self, obj):
        return [x[0] for x in get_setting('CURRENCY_CHOICES')]

    def get_rates_in_default_currency(self, obj):
        return obj.rates_in_default_currency()

    def validate(self, data):
        data = super().validate(data)
        currencies_with_ids = [(x.get('id'), x.get('currency')) for x in data.get('rates', [])]
        currencies_without_ids = [x[1] for x in currencies_with_ids]
        all_currencies = []
        has_error = get_setting('DEFAULT_AGENCY_CURRENCY') not in currencies_without_ids
        if has_error and self.context['view'].action != 'partial_update':
            raise serializers.ValidationError(_("You need to provide rate in the default currency."))
        elif self.context['view'].action == 'partial_update':
            all_currencies = [x for x in self.instance.rates.all().values_list('currency', flat=True)]
        for currency in currencies_with_ids:
            # We need to check that there is no duplication in new data
            # and there is no duplication at the existing data (excluding editing already added rates)
            if currencies_without_ids.count(currency[1]) > 1 or (
                    all_currencies.count(currency[1]) > 0 and not currency[0]):
                raise serializers.ValidationError(_("You can't duplicate rates currencies per Step."))
        for x in data.get('rates', []):
            if not x.get('id') and not x.get('currency'):
                raise serializers.ValidationError(_("You can't create rates without currency"))
        return data

    def create(self, data):
        """ Because of nested serializers we need to write custom create method
        """
        step = Step.objects.create(
            name=data['name'],
        )
        for rate_data in data['rates']:
            daily = Money(rate_data['daily_rate'], rate_data['currency']) if rate_data.get('daily_rate') else None
            # we need this to avoid duplication of the same data inside the database
            qs = Rate.objects.filter(
                step=step,
                currency=rate_data['currency'],
                hourly_rate=Money(rate_data['hourly_rate'], rate_data['currency']),
                daily_rate=daily
            )
            if qs.exists():
                rate = qs.latest('id')
            else:
                rate = Rate.objects.create(
                    step=step,
                    currency=rate_data['currency'],
                    hourly_rate=Money(rate_data['hourly_rate'], rate_data['currency']),
                    daily_rate=daily
                )
        return step

    def update(self, instance, data):
        """ Because of nested serializers we need to write custom update method
        """
        data_rates = data.pop('rates', [])
        if data_rates:
            to_remove_rates = []
            for rate_data in data_rates:
                if rate_data.get('id'):
                    rate = Rate.objects.get(id=rate_data.get('id'))
                    if rate_data.get("remove"):
                        rate.delete()
                    else:
                        # We can't easy validate change of the currency for the rate
                        # so we allow to change only rates, not the currency
                        rate.hourly_rate = rate_data.get('hourly_rate')
                        rate.daily_rate = rate_data.get('daily_rate')
                        rate.save(update_fields=['hourly_rate', 'daily_rate'])
                else:
                    daily = Money(rate_data['hourly_rate'], rate_data['currency']
                                  ) if rate_data.get('daily_rate') else None
                    rate = Rate.objects.create(
                        step=instance,
                        currency=rate_data['currency'],
                        hourly_rate=Money(rate_data['hourly_rate'], rate_data['currency']),
                        daily_rate=daily
                    )
        return super().update(instance, data)

    class Meta:
        model = Step
        fields = ['uuid', 'name', 'available_currencies', 'rates', 'rates_in_default_currency']


class ActivityGroupSerializerActivity(BaseSimpleSerializer):
    """ Nested serializer inside ActivitySerializer is a bit different from the
    ActivityGroupSerializer(default)

    """
    uuid = serializers.UUIDField(source="id", required=False)
    name = serializers.CharField(required=False)

    class Meta:
        model = ActivityGroup
        fields = ['uuid', 'name']


class TemplateActivityStepSerializer(BaseSimpleSerializer):
    """ Nested serializer to handle TemplateActivitySteps
    """
    uuid = serializers.UUIDField(source="id", required=False)
    step = StepSerializer(required=False, read_only=True)
    step_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Step.objects.all(),
        source="step",
        required=True,
        write_only=True
    )
    dependency_position = serializers.IntegerField(required=False, allow_null=True)
    removed = serializers.BooleanField(required=False)  # remove fron the activity
    deleted = serializers.BooleanField(required=False)  # delete from the database

    def validate(self, data):
        # if we don't really patch the object, some fields are required
        if not data.get('id'):
            if not data.get('step'):
                raise serializers.ValidationError(_("Step_uuid is required for the creation of TemplateActivityStep"))
            if not data.get('position'):
                raise serializers.ValidationError(_("Position is required for the creation of TemplateActivityStep"))
        return data

    class Meta:
        model = TemplateActivityStep
        fields = ["uuid", "position", "step", "unit", "dependency_position",
                  "removed", "deleted", "step_uuid", "effort"]


class ProjectActivityTemplateSerializer(BaseProjectActivitySerializer):
    """ This is nested serializer for the `real_use_cases` inside TemplateActivitySerializer
    """
    project = serializers.SerializerMethodField()

    def get_project(self, obj):
        return {
            'uuid': obj.project.pk,
            'title': obj.project.title,
            'company': {
                'uuid': obj.project.company.pk,
                'name': obj.project.company.name
            }
        }

    class Meta(BaseProjectActivitySerializer.Meta):
        fields = BaseProjectActivitySerializer.Meta.fields + [
            "project",
        ]


class TemplateActivitySerializer(serializers.ModelSerializer):
    """ Serializer for the TemplateActivity ViewSet
    """
    uuid = serializers.UUIDField(source="id", required=False, read_only=True)
    activity_group = ActivityGroupSerializerActivity(required=False, read_only=True)
    activity_group_uuid = serializers.PrimaryKeyRelatedField(
        queryset=ActivityGroup.objects.all(),
        source="activity_group",
        required=False,
        allow_null=True,
        write_only=True
    )
    template_activity_steps = TemplateActivityStepSerializer(many=True, required=False)
    updated_at = serializers.DateTimeField(format='iso-8601', read_only=True)
    last_use = serializers.DateTimeField(format='iso-8601', read_only=True)
    real_use_cases = ProjectActivityTemplateSerializer(source='projectactivity_set', read_only=True, many=True)

    @staticmethod
    def setup_eager_loading(queryset):
        """ Perform necessary eager loading of data. """
        # select_related for "to-one" relationships
        queryset = queryset.select_related(
            'activity_group',
        )

        # prefetch_related for "to-many" relationships
        queryset = queryset.prefetch_related(
            'template_activity_steps',
            'template_activity_steps__step',
            'projectactivity_set',
            'projectactivity_set__project',
            'projectactivity_set__activity_steps',
            'projectactivity_set__activity_steps__assignments',
            'projectactivity_set__activity_steps__assignments__worker',
            'projectactivity_set__activity_steps__used_step',
            'projectactivity_set__activity_group',
        )

        return queryset

    def __init__(self, *args, **kwargs):

        super(TemplateActivitySerializer, self).__init__(*args, **kwargs)
        request = self.context.get("request")

        # Adding ?exclude=real_use_cases excludes this field from the list of fields
        if request and request.query_params.get('exclude'):
            exclude = request.query_params.get('exclude')
            if exclude:
                excluded = exclude.split(',')

            for field_name in excluded:
                self.fields.pop(field_name)

    class Meta:
        model = TemplateActivity
        fields = [
            'activity_group_uuid',
            'activity_group',
            'description',
            'last_use',
            'name',
            'real_use_cases',
            'template_activity_steps',
            'updated_at',
            'use_count',
            'uuid',
        ]

    def validate(self, data):
        data = super().validate(data)
        if data.get('template_activity_steps', []):
            # we need to make sure there is no duplicates in the position
            positions = [x.get("position") for x in data['template_activity_steps'] if x.get("position")]
            if set([x for x in positions if positions.count(x) > 1]):
                raise serializers.ValidationError(_("There is duplicate inside template_activity_steps positions"))
            for w_data in data['template_activity_steps']:
                action_type = self.context['view'].action
                # We need to set the dependancy via the position inside the activity,
                # so we need complex validation logic
                if action_type == "partial_update" and w_data.get("dependency_position") and w_data.get(
                        "dependency_position") != -1:
                    # in case if it is partial update we don't have the full data, so need to rely on instance
                    instance = self.instance
                    if not TemplateActivityStep.objects.filter(
                            position=w_data.get("dependency_position"),
                            templateactivity__id=instance.id).exists() and \
                            w_data.get("dependency_position") not in positions:
                        raise serializers.ValidationError(
                            _("There is no Workstep with such position "
                              "to depend on inside template_activity_steps positions"))
                elif action_type == "create" and w_data.get("dependency_position"):
                    # in case of creation we need just to make sure that the position we need is inside the given list
                    if w_data.get("dependency_position") not in positions:
                        raise serializers.ValidationError(
                            _("There is no Workstep with such position "
                              "to depend on inside template_activity_steps positions"))
        return data

    def create(self, validated_data):
        """ Because of nested serializers we need to write custom create method
        """
        template_activity_steps_data = validated_data.pop('template_activity_steps', [])
        activity = super().create(validated_data)
        for template_activity_step_data in template_activity_steps_data:
            serializer = TemplateActivityStepSerializer(
                data=template_activity_step_data, context={'activity': activity.id})
            if not template_activity_step_data.get("id"):
                dependency = template_activity_step_data.pop('dependency_position', None)
                if dependency and dependency != -1:
                    dependency = activity.template_activity_steps.filter(
                        position=dependency).last()
                workstep = serializer.create(template_activity_step_data)
                if dependency == -1:
                    workstep.dependency = None
                else:
                    workstep.dependency = dependency
                workstep.save(update_fields=['dependency'])
                activity.template_activity_steps.add(workstep)
            else:
                workstep = TemplateActivityStep.objects.get(id=template_activity_step_data.get("id"))
                dependency = template_activity_step_data.pop('dependency_position', None)
                if dependency:
                    dependency = activity.template_activity_steps.filter(
                        position=dependency).last()
                serializer.update(workstep, template_activity_step_data)
                workstep.dependency = dependency
                workstep.save(update_fields=['dependency'])
        return activity

    def update(self, instance, validated_data):
        """ Because of nested serializers we need to write custom update method
        """
        template_activity_steps_data = validated_data.pop('template_activity_steps', [])
        for template_activity_step_data in template_activity_steps_data:
            serializer = TemplateActivityStepSerializer(
                data=template_activity_step_data, context={'activity': instance.id})
            if not template_activity_step_data.get("id"):
                dependency = template_activity_step_data.pop('dependency_position', None)
                if dependency and dependency != -1:
                    dependency = instance.template_activity_steps.filter(
                        position=dependency).last()
                workstep = serializer.create(template_activity_step_data)
                if dependency == -1:
                    workstep.dependency = None
                else:
                    workstep.dependency = dependency
                workstep.save(update_fields=['dependency'])
                instance.template_activity_steps.add(workstep)
            else:
                workstep = TemplateActivityStep.objects.get(id=template_activity_step_data.get("id"))
                dependency = template_activity_step_data.pop('dependency_position', None)
                if dependency:
                    dependency = instance.template_activity_steps.filter(
                        position=dependency).last()
                serializer.update(workstep, template_activity_step_data)
                workstep.dependency = dependency
                workstep.save(update_fields=['dependency'])
                if template_activity_step_data.get("deleted"):
                    instance.template_activity_steps.remove(workstep)
                    workstep.delete()
        return super().update(instance, validated_data)
