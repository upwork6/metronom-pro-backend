from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class EmailConfig(AppConfig):
    name = 'apps.emails'
    verbose_name = _('Emails')