from rest_framework import serializers

from .models import Email


class EmailSerializer(serializers.ModelSerializer):
    """Serialize data from the User."""

    class Meta:
        model = Email
        fields = (
            'email',
        )
