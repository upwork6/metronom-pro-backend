import factory

from ..models import Email


class EmailFactory(factory.django.DjangoModelFactory):
    email = factory.Faker('free_email', locale='de_DE')

    class Meta:
        model = Email
