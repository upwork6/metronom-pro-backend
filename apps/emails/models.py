from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.metronom_commons.models import UserAccessMetronomBaseModel


class Email(UserAccessMetronomBaseModel):

    email = models.EmailField(_('e-mail address'))

    class Meta:
        ordering = ['email']
        verbose_name = _('Email')
        verbose_name_plural = _('Emails')

    def __str__(self):
        return self.email
