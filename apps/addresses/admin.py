from django.contrib import admin

from .models import Address


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'raw',
        'formatted',
        'address1',
        'address2',
        'postal_code',
        'city',
        'country',
        'address_status',
        'notes',
    )
    search_fields = ('name',)
