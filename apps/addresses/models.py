from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField

from apps.metronom_commons.models import UserAccessMetronomBaseModel


class Address(UserAccessMetronomBaseModel):

    ACTIVE = 'active'
    RESTRICTED = 'restricted'
    ARCHIVED = 'archived'

    ADDRESS_STATUS_CHOICES = (
        (ACTIVE, _('active')),
        (RESTRICTED, _('restricted')),
        (ARCHIVED, _('archived')),
    )

    name = models.CharField(_('Name'), max_length=100, blank=True)

    raw = models.TextField(_('Raw address'), blank=True)
    formatted = models.TextField(_('Formatted address'), blank=True)

    address1 = models.CharField(_('Address line 1'), max_length=256, blank=True)
    address2 = models.CharField(_('Address line 2'), max_length=256, null=True, blank=True)

    postal_code = models.CharField(_('Postal Code'), max_length=32)
    city = models.CharField(_('City'), max_length=128)
    country = CountryField(default='')

    lon = models.FloatField(_('Longitude'), null=True, blank=True)
    lat = models.FloatField(_('Latitude'), null=True, blank=True)

    address_status = models.CharField(
        _('Status of address'),
        max_length=12,
        choices=ADDRESS_STATUS_CHOICES,
        default=ACTIVE)
    notes = models.TextField(_('Notes'), blank=True)

    class Meta:
        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')

    def __str__(self):
        if self.address1:
            return f'{self.address1}, {self.postal_code} {self.city}'
        else:
            return f'{self.postal_code} {self.city}'
