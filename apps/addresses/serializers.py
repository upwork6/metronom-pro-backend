from rest_framework import serializers
from .models import Address


class AddressSerializer(serializers.ModelSerializer):
    """Serialize data from the Address."""
    # `country` field is a custom `CountryField at the model level and  does not support well `blank=True`
    # we add here `required=False`: https://github.com/jensneuhaus/metronom-pro-backend/issues/311#issue-316165470
    country = serializers.CharField(required=False)

    class Meta:
        model = Address
        fields = (
            'id',
            'name',
            'raw',
            'formatted',
            'address1',
            'address2',
            'postal_code',
            'city',
            'country',
            'lon',
            'lat',
            'address_status',
            'notes',
        )
