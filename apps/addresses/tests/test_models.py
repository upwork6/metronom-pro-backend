from apps.addresses.tests.factories import AddressFactory
from apps.addresses.models import Address
from django.test import TestCase


class TestCreateAddress(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.address = 'Wehntalerstrasse 546',
        cls.postal_code = '8046',
        cls.city = 'Zurich',
        cls.country = 'CH'

    def test_create_address(self):
        new_address = Address.objects.create(
            address1=self.address,
            postal_code=self.postal_code,
            city=self.city,
            country=self.country
        )
        self.assertEqual(new_address.id, Address.objects.first().id)
        self.assertEqual(new_address.address1, self.address)
        self.assertEqual(new_address.postal_code, self.postal_code)
        self.assertEqual(new_address.city, self.city)
        self.assertEqual(new_address.country, self.country)

    def test_create_address_without_address1(self):
        address_without_address1 = Address.objects.create(
            postal_code=self.postal_code,
            city=self.city,
            country=self.country
        )
        self.assertEqual(address_without_address1.id, Address.objects.first().id)
        self.assertEqual(address_without_address1.address1, '')

    def test_create_address_only_with_address1(self):
        address_only_with_address1 = Address.objects.create(
            address1=self.address
        )
        self.assertEqual(address_only_with_address1.id, Address.objects.first().id)
        self.assertEqual(address_only_with_address1.address1, self.address)
        self.assertEqual(address_only_with_address1.city, '')
        self.assertEqual(address_only_with_address1.postal_code, '')

    def test_create_address_without_postal_code(self):
        address_without_address1 = Address.objects.create(
            address1=self.address,
            city=self.city,
            country=self.country
        )
        self.assertEqual(address_without_address1.id, Address.objects.first().id)
        self.assertEqual(address_without_address1.postal_code, '')

    def test_create_address_only_with_postal_code(self):
        address_only_with_postal_code = Address.objects.create(
            postal_code=self.postal_code
        )
        self.assertEqual(address_only_with_postal_code.id, Address.objects.first().id)
        self.assertEqual(address_only_with_postal_code.postal_code, self.postal_code)
        self.assertEqual(address_only_with_postal_code.city, '')
        self.assertEqual(address_only_with_postal_code.address1, '')

    def test_create_address_without_city(self):
        address_without_address1 = Address.objects.create(
            address1=self.address,
            postal_code=self.postal_code,
            country=self.country
        )
        self.assertEqual(address_without_address1.id, Address.objects.first().id)
        self.assertEqual(address_without_address1.city, '')

    def test_create_address_only_with_city(self):
        address_only_with_city = Address.objects.create(
            city=self.city
        )
        self.assertEqual(address_only_with_city.id, Address.objects.first().id)
        self.assertEqual(address_only_with_city.city, self.city)
        self.assertEqual(address_only_with_city.postal_code, '')
        self.assertEqual(address_only_with_city.address1, '')

    def test_create_address_without_country(self):
        address_without_address1 = Address.objects.create(
            address1=self.address,
            postal_code=self.postal_code,
            city=self.city
        )
        self.assertEqual(address_without_address1.id, Address.objects.first().id)
        self.assertEqual(address_without_address1.country, '')

    def test_create_address_only_with_country(self):
        address_only_with_country = Address.objects.create(
            country="DE"
        )
        self.assertEqual(address_only_with_country.id, Address.objects.first().id)
        self.assertEqual(address_only_with_country.country, 'DE')
