import factory

from ..models import Address


class AddressFactory(factory.django.DjangoModelFactory):

    address1 = factory.Faker('street_address', locale="de_DE")
    postal_code = factory.Faker('postcode', locale="de_DE")
    city = factory.Faker('city', locale="de_DE")
    country = factory.Iterator(['DE', 'CH', 'AT', 'CH', 'CH', 'CH'])

    class Meta:
        model = Address
