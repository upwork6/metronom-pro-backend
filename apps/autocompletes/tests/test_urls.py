from django.urls import reverse
from rest_framework import status

from apps.metronom_commons.test import MetronomBaseAPITestCase


class TestAutocompleteURLs(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.person_endpoint = reverse('autocompletes:persons')
        cls.base_endpoint = reverse('autocompletes:united')

        cls.methods = (
            cls.user_client.post,
            cls.user_client.put,
            cls.user_client.delete,
            cls.user_client.patch,
        )
        cls.endpoints = (
            cls.base_endpoint,
            cls.person_endpoint,
        )

    def test_methods_not_allowed(self):
        for method in self.methods:
            for endp in self.endpoints:
                print('%s,%s' % (method, endp))
                response = method(endp)
                self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
