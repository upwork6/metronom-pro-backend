
import json

import pytest
from django.core.cache import caches
from django.core.urlresolvers import reverse
from django.db.models import Value
from django.db.models.functions import Concat
from rest_framework import status

from apps.autocompletes.views import UnitedAutocompleteView
from apps.backoffice.models import TermsCollection, TermsParagraph
from apps.backoffice.tests.factories import (TermsCollectionFactory,
                                             TermsParagraphCollectionFactory,
                                             TermsParagraphFactory)
from apps.companies.models import Company
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.metronom_commons.data import REOCURRING_TYPES
from apps.persons.models import Person
from apps.users.models import Profile

cache = caches['autocompletes']


class TestUnitedAutocompleteView(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.base_endpoint = reverse('autocompletes:united')
        cache.clear()
        cls.response = cls.user_client.get(cls.base_endpoint)
        assert cls.response.status_code == status.HTTP_200_OK
        cls.response_body = json.loads(cls.response.content)

    def test_types(self):
        self.assertIsInstance(self.response_body, dict)
        returned_list_sources = list(self.response_body.keys())
        right_list_sources = list(UnitedAutocompleteView().sources.keys())
        self.assertEqual(returned_list_sources, right_list_sources)

    @pytest.mark.skip(reason="Not working for some reasons now. No team exists now")
    def test_caching(self):
        returned_teams = self.response_body['agencies:teams']
        returned_team_names = set(returned_teams.values())
        self.assertEqual(returned_team_names, self.right_team_names)

        # Team.objects.all().update(name=Concat('name', Value(' changed')))

        # Clear queryset cache
        response = self.user_client.get(self.base_endpoint)
        response_body = json.loads(response.content)
        returned_teams = response_body['agencies:teams']
        returned_team_names = set(returned_teams.values())
        # right_team_names_changed = {str(t.name) for t in Team.objects.all()}
        self.assertNotEqual(returned_team_names, right_team_names_changed)
        self.assertEqual(returned_team_names, self.right_team_names)

        # Let's check cache clearing after update of model instance.
        # team = Team.objects.all()[0]

        # team.save()
        # Clear queryset cache
        self.response = self.user_client.get(self.base_endpoint)
        response_body = json.loads(self.response.content)
        returned_teams = response_body['agencies:teams']
        returned_team_names = set(returned_teams.values())
        self.assertEqual(returned_team_names, right_team_names_changed)

    @pytest.mark.skip(reason="Caching not working for some reasons")
    def test_etag(self):
        etag_cache_key = UnitedAutocompleteView.get_etag_cache_key()
        etag = cache.get(etag_cache_key, '')

        self.response = self.user_client.get(self.base_endpoint, HTTP_IF_NONE_MATCH=etag)
        self.assertEqual(self.response.status_code, status.HTTP_304_NOT_MODIFIED)


class TestUnitedAutocompleteViewFields(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('autocompletes:united')

    def setUp(self, *args, **kwargs):
        super().setUp(*args, **kwargs)
        paragraphs = TermsParagraphFactory.create_batch(size=3)
        terms_collections = TermsCollectionFactory.create_batch(size=2)
        TermsParagraphCollectionFactory(terms_paragraph=paragraphs[0], terms_collection=terms_collections[0])
        TermsParagraphCollectionFactory(terms_paragraph=paragraphs[1], terms_collection=terms_collections[0])
        TermsParagraphCollectionFactory(terms_paragraph=paragraphs[2], terms_collection=terms_collections[0])
        TermsParagraphCollectionFactory(terms_paragraph=paragraphs[0], terms_collection=terms_collections[1])
        TermsParagraphCollectionFactory(terms_paragraph=paragraphs[1], terms_collection=terms_collections[1])
        self.response = self.user_client.get(self.endpoint)
        assert self.response.status_code == status.HTTP_200_OK
        self.response_body = json.loads(self.response.content)

    def test_genders(self):
        sut = self.response_body.get('persons:genders', None)
        self.assertIsNotNone(sut)
        self.assertEquals(len(sut), len(Person.GENDER_CHOICES))

    def test_civil_states(self):
        sut = self.response_body.get('profiles:civil_states', None)
        self.assertIsNotNone(sut)
        self.assertEquals(len(sut), len(Profile.CIVIL_STATE_CHOICES))

    def test_agencies_terms(self):
        sut = self.response_body.get('agencies:terms', None)
        self.assertIsNotNone(sut)
        self.assertEquals(len(sut), TermsCollection.objects.filter(is_archived=False).count())

    def test_agencies_term_paragraphs(self):
        sut = self.response_body.get('agencies:term_paragraphs', None)
        self.assertIsNotNone(sut)
        self.assertEquals(len(sut), TermsParagraph.objects.filter(is_archived=False).count())

    def test_agencies_providers(self):
        providers = self.response_body.get('agencies:providers', None)
        self.assertIsNotNone(providers)
        self.assertEquals(len(providers), Company.objects.exclude(allocations=None).count())

    def test_reocurring(self):
        reocurring = self.response_body.get("commons:reoccurring")
        self.assertIsNotNone(reocurring)
        self.assertEqual(len(reocurring), len(REOCURRING_TYPES.CHOICES))
