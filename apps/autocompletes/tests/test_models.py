from django.test import TestCase

from apps.autocompletes.sources import (AutocompleteSourceCollection,
                                        ModelAutocompleteSource,
                                        VariableAutocompleteSource)
from apps.companies.models import COMPANY_TYPE
from apps.metronom_commons.data import BILLING_AGREEMENT, BILLING_INTERVAL
from apps.persons.models import Person
from apps.persons.tests.factories import PersonFactory


class TestAutocompleteSources(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.persons_count = 5
        PersonFactory.create_batch(cls.persons_count)

    def test_model_source(self):
        source = ModelAutocompleteSource(
            Person.objects.all(),
            id_prop='id',
            name_prop='name',
        )
        autocomplete_dict = source.get_data()
        self.assertEqual(len(autocomplete_dict), self.persons_count)

        persons_from_db = Person.objects.all()

        person_ids = [str(p.id) for p in persons_from_db]
        autocomplete_keys = list(autocomplete_dict.keys())
        self.assertEqual(autocomplete_keys, person_ids)

        person_names = [str(p.name) for p in persons_from_db]
        autocomplete_values = list(autocomplete_dict.values())
        self.assertEqual(autocomplete_values, person_names)

    def test_united_dict(self):
        sources_collection = AutocompleteSourceCollection()
        sources_collection.add_source(
            'company:company_type',
            VariableAutocompleteSource(COMPANY_TYPE.CHOICES)
        )
        sources_collection.add_source(
            'company:billing_agreement',
            VariableAutocompleteSource(BILLING_AGREEMENT.CHOICES)
        )
        sources_collection.add_source(
            'company:billing_interval',
            VariableAutocompleteSource(BILLING_INTERVAL.CHOICES)
        )
        sources_collection.add_source(
            'person',
            ModelAutocompleteSource(
                Person.objects.all(),
                id_prop='id',
                name_prop='name',
            )
        )
        united_dict = sources_collection.get_united_autocomplete_dictionary()

        self.assertEqual(
            list(united_dict.keys()),
            ['company:company_type',
             'company:billing_agreement',
             'company:billing_interval',
             'person']
        )

        self.assertEqual(united_dict['company:company_type'], dict(COMPANY_TYPE.CHOICES))
