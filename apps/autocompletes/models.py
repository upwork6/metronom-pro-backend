from django.contrib.auth import get_user_model
from django.core.cache import caches
from django.db.models.signals import m2m_changed, post_delete, post_save
from django.dispatch import receiver

from apps.autocompletes.views import UnitedAutocompleteView
from apps.companies.models import Company, Sector

User = get_user_model()

cache = caches['autocompletes']


@receiver(post_save)
@receiver(post_delete)
@receiver(m2m_changed)
def clear_united_autocomplete_view_output_cache(sender, instance=None, **kwargs):
    if sender in [Company, Sector, User]:
        UnitedAutocompleteView.clear_cache()
