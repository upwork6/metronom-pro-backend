
from django.core.cache import caches
from django.http import HttpResponseNotModified, JsonResponse
from django.utils.cache import set_response_etag
from django_countries.data import COUNTRIES
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from apps.accounting.models import Allocation
from apps.activities.models import ActivityGroup, Step
from apps.articles.models import ArticleGroup
from apps.autocompletes.serializers import AutoCompleteSerializer
from apps.autocompletes.sources import (AutocompleteSourceCollection,
                                        FunctionalModelAutocompleteSource,
                                        JSONFileAutocompleteSource,
                                        ModelAutocompleteSource,
                                        VariableAutocompleteSource)
from apps.autocompletes.utils import get_timezone_choices
from apps.backoffice.models import (AgencyBankAccount, TaxNote,
                                    TermsCollection, TermsParagraph,
                                    ThankYouNote)
from apps.companies.models import COMPANY_TYPE, Company, Sector
from apps.contacts.models import Contact
from apps.metronom_commons.data import (ABSENCE_REASONS,
                                        ADDED_CHARGES_DEPENDENCY,
                                        BILLING_AGREEMENT, BILLING_DELIVERY,
                                        BILLING_INTERVAL, INVOICE_TYPE,
                                        REOCURRING_TYPES)
from apps.persons.models import Person
from apps.projects.models import Campaign, Project
from apps.users.models import Profile, User
from apps.workinghours.models import FlexWork

cache = caches['autocompletes']


class IDNameAutocompleteView(ListAPIView):
    def get_serializer(self, *args, **kwargs):
        kwargs['context'] = self.get_serializer_context()
        return AutoCompleteSerializer(*args,
                                      id_field_name='id',
                                      name_field_name='name',
                                      **kwargs)


class BaseAutocompleteView(APIView):
    """A base class which must be used as parent class for all classes getting autocomplete dictionary."""

    # Dictionary of autocomplete sources.
    # Example: {
    #   'company:company_type': VariableAutocompleteSource(COMPANY_TYPE.CHOICES)
    # }
    sources = {}
    permission_classes = (IsAuthenticated,)

    @classmethod
    def get_cache_key(cls):
        return cls.__name__

    def _get_data_from_cache(self):
        data = cache.get(self.get_cache_key())
        return data

    def _get_data(self):
        cached_data = self._get_data_from_cache()
        if cached_data:
            return cached_data

        sources_collection = AutocompleteSourceCollection()
        for name, source in self.sources.items():
            sources_collection.add_source(name, source)
        data = sources_collection.get_united_autocomplete_dictionary()
        cache.set(self.get_cache_key(), data)
        return data

    @classmethod
    def get_etag_cache_key(cls):
        return '%s_etag' % cls.__name__

    def get(self, request, *args, **kwargs):
        """


        :param request:
        :param format:
        :return: Example
        '{
            "company:company_type": {
                "some_id": "some_name"
                "some_id2": "some_name2"
            },
            "company:billing_interval": {
                "some_id": "some_name"
                "some_id2": "some_name2"
                "some_id3": "some_name3"
            }
        }'
        """
        etag_cache_key = self.get_etag_cache_key()
        etag = cache.get(etag_cache_key, '')
        if_none_match = request.META.get('HTTP_IF_NONE_MATCH', '')
        if if_none_match and if_none_match == etag:
            return HttpResponseNotModified()

        json_data = self._get_data()
        response = JsonResponse(json_data, safe=False)
        set_response_etag(response)
        cache.set(etag_cache_key, response['ETag'])
        return response

    @classmethod
    def clear_queryset_cache_of_sources(cls):
        for key, source in cls.sources.items():
            if isinstance(source, ModelAutocompleteSource):
                source.clear_queryset_cache()

    @classmethod
    def clear_cache(cls):
        cache_key = cls.get_cache_key()
        cache.delete(cache_key)
        etag_cache_key = cls.get_etag_cache_key()
        cache.delete(etag_cache_key)
        cls.clear_queryset_cache_of_sources()


def _make_serialization_activity_group(instance):
    return instance.make_autocomplete_data()


def _make_serialization_article_group(instance):
    def serialize_articles(article):
        return {str(article.id): article.name}

    return list(map(serialize_articles, instance.templatearticle_set.order_by('position')))


class UnitedAutocompleteView(BaseAutocompleteView):
    """A View class for autocomplete function of several form fields."""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.sources = {

            'contacts:statuses': VariableAutocompleteSource(Contact.STATUS_CHOICES),

            'persons:genders': VariableAutocompleteSource(Person.GENDER_CHOICES),
            'persons:languages': VariableAutocompleteSource(Person.LANGUAGE_CHOICES),

            'profiles:civil_states': VariableAutocompleteSource(Profile.CIVIL_STATE_CHOICES),

            'companies:sectors': ModelAutocompleteSource(Sector.objects.all().order_by('name')),
            'companies:added_charges_dependencies': VariableAutocompleteSource(ADDED_CHARGES_DEPENDENCY.CHOICES),
            'companies:billing_deliveries': VariableAutocompleteSource(BILLING_DELIVERY.CHOICES),
            'companies:billing_agreements': VariableAutocompleteSource(BILLING_AGREEMENT.CHOICES),
            'companies:billing_intervals': VariableAutocompleteSource(BILLING_INTERVAL.CHOICES),
            'companies:company_types': VariableAutocompleteSource(COMPANY_TYPE.CHOICES),

            'agencies:users': ModelAutocompleteSource(User.objects.all().order_by('first_name'), id_prop='id'),
            'agencies:activity_groups': ModelAutocompleteSource(ActivityGroup.objects.all().order_by("position"),
                                                                id_prop='id', name_prop='name'),
            'agencies:activities': FunctionalModelAutocompleteSource(
                ActivityGroup.objects.all().order_by("position"), id_prop='id',
                serialize_function=_make_serialization_activity_group),
            'agencies:article_groups': ModelAutocompleteSource(ArticleGroup.objects.all().order_by("position"),
                                                               id_prop='id', name_prop='name'),
            'agencies:articles': FunctionalModelAutocompleteSource(
                ArticleGroup.objects.all().order_by("position"), id_prop='id',
                serialize_function=_make_serialization_article_group),
            'agencies:steps': ModelAutocompleteSource(
                Step.objects.all().order_by("position"), id_prop='id', name_prop='name',
            ),
            'agencies:available_step_rates': FunctionalModelAutocompleteSource(
                Step.objects.all().order_by("position"), id_prop='id',
                serialize_function=self._make_serialization_available_step_rates),
            'agencies:terms': ModelAutocompleteSource(
                TermsCollection.objects.filter(is_archived=False).order_by("position"), id_prop='id', name_prop='name'
            ),
            'agencies:term_paragraphs': ModelAutocompleteSource(
                TermsParagraph.objects.filter(is_archived=False).order_by("position"), id_prop='id', name_prop='name'
            ),
            'agencies:providers': ModelAutocompleteSource(
                Company.objects.exclude(allocations=None), id_prop='id', name_prop='name'
            ),
            'agencies:bankaccounts': ModelAutocompleteSource(
                AgencyBankAccount.objects.all().order_by("bank_name"), id_prop='id', name_prop='public_name'
            ),
            'agencies:tax_notes': ModelAutocompleteSource(
                TaxNote.objects.all(), id_prop='id', name_prop='name'
            ),
            'agencies:thankyou_notes': ModelAutocompleteSource(
                ThankYouNote.objects.all(), id_prop='id', name_prop='name'
            ),
            'agencies:flexwork_models': ModelAutocompleteSource(
                FlexWork.objects.order_by("given_name"), id_prop='id', name_prop='name'
            ),

            'companies:allocations': ModelAutocompleteSource(Allocation.objects.all().order_by("public_id"),
                                                             id_prop='id', name_prop='name'),
            'commons:tenants': JSONFileAutocompleteSource("tenants.json"),
            'commons:currencies': JSONFileAutocompleteSource("currencies.json"),
            'commons:countries': VariableAutocompleteSource(COUNTRIES),
            'commons:timezones': VariableAutocompleteSource(get_timezone_choices()),
            'commons:absence_reasons': VariableAutocompleteSource(ABSENCE_REASONS.CHOICES),
            'commons:invoice_types': VariableAutocompleteSource(INVOICE_TYPE.CHOICES),
            'commons:reoccurring': VariableAutocompleteSource(REOCURRING_TYPES.CHOICES),

            'projects:projects': ModelAutocompleteSource(Project.objects.all().order_by('title'), name_prop='title'),
            'projects:campaigns': ModelAutocompleteSource(Campaign.objects.all().order_by('title'), name_prop='title'),

        }

    def _make_serialization_available_step_rates(self, instance):
        rates = instance.rates.all().order_by("-id")
        final_dict = {}
        for rate in rates:
            final_dict[rate.currency] = {
                'hourly': rate.hourly_rate.amount,
                'daily': rate.daily_rate.amount if rate.daily_rate else ''
            }
        return final_dict
