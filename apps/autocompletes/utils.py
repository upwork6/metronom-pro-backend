import pytz


def get_timezone_choices():
    """Return list of tuples with timezone names."""
    choices = [(tz, tz) for tz in pytz.all_timezones]
    return choices
