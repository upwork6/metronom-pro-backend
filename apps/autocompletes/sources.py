"""The module contains classes returning dictionary for autocomplete form fields."""

import json
from collections import Iterable

from django.utils.translation import ugettext_lazy as _


class BaseAutocompleteSource(object):
    """A base class for all source classes which can be used in SourceCollection."""

    def get_data(self) -> dict:
        raise NotImplementedError


class VariableAutocompleteSource(BaseAutocompleteSource):
    """A class which prepares autocomplete dictionary from variable with type List or Dict.

    For example choices for model field like COMPANY_TYPE.CHOICES
    """

    def __init__(self, source_variable):
        self.source_variable = source_variable

    def get_data(self) -> dict:
        if isinstance(self.source_variable, dict):
            return self.source_variable
        if isinstance(self.source_variable, Iterable):
            return dict(self.source_variable)
        raise ValueError(_('Source variable should be a list or dictionary'))


class ModelAutocompleteSource(BaseAutocompleteSource):
    """A class which prepares autocomplete dictionary from queryset."""

    def __init__(self, queryset, id_prop: str = 'id', name_prop: str = 'name'):
        self.queryset = queryset
        self.id_prop = id_prop
        self.name_prop = name_prop

    def get_data(self) -> dict:
        data = {str(getattr(m, self.id_prop)): str(getattr(m, self.name_prop))
                for m in self.queryset}
        return data

    def clear_queryset_cache(self):
        self.queryset._result_cache = None


class FunctionalModelAutocompleteSource(BaseAutocompleteSource):
    """ Class which prepares autocomplete dictionary from queryset using serialize function
    """

    def __init__(self, queryset, serialize_function, id_prop: str = 'id'):
        """
        :param queryset: queryset to serialize
        :param serialize_function: function to serialize data. Should return dict
        :param id_prop: string name of the id field of the models in queryset
        """
        self.queryset = queryset
        self.serialize_function = serialize_function
        self.id_prop = id_prop

    def _id_to_str(self):
        """ Here we prepare strings from the any id we get from the queryset

        """
        return map(str, self.queryset.order_by(self.id_prop).values_list(self.id_prop, flat=True))

    def get_data(self) -> dict:
        # a bit of functions here: first we get ids list, then we make list of dicts with the data
        # and then make tuples (id, dict) with zip and then this tuple goes to the dict
        data = dict(zip(self._id_to_str(), map(self.serialize_function, self.queryset.order_by(self.id_prop))))
        return data

    def clear_queryset_cache(self):
        self.queryset._result_cache = None


class JSONFileAutocompleteSource(BaseAutocompleteSource):
    def __init__(self, file_path):
        self.file_path = file_path

    def get_data(self):
        json_data = json.load(open(self.file_path))
        if isinstance(json_data, dict):
            return json_data
        else:
            raise ValueError(_('The file should contains the dictionary like as {"id1": "name1", "id2": "name2"...}'))
        return json_data


class AutocompleteSourceCollection(object):
    """A collection which return united dictionary from all gotten sources."""

    def __init__(self):
        self.__sources = {}

    def add_source(self, name: str, source) -> None:
        if not isinstance(name, str):
            raise TypeError(_('The name should be string'))
        if not issubclass(source.__class__, BaseAutocompleteSource):
            raise TypeError(_('The class of source object should be related from BaseAutocompleteSource'))
        if name in self.__sources:
            raise ValueError(_('The source with same name already added'))
        self.__sources[name] = source

    def get_united_autocomplete_dictionary(self) -> dict:
        data = {}
        for name, source in self.__sources.items():
            data[name] = source.get_data()
        return data
