import pytest
from django.core.cache import caches
from django.urls import reverse
from django.utils import timezone
from rest_framework import status

from apps.companies.models import Company
from apps.companies.tests.factories import CompanyFactory, SectorFactory
from apps.employments.tests.factories import EmploymentFactory
from apps.metronom_commons.drf_caching import get_drf_view_cache
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.persons.models import Person
from apps.persons.tests.factories import PersonFactory
from apps.users.tests.factories import UserFactory


default_cache = caches['default']
drf_cache = get_drf_view_cache()


class TestContactList(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.contact1 = PersonFactory()
        cls.contact2 = CompanyFactory()
        cls.user = UserFactory(person_in_crm=PersonFactory())
        cls.endpoint = reverse('contacts-list')
        cls.response = cls.user_client.get(cls.endpoint)

    def _create_test_data(self, count=3):
        for i in range(0, count):
            PersonFactory()

    def test_get_should_return_ok_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_get_should_contain_the_list_of_contacts(self):
        self.contact1.refresh_from_db()
        self.contact2.refresh_from_db()
        self.assertIn(self.user.person_in_crm.full_name, [x['full_name'] for x in self.response.data['users']])
        self.assertIn(self.contact1.full_name, [x['full_name'] for x in self.response.data['persons']])
        self.assertIn(self.contact2.full_name, [x['full_name'] for x in self.response.data['companies']])

    def test_get_should_contain_three_objects(self):
        assert len(self.response.data) == 3

    def test_response_should_contain_users(self):
        """
        2 users are created as part of the super().setUpTestData()
        1 user is created as part of self.setUpTestData()
        """
        user_count = Person.objects.exclude(user=None).count()
        assert len(self.response.data.get('users')) == user_count

    def test_get_should_contain_one_person(self):
        assert len(self.response.data['persons']) == 1

    def test_get_should_contain_one_company(self):
        assert len(self.response.data['companies']) == 1

    def test_result_should_contain_the_correct_uuids(self):
        pks = [x['id'] for x in self.response.data['persons']]
        companies_pks = [x['id'] for x in self.response.data['companies']]
        assert str(self.contact1.pk) in pks
        assert str(self.contact2.pk) in companies_pks

    def test_result_should_contain_a_public_id(self):
        company_public_ids = [company['public_id'] for company in self.response.data['companies']]
        assert self.response.data['persons'][0]['public_id'] == self.contact1.public_id
        assert self.contact2.public_id in company_public_ids

    def test_result_should_contain_a_phone(self):
        landline_phone = '+79008887766'
        employment = EmploymentFactory(
            person=self.contact1,
            company=self.contact2,
            landline_phone=landline_phone,
        )

        caches['default'].clear()
        response = self.user_client.get(self.endpoint)
        person_phones = [person['phone'] for person in response.data['persons']]

        assert landline_phone in person_phones

    def test_sector_should_be_flat(self):
        sector = SectorFactory()
        contact3 = CompanyFactory(sector=sector)
        response = self.user_client.get(self.endpoint)
        company_sectors = [company.get('sector_uuid') for company in response.data['companies']]
        assert contact3.sector.id in company_sectors

    def test_latest_employment_should_be_nested(self):
        employment = EmploymentFactory(person=self.contact1, company=self.contact2, position_name="CTO")
        response = self.user_client.get(self.endpoint)
        person = response.data['persons'][0]
        position_names = [x['latest_employment']['position_name'] for x in response.data['persons']]
        assert employment.position_name in position_names

    def test_company_should_be_nested(self):
        employment = EmploymentFactory(person=self.contact1, company=self.contact2)
        response = self.user_client.get(self.endpoint)
        person = response.data['persons'][0]
        company_uuid = person['latest_employment']['company']['id']
        assert str(company_uuid) == str(self.contact2.id)

    def test_company_of_the_person_contains_the_public_id(self):
        employment = EmploymentFactory(person=self.contact1, company=self.contact2)
        response = self.user_client.get(self.endpoint)
        person = response.data['persons'][0]
        public_id = person['latest_employment']['company']['public_id']
        assert public_id == self.contact2.public_id

    @pytest.mark.skip(reason="We have issues with last modified in #321, so it's disabled for now")
    def test_get_last_modified(self):
        date_2016 = timezone.make_aware(timezone.datetime(2016, 1, 1, 0, 0, 0, 0))
        date_2017 = timezone.make_aware(timezone.datetime(2017, 1, 1, 0, 0, 0, 0))

        default_cache.clear()

        self.contact1.first_name = 'Old'
        self.contact1.last_name = "Doe"
        self.contact1.save()

        person2 = PersonFactory()

        Person.objects.all().update(updated_at=date_2016)
        response = self.user_client.get(self.endpoint)

        names1 = [x['full_name'] for x in response.data['persons']]
        self.assertTrue(response.has_header('Last-Modified'))
        self.assertTrue('Old Doe' in names1)
        self.assertEqual(len(names1), 2)

        new_person = PersonFactory(first_name='New1', last_name='Doe')

        response = self.user_client.get(self.endpoint, HTTP_IF_MODIFIED_SINCE=new_person.updated_at)

        names2 = [x['full_name'] for x in response.data['persons']]
        self.assertTrue('New1 Doe' in names2)
        self.assertEqual(len(names2), 1)

        self.contact1.first_name = 'Changed'
        self.contact1.save()

        response = self.user_client.get(self.endpoint, HTTP_IF_MODIFIED_SINCE=new_person.updated_at)

        names3 = [x['full_name'] for x in response.data['persons']]
        self.assertTrue('Changed Doe' in names3)
        self.assertEqual(len(names3), 2)

        default_cache.clear()
        Company.objects.all().update(updated_at=date_2016)
        company = Company.objects.all().last()
        company.sector = SectorFactory()
        company.save()

        response = self.user_client.get(self.endpoint, HTTP_IF_MODIFIED_SINCE=date_2017)

        self.assertEqual(len(response.data['companies']), 1)

        Company.objects.all().update(updated_at=date_2016)
        company.sector.name = 'changed'
        company.sector.save()

        response = self.user_client.get(self.endpoint, HTTP_IF_MODIFIED_SINCE=date_2017)

        self.assertEqual(len(response.data['companies']), 1)
        self.assertEqual(response.data['companies'][0]['public_id'], company.public_id)

    def _separate_full_name_of_persons_from_response(self, response):
        full_names = [p['full_name'] for p in response.data['persons']]
        return full_names

    @pytest.mark.skip(reason="Caching failed for some reasons all over the app")
    def test_drf_caching(self):
        self._create_test_data()
        default_cache.clear()
        drf_cache.clear()
        response = self.user_client.get(self.endpoint)
        person = Person.objects.filter(user=None)[1]
        first_name1 = 'changed193'
        person.first_name = first_name1
        person.save()
        name_changed = person.full_name
        response = self.user_client.get(self.endpoint)
        full_names = self._separate_full_name_of_persons_from_response(response)
        self.assertIn(name_changed, full_names)
        first_name2 = 'changedV2'
        Person.objects.filter(pk=person.pk).update(first_name=first_name2)
        name_changed_without_signals = person.full_name.replace(first_name1, first_name2)
        response = self.user_client.get(self.endpoint)
        full_names = self._separate_full_name_of_persons_from_response(response)
        self.assertIn(name_changed, full_names)
        self.assertFalse(name_changed_without_signals in full_names)
