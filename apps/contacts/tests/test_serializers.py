from django.core.cache import caches
from django.test import TestCase

from apps.metronom_commons.drf_caching import get_drf_view_cache
from apps.persons.serializers.plain import PersonContactListSerializer
from apps.persons.tests.factories import PersonFactory
from apps.users.tests.factories import UserFactory


default_cache = caches['default']
drf_cache = get_drf_view_cache()


class TestPersonContactListSerializer(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestPersonContactListSerializer, cls).setUpTestData()
        default_cache.clear()
        drf_cache.clear()

        person = PersonFactory()
        user = UserFactory(person_in_crm=PersonFactory())
        cls.person_serializer = PersonContactListSerializer(instance=person)

        default_cache.clear()
        drf_cache.clear()

        cls.user_serializer = PersonContactListSerializer(instance=user.person_in_crm)
        cls.person_keys = cls.person_serializer.data.keys()
        cls.user_keys = cls.user_serializer.data.keys()

    # def test_person_does_not_have_image(self):
    #     self.assertNotIn('has_image', self.person_keys)
    #
    # def test_user_has_image(self):
    #     self.assertIn('has_image', self.user_keys)

    # TODO: Add tests for the Serializers
