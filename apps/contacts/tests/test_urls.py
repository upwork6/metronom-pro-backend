from django.test import TestCase
from django.urls import resolve, reverse


class TestContactURLs(TestCase):
    def test_contact_base_reverse(self):
        assert reverse('contacts-list') == '/api/contacts/'

    def test_contact_base_resolve(self):
        assert resolve('/api/contacts/').view_name == 'contacts-list'
