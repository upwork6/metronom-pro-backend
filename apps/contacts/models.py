from django.db import models
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from apps.cache_controller.signals import model_instances_changed
from apps.metronom_commons.models import UserAccessMetronomBaseModel, SoftDeleteMixin


class Contact(SoftDeleteMixin, UserAccessMetronomBaseModel):

    ACTIVE = 'active'
    RESTRICTED = 'restricted'
    ARCHIVED = 'archived'

    STATUS_CHOICES = (
        (ACTIVE, _('active')),
        (RESTRICTED, _('restricted')),
        (ARCHIVED, _('archived')),
    )

    public_id = models.IntegerField(_('Public ID'), help_text=_('Unique public ID'), unique=True, blank=True)

    status = models.CharField(_('Status'), max_length=10, choices=STATUS_CHOICES, default=ACTIVE)

    # TODO: tags

    def save(self, *args, **kwargs):
        """Fill up the sorting_field every time we save the object.

        For companies, we only use the name, for persons however we use
        the forename by default.
        """

        if not self.public_id:
            result = Contact.all_objects.all().aggregate(models.Max('public_id'))

            if result["public_id__max"] is None:
                self.public_id = 1
            else:
                self.public_id = result["public_id__max"] + 1

        super(Contact, self).save(*args, **kwargs)


@receiver(model_instances_changed)
def clear_drf_view_response_cache(sender, instances, **kwargs):
    from apps.contacts.drf_caching import CompanyUpdatedAtKeyBit, PersonUpdatedAtKeyBit
    CompanyUpdatedAtKeyBit.update_cache()
    PersonUpdatedAtKeyBit.update_cache()
