from rest_framework import mixins, serializers, status, viewsets
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework_extensions.cache.decorators import cache_response

from apps.api.mixins import LoggerMixin
from apps.companies.models import Company
from apps.companies.serializers.base import CompanyContactListSerializer
from apps.contacts.drf_caching import ContactListKeyConstructor
from apps.contacts.models import Contact
from apps.persons.models import Person
from apps.persons.serializers.plain import (PersonContactListSerializer,
                                            UserContactListSerializer)


class ContactListSerializer(serializers.Serializer):
    persons = PersonContactListSerializer(many=True)
    users = UserContactListSerializer(many=True)
    companies = CompanyContactListSerializer(many=True)


class ContactListViewSet(LoggerMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    """
        Read-only-view for Contacts with Persons & companies from the CRM and Users in the instance
    """

    queryset = Contact.objects.all()
    renderer_classes = [JSONRenderer]
    http_method_names = ['get', 'head', 'options', 'trace']
    serializer_class = ContactListSerializer

    def get_persons_users_and_companies(self, if_modified_since):
        """ Receives a list of persons + companies"""
        # TODO Dmitry Koldunov: Maybe add the context managers for every step to do more clearly code.

        persons = Person.objects.prefetch_related("employments").filter(user=None)
        users = Person.objects.prefetch_related("employments").exclude(user=None)
        companies = Company.objects.prefetch_related("employments", "offices").select_related("sector").all()

        # TODO: We need a mechanism to invalidate the cache, when there is a new version!!
        if if_modified_since:
            persons = persons.updated_later_than(if_modified_since)
            users = users.updated_later_than(if_modified_since)
            companies = companies.updated_later_than(if_modified_since)

        return persons, users, companies

    @cache_response(key_func=ContactListKeyConstructor())
    def list(self, request):
        # TODO: last_modified logic here seems ok, but for some reasons it behaves weird
        # check #321 for the details
        # For now I commented all the code to check it in future
        # if_modified_since = request.META.get('HTTP_IF_MODIFIED_SINCE', '')
        if_modified_since = None

        persons, users, companies = self.get_persons_users_and_companies(if_modified_since)

        if not persons.exists() and not users.exists() and not companies.exists():
            return Response({}, status=status.HTTP_304_NOT_MODIFIED)

        persons_serializer = PersonContactListSerializer(persons, many=True)
        users_serializer = UserContactListSerializer(users, many=True)
        companies_serializer = CompanyContactListSerializer(companies, many=True)
        contacts_json = {
            "persons": persons_serializer.data,
            "users": users_serializer.data,
            "companies": companies_serializer.data
        }

        return Response(contacts_json, status=status.HTTP_200_OK)
