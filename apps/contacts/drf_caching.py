from rest_framework_extensions.key_constructor.bits import UniqueMethodIdKeyBit
from rest_framework_extensions.key_constructor.constructors import \
    KeyConstructor

from apps.metronom_commons.drf_caching import (IfModifiedSinceHeadersKeyBit,
                                               ModelUpdatedAtKeyBit)


class PersonUpdatedAtKeyBit(ModelUpdatedAtKeyBit):
    model_import_path = 'persons.Person'


class CompanyUpdatedAtKeyBit(ModelUpdatedAtKeyBit):
    model_import_path = 'companies.Company'


class ContactListKeyConstructor(KeyConstructor):
    unique_method_id = UniqueMethodIdKeyBit()
    if_modified_since = IfModifiedSinceHeadersKeyBit()
    person_updated_at = PersonUpdatedAtKeyBit()
    company_updated_at = CompanyUpdatedAtKeyBit
