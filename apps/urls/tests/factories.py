import factory

from apps.urls.models import URLType


class URLTypeNiceFactory(factory.django.DjangoModelFactory):
    name = factory.Iterator(["Webseite", "Facebook", "XING", "LinkedIn", "Twitter", "Projekt-Seite", "Trello"])

    class Meta:
        model = URLType
