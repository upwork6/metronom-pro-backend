from rest_framework import serializers

from .models import URL


class URLSerializer(serializers.ModelSerializer):
    """Serialize data from the Email."""

    class Meta:
        model = URL
        fields = (
            'url',
            'url_type'
        )
