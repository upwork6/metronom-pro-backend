from django.contrib import admin

from .models import URL, URLType


# @admin.register(URLType)
# class URLTypeAdmin(admin.ModelAdmin):
#     list_display = ('id', 'name')
#     search_fields = ('name',)
#
#
# @admin.register(URL)
# class URLAdmin(admin.ModelAdmin):
#     list_display = ('id', 'url_type', 'url',)
#     list_filter = ('url_type', )
