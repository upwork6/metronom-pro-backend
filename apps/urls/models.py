from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.metronom_commons.models import MetronomBaseModel


class URLType(models.Model):
    name = models.CharField(_('URL Type'), max_length=20)


class URL(MetronomBaseModel):

    url_type = models.ForeignKey(URLType, related_name='type', verbose_name=_('URL type'))
    url = models.URLField(_('URL'))

    class Meta:
        ordering = ['url_type']
        verbose_name = _('URL')
        verbose_name_plural = _('URLs')

    def __str__(self):
        return self.type
