from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class URLsConfig(AppConfig):
    name = 'apps.urls'
    verbose_name = _('URLs')