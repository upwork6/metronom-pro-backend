from rest_framework.viewsets import ModelViewSet

from .models import URL
from .serializers import URLSerializer


class URLViewSet(ModelViewSet):
    """URLS used in the CRM"""

    serializer_class = URLSerializer
    queryset = URL.objects.all()
