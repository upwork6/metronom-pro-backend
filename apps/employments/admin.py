from django.contrib import admin

from .models import Employment


@admin.register(Employment)
class EmploymentAdmin(admin.ModelAdmin):
    list_display = ('id', 'company', 'person', 'position_name')
    list_filter = ('company', 'person')
