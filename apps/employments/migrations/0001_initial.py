# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-03-20 14:05
from __future__ import unicode_literals

import apps.metronom_commons.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('companies', '0001_initial'),
        ('persons', '0001_initial'),
        ('users', '0001_initial'),
        ('emails', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Employment',
            fields=[
                ('created_date', model_utils.fields.AutoCreatedField(
                    default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('updated_date', model_utils.fields.AutoLastModifiedField(
                    default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('id', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False,
                                        primary_key=True, serialize=False, unique=True, verbose_name='UUID')),
                ('position_name', models.CharField(blank=True, max_length=255, null=True, verbose_name='Position')),
                ('landline_phone', apps.metronom_commons.fields.LandlinePhoneNumberField(blank=True, max_length=128, null=True)),
                ('mobile_phone', apps.metronom_commons.fields.MobilePhoneNumberField(blank=True, max_length=128, null=True)),
                ('company', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.CASCADE,
                                              related_name='employments', to='companies.Company')),
                ('email', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE,
                                               related_name='employment', to='emails.Email', verbose_name='Email')),
                ('person', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.CASCADE,
                                             related_name='employments', to='persons.Person')),
                ('is_void', models.BooleanField(default=False, verbose_name='Is in void?')),
            ],
            options={
                'verbose_name': 'Employment',
                'verbose_name_plural': 'Employments',
            },
        ),
        migrations.AlterUniqueTogether(
            name='employment',
            unique_together=set([('person', 'company')]),
        ),
        migrations.CreateModel(
            name='TenantEmployment',
            fields=[
                ('created_date', model_utils.fields.AutoCreatedField(
                    default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('updated_date', model_utils.fields.AutoLastModifiedField(
                    default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('id', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False,
                                        primary_key=True, serialize=False, unique=True, verbose_name='UUID')),
                ('position_name', models.CharField(blank=True, max_length=255, null=True, verbose_name='Position')),
                ('landline_phone', apps.metronom_commons.fields.LandlinePhoneNumberField(blank=True, max_length=128, null=True)),
                ('mobile_phone', apps.metronom_commons.fields.MobilePhoneNumberField(blank=True, max_length=128, null=True)),
                ('email', models.OneToOneField(blank=True, null=True,
                                               on_delete=django.db.models.deletion.CASCADE, to='emails.Email', verbose_name='Email')),
                ('user', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('is_void', models.BooleanField(default=False, verbose_name='Is in void?')),
            ],
            options={
                'verbose_name': 'Employment with a Tenant',
                'verbose_name_plural': 'Tenant-Employments',
            },
        ),
    ]
