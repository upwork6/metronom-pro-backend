from rest_framework import serializers

from apps.companies.models import Company
from apps.employments.models import Employment
from apps.employments.serializers.plain import EmploymentBaseSerializer
from apps.persons.models import Person


class EmploymentCompanySerializer(serializers.ModelSerializer):
    """Serialize data from the Company."""

    class Meta:
        model = Company
        fields = (
            'id',
            'name',
            'public_id'
        )


class EmploymentPersonSerializer(serializers.ModelSerializer):
    """Serialize data of a person without their Employment.

    This is done in order to avoid circular dependencies between the serializers.
    """

    # TODO:  Maybe there is another solution? A maximum nested level? We have redundancy now

    class Meta:
        model = Person
        fields = (
            'id',
            'first_name',
            'last_name',
            'gender',
        )


class EmploymentSerializerForCompany(serializers.ModelSerializer):
    """Serialize data from the Employment without the Company.

    This is done in order to avoid circular dependencies between the serializers.
    """
    person = EmploymentPersonSerializer()
    email = serializers.EmailField(source='email.email', required=False)

    class Meta:
        model = Employment
        fields = (
            'id',
            'person',
            'position_name',
            'email',
            'landline_phone',
            'mobile_phone',
        )


class EmploymentSerializerForPerson(serializers.ModelSerializer):
    """Serialize data from the Employment without the Company.

    This is done in order to avoid circular dependencies between the serializers.
    """
    email = serializers.EmailField(source='email.email', required=False)
    company = EmploymentCompanySerializer()

    class Meta:
        model = Employment
        fields = (
            'id',
            'company',
            'position_name',
            'email',
            'landline_phone',
            'mobile_phone',
        )


class EmploymentSerializerForPersonInContactList(serializers.ModelSerializer):
    """This serializer is used in the persons list of the /contacts/ API """
    company = EmploymentCompanySerializer()

    class Meta:
        model = Employment
        fields = (
            'company',
            'position_name',
        )


class EmploymentSerializer(EmploymentBaseSerializer):
    company_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Company.objects.all(),
        source="company",
    )

    person_uuid = serializers.PrimaryKeyRelatedField(
        queryset=Person.objects.all(),
        source="person",
    )

    class Meta(EmploymentBaseSerializer.Meta):
        fields = EmploymentBaseSerializer.Meta.fields + (
            'company_uuid',
            'person_uuid',
        )
