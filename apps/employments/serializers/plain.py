from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.employments.models import Employment
from apps.employments.validators import NotEditableFieldsValidator
from apps.metronom_commons.serializers import \
    ForeignKeyFieldsCreateOrUpdateMixin


class EmploymentBaseSerializer(ForeignKeyFieldsCreateOrUpdateMixin, serializers.ModelSerializer):
    email = serializers.EmailField(source='email.email', required=False)
    position_name = serializers.CharField(required=False)

    class Meta:
        model = Employment
        fields = (
            'id',
            'position_name',
            'email',
            'landline_phone',
            'mobile_phone',
        )

        # For ForeignKeyFieldsCreateOrUpdateMixin
        fk_keys = ['email']

        validators = [
            NotEditableFieldsValidator(editable_fields=[
                'position_name',
                'email',
                'landline_phone',
                'mobile_phone',
            ])
        ]

    def create(self, validated_data, commit=True):
        employment = super().create(validated_data, commit=False)

        try:
            employment.save()
        except ValidationError as e:
            raise serializers.ValidationError({
                'non_field_errors': [e.message]
            })
        except IntegrityError as e:
            raise serializers.ValidationError({
                'non_field_errors': [_("The fields person, company have to form a unique quantity.")]
            })

        return employment


class EmploymentResponseSerializer(EmploymentBaseSerializer):
    """ used during patch employment responses """

    from apps.companies.serializers.additional import CompanyInEmploymentSerializer
    from apps.persons.serializers import PersonBaseSerializer

    company = CompanyInEmploymentSerializer()
    person = PersonBaseSerializer()

    class Meta(EmploymentBaseSerializer.Meta):
        depth = 1
        fields = EmploymentBaseSerializer.Meta.fields + (
            'person',
            'company',
        )
