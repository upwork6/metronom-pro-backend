from django.utils.translation import ugettext_lazy as _
from rest_framework.validators import ValidationError


class NotEditableFieldsValidator(object):
    """Deny update any field which is not in attribute editable_fields.
    Is used as validator for serializer class (not for serializer field)."""
    message = _('This fields %s should not be changed')

    def __init__(self, editable_fields):
        self.editable_fields = editable_fields

    def set_context(self, serializer_instance):
        self.is_update = serializer_instance.instance is not None

    def __call__(self, values):
        should_not_updated_fields = set(values.keys()) - set(self.editable_fields)
        if self.is_update and should_not_updated_fields:
            should_not_updated_fields_str = ', '.join(should_not_updated_fields)
            raise ValidationError(self.message % should_not_updated_fields_str, code='not_editable')
