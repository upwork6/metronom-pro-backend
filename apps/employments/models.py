from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

from apps.companies.models import Company
from apps.emails.models import Email
from apps.metronom_commons.fields import (LandlinePhoneNumberField,
                                          MobilePhoneNumberField)
from apps.metronom_commons.models import UserAccessMetronomBaseModel
from apps.persons.models import Person

User = get_user_model()


class AbstractEmployment(UserAccessMetronomBaseModel):
    class Meta:
        abstract = True

    position_name = models.CharField(_('Position'), max_length=255, null=True, blank=True)

    landline_phone = LandlinePhoneNumberField(blank=True, null=True)
    mobile_phone = MobilePhoneNumberField(blank=True, null=True)


class Employment(AbstractEmployment):

    email = models.OneToOneField(Email, related_name='employment', verbose_name=_('Email'), null=True, blank=True)

    person = models.ForeignKey(Person, related_name="employments", on_delete=models.CASCADE, editable=False)
    company = models.ForeignKey(Company, related_name="employments", on_delete=models.CASCADE, editable=False)

    class Meta:
        verbose_name = _('Employment')
        verbose_name_plural = _('Employments')
        unique_together = ("person", "company")

    def __str__(self):
        return f'{self.person} {_("working at")} {self.company}'


def populate_person_latest_employment(sender, instance, created, *args, **kwargs):
    if created:
        person = instance.person
        person.latest_employment = instance
        person.save(update_fields=['latest_employment'])


post_save.connect(populate_person_latest_employment, Employment, dispatch_uid="populate_person_latest_employment")



