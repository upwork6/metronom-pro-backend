from django.test import TestCase

from apps.employments.tests.factories import EmploymentFactory


class TestEmployment(TestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.sut = EmploymentFactory()

    def test_sut(self):
        self.assertTrue(hasattr(self, 'sut'))
        self.assertIsNotNone(self.sut)

    # ## Employment specific fields: ####

    def test_email(self):
        self.assertTrue(hasattr(self.sut, 'email'))
        self.assertIsNotNone(self.sut.email)

    def test_person(self):
        self.assertTrue(hasattr(self.sut, 'person'))
        self.assertIsNotNone(self.sut.person)

    def test_company(self):
        self.assertTrue(hasattr(self.sut, 'company'))
        self.assertIsNotNone(self.sut.company)

    # ## Abstract Employment specific fields: ####

    def test_position_name(self):
        self.assertTrue(hasattr(self.sut, 'position_name'))
        self.assertIsNotNone(self.sut.position_name)

    def test_landline_phone(self):
        self.assertTrue(hasattr(self.sut, 'landline_phone'))
        self.assertIsNotNone(self.sut.landline_phone)

    def test_mobile_phone(self):
        self.assertTrue(hasattr(self.sut, 'mobile_phone'))
        self.assertIsNotNone(self.sut.mobile_phone)
