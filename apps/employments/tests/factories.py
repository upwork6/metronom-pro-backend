import random

import factory

from apps.companies.models import Company
from apps.companies.tests.factories import CompanyFactory
from apps.contracts.tests.factories import ContractFactory
from apps.emails.tests.factories import EmailFactory
from apps.employments.models import AbstractEmployment, Employment
from apps.persons.models import Person
from apps.persons.tests.factories import PersonFactory
from apps.users.tests.factories import UserFactory


class AbstractEmploymentFactory(factory.DjangoModelFactory):
    email = factory.SubFactory(EmailFactory)
    position_name = factory.Iterator(["Geschäftsführer", "CTO", "Frontend-Entwickler", "Backend-Entwickler", "UI-Designer", "UI/UX-Designer", "UX-Designer", "Produktmanager", "Vertriebler", "Verkauf", "Einkauf", "CEO", "Buchhalter", "DirektorVertrieb", "Angestellter", "Bürokraft", "Consultant", "SeniorPython-Entwickler", "PHP-Entwickler", "IT-Administrator", "Chefredakteur", "Redakteur"])

    # Can't use the factory.Faker('phone_number', locale="de_DE") because for some reasons (didn't check them though)
    # it has weird behaviour with the PhoneNumberField. To be more accurate it sometimes generate empty instances of
    # the PhoneNumberField and assign them to the Person model instance. And when this happen the string representation
    # of the PhoneNumberField becomes really weird - "+NoneNone"
    landline_phone = factory.Sequence(lambda n: "+494854073{}{}".format(n, random.choice(range(10, 99))))
    mobile_phone = factory.Sequence(lambda n: "+498097909{}{}".format(n, random.choice(range(10, 99))))

    class Meta:
        model = AbstractEmployment
        abstract = True


class EmploymentFactory(AbstractEmploymentFactory):
    person = factory.SubFactory(PersonFactory)
    company = factory.SubFactory(CompanyFactory)

    class Meta:
        model = Employment


class EmploymentNiceFactory(AbstractEmploymentFactory):
    person = factory.Iterator(Person.objects.filter(employments__isnull=True))
    company = factory.Iterator(Company.objects.filter(employments__isnull=True))

    class Meta:
        model = Employment
