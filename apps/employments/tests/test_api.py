
import copy
from operator import itemgetter
from typing import List

from django.urls import reverse
from rest_framework import status

from apps.companies.models import Company
from apps.companies.tests.factories import CompanyFactory
from apps.emails.models import Email
from apps.emails.tests.factories import EmailFactory
from apps.employments.models import Employment
from apps.employments.tests.factories import EmploymentFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.persons.models import Person
from apps.persons.tests.factories import PersonFactory

ListOfDict = List[dict]


class TestCreateEmployment(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:employment-list')
        cls.landline_phone = '+79001112233'
        cls.landline_phone2 = '+48500500502'
        cls.mobile_phone = '+48500500500'
        cls.email = 'cto@company.com'
        cls.email2 = 'test-2@company.com'
        cls.company = CompanyFactory()
        cls.person = PersonFactory()
        cls.post_data = {
            'person_uuid': str(cls.person.id),
            'company_uuid': str(cls.company.id),
            'position_name': 'CTO',
            'email': cls.email,
        }

        cls.fake_uuid = '921d9f85-5b5a-42d8-afcb-47eb5d48c0d4'

    def test_minimal_post(self):
        response = self.user_client.post(self.endpoint, data=self.post_data, format='json')
        assert response.status_code == status.HTTP_201_CREATED

        employment = Employment.objects.get(person=self.person, company=self.company)

        assert response.data['id'] == str(employment.id)
        assert response.data['person_uuid'] == self.person.id
        assert response.data['company_uuid'] == self.company.id
        assert response.data['position_name'] == 'CTO'
        assert response.data['email'] == 'cto@company.com'
        assert response.data['landline_phone'] is None
        assert response.data['mobile_phone'] is None

    def test_two_employments_for_company_and_person_are_not_allowed(self):
        response = self.user_client.post(self.endpoint, data=self.post_data, format='json')
        assert response.status_code == status.HTTP_201_CREATED
        response = self.user_client.post(self.endpoint, data=self.post_data, format='json')
        self.assertErrorResponse(
            response,
            {"non_field_errors": ["The fields person, company have to form a unique quantity."]}
        )

    def test_post_with_landline_phone(self):
        post_data = copy.copy(self.post_data)
        post_data['landline_phone'] = self.landline_phone
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        print(response.data)
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data['landline_phone'] == self.landline_phone

    def test_post_with_mobile_phone(self):
        post_data = copy.copy(self.post_data)
        post_data['mobile_phone'] = self.mobile_phone
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        print(response.data)
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data['mobile_phone'] == self.mobile_phone

    def test_wrong_person_uuid_should_fail(self):
        post_data = copy.copy(self.post_data)
        post_data['person_uuid'] = self.fake_uuid
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert "object does not exist" in str(response.data)

    def test_wrong_company_uuid_should_fail(self):
        post_data = copy.copy(self.post_data)
        post_data['company_uuid'] = self.fake_uuid
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert "object does not exist" in str(response.data)

    def test_new_employment_should_change_persons_latest_employment(self):
        self.person = Person.objects.get(pk=self.person.pk)
        assert self.person.latest_employment is None

        employment = EmploymentFactory(person=self.person, company=self.company)
        self.person = Person.objects.get(pk=self.person.pk)
        assert self.person.latest_employment.pk == employment.pk

    def test_new_employment_should_be_reflected_in_contacts_company_list(self):
        """ The new employment will change the companies employment number """
        assert self.company.employment_count == 0
        EmploymentFactory(person=self.person, company=self.company)
        # Method 'refresh_from_db' isn't suited because the property 'employment_count' is cached
        self.company = Company.objects.get(pk=self.company.pk)
        assert self.company.employment_count == 1

    def test_new_employment_should_be_reflected_in_contacts_persons_list(self):
        """ The new employment will change the persons main company, phone and email """
        person = PersonFactory()
        assert person.company is None
        EmploymentFactory(
            person=person,
            company=self.company,
            landline_phone=self.landline_phone,
            email=EmailFactory(email=self.email),
        )
        # Method 'refresh_from_db' isn't suited because the property 'company' is cached
        person = Person.objects.get(pk=person.pk)
        assert person.company.pk == self.company.pk
        assert str(person.main_phone) == self.landline_phone
        assert str(person.main_email) == self.email

        company2 = CompanyFactory()
        EmploymentFactory(
            person=person,
            company=company2,
            landline_phone=self.landline_phone2,
            email=EmailFactory(email=self.email2),
        )
        person = Person.objects.get(pk=person.pk)
        assert person.company.pk == company2.pk
        assert str(person.main_phone) == self.landline_phone2
        assert str(person.main_email) == self.email2

    def test_add_employment_without_person_should_not_be_possible(self):
        post_data = copy.copy(self.post_data)
        del post_data['person_uuid']
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_add_employment_without_company_should_not_be_possible(self):
        post_data = copy.copy(self.post_data)
        del post_data['company_uuid']
        response = self.user_client.post(self.endpoint, data=post_data, format='json')
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_only_company_and_person_are_required(self):
        person_and_company = {
            'person_uuid': self.post_data['person_uuid'],
            'company_uuid': self.post_data['company_uuid'],
        }
        response = self.user_client.post(self.endpoint, data=person_and_company, format='json')
        assert response.status_code == status.HTTP_201_CREATED


class TestUpdateEmployment(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.person = PersonFactory()
        cls.company = CompanyFactory()
        cls.email1 = 'test-1@test.com'
        cls.email2 = 'test-2@test.com'
        cls.landline_phone1 = "+79001112233"
        cls.landline_phone2 = "+48500500502"

        cls.employment = EmploymentFactory(
            person=cls.person,
            company=cls.company,
            email=EmailFactory(email=cls.email1),
            position_name="CTO",
            landline_phone=cls.landline_phone1,
            mobile_phone="+48500500500"
        )

        cls.post_data = {}

        cls.endpoint = reverse('api:employment-detail', kwargs={'pk': str(cls.employment.id)})

    def test_post_not_allowed(self):
        self.post_data['position_name'] = "CEO"
        response = self.user_client.post(self.endpoint, data=self.post_data, format='json')
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_put_not_allowed(self):
        self.post_data.update({
            'position_name': 'CEO',
        })

        response = self.user_client.put(self.endpoint, data=self.post_data, format='json')
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_update_company_should_not_work(self):
        new_company = CompanyFactory()
        patch_data = {
            'company_uuid': new_company.pk,
        }
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_update_person_should_not_work(self):
        new_person = PersonFactory()
        patch_data = {
            'person_uuid': new_person.pk,
        }
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_update_person_to_be_nested(self):
        patch_data = {}
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')
        self.assertIsNone(response.data.get('person_uuid', None))
        self.assertIsNone(response.data.get('company_uuid', None))
        self.assertIsNotNone(response.data.get('person', None))

    def test_update_position_name(self):
        self.post_data['position_name'] = "CEO"
        response = self.user_client.patch(self.endpoint, data=self.post_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert response.data['position_name'] == "CEO"

    def test_update_email(self):
        self.post_data['email'] = "newmail@company.com"
        response = self.user_client.patch(self.endpoint, data=self.post_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert response.data['email'] == "newmail@company.com"

    def test_update_email_should_update_the_instance(self):
        patch_data = {
            'email': self.email2,
        }
        response = self.user_client.patch(self.endpoint, data=patch_data, format='json')

        assert response.status_code == status.HTTP_200_OK
        email_instance_has_been_changed = not Email.objects.filter(email=self.email1).exists()
        assert email_instance_has_been_changed
        assert response.data['email'] == self.email2

    def test_update_landline_phone(self):
        self.post_data['landline_phone'] = "+79002222233"
        response = self.user_client.patch(self.endpoint, data=self.post_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert response.data['landline_phone'] == "+79002222233"

    def test_update_mobile_phone(self):
        self.post_data['mobile_phone'] = "+48600600600"
        response = self.user_client.patch(self.endpoint, data=self.post_data, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert response.data['mobile_phone'] == "+48600600600"


class TestDeleteEmployment(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.employment = EmploymentFactory(
            person=PersonFactory(),
            company=CompanyFactory(),
            email=EmailFactory(),
            position_name="CTO",
            landline_phone="+79001112233",
            mobile_phone="+48500500500"
        )

        cls.endpoint = reverse('api:employment-detail', kwargs={'pk': str(cls.employment.id)})

    def test_delete_should_be_allowed_for_users(self):
        response = self.user_client.delete(self.endpoint, format='json')
        assert response.status_code == status.HTTP_204_NO_CONTENT


class TestPersonAndCompanyListing(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.person = PersonFactory()
        cls.company = CompanyFactory()
        cls.email1 = 'test-1@test.test'
        cls.email2 = 'test-2@test.test'
        cls.landline_phone1 = "+79001112233"
        cls.landline_phone2 = "+79001112231"

        cls.employment = EmploymentFactory(
            person=cls.person,
            company=cls.company,
            email=EmailFactory(email=cls.email1),
            position_name="CTO",
            landline_phone=cls.landline_phone1,
            mobile_phone="+48500500500"
        )

        cls.persons_list_endpoint = reverse('api:person-list')
        cls.persons_detail_endpoint = reverse('api:person-detail', kwargs={'id': str(cls.person.id)})
        cls.companies_list_endpoint = reverse('api:company-list')
        cls.companies_detail_endpoint = reverse('api:company-detail', kwargs={'id': str(cls.company.id)})

    def _get_employment_ids_from_employments_response_data(self, employment_data):
        given_employment_ids = list(map(itemgetter('id'), employment_data))
        return given_employment_ids

    def _get_employment_from_list_by_id(self, employments: ListOfDict, employment_id):
        for emp in employments:
            if emp['id'] == str(employment_id):
                return emp
        return None

    def test_new_employment_should_be_listed_in_persons_details(self):
        response = self.user_client.get(self.persons_detail_endpoint, format='json')
        employment_ids = [str(self.employment.pk)]
        given_employment_ids = self._get_employment_ids_from_employments_response_data(response.data['employments'])
        assert employment_ids == given_employment_ids

        employment2 = EmploymentFactory(
            person=self.person,
            company=CompanyFactory(),
            email=EmailFactory(),
            position_name="CTO",
            landline_phone="+79001112233",
            mobile_phone="+48500500500"
        )
        response = self.user_client.get(self.persons_detail_endpoint, format='json')
        given_employment_ids = self._get_employment_ids_from_employments_response_data(response.data['employments'])
        assert str(employment2.pk) in given_employment_ids

    def test_employment_should_be_updated_in_persons_details(self):
        self.employment.landline_phone = self.landline_phone2
        self.employment.save()

        response = self.user_client.get(self.persons_detail_endpoint, format='json')
        employment_from_response = self._get_employment_from_list_by_id(response.data['employments'],
                                                                        self.employment.pk)
        assert employment_from_response['landline_phone'] == self.landline_phone2

    def test_two_employment_should_be_both_shown_in_persons_details(self):
        employment2 = EmploymentFactory(
            person=self.person,
            company=CompanyFactory(),
            email=EmailFactory(),
            position_name="CTO",
            landline_phone="+79001112233",
            mobile_phone="+48500500500"
        )
        response = self.user_client.get(self.persons_detail_endpoint, format='json')
        given_employment_ids = self._get_employment_ids_from_employments_response_data(response.data['employments'])
        ids_from_db = [
            str(self.employment.pk),
            str(employment2.pk),
        ]
        assert set(given_employment_ids) == set(ids_from_db)

    def test_new_employment_should_be_listed_in_companies_details(self):
        employment2 = EmploymentFactory(
            person=PersonFactory(),
            company=self.company,
            email=EmailFactory(),
            position_name="CTO",
            landline_phone="+79001112233",
            mobile_phone="+48500500500"
        )
        response = self.user_client.get(self.companies_detail_endpoint, format='json')
        given_employment_ids = self._get_employment_ids_from_employments_response_data(response.data['employments'])
        assert str(employment2.pk) in given_employment_ids

    def test_employment_should_be_updated_in_companies_details(self):
        self.employment.email.email = self.email2
        self.employment.email.save()

        response = self.user_client.get(self.companies_detail_endpoint, format='json')

        employment_from_response = self._get_employment_from_list_by_id(response.data['employments'],
                                                                        self.employment.pk)
        assert employment_from_response['email'] == self.email2

    def test_two_employment_should_be_both_shown_in_companies_details(self):
        employment2 = EmploymentFactory(
            person=PersonFactory(),
            company=self.company,
            email=EmailFactory(),
            position_name="CTO",
            landline_phone="+79001112233",
            mobile_phone="+48500500500"
        )

        response = self.user_client.get(self.companies_detail_endpoint, format='json')

        given_employment_ids = self._get_employment_ids_from_employments_response_data(response.data['employments'])
        ids_from_db = [
            str(self.employment.pk),
            str(employment2.pk),
        ]
        assert set(given_employment_ids) == set(ids_from_db)

    def test_new_employment_should_be_listed_in_persons_list(self):
        person2 = PersonFactory()
        employment2 = EmploymentFactory(
            person=person2,
            company=CompanyFactory(),
            email=EmailFactory(),
            position_name="CTO",
            landline_phone="+79001112233",
            mobile_phone="+48500500500"
        )

        response = self.user_client.get(self.persons_list_endpoint)

        given_person2 = [p for p in response.data if p['public_id'] == person2.public_id][0]
        given_employment_ids = self._get_employment_ids_from_employments_response_data(given_person2['employments'])
        assert str(employment2.pk) in given_employment_ids

    def test_employment_should_be_updated_in_persons_list(self):
        self.employment.landline_phone = self.landline_phone2
        self.employment.save()

        response = self.user_client.get(self.persons_list_endpoint)

        given_person = [p for p in response.data if p['public_id'] == self.person.public_id][0]
        assert given_person['employments'][0]['landline_phone'] == self.landline_phone2

    def test_two_employment_should_be_both_shown_in_persons_list(self):
        employment2 = EmploymentFactory(
            person=self.person,
            company=CompanyFactory(),
            email=EmailFactory(),
            position_name="CTO",
            landline_phone="+79001112233",
            mobile_phone="+48500500500"
        )
        response = self.user_client.get(self.persons_list_endpoint)
        given_person = [p for p in response.data if p['public_id'] == self.person.public_id][0]
        given_employment_ids = self._get_employment_ids_from_employments_response_data(given_person['employments'])
        ids_from_db = [
            str(self.employment.pk),
            str(employment2.pk),
        ]
        assert set(given_employment_ids) == set(ids_from_db)

    def test_new_employment_should_be_listed_in_companies_list(self):
        employment2 = EmploymentFactory(
            person=PersonFactory(),
            company=self.company,
            email=EmailFactory(),
            position_name="CTO",
            landline_phone="+79001112233",
            mobile_phone="+48500500500"
        )
        response = self.user_client.get(self.companies_list_endpoint)
        given_company = [p for p in response.data if p['public_id'] == self.company.public_id][0]
        given_employment_ids = self._get_employment_ids_from_employments_response_data(given_company['employments'])
        assert str(employment2.pk) in given_employment_ids

    def test_employment_should_be_updated_in_companies_list(self):
        self.employment.landline_phone = self.landline_phone2
        self.employment.save()

        response = self.user_client.get(self.companies_list_endpoint)

        given_company = [p for p in response.data if p['public_id'] == self.company.public_id][0]
        assert given_company['employments'][0]['landline_phone'] == self.landline_phone2

    def test_two_employment_should_be_both_shown_in_companies_list(self):
        employment2 = EmploymentFactory(
            person=PersonFactory(),
            company=self.company,
            email=EmailFactory(),
            position_name="CTO",
            landline_phone="+79001112233",
            mobile_phone="+48500500500"
        )
        response = self.user_client.get(self.companies_list_endpoint)
        given_company = [p for p in response.data if p['public_id'] == self.company.public_id][0]
        given_employment_ids = self._get_employment_ids_from_employments_response_data(given_company['employments'])
        ids_from_db = [
            str(self.employment.pk),
            str(employment2.pk),
        ]
        assert set(given_employment_ids) == set(ids_from_db)
