from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class EmploymentsConfig(AppConfig):
    name = 'apps.employments'
    verbose_name = _('Employments')