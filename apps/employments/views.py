from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.api.mixins import LoggerMixin

from .models import Employment
from .serializers import EmploymentSerializer


class EmploymentViewSet(LoggerMixin, ModelViewSet):
    """Employments (Persons working for a Company)"""

    permission_classes = (IsAuthenticated,)
    serializer_class = EmploymentSerializer
    queryset = Employment.objects.all()
    http_method_names = ['post', 'patch', 'delete', 'head', 'options', 'trace']

    def update(self, request, *args, **kwargs):
        from apps.employments.serializers.plain import EmploymentResponseSerializer

        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(EmploymentResponseSerializer(instance).data)
