from rest_framework import serializers

from apps.accounting.models import Allocation, AllocationCategory
from apps.articles.models import TemplateArticle


class AllocationCategorySerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source="id", required=False)

    class Meta:
        model = AllocationCategory
        fields = ('uuid', 'name')


class CompanyAllocationSerializer(serializers.ModelSerializer):
    category_uuid = serializers.PrimaryKeyRelatedField(
        source="category", queryset=AllocationCategory.objects.all(), write_only=True
    )

    class Meta:
        model = Allocation
        fields = (
            'public_id',
            'id',
            'name',
            'category_uuid'
        )


class AllocationSerializer(serializers.ModelSerializer):
    category_uuid = serializers.PrimaryKeyRelatedField(source="category", queryset=AllocationCategory.objects.all(),
                                                       write_only=True)
    category = AllocationCategorySerializer(read_only=True)

    class Meta:
        model = Allocation
        fields = (
            'public_id',
            'id',
            'name',
            'category_uuid',
            'category',
            'description',
        )
