import pytest
from rest_framework import status
from rest_framework.reverse import reverse

from apps.accounting.models import AllocationCategory
from apps.accounting.tests.factories import (AllocationCategoryFactory,
                                             AllocationFactory)
from apps.companies.tests.factories import CompanyFactory
from apps.metronom_commons.test import MetronomBaseAPITestCase
from apps.articles.tests.factories import TemplateArticleFactory


class TestAllocationList(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:allocation-list')
        AllocationFactory.create_batch(size=4)
        cls.response = cls.admin_client.get(cls.endpoint, format='json')

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_length(self):
        assert len(self.response.data) == 4

    def test_response_keys(self):
        expected_keys = ['public_id', 'id', 'name', 'category', 'description']
        for x in self.response.data:
            self.assertTrue(all([key in expected_keys for key in x.keys()]))


class TestAllocationPost(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api:allocation-list')
        cls.category = AllocationCategoryFactory()

        cls.post_data = {
            'name': 'New Allocation',
            'public_id': 4444,
            'category_uuid': cls.category.pk
        }

        cls.response = cls.admin_client.post(cls.endpoint, cls.post_data, format='json')

    def test_response_allocation_post_status(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_response_allocation_post_name(self):
        assert self.response.data.get('name') == 'New Allocation'

    def test_response_allocation_post_public_id(self):
        assert self.response.data['public_id'] == 4444

    def test_response_allocation_post_category(self):
        assert self.response.data['category']['uuid'] == str(self.category.pk)
        assert self.response.data['category']['name'] == self.category.name

    def test_permissions_allocation_post_create(self):
        """ Check that only groups that have create access to this endpoint are Admin, Backoffice
        """
        post_data = {
            'name': 'New Allocation 2',
            'public_id': 4445,
            'category_uuid': self.category.id
        }
        response = self.user_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        post_data = {
            'name': 'New Allocation 3',
            'public_id': 4446,
            'category_uuid': self.category.id
        }
        response = self.admin_client.post(self.endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)


class TestAllocationPatch(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.allocation = AllocationFactory()
        cls.category = AllocationCategoryFactory()

        cls.patch_data = {
            'name': 'New Allocation'
        }

        cls.endpoint = reverse('api:allocation-detail', kwargs={'pk': cls.allocation.pk})
        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format='json')

    def test_response_allocation_patch_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_allocation_patch_name(self):
        assert self.response.data.get('name') == self.patch_data.get('name')

    def test_response_allocation_patchpublic_id(self):
        patch_data = dict(public_id=1234)
        self.response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert self.response.data.get('public_id') == patch_data.get('public_id')

    def test_permissions_allocation_patch_read(self):
        """ Check that only groups that have read access to this endpoint are Admin, Backoffice, User
        """
        response = self.user_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)

    def test_permissions_patch_allocation_patch_not_used(self):
        """ Check that only groups that have patch access to this endpoint are Admin, Backoffice
        """

        patch_data = {
            'name': "New name"
        }
        response = self.user_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_patch_allocation_patch_used(self):
        """ Check that only groups that have patch access to this endpoint are Admin, Backoffice
        """
        article = TemplateArticleFactory(allocation=self.allocation)
        patch_data = {
            'name': "New name"
        }
        response = self.backoffice_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_delete_allocation_patch_not_used(self):
        """ Check that only groups that have delete access to this endpoint are Admin, Backoffice
        """
        allocation2 = AllocationFactory()
        detail_endpoint = reverse('api:allocation-detail', args=[allocation2.id])
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)
        allocation2 = AllocationFactory()
        detail_endpoint = reverse('api:allocation-detail', args=[allocation2.id])
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_delete_allocation_patch_used(self):
        """ Check that only groups that have delete access to this endpoint are Admin, Backoffice
        """
        allocation2 = AllocationFactory()
        article = TemplateArticleFactory(allocation=allocation2)
        detail_endpoint = reverse('api:allocation-detail', args=[allocation2.id])
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used Allocation"]
            }
        )
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 400)

    def test_response_category(self):
        new_category = AllocationCategoryFactory()
        patch_data = dict(category_uuid=new_category.pk)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert response.data['category']['uuid'] != str(self.category.pk)
        assert response.data['category']['uuid'] == str(new_category.pk)
        assert response.data['category']['name'] != self.category.name
        assert response.data['category']['name'] == new_category.name


class TestAllocationDelete(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.allocation = AllocationFactory()
        cls.endpoint = reverse('api:allocation-detail', kwargs={'pk': cls.allocation.pk})

    def test_non_admin_cannot_delete_allocation(self):
        response = self.user_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_response_allocation_delete(self):
        response = self.admin_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_204_NO_CONTENT


class TestCompanyAllocationBase(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company_allocation_name = "test company allocation name"
        cls.company = CompanyFactory()
        cls.allocation1 = AllocationFactory(name=cls.company_allocation_name)
        cls.allocation2 = AllocationFactory()
        cls.company.allocations.add(cls.allocation1)
        cls.company.allocations.add(cls.allocation2)
        cls.company.save()


class TestCompanyAllocationList(TestCompanyAllocationBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.endpoint = reverse('api_company:company-allocations-list', kwargs={
            'company_id': cls.company.pk
        })

        cls.response = cls.user_client.get(cls.endpoint)

    def test_allocation_list_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_allocation_list_response_length(self):
        assert len(self.response.data) == 2

    def test_allocation_list_response_keys(self):
        expected_keys = ['public_id', 'id', 'name']
        for x in self.response.data:
            self.assertTrue(all([key in expected_keys for key in x.keys()]))


class TestCompanyAllocationPost(TestCompanyAllocationBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.post_data = {
            'name': "Some Allocation 1",
            'public_id': 4444,
            'category_uuid': AllocationCategory.objects.last().pk
        }

        cls.endpoint = reverse('api_company:company-allocations-list', kwargs={
            'company_id': cls.company.pk
        })

        cls.response = cls.user_client.post(cls.endpoint, cls.post_data, format='json')

    def test_company_allocation_post_response_status(self):
        assert self.response.status_code == status.HTTP_201_CREATED

    def test_company_allocation_post_response_name(self):
        assert self.response.data.get('name') == "Some Allocation 1"

    def test_company_allocation_post_response_public_id(self):
        assert self.response.data.get('public_id') == self.post_data.get('public_id')


class TestCompanyAllocationPatch(TestCompanyAllocationBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.patch_data = {
            'name': 'Patch Company Allocation'
        }

        cls.endpoint = reverse('api_company:company-allocations-detail', kwargs={
            'company_id': cls.company.pk, 'id': cls.allocation1.pk
        })
        cls.response = cls.admin_client.patch(cls.endpoint, cls.patch_data, format='json')

    def test_company_allocation_patch_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_company_allocation_patch_response_allocation_name(self):
        assert self.response.data.get('name') == self.patch_data.get('name')

    def test_company_allocation_patch_response_public_id(self):
        patch_data = dict(public_id=1234)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')
        assert response.data.get('public_id') == patch_data.get('public_id')

    def test_company_allocation_patch_response_category(self):
        new_category = AllocationCategoryFactory()
        patch_data = dict(category_uuid=new_category.pk)
        response = self.admin_client.patch(self.endpoint, patch_data, format='json')

        assert response.status_code == status.HTTP_200_OK

        self.allocation1.refresh_from_db()
        assert self.allocation1.category.pk == new_category.pk


class TestCompanyAllocationDelete(TestCompanyAllocationBase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.allocation = AllocationFactory()
        cls.endpoint = reverse('api_company:company-allocations-detail', kwargs={
            'company_id': cls.company.pk, 'id': cls.allocation1.pk
        })

    def test_company_allocation_delete_not_allowed(self):
        response = self.admin_client.delete(self.endpoint)
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED


class TestAllocationCategory(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.allocation_category1 = AllocationCategoryFactory()
        cls.list_endpoint = reverse('api:allocationcategory-list')
        cls.detail_endpoint = reverse('api:allocationcategory-detail', args=[cls.allocation_category1.pk])

    def test_list_allocation_categories(self):
        response = self.admin_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]['name'], self.allocation_category1.name)

    def test_create_allocation_categories(self):
        before = AllocationCategory.objects.count()
        post_data = {
            'name': "New name"
        }
        response = self.admin_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(before + 1, AllocationCategory.objects.count())

    def test_retrieve(self):
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], self.allocation_category1.name)

    def test_patch(self):
        patch_data = {
            'name': "New name"
        }
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.detail_endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], patch_data['name'])

    def test_delete(self):
        allocation_category2 = AllocationCategoryFactory()
        detail_endpoint = reverse('api:allocationcategory-detail', args=[allocation_category2.pk])
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_read(self):
        """ Check that only groups that have read access to this endpoint are Admin, Backoffice, User
        """
        response = self.user_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.accounting_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.get(self.list_endpoint)
        self.assertEqual(response.status_code, 200)

    def test_permissions_create(self):
        """ Check that only groups that have create access to this endpoint are Admin, Backoffice
        """
        post_data = {
            'name': "New name"
        }
        response = self.user_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)
        post_data = {
            'name': "New name2"
        }
        response = self.admin_client.post(self.list_endpoint, post_data, format="json")
        self.assertEqual(response.status_code, 201)

    def test_permissions_patch_allocation_category_not_used(self):
        """ Check that only groups that have patch access to this endpoint are Admin, Backoffice
        """
        patch_data = {
            'name': "New name"
        }
        response = self.user_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_patch_allocation_category_used(self):
        """ Check that only groups that have patch access to this endpoint are Admin, Backoffice
        """
        allocation = AllocationFactory(category=self.allocation_category1)
        patch_data = {
            'name': "New name"
        }
        response = self.backoffice_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)
        response = self.admin_client.patch(self.detail_endpoint, patch_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_permissions_delete_allocation_category_not_used(self):
        """ Check that only groups that have delete access to this endpoint are Admin, Backoffice
        """
        allocation_category2 = AllocationCategoryFactory()
        detail_endpoint = reverse('api:allocationcategory-detail', args=[allocation_category2.id])
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)
        allocation_category2 = AllocationCategoryFactory()
        detail_endpoint = reverse('api:allocationcategory-detail', args=[allocation_category2.id])
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 204)

    def test_permissions_delete_allocation_category_used(self):
        """ Check that only groups that have delete access to this endpoint are Admin, Backoffice
        """
        allocation_category2 = AllocationCategoryFactory()
        allocation = AllocationFactory(category=allocation_category2)
        detail_endpoint = reverse('api:allocationcategory-detail', args=[allocation_category2.id])
        response = self.user_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.accounting_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.hr_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 403)
        response = self.backoffice_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 400)
        response = self.admin_client.delete(detail_endpoint)
        self.assertEqual(response.status_code, 400)
        self.assertErrorResponse(
            response,
            {
                "__all__": ["You can't delete used AllocationCategory"]
            }
        )
