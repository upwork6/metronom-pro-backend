
import json

from django.core.cache import caches
from django.urls import reverse
from rest_framework import status

from apps.metronom_commons.test import MetronomBaseAPITestCase

from .factories import AllocationFactory

cache = caches['autocompletes']


class TestAllocationAutocomplete(MetronomBaseAPITestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.allocation1 = AllocationFactory()
        cls.allocation2 = AllocationFactory()
        cls.allocation3 = AllocationFactory()
        cls.united_url = reverse('autocompletes:united')
        cache.clear()
        cls.response = cls.user_client.get(cls.united_url)
        cls.response_body = json.loads(cls.response.content)['companies:allocations']

    def test_response(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_allocations(self):
        for allocation in [self.allocation1, self.allocation2, self.allocation3]:
            self.assertIn(str(allocation.id), self.response_body.keys())
            name = self.response_body.get(str(allocation.id))
            self.assertEqual(name, allocation.name)
