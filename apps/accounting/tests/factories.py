import factory

from apps.accounting.models import Allocation, AllocationCategory


class AllocationCategoryFactory(factory.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'Category Name: {}'.format(n))

    class Meta:
        model = AllocationCategory
        django_get_or_create = ('name',)


class AllocationFactory(factory.DjangoModelFactory):
    public_id = factory.Sequence(lambda n: n)
    name = factory.Sequence(lambda n: 'Name: {}'.format(n))
    category = factory.SubFactory(AllocationCategoryFactory)
    description = factory.Faker('sentence')

    class Meta:
        model = Allocation
        django_get_or_create = ('name',)
