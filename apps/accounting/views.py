
from dry_rest_permissions.generics import DRYPermissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.accounting.models import Allocation, AllocationCategory
from apps.accounting.serializers import (AllocationCategorySerializer,
                                         AllocationSerializer,
                                         CompanyAllocationSerializer)
from apps.api.filters import MetronomPermissionsFilter
from apps.api.mixins import LoggerMixin
from apps.articles.models import TemplateArticle
from apps.companies.models import Company
from apps.metronom_commons.mixins.views import DeleteForbiddenIfUsedMixin


class CompanyAllocationViewSet(LoggerMixin, ModelViewSet):
    """Allocations used for a company."""
    lookup_url_kwarg = 'id'
    serializer_class = CompanyAllocationSerializer
    filter_backends = (MetronomPermissionsFilter,)
    http_method_names = ['get', 'post', 'patch', 'head', 'options', 'trace']

    def get_queryset(self):
        """Return only allocations connected to the company defined with company_public_id."""
        company_id = self.kwargs.get('company_id', None)
        queryset = Allocation.objects.filter(companies=company_id)
        return queryset.order_by("public_id")

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            company = Company.objects.get(id=self.kwargs.get('company_id'))
            allocation = serializer.save()
            company.allocations.add(allocation)
            company.save()
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response(serializer.data)


class AllocationCategoryViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    queryset = AllocationCategory.objects.all().order_by('name')
    serializer_class = AllocationCategorySerializer
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    forbid_delete_if_used_in = {
        Allocation: ["category"]
    }


class AllocationViewSet(DeleteForbiddenIfUsedMixin, ModelViewSet):
    queryset = Allocation.objects.all().order_by('public_id')
    serializer_class = AllocationSerializer
    permission_classes = (DRYPermissions,)
    filter_backends = (MetronomPermissionsFilter,)
    forbid_delete_if_used_in = {
        TemplateArticle: ["allocation"]
    }
