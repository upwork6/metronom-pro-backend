from django.contrib import admin

from apps.accounting.models import Allocation, AllocationCategory


@admin.register(Allocation)
class AllocationAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'public_id',
        'category',
        'name'
    )
    search_fields = ('name',)
    list_filter = ('category', )


admin.site.register(AllocationCategory)
