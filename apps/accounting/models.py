
from django.contrib.auth.models import Group
from django.db import models
from django.utils.translation import ugettext_lazy as _
from dry_rest_permissions.generics import authenticated_users

from apps.metronom_commons.models import UserAccessBackofficeMetronomBaseModel


class AllocationCategory(UserAccessBackofficeMetronomBaseModel):
    name = models.CharField(_('Name'), max_length=255)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = _('Allocation Category')
        verbose_name_plural = _('Allocation Categories')


class Allocation(UserAccessBackofficeMetronomBaseModel):
    public_id = models.PositiveIntegerField(_('Account number'), help_text=_(
        'Unique public ID'), unique=True)
    name = models.CharField(_('Name'), max_length=255, null=False, unique=True)
    description = models.TextField(verbose_name=_('Description'), blank=True)
    category = models.ForeignKey(AllocationCategory, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Allocation')
        verbose_name_plural = _('Allocations')

    def __str__(self):
        return f'{self.public_id} {self.name}'
