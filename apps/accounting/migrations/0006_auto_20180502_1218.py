# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-02 12:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0005_auto_20180501_1505'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='allocation',
            name='created_date',
        ),
        migrations.RemoveField(
            model_name='allocation',
            name='updated_date',
        ),
        migrations.RemoveField(
            model_name='allocationcategory',
            name='created_date',
        ),
        migrations.RemoveField(
            model_name='allocationcategory',
            name='updated_date',
        ),
        migrations.AlterField(
            model_name='allocation',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='allocation',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified'),
        ),
        migrations.AlterField(
            model_name='allocationcategory',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='allocationcategory',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Modified'),
        ),
    ]
