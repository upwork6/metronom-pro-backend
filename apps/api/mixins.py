import time
import uuid

from django.conf import settings
from django.core.exceptions import ValidationError
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.utils.timezone import now

from apps.metronom_commons.logging import logger


class LoggerMixin(object):
    """ Logging for django-rest-framework"""

    message = ""

    def initial(self, request, *args, **kwargs):

        if settings.API_LOGGING is True:

            self.request.start = now()

            method = request.method.lower()

            view_method = request.method.lower()

            if hasattr(self, 'view_method'):
                attributes = getattr(self, view_method)
                view_name = (type(attributes.__self__).__module__ + '.' + type(attributes.__self__).__name__)
            else:
                view_name = ""

            if hasattr(self, 'action'):
                view_method = self.action if self.action else ''
            else:
                view_method = method.lower()

            origin = request.META.get('HTTP_HOST', u'Unknown')
            user_agent = request.META.get('HTTP_USER_AGENT', u'Unknown')
            query_param = request.query_params.dict()
            language = request.META.get('HTTP_ACCEPT_LANGUAGE', u'-')

            # TODO: Received JWT Token
            # TODO: Caching informations

            self.request_id = uuid.uuid4().hex
            user_pk = self.request.user.pk if self.request.user else "self.request.user is None"

            self.message = f"\n\n********* {method.upper()} " \
                           f"{request.get_full_path()} *********\n" \
                           f"Request ID: {self.request_id}\n" \
                           f"From: {origin}, " \
                           f"Client: {user_agent}, " \
                           f"Language: {language}, " \
                           f"Content: {request.content_type}, " \
                           f"User ID: {user_pk}\n" \
                           f"Function: {view_name}.{view_method}\n" \
                           f"Queryset: {query_param}\n" \
                           f"REQUEST: {request.body}\n" \
                           f"**********************************************\n"

        return super(LoggerMixin, self).initial(request, *args, **kwargs)

    def finalize_response(self, request, response, *args, **kwargs):
        """
        Returns the final response object.
        """

        # response.request = request
        other_response = super(LoggerMixin, self).finalize_response(request, response, *args, **kwargs)

        if settings.API_LOGGING is True:

            response_timedelta = now() - self.request.start
            response_ms = int(response_timedelta.total_seconds() * 1000)

            json_data = "-"

            if hasattr(other_response, "rendered_content"):
                json_data = other_response.rendered_content.decode('utf8').replace("'", '"')

            logger.debug(f"{self.message}"
                         f"Status: {other_response.status_code}\n"
                         f"RESPONSE: {json_data}\n"
                         f"Headers: {self.headers}\n"
                         f"Response time: {response_ms}ms"
                         )

            if getattr(settings, 'METRONOM_INFO_SETTINGS', None).get('GIT_COMMIT_HASH'):
                other_response['HTTP_GIT_BRANCH'] = getattr(
                    settings, 'METRONOM_INFO_SETTINGS', None
                ).get('GIT_COMMIT_HASH')
            else:
                other_response['HTTP_GIT_BRANCH'] = 'local'
            other_response['HTTP_X_RESPONSE_ID'] = self.request_id

        return other_response


class PublicIDLookupMixin(object):
    """
    Apply this mixin to any view or viewset to get multiple field filtering
    based on a `lookup_fields` attribute, instead of the default single field filtering.
    """

    def get_object(self):
        """
        Write Tests for this
        """

        queryset = self.get_queryset()

        try:
            value = int(self.kwargs["id"])
            filter = {'public_id': value}
        except ValueError:
            value = self.kwargs["id"]
            filter = {'id': value}

        try:
            obj = get_object_or_404(queryset, **filter)
        except ValidationError:
            raise Http404('No matches for the given query.')

        self.check_object_permissions(self.request, obj)
        return obj


class PerformanceDebuggingMixin(object):
    started = 0.0
    dispatch_time = 0.0
    db_time = 0.0
    serializer_time = 0.0

    def initial(self, request, *args, **kwargs):
        self.started = time.time()

        print("Using PerformanceDebuggingMixin...")

    def dispatch(self, request, *args, **kwargs):
        dispatch_start = time.time()

        response = super(PerformanceDebuggingMixin, self).dispatch(request, *args, **kwargs)

        self.dispatch_time = time.time() - dispatch_start

        return response

    def finalize_response(self, request, response, *args, **kwargs):
        finalization_start = time.time()

        response = super(PerformanceDebuggingMixin, self).finalize_response(request, response, *args, **kwargs)

        finalization_time = time.time() - finalization_start

        total = time.time() - self.started

        print("")
        print("----------------------------------------------------------")
        print(f"Time of View {self.__class__}:")
        print("Database lookup               | %.4fs" % self.db_time)
        print("Serialization                 | %.4fs" % self.serializer_time)
        print("Dispatch time                 | %.4fs" % self.dispatch_time)
        print("Response finalization         | %.4fs" % finalization_time)
        print("")
        print("Total time                    | %.4fs" % total)
        print("----------------------------------------------------------")
        print("")

        return response
