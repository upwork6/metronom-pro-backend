from django.conf import settings
from django.core.exceptions import ValidationError as DjangoValidationError
from django.utils.translation import ugettext_lazy as _

from rest_framework import status, serializers
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError as RestFrameworkValidationError
from rest_framework.views import exception_handler

from apps.metronom_commons.logging import logger
from apps.users.mixins import MeURLMixin
import traceback


class ObjectValidationError(RestFrameworkValidationError):
    """
        The ObjectValidationError received an identifier of the Object. It can be used to give an
        error on a specific object (you can use the uuid or the position)
    """

    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('Invalid object input.')
    default_code = 'object invalid'

    def __init__(self, detail=None, code=None, identifier=None):
        super(ObjectValidationError, self).__init__(detail, code)

        if identifier is not None:

            self.detail = {
                str(identifier): self.detail
            }


def core_exception_handler(exc, context):
    """
        For some errors we have special handlers for explicit handling. If an exception is thrown that we don't
        explicitly handle here, we want to delegate to the default exception handler offered by DRF - we just have
        a little cleanup then.

        All errors will be sent to the Frontend with the following format:

        {
            "errors": {
                "__all__": [
                    "I am general error not specific for one special field"
                ],
                "name": [
                    "I am error for a given field"
                ]
                "activity_steps": [
                    {
                        "start_date": [
                            "I am a nested error within the given activity steps"
                        ],
                        "step": {
                            "uuid": {
                                "098bd5a5-137b-49f3-87e3-9cbf240fe0d7": [
                                    "I am an error for that specific object."
                                ]
                            }
                        }
                    },
                ],
            },
            "status_code": 400                                                       // We still sent the HTTP Status code so this is redundant
        }

        When API_LOGGING=True, we also send some debugging information s

            "debugging_information": {
                "exception_class": "ValidationError",                               // the thrown Exception class
                "exception_raw_message": "{'name': 'No name with this'",            // the raw and not yet formatted error
                "url_context": {                                                    // the URL context (e.g. /api/projects/<project_pk>/)
                    "project_pk": "6c415171-d07d-4b3b-92af-a93b88c39164"
                },
                "received_context": {                                               // the full received POST context
                    "name": "test",
                    ....
                }
            }

    """

    logger.debug(f"Starting Exception handling for {exc} with context {context}")

    response = exception_handler(exc, context)
    handlers = {
        'NotFound': _handle_not_found_error,
        'Http404': _handle_not_found_error,
        'ValidationError': _handle_validation_error,
        'ImproperlyConfigured': _handle_improperly_configured_error,
    }

    # Lets dentify the type of the current exception to check if we should handle it
    exception_class = exc.__class__.__name__

    # This is the error given, if all things go wrong
    response_data_default = {
        'errors': 'Could not process this request because of an internal error'
    }

    # Some errors do not give an Response. We need to handle this case as well
    if response is None:
        response = Response(
            data=response_data_default,
            content_type="application/json",
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    # If this exception is one that we can handle, handle it.
    if exception_class in handlers:
        logger.debug(f"Exception class {exception_class} ({exc.__class__}) found - using handler.")

        try:
            return handlers[exception_class](exc, context, response)
        except Exception as exc2:
            logger.error("Exception in core_exception_handler.", exc_info=True, extra={
                'view': context['view'],
                'first_exc': exc,
                'second_exc': exc2,
                'response': response,
                'context': context,
            })

            response_data_error_in_error_handling = {
                'errors': 'Could not process this request because of an error at the error handling. Bad things happen!',
                'first_error': str(exc),
                'second_error': str(exc2),
                'traceback:': traceback.print_exc()
            }

            return Response(
                data=response_data_error_in_error_handling,
                content_type="application/json",
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

    # Otherwise return the response generated earlier by the default exception, just have a little cleanup
    else:
        logger.error(f"Exception class {exception_class} ({exc.__class__}) with error {exc} NOT found in handlers.", exc_info=True, extra={
            'view': context['view'],
            'exception': exc,
            'response': response,
            'context': context,
        })

        return _handle_generic_error(exc, context, response)


def _handle_generic_error(exc, context, response):
    """
        The generic error handling is used both as a fallback and for all other handlers.
        We take the response generated by DRF and wrap it in the `errors` key.
    """

    if 'errors' not in response.data:

        response.data = {
            'errors': response.data,
        }

    response.data.update({
        'status_code': response.status_code,
    })

    if settings.API_LOGGING is True:
        response.data.update({
            'debugging_information': {
                'exception_class': exc.__class__.__name__,
                'exception_raw_message': str(exc),
                'url_context': dict(context['kwargs'], **context['request'].query_params.dict()),
                'received_context': context['request'].data,
            }
        })

    return response


def _handle_validation_error(exc, context, response):
    """
        Handling of an Validation Error. We need to take care, that there are three types of Validation Errors

        1. DjangoValidationError (happening at the Model level, does not provide an HTTP response
        2. RestFrameworkValidationError (pimped up version by drf)
        3. ObjectValidationError (object-level Validation error)

    """

    response.status_code = status.HTTP_400_BAD_REQUEST

    if hasattr(response, "data"):
        response.data = {
            'errors': response.data
        }

    # TODO: Have some cleanup, right now we need to figure out, how all of those Exceptions behave
    if isinstance(exc, DjangoValidationError):
        if '__all__' in dict(exc):
            response.data = {
                'errors': dict(exc)
            }
        else:
            response.data = {
                'errors': dict(exc)
            }

    if isinstance(exc, RestFrameworkValidationError):

        error_dict = serializers.as_serializer_error(exc)

        if 'error' in error_dict:
            error_dict['__all__'] = error_dict.pop('error')

            response.data = {
                'errors': error_dict
            }

    return _handle_generic_error(exc, context, response)


def _handle_improperly_configured_error(exc, context, response):
    """
        Handling of an Validation Error

        Could be from the Model:
            {'__all__': ['Contract start/end dates overlap with another Contract.']}

    """

    response.status_code = status.HTTP_422_UNPROCESSABLE_ENTITY

    response.data = {
        'errors': {
            '__all__': str(exc)
        }
    }

    return _handle_generic_error(exc, context, response)


def _handle_not_found_error(exc, context, response):

    view = context.get('view', None)

    pk_message = ""

    if 'kwargs' in context and 'user_pk' in context['kwargs']:
        pk_message = f"for '{context['kwargs']['user_pk']}'"
    elif 'kwargs' in context and 'pk' in context['kwargs']:
        pk_message = f"for '{context['kwargs']['pk']}'"

    error_message = f"No objects found."

    if issubclass(view.__class__, MeURLMixin):
        # Lets have a little nicer message for the Users

        if pk_message:
            error_message = f"No user found {pk_message}. You must use 'me' or an (correct) 'uuid'"
        else:
            error_message = f"No users found"

    else:

        if pk_message:
            error_message = f"No object found {pk_message}. You must use an (correct) 'uuid'"

    response.data = {
        'errors': {
            'Not found': error_message
        }
    }

    return _handle_generic_error(exc, context, response)
