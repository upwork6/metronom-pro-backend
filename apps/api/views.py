from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from apps.api.mixins import LoggerMixin
from apps.metronom_commons.logging import logger


class MetronomBaseModelViewSet(LoggerMixin, ModelViewSet):
    """ We are using our own ModelViewSet with some special functionalities """

    def get_queryset(self):

        serializer = self.get_serializer()

        # Use the setup_eager_loading of a given Serializer
        if hasattr(serializer, "setup_eager_loading"):
            return serializer.setup_eager_loading(self.queryset)
        else:
            logger.warning(f"{serializer.__class__.__name__} does not have a setup_eager_loading method")
            return self.queryset

    # TODO: For things which have to be implemented
    # raise NotImplementedError(".filter_queryset() must be overridden.")


class MetronomInfoAPIView(LoggerMixin, APIView):
    def get(self, request, *args, **kwargs):
        return Response(
            getattr(settings, 'METRONOM_INFO_SETTINGS', None), status=status.HTTP_200_OK
        )
