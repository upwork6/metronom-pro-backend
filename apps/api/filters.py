from rest_framework.filters import BaseFilterBackend


class MetronomPermissionsFilter(BaseFilterBackend):
    """
    A filter backend that limits results

    """

    def filter_queryset(self, request, queryset, view):
        """
        Return a filtered queryset.
        """
        return queryset
