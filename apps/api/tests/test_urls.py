from django.test import TestCase
from django.urls import reverse, resolve


class TestMetronomInfoAPIViewUrls(TestCase):
    def test_info_api_view_reverse(self):
        assert reverse('api_info') == f'/api/info/'

    def test_info_api_view_resolve(self):
        assert resolve(f'/api/info/').view_name == 'api_info'
