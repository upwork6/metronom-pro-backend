from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet
from apps.api.mixins import LoggerMixin
from rest_framework.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from rest_framework.decorators import list_route


class ExceptionBasicViewSet(LoggerMixin, ViewSet):

    def list(self, request, *args, **kwargs):
        return Response({'is_okay': True}, status=status.HTTP_200_OK)

    @list_route()
    def validation_error(self, request):
        raise ValidationError(_('I am a ValidationError'))
