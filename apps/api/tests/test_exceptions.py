import json
import uuid
import pytest

from rest_framework import status
from rest_framework.reverse import reverse

from apps.metronom_commons.test import MetronomBaseAPITestCase


def _j(a):
    # TODO: Can be removed probably after my work
    return json.dumps(a, sort_keys=True)


class TestMetronomExceptionsAPIView(MetronomBaseAPITestCase):

    def test_exception_base_view_set_is_working_as_expected(self):

        self.endpoint = reverse('exception-test')
        self.response = self.user_client.get(self.endpoint)

        assert self.response.status_code == status.HTTP_200_OK
        assert self.response.data == {'is_okay': True}

    def test_validation_error(self):

        self.endpoint = reverse('exception-test-validation-error')
        self.response = self.user_client.get(self.endpoint)
        self.assertErrorResponse(
            self.response,
            {'__all__': ['I am a ValidationError']}
        )


class TestUserAPIExceptions(MetronomBaseAPITestCase):

    def test_user_api_works_correct_for_me(self):

        self.endpoint = reverse('api_users:user-worklog-list', kwargs={'user_pk': 'me'})
        self.response = self.user_client.get(self.endpoint)

        assert self.response.status_code == status.HTTP_200_OK
        assert self.response.data == []

    def test_user_api_handles_wrong_string(self):

        self.endpoint = reverse('api_users:user-worklog-list', kwargs={'user_pk': 'wrong'})
        self.response = self.user_client.get(self.endpoint)

        self.assertErrorResponse(
            self.response,
            {'Not found': "No user found for 'wrong'. You must use 'me' or an (correct) 'uuid'"},
            expected_status_code=404
        )

    def test_user_api_handles_wrong_integer(self):

        self.endpoint = reverse('api_users:user-worklog-list', kwargs={'user_pk': 1})
        self.response = self.user_client.get(self.endpoint)

        self.assertErrorResponse(
            self.response,
            {'Not found': "No user found for '1'. You must use 'me' or an (correct) 'uuid'"},
            expected_status_code=404
        )

    def test_user_api_handles_a_wrong_uuid(self):

        generated_uuid = uuid.uuid4()
        self.endpoint = reverse('api_users:user-worklog-list', kwargs={'user_pk': generated_uuid})
        self.response = self.user_client.get(self.endpoint)

        self.assertErrorResponse(
            self.response,
            {'Not found': f"No user found for '{generated_uuid}'. You must use 'me' or an (correct) 'uuid'"},
            expected_status_code=404
        )

    @pytest.mark.skip("Lets work on this when we have Permissions back working")
    def test_user_api_handles_a_permission_error(self):

        self.endpoint = reverse('api_users:contract-list', kwargs={'user_pk': self.test_backoffice_user.uuid})
        self.response = self.user_client.get(self.endpoint)

        assert self.response.status_code == status.HTTP_403_FORBIDDEN
        assert self.response.data == {
            "errors":  {'__all__':
                        f"No user found for '{ self.test_backoffice_user.uuid}'. "
                        "You must use 'me' or an (correct) 'uuid'"}}
