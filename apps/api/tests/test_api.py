import hashlib

from datetime import datetime
from django.test import override_settings
from django.utils import timezone
from rest_framework import status
from rest_framework.reverse import reverse

from apps.metronom_commons.test import MetronomBaseAPITestCase

DEPLOYMENT_DATE = timezone.now()
GIT_COMMIT_HASH = int(hashlib.sha1().hexdigest(), 16) % (10 ** 7)


@override_settings(
    METRONOM_INFO_SETTINGS={
        'DEPLOYMENT_DATE': DEPLOYMENT_DATE,
        'GIT_COMMIT_HASH': GIT_COMMIT_HASH
    }
)
class TestMetronomInfoAPIView(MetronomBaseAPITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.endpoint = reverse('api_info')
        cls.response = cls.user_client.get(cls.endpoint)

    def test_response_status(self):
        assert self.response.status_code == status.HTTP_200_OK

    def test_response_deployment_date(self):
        assert datetime.strftime(
            self.response.data.get('DEPLOYMENT_DATE'), '%Y-%m-%d %H-%m'
        ) == DEPLOYMENT_DATE.strftime('%Y-%m-%d %H-%m')

    def test_response_git_commit_hash(self):
        field = self.response.data.get('GIT_COMMIT_HASH')
        assert field == GIT_COMMIT_HASH
        assert len(str(field)) == len(str(GIT_COMMIT_HASH))
        assert len(str(field)) == 7
