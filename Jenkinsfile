import java.text.SimpleDateFormat

node {

  def project = "metronom-dev"
  def stagingPostgresURL = "${project}:europe-west3:metronom-staging-postgres"
  def productionPostgresURL = "${project}:europe-west3:metronom-production-postgres"
  def dateFormat = new SimpleDateFormat("yyyy-MM-dd/HH:mm")
  def date = new Date()
    
  checkout scm
  env.GIT_COMMIT = sh(script: "git rev-parse HEAD | cut -c1-7", returnStdout: true).trim()
  env.DEPLOYMENT_DATE = dateFormat.format(date)
    
  def backendimageTag = "eu.gcr.io/${project}/backend:${env.GIT_COMMIT}"
  def backendimageLatestTag = "eu.gcr.io/${project}/backend:latest"

  try {
    notifyBuild('STARTED')

  stage 'Push & Deploy'
  switch (env.BRANCH_NAME) {
    
    case "datetime-during-build":
      //Build image
      stage 'Build Backend image'
      sh("docker build --build-arg GIT_COMMIT=${env.GIT_COMMIT} --build-arg DEPLOYMENT_DATE=${env.DEPLOYMENT_DATE} -t ${backendimageTag} -f devops/docker/Dockerfile . --no-cache")
      //Push image
      stage 'Push Backend image to registry'
        sh("gcloud docker -- push ${backendimageTag}")

        break

    // Roll out to staging environment
    case "master":
      //Build image
      stage 'Build Backend image'
      sh("docker build --build-arg GIT_COMMIT=${env.GIT_COMMIT} --build-arg DEPLOYMENT_DATE=${env.DEPLOYMENT_DATE} -t ${backendimageTag} -t ${backendimageLatestTag} -f devops/docker/Dockerfile . --no-cache")
      //Push image
      stage 'Push Backend image to registry'
        sh("gcloud docker -- push ${backendimageTag}")
    // Change deployed image in staging environment to the one we just built
      stage 'Deploy Backend image to Kubernetes'
        sh("sed -i.bak 's#eu.gcr.io/metronom-dev/backend:PROMOTED_NUMBER#${backendimageTag}#' ./devops/kubernetes/backend.yaml")
        sh("sed -i.bak 's#POSTGRES-INSTANCE-CONNECTION-NAME#${stagingPostgresURL}#' ./devops/kubernetes/backend.yaml")
        sh("kubectl --namespace=staging apply -f devops/kubernetes/backend.yaml")

        stage 'Run database migrations'
            //Check if new deployment up & running!
                sh '''#!/bin/bash
                        OUTPUT=''
                        while [ "$OUTPUT" != Running ]; do
                        echo "New deployment not ready yet!";
                        sleep 5;
                        OUTPUT=$(kubectl get pods --selector=app=backend -n staging | awk '{print $3}' | tail -n 1)
                        done
                '''
            //Get new Backend pod name
            echo "New Backend pod deployed"
            def newBackendPod = sh(script: '''kubectl get pods --selector=app=backend -n staging | grep Running | awk '{print $1}' | tr -d '\n' ''', returnStdout: true)
            println newBackendPod
            //Run migrations command on Backend pod
            sh("kubectl exec -it -n staging ${newBackendPod} python manage.py migrate")
          // If migrations was succesfull, tag image as `latest` and push to repository
          stage 'Tag Backend image as latest'
          sh("gcloud docker -- push ${backendimageLatestTag}")
          //Send slack notification that new `latest` image is ready
          notifyBuild('New LATEST Backend image available!')
        //Re-create REDIS to flush cache
        stage 'Re-create REDIS container'
        def redispod = sh(script: '''kubectl get pods --selector=app=redis -n staging | grep redis | awk '{print $1}' | tr -d '\n' ''', returnStdout: true)
        println redispod
        sh("kubectl delete pods ${redispod} -n staging")
        break

    // Roll out to production environment
    case "production":
    // Wait for manual aproval
    timeout(time: 3600, unit: 'SECONDS') {
     input 'Do you want to proceed to the Production Deployment?'
    }
      //Build image
      stage 'Build Backend image'
      sh("docker build --build-arg GIT_COMMIT=${env.GIT_COMMIT} --build-arg DEPLOYMENT_DATE=${env.DEPLOYMENT_DATE} -t ${backendimageTag} -f devops/docker/Dockerfile . --no-cache")
      //Push image
      stage 'Push Backend image to registry'
        sh("gcloud docker -- push ${backendimageTag}")
    // Change deployed image in production environment to the one we just built
      stage 'Deploy Backend image to Kubernetes'
        sh("sed -i.bak 's#eu.gcr.io/metronom-dev/backend:PROMOTED_NUMBER#${backendimageTag}#' ./devops/kubernetes/backend.yaml")
        sh("sed -i.bak 's#POSTGRES-INSTANCE-CONNECTION-NAME#${productionPostgresURL}#' ./devops/kubernetes/backend.yaml")
        sh("kubectl --namespace=production apply -f devops/kubernetes/backend.yaml")

        stage('Run database migrations') {
            //Check if new deployment up & running!
                sh '''#!/bin/bash
                        OUTPUT=''
                        while [ "$OUTPUT" != Running ]; do
                        echo "New deployment not ready yet!";
                        sleep 5;
                        OUTPUT=$(kubectl get pods --selector=app=backend -n production | awk '{print $3}' | tail -n 1)
                        done
                '''
            //Get new Backend pod name
            echo "New Backend pod deployed"
            def newBackendPod = sh(script: '''kubectl get pods --selector=app=backend -n production | grep Running | awk '{print $1}' | tr -d '\n' ''', returnStdout: true)
            println newBackendPod
            //Run migrations command on Backend pod
            sh("kubectl exec -it -n production ${newBackendPod} python manage.py migrate")
            }

        //Re-create REDIS to flush cache
        stage 'Re-create REDIS container'
        def redispod = sh(script: '''kubectl get pods --selector=app=redis -n production | grep redis | awk '{print $1}' | tr -d '\n' ''', returnStdout: true)
        println redispod
        sh("kubectl delete pods ${redispod} -n production")
        break

    }
  } catch (e) {
    // If there was an exception thrown, the build failed
    currentBuild.result = "FAILED"
    throw e
  } finally {
    // Success or failure, always send notifications
    notifyBuild(currentBuild.result)
  }
}

def notifyBuild(String buildStatus = 'STARTED') {
  // build status of null means successful
  buildStatus =  buildStatus ?: 'SUCCESSFUL'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.GIT_COMMIT}]'"
  def summary = "${subject} (${env.BUILD_URL})"
  def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.GIT_COMMIT}]':</p>
    <p>Check console output at "<a href="${env.BUILD_URL}">${env.JOB_NAME} [${env.GIT_COMMIT}]</a>"</p>"""

  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  } else if (buildStatus == 'SUCCESSFUL') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else if (buildStatus == 'New LATEST Backend image available!') {
    color = 'VIOLET'
    colorCode = '#8A2BE2'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  // Send notifications
  slackSend (color: colorCode, message: summary)
}
