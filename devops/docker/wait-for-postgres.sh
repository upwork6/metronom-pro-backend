#!/bin/bash

set -e
cmd="$@"

function postgres_ready(){
python << END
import sys
import psycopg2
from urllib.parse import urlparse

result = urlparse("$DATABASE_URL")
username = result.username
password = result.password
database = result.path[1:]
hostname = result.hostname

# print(f"FOR DEBUGGING ONLY: Trying {username}, {password}, {database}, {hostname}")

try:
    conn = psycopg2.connect(
        database=database,
        user=username,
        password=password,
        host=hostname
    )

except psycopg2.OperationalError as err:
    print(f"Postgres database {database} is unavailable - sleeping")
    print(f"(Error: {err})")
    sys.exit(-1)
sys.exit(0)
END
}

if [ -z "$DATABASE_URL" ]
then
      echo "$DATABASE_URL is empty - it needs to be set."
      exit 1
else
    >&2 echo "Connecting to $DATABASE_URL ..."
fi

until postgres_ready; do
  sleep 1
done

>&2 echo "Postgres is up - continuing..."
exec $cmd