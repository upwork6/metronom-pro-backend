#!/bin/bash

# Exit immediately if a pipeline returns a non-zero status (= set- e)
set -e
# If set, the return value of a pipeline is the value of the last (rightmost) command to exit with a non-zero status
# , or zero if all commands in the pipeline exit successfully. This option is disabled by default.
# set -o pipefail
# Treat unset variables and parameters other than the special parameters ‘@’ or ‘*’ as an error when performing
# parameter expansion. An error message will be written to the standard error, and a non-interactive shell will exit.
# set -o nounset

cmd="$@"

if [ -z ${ENV+x} ]; then
    echo "ENV is empty - You need to be set either ENV=local, ENV=ci or ENV=production."
    exit 1
fi

if [ "$ENV" = "local" ]; then
  echo "--- (1/4) Migrations ----"
  ./manage.py migrate --noinput
  echo "--- (2/4) Create BASE_SUPERUSERS ----"
  ./manage.py create_base_users
  echo "--- (3/4) Update settings ----"
  ./manage.py update_settings
  echo "--- (4/4) Starting Development server ----"
  ./manage.py runserver_plus 0.0.0.0:8009
elif [ "$ENV" = "ci" ]; then
  echo "ENV=ci - Running Test..."
  py.test
elif [ "$ENV" = "frontend_development" ]; then
  echo "--- (1/4) Migrations ----"
  ./manage.py migrate --noinput
  echo "--- (2/4) Create BASE_SUPERUSERS ----"
  ./manage.py create_base_users
  echo "--- (3/4) Update settings ----"
  ./manage.py update_settings
  echo "--- (4/4) Run production server ----"
  ./manage.py runserver 0.0.0.0:8009
elif [ "$ENV" = "production" ]; then
  echo "--- (1/4) Migrations ----"
  ./manage.py migrate --noinput
  echo "--- (2/4) Update settings ----"
  ./manage.py update_settings
  echo "--- (3/4) Collect static files ----"
  ./manage.py collectstatic --noinput
  echo "--- (4/4) Run production server ----"
  gunicorn config.wsgi -w 4 -b 0.0.0.0:8009 --chdir=/app
else
    echo "ENV=$ENV is not valid. You need to be set either ENV=local, ENV=ci or ENV=production."
    exit 1
fi
