#!/bin/bash
POSTGRES_SERVICE=postgres

cat /path/to/db_dump.sql | docker exec -i  $POSTGRES_SERVICE psql -U otcfdanawlqyes -d metronom_legacy > log.txt 2>&1
docker exec -ti postgres psql -U otcfdanawlqyes -d metronom_legacy
DELETE FROM django_migrations;

# to dump the data from the processed one
# docker exec postgres pg_dump -U metronom -d metronom > backup.pg_dump