#!/bin/bash

FRONTEND_SERVICE=frontend
BACKEND_SERVICE=backend
POSTGRES_SERVICE=postgres
REDIS=redis

echo "************************************************ Basics ************************************************" > debug.txt
echo " ************************** Versions **************************" >> debug.txt
uname -a >> debug.txt
python -V >> debug.txt
docker -v >> debug.txt
docker-compose -v >> debug.txt
git --version >> debug.txt
echo " ************************** Ping **************************" >> debug.txt
ping metronom-dev.ch -c 3 >> debug.txt
echo " ************************** Memory **************************" >> debug.txt
top -l 1 -s 0 | awk ' /Processes/ || /PhysMem/ || /Load Avg/{print}' >> debug.txt
echo "************************************************ Pip ************************************************" >> debug.txt
pip -V >> debug.txt
pip freeze >> debug.txt
echo "************************************************ Git ************************************************" >> debug.txt
echo " ************************** Status **************************" >> debug.txt
git status >> debug.txt
echo " ************************** Log **************************" >> debug.txt
git log -10 >> debug.txt
echo " ************************** Reflog **************************" >> debug.txt
git reflog -10 >> debug.txt
echo " ************************** Config **************************" >> debug.txt
git config -l >> debug.txt
echo " ************************** Remotes **************************" >> debug.txt
git remote -v >> debug.txt
echo "************************************************ Folder ************************************************" >> debug.txt
ls -lsa >> debug.txt
echo "************************************************ .env ************************************************" >> debug.txt
cat .env >> debug.txt
echo "************************************************ PORTS ************************************************" >> debug.txt
echo " ************************** 5432 **************************" >> debug.txt
sudo lsof -i :5432 >> debug.txt || true
echo " ************************** 5433 **************************" >> debug.txt
sudo lsof -i :5433 >> debug.txt || true
echo " ************************** 6379 **************************" >> debug.txt
sudo lsof -i :6379 >> debug.txt || true
echo " ************************** 6380 **************************" >> debug.txt
sudo lsof -i :6380 >> debug.txt || true
echo " ************************** 3005 **************************" >> debug.txt
sudo lsof -i :3005 >> debug.txt || true
echo " ************************** 8000 **************************" >> debug.txt
sudo lsof -i :8000 >> debug.txt || true
echo " ************************** 8009 **************************" >> debug.txt
sudo lsof -i :8009 >> debug.txt || true
echo "************************ Docker *******************************" >> debug.txt
echo " ******************* Images **************************" >> debug.txt
docker images >> debug.txt
echo " ************************** PS **************************" >> debug.txt
docker ps >> debug.txt
echo "************************************************ Docker inspect ************************************************" >> debug.txt
docker inspect $(docker ps -a -q) >> debug.txt

echo "************************************************ Docker Logs ************************************************" >> debug.txt

for i in `docker ps -a -q`;
    do
        echo " ******************* $i *******************" >> debug.txt
        docker logs $i >> debug.txt || true
    done

pbcopy < debug.txt || true

echo "************************************************************************************************" >> debug.txt
