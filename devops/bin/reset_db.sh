#!/bin/bash

POSTGRES_SERVICE=postgres

echo ""

if [ ! "$(docker ps -q -f name=$POSTGRES_SERVICE)" ]; then
  echo "!!!! YOU NEED TO HAVE THE DOCKER CONTAINERS RUNNING !!!! Please run the command 'make local' first"
  exit 1
fi

read -p "This wil DROP + CREATE your metronom database with *ALL* DATA. DO YOU WANT TO CONTINUE? (y/n)?" choice
case "$choice" in
  y|Y )

    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "REVOKE CONNECT ON DATABASE test_metronom FROM public;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'test_metronom';"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP DATABASE test_metronom;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "REVOKE CONNECT ON DATABASE test_metronom_gw0 FROM public;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'test_metronom_gw0';"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP DATABASE test_metronom_gw0;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "REVOKE CONNECT ON DATABASE test_metronom_gw1 FROM public;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'test_metronom_gw1';"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP DATABASE test_metronom_gw1;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "REVOKE CONNECT ON DATABASE test_metronom_gw1 FROM public;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'test_metronom_gw1';"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP DATABASE test_metronom_gw1;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "REVOKE CONNECT ON DATABASE test_metronom_gw2 FROM public;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'test_metronom_gw2';"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP DATABASE test_metronom_gw2;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "REVOKE CONNECT ON DATABASE test_metronom_gw3 FROM public;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'test_metronom_gw3';"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP DATABASE test_metronom_gw3;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "REVOKE CONNECT ON DATABASE test_metronom_gw4 FROM public;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'test_metronom_gw4';"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP DATABASE test_metronom_gw4;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "REVOKE CONNECT ON DATABASE test_metronom_gw5 FROM public;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'test_metronom_gw5';"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP DATABASE test_metronom_gw5;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "REVOKE CONNECT ON DATABASE test_metronom_gw6 FROM public;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'test_metronom_gw6';"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP DATABASE test_metronom_gw6;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "REVOKE CONNECT ON DATABASE test_metronom_gw7 FROM public;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'test_metronom_gw7';"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP DATABASE test_metronom_gw7;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "REVOKE CONNECT ON DATABASE metronom FROM public;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'metronom';"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP DATABASE metronom;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "REVOKE CONNECT ON DATABASE metronom_legacy FROM public;"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'metronom_legacy';"
    docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP DATABASE metronom_legacy;"
	docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "DROP USER metronom;" || exit 1
	docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "CREATE DATABASE metronom;" || exit 1
	docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "CREATE DATABASE metronom_legacy;" || exit 1
	docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "CREATE USER metronom WITH PASSWORD 'secret' CREATEDB;" || exit 1
	docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE metronom TO metronom;" || exit 1
	docker exec -ti $POSTGRES_SERVICE psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE metronom_legacy TO metronom;" || exit 1
	echo ""
  echo "Done! You can now use DATABASE_URL=postgresql://metronom:secret@postgres/metronom";;

  n|N ) echo "Okay. Nothing was deleted.";;
  * ) echo "invalid" || exit 1;;
esac
