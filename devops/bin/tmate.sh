#!/bin/bash

set -e

read -p "# " CHANNEL


SLACK_WEBHOOK_URL=https://hooks.slack.com/services/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
DEFAULT_EMOJI=:slack:
#CHANNEL="@jens"
AUTHOR="Tmate Bot"

tmate -S /tmp/tmate.sock new-session -d && tmate -S /tmp/tmate.sock wait tmate-ready
TMATE_URL=`tmate -S /tmp/tmate.sock display -p '#{tmate_ssh}'`


PAYLOAD='payload={"channel": "@'$CHANNEL'", "username": "'$AUTHOR'", "text": "`'$TMATE_URL'`", "icon_emoji": "'$DEFAULT_EMOJI'"}'

curl -sS -o /dev/null -X POST --data-urlencode  "$PAYLOAD" $SLACK_WEBHOOK_URL

tmate -S /tmp/tmate.sock attach

PAYLOAD='payload={"channel": "@'$CHANNEL'", "username": "'$AUTHOR'", "text": "`Tmate connection closed.`", "icon_emoji": "'$DEFAULT_EMOJI'"}'

curl -sS -o /dev/null -X POST --data-urlencode  "$PAYLOAD" $SLACK_WEBHOOK_URL
