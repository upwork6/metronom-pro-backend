#!/bin/bash
read -p "This wil delete *ALL* Docker data (containers, images, volumes). DO YOU WANT TO CONTINUE? (y/n)?" choice
case "$choice" in
  y|Y )
    docker kill $(docker ps -q)
    docker rm -f $(docker ps -a -q)
    docker rmi -f $(docker images -q)
    docker volume rm $(docker volume ls -qf dangling=true)
    docker system prune -f
    docker ps
    docker images
    docker volume ls;;
  n|N ) echo "no";;
  * ) echo "invalid";;
esac

