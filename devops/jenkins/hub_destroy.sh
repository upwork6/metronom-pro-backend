#!/usr/bin/env bash

set -e

# Default compose args
COMPOSE_ARGS="-f hub_base.yml -f production.yml"

# Stop the containers

docker-compose $COMPOSE_ARGS down --rmi=all
#docker-compose $COMPOSE_ARGS rm --force -v
#docker ps -a -q | xargs --no-run-if-empty docker stop
#docker ps -a -q | xargs --no-run-if-empty docker rm
#docker images -q -f | xargs --no-run-if-empty docker rmi