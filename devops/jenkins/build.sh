#!/usr/bin/env bash

set -e

# Default compose args
COMPOSE_ARGS="-f base.yml -f production.yml"

# In production we need to export some environment variables here, will be copied into Jenkins later
touch backend/.env
echo "JWT_SECRET=11" >> backend/.env
echo "SECRET_KEY=11" >> backend/.env
echo "ALLOWED_HOSTS=*" >> backend/.env
echo "REDIS_URL=redis://redis:6379" >> backend/.env
touch frontend/.env
echo "JWT_SECRET=11" >> frontend/.env
echo "SECRET_KEY=11" >> frontend/.env

# Build the system
docker-compose $COMPOSE_ARGS build --no-cache

