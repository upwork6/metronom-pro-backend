#!/usr/bin/env bash

set -e

# Default compose args
#COMPOSE_ARGS="-f base.yml -f production.yml"
COMPOSE_ARGS="-f ci-test.yml"
# Stop the containers
docker-compose $COMPOSE_ARGS down --rmi=all
