#!/usr/bin/env bash

set -e

# Default compose args
COMPOSE_ARGS="-f base.yml -f production.yml"

# Tagging
HASH=$(git rev-parse --short HEAD)
docker tag metronompro_backend metronom/backend:$HASH
docker tag metronompro_backend metronom/backend:latest
docker tag metronompro_frontend metronom/frontend:$HASH
docker tag metronompro_frontend metronom/frontend:latest

# Login to Docker and deploy

echo "Done."
