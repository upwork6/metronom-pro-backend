#!/usr/bin/env bash

set -e

# Default compose args
COMPOSE_ARGS="-f base.yml -f production.yml"

# Run tests
docker-compose $COMPOSE_ARGS up -d --build
docker-compose $COMPOSE_ARGS run --no-deps --rm -e ENV=ci backend
docker-compose $COMPOSE_ARGS run --no-deps --rm -e ENV=ci frontend

# TODO: Some minimal smoke tests with curl
# curl --fail http://localhost:8009/ht
# maybe with https://github.com/asm89/smoke.sh

