#!/usr/bin/env bash

set -e

# Default compose args
COMPOSE_ARGS="-f hub_base.yml -f production.yml"

# Run tests
docker-compose $COMPOSE_ARGS up -d --build

docker-compose $COMPOSE_ARGS run --no-deps --rm -e ENV=ci \
	-e "JWT_SECRET=11" \
	-e "SECRET_KEY=11" \
	-e "ALLOWED_HOSTS=*" \
	-e "REDIS_URL=redis://redis:6379" \
    backend

docker-compose $COMPOSE_ARGS run --no-deps --rm -e ENV=ci \
	-e "JWT_SECRET=11" \
	-e "SECRET_KEY=11" \
    frontend

# TODO: Some minimal smoke tests with curl
# curl --fail http://localhost:8009/ht
# maybe with https://github.com/asm89/smoke.sh

