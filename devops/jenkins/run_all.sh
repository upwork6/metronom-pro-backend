#!/usr/bin/env bash

set -e

./bin/jenkins/destroy.sh
./bin/jenkins/build.sh
./bin/jenkins/test.sh
./bin/jenkins/deploy.sh
./bin/jenkins/destroy.sh