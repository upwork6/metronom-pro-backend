
# URL Types / Webseitentypen
URL_TYPES = [
    {"name": "Webseite"},
    {"name": "Facebook"},
    {"name": "XING"},
    {"name": "LinkedIn"},
    {"name": "Twitter"},
    {"name": "Projekt-Seite"},
    {"name": "Trello"}
]

# Phone number Types / Telefonnummer-Typen
PHONENUMBER_TYPES = [
    {"name": "Mobile"},
    {"name": "Festnetz"},
    {"name": "Fax"},
    {"name": "Skype"},
]

# Banks / Banken
BANKS = [
    {"bank_name": "ING-DiBa", "bic": "INGDDEFFXXX", "country_code": "DE", "bank_code": "50010517"},
]

# Sectors / Branchen
SECTORS = [
    {"name": "Nichts passendes gefunden"},
    {"name": "Architektur / Ingenieurwesen"},
    {"name": "Industrie"},
    {"name": "Fotografie / Film / Ton"},
    {"name": "IT / Telekommunikation"},
    {"name": "Elektrotechnik / Elektronik / EDV"},
    {"name": "Finanzdienstleistung"},
    {"name": "\u00c4mter / Beh\u00f6rden"},
    {"name": "Grosshandel"},
    {"name": "Design / Grafik / Visualisierung"},
    {"name": "Detailhandel"},
    {"name": "Druck / Papier"},
    {"name": "Transport / Touristik / Verkehr"},
    {"name": "Bildung / Wissenschaft / Forschung"},
    {"name": "Medien / Presse / Verlage"},
    {"name": "Gesundheits-/Sozialwesen"},
    {"name": "Text / Konzept"},
    {"name": "Verb\u00e4nde / Vereinigungen / Institutionen"},
    {"name": "Korrektorat / Lektorat / \u00dcbersetzung"},
    {"name": "Consulting"},
    {"name": "Event / Messebau"},
    {"name": "Werbemittel"},
    {"name": "Sport / Spiel"},
    {"name": "Baugewerbe"},
    {"name": "Digital-Publishing / Web"},
    {"name": "Werbung / PR / Marketing / Marktforschung"},
    {"name": "Freizeit / Kultur / Unterhaltung"},
    {"name": "Gastronomie / Hotelgewerbe"},
    {"name": "Fahrzeuge"},
    {"name": "Handwerk"},
    {"name": "Immobilien"},
    {"name": "Kosmetik / K\u00f6rperpflege"},
    {"name": "Landwirtschaft / Energie"},
    {"name": "\u00d6kologie / Umwelttechnik / Recycling"},
    {"name": "Schmuck / Uhren"},
    {"name": "Textil"},
    {"name": "Versicherung"},
    {"name": "Stiftungen"},
    {"name": "Nahrungsmittel"},
]

# Promotions / Werbemaßnahmen
PROMOTIONS = [
    {"name": "Newsletter"},
    {"name": "Neujahr - 1 Flasche Wein"},
    {"name": "Neujahr - 1 Kiste Wein"},
    {"name": "Neujahr - nur Karte"}
]

# Thank you notes
THANKYOU_NOTES = [
    {"name": ""},
    {"name": "Herzlichen Dank für den sehr geschätzten Auftrag."},
]

# Thank you notes
TAX_NOTES = [
    {"name": "Der Betrag versteht sich inkl. MwSt."},
    {"name": "Der Betrag versteht sich exkl. MwSt. (Art. 8 Abs. 1 MWSTG)"},
    {"name": "Leistungen von der MwSt. ausgenommen, gem. Artikel 18, Ziffer 1"},
    {"name": "Steuerschuldner ist der Rechnungsempfänger § 13b UST, Reverse Charge Verfahren"},
]

# Terms
TERMS_PARAGRAPHS = [
    {
        "terms_paragraph__name": "Gerichsstand",
        "terms_paragraph__text": "Der Gerichsstand ist Winterthur",
        'terms_collection__name': "Standard",
    },
]

# Agency bank Accounts
AGENCY_BANKACCOUNTS = [
    {
        "bank_name": "Credit Suisse AG",
        "account_name": "Standard-Konto",
        "iban": "CH3908704016075473007",
        "bic": "AEKTCH22XXX",
    },
    {
        "bank_name": "Deutsche Postbank AG",
        "account_name": "Postbank",
        "iban": "DE12500105170648489890",
        "bic": "PBNKDEFF",
    },
]

# Steps for activities / Schritte für Aufgaben
STEPS = [
    {'name': "Beratung Senior"},
    {'name': "Aktenstudium / Recherche"},
    {'name': "Beratung"},
    {'name': "Präsentation Senior"},
    {'name': "Workshop Senior"},
    {'name': "Art Direction"},
    {'name': "Creative Direction"},
    {'name': "Design"},
    {'name': "Konzeptentwicklung"},
    {'name': "Präsentation"},
    {'name': "Projektleitung"},
    {'name': "Shooting"},
    {'name': "Text / Idee"},
    {'name': "Workshop"},
    {'name': "Budgetierung / Procurement"},
    {'name': "Datenhandling"},
    {'name': "Erstellung PPT"},
    {'name': "Initialiserung / Vorbereitung"},
    {'name': "IT / Support"},
    {'name': "Nachbearbeitung / Dokumentation"},
    {'name': "Projektorganisation"},
    {'name': "Qualitätssicherung"},
    {'name': "Realisation"},
    {'name': "Regie"},
    {'name': "Schulung / Instruktion"},
    {'name': "Terminplanung"},
    {'name': "Text / Content"},
    {'name': "Administration"},
    {'name': "Assistenz"},
    {'name': "Contentmanagement"},
    {'name': "Reisezeit"},
]

# Activities / Aufgaben
ACTIVITIES = [
    {
        "name": "Mediaplanung",
        "activity_group__name": "Organisation",
    },
    {
        "name": "Systembetreuung / IT-Support",
        "activity_group__name": "Organisation",
    },
    {
        "name": "Schulung",
        "activity_group__name": "Organisation",
    },
    {
        "name": "Zusatz- und Mehraufwand",
        "activity_group__name": "Organisation",
    },
    {
        "name": "Internas",
        "activity_group__name": "Organisation",
    },
    {
        "name": "Projektmanagement",
        "activity_group__name": "Organisation",
    },
    {
        "name": "Autokorrekturen",
        "activity_group__name": "Organisation",
    },
    {
        "name": "Kundensupport",
        "activity_group__name": "Organisation",
    },
    {
        "name": "Initialisierung",
        "activity_group__name": "Organisation",
    },
    {
        "name": "Beratung",
        "description": "Beratung, Monitoring und Coaching; Abstimmungsmeetings auf diversen Levels; Präsentationen; laufendes Sparring & Support; Steuerung des Gesamtprojektes, laufende Überwachung, Kommunikation mit Auftraggeber",
        "activity_group__name": "Beratung",
    },
    {
        "name": "Strategie",
        "description": "Konzeption und Verfassen [Strategie/Studie]; Erarbeitung und Definition [Studien]-Struktur, Zusammenstellung und Ausarbeitung Fragenkatalog, Auswertung und Reporting, Aufbereitung für Präsentation, Präsentation; Beauftragung von Umfragen",
        "activity_group__name": "Beratung",
    },
    {
        "name": "Markenanalyse und -strategie Summary",
        "description": "Dokumentation und Recherche vorhandener Daten und Unterlagen, Analysen und Überlegungen der Marke und Markenhierarchie [innerhalb der Firma/Institution und] im Markt; Strukturierung und Definition aller Marken Kombinationen/Versionen, Ausarbeiten der Markenhierarchie; Soll-Positionierung und Definition Kernwerte und Differenzierungsmerkmale (USP); Definition Markenidentität/Branding; Entwicklung Markenstrategie; Aufbereitung für Präsentation; Präsentation, Beratung",
        "activity_group__name": "Beratung",
    },
    {
        "name": "Executive Summary",
        "description": "Verfassen Executive Summary auf Grund [Meeting/Workshop/Lab-Session], Aufbereitung für Präsentation, Präsentation",
        "activity_group__name": "Beratung",
    },
    {
        "name": "Markenpositionierung",
        "description": "Ausarbeitung Branding-Szenarien; Grafische Umsetzung der favorisierten Markenwelt, Aufzeigen möglicher Varianten; Gestalterisches Durchdeklinieren aller möglicher Logo/Markenvarianten; Weiterentwicklung und Konkretisierung der gewählten Markenwelt; Aufbereitung für Präsentation; Präsentation",
        "activity_group__name": "Beratung",
    },
    {
        "name": "Naming",
        "description": "Brainstorming und Ideenfindung [Naming/Claim]-Vorschläge, in Zusammenarbeit mit Auftraggeber Varianten-Ranking erstellen; Konkurrenzanalyse der favorisierten [3] Varianten; Ausformulierung von Pros & Cons der geprüften Vorschläge, Präsentation und Diskussion mit Auftraggeber; Inhaltliche Überarbeitung nach Kunden-Feedbacks; Markenrechtliche Vorabklärungen; [Beauftragung Markenrechtliche Abklärung (zugezogener Markenrechtsexperte)] ",
        "activity_group__name": "Beratung",
    },
    {
        "name": "Workshop",
        "description": "Vorbereitung und Organisation Workshop; Workshop-Moderation; Bereitstellen der Workshop-Unterlagen; Workshop-Protokoll; Erstellen Executive Summary	",
        "activity_group__name": "Beratung",
    },
    {
        "name": "Marketingkonzept",
        "activity_group__name": "Konzept",
    },
    {
        "name": "Kommunikationskonzept",
        "activity_group__name": "Konzept",
    },
    {
        "name": "Kampagnenkonzept",
        "activity_group__name": "Konzept",
    },
    {
        "name": "Publishing-Konzept",
        "activity_group__name": "Konzept",
    },
    {
        "name": "Digital-Konzept",
        "activity_group__name": "Konzept",
    },
    {
        "name": "Designkonzept",
        "activity_group__name": "Design",
    },
    {
        "name": "Corporate Design",
        "activity_group__name": "Design",
    },
    {
        "name": "Styleguide",
        "activity_group__name": "Design",
    },
    {
        "name": "Detaildesign",
        "activity_group__name": "Design",
    },
    {
        "name": "Illustration",
        "activity_group__name": "Design",
    },
    {
        "name": "Informationsgrafik",
        "activity_group__name": "Design",
    },
    {
        "name": "Signaletikkonzept ",
        "activity_group__name": "Design",
    },
    {
        "name": "Layout und Bildintegration",
        "activity_group__name": "Realisation",
    },
    {
        "name": "Layout-Adaption ",
        "activity_group__name": "Realisation",
    },
    {
        "name": "Realisation Corporate Design",
        "activity_group__name": "Realisation",
    },
    {
        "name": "Briefschaften",
        "activity_group__name": "Realisation",
    },
    {
        "name": "Plandossier",
        "activity_group__name": "Realisation",
    },
    {
        "name": "PowerPoint-Präsentationen",
        "activity_group__name": "Realisation",
    },
    {
        "name": "Templating Drucksachen",
        "activity_group__name": "Realisation",
    },
    {
        "name": "Templating für Office-Applikationen",
        "activity_group__name": "Realisation",
    },
    {
        "name": "Realisation Signaletik",
        "activity_group__name": "Realisation",
    },
    {
        "name": "UX-/UI-Konzept",
        "activity_group__name": "UX/UI",
    },
    {
        "name": "UX Design",
        "activity_group__name": "UX/UI",
    },
    {
        "name": "UI Design",
        "activity_group__name": "UX/UI",
    },
    {
        "name": "Micro-Interactions",
        "activity_group__name": "UX/UI",
    },
    {
        "name": "Usabilitytest",
        "activity_group__name": "UX/UI",
    },
    {
        "name": "Coding / Web-Programmierung",
        "activity_group__name": "Digital",
    },
    {
        "name": "Browsertesting / Qualitätssicherung",
        "activity_group__name": "Digital",
    },
    {
        "name": "Content Management",
        "activity_group__name": "Digital",
    },
    {
        "name": "Management Online-Kampagne ",
        "activity_group__name": "Digital",
    },
    {
        "name": "On-Page Suchmaschinen-Optimierung",
        "activity_group__name": "Digital",
    },
    {
        "name": "Tracking / Monitoring",
        "activity_group__name": "Digital",
    },
    {
        "name": "Setup Hosting / CMS",
        "activity_group__name": "Digital",
    },
    {
        "name": "Maintenance",
        "activity_group__name": "Digital",
    },
    {
        "name": "Animation",
        "activity_group__name": "Digital",
    },
    {
        "name": "Blog",
        "activity_group__name": "Digital",
    },
    {
        "name": "Wohnungsfinder",
        "activity_group__name": "Digital",
    },
    {
        "name": "Shop",
        "activity_group__name": "Digital",
    },
    {
        "name": "Formular",
        "activity_group__name": "Digital",
    },
    {
        "name": "Wettbewerb",
        "activity_group__name": "Digital",
    },
    {
        "name": "Banner",
        "activity_group__name": "Digital",
    },
    {
        "name": "Inhalts- und Text-Konzept",
        "activity_group__name": "Text",
    },
    {
        "name": "Werbetexte",
        "activity_group__name": "Text",
    },
    {
        "name": "PR- und Corporate-Texte",
        "activity_group__name": "Text",
    },
    {
        "name": "Redaktion",
        "activity_group__name": "Text",
    },
    {
        "name": "Korrektorat",
        "activity_group__name": "Text",
    },
    {
        "name": "Lektorat",
        "activity_group__name": "Text",
    },
    {
        "name": "Bildkonzept",
        "activity_group__name": "Bild",
    },
    {
        "name": "Story-/Shootingboard (Bild)",
        "activity_group__name": "Bild",
    },
    {
        "name": "PPM (Bild)",
        "activity_group__name": "Bild",
    },
    {
        "name": "Bildproduktion / Shooting",
        "activity_group__name": "Bild",
    },
    {
        "name": "Bildcomposing / Key Visuals ",
        "activity_group__name": "Bild",
    },
    {
        "name": "Bildbearbeitung",
        "activity_group__name": "Bild",
    },
    {
        "name": "Bildauswahl",
        "activity_group__name": "Bild",
    },
    {
        "name": "Bildrecherche und -einkauf",
        "activity_group__name": "Bild",
    },
    {
        "name": "Story-/Shootingboard (Film)",
        "activity_group__name": "Film",
    },
    {
        "name": "PPM (Film)",
        "activity_group__name": "Film",
    },
    {
        "name": "Shooting/Dreh",
        "activity_group__name": "Film",
    },
    {
        "name": "Postproduction",
        "activity_group__name": "Film",
    },
]

# Articles / Artikel
ARTICLES = [
    {
        "name": "Konzept & Idee",
        "allocation__name": "Unsortiert",
        "article_group__name": "Konzeptarbeiten"
    }, {
        "name": "Onlinemarketing",
        "allocation__name": "Unsortiert",
        "article_group__name": "Konzeptarbeiten",
        },
    {
        "name": "Kommunikation & PR",
        "allocation__name": "Unsortiert",
        "article_group__name": "Konzeptarbeiten",
        },
    {
        "name": "Illustration",
        "allocation__name": "Unsortiert",
        "article_group__name": "Design",
    },
    {
        "name": "Informationsgrafiken",
        "allocation__name": "Unsortiert",
        "article_group__name": "Design",
    },
    {
        "name": "3-D Visualisierungen",
        "allocation__name": "Unsortiert",
        "article_group__name": "Design",
    },
    {
        "name": "Architekturvisualisierungen",
        "allocation__name": "Unsortiert",
        "article_group__name": "Design",
    },
    {
        "name": "UX-UI",
        "allocation__name": "Unsortiert",
        "article_group__name": "Design",
    },
    {
        "name": "Werbetexte",
        "allocation__name": "Unsortiert",
        "article_group__name": "Text",
    },
    {
        "name": "PR- und Corporatetexte",
        "allocation__name": "Unsortiert",
        "article_group__name": "Text",
    },
    {
        "name": "Redaktion / Editorialtext",
        "allocation__name": "Unsortiert",
        "article_group__name": "Text",
    },
    {
        "name": "Korrektorat / Lektorat",
        "allocation__name": "Unsortiert",
        "article_group__name": "Text",
    },
    {
        "name": "Übersetzungen",
        "allocation__name": "Unsortiert",
        "article_group__name": "Text",
    },
    {
        "name": "Programmierung",
        "allocation__name": "Unsortiert",
        "article_group__name": "Digital",
    },
    {
        "name": "Hosting / DNS / Zertifikate",
        "allocation__name": "Unsortiert",
        "article_group__name": "Digital",
    },
    {
        "name": "Web-Services",
        "allocation__name": "Unsortiert",
        "article_group__name": "Digital",
    },
    {
        "name": "Suchmaschinenotimierung",
        "allocation__name": "Unsortiert",
        "article_group__name": "Digital",
    },
    {
        "name": "Fotoproduction",
        "allocation__name": "Unsortiert",
        "article_group__name": "Fotografie",
    },
    {
        "name": "Fotoshooting",
        "allocation__name": "Unsortiert",
        "article_group__name": "Fotografie",
    },
    {
        "name": "Modelhonorare",
        "allocation__name": "Unsortiert",
        "article_group__name": "Fotografie",
    },
    {
        "name": "Location / Locationmiete",
        "allocation__name": "Unsortiert",
        "article_group__name": "Fotografie",
    },
    {
        "name": "Requisiten / Material",
        "allocation__name": "Unsortiert",
        "article_group__name": "Fotografie",
    },
    {
        "name": "Equipment / Equipmentmiete",
        "allocation__name": "Unsortiert",
        "article_group__name": "Fotografie",
    },
    {
        "name": "Reportagefotografie",
        "allocation__name": "Unsortiert",
        "article_group__name": "Fotografie",
    },
    {
        "name": "Shooting / Production",
        "allocation__name": "Unsortiert",
        "article_group__name": "Film / Audio",
    },
    {
        "name": "Postproduction",
        "allocation__name": "Unsortiert",
        "article_group__name": "Film / Audio",
    },
    {
        "name": "Animation",
        "allocation__name": "Unsortiert",
        "article_group__name": "Film / Audio",
    },
    {
        "name": "Präsentationsmaterial",
        "allocation__name": "Unsortiert",
        "article_group__name": "Produktionskosten",
    },
    {
        "name": "Proof A4",
        "allocation__name": "Unsortiert",
        "article_group__name": "Produktionskosten",
    },
    {
        "name": "Proof A3",
        "allocation__name": "Unsortiert",
        "article_group__name": "Produktionskosten",
    },
    {
        "name": "Proof A2",
        "allocation__name": "Unsortiert",
        "article_group__name": "Produktionskosten",
    },
    {
        "name": "Druck",
        "allocation__name": "Unsortiert",
        "article_group__name": "Produktionskosten",
    },
    {
        "name": "Beschriftung & Messebau",
        "allocation__name": "Unsortiert",
        "article_group__name": "Produktionskosten",
    },
    {
        "name": "Werbegeschenke",
        "allocation__name": "Unsortiert",
        "article_group__name": "Produktionskosten",
    },
    {
        "name": "Nutzungsrechte Bild",
        "allocation__name": "Unsortiert",
        "article_group__name": "Lizenzkosten",
    },
    {
        "name": "Nutzungsrechte Schriften",
        "allocation__name": "Unsortiert",
        "article_group__name": "Lizenzkosten",
    },
    {
        "name": "Nutzungsrechte Audio/Video",
        "allocation__name": "Unsortiert",
        "article_group__name": "Lizenzkosten",
    },
    {
        "name": "Nutzungsrechte Software",
        "allocation__name": "Unsortiert",
        "article_group__name": "Lizenzkosten",
    },
    {
        "name": "Schaltkosten Online",
        "allocation__name": "Unsortiert",
        "article_group__name": "Media",
    },
    {
        "name": "Schaltkosten Offline",
        "allocation__name": "Unsortiert",
        "article_group__name": "Media",
    },
    {
        "name": "Mediaplanung / Monitoring",
        "allocation__name": "Unsortiert",
        "article_group__name": "Media",
    },
    {
        "name": "Woodwing Redaktionssystem",
        "allocation__name": "Unsortiert",
        "article_group__name": "SaaS",
    },
    {
        "name": "Medienarchiv SixOMC",
        "allocation__name": "Unsortiert",
        "article_group__name": "SaaS",
    },
    {
        "name": "Medianarchiv Disk Space 100GB",
        "allocation__name": "Unsortiert",
        "article_group__name": "SaaS",
    },
    {
        "name": "iPad-Kiosk",
        "allocation__name": "Unsortiert",
        "article_group__name": "SaaS",
    },
    {
        "name": "Rechts- und Beratungsaufwand (extern)",
        "allocation__name": "Unsortiert",
        "article_group__name": "Legal",
    },
    {
        "name": "Markenrechtsabklärungen",
        "allocation__name": "Unsortiert",
        "article_group__name": "Legal",
    },
    {
        "name": "Präsentationsmaterial",
        "allocation__name": "Unsortiert",
        "article_group__name": "Spesen",
    },
    {
        "name": "Reisespesen",
        "allocation__name": "Unsortiert",
        "article_group__name": "Spesen",
    },
    {
        "name": "Verpflegungsspesen",
        "allocation__name": "Unsortiert",
        "article_group__name": "Spesen",
    },
    {
        "name": "Übernachtungsspesen	",
        "allocation__name": "Unsortiert",
        "article_group__name": "Spesen",
    },
    {
        "name": "Autospesen",
        "allocation__name": "Unsortiert",
        "article_group__name": "Spesen",
    },
    {
        "name": "Porti",
        "allocation__name": "Unsortiert",
        "article_group__name": "Spesen",
    },
    {
        "name": "Kurierkosten",
        "allocation__name": "Unsortiert",
        "article_group__name": "Spesen",
    }
]

# Users / Mitarbeiter (generated)
USERS = 10

# Persons / Personen
PERSONS = 40

# Companies / Firmen
COMPANIES = 20

# Employments / Anstellungen
EMPLOYMENTS = 30

# Projects & campaigns / Projekte & Kampagnen
PROJECTS = 8
CAMPAIGNS = 4

# Allocations / Kontierungen
ALLOCATIONS = [
    {
        "public_id": "4400",
        "name": "Aufwand für Drittleistungen",
        'category__name': "Direkter Aufwand",
    },
    {
        "public_id": "4410",
        "name": "Produktionskosten",
        'category__name': "Direkter Aufwand",
    },
    {
        "public_id": "4420",
        "name": "Kreativleistungen",
        'category__name': "Direkter Aufwand",
    },
    {
        "public_id": "4425",
        "name": "Übersetzungen / Korrektorat",
        'category__name': "Direkter Aufwand",
    },
    {
        "public_id": "4430",
        "name": "Nutzungsrechte",
        'category__name': "Direkter Aufwand",
    },
    {
        "public_id": "4440",
        "name": "Programmierung",
        'category__name': "Direkter Aufwand",
    },
    {
        "public_id": "4450",
        "name": "Hostingkosten",
        'category__name': "Direkter Aufwand",
    },
    {
        "public_id": "4460",
        "name": "Media/Schaltkosten",
        'category__name': "Direkter Aufwand",
    },
    {
        "public_id": "5000",
        "name": "Löhne Produktion",
        'category__name': "Personalaufwand Produktion",
    },
    {
        "public_id": "5002",
        "name": "Prämienfreie Löhne",
        'category__name': "Personalaufwand Produktion",
    },
    {
        "public_id": "5005",
        "name": "Leistungen von Sozialversicherungen",
        'category__name': "Personalaufwand Produktion",
    },
    {
        "public_id": "5070",
        "name": "AHV, IV, EO, ALV, FAK",
        'category__name': "Personalaufwand Produktion",
    },
    {
        "public_id": "5072",
        "name": "Berufliche Vorsorge",
        'category__name': "Personalaufwand Produktion",
    },
    {
        "public_id": "5073",
        "name": "Unfallversicherung",
        'category__name': "Personalaufwand Produktion",
    },
    {
        "public_id": "5074",
        "name": "Krankentaggeldversicherung",
        'category__name': "Personalaufwand Produktion",
    },
    {
        "public_id": "5081",
        "name": "Aus- und Weiterbildung",
        'category__name': "Personalaufwand Produktion",
    },
    {
        "public_id": "5085",
        "name": "Reisespesen Job bezogen",
        'category__name': "Personalaufwand Produktion",
    },
    {
        "public_id": "5086",
        "name": "Verpflegungsspesen",
        'category__name': "Personalaufwand Produktion",
    },
    {
        "public_id": "5087",
        "name": "Übernachtungsspesen",
        'category__name': "Personalaufwand Produktion",
    },
    {
        "public_id": "5089",
        "name": "Sonstiger Personalaufwand ",
        'category__name': "Personalaufwand Produktion",
    },
    {
        "public_id": "5600",
        "name": "Löhne Verwaltung",
        'category__name': "Personalaufwand Verwaltung (wird ab 2018 aufgehoben)",
    },
    {
        "public_id": "5602",
        "name": "Prämienfreie Löhne",
        'category__name': "Personalaufwand Verwaltung (wird ab 2018 aufgehoben)",
    },
    {
        "public_id": "5605",
        "name": "Leistungen von Sozialversicherungen",
        'category__name': "Personalaufwand Verwaltung (wird ab 2018 aufgehoben)",
    },
    {
        "public_id": "5670",
        "name": "AHV, IV, EO, ALV, FAK",
        'category__name': "Personalaufwand Verwaltung (wird ab 2018 aufgehoben)",
    },
    {
        "public_id": "5672",
        "name": "Berufliche Vorsorge",
        'category__name': "Personalaufwand Verwaltung (wird ab 2018 aufgehoben)",
    },
    {
        "public_id": "5673",
        "name": "Unfallversicherung",
        'category__name': "Personalaufwand Verwaltung (wird ab 2018 aufgehoben)",
    },
    {
        "public_id": "5674",
        "name": "Krankentaggeldversicherung",
        'category__name': "Personalaufwand Verwaltung (wird ab 2018 aufgehoben)",
    },
    {
        "public_id": "5681",
        "name": "Aus- und Weiterbildung",
        'category__name': "Personalaufwand Verwaltung (wird ab 2018 aufgehoben)",
    },
    {
        "public_id": "5685",
        "name": "Reisespesen Job bezogen",
        'category__name': "Personalaufwand Verwaltung (wird ab 2018 aufgehoben)",
    },
    {
        "public_id": "5686",
        "name": "Verpflegungsspesen",
        'category__name': "Personalaufwand Verwaltung (wird ab 2018 aufgehoben)",
    },
    {
        "public_id": "5687",
        "name": "Übernachtungsspesen",
        'category__name': "Personalaufwand Verwaltung (wird ab 2018 aufgehoben)",
    },
    {
        "public_id": "5689",
        "name": "Sonstiger Personalaufwand",
        'category__name': "Personalaufwand Verwaltung (wird ab 2018 aufgehoben)",
    },
    {
        "public_id": "6000",
        "name": "Mietzins Winterthur",
        'category__name': "Raumaufwand",
    },
    {
        "public_id": "6001",
        "name": "Mietzins Zürich",
        'category__name': "Raumaufwand",
    },
    {
        "public_id": "6040",
        "name": "Reinigung",
        'category__name': "Raumaufwand",
    },
    {
        "public_id": "6100",
        "name": "URE Mobiliar und Einrichtungen",
        'category__name': "Unterhalt, Reparatur, Ersatz",
    },
    {
        "public_id": "6101",
        "name": "URE Büromaschinen",
        'category__name': "Unterhalt, Reparatur, Ersatz",
    },
    {
        "public_id": "6102",
        "name": "URE EDV-Hardware",
        'category__name': "Unterhalt, Reparatur, Ersatz",
    },
    {
        "public_id": "6103",
        "name": "URE EDV-Software",
        'category__name': "Unterhalt, Reparatur, Ersatz",
    },
    {
        "public_id": "6104",
        "name": "URE EDV-Verbrauchsmaterial",
        'category__name': "Unterhalt, Reparatur, Ersatz",
    },
    {
        "public_id": "6105",
        "name": "Wartungsverträge EDV-Hardware",
        'category__name': "Unterhalt, Reparatur, Ersatz",
    },
    {
        "public_id": "6106",
        "name": "Wartungsverträge EDV-Software",
        'category__name': "Unterhalt, Reparatur, Ersatz",
    },
    {
        "public_id": "6107",
        "name": "Web-Services",
        'category__name': "Unterhalt, Reparatur, Ersatz",
    },
    {
        "public_id": "6160",
        "name": "Maschinen und Apparate im Leasing",
        'category__name': "Unterhalt, Reparatur, Ersatz",
    },
    {
        "public_id": "6201",
        "name": "Reparaturen, Service, Reinigung PW",
        'category__name': "Fahrzeugaufwand",
    },
    {
        "public_id": "6211",
        "name": "Benzin PW",
        'category__name': "Fahrzeugaufwand",
    },
    {
        "public_id": "6221",
        "name": "Versicherung PW",
        'category__name': "Fahrzeugaufwand",
    },
    {
        "public_id": "6231",
        "name": "Verkehrsabgaben PW",
        'category__name': "Fahrzeugaufwand",
    },
    {
        "public_id": "6261",
        "name": "Leasing PW",
        'category__name': "Fahrzeugaufwand",
    },
    {
        "public_id": "6270",
        "name": "Privatanteil Fahrzeugaufwand",
        'category__name': "Fahrzeugaufwand",
    },
    {
        "public_id": "6300",
        "name": "Sachversicherungen",
        'category__name': "Sachversicherungen/Abgaben/Gebühren",
    },
    {
        "public_id": "6310",
        "name": "Betriebshaftpflichtversicherungen",
        'category__name': "Sachversicherungen/Abgaben/Gebühren",
    },
    {
        "public_id": "6400",
        "name": "Elektrizität",
        'category__name': "Energie- und Entsorgungsaufwand",
    },
    {
        "public_id": "6460",
        "name": "Kehrichtabfuhr",
        'category__name': "Energie- und Entsorgungsaufwand",
    },
    {
        "public_id": "6500",
        "name": "Büromaterial",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6501",
        "name": "Drucksachen",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6502",
        "name": "Fotokopien",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6503",
        "name": "Fachliteratur",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6510",
        "name": "Telefon/Mobile",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6512",
        "name": "Internet/Datentransfer",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6513",
        "name": "Porti",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6514",
        "name": "Kurierkosten",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6520",
        "name": "Beiträge, Spenden, Vergabungen",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6530",
        "name": "Buchhaltungsaufwand",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6532",
        "name": "Rechts- und Beratungsaufwand",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6542",
        "name": "Revision",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6550",
        "name": "VR-Honorare",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6590",
        "name": "Kassa-Differenzen",
        'category__name': "Verwaltungs- und Informatikaufwand",
    },
    {
        "public_id": "6600",
        "name": "Werbeinserate, elektronische Medien",
        'category__name': "Werbeaufwand",
    },
    {
        "public_id": "6640",
        "name": "Reisespesen Akquisition",
        'category__name': "Werbeaufwand",
    },
    {
        "public_id": "6643",
        "name": "Kundenbetreuung",
        'category__name': "Werbeaufwand",
    },
    {
        "public_id": "6646",
        "name": "Kundengeschenke",
        'category__name': "Werbeaufwand",
    },
    {
        "public_id": "6649",
        "name": "Sonstiger Werbeaufwand",
        'category__name': "Werbeaufwand",
    },
]

WORKING_HOURS = [
    {
        'weekday': 1,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Volle Woche',
    },
    {
        'weekday': 1,
        'from_hour': '13:00',
        'to_hour': '17:00',
        'flex_work__given_name': 'Volle Woche',
    },
    {
        'weekday': 2,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Volle Woche',
    },
    {
        'weekday': 2,
        'from_hour': '13:00',
        'to_hour': '17:00',
        'flex_work__given_name': 'Volle Woche',
    },
    {
        'weekday': 3,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Volle Woche',
    },
    {
        'weekday': 3,
        'from_hour': '13:00',
        'to_hour': '17:00',
        'flex_work__given_name': 'Volle Woche',
    },
    {
        'weekday': 4,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Volle Woche',
    },
    {
        'weekday': 4,
        'from_hour': '13:00',
        'to_hour': '17:00',
        'flex_work__given_name': 'Volle Woche',
    },
    {
        'weekday': 5,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Volle Woche',
    },
    {
        'weekday': 5,
        'from_hour': '13:00',
        'to_hour': '17:00',
        'flex_work__given_name': 'Volle Woche',
    },
    {
        'weekday': 1,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Nur morgens',
    },
    {
        'weekday': 2,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Nur morgens',
    },
    {
        'weekday': 3,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Nur morgens',
    },
    {
        'weekday': 4,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Nur morgens',
    },
    {
        'weekday': 5,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Nur morgens',
    },
    {
        'weekday': 3,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Mo+Di frei',
    },
    {
        'weekday': 3,
        'from_hour': '13:00',
        'to_hour': '17:00',
        'flex_work__given_name': 'Mo+Di frei',
    },
    {
        'weekday': 4,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Mo+Di frei',
    },
    {
        'weekday': 4,
        'from_hour': '13:00',
        'to_hour': '17:00',
        'flex_work__given_name': 'Mo+Di frei',
    },
    {
        'weekday': 5,
        'from_hour': '08:00',
        'to_hour': '12:00',
        'flex_work__given_name': 'Mo+Di frei',
    },
]

CLOSING_HOURS = [
    {
        'holiday__name': 'Tag der Arbeit',
        'holiday__date': '2018-05-01',
        'holiday__is_all_day': True
    },
    {
        'holiday__name': 'Auffahrt',
        'holiday__date': '2018-05-10',
        'holiday__is_all_day': True
    },
    {
        'holiday__name': 'Pfingstmontag',
        'holiday__date': '2018-05-21',
        'holiday__is_all_day': True
    },
    {
        'holiday__name': 'Nationalfeiertag',
        'holiday__date': '2018-08-01',
        'holiday__is_all_day': True
    },
    {
        'from_hour': '13:00',
        'to_hour': '18:00',
        'holiday__name': 'Heiligabend',
        'holiday__date': '2018-12-24',
        'holiday__is_all_day': False
    },
    {
        'holiday__name': '1.Weihnachtstag',
        'holiday__date': '2018-12-25',
        'holiday__is_all_day': True
    },
    {
        'holiday__name': 'Stephanstag',
        'holiday__date': '2018-12-26',
        'holiday__is_all_day': True
    },
]

# TODO: Phone numbers + Emails
# TODO: Cleanup of all Factories, used for tests
# TODO: Can we use more of https://faker.readthedocs.io/en/master/locales/de_DE.html?
#
