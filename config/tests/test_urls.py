import pytest
from django.test import TestCase, override_settings
from django.urls import NoReverseMatch, Resolver404, resolve, reverse


@pytest.mark.skip(reason="Doesn't work locally. Read the comment below.")
@override_settings(STAGE='production')
class TestMainURLsProduction(TestCase):
    """ Swagger API Docs will not be released to the public for now """

    def test_api_docs_reverse_not_existing(self):
        with self.assertRaises(NoReverseMatch):
            reverse('api_docs')

    def test_api_docs_can_not_be_resolved(self):
        with self.assertRaises(Resolver404):
            resolve('/docs/')


@pytest.mark.skip(reason="This works locally but not on the CI server - Reverse for 'api_docs' not found. "
                         "'api_docs' is not a valid view function or pattern name.")
@override_settings(STAGE='local')
class TestMainURLsLocal(TestCase):
    """ Swagger API Docs should be shown locally """

    def test_api_docs_reverse_should_work(self):
        assert reverse('api_docs') == '/docs/'

    def test_api_docs_can_be_resolved(self):
        assert resolve('/docs/').view_name == 'api_docs'
