import subprocess

from django.http import HttpResponse
from django.views.generic.base import TemplateView


class HomeView(TemplateView):

    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = TemplateView.get_context_data(self, **kwargs)

        return context


def migration_ready(request):
    """ Checks, if migrations are ready"""

    output = int(subprocess.getoutput("python manage.py showmigrations | grep '\[ \]' | wc -l | tr -d ' '"))

    if int(output) == 0:
        return HttpResponse(status=201)
    else:
        return HttpResponse(status=400)
