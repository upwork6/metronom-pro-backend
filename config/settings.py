import datetime
import os
import sys
import warnings
from decimal import Decimal

import environ
from corsheaders.defaults import default_headers
from django.utils.translation import ugettext_lazy as _

from apps.metronom_commons.data import CURRENCY, NUMBER_FORMAT

########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                   DJANGO SETTINGS                                                                    #
#                                                                                                                      #
#   For more information on this file, see                                                                             #
#   https://docs.djangoproject.com/en/1.11/topics/settings/                                                            #
#                                                                                                                      #
#   For the full list of settings and their values, see                                                                #
#   https://docs.djangoproject.com/en/1.11/ref/settings/                                                               #
#                                                                                                                      #
#                                                                                                                      #
#                   1 Django basics                                                                                    #
#                   2 Installed Apps & Middleware                                                                      #
#                   3 Databases & Caching                                                                              #
#                   4 Static files                                                                                     #
#                   5 Logging                                                                                          #
#                   6 Celery                                                                                           #
#                   7 API & Authentication                                                                             #
#                   8 Metronom                                                                                         #
#                   9 Others                                                                                           #
#                   10 Testing                                                                                         #
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################

# We are using django-environ to read from .env files

env = environ.Env()

with warnings.catch_warnings():
    try:
        warnings.simplefilter('error', Warning)
        environ.Env.read_env('.env')
    except Warning:
        print("**** No .env found - this can be ignored on production ****")

ENV = STAGE = env.str('ENV')

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Put in dependency variables, which are used in some of the settings files
DRF_VIEW_CACHE_ALIAS = 'DRF-default'
SITE_ID = 1

METRONOM_INFO_SETTINGS = {
    'GIT_COMMIT_HASH': env.str('GIT_COMMIT_HASH', default=None),
    'DEPLOYMENT_DATE': env.str('DEPLOYMENT_DATE', default=None)
}

########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                             Django basics                                                            #
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################

root = environ.Path(__file__) - 2

# Avoid the use of "python manage.py test", we are using py.test for testing
if sys.argv[1:2] == ['test']:
    print("TESTING is only supported with py.test!")
    sys.exit(-1)

# Fail hard, every environment needs to set the stage
ENV = STAGE = env.str('ENV')

# To make things easy for new developers, we are starting with a SECRET_KEY - we are checking this on production
SECRET_KEY = env('SECRET_KEY', default='notsafeforproduction')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool('DEBUG', False)

if ENV == 'production' and DEBUG is True:
    print("**** CAUTON: You are running in production with DEBUG=True ****")

# Should have '*' for local, the site URL for production
ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', [])

API_LOGGING = env.bool('API_LOGGING', True)

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [str(root.path('templates'))],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.tz',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'apps.metronom_commons.context_processors.admin_settings'
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

# Internationalization (https://docs.djangoproject.com/en/1.11/topics/i18n/)
LANGUAGE_CODE = 'en-us'
LANGUAGES = [
    ('de', _('German')),
    ('en', _('English')),
]

LOCALE_PATHS = (str(root.path('locale')),)

TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
        'OPTIONS': {
            'max_similarity': 0.7,
            'user_attributes': ('username', 'first_name', 'last_name', 'email')
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 8,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
        # optional path for password black list (default path: django/contrib/auth/common-passwords.txt.gz)
        'OPTIONS': {
            # 'password_list_path': '/path/to/passwordBlackList.txt.gz',
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                                 Installed apps
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.postgres',

    # API related
    'safedelete',
    'rest_framework',
    'rest_framework_swagger',
    'rest_framework.authtoken',
    'drf_yasg',
    'dry_rest_permissions',

    'corsheaders',
    'health_check',  # required
    'health_check.db',
    'health_check.cache',
    'rest_auth',
    'allauth',
    'allauth.account',
    'rest_auth.registration',

    # Other apps
    'djmoney',
    'actstream',
    'phonenumber_field',
    'django_countries',
    'djmoney_rates',
    # 'cachalot',

    # Our apps
    'apps.accounting.apps.AccountingConfig',
    'apps.activities.apps.ActivitiesConfig',
    'apps.addresses.apps.AddressesConfig',
    'apps.articles.apps.ArticlesConfig',
    'apps.autocompletes',
    'apps.backoffice.apps.BackofficeConfig',
    'apps.bankaccounts.apps.BankAccountsConfig',
    'apps.cache_controller',
    'apps.comments.apps.CommentsConfig',
    'apps.companies.apps.CompanyConfig',
    'apps.contacts.apps.ContactsConfig',
    'apps.contracts.apps.ContractsConfig',
    'apps.emails.apps.EmailConfig',
    'apps.employments.apps.EmploymentsConfig',
    'apps.invoices.apps.InvoicesConfig',
    'apps.metronom_commons.apps.MetronomCommonsConfig',
    'apps.offers.apps.OffersConfig',
    'apps.persons.apps.PersonConfig',
    'apps.profile_images.apps.ProfileImagesConfig',
    'apps.phonenumbers.apps.PhonenumbersConfig',
    'apps.projects.apps.ProjectsConfig',
    'apps.urls.apps.URLsConfig',
    'apps.users.apps.UsersConfig',
    'apps.legacy',
    'apps.search.apps.SearchConfig',
    'apps.workinghours.apps.WorkingHoursConfig',
]

########################################################################################################################
#                                                 Middleware
#
########################################################################################################################

# We are separating MIDDLEWARE so that we can add Silk in the middle when developing locally

MIDDLEWARE_TOP = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
]

MIDDLEWARE_BOTTOM = [
    'django.middleware.locale.LocaleMiddleware',
    'apps.metronom_commons.middleware.cors.MetronomCorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

MIDDLEWARE = MIDDLEWARE_TOP + MIDDLEWARE_BOTTOM

if ENV == 'local':
    # Do not use potentially insecure and unnecessary apps in production
    INSTALLED_APPS += [
        'django_extensions',
        'silk'
    ]

    MIDDLEWARE = MIDDLEWARE_TOP + ['silk.middleware.SilkyMiddleware'] + MIDDLEWARE_BOTTOM

########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                                 Database                                                             #
#                                                                                                                      #
#                             https://docs.djangoproject.com/en/1.11/ref/settings/#databases                           #
#                                                                                                                      #
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################

DATABASES = {
    'default': env.db(),
}

DATABASES['default']['ATOMIC_REQUESTS'] = True

LEGACY_DATABASE_URL = env.db('LEGACY_DATABASE_URL', default="postgres://")

if LEGACY_DATABASE_URL['NAME'] is not '':
    DATABASES['legacy'] = LEGACY_DATABASE_URL
    DATABASES['legacy']['ATOMIC_REQUESTS'] = True
    print(f"--> Using Legacy database: {LEGACY_DATABASE_URL['NAME']}")

########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                                 Caching                                                              #
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################


REDIS_URL = env.str('REDIS_URL', default="redis://redis:6379")
CACHING = env.bool('CACHING', default=False)

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': '{}/0'.format(REDIS_URL),
        'TIMEOUT': env.int('CACHE_TIMEOUT', default=86400),
    },
    'autocompletes': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': '{}/1'.format(REDIS_URL),
        'TIMEOUT': env.int('CACHE_TIMEOUT', default=86400),
    },
    'cachalot': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': '{}/2'.format(REDIS_URL),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        },
        'TIMEOUT': env.int('CACHE_TIMEOUT', default=86400),
    },
    DRF_VIEW_CACHE_ALIAS: {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': '{}/3'.format(REDIS_URL),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        },
        'TIMEOUT': env.int('CACHE_TIMEOUT', default=86400),
    }
}

########################################################################################################################
#                                                 Django-Cachalot                                                      #
########################################################################################################################

# Cachalot is not installed for now!!!!

CACHALOT_ENABLED = False
CACHALOT_CACHE = "default"

########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                       Static files (CSS, JavaScript, Images)                                         #
#                                                                                                                      #
#                             https://docs.djangoproject.com/en/1.11/howto/static-files/                               #
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################

STATIC_URL = '/static/'

if ENV != 'local':
    STATIC_ROOT = str(root.path('staticfiles'))

    STATIC_URL = env.str('STATIC_URL', default='/static/')

    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    )

    STATICFILES_DIRS = (
        str(root.path('static')),
    )

    MIDDLEWARE = ['whitenoise.middleware.WhiteNoiseMiddleware', ] + MIDDLEWARE
    STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
    # STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'

########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                                   Logging                                                            #
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################

if ENV == 'local' or API_LOGGING is True:

    REQUEST_ID_RESPONSE_HEADER = "HTTP_X_RESPONSE_ID"

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'verbose': {
                'format': "%(asctime)s %(levelname)s - %(filename)s:%(lineno)s %(funcName)s() - %(message)s",
                'datefmt': "%d/%b/%Y %H:%M:%S"
            },
            'simple': {
                'format': '%(asctime)s %(levelname)s - %(message)s'
            },
            'time': {
                'format': '[%(request_id)s] %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'verbose'
            },
            'simple_console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'simple'
            },
        },
        'loggers': {
            'django': {
                'handlers': ['simple_console'],
                'propagate': True,
                'level': 'WARNING',
            },
            'django.db': {
                'handlers': ['simple_console'],
                'propagate': True,
                'level': 'WARNING',
            },
            'backend': {
                'handlers': ['console'],
                'level': 'DEBUG',
                "formatter": "coloredlogs",
                'propagate': False,
            },
            'main': {
                'handlers': ['console'],
                'level': 'INFO',
            },
        }
    }

SENTRY_DSN = env.str('SENTRY_DSN', default=None)

if SENTRY_DSN is not None:
    INSTALLED_APPS += (
        'raven.contrib.django.raven_compat',
    )

    MIDDLEWARE = [
        # We recommend putting this as high in the chain as possible
        'raven.contrib.django.raven_compat.middleware.SentryResponseErrorIdMiddleware',
    ] + MIDDLEWARE

    RAVEN_CONFIG = {
        'dsn': SENTRY_DSN,
        # 'CELERY_LOGLEVEL': logging.INFO
    }

    if METRONOM_INFO_SETTINGS and 'GIT_COMMIT_HASH' in METRONOM_INFO_SETTINGS:
        RAVEN_CONFIG['release'] = METRONOM_INFO_SETTINGS['GIT_COMMIT_HASH']

    if not API_LOGGING is True:

        LOGGING = {
            'version': 1,
            'disable_existing_loggers': True,
            'root': {
                'level': 'WARNING',
                'handlers': ['sentry'],
            },
            'formatters': {
                'verbose': {
                    'format': '%(levelname)s %(asctime)s %(module)s '
                              '%(process)d %(thread)d %(message)s'
                },
            },
            'handlers': {
                'sentry': {
                    'level': 'ERROR',  # To capture more than ERROR, change to WARNING, INFO, etc.
                    'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
                    'tags': {'custom-tag': 'x'},
                },
                'console': {
                    'level': 'DEBUG',
                    'class': 'logging.StreamHandler',
                    'formatter': 'verbose'
                }
            },
            'loggers': {
                'django.db.backends': {
                    'level': 'ERROR',
                    'handlers': ['console'],
                    'propagate': False,
                },
                'raven': {
                    'level': 'DEBUG',
                    'handlers': ['console'],
                    'propagate': False,
                },
                'sentry.errors': {
                    'level': 'DEBUG',
                    'handlers': ['console'],
                    'propagate': False,
                },
            },
        }

OPBEAT_ORGANIZATION_ID = env.str('OPBEAT_ORGANIZATION_ID', default=None)
OPBEAT_APP_ID = env.str('OPBEAT_APP_ID', default=None)
OPBEAT_SECRET_TOKEN = env.str('OPBEAT_SECRET_TOKEN', default=None)

if OPBEAT_APP_ID is not None:
    INSTALLED_APPS += (
        'opbeat.contrib.django',
    )

    MIDDLEWARE = [
        'opbeat.contrib.django.middleware.OpbeatAPMMiddleware',
    ] + MIDDLEWARE

    OPBEAT = {
        'ORGANIZATION_ID': OPBEAT_ORGANIZATION_ID,
        'APP_ID': OPBEAT_APP_ID,
        'SECRET_TOKEN': OPBEAT_SECRET_TOKEN,
    }

########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                                 Celery & Websockets                                                  #
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################

# Settings should be moved to the new lowercase format
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#new-lowercase-settings

CELERY_BROKER_URL = env.str('CLOUDAMQP_URL', default='amqp://guest:guest@127.0.0.1')
BROKER_POOL_LIMIT = 1  # Will decrease connection usage
BROKER_HEARTBEAT = None  # We're using TCP keep-alive instead
BROKER_CONNECTION_TIMEOUT = 30  # May require a long timeout due to Linux DNS timeouts etc
CELERY_RESULT_BACKEND = None  # AMQP is not recommended as result backend as it creates thousands of queues
CELERY_SEND_EVENTS = False  # Will not create celeryev.* queues
CELERY_EVENT_QUEUE_EXPIRES = 60  # Will delete all celeryev. queues without consumers after 1 minute.

# When the setting CELERY_ALWAYS_EAGER is set to True, will all taskes be executed locally by blocking until the task
# returns. That is, tasks will be executed locally instead of being sent to the queue. This forces all calls to
# .delay()/.apply_async() that would normally get delegated to the worker to instead execute synchronously.

if ENV == 'local':
    CELERY_TASK_ALWAYS_EAGER = env.bool('CELERY_TASK_ALWAYS_EAGER', default=True)
else:
    CELERY_TASK_ALWAYS_EAGER = env.bool('CELERY_TASK_ALWAYS_EAGER', default=False)

CELERY_TASK_ALWAYS_EAGER = True

if CELERY_TASK_ALWAYS_EAGER is False:
    print(f"********************* CELERY_TASK_ALWAYS_EAGER is False!!!!!!! *********************")

########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                                 API & Authentication                                                 #
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################

ACCOUNT_ALLOW_REGISTRATION = env.bool('ACCOUNT_ALLOW_REGISTRATION', True)
ACCOUNT_CONFIRM_EMAIL_ON_GET = True
ACCOUNT_LOGOUT_ON_GET = True
ACCOUNT_LOGOUT_ON_PASSWORD_CHANGE = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_ADAPTER = 'apps.users.adapters.AccountAdapter'
ACCOUNT_LOGIN_ATTEMPTS_LIMIT = 5
ACCOUNT_LOGIN_ATTEMPTS_TIMEOUT = 300
# ACCOUNT_DEFAULT_HTTP_PROTOCOL="https"

EMAIL_VERIFICATION_REDIRECT_URL = env.str('EMAIL_VERIFICATION_REDIRECT_URL', None)

REST_SESSION_LOGIN = True
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'

AUTH_USER_MODEL = 'users.User'
LOGIN_REDIRECT_URL = 'api:redirect'
LOGIN_URL = 'admin:index'

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',
    # for specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',
)

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]

# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators


REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'apps.metronom_commons.permissions.IsAuthenticatedAndDeleteAllowedOnlyToAdmin',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
    'NON_FIELD_ERRORS_KEY': 'error',
    'EXCEPTION_HANDLER': 'apps.api.exceptions.core_exception_handler'

}

if ENV == 'local':
    REST_FRAMEWORK.pop('DEFAULT_RENDERER_CLASSES', None)

REST_AUTH_SERIALIZERS = {
    'USER_DETAILS_SERIALIZER': 'apps.users.serializers.JWTSignInSerializer',
}

REST_AUTH_REGISTER_SERIALIZERS = {
    'REGISTER_SERIALIZER': 'apps.tenants.serializers.TenantSignUpSerializer',
}

REST_FRAMEWORK_EXTENSIONS = {
    'DEFAULT_USE_CACHE': DRF_VIEW_CACHE_ALIAS,
}

CORS_ORIGIN_WHITELIST = env.list('CORS_ORIGIN_WHITELIST',
                                 [])  # Should have '*' for local, the site URL for production
CORS_ALLOW_HEADERS = default_headers + ('If-None-Match', 'Last-Modified', 'Accept-Language', 'If-Modified-Since')
CORS_EXPOSE_HEADERS = ('ETag', 'Last-Modified', 'HTTP_X_RESPONSE_ID', 'HTTP_GIT_BRANCH')

########################################################################################################################
#                                                 Authentication (JWT)                                                 #
########################################################################################################################

JWT_SECRET = env('JWT_SECRET', default='notsafeforproduction')
JWT_ISSUER_NAME = env.str('JWT_ISSUER_NAME', default='metronom-pro')

# TODO: Most of these options are not used by rest-auth.
JWT_AUTH = {
    'JWT_PAYLOAD_HANDLER': 'apps.authentication.jwt.payload_handler',
    # 'JWT_RESPONSE_PAYLOAD_HANDLER': 'config.jwt.response_payload_handler',
    'JWT_PAYLOAD_GET_USERNAME_HANDLER': 'apps.authentication.jwt.get_username_from_payload_handler',
    'JWT_SECRET_KEY': JWT_SECRET,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=60 * 60 * 72),
    'JWT_ALLOW_REFRESH': True,
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=60 * 60 * 12),
    'JWT_AUTH_HEADER_PREFIX': 'Bearer',
    'JWT_ISSUER': 'metronom-pro',
}

REST_USE_JWT = True  # TODO: Is this used? if getattr(settings, 'REST_USE_JWT', False): in rest-auth gives False

SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        "api_key": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header"
        },
    }
}

# Frontend wants to see docs, when in production mode
SHOW_DOCS_IN_PRODUCTION_MODE = env.bool('SHOW_DOCS_IN_PRODUCTION_MODE', False)

if SHOW_DOCS_IN_PRODUCTION_MODE is True:
    REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    )

    # if SHOW_DOCS_IN_PRODUCTION_MODE is True:
    #     SWAGGER_SETTINGS['USE_SESSION_AUTH'] = True

########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                            Metronom individual settings                                              #
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################

PROJECT_NAME = env.str('PROJECT_NAME', default='Metronom Pro')

TENANT_ROOT_SITE_ID = env.int('TENANT_ROOT_SITE_ID', default=SITE_ID)
TENANT_INVITE_EXPIRATION_IN_DAYS = 1

BASE_SUPERUSERS = env.json("BASE_SUPERUSERS", default=[])

FRONTEND_URL = env.str('FRONTEND_URL', default='http://localhost:3005')
BACKEND_URL = env.str('BACKEND_URL', default='http://localhost:8009')
PDF_URL = env.str('PDF_URL', default='http://pdfengine:8200')

MANAGEMENT_URLS = {
    'JENKINS': env.str('JENKINS_URL', default='http://ci.metronom-dev.ch'),
    'RANCHER': env.str('RANCHER_URL', default='http://rancher.metronom-dev.ch'),
    'SENTRY': env.str('SENTRY_URL', default='https://sentry.io/einhornmanufaktur/'),
    'MAILHOG': env.str('MAILHOG_URL', default='http://localhost:8025'),
    'SPHINX': env.str('SPHINX_URL', default='http://localhost:8007'),
    'GITHUB': env.str('GITHUB_URL', default='https://github.com/jensneuhaus/metronom-pro-backend'),
    'GITHUB_COMMIT': env.str('GITHUB_URL', default='https://github.com/jensneuhaus/metronom-pro-backend/commit/'),
    'REDIS_BROWSER': env.str('REDIS_BROWSER_URL', default='http://localhost:8019'),
    'POSTGRES_ADMIN': env.str('POSTGRES_ADMIN_URL',
                              default='http://localhost:5050/?pgsql=postgres&username=metronom&db=metronom&password=secret&ns=public'),
    'RABBITMQ': env.str('RABBITMQ_URL', default='http://localhost:15672'),
    'DOCKER': env.str('DOCKER_URL', default='https://hub.docker.com/r/einhornmanufaktur/metronom-backend/'),
}

METRONOM_SETTINGS = {
    'CURRENCY_CHOICES': {'value': env.list('METRONOM_SETTING_TENANT_CURRENCY_CHOICES', default=CURRENCY.MONEY_CHOICES)},
    'DEFAULT_AGENCY_CURRENCY': {'value': env.str('METRONOM_SETTING_DEFAULT_AGENCY_CURRENCY', default=CURRENCY.CHF)},
    'WORKDAY_LENGTH_IN_HOURS': {'value': Decimal(env.float('METRONOM_SETTING_WORKDAY_LENGTH_IN_HOURS', default=8)),
                                'editable_in_time_frame': 'monthly'},
    'DEFAULT_AGENCY_WORKING_HOURS': {'value': env.int('METRONOM_SETTING_DEFAULT_AGENCY_WORKING_HOURS', default=40)},
    'TENANT_NAME': {'value': env.str('METRONOM_SETTING_TENANT_NAME', default='Example Company')},
    'TENANT_EMAIL': {'value': env.str('METRONOM_SETTING_TENANT_EMAIL', default='mail@example-company.ch')},
    'PDF_ADDRESS_HEADLINE': {
        'value': env.str('PDF_ADDRESS_HEADLINE', default='Example Company · Example Street 123 · CH-8400 Winterthur')},
    'PDF_FOOTER': {'value': env.str(
        'PDF_FOOTER',
        default='Example Company ⋅ Doing examples, mostly ⋅ Example Street 123 ⋅ CH-8400 Winterthur</br>Telefon +41 12 345 67 89 ⋅ www.example-compny.ch ⋅ mail@example-company.ch'
    )},
    'NUMBER_FORMAT': {'value': env.list('METRONOM_SETTING_NUMBER_FORMAT', default=NUMBER_FORMAT.SWISS)},
    'DEFAULT_BILLING_CURRENCY': {'value': env.str('METRONOM_SETTING_DEFAULT_BILLING_CURRENCY', default=CURRENCY.CHF)},
    'DEFAULT_BILLING_PAYMENT_WITHIN': {'value': env.int('METRONOM_SETTING_DEFAULT_BILLING_PAYMENT_WITHIN', default=30)},
    'DEFAULT_TAX': {'value': Decimal(env.float('METRONOM_SETTING_DEFAULT_TAX', default=8))},
    'OFFER_CALLBACK_DEFAULT_IN_DAYS': {'value': env.int('METRONOM_SETTING_OFFER_CALLBACK_DEFAULT_IN_DAYS', default=10)},
    'OFFER_VALID_DEFAULT_IN_DAYS': {'value': env.int('METRONOM_SETTING_OFFER_VALID_DEFAULT_IN_DAYS', default=30)},
    'DEFAULT_PAGE_SIZE': {'value': env.int('METRONOM_SETTING_PAGE_SIZE', default=10)},
    'DEFAULT_MAX_PAGE_SIZE': {'value': env.int('METRONOM_SETTING_MAX_PAGE_SIZE', default=100)}
}

########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                           Other settings                                                             #
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################

if ENV == 'production':
    SECURE_SSL_REDIRECT = env.bool('SECURE_SSL_REDIRECT', default=False)
    SECURE_REDIRECT_EXEMPT = env.list('SECURE_REDIRECT_EXEMPT', default=['ht/'])

    # # In order to detect when a request is made via SSL in Django (for use in request.is_secure())
    # # https://devcenter.heroku.com/articles/http-routing#heroku-headers
    #
    SECURE_PROXY_SSL_HEADER = (env.str('SECURE_PROXY_SSL_HEADER', default='x-forwarded-proto'), "https")

    # https://docs.djangoproject.com/en/1.10/ref/settings/#std:setting-SESSION_COOKIE_SECURE
    SESSION_COOKIE_SECURE = env.bool('SESSION_COOKIE_SECURE', default=True)
    # https://docs.djangoproject.com/en/1.10/ref/settings/#session-cookie-httponly
    CSRF_COOKIE_HTTPONLY = env.bool('CSRF_COOKIE_HTTPONLY', default=True)
    # https://docs.djangoproject.com/en/1.10/ref/settings/#csrf-cookie-secure
    CSRF_COOKIE_SECURE = env.bool('CSRF_COOKIE_SECURE', default=True)

PHONENUMBER_DB_FORMAT = env.str('PHONENUMBER_DB_FORMAT', default='INTERNATIONAL')

ALLOWED_EMAIL_DOMAINS = [
    'jensneuhaus.de',
]

DEFAULT_PROTOCOL = env.str('DEFAULT_PROTOCOL', default='https')

AGENCY_COMPANY_PUBLIC_ID = env.int("AGENCY_COMPANY_PUBLIC_ID", default=1)

if ENV == 'local':
    DEFAULT_PROTOCOL = 'http'

DJANGO_MONEY_RATES = {
    'DEFAULT_BACKEND': 'djmoney_rates.backends.OpenExchangeBackend',
    'OPENEXCHANGE_URL': env.str('OPENEXCHANGE_URL', default=None),
    'OPENEXCHANGE_APP_ID': env.str('OPENEXCHANGE_APP_ID', default=None),
    'OPENEXCHANGE_BASE_CURRENCY': 'USD',
}

########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                               Testing                                                                #
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################


# RUNNING_TESTS should used really rarely because we want the CI to test the real production setup
if ENV == 'ci':
    # We are running with ENV=ci, so we are probably on Jenkins, set RUNNING_TESTS
    RUNNING_TESTS = True
else:
    # We are running py.test directly on local or production environment, set RUNNING_TESTS as well
    RUNNING_TESTS = env.bool('RUNNING_TESTS', default=False)

if RUNNING_TESTS:
    # Django uses strong hashing algorithms, these are not needed in testing
    PASSWORD_HASHERS = (
        'django.contrib.auth.hashers.MD5PasswordHasher',
    )

    # Disabling debugging speeds up things
    DEBUG = False
    TEMPLATE_DEBUG = False

    # Celery needs ot be taken care of
    CELERY_TASK_ALWAYS_EAGER = True
    CELERY_EAGER_PROPAGATES_EXCEPTIONS = True

    # No SSL in testing
    DEFAULT_PROTOCOL = 'http'
    API_LOGGING = False

DATABASE_ROUTERS = ['apps.legacy.router.LegacyRouter']
TESTING = os.path.basename(sys.argv[0]) in ('pytest', 'py.test')
LEGACY_DEFAULT_PASSWORD = env.str("DEFAULT_PASSWORD", "test123")
########################################################################################################################
#                                                                                                                      #
#                                                                                                                      #
#                                               DRF Search                                                             #
#                                                                                                                      #
#                                                                                                                      #
########################################################################################################################
DRF_SEARCH_SETTINGS = {
    "endpoint": "search",
    "postgres_full_text_search": False,
    "enable_cache": False,
    "cache_backend": "redis",
    "cache_timeout": 3600,
    "search_keyword_min_length": 3,
    "save_user_search": True,
}
