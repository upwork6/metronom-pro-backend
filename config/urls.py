from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import RedirectView
from rest_framework.routers import DefaultRouter
from rest_framework_nested.routers import NestedDefaultRouter
from rest_framework_swagger.views import get_swagger_view

from apps.accounting.views import AllocationCategoryViewSet, AllocationViewSet, CompanyAllocationViewSet
from apps.activities.views import ActivityGroupViewSet, StepViewSet, TemplateActivityViewSet
from apps.api.views import MetronomInfoAPIView
from apps.api.tests.views import ExceptionBasicViewSet
from apps.articles.views import ArticleGroupViewSet, TemplateArticleViewSet
from apps.autocompletes.views import UnitedAutocompleteView

from apps.backoffice.views import (AgencyBankAccountViewSet, SettingViewSet, TaxNoteViewSet, TermsCollectionViewSet,
                                   ThankYouNoteViewSet, TermsParagraphViewSet)
from apps.bankaccounts.views import BankAccountViewSet, CheckIBAN
from apps.companies.views import CompanyAutocompleteView, CompanyOfficeViewSet, CompanyViewSet, SectorViewSet
from apps.contacts.views import ContactListViewSet
from apps.contracts.views import UserContractViewSet
from apps.employments.views import EmploymentViewSet
from apps.metronom_commons.views import ReorderingView, BatchUpdateView
from apps.offers.views import (CampaignOffersViewSet, OffersViewSet, OffersStatusViewSet, CampaignOfferStatusViewSet,
                               ProjectOfferPDFView, LatestProjectOfferPDFView,
                               CampaignOfferPDFView, LatestCampaignOfferPDFView, ProjectOffersSearchView)
from apps.persons.views import PersonAutocompleteView, PersonViewSet, PromotionViewSet
from apps.phonenumbers.views import CheckPhoneNumber
from apps.projects.views import (CampaignProjectViewSet, CampaignViewSet, ProjectActivityViewSet, ProjectArticleViewSet,
                                 ProjectOrderingView, ProjectViewSet, ProjectCampaignViewSet, WorkLogViewSet, ProjectArticleExpenseViewSet,
                                 UserWorkLogViewSet, UserProjectActivityViewSet, DuplicateProjectViewSet)
from apps.search.views import Search, SearchHistory
from apps.users.views import FamilyViewSet, ProfileViewSet, UserViewSet
from apps.workinghours.views import (UserAbsenceViewSet, FlexWorkViewSet, HolidayViewSet, UserWorkingHoursViewSet,
                                     TenantAbsenceView, UserWorkingStatisticsViewSet, UserWorkYearViewSet,
                                     UserStatisticsViewSet)
from config.views import migration_ready

router = DefaultRouter()

router.register(r'companies', CompanyViewSet)
companies_router = NestedDefaultRouter(router, r'companies', lookup='company')
companies_router.register(r'offices', CompanyOfficeViewSet, base_name='company-offices')
companies_router.register(r'allocations', CompanyAllocationViewSet, base_name='company-allocations')

router.register(r'persons', PersonViewSet)

router.register(r'employments', EmploymentViewSet)
router.register(r'bankaccounts', BankAccountViewSet)

router.register(r'campaigns', CampaignViewSet)
campaign_router = NestedDefaultRouter(router, r'campaigns', lookup='campaign')
campaign_router.register('projects', CampaignProjectViewSet, base_name='project')
campaign_router.register('offers', CampaignOffersViewSet, base_name='offer')

router.register(r'projects_and_campaigns', ProjectCampaignViewSet, base_name="campaign-projects")


router.register(r'projects', ProjectViewSet)
projects_router = NestedDefaultRouter(router, r'projects', lookup='project')
projects_router.register('activities', ProjectActivityViewSet, base_name='activity')
projects_router.register('articles', ProjectArticleViewSet, base_name='article')
projects_router.register('offers', OffersViewSet, base_name='offer')

articles_router = NestedDefaultRouter(projects_router, r'articles', lookup='article')
articles_router.register('expenses', ProjectArticleExpenseViewSet, base_name='expense')

router.register(r'activity-groups', ActivityGroupViewSet)
router.register(r'steps', StepViewSet)
router.register(r'activities', TemplateActivityViewSet, base_name='activity')
router.register(r'allocations', AllocationViewSet)
router.register(r'allocation-categories', AllocationCategoryViewSet)

router.register(r'articles-groups', ArticleGroupViewSet)
router.register(r'articles', TemplateArticleViewSet)
router.register(r'settings', SettingViewSet)
router.register(r'taxnotes', TaxNoteViewSet)
router.register(r'thankyounotes', ThankYouNoteViewSet)
router.register(r'agencybankaccounts', AgencyBankAccountViewSet)
router.register(r'flexwork', FlexWorkViewSet)
router.register(r'holidays', HolidayViewSet)

router.register(r'sectors', SectorViewSet)
router.register(r'promotions', PromotionViewSet)

router.register(r'users', UserViewSet, base_name='user')
users_router = NestedDefaultRouter(router, r'users', lookup='user')
users_router.register(r'absences', UserAbsenceViewSet, base_name='absences')
users_router.register(r'contracts', UserContractViewSet, base_name='contract')
users_router.register(r'worklogs', UserWorkLogViewSet, base_name='user-worklog')
users_router.register(r'working_hours', UserWorkingHoursViewSet, base_name='user-working-hours')
users_router.register(r'working_statistics', UserWorkingStatisticsViewSet, base_name='user-working-statistics')
users_router.register(r'statistics', UserStatisticsViewSet, base_name='user-statistics')
users_router.register(r'work_years', UserWorkYearViewSet, base_name='user-work-year')
users_router.register(r'activities', UserProjectActivityViewSet, base_name='user-activities')
users_router.register(r'family_members', FamilyViewSet, base_name='family')
users_router.register(r'tenant_personnel_data', ProfileViewSet, base_name='tenant_personnel_data')

backoffice_router = DefaultRouter()
backoffice_router.register(r'terms', TermsCollectionViewSet, base_name='term')
backoffice_router.register(r'terms-paragraphs', TermsParagraphViewSet, base_name='terms-paragraph')

worklogs_router = DefaultRouter()
worklogs_router.register(r'worklogs', WorkLogViewSet, base_name='worklog')

agency_urls = [
    url(r'^absences/', TenantAbsenceView.as_view(), name='absences')
]

autocomplete_urls = [
    url(r'united/$', UnitedAutocompleteView.as_view(), name='united'),
    url(r'persons/$', PersonAutocompleteView.as_view(), name='persons'),
    url(r'companies/$', CompanyAutocompleteView.as_view(), name='companies')
]

api_helper_urls = [
    url(r'check/iban/$', CheckIBAN.as_view(), name='check_iban'),
    url(r'check/phonenumber/$', CheckPhoneNumber.as_view(), name='check_phone_number')
]

drf_endpoint = getattr(settings, "DRF_SEARCH_SETTINGS", None)

urlpatterns = (
    # Basic URLs + Devop
    url(r'^$', RedirectView.as_view(url='/admin/'), name='home'),
    url(r'^admin/', admin.site.urls),
    url(r'^crossdomain\.xml$', RedirectView.as_view(url=settings.STATIC_URL + 'crossdomain.xml')),
    url(r'^ht/', include('health_check.urls')),
    url(r'^90057a0e-3327-4650-9941-f7c5258667c1/', migration_ready),

    # Autocompletes & Authentication
    url(r'^autocompletes/', include(autocomplete_urls, namespace='autocompletes')),
    url(r'^authentication/', include('apps.authentication.urls')),

    # API
    url(r'^api/', include(router.urls, namespace='api')),
    url(r'^api/', include(companies_router.urls, namespace='api_company')),
    url(r'^api/', include(users_router.urls, namespace='api_users')),
    url(r'^api/', include(projects_router.urls, namespace='api_projects')),
    url(r'^api/', include(campaign_router.urls, namespace='api_campaigns')),
    url(r'^api/', include(articles_router.urls, namespace='api_articles')),
    url(r'^api/', include(worklogs_router.urls, namespace='api_worklogs')),
    url(r'^api/', include(api_helper_urls, namespace='api_helpers')),
    url(r'^api/contacts/$', ContactListViewSet.as_view({'get': 'list'}), name='contacts-list'),
    url(r'^api/agency/', include(agency_urls, namespace='api_agency')),
    url(r'^api/backoffice/', include(backoffice_router.urls, namespace='api_backoffice')),
    url(r'^api/offers/$', ProjectOffersSearchView.as_view({'get': 'list'}), name='search-offers'),
    url(r'^api/projects/(?P<project_pk>[0-9a-f-]+)/duplicate/',
        DuplicateProjectViewSet.as_view(), name="project-duplicate"),
    url(r'^projects/(?P<project_pk>[0-9a-f-]+)/offers/(?P<offer_pk>[0-9a-f-]+)/pdf/$',
        LatestProjectOfferPDFView.as_view(), name="latest-project-offer-pdf"),
    url(r'^projects/(?P<project_pk>[0-9a-f-]+)/offers/(?P<offer_pk>[0-9a-f-]+)/pdf/(?P<id>[0-9a-f-]+)/',
        ProjectOfferPDFView.as_view(), name="project-offer-pdf"),
    url(r'^campaigns/(?P<campaign_pk>[0-9a-f-]+)/offers/(?P<offer_pk>[0-9a-f-]+)/pdf/',
        LatestCampaignOfferPDFView.as_view(), name="latest-campaign-offer-pdf"),
    url(r'^campaigns/(?P<campaign_pk>[0-9a-f-]+)/offers/(?P<offer_pk>[0-9a-f-]+)/pdf/(?P<id>[0-9a-f-]+)/',
        CampaignOfferPDFView.as_view(), name="campaign-offer-pdf"),
    url(r'^api/projects/(?P<project_pk>[0-9a-f-]+)/offers/(?P<id>[0-9a-f-]+)/status/$',
        OffersStatusViewSet.as_view({"patch": "partial_update"}), name="offer-status"),
    url(r'^api/campaigns/(?P<campaign_pk>[0-9a-f-]+)/offers/(?P<id>[0-9a-f-]+)/status/$',
        CampaignOfferStatusViewSet.as_view({"patch": "partial_update"}), name="campaign-status"),
    url(r'^api/projects/(?P<project_pk>[0-9a-f-]+)/ordering/$',
        ProjectOrderingView.as_view(), name="project-ordering"),

    url(r'^api/search/$', csrf_exempt(Search.as_view()), name="search"),
    url(r'^api/search/history$', csrf_exempt(SearchHistory.as_view()), name="search-history"),
    url(r'^api/info/', MetronomInfoAPIView.as_view(), name='api_info'),

    url(r'^api/test/exceptions$', ExceptionBasicViewSet.as_view({'get': 'list'}), name='exception-test'),
    url(r'^api/test/exceptions/ValidationError$',
        ExceptionBasicViewSet.as_view({'get': 'validation_error'}), name='exception-test-validation-error'),
    url(r'^api/test/exceptions/ValidationError$', ExceptionBasicViewSet.as_view({'get': 'validation_error'}),
        name='exception-test-validation-error'),
    url(r'^api/reorder/$', ReorderingView.as_view(), name='reordering'),
    url(r'^api/batch-update/$', BatchUpdateView.as_view(), name='batch_update'),
)

if settings.STAGE == 'local' or settings.SHOW_DOCS_IN_PRODUCTION_MODE is True:
    from rest_framework.permissions import AllowAny
    from drf_yasg.views import get_schema_view
    from drf_yasg import openapi

    schema_view = get_schema_view(
        openapi.Info(
            title="Metronom Pro",
            default_version='v1',
            # description="Test description",
            # terms_of_service="https://www.google.com/policies/terms/",
            # contact=openapi.Contact(email="contact@snippets.local"),
            # license=openapi.License(name="BSD License"),
        ),
        validators=['flex', 'ssv'],
        public=True,
        permission_classes=(AllowAny,),
    )

    urlpatterns += (
        url(r'^docs/$', get_swagger_view(title='Metronom Pro API'), name='api_docs'),
        url(r'^swagger(?P<format>.json|.yaml)$', schema_view.without_ui(cache_timeout=1), name='schema-json'),
        url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
        url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    )

if 'silk' in settings.INSTALLED_APPS:
    urlpatterns += (
        url(r'^silk/', include('silk.urls', namespace='silk')),
    )
